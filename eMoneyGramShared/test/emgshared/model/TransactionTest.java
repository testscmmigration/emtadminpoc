package emgshared.model;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.PropertyLoader;
import emgshared.util.Constants;
import static org.mockito.Mockito.*;


public class TransactionTest {
	private Transaction t;
	private PropertyLoader pv;
	
	@Before
	public void setUp() throws Exception {
		t = new Transaction();
		t.setEmgTranTypeCode(TransactionType.MONEY_TRANSFER_SEND_CODE);
		t.setStatus(TransactionStatus.APPROVED_CC_AUTH);
		
		t.setPartnerSiteId(Constants.MGOUK_PARTNER_SITE_ID);
		t.setTCProviderCode(Constants.GLOBAL_COLLECT_PROVIDER_CODE);
		
		t.setCreateDate(new Date(System.currentTimeMillis() - 84600000)); //Yesterday
		t.setTranStatDate(new Date(System.currentTimeMillis() - 84601000)); //Yesterday

		//Mocks the environment variables used internally in the method
		pv = mock(PropertyLoader.class);
		when(pv.getString("USE_GC_For_Processing")).thenReturn("false");
		EMTSharedContainerProperties.initialize(pv);
		//t.setPartnerSiteId(Constants.WAP_PARTNER_SITE_ID);
	}

	@Test
	public void testCanShowPartialRefundButtonForPartnerSite() {
		
		//Default UK
		boolean canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals(true, canShowPartialRefundButton);

		when(pv.getString("USE_GC_For_Processing")).thenReturn("true");
		EMTSharedContainerProperties.initialize(pv);

		t.setPartnerSiteId(Constants.MGO_PARTNER_SITE_ID);
		t.setTCProviderCode(Constants.GLOBAL_COLLECT_PROVIDER_CODE);

		canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("MGO_PARTNER_SITE_ID-GLOBAL_COLLECT_PROVIDER_CODE", true, canShowPartialRefundButton);

		t.setPartnerSiteId(Constants.WAP_PARTNER_SITE_ID);
		t.setTCProviderCode(666); //Invalid provider code

		canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("WAP_PARTNER_SITE_ID Invalid provider code", false, canShowPartialRefundButton);

		t.setPartnerSiteId(Constants.WAP_PARTNER_SITE_ID);
		t.setTCProviderCode(Constants.GLOBAL_COLLECT_PROVIDER_CODE);

		canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("WAP_PARTNER_SITE_ID Correct provider code", true, canShowPartialRefundButton);
		
	}

	
	@Test
	public void testCanShowPartialRefundButtonForCorrectStatus() {

		t.setStatus(TransactionStatus.CANCELED_CC_CANCELED);
		boolean canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("CANCELED_CC_CANCELED", true, canShowPartialRefundButton);
	
		t.setStatus(TransactionStatus.SENT_CC_AUTH);
		canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("SENT_CC_AUTH ", true, canShowPartialRefundButton);

		t.setStatus(TransactionStatus.SENT_CC_AUTH_REVERSED);
		canShowPartialRefundButton = t.canShowPartialRefundButton();
		assertEquals("SENT_CC_AUTH_REVERSED ", false, canShowPartialRefundButton);
	}	
}
