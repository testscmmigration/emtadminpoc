package shared.mgo.dto;

import java.util.ArrayList;
import java.util.List;

public class MGOProduct {
	
	public static final String BILL_PAY = "EPSEND";
	public static final String SAME_DAY = "MGSEND";
	public static final String ECONOMY = "ESSEND";
	public static final String BILL_PAY_CASH = "EPCASH";
	public static final String SAME_DAY_CASH = "MGCASH";
	private String productName;
	private Boolean enabled;
    private ArrayList mgoPaymentOptions;
	    
	public ArrayList getMgoPaymentOptions() {
			return mgoPaymentOptions;
	}
	
	public void setMgoPaymentOptions(List mgoPaymentOptions) {
		this.mgoPaymentOptions = (ArrayList) mgoPaymentOptions;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	

}
