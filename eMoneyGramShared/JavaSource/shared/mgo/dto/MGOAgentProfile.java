package shared.mgo.dto;


public class MGOAgentProfile {

	private String MGOAgentKeyIdentifier;
	private MGOProduct mgoProduct;
	private Integer profileId;
	private String mgAgentId;
	private String agentSequence;
	private String token;
	private String mgoAgentProfileDescription;

	public String getMgoAgentProfileDescription() {
		return mgoAgentProfileDescription;
	}
	public void setMgoAgentProfileDescription(String mgoAgentProfileDescription) {
		this.mgoAgentProfileDescription = mgoAgentProfileDescription;
	}
	public String getMGOAgentIdentifier() {
		return MGOAgentKeyIdentifier;
	}
	public MGOProduct getMgoProduct() {
		return mgoProduct;
	}
	public void setMgoProduct(MGOProduct mgoProduct) {
		this.mgoProduct = mgoProduct;
	}
	public void setMGOAgentIdentifier(String agentIdentifier) {
		MGOAgentKeyIdentifier = agentIdentifier;
	}
	public Integer getProfileId() {
		return profileId;
	}
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	public String getAgentId() {
		return mgAgentId;
	}
	public void setAgentId(String agentId) {
		this.mgAgentId = agentId;
	}
	public String getAgentSequence() {
		return agentSequence;
	}
	public void setAgentSequence(String agentSequence) {
		this.agentSequence = agentSequence;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

}
