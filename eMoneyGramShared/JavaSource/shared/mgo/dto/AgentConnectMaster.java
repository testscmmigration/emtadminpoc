/*
 * Created on Aug 31, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package shared.mgo.dto;

import java.util.ArrayList;
import java.util.HashMap;

import shared.mgo.services.MGOServiceFactory;

/**
 * @author T004
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AgentConnectMaster {

    private static HashMap countryList;
    private static HashMap currencyList;
    private static HashMap deliveryOptionList;
    private static HashMap stateProvinceList;
    private static ArrayList receiveCountryRequirementsList;
    
	

	
	/**
	 * @return Returns the receiveCountryRequirementsList.
	 */
	public static ArrayList getReceiveCountryRequirementsList() {
		return receiveCountryRequirementsList;
	}
	/**
	 * @param receiveCountryRequirementsList The receiveCountryRequirementsList to set.
	 */
	public static void setReceiveCountryRequirementsList(
			ArrayList receiveCountryRequirementsList) {
		AgentConnectMaster.receiveCountryRequirementsList = receiveCountryRequirementsList;
	}
	
	/**
	 * @return Returns the countryList.
	 */
	public static HashMap getCountryList() {
		if ((countryList == null) || countryList.isEmpty()) {
			try {
				MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank().refreshMasterFromAgentConnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return (countryList != null) ? countryList : new HashMap();
	}
	/**
	 * @param countryList The countryList to set.
	 */
	public static void setCountryList(HashMap countryList) {
		AgentConnectMaster.countryList = countryList;
	}
	/**
	 * @return Returns the currencyList.
	 */
	public static HashMap getCurrencyList() {
		return currencyList;
	}
	/**
	 * @param currencyList The currencyList to set.
	 */
	public static void setCurrencyList(HashMap currencyList) {
		AgentConnectMaster.currencyList = currencyList;
	}
	/**
	 * @return Returns the deliveryOptionList.
	 */
	public static HashMap getDeliveryOptionList() {
		return deliveryOptionList;
	}
	/**
	 * @param deliveryOptionList The deliveryOptionList to set.
	 */
	public static void setDeliveryOptionList(HashMap deliveryOptionList) {
		AgentConnectMaster.deliveryOptionList = deliveryOptionList;
	}
	/**
	 * @return Returns the stateProvinceList.
	 */
	public static HashMap getStateProvinceList() {
		return stateProvinceList;
	}
	/**
	 * @param stateProvinceList The stateProvinceList to set.
	 */
	public static void setStateProvinceList(HashMap stateProvinceList) {
		AgentConnectMaster.stateProvinceList = stateProvinceList;
	}
}
