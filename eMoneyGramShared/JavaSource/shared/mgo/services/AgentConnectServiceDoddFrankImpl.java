/*
 * Created on Dic 5, 2012
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package shared.mgo.services;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import shared.mgo.dto.AgentConnectMaster;
import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOCountry;
import shared.mgo.dto.MGOProductFieldInfo;
import shared.mgo.dto.ServiceOption;

import com.moneygram.agentconnect1305.wsclient.CountryCurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.CountryInfo;
import com.moneygram.agentconnect1305.wsclient.CurrencyInfo;
import com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo;
import com.moneygram.agentconnect1305.wsclient.FQDOInfo;
import com.moneygram.agentconnect1305.wsclient.FeeInfo;
import com.moneygram.agentconnect1305.wsclient.FeeLookupResponse;
import com.moneygram.agentconnect1305.wsclient.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.agentconnect1305.wsclient.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.agentconnect1305.wsclient.GetFieldsForProductResponse;
import com.moneygram.agentconnect1305.wsclient.ProductFieldInfo;
import com.moneygram.agentconnect1305.wsclient.ProductProfileItem;
import com.moneygram.agentconnect1305.wsclient.ProductType;
import com.moneygram.agentconnect1305.wsclient.ProductVariant;
import com.moneygram.agentconnect1305.wsclient.ProfileResponse;
import com.moneygram.agentconnect1305.wsclient.ReceiveCountryRequirementsInfo;
import com.moneygram.agentconnect1305.wsclient.StateProvinceInfo;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.dataaccessors.CountryExceptionDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.model.CountryExceptionBean;
import emgshared.property.EMTSharedDynProperties;

/**
 * @author T004
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AgentConnectServiceDoddFrankImpl implements
        AgentConnectServiceDoddFrank {

    final private String BRAZIL = "BRA";
    final private String USA = "USA";
    final private String USD = "USD";
    final private String ALL = "all";
    final private String USD_PAYOUT = "usd_payout";
    final private String FIXED_RATE = "fixed_rate";
    final private String US_ONLY = "us_only";
    final private String DELIVERY_OPTION_WILL_CALL = "WILL_CALL";
    private static HashMap deliveryOptionEMTNames = new HashMap();
    static Logger logger = LogFactory.getInstance()
                                     .getLogger(
                                             AgentConnectServiceDoddFrankImpl.class);

    static {
        try {
            System.out.println("In static initializer");
            deliveryOptionEMTNames.put("0", "Pick up at Agent");
            deliveryOptionEMTNames.put("4", "Pick up at Bancomer");
            deliveryOptionEMTNames.put("5", "Pick up at Cambio Plus");
            deliveryOptionEMTNames.put("6", "Home Delivery");
            deliveryOptionEMTNames.put("7", "Home Delivery");
            deliveryOptionEMTNames.put("8", "Home Delivery");
            deliveryOptionEMTNames.put("9", "ATM Card Load");
            deliveryOptionEMTNames.put("10", "Account Deposit");
            deliveryOptionEMTNames.put("11", "Pick up at");
        } catch (Exception e) {
            System.out.println("Error in AgentConnectServiceImpl static loader: "
                    + e.getMessage());
            e.printStackTrace();
        }

    }

    private static final AgentConnectServiceDoddFrankImpl instance = new AgentConnectServiceDoddFrankImpl();

    // private AgentConnectAccessor acc = new AgentConnectAccessor();

    private AgentConnectServiceDoddFrankImpl() {
    }

    public static final AgentConnectServiceDoddFrankImpl getInstance() {
        return instance;
    }

    public MGOCountry getCountry(String countryCode) throws Exception {

        MGOCountry country = null;
        Iterator iter1 = AgentConnectMaster.getCountryList()
                                           .values()
                                           .iterator();

        while (iter1.hasNext()) {
            country = (MGOCountry) iter1.next();
            if (countryCode.equals(country.getCountryCode())) {
                return country;
            }
        }

        return country;
    }

    // public AgentConnectAccessor getAgentConnectAccessor() throws Exception{
    // return acc;
    // }

    public void refreshMasterFromAgentConnect() throws Exception {
        // use MGO Same Day agent for AC Master cache
        EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        AgentConnectAccessorDoddFrank acc = null;
        try {
            MGOAgentProfileService maps = MGOServiceFactory.getInstance()
                                                           .getMGOAgentProfileService();
            // use MGO Same Day agent for init of general caching
            // (countries/states/etc.)
            MGOAgentProfile mgoAgentProfile = maps.getMGOProfile("MGO",
                    "MGSEND");
            acc = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
            if (acc != null) {
                acc.refreshCodeTablesFromAgentConnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error refreshing Code tables."
                    + e.getLocalizedMessage());
        }
        CountryInfo[] countryInfos = (acc != null) ? acc.getCountryInfo()
                : new CountryInfo[0];
        DeliveryOptionInfo[] deliveryOptionInfos = (acc != null) ? acc.getDeliveryOptionInfo()
                : new DeliveryOptionInfo[0];
        CurrencyInfo[] currencyInfos = (acc != null) ? acc.getCurrencyInfo()
                : new CurrencyInfo[0];
        StateProvinceInfo[] stateProvinceInfos = (acc != null) ? acc.getStateProvinceInfo()
                : new StateProvinceInfo[0];
        CountryCurrencyInfo[] countryCurrencyInfos = (acc != null) ? acc.getCountryCurrencyInfo()
                : new CountryCurrencyInfo[0];
        ReceiveCountryRequirementsInfo[] requirementsInfos = (acc != null) ? acc.getRequirementsInfo()
                : new ReceiveCountryRequirementsInfo[0];
        ProfileResponse pr = (acc != null) ? acc.getProfile()
                : new ProfileResponse();
        ProductProfileItem[] ppi = pr.getProductProfileItem();

        // Integrating the CountryException List
        HashMap cntryExceptions = new HashMap();
        Iterator itExp = getCntryExceptions().iterator();

        while (itExp.hasNext()) {
            CountryExceptionBean ceb = (CountryExceptionBean) itExp.next();
            cntryExceptions.put(ceb.getRcvIsoCntryCode(), ceb);
        }
        // Creating Delivery Options Map
        HashMap deliveryOptions = new HashMap();
        for (int i = 0; i < deliveryOptionInfos.length; ++i) {
            switch (deliveryOptionInfos[i].getDeliveryOptionID()
                                          .intValue()) {
            case 0: // hardcode for WILL_CALL for option 0 because of Agent
                    // Connect bug
            case 15: // hardcode for ONLY_AT for option 15 (WAP)
                deliveryOptions.put(
                        deliveryOptionInfos[i].getDeliveryOptionID(),
                        deliveryOptionInfos[i]);
            default:
                for (int j = 0; j < ppi.length; ++j) {
                    if ((ppi[j].getKey().equalsIgnoreCase("ALLOW_DEL_OPT"))
                            && (ppi[j].getValue() != null)
                            && (ppi[j].getIndex() == deliveryOptionInfos[i].getDeliveryOptionID()
                                                                           .intValue())
                            && (ppi[j].getValue().equalsIgnoreCase("Y"))) {
                        deliveryOptions.put(
                                deliveryOptionInfos[i].getDeliveryOptionID(),
                                deliveryOptionInfos[i]);

                    }
                }
            }
        }

        // Creating Currency Map
        HashMap currencies = new HashMap();
        for (int i = 0; i < currencyInfos.length; i++) {
            currencies.put(currencyInfos[i].getCurrencyCode(), currencyInfos[i]);
        }

        // Creating CountryCurrency List
        ArrayList countryCurrencies = new ArrayList();
        for (int i = 0; i < countryCurrencyInfos.length; i++) {
            countryCurrencies.add(countryCurrencyInfos[i]);
        }

        // Creating ReceiveCountryRequirementsInfo List
        ArrayList receiveCountryRequirements = new ArrayList();
        for (int i = 0; i < requirementsInfos.length; i++) {
            receiveCountryRequirements.add(requirementsInfos[i]);
        }

        // Creating State/Province Map
        HashMap stateProvinces = new HashMap();
        for (int i = 0; i < stateProvinceInfos.length; i++) {
            // filter out Puerto Rico as a state because it causes receive
            // errors, it is on the Country List instead.
            if (!stateProvinceInfos[i].getStateProvinceCode()
                                      .equals("PR"))
                stateProvinces.put(
                        stateProvinceInfos[i].getStateProvinceCode(),
                        stateProvinceInfos[i]);
        }

        System.out.println("Starting Country Loop : " + sdf.format(new Date()));
        System.out.println("Country Count from AC 13.05 : "
                + countryInfos.length);

        // Creating Country Map
        HashMap countries = new HashMap();

        // Scanning thru Countries and adding all its properties
        for (int i = 0; i < countryInfos.length; i++) {

            MGOCountry country = new MGOCountry();

            country.setCountryCode(countryInfos[i].getCountryCode());
            country.setCountryLegacyCode(countryInfos[i].getCountryLegacyCode());
            country.setCountryName(countryInfos[i].getCountryName());
            country.setDirectedSendCountry(countryInfos[i].isDirectedSendCountry());
            // country.setMgDirectedSendCountry(countryInfos[i].isMgDirectedSendCountry());
            country.setReceiveActive(countryInfos[i].isReceiveActive());
            country.setSendActive(countryInfos[i].isSendActive());
            country.setBaseReceiveCurrency(countryInfos[i].getBaseReceiveCurrency());

            // Getting all Currencies for this country and checking for
            // Multi-Currency
            ArrayList specificCountryCurrencies = new ArrayList();
            ArrayList multiCurrencyList = new ArrayList();
            HashMap multiCurrency = new HashMap();

            Iterator it = countryCurrencies.iterator();
            while (it.hasNext()) {
                CountryCurrencyInfo cci = (CountryCurrencyInfo) it.next();
                if (cci.getCountryCode()
                       .equalsIgnoreCase(countryInfos[i].getCountryCode())) {
                    specificCountryCurrencies.add(cci);
                    if (!multiCurrency.containsKey(cci.getReceiveCurrency())) {
                        multiCurrencyList.add(currencies.get(cci.getReceiveCurrency()));
                        multiCurrency.put(cci.getReceiveCurrency(), cci);
                    }
                }
            }

            country.setCountryCurrencyList(specificCountryCurrencies);
            country.setReceiveCurrencyList(multiCurrencyList);

            // Multi-Currency
            if (multiCurrency.size() > 1)
                country.setMultiCurrencyCountry(true);

            // Getting State/Province for this country
            ArrayList specificStateProvinces = new ArrayList();
            Iterator it1 = stateProvinces.keySet()
                                         .iterator();
            while (it1.hasNext()) {
                StateProvinceInfo spi = (StateProvinceInfo) stateProvinces.get(it1.next());
                // TODO: add sort ???
                // StateProvinceInfo.setSortBy("stateProvinceName");
                if (spi.getCountryCode()
                       .equalsIgnoreCase(countryInfos[i].getCountryCode()))
                    specificStateProvinces.add(spi);
            }
            // Collections.sort(specificStateProvinces);
            country.setStateProvinceList(specificStateProvinces);

            // Getting FQDOs for this country
            if (country.isDirectedSendCountry()) {
                try {
                    FQDOInfo[] fQDOInfos = (acc != null) ? acc.getFQDOsForCountryResponse(
                            country.getCountryCode())
                                                              .getFqdoInfo()
                            : new FQDOInfo[0];
                    country.setFQDOList(Arrays.asList(fQDOInfos));
                } catch (Throwable e) {
                    System.err.println("Country '"
                            + country.getCountryName()
                            + "' is flagged as a directed sends country but has not been setup properly in the database.");
                }
            }

            // Setting up esCaliforniaAllowed flag
            if (country.getCountryCode()
                       .equalsIgnoreCase(BRAZIL)) {
                country.setEsCaliforniaAllowed(false);
            } else {
                if (dynProps.getSpecialRestriction_EconomyService()
                            .equalsIgnoreCase(ALL)) {
                    country.setEsCaliforniaAllowed(true);
                }

                if (dynProps.getSpecialRestriction_EconomyService()
                            .equalsIgnoreCase(USD_PAYOUT)) {
                    if (multiCurrency.containsKey(USD))
                        country.setEsCaliforniaAllowed(true);
                    else
                        country.setEsCaliforniaAllowed(false);
                }

                if (dynProps.getSpecialRestriction_EconomyService()
                            .equalsIgnoreCase(FIXED_RATE)) {
                    boolean indicativeRateAvailable = false;
                    Iterator itRate = country.getCountryCurrencyList()
                                             .iterator();
                    while (itRate.hasNext()) {
                        CountryCurrencyInfo cci = (CountryCurrencyInfo) itRate.next();
                        // NOT SURE HOW TO HANDLE MULTIPLE CURRENCY PAYOUT
                        // COUNTRY HERE.
                        if (cci.isIndicativeRateAvailable()) {
                            indicativeRateAvailable = true;
                            break;
                        }
                    }
                    if (indicativeRateAvailable)
                        country.setEsCaliforniaAllowed(false);
                    else
                        country.setEsCaliforniaAllowed(true);
                }

                if (dynProps.getSpecialRestriction_EconomyService()
                            .equalsIgnoreCase(US_ONLY)) {
                    if (country.getCountryCode()
                               .equalsIgnoreCase(USA))
                        country.setEsCaliforniaAllowed(true);
                }

                if (!dynProps.isEconomyCaliforniaSender()) {
                    country.setEsCaliforniaAllowed(false);
                }

            }

            // Setting up mgCaliforniaAllowed flag
            if (dynProps.getSpecialRestriction_SameDayService()
                        .equalsIgnoreCase(ALL)) {
                country.setMgCaliforniaAllowed(true);
            }

            if (dynProps.getSpecialRestriction_SameDayService()
                        .equalsIgnoreCase(USD_PAYOUT)) {
                if (multiCurrency.containsKey(USD))
                    country.setMgCaliforniaAllowed(true);
                else
                    country.setMgCaliforniaAllowed(false);
            }

            if (dynProps.getSpecialRestriction_SameDayService()
                        .equalsIgnoreCase(FIXED_RATE)) {
                boolean indicativeRateAvailable = false;
                Iterator itRate = country.getCountryCurrencyList()
                                         .iterator();
                while (itRate.hasNext()) {
                    CountryCurrencyInfo cci = (CountryCurrencyInfo) itRate.next();
                    // NOT SURE HOW TO HANDLE MULTIPLE CURRENCY PAYOUT COUNTRY
                    // HERE.
                    if (cci.isIndicativeRateAvailable()) {
                        indicativeRateAvailable = true;
                        break;
                    }
                }
                if (indicativeRateAvailable)
                    country.setMgCaliforniaAllowed(false);
                else
                    country.setMgCaliforniaAllowed(true);
            }

            if (dynProps.getSpecialRestriction_SameDayService()
                        .equalsIgnoreCase(US_ONLY)) {
                if (country.getCountryCode()
                           .equalsIgnoreCase(USA))
                    country.setMgCaliforniaAllowed(true);
            }

            if (!dynProps.isEconomyCaliforniaSender()) {
                country.setMgCaliforniaAllowed(false);
            }

            // Disabling the country if it is in the Exception List and
            // isSndAllowed is 'N'
            CountryExceptionBean ceb = (CountryExceptionBean) cntryExceptions.get(country.getCountryCode());

            if (ceb != null) {
                if (ceb.isSndAllowed()) {
                    country.setEmtDisabled(false);
                } else {
                    country.setEmtDisabled(true);
                }

                country.setSndMaxAmtNbr(ceb.getSndMaxAmtNbr());
                country.setSndThrldWarnAmtNbr(ceb.getSndThrldWarnAmtNbr());
            } else {
                country.setEmtDisabled(false);
                country.setSndMaxAmtNbr(0.0F);
                country.setSndThrldWarnAmtNbr(0.0F);
            }

            // Generate ServiceOption List
            countries.put(countryInfos[i].getCountryCode(), country);

            ArrayList sol = (ArrayList) getDeliveryOptionInfos(country,
                    deliveryOptions, currencies);
            country.setServiceOptionList(sol);
            it = sol.iterator();
            boolean directedSendCountry = false;
            // see if at least 1 DS service option, if not then set country DS
            // flag to false
            while (it.hasNext()) {
                ServiceOption so = (ServiceOption) it.next();
                if (so.isDirectedSend())
                    directedSendCountry = true;
            }
            // doesn't matter what any other settings are, NEVER allow CA to BRA
            // until Legal allows.
            if (country.getCountryCode()
                       .equalsIgnoreCase("BRA"))
                country.setMgCaliforniaAllowed(false);
            country.setDirectedSendCountry(directedSendCountry);
        }

        AgentConnectMaster.setDeliveryOptionList(deliveryOptions);
        AgentConnectMaster.setCurrencyList(currencies);
        AgentConnectMaster.setStateProvinceList(stateProvinces);
        AgentConnectMaster.setReceiveCountryRequirementsList(receiveCountryRequirements);
        AgentConnectMaster.setCountryList(countries);

        System.out.println("Ending Country Loop : " + sdf.format(new Date()));
    }

    public Collection getStateProvinceInfo(String countryCode) throws Exception {
        MGOCountry country = getCountry(countryCode);

        return country.getStateProvinceList();
    }

    public Collection getFQDOInfo(String countryCode) throws Exception {
        MGOCountry country = getCountry(countryCode);
        return country.getFQDOList();
    }

    public Collection getCountryCurrencyInfo(String countryCode)
            throws Exception {
        MGOCountry country = getCountry(countryCode);
        return country.getCountryCurrencyList();
    }

    public Map getDeliveryOptionInfo() throws Exception {
        return AgentConnectMaster.getDeliveryOptionList();
    }

    public Collection getReceiveCurrencyInfo(String countryCode)
            throws Exception {
        MGOCountry country = getCountry(countryCode);
        return country.getReceiveCurrencyList();
    }

    public Map getCountries() throws Exception {
        HashMap countries = AgentConnectMaster.getCountryList();
        Collections.sort((ArrayList) countries.values());
        return countries;
    }

    public List getCntryExceptions() throws Exception {

        ArrayList list = new ArrayList();
        CountryExceptionDAO dao = null;
        // CountryMasterBean cmb = null;

        try {
            dao = new CountryExceptionDAO();
            list = (ArrayList) dao.getCntryExceptions("USA"); // TODO validate
                                                              // this hard-coded
                                                              // country; would
                                                              // a country
                                                              // parameter make
                                                              // sense?
            safeCommit(dao);
        } catch (Exception e) {
            logger.error("Problems obtaining country exceptions", e);
            System.err.println("Problems obtaining country exceptions: "
                    + e.getMessage());
            e.printStackTrace();
            if (dao != null) {
                dao.rollback();
            }
            throw e;
        } catch (Throwable t) {
            logger.error("Fatal problems obtaining country exceptions", t);
            System.err.println("Fatal problems obtaining country exceptions: "
                    + t.getMessage());
            t.printStackTrace();
            if (dao != null) {
                dao.rollback();
            }
            throw new Exception(t);
        } finally {
            if (dao != null) {
                dao.close();
            }
        }
        return list;
    }

    private void safeCommit(CountryExceptionDAO dao) {
        try {
            dao.commit();
        } catch (DataSourceException e) {
            logger.error("WARN: Commit was not possible. This might be possible and acceptable in Java EE environments");
        }
    }

    public DeliveryOptionInfo getDeliveryOptionInfo(int deliveryOptionId)
            throws Exception {
        DeliveryOptionInfo doi = null;
        Iterator iter1 = AgentConnectMaster.getDeliveryOptionList()
                                           .values()
                                           .iterator();

        while (iter1.hasNext()) {
            doi = (DeliveryOptionInfo) iter1.next();
            if (deliveryOptionId == doi.getDeliveryOptionID()
                                       .intValue()) {
                return doi;
            }
        }

        return doi;
    }

    public DeliveryOptionInfo getDeliveryOptionInfo(String deliveryOptionCode)
            throws Exception {
        HashMap dois = AgentConnectMaster.getDeliveryOptionList();
        Iterator iter = dois.values()
                            .iterator();
        DeliveryOptionInfo doi = null;

        while (iter.hasNext()) {
            doi = (DeliveryOptionInfo) iter.next();
            if (deliveryOptionCode.equalsIgnoreCase(doi.getDeliveryOption())) {
                return doi;
            }
        }
        return doi;
    }

    // express pay fee lookup requires some special parms, hence a method all
    // it's own
    // mgoAgentId is the Sending agent
    public FeeLookupResponse epFeeLookup(MGOAgentProfile mgoAgentProfile,
            String agencyId, String amount) throws Exception {
        AgentConnectAccessorDoddFrank acc = null;
        FeeLookupResponse flr = null;
        try {
            acc = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
            flr = acc.feeLookup(new BigDecimal(amount), USA, ProductType.BP,
                    "USD", DELIVERY_OPTION_WILL_CALL, agencyId, null);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return flr;
    }

    public FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile,
            BigDecimal amount, String recCountry, String rewardsNumber)
            throws Exception {
        AgentConnectAccessorDoddFrank acc = null;
        FeeLookupResponse flr = null;
        try {
            acc = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
            flr = acc.feeLookup(amount, recCountry, ProductType.SEND, null,
                    DELIVERY_OPTION_WILL_CALL, null, rewardsNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return flr;
    }

    public FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile,
            BigDecimal amount, String recCountry, String recCurrency,
            String deliveryOption, String rewardsNumber) throws Exception {
        AgentConnectAccessorDoddFrank acc = null;
        FeeLookupResponse flr = null;
        try {
            acc = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
            flr = acc.feeLookup(amount, recCountry, ProductType.SEND,
                    recCurrency, deliveryOption, rewardsNumber, null);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return flr;
    }

    public FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile,
            BigDecimal amount, String recCountry, String recCurrency,
            String rewardsNumber) throws Exception {
        FeeLookupResponse flr = null;
        try {
            AgentConnectAccessorDoddFrank acc = new AgentConnectAccessorDoddFrank(
                    mgoAgentProfile);
            acc = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
            flr = acc.feeLookup(amount, recCountry, ProductType.SEND,
                    recCurrency, DELIVERY_OPTION_WILL_CALL, null, rewardsNumber);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return flr;
    }

    public Collection getFilteredCountryList(String state) throws Exception {
        HashMap coutries = AgentConnectMaster.getCountryList();

        if (!"CA".equalsIgnoreCase(state)) {
            ArrayList countryList = new ArrayList();
            Iterator iter = coutries.keySet()
                                    .iterator();
            while (iter.hasNext()) {
                MGOCountry ci = (MGOCountry) coutries.get(iter.next());
                countryList.add(ci);
            }
            Collections.sort(countryList);
            return (countryList);
        }

        // California cannot send to Brazil because it must be fixed rates
        // for both send and receive countries

        ArrayList newList = new ArrayList();
        Iterator iter = coutries.keySet()
                                .iterator();

        while (iter.hasNext()) {
            MGOCountry ci = (MGOCountry) coutries.get(iter.next());
            if (!"BRA".equalsIgnoreCase(ci.getCountryCode())
                    && !ci.isEmtDisabled()) {
                newList.add(ci);
                Collections.sort(newList);
            }
        }
        return (newList);
    }

    public Collection getActiveCountryList() throws Exception {

        HashMap coutries = AgentConnectMaster.getCountryList();

        List newList = new ArrayList();
        Iterator iter = coutries.keySet()
                                .iterator();

        while (iter.hasNext()) {
            MGOCountry ci = (MGOCountry) coutries.get(iter.next());
            if ((!ci.isEmtDisabled()) && (ci.isReceiveActive())) {
                newList.add(ci);
            }
        }

        Collections.sort(newList);
        return newList;
    }

    public Collection getEconomyCountryList(String originationStateCode)
            throws Exception {

        if (originationStateCode != null && originationStateCode.equals("CA")) {
            HashMap coutries = AgentConnectMaster.getCountryList();

            EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
            /*
             * If the Economy California Sender switch is turned off then return
             * null as the user is not allowed to do an Economy Service send if
             * they live in CA.
             */
            if (!dynProps.isEconomyCaliforniaSender()) {
                return null;
            }

            List newList = new ArrayList();
            Iterator iter = coutries.values()
                                    .iterator();

            while (iter.hasNext()) {
                MGOCountry ci = (MGOCountry) iter.next();
                if ((!ci.isEmtDisabled()) && ci.isEsCaliforniaAllowed()
                        && ci.isReceiveActive()) {
                    newList.add(ci);
                }
            }
            Collections.sort(newList);
            return newList;
        } else {
            return getEMTEnabledCountryList();
        }
    }

    public Collection getSameDayCountryList(String originationStateCode)
            throws Exception {
        HashMap coutries = AgentConnectMaster.getCountryList();

        if (originationStateCode != null && originationStateCode.equals("CA")) {
            // Return Same Day Service California Countries.
            List newList = new ArrayList();
            Iterator iter = coutries.values()
                                    .iterator();

            while (iter.hasNext()) {
                MGOCountry ci = (MGOCountry) iter.next();
                if (!ci.isEmtDisabled() && ci.isMgCaliforniaAllowed()
                        && ci.isReceiveActive()) {
                    newList.add(ci);
                }
            }

            Collections.sort(newList);
            return newList;

        } else {
            return getEMTEnabledCountryList();
        }
    }

    private Collection getEMTEnabledCountryList() throws Exception {

        HashMap coutries = AgentConnectMaster.getCountryList();
        if (coutries == null) {
            refreshMasterFromAgentConnect();
            coutries = AgentConnectMaster.getCountryList();
        }

        List newList = new ArrayList();
        Iterator iter = coutries.values()
                                .iterator();

        while (iter.hasNext()) {
            MGOCountry ci = (MGOCountry) iter.next();
            if ((!ci.isEmtDisabled()) && ci.isReceiveActive()) {
                newList.add(ci);
            }
        }
        Collections.sort(newList);
        return newList;
    }

    public Collection getServiceOptionInfos(String countryCode)
            throws Exception {
        MGOCountry country = getCountry(countryCode);
        return country.getServiceOptionList();
    }

    private Collection getDeliveryOptionInfos(MGOCountry country,
            HashMap deliveryOptionInfos, HashMap currencies) throws Exception {
        boolean multiCurrencyCountry = country.isMultiCurrencyCountry();
        List newList = new ArrayList();
        String deliveryOptionName = null;
        int counter = 0;

        // Adding DeliveryOptions
        Iterator iter = country.getCountryCurrencyList()
                               .iterator();
        while (iter.hasNext()) {
            CountryCurrencyInfo cci = (CountryCurrencyInfo) iter.next();

            DeliveryOptionInfo doi = null;
            Iterator iiter = deliveryOptionInfos.values()
                                                .iterator();
            while (iiter.hasNext()) {
                doi = (DeliveryOptionInfo) iiter.next();
                if (cci.getDeliveryOption()
                       .equalsIgnoreCase(doi.getDeliveryOption()))
                    break;
                doi = null;
            }

            if (doi == null)
                continue;

            // Poland is a multicurrency country with DSS, so suppress it. Per
            // Business request
            if (multiCurrencyCountry && doi.isDssOption()
                    && "POLAND".equalsIgnoreCase(country.getCountryName())) {
                continue;
            }

            ServiceOption so = new ServiceOption();
            so.setSeqId(counter++);
            so.setId(doi.getDeliveryOptionID()
                        .intValue());
            so.setCode(cci.getDeliveryOption());
            so.setEmtServiceOptionName((String) deliveryOptionEMTNames.get(String.valueOf(so.getId())));
            so.setAgentManaged(cci.getAgentManaged());
            so.setCountryCode(cci.getCountryCode());
            so.setIndicativeRateAvailable(cci.isIndicativeRateAvailable());
            so.setLocalCurrency(cci.getLocalCurrency());
            so.setMgManaged(cci.getMgManaged());
            so.setReceiveAgentAbbr(cci.getReceiveAgentAbbreviation());
            so.setReceiveAgentID(cci.getReceiveAgentID());
            so.setReceiveCurrency(cci.getReceiveCurrency());
            so.setSendCurrency(cci.getBaseCurrency());

            // -------
            // ddu 4/3/09 - added to fix multiCurrencyCountry direct send issue
            // I did not remove the two setDirectedSend codes to minimize the
            // impacts
            // ddu 4/13/09 - Retrofit this production fix for the May-09 Release
            so.setDirectedSend(false);

            if (doi.isDssOption()) {
                so.setDirectedSend(true);
            }
            // -------

            if (multiCurrencyCountry) {
                CurrencyInfo currency = (CurrencyInfo) currencies.get(cci.getReceiveCurrency());
                deliveryOptionName = so.getEmtServiceOptionName() + " - "
                        + currency.getCurrencyName() + " Payout";
            } else {
                // Check if this is a FQDO
                // if (cci.getMgManaged() != null){
                if (doi.isDssOption()) {
                    so.setDirectedSend(true);
                    if (cci.getReceiveAgentAbbreviation() != null
                            && !"".equalsIgnoreCase(cci.getReceiveAgentAbbreviation()))
                        deliveryOptionName = so.getEmtServiceOptionName()
                                + " - " + cci.getReceiveAgentAbbreviation();
                    else
                        deliveryOptionName = so.getEmtServiceOptionName();
                } else {
                    so.setDirectedSend(false);
                    deliveryOptionName = so.getEmtServiceOptionName();
                }
            }

            so.setEmtServiceOptionName(deliveryOptionName);
            so.setName(deliveryOptionName);
            newList.add(so);
        }

        return newList;
    }

    public CurrencyInfo getCurrencyInfo(String currencyCode) throws Exception {
        CurrencyInfo currency = null;
        Iterator iter1 = AgentConnectMaster.getCurrencyList()
                                           .values()
                                           .iterator();

        while (iter1.hasNext()) {
            currency = (CurrencyInfo) iter1.next();
            if (currencyCode.equals(currency.getCurrencyCode())) {
                return currency;
            }
        }
        return currency;
    }

    public boolean isRRNumberFound(MGOAgentProfile mgoAgentProfile, String rrn)
            throws Exception {

        GetFQDOByCustomerReceiveNumberRequest req = new GetFQDOByCustomerReceiveNumberRequest();
        req.setMgCustomerReceiveNumber(rrn);
        try {
            AgentConnectAccessorDoddFrank acc = new AgentConnectAccessorDoddFrank(
                    mgoAgentProfile);
            GetFQDOByCustomerReceiveNumberResponse res = acc.getFQDOByCustomerReceiveNumber(
                    req, rrn);
            if (res.getFqdoInfo() == null || !res.getRegistrationStatusCode()
                                                 .equals("ACT"))
                return false;
        } catch (Exception e) {
            System.out.println("AC call return exception.");
            return false;
        }

        return true;
    }

    public GetFQDOByCustomerReceiveNumberResponse getRRNInfo(
            MGOAgentProfile mgoAgentProfile, String rrn) throws Exception {
        GetFQDOByCustomerReceiveNumberRequest req = new GetFQDOByCustomerReceiveNumberRequest();
        req.setMgCustomerReceiveNumber(rrn);
        GetFQDOByCustomerReceiveNumberResponse res = null;
        try {
            AgentConnectAccessorDoddFrank acc = new AgentConnectAccessorDoddFrank(
                    mgoAgentProfile);
            res = acc.getFQDOByCustomerReceiveNumber(req, rrn);
        } catch (Exception e) {
            System.out.println("AC call return exception.");
            return null;
        }
        return res;
    }

    public Collection getReceiveCountryRequirementsList(String receiveCountry)
            throws Exception {
        ArrayList requirementsList = AgentConnectMaster.getReceiveCountryRequirementsList();
        ArrayList newList = new ArrayList();
        Iterator iter = requirementsList.iterator();

        while (iter.hasNext()) {
            ReceiveCountryRequirementsInfo ri = (ReceiveCountryRequirementsInfo) iter.next();
            if (ri.getReceiveCountry()
                  .equals(receiveCountry)) {
                newList.add(ri);
            }
        }
        return newList;
    }

    public CountryCurrencyInfo getCountryCurrencyInfo(String countryCode,
            String currencyCode, String deliveryOptionCode) throws Exception {
        MGOCountry country = getCountry(countryCode);
        ArrayList ccl = (ArrayList) country.getCountryCurrencyList();
        Iterator i = ccl.iterator();

        while (i.hasNext()) {
            CountryCurrencyInfo cci = (CountryCurrencyInfo) i.next();
            if (cci.getReceiveCurrency()
                   .equalsIgnoreCase(currencyCode)
                    && cci.getDeliveryOption()
                          .equalsIgnoreCase(deliveryOptionCode)) {
                return cci;
            }
        }

        return null;
    }

    public void refreshExceptionCountry() throws Exception {
        // synchronize with Oracle DB
        HashMap cntryExceptions = new HashMap();
        Iterator itExp = getCntryExceptions().iterator();

        while (itExp.hasNext()) {
            CountryExceptionBean ceb = (CountryExceptionBean) itExp.next();
            cntryExceptions.put(ceb.getRcvIsoCntryCode(), ceb);
        }

        HashMap coutries = AgentConnectMaster.getCountryList();
        if (coutries == null) {
            refreshMasterFromAgentConnect();
            coutries = AgentConnectMaster.getCountryList();
        }
        Iterator iter = coutries.values()
                                .iterator();

        // Disabling the country if it is in the Exception List and isSndAllowed
        // is 'N'
        while (iter.hasNext()) {
            MGOCountry country = (MGOCountry) iter.next();
            CountryExceptionBean ceb = (CountryExceptionBean) cntryExceptions.get(country.getCountryCode());

            synchronized (country) {
                if (ceb != null) {
                    if (ceb.isSndAllowed()) {
                        country.setEmtDisabled(false);
                    } else {
                        country.setEmtDisabled(true);
                    }

                    country.setSndMaxAmtNbr(ceb.getSndMaxAmtNbr());
                    country.setSndThrldWarnAmtNbr(ceb.getSndThrldWarnAmtNbr());
                } else {
                    country.setEmtDisabled(false);
                    country.setSndMaxAmtNbr(0.0F);
                    country.setSndThrldWarnAmtNbr(0.0F);
                }
            }
        }
    }

    public MGOProductFieldInfo getFieldsByProduct(
            MGOAgentProfile mgoAgentProfile, String rcvCountryCode,
            String rcvCurrencyCode, String deliveryOption, String consumerId,
            BigDecimal amount) throws Exception {
        return getFieldsByProduct(mgoAgentProfile, rcvCountryCode,
                rcvCurrencyCode, null, deliveryOption, consumerId, amount);
    }

    public MGOProductFieldInfo getFieldsByProduct(
            MGOAgentProfile mgoAgentProfile, String rcvCountryCode,
            String rcvCurrencyCode, String rcvAgentID, String deliveryOption,
            String consumerId, BigDecimal amount) throws Exception {

        GetFieldsForProductResponse response = null;
        MGOProductFieldInfo productFieldInfo = new MGOProductFieldInfo();
        HashMap map = new HashMap();
        AgentConnectAccessorDoddFrank acc = new AgentConnectAccessorDoddFrank(
                mgoAgentProfile);
        response = acc.getFieldsForProduct(rcvCountryCode, rcvCurrencyCode,
                rcvAgentID, deliveryOption, consumerId, amount);
        ProductFieldInfo[] pfInfo = response.getProductFieldInfo();
        productFieldInfo.setDeliveryOption(deliveryOption);
        productFieldInfo.setReceiveCountry(rcvCountryCode);
        productFieldInfo.setReceiveCurrency(rcvCurrencyCode);
        productFieldInfo.setReceiveAgentID(rcvAgentID);

        for (int i = 0; i < pfInfo.length; i++) {
            if ("customerReceiveNumber".equalsIgnoreCase(pfInfo[i].getXmlTag())) {
                productFieldInfo.setRrnVisibility(pfInfo[i].getVisibility()
                                                           .getValue());
            }
            if ("receiverLastName2".equalsIgnoreCase(pfInfo[i].getXmlTag())) {
                productFieldInfo.setLastName2Required(pfInfo[i].getVisibility()
                                                               .getValue()
                                                               .equalsIgnoreCase(
                                                                       "REQ"));
            }
            if ("messageField1".equalsIgnoreCase(pfInfo[i].getXmlTag())) {
                productFieldInfo.setMessageField1Allowed(!pfInfo[i].getVisibility()
                                                                   .getValue()
                                                                   .equalsIgnoreCase(
                                                                           "NOT_ALL"));
            }

            map.put(pfInfo[i].getXmlTag(), pfInfo[i].getFieldMax());
        }

        productFieldInfo.setProductFieldInfo(map);

        return productFieldInfo;
    }
}
