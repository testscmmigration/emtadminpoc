package shared.mgo.services;

import java.util.ArrayList;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.exceptions.MGOProfileNotFoundException;



public interface MGOAgentProfileService {
	
	MGOAgentProfile getMGOProfile(String mgoAgentIdentifier, String mgoProductName) throws MGOProfileNotFoundException;

	MGOAgentProfile getMGOProfileByAgtId(String mgoAgentId, String mgoProductName) throws MGOProfileNotFoundException;
	
	ArrayList getAllMGOProfiles(String siteIdentifier) throws MGOProfileNotFoundException;
	
	void initAgentProfileCache() throws Exception;

}
