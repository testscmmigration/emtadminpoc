package shared.mgo.services;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.axis.client.Stub;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.exceptions.ACRuntimeException;

import com.moneygram.acclient.AcknowledgeRequest;
import com.moneygram.acclient.AcknowledgeResponse;
import com.moneygram.acclient.AgentConnectServiceLocator;
import com.moneygram.acclient.AgentConnect_PortType;
import com.moneygram.acclient.BillPaymentSendRequest;
import com.moneygram.acclient.BillPaymentSendResponse;
import com.moneygram.acclient.CodeTableRequest;
import com.moneygram.acclient.CodeTableResponse;
import com.moneygram.acclient.CountryCurrencyInfo;
import com.moneygram.acclient.CountryInfo;
import com.moneygram.acclient.CurrencyInfo;
import com.moneygram.acclient.DeliveryOptionInfo;
import com.moneygram.acclient.DetailLookupRequest;
import com.moneygram.acclient.DetailLookupResponse;
import com.moneygram.acclient.Error;
import com.moneygram.acclient.FQDOsForCountryRequest;
import com.moneygram.acclient.FQDOsForCountryResponse;
import com.moneygram.acclient.FeeInfo;
import com.moneygram.acclient.FeeLookupRequest;
import com.moneygram.acclient.FeeLookupResponse;
import com.moneygram.acclient.FormFreeBPSubmittalRequest;
import com.moneygram.acclient.FormFreeBPSubmittalResponse;
import com.moneygram.acclient.FormFreeSendSubmittalRequest;
import com.moneygram.acclient.FormFreeSendSubmittalResponse;
import com.moneygram.acclient.GetFQDOByCustomerReceiveNumberRequest;
import com.moneygram.acclient.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.acclient.GetFieldsForProductRequest;
import com.moneygram.acclient.GetFieldsForProductResponse;
import com.moneygram.acclient.MoneyGramSendRequest;
import com.moneygram.acclient.MoneyGramSendResponse;
import com.moneygram.acclient.ProductType;
import com.moneygram.acclient.ProductVariant;
import com.moneygram.acclient.ProfileRequest;
import com.moneygram.acclient.ProfileResponse;
import com.moneygram.acclient.ReceiveCountryRequirementsInfo;
import com.moneygram.acclient.ReceiveCountryRequirementsRequest;
import com.moneygram.acclient.ReceiveCountryRequirementsResponse;
import com.moneygram.acclient.Request;
import com.moneygram.acclient.SendReversalRequest;
import com.moneygram.acclient.SendReversalResponse;
import com.moneygram.acclient.StateProvinceInfo;
import com.moneygram.acclient.ThirdPartyType;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

/**
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class AbstractAgentConnectAccessor
{

	private static CodeTableResponse codeTableResponse;
	private static CodeTableResponse codeTableResponseForRefresh;
	private static FQDOsForCountryResponse fQDOsForCountryResponse;
	private static ReceiveCountryRequirementsResponse receiveCountryRequirementsResponse;	
	private static Hashtable countries;
	private static List sortedCountries;
	private AgentConnect_PortType agentConnect;
	private AgentConnectServiceLocator agentConnectService;

	// shared props
	final public static String API_VER_PROP_KEY = "AC4APIVersion";
	final public static String CLIENT_VER_PROP_KEY = "AC4ClientVersion";
	final public static String ADDRESS_PROP_KEY = "AC4Address";
	final public static String NAMESPACE_URI_PROP_KEY = "AC4NamespaceURI";
	final public static String LOCAL_PART_PROP_KEY = "AC4LocalPart";
	final public static String SERVICE_NAME_PROP_KEY = "AC4WSDDServiceName";
	final public static String RETRY_COUNT = "AC4RetryCount";
	final public static String RETRY_WAIT = "AC4RetryWaitTime";
	final public static String TIME_OUT = "A4CTimeout";

	private Integer profileId;
	private String agentId;
	private String agentSequence;
	private String token;	
	private static String apiVersion;
	private static String clientApiVersion;
	private static String address;
	private static String nameSpaceURI;
	private static String localPart;
	private static String serviceName;
	private static int retryCount = 3;
	private static long retryWaitTime = 15000;
	private static int timeout = 30000;

	//
	private static boolean initialized = false;
	//
	static Logger logger = LogFactory.getInstance().getLogger(AgentConnectAccessor.class);

	/**
	 * 
	 * @throws IllegalStateException
	 */
	protected AbstractAgentConnectAccessor(MGOAgentProfile mgoAgtProfile)
	{
		//if (!isInitialized())
			// new IllegalStateException("AgentConnect is not initialized.");

		profileId = mgoAgtProfile.getProfileId();
		agentId = mgoAgtProfile.getAgentId();
		agentSequence = mgoAgtProfile.getAgentSequence();
		token = mgoAgtProfile.getToken();
	}

	/**
	 * 
	 * @param props
	 * @throws ACRuntimeException
	 */
	protected AbstractAgentConnectAccessor(Properties props,MGOAgentProfile  mgoAgtProfile)
	{
		try
		{
			logger.info("Intializing AgentConnect ...");
			initOnce(props, mgoAgtProfile);
			setCodeTableResponse();
			setReceiveCountryRequirementsResponse();
			setInitialized(true);
			logger.info("Done Intializing AgentConnect");
		} catch (Exception e)
		{
			throw new ACRuntimeException(e.toString());
		}
	}

	public CountryCurrencyInfo[] getCountryCurrencyInfo()
	{
		return getCodeTableResponse().getCountryCurrencyInfo();
	}

	public CodeTableResponse getCodeTableResponse() {
		if (codeTableResponse == null) {
			try {
				setCodeTableResponse();
			} catch (Exception e) {
				System.err.println("Problems restoring code table response. Errors expected: " + e.getMessage());
				e.printStackTrace();
				logger.error("Problems restoring code table response. Errors expected", e);
			}
		}
		return codeTableResponse;
	}

	public void refreshCodeTablesFromAgentConnect() throws Exception
	{
	    try {
	        setCodeTableResponse();    
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error Refreshing Code Tables" + e.getMessage());
            throw e;
        }
	    
	}
	
	private synchronized void setCodeTableResponse() throws Exception {
		logger.info("Requesting AgentConnect for codeTable ...");

		CodeTableRequest req = new CodeTableRequest();
		req = (CodeTableRequest) setHeader(req);
		//FIXME - S26 - remove following logging
        logger.error("req.apiversion = " + req.getApiVersion() + ";clientsoftvers = " + req.getClientSoftwareVersion());
		int count = 0;
		while (count < retryCount) {
			try {
				logger.error("Refreshing/Building Code Tables from Agent Connect" + System.currentTimeMillis());
				refreshCodeTableResponse(req);
				logger.error("Done Refreshing/Building Code Tables from Agent Connect" + System.currentTimeMillis());
				break;
			} catch (Exception e) {
				wait(retryWaitTime);
				count++;
				logger.warn("getCodeTable()- Attempt : " + count + " Failed.", e);
				System.err.println("getCodeTable()- Attempt : " + count + " Failed: " + e.getMessage());
				e.printStackTrace();
				if (count == retryCount) {
					throw (e);
				}
				continue;
			}
		}
	}

	private void refreshCodeTableResponse(CodeTableRequest req) throws RemoteException, Error,
			Exception {
		codeTableResponseForRefresh = getAgentConnect().codeTable(req);
		codeTableResponse = codeTableResponseForRefresh;
	}
	
	public AcknowledgeResponse acknowledge(String refNbr, ProductType p,
            Calendar timeStamp) throws Exception {
        logger.info("Requesting AgentConnect for acknowledge ... ");
        AcknowledgeRequest req = new AcknowledgeRequest();
        req = (AcknowledgeRequest) setHeader(req);
        req.setReferenceNumber(refNbr);
        req.setProductType(p);
        req.setTimeStamp(timeStamp);
        AcknowledgeResponse resp = getAgentConnect().acknowledge(req);
        logSOAPMessages();
        return resp;
    }
		
	public FQDOsForCountryResponse getFQDOsForCountryResponse(String countryCode) throws Exception
	{
		FQDOsForCountryRequest req = new FQDOsForCountryRequest();
		req = (FQDOsForCountryRequest) setHeader(req);
		req.setReceiveCountry(countryCode);
		
		return getAgentConnect().getFQDOsForCountry(req);
	}
	
	public DeliveryOptionInfo [] getDeliveryOptionInfo()
	{
		return getCodeTableResponse().getDeliveryOptionInfo();
	}
	
	public CountryInfo[] getCountryInfo()
	{
		return getCodeTableResponse().getCountryInfo();
	}
	public CurrencyInfo[] getCurrencyInfo()
	{
		return getCodeTableResponse().getCurrencyInfo();
	}

	public ReceiveCountryRequirementsResponse getReceiveCountryRequirementsResponse()
	{
		return receiveCountryRequirementsResponse;
	}

	
	public StateProvinceInfo[] getStateProvinceInfo()
	{
		return getCodeTableResponse().getStateProvinceInfo();
	}

	private boolean isInitialized()
	{
		return initialized;
	}

	private synchronized void setInitialized(boolean init)
	{
		initialized = init;
	}

	private synchronized void initOnce(
		Properties props,
		MGOAgentProfile mgoAgtProfile)
	{
		//FIXME: S26 - logging remove
		logger.error("S26:initOnce called...");
		//***********************************
		//  unique parameters for each agent
		profileId = mgoAgtProfile.getProfileId();
		agentId = mgoAgtProfile.getAgentId();
		agentSequence = mgoAgtProfile.getAgentSequence();
		token = mgoAgtProfile.getToken();
		//  shared parameters for all agents
		apiVersion = props.getProperty(API_VER_PROP_KEY, "1");
		clientApiVersion = props.getProperty(CLIENT_VER_PROP_KEY, "1");
		address = props.getProperty(ADDRESS_PROP_KEY);
		if (address==null)
		{ Exception e = new Exception("test");
		  e.printStackTrace();
		}
		nameSpaceURI = props.getProperty(NAMESPACE_URI_PROP_KEY);
		localPart = props.getProperty(LOCAL_PART_PROP_KEY);
		serviceName = props.getProperty(SERVICE_NAME_PROP_KEY);
		retryCount = (int) toLong(props.getProperty(RETRY_COUNT), retryCount);
		retryWaitTime = toLong(props.getProperty(RETRY_WAIT), retryWaitTime);
		timeout = (int) toLong(props.getProperty(TIME_OUT), timeout);
	}

	private long toLong(String val, long defVal)
	{
		try
		{
			return Long.parseLong(val);
		} catch (Exception e1)
		{
		}

		return defVal;
	}


	protected Request setHeader(Request req)
	{
		req.setUnitProfileID(profileId);
		req.setAgentID(agentId);
		req.setAgentSequence(agentSequence);
		req.setApiVersion(apiVersion);
		req.setClientSoftwareVersion(clientApiVersion);
		req.setToken(token);
		req.setTimeStamp(Calendar.getInstance());
		//FIXME: S26 - remove logging and setting
		if (req.getApiVersion() == null) {
			req.setApiVersion("1");
			System.out.println("***S26 - AbstractAgentConnectAccessor.setHeader - apiVersion is null***");
		}
		if (req.getClientSoftwareVersion() == null) {
			req.setClientSoftwareVersion("1");
			System.out.println("***S26 - AbstractAgentConnectAccessor.setHeader - clientSoftwareVersion is null***");
		}
		//*************************************************
		return req;
	}


	public AgentConnect_PortType getAgentConnect() throws Exception
	{
		if (agentConnect == null)
		{
			agentConnectService = new AgentConnectServiceLocator();
			agentConnectService.setAgentConnectEndpointAddress(address);
			agentConnectService.setAgentConnectWSDDServiceName(serviceName);
			agentConnect = agentConnectService.getAgentConnect();
			((Stub) agentConnect).setTimeout(timeout);
		}
		return agentConnect;
	}

	/**
	 * @return
	 */
	public String getAgentId()
	{
		return agentId;
	}
	
	private synchronized void setReceiveCountryRequirementsResponse() throws Exception
	{

		logger.info("Requesting AgentConnect for Code Table ... ");
		ReceiveCountryRequirementsRequest req = new ReceiveCountryRequirementsRequest();
		req = (ReceiveCountryRequirementsRequest) setHeader(req);
		receiveCountryRequirementsResponse = getAgentConnect().receiveCountryRequirements(req);
	}
	
	protected void logSOAPMessages()
	{
		try
		{
			logger.debug(
				"SOAP Request : \n"
					+ agentConnectService
						.getCall()
						.getMessageContext()
						.getRequestMessage()
						.getSOAPPartAsString());

			logger.debug(
				"SOAP Response : \n"
					+ agentConnectService
						.getCall()
						.getMessageContext()
						.getResponseMessage()
						.getSOAPPartAsString());
		} catch (Throwable t)
		{
			logger.warn("logSOAPMessages Failed - " + t.getMessage());
		}
	}

	public DetailLookupResponse detailLookup(String refNbr) throws Exception
	{
		logger.info("Requesting AgentConnect for detailLookup ... ");
		DetailLookupRequest req = new DetailLookupRequest();
		req = (DetailLookupRequest) setHeader(req);
		req.setReferenceNumber(refNbr);
		DetailLookupResponse resp = getAgentConnect().detailLookup(req);
		//
		logSOAPMessages();
		return resp;
	}

	public SendReversalResponse sendReversal(
		String refNbr,
		BigDecimal sndAmt,
		BigDecimal feeAmt)
		throws Exception
	{
		logger.info("Requesting AgentConnect for detailLookup ... ");
		SendReversalRequest req = new SendReversalRequest();
		req = (SendReversalRequest) setHeader(req);
		req.setReferenceNumber(refNbr);
		// TODO: this is hard-coded as a "C"ancel for now.  When Refunds are implemented
		// this will need to be a parameter.  This is a hard-code for an AHD fix.  Also 
		// hard-coded is "Y" for refund the fee, which is the only allowed value for Cancels
		req.setReversalType("C");
		req.setFeeRefund("Y");
		req.setSendAmount(sndAmt);
		req.setFeeAmount(feeAmt);
		SendReversalResponse resp = getAgentConnect().sendReversal(req);
		//
		logSOAPMessages();
		return resp;
	}

	private String truncate(String str, int length)
	{
		if (str == null)
			return str;
		if (str.length() > length)
			return str.substring(0, length);
		else
			return str;
	}

	public static CountryInfo getCountry(String cid)
	{
		if ((countries == null) || (countries.get(cid) == null))
			return null;
		return (CountryInfo) countries.get(cid);
	}	
	
	public ReceiveCountryRequirementsInfo[] getRequirementsInfo() throws Exception{

		ArrayList list = new ArrayList();
		
		ReceiveCountryRequirementsRequest req = new ReceiveCountryRequirementsRequest();
		req = (ReceiveCountryRequirementsRequest) setHeader(req);
		
		ReceiveCountryRequirementsResponse resp = getAgentConnect().receiveCountryRequirements(req);
		ReceiveCountryRequirementsInfo[] rcri = resp.getReceiveCountryRequirementsInfo();
		return rcri;
	}


	public FeeInfo[] feeLookupAllOptions(BigDecimal sendAmount,String recCountry,ProductType productType,
			String agencyId, ProductVariant productVariant)
			throws Exception
	{
			logger.info("Requesting AgentConnect for feeLookup ... ");
			FeeLookupRequest req = new FeeLookupRequest();
			req = (FeeLookupRequest) setHeader(req);
			req.setReceiveCode(agencyId);
			//req.setDeliveryOption(deliveryOption);
			req.setProductType(productType);
			req.setProductVariant(productVariant);
			//req.setReceiveCurrency(recCurrency);
			req.setAmountExcludingFee(sendAmount);
			req.setReceiveCountry(recCountry);
			req.setAllOptions(new Boolean(true));
			FeeLookupResponse resp = getAgentConnect().feeLookup(req);
			logSOAPMessages();	
			return resp.getFeeInfo();
	}


	
	public FeeLookupResponse feeLookup(
			BigDecimal amount,
			String recCountry,
			ProductType type,
			String recCurrency,
			String deliveryOption,
			String rewardsNumber,
			String agencyId)
			throws Exception
	{
			logger.info("Requesting AgentConnect for feeLookup ... ");
			FeeLookupRequest req = new FeeLookupRequest();
			req = (FeeLookupRequest) setHeader(req);
			//req.setReceiveAgentID(agencyId);
			req.setReceiveCode(agencyId);
			if ((rewardsNumber!=null) && (!rewardsNumber.equals("")))
				req.setFreqCustCardNumber(rewardsNumber);
			req.setDeliveryOption(deliveryOption);
			req.setProductType(type);
			req.setReceiveCurrency(recCurrency);
			req.setAmountExcludingFee(amount);
			req.setReceiveCountry(recCountry);
			FeeLookupResponse resp = getAgentConnect().feeLookup(req);
			logSOAPMessages();
			return resp;
	}

	
	public ProfileResponse getProfile()
			throws Exception
	{
			logger.info("Requesting AgentConnect for profile ... ");
			ProfileRequest req = new ProfileRequest();
			req = (ProfileRequest) setHeader(req);
			ProfileResponse resp = getAgentConnect().profile(req);
			logSOAPMessages();
			return resp;
	}

	
	public FormFreeBPSubmittalResponse formFreeBPSubmittal(FormFreeBPSubmittalRequest req)
	throws Exception
	{	
		logger.info("Requesting AgentConnect for formFreeBPSubmittal ... ");
		req = (FormFreeBPSubmittalRequest) setHeader(req);
		FormFreeBPSubmittalResponse resp =
			getAgentConnect().formFreeBPSubmittal(req);
		logSOAPMessages();
		return resp;
	}	

	public FormFreeSendSubmittalResponse formFreeMGSubmittal(FormFreeSendSubmittalRequest req)
	throws Exception
	{
		logger.info("Requesting AgentConnect for formFreeSendSubmittal ... ");
		req = (FormFreeSendSubmittalRequest) setHeader(req);
		FormFreeSendSubmittalResponse resp =
			getAgentConnect().formFreeSendSubmittal(req);
		return resp;
	}
	
	public GetFQDOByCustomerReceiveNumberResponse getFQDOByCustomerReceiveNumber(GetFQDOByCustomerReceiveNumberRequest req, String rrn)
	throws Exception
	{
		logger.info("Requesting AgentConnect for getFQDOByCustomerReceiveNumber ... ");
		req = (GetFQDOByCustomerReceiveNumberRequest) setHeader(req);
		req.setMgCustomerReceiveNumber(rrn);
		GetFQDOByCustomerReceiveNumberResponse resp = getAgentConnect().getFQDOByCustomerReceiveNumber(req);
		logSOAPMessages();
		return resp;
	}	
		
	public BillPaymentSendResponse expressPaySend(BillPaymentSendRequest req)
            throws Exception {
        logger.info("Requesting AgentConnect for EP Send ... "
                + System.currentTimeMillis());
        req = (BillPaymentSendRequest) setHeader(req);
        BillPaymentSendResponse resp = new BillPaymentSendResponse();
        resp.setThrowable(null);
        try {
            resp = getAgentConnect().billPaymentSend(req);
            logSOAPMessages();
            acknowledge(resp.getReferenceNumber(), ProductType.BP, req.getTimeStamp());
        } catch (Exception e) {
            logSOAPMessages();
            logger.info("Error Requesting AgentConnect for EP Send ... "
                    + System.currentTimeMillis() + e.getLocalizedMessage());
            resp.setThrowable(e);
        }
        logger.info("Done Requesting AgentConnect for EP Send ... "
                + System.currentTimeMillis());
        return resp;
    }

	public MoneyGramSendResponse moneyGramSend(MoneyGramSendRequest req)
            throws Exception {
        logger.info("Requesting AgentConnect for MG Send ... "
                + System.currentTimeMillis());
        req = (MoneyGramSendRequest) setHeader(req);
        MoneyGramSendResponse resp = new MoneyGramSendResponse();
        resp.setThrowable(null);
        try {
            resp = getAgentConnect().moneyGramSend(req);
            logSOAPMessages();
            acknowledge(resp.getReferenceNumber(), ProductType.SEND, req
                    .getTimeStamp());
        } catch (Exception e) {
            logger.info("Error Requesting AgentConnect for MG Send ... "
                    + System.currentTimeMillis() + e.getLocalizedMessage());
            resp.setThrowable(e);
            logSOAPMessages();
        }
        logger.info("Done Requesting AgentConnect for MG Send ... "
                + System.currentTimeMillis());
        return resp;
    }

	public GetFieldsForProductResponse getFieldsForProduct(String rcvCountryCode,
		String rcvCurrencyCode,	String rcvAgentID,String deliveryOption, String consumerId, BigDecimal amount) throws Exception {
		
		logger.info("Requesting AgentConnect for getFieldsForProduct ... ");
		GetFieldsForProductRequest req = new GetFieldsForProductRequest();
		req = (GetFieldsForProductRequest) setHeader(req);
		req.setProductType(ProductType.SEND);
		req.setDeliveryOption(deliveryOption);
		req.setReceiveCountry(rcvCountryCode);
		req.setReceiveCurrency(rcvCurrencyCode);
		req.setReceiveAgentID(rcvAgentID);
		req.setThirdPartyType(ThirdPartyType.NONE);
		req.setConsumerId(consumerId);
		req.setAmount(amount);
		GetFieldsForProductResponse resp = getAgentConnect().getFieldsForProduct(req);
		logSOAPMessages();
		return resp;
	}
		
}	
