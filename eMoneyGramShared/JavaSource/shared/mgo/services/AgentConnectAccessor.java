package shared.mgo.services;

import java.util.Properties;

import shared.mgo.dto.MGOAgentProfile;



public class AgentConnectAccessor extends AbstractAgentConnectAccessor {
	public final static String PROFILE_ID_PROP_KEY = "AC4ProfileId";
	public final static String AGENT_ID_PROP_KEY = "AC4AgentId";
	public final static String AGENT_SEQ_PROP_KEY = "AC4AgentSequence";
	public final static String TOKEN_PROP_KEY = "AC4token";

	// TODO: REMOVE THIS and do it right!
	//private static MGOAgentProfile mgoAgtProfile = new MGOAgentProfile();
	
	static {
	//	mgoAgtProfile.setAgentId("43537899");
		//mgoAgtProfile.setAgentSequence("01");
		//mgoAgtProfile.setProfileId(new Integer(100416));
		//mgoAgtProfile.setToken("TEST");
		
	}

	public AgentConnectAccessor(MGOAgentProfile mgoAgentProfile) {
		super(mgoAgentProfile);
	}

	/*public AgentConnectAccessor() {
		super(mgoAgtProfile);
	}*/

	public AgentConnectAccessor(Properties props,MGOAgentProfile mgoAgentProfile) {
		super(props, mgoAgentProfile);
	}	

}
