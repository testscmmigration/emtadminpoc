package shared.mgo.services;

import java.util.Properties;

import shared.mgo.dto.MGOAgentProfile;



public class AgentConnectAccessorDoddFrank extends AbstractAgentConnectAccessorDoddFrank {
	public final static String PROFILE_ID_PROP_KEY = "ACDoddFrankProfileId";
	public final static String AGENT_ID_PROP_KEY = "ACDoddFrankAgentId";
	public final static String AGENT_SEQ_PROP_KEY = "ACDoddFrankAgentSequence";
	public final static String TOKEN_PROP_KEY = "ACDoddFranktoken";
	
	public AgentConnectAccessorDoddFrank(MGOAgentProfile mgoAgentProfile) {
		super(mgoAgentProfile);
	}

	public AgentConnectAccessorDoddFrank(Properties props,MGOAgentProfile mgoAgentProfile) {
		super(props, mgoAgentProfile);
	}	

}
