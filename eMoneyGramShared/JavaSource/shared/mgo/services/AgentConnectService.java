/*
 * Created on Sep 1, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package shared.mgo.services;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOCountry;
import shared.mgo.dto.MGOProductFieldInfo;

import com.moneygram.acclient.CountryCurrencyInfo;
import com.moneygram.acclient.CurrencyInfo;
import com.moneygram.acclient.DeliveryOptionInfo;
import com.moneygram.acclient.FeeInfo;
import com.moneygram.acclient.FeeLookupResponse;
import com.moneygram.acclient.FormFreeBPSubmittalRequest;
import com.moneygram.acclient.FormFreeBPSubmittalResponse;
import com.moneygram.acclient.FormFreeSendSubmittalRequest;
import com.moneygram.acclient.FormFreeSendSubmittalResponse;
import com.moneygram.acclient.GetFQDOByCustomerReceiveNumberResponse;
import com.moneygram.acclient.ProductType;
import com.moneygram.acclient.ProductVariant;



/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface AgentConnectService {
	
	MGOCountry getCountry(String countryCode) throws Exception;
	
	void refreshMasterFromAgentConnect() throws Exception;
	
	void refreshExceptionCountry() throws Exception;
	
	Collection getStateProvinceInfo(String countryCode) throws Exception;
	
	Collection getFQDOInfo(String countryCode) throws Exception;

	Collection getCountryCurrencyInfo(String countryCode) throws Exception;

	Map getDeliveryOptionInfo() throws Exception;
	
	com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo getDeliveryOptionInfo(int deliveryOptionId) throws Exception;

	DeliveryOptionInfo getDeliveryOptionInfo(String deliveryOptionCode) throws Exception;
	
	List getCntryExceptions() throws Exception;	
	
	Collection getActiveCountryList() throws Exception;
	
	FeeLookupResponse epFeeLookup(MGOAgentProfile mgoAgentProfile ,String agencyId, String amount) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry,String rewardsNumber) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry, String currency,String rewardsNumber) throws Exception;
	
	FeeLookupResponse mgFeeLookup(MGOAgentProfile mgoAgentProfile ,BigDecimal amount, String recCountry, String recCurrency, String deliveryOption,String rewardsNumber) throws Exception;
			
	Collection getFilteredCountryList(String state) throws Exception;
	
	Collection getEconomyCountryList(String originationStateCode) throws Exception;
	
	Collection getSameDayCountryList(String originationStateCode) throws Exception;
	
	//AgentConnectAccessor getAgentConnectAccessor() throws Exception;
	
	FormFreeBPSubmittalResponse formFreeBPSubmittal(MGOAgentProfile mgoAgentProfile, FormFreeBPSubmittalRequest req)	throws Exception;

	FormFreeSendSubmittalResponse formFreeMGSubmittal(MGOAgentProfile mgoAgentProfile,FormFreeSendSubmittalRequest req)	throws Exception;
		
	Map getCountries() throws Exception;
	
	Collection getReceiveCurrencyInfo(String countryCode) throws Exception;
		
	Collection getServiceOptionInfos(String countryCode) throws Exception;

	CurrencyInfo getCurrencyInfo(String currencyCode) throws Exception;
	
	CountryCurrencyInfo getCountryCurrencyInfo(String countryCode, String currencyCode, String deliveryOptionCode) throws Exception;
	
	Collection getReceiveCountryRequirementsList(String receiveCountry) throws Exception;
	
	boolean isRRNumberFound(MGOAgentProfile mgoAgentProfile,String rrn) throws Exception; 
	
	GetFQDOByCustomerReceiveNumberResponse getRRNInfo(MGOAgentProfile mgoAgentProfile,String rrn) throws Exception;
	
	MGOProductFieldInfo getFieldsByProduct(MGOAgentProfile mgoAgentProfile ,String rcvCountryCode, String rcvCurrencyCode,
		   String rcvAgentID, String deliveryOption, String consumerId,BigDecimal amount) throws Exception;
	
	MGOProductFieldInfo getFieldsByProduct(MGOAgentProfile mgoAgentProfile,String rcvCountryCode, String rcvCurrencyCode,String deliveryOption,String consumerId,BigDecimal amount) throws Exception;
	
}
