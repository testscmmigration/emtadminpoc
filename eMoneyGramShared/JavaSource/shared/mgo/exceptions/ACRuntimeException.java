/*
 * Created on Jan 19, 2005
 *
 */
package shared.mgo.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * 
 *  This is the common superclass for all unchecked exceptions. This
 *  class and its subclasses support the chained exception facility that allows
 *  a root cause Throwable to be wrapped by this class or one of its 
 *  descendants.
 */
public class ACRuntimeException extends RuntimeException
{
	protected Throwable rootCause = null;

	public ACRuntimeException()
	{
		super();
	}

	public ACRuntimeException(String s)
	{
		super(s);
	}

	public ACRuntimeException(Throwable rootCause)
	{
		this.rootCause = rootCause;
	}

	public void setRootCause(Throwable anException)
	{
		rootCause = anException;
	}

	public Throwable getRootCause()
	{
		return rootCause;
	}

	public void printStackTrace()
	{
		printStackTrace(System.err);
	}

	public void printStackTrace(PrintStream outStream)
	{
		printStackTrace(new PrintWriter(outStream));
	}

	public void printStackTrace(PrintWriter writer)
	{
		super.printStackTrace(writer);

		if (getRootCause() != null)
		{
			getRootCause().printStackTrace(writer);
		}
		writer.flush();
	}
}
