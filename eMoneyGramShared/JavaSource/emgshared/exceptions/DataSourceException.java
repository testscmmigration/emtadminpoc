/*
 * Created on Mar 18, 2004
 *
 */
package emgshared.exceptions;

/**
 * @author A131
 *
 */
public class DataSourceException extends EMGBaseException
{
	public DataSourceException()
	{
		super();
	}

	public DataSourceException(String s)
	{
		super(s);
	}

	public DataSourceException(Throwable rootCause)
	{
		super(rootCause);
	}

	public DataSourceException(String msg, Throwable rootCause) {
		super(msg, rootCause);
	}
}
