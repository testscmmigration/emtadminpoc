package emgshared.exceptions;

public class ParseDateException extends EMGBaseException {

	public ParseDateException() {
		super();
	}

	public ParseDateException(String s) {
		super(s);
	}
}
