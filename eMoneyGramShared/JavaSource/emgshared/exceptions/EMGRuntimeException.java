/*
 * Created on Jan 19, 2005
 *
 */
package emgshared.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * @author A131
 * 
 *  This is the common superclass for all unchecked exceptions. This
 *  class and its subclasses support the chained exception facility that allows
 *  a root cause Throwable to be wrapped by this class or one of its 
 *  descendants.
 */
public class EMGRuntimeException extends RuntimeException
{
	protected Throwable rootCause = null;

	public EMGRuntimeException()
	{
		super();
	}

	public EMGRuntimeException(String s)
	{
		super(s);
	}

	public EMGRuntimeException(String s,Throwable rootCause)
	{
		super(s);
		this.rootCause = rootCause;
	}
	
	public EMGRuntimeException(Throwable rootCause)
	{
		this.rootCause = rootCause;
	}

	public void setRootCause(Throwable anException)
	{
		rootCause = anException;
	}

	public Throwable getRootCause()
	{
		return rootCause;
	}

	public void printStackTrace()
	{
		printStackTrace(System.err);
	}

	public void printStackTrace(PrintStream outStream)
	{
		printStackTrace(new PrintWriter(outStream));
	}

	public void printStackTrace(PrintWriter writer)
	{
		super.printStackTrace(writer);

		if (getRootCause() != null)
		{
			getRootCause().printStackTrace(writer);
		}
		writer.flush();
	}
}
