/*
 * Created on Mar 18, 2004
 *
 */
package emgshared.exceptions;


/**
 * @author T349
 *
 */
public class LoginAccountException extends EMGBaseException
{
	public LoginAccountException()
	{
		super();
	}

	public LoginAccountException(String s)
	{
		super(s);
	}

	public LoginAccountException(Throwable rootCause)
	{
		super(rootCause);
	}
}
