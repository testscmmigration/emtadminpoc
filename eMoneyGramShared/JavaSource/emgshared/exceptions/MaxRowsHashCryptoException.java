package emgshared.exceptions;

public class MaxRowsHashCryptoException extends EMGBaseException {

	public MaxRowsHashCryptoException() {
		super();
	}

	public MaxRowsHashCryptoException(String s) {
		super(s);
	}

	public MaxRowsHashCryptoException(Exception e) {
		super(e);
	}

}
