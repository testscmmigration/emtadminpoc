/*
 * Created on June 27,2005
 *
 */
package emgshared.exceptions;

/**
 * @author G.R.Svenddal
 * This exception is thrown by the  code that directly supports 
 * Verified By Visa. Currently ( 6/05 ) that would be 
 * emgshared.services.CyberSourceVerifiedByVisaService and 
 * and emg.support.VisaIssuerPostbackAction.
 *   
 * The SimpleSoapClient, having no business logic,  should only throw 
 * infrastructure exceptions. The SimpleSoapServlet ( in emgadm ) should only
 * throw IOExceptions ( even ServletExceptions are intrinsically business logic
 * exceptions ) The SimpleSoapServlet returns pages that are just XML to be
 * parsed by the client, so unless there's an IO problem it should always 
 * return some kind of valid bean. If there's a business logic problem the 
 * bean should say so.  
 *
 */
public class CyberSourceVBVException extends EMGBaseException
{
	public CyberSourceVBVException()
	{
		super();
	}

	public CyberSourceVBVException(String s)
	{
		super(s);
	}

	public CyberSourceVBVException(Throwable rootCause)
	{
		super(rootCause);
	}
}
