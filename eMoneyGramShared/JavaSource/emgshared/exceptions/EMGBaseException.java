/**
 * @author T349
 * @author A131
 *
 */
package emgshared.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 *  This is the common superclass for all application exceptions. This
 *  class and its subclasses support the chained exception facility that allows
 *  a root cause Throwable to be wrapped by this class or one of its 
 *  descendants.
 */
public abstract class EMGBaseException extends Exception
{
	protected Throwable rootCause = null;

	protected EMGBaseException()
	{
		super();
	}

	protected EMGBaseException(String s)
	{
		super(s);
	}

	protected EMGBaseException(Throwable rootCause)
	{
		this.rootCause = rootCause;
	}

	public EMGBaseException(String msg, Throwable rootCause) {
		super(msg, rootCause);
	}

	public void setRootCause(Throwable anException)
	{
		rootCause = anException;
	}

	public Throwable getRootCause()
	{
		return rootCause;
	}

	public void printStackTrace()
	{
		printStackTrace(System.err);
	}

	public void printStackTrace(PrintStream outStream)
	{
		printStackTrace(new PrintWriter(outStream));
	}

	public void printStackTrace(PrintWriter writer)
	{
		super.printStackTrace(writer);

		if (getRootCause() != null)
		{
			getRootCause().printStackTrace(writer);
		}
		writer.flush();
	}
}
