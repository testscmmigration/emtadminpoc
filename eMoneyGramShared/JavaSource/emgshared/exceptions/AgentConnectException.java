package emgshared.exceptions;


public class AgentConnectException extends EMGBaseException {

	public AgentConnectException() {
		super();
	}

	public AgentConnectException(String message) {
		super(message);
	}

	public AgentConnectException(Throwable arg0) {
		super(arg0);
	}
}
