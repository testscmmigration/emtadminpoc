/*
 * Created on Mar 15, 2005
 *
 */
package emgshared.exceptions;

/**
 * @author A135
 *
 */
public class SameStatusException extends EMGBaseException {
	public SameStatusException()
	{
		super();
	}

	public SameStatusException(String s)
	{
		super(s);
	}

	public SameStatusException(Throwable rootCause)
	{
		super(rootCause);
	}
}
