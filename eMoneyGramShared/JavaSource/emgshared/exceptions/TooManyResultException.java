/*
 * Created on Feb 4, 2005
 *
 */
package emgshared.exceptions;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TooManyResultException extends EMGBaseException{

	public TooManyResultException() {
		super();
	}

	/**
	 * @param s
	 */
	public TooManyResultException(String s) {
		super(s);
	}

}
