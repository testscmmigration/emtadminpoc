/*
 * Created on Jan 19, 2005
 *
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.ParseDateException;
import emgshared.model.EMTTransactionType;
import emgshared.model.TransactionComment;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;

/**
 * @author A131
 *
 */
public class TransactionDAO extends AbstractOracleDAO
{
	public int insertComment(TransactionComment comment, String callerLoginId)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_comments.prc_insrt_emg_tran_comment";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, comment.getTranId());
			cs.setString(++paramIndex, comment.getReasonCode());
			cs.setString(++paramIndex, comment.getText());

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return newId;
	}

	public Map getHolidays(String userId) throws DataSourceException
	{
		Map holidayMap = new HashMap();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormatter df = new DateFormatter("yyyyMMdd", true);

		String storedProcName = "pkg_em_transactions.prc_get_frb_holiday_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				String strDate = df.format(rs.getDate("hldy_date"));
				java.util.Date outDate = df.parse(strDate);
				holidayMap.put(strDate, outDate);
			}
		} catch (SQLException e1)
		{
			throw new DataSourceException(e1);
		} catch (ParseDateException e2)
		{
			throw new DataSourceException(e2);
		} finally
		{
			close(rs);
			close(cs);
		}

		return holidayMap;
	}

	public Collection getTransactionTypes(int partnerSiteCode)
		throws DataSourceException, SQLException
	{
		Collection tranTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_emg_tran_type_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, partnerSiteCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				EMTTransactionType type = new EMTTransactionType();
				String partnerSiteId = rs.getString("BSNS_CODE");
				type.setPartnerSiteId(partnerSiteId);
				type.setEmgTranTypeCode(rs.getString("EMG_TRAN_TYPE_CODE"));
				type.setEmgTranTypeDesc(rs.getString("EMG_TRAN_TYPE_DESC"));
				type.setEmgTranTypeCodeLabel(type.getEmgTranTypeCode());
				type.setEmgTranTypeDescLabel(type.getEmgTranTypeDesc());
				type.setScoreFlag(rs.getString("SCORE_FLAG"));
				type.setAutoApproveFlag(rs.getString("AUTO_APRV_FLAG"));
				tranTypes.add(type);
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return tranTypes;
	}
}
