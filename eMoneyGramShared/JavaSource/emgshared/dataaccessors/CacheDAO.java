package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.SimpleCache;

public class CacheDAO extends emgshared.dataaccessors.AbstractOracleDAO
{

	public SimpleCache getCache(String userID, String cacheName)
		throws DataSourceException, SQLException, TooManyResultException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		SimpleCache sc = new SimpleCache();

		String storedProcName = "pkg_em_status_and_type.prc_get_cache_mgmt_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, cacheName);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			if (rs.next())
			{
				sc = new SimpleCache();
				sc.setName(rs.getString("CACHE_NAME"));
				sc.setTimeStamp(rs.getTimestamp("SRC_UPDATE_DATE"));
			}

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return sc;

	}

	public void updateCache(String cacheName) throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_emg_type_stat_maint.prc_iu_cache_mgmt";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, cacheName);
			cs.execute();

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

}
