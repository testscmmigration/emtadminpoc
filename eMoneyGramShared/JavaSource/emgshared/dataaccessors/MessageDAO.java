package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AdminMessage;
import emgshared.model.AdminMessageType;
import emgshared.model.MessageRecipientBean;
import emgshared.model.UserMessages;

public class MessageDAO extends emgshared.dataaccessors.AbstractOracleDAO
{

	public List getAdminMessages(String userId, AdminMessage criteria)
		throws DataSourceException
	{
		List list = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_admin_message_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (criteria.getAdminMsgId() == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, criteria.getAdminMsgIdNbr());
			}

			cs.setTimestamp(
				++paramIndex,
				criteria.getMsgBegDate() == null
					? null
					: new Timestamp(criteria.getMsgBegDate().getTime()));

			cs.setTimestamp(
				++paramIndex,
				criteria.getMsgThruDate() == null
					? null
					: new Timestamp(criteria.getMsgThruDate().getTime()));

			if (criteria.getAdminMsgTypeCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, criteria.getAdminMsgTypeCode());
			}

			if (criteria.getMsgPrtyCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, criteria.getMsgPrtyCode());
			}

			if (criteria.getMsgText() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, criteria.getMsgText());
			}

			if (criteria.getCreateUserid() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, criteria.getCreateUserid());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				AdminMessage am = new AdminMessage();
				am.setAdminMsgId(new Integer(rs.getInt("admin_msg_id")));
				am.setMsgText(rs.getString("msg_text"));
				am.setMsgBegDate(rs.getTimestamp("msg_beg_date"));
				am.setMsgThruDate(rs.getTimestamp("msg_thru_date"));
				am.setAdminMsgTypeCode(rs.getString("admin_msg_type_code"));
				am.setAdminMsgTypeDesc(rs.getString("admin_msg_type_desc"));
				am.setMsgPrtyCode(rs.getString("msg_prty_code"));
				am.setCreateDate(rs.getTimestamp("create_date"));
				am.setCreateUserid(rs.getString("create_userid"));
				am.setLastUpdateDate(rs.getTimestamp("last_update_date"));
				am.setLastUpdateUserid(rs.getString("last_update_userid"));
				list.add(am);

			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return list;
	}

	public Map getActiveUserMessages(String userId) throws DataSourceException
	{
		Map msgMap = new HashMap();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_active_admin_msgs_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			UserMessages um = null;
			String saveId = null;
			String uid = null;
			Date rcvDate = null;
			int msgId = 0;

			while (rs.next())
			{
				uid = rs.getString("msg_recipnt_userid");
				msgId = rs.getInt("admin_msg_id");
				rcvDate = rs.getTimestamp("msg_rcv_date");

				if (um != null && !uid.equals(saveId))
				{
					msgMap.put(um.getUserId(), um);
					um = null;
				}

				if (um == null)
				{
					um = new UserMessages();
					um.setUserId(uid);
					saveId = uid;
				}

				um.addMsg(msgId, rcvDate);
			}

			if (um != null)
			{
				msgMap.put(um.getUserId(), um);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return msgMap;
	}

	public List getAdminMessageTypes(String userId) throws DataSourceException
	{
		List list = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_admin_message_type_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				list.add(
					new AdminMessageType(
						rs.getString("admin_msg_type_code"),
						rs.getString("admin_msg_type_desc")));
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return list;
	}

	public AdminMessage insertAdminMessage(
		String userId,
		AdminMessage msg,
		String[] users)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_emg_type_stat_maint.prc_iu_admin_message";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setString(++paramIndex, msg.getMsgText());
			cs.setTimestamp(
				++paramIndex,
				new Timestamp(msg.getMsgBegDate().getTime()));
			cs.setTimestamp(
				++paramIndex,
				new Timestamp(msg.getMsgThruDate().getTime()));
			cs.setString(++paramIndex, msg.getAdminMsgTypeCode());
			cs.setString(++paramIndex, msg.getMsgPrtyCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;

			cs.execute();

			int msgId = ((Integer) cs.getObject(newIdIndex)).intValue();
			msg.setAdminMsgId(new Integer(msgId));

			insertMessageRecipnts(conn, userId, msg, users);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return msg;
	}

	private void insertMessageRecipnts(
		Connection conn,
		String userId,
		AdminMessage msg,
		String[] users)
		throws DataSourceException
	{
		CallableStatement cs = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_iu_admin_message_recipnt";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,? ) }");

		try
		{
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters

			cs.setInt(1, msg.getAdminMsgIdNbr());

			for (byte i = 0; i < users.length; i++)
			{
				cs.setString(2, users[i]);
				cs.setNull(3, Types.DATE);
				cs.execute();
			}

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return;
	}

	public List getUserAdminMessages(
		String userId,
		String recipntId,
		boolean unreadOnly)
		throws DataSourceException
	{
		List list = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_admin_msg_recipnt_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setString(++paramIndex, recipntId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				boolean unread =
					rs.getTimestamp("msg_rcv_date") == null ? true : false;
				if ((unread && unreadOnly) || !unreadOnly)
				{
					AdminMessage am = new AdminMessage();
					am.setAdminMsgId(new Integer(rs.getInt("admin_msg_id")));
					List msgList = getAdminMessages(userId, am);
					if (msgList.size() != 0)
					{
						am = (AdminMessage) msgList.iterator().next();
						am.setReadFlag(unread ? "N" : "Y");
						list.add(am);
					}
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return list;
	}

	public void setMsgRecipnt(int msgId, String recipntId, boolean toUnread)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_update_admin_msg_rcv_date";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			cs.setInt(1, msgId);
			cs.setString(2, recipntId);
			if (toUnread)
			{
				cs.setNull(3, Types.DATE);
			} else
			{
				cs.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
			}

			//  output parameters
			cs.registerOutParameter(4, Types.INTEGER);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public void purgeMessage(String userId, int msgId)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_delete_admin_message";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			cs.setString(1, userId);
			cs.setString(2, dbCallTypeCode);

			cs.setInt(3, msgId);

			//  output parameters
			cs.registerOutParameter(4, Types.INTEGER);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public List getAdminMessageRecipients(String userId, int msgId)
		throws DataSourceException
	{
		List list = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_admin_msg_recipnt_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			
			cs.setInt(++paramIndex, msgId);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				list.add(
					new MessageRecipientBean(
						msgId,
						rs.getString("msg_recipnt_userid"),
						rs.getTimestamp("msg_rcv_date")));
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return list;
	}

}
