package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.model.CountryExceptionBean;
import emgshared.model.CountryMasterBean;

/**
 * @author
 *
 */
public class CountryExceptionDAO extends AbstractOracleDAO
{

	public List getCntryExceptions(String country) throws DataSourceException
	{
		List excps = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		StringBuffer sqlBuffer = new StringBuffer(64);
		String storedProcName = null;
		storedProcName =
			"pkg_em_biller_and_country.prc_get_country_exception_cv";
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setString(++parm, country);

			//  output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(parm);
			long count = 0;
			while (rs.next())
			{
			    count++;
				CountryExceptionBean excp = new CountryExceptionBean();
				excp.setRcvIsoCntryCode(rs.getString("RCV_ISO_CNTRY_CODE"));
				excp.setIsoCntryCode(rs.getString("ISO_CNTRY_CODE"));
				excp.setSndAllowFlag(rs.getString("SND_ALLOW_FLAG"));
				//				excp.setSndMaxAmt(nf.format(rs.getDouble("SND_MAX_AMT")));
				//				excp.setSndThrldWarnAmt(nf.format(rs.getDouble("SND_THRLD_WARN_AMT")));
				excp.setSndMaxAmtNbr(rs.getFloat("SND_MAX_AMT"));
				excp.setSndThrldWarnAmtNbr(rs.getFloat("SND_THRLD_WARN_AMT"));
				excp.setExcpCmntText(rs.getString("EXCP_CMNT_TEXT"));
				excp.setManualReviewAllowFlag(rs.getString("MANL_RVW_ALLOW_FLAG"));
				excps.add(excp);
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return excps;
	}

	public CountryExceptionBean getCntryException(String id1, String id2)
		throws DataSourceException
	{
		CountryExceptionBean excp = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		StringBuffer sqlBuffer = new StringBuffer(64);
		String storedProcName = null;
		storedProcName =
			"pkg_em_biller_and_country.prc_get_country_exception_cv";
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setString(++parm, id1);
			cs.setString(++parm, id2);

			//  output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(parm);
			long count = 0;
			if (rs.next())
			{
			    count++;
				excp = new CountryExceptionBean();
				excp.setRcvIsoCntryCode(rs.getString("RCV_ISO_CNTRY_CODE"));
				excp.setIsoCntryCode(rs.getString("ISO_CNTRY_CODE"));
				excp.setSndAllowFlag(rs.getString("SND_ALLOW_FLAG"));
				//				excp.setSndMaxAmt(nf.format(rs.getDouble("SND_MAX_AMT")));
				//				excp.setSndThrldWarnAmt(nf.format(rs.getDouble("SND_THRLD_WARN_AMT")));
				excp.setSndMaxAmtNbr(rs.getFloat("SND_MAX_AMT"));
				excp.setSndThrldWarnAmtNbr(rs.getFloat("SND_THRLD_WARN_AMT"));

				excp.setExcpCmntText(rs.getString("EXCP_CMNT_TEXT"));
				excp.setManualReviewAllowFlag(rs.getString("MANL_RVW_ALLOW_FLAG"));
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return excp;
	}

	public void setCntryException(CountryExceptionBean excp)
		throws DataSourceException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_biller_and_country.prc_iu_country_exception";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
		//		NumberFormat nf = new DecimalFormat("#,##0.00");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setString(++parm, excp.getRcvIsoCntryCode());
			cs.setString(++parm, excp.getIsoCntryCode());
			cs.setString(++parm, excp.getSndAllowFlag());
			//			cs.setDouble(++parm,  (nf.parse(excp.getSndMaxAmt()).doubleValue()));
			//			cs.setDouble(++parm,  (nf.parse(excp.getSndThrldWarnAmt()).doubleValue()));
			cs.setFloat(++parm, excp.getSndMaxAmtNbr());
			cs.setFloat(++parm, excp.getSndThrldWarnAmtNbr());
			cs.setString(++parm, excp.getExcpCmntText());
			cs.setString(++parm, excp.getManualReviewAllowFlag());

			//  output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void deleteCntryException(String id1, String id2)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_biller_and_country.prc_delete_country_exception";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			//  input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setString(++parm, id1);
			cs.setString(++parm, id2);

			//  output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);

			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return;
	}


	/**
	 * @param countryStatus, if empty string "", then get all, else "ACT" active or "NAT", inactive countries
	 * @return Hashmap of CountryMaster objects, keyed by ISO Country Code
	 * @throws DataSourceException
	 */
	public HashMap getCntryMasterList(String countryStatus)
			throws DataSourceException
		{
			if (countryStatus == null)
			{
				throw new DataSourceException("Invalid Country Status Parameter: null value");
			}
			else if (!((countryStatus.equalsIgnoreCase("ACT")) ||
			          (countryStatus.equalsIgnoreCase("NAT")) ||
					  (countryStatus.equals(""))) )
			{
					throw new DataSourceException("Invalid Country Status Parameter:" + countryStatus);
			}
			HashMap countryMasterList = new HashMap();
			ResultSet rs = null;
			CallableStatement cs = null;
			Connection conn = null;
			StringBuffer sqlBuffer = new StringBuffer(64);
			String storedProcName = new String("pkg_em_status_and_type.prc_get_country_master_cv");
			sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");
			try
			{
				conn = getConnection();
				cs = conn.prepareCall(sqlBuffer.toString());

				//  input parameters
				int parm = 0;
				cs.setString(++parm, "WEB");
				cs.setString(++parm, "WEB");
				cs.setNull(++parm, Types.VARCHAR);
				cs.setString(++parm, countryStatus.toUpperCase());
				//  output parameters
				cs.registerOutParameter(++parm, Types.INTEGER);
				int dbLogIdIndex = parm;
				cs.registerOutParameter(++parm, OracleTypes.CURSOR);
				cs.execute();
				long dbLogId = cs.getLong(dbLogIdIndex);
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
				rs = (ResultSet) cs.getObject(parm);
				long count = 0;
				while (rs.next())
				{
				    count++;
					CountryMasterBean cmb = new CountryMasterBean();
					cmb.setActiveBeginDate(rs.getDate("actv_beg_date"));
					cmb.setActiveThruDate(rs.getDate("actv_thru_date"));
					cmb.setCountryGeoCodeId(new Integer(rs.getInt("cntry_geocode_id")));
					cmb.setCountryName(rs.getString("cntry_name"));
					cmb.setISOCountryAbbrCode(rs.getString("iso_cntry_abbr_code"));
					cmb.setISOCountryCode(rs.getString("iso_cntry_code"));
					cmb.setISOCountryNumber(new Integer(rs.getInt("iso_cntry_nbr")));
					cmb.setLclCurrencyCode(rs.getString("lcl_currency_code"));
					cmb.setMainFrameCountryCode(rs.getString("mf_cntry_code"));
					countryMasterList.put(cmb.getISOCountryCode(),cmb);
				}
				logEndTime(dbLogId,count);
			} catch (Exception e)
			{
				throw new DataSourceException(e);
			} finally
			{
				close(rs);
				close(cs);
			}
			return countryMasterList;
		}

}
