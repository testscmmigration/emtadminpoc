/**
 * @author A131
 *
 * Logger class that returns an instance of the MoneyGram LogFactory class.
 * This class takes in the class file name so that it will appear in the log entry.
 */
package emgshared.dataaccessors;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

public class EMGSharedLogger
{
	public static Logger getLogger(String className)
	{
		return LogFactory.getInstance().getLogger(className);
	}
	
	public static Logger getLogger(Class clazz)
	{
		return LogFactory.getInstance().getLogger(clazz);
	}
	
}
