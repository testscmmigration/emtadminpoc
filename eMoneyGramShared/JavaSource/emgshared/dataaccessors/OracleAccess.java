package emgshared.dataaccessors;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.moneygram.common.log.Logger;

import emgshared.exceptions.DataSourceException;

public class OracleAccess {
	public static final String E_MONEYGRAM_RESOURCE_REF = "java:comp/env/jdbc/eMoneyGramDB";
	private static final Logger LOGGER = EMGSharedLogger.getLogger(OracleAccess.class.getName().toString());
	private static DataSource dataSource;
	private static InitialContext initialContext;

	static {
		dataSource = getDataSource(E_MONEYGRAM_RESOURCE_REF);
	}
	
	private OracleAccess()	{
	}

	public static Connection getConnection() throws DataSourceException  {
		Connection conn = null;

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("In JndiDataSourceHelper: getting MGT connection from jndi datasource: "
							+ E_MONEYGRAM_RESOURCE_REF + "...");
		}
        try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			throw new DataSourceException(e);
		}

        return conn;
    }

	public static DataSource getDataSource() {
		return dataSource;
	}
	
    private static DataSource getDataSource(String dataSourceName) {
        DataSource ds = null;
        try {

           if (null == initialContext) {
              initialContext = new InitialContext();
           }
           if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Getting Connection: " + dataSourceName + "...");
				LOGGER.debug(Thread.currentThread().getName() + ": "
						+ "Getting Connection: " + dataSourceName + "...");
			}
           ds = (DataSource) initialContext.lookup(dataSourceName);
        }
        catch (NamingException ne) {
        	LOGGER.error(Thread.currentThread().getName() + ": " + "JndiDataSourceHelper: getConnection: NamingException - ", ne);
        }
        catch (Throwable e) {
        	
            LOGGER.error("Exception in JndiDataSource: " + e.getMessage());
            LOGGER.error(Thread.currentThread().getName() + ": " + "JndiDataSourceHelper: getConnection: Exception: ", e);
        }
        return ds;
    }


	public static void close(Connection conn)
	{
		if (conn != null)
		{
			try
			{
				conn.close();
			} catch (SQLException ignore)
			{
				LOGGER.error(OracleAccess.class.getName(), ignore);
			} finally
			{
				conn = null;
			}
		}
	}

	public static void close(Statement stmt)
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
			} catch (SQLException ignore)
			{
				//  EMoneyGramAdmLogger.getLogger(this.getClass().getName().toString()).error(this.getClass().getName(), ignore);
				LOGGER.error(
						E_MONEYGRAM_RESOURCE_REF
						+ " - Error: Close Statement failed - '"
						+ ignore.getMessage()
						+ "'");
			} finally
			{
				stmt = null;
			}
		}
	}

	public static void close(ResultSet rs)
	{
		if (rs != null)
		{
			try
			{
				rs.close();
			} catch (SQLException ignore)
			{
				LOGGER.error(
						E_MONEYGRAM_RESOURCE_REF
						+ " - Error: Close ResultSet failed - '"
						+ ignore.getMessage()
						+ "'");
			} finally
			{
				rs = null;
			}
		}
	}

}