package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;

import emgshared.exceptions.DataSourceException;
import emgshared.model.MDBatch;
import emgshared.model.MicroDeposit;
import emgshared.model.MicroDepositStatus;
import emgshared.util.DateFormatter;

/**
 * @author T349
 *
 */
public class MicroDepositDAO extends AbstractOracleDAO
{
	private DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
	private NumberFormat nf = new DecimalFormat("#0.00");

	public int insertMicroDepositValidation(MicroDeposit microDeposit)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		int validationId = -1;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_insert_micro_deposit";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, microDeposit.getCustAccountId());
			cs.setInt(++paramIndex, microDeposit.getCustAccountVersionNbr());
			cs.setString(++paramIndex, microDeposit.getStatus().getMdStatus());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int validationIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			validationId =
				((Integer) cs.getObject(validationIdIndex)).intValue();
			logEndTime(dbLogId,1);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return validationId;
	}

	public void insertMicroDepositTransaction(MicroDeposit microDeposit)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_insert_micro_dep_tran";
			cs =
				conn.prepareCall("{ call " + storedProcName + "(?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, microDeposit.getValidationId());
			cs.setFloat(
				++paramIndex,
				microDeposit
					.getDepositAmount()[microDeposit
					.getDepositAmountIndex()]);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public void updateMicroDepositValidation(MicroDeposit microDeposit)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_update_mic_dep_status";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, microDeposit.getValidationId());
			cs.setInt(++paramIndex, microDeposit.getCustAccountId());
			cs.setInt(++paramIndex, microDeposit.getCustAccountVersionNbr());
			cs.setString(++paramIndex, microDeposit.getStatus().getMdStatus());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public MicroDeposit validateMicroDepositAmounts(MicroDeposit microDeposit)
		throws DataSourceException, SQLException
	{
		MicroDeposit mdReturn = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName =
			"pkg_em_cust_account_maint.prc_validate_micro_deposit";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");
		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters	
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, microDeposit.getValidationId());
			cs.setFloat(++paramIndex, microDeposit.getDepositAmount()[0]);
			cs.setFloat(++paramIndex, microDeposit.getDepositAmount()[1]);
			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			if (rs.next())
			{
			    count++;
				mdReturn = new MicroDeposit();
				mdReturn.setValidationId(rs.getInt("MIC_DEP_VLDN_ID"));
				mdReturn.setCustAccountId(rs.getInt("CUST_ACCT_ID"));
				mdReturn.setCustAccountVersionNbr(
					rs.getInt("CUST_ACCT_VER_NBR"));
				MicroDepositStatus status = new MicroDepositStatus();
				status.setMdStatus(rs.getString("MIC_DEP_STAT_CODE"));
				status.setMdStatusDesc(rs.getString("MIC_DEP_STAT_DESC"));
				mdReturn.setStatus(status);
				mdReturn.setStatUpdateDate(
					df.format(rs.getTimestamp("stat_update_date")));
				mdReturn.setValidationTryCount(rs.getInt("VLDN_TRY_CNT"));
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return mdReturn;
	}

	/* Method returns all the micro deposit accounts that are in the validation table for
	 * a given cust id.
	 * Returns a MultiMap as multiple account id's may be in the map.
	 */
	public MultiMap getMicroDepositAccounts(int custId)
		throws DataSourceException, SQLException
	{
		MultiMap accounts = new MultiHashMap();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName =
			"pkg_em_cust_account_maint.prc_get_micro_deposits";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");
		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters	
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				MicroDeposit md = new MicroDeposit();
				md.setValidationId(rs.getInt("MIC_DEP_VLDN_ID"));
				md.setCustAccountId(rs.getInt("CUST_ACCT_ID"));
				md.setCustAccountVersionNbr(rs.getInt("CUST_ACCT_VER_NBR"));
				MicroDepositStatus status = new MicroDepositStatus();
				status.setMdStatus(rs.getString("MIC_DEP_STAT_CODE"));
				status.setMdStatusDesc(rs.getString("MIC_DEP_STAT_DESC"));
				md.setStatus(status);
				md.setValidationTryCount(rs.getInt("VLDN_TRY_CNT"));
				md.setStatUpdateDate(
					df.format(rs.getTimestamp("stat_update_date")));
				md.setCreateDate(rs.getTimestamp("CREATE_DATE").toString());
				accounts.put(String.valueOf(md.getCustAccountId()), md);
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return accounts;
	}

	public Collection getMicroDepositValidations(MicroDeposit microDeposit)
		throws DataSourceException, SQLException
	{
		Collection accounts = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName =
			"pkg_em_cust_account_maint.prc_get_micro_deposits";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");
		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters	
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, microDeposit.getCustAccountId());
			cs.setInt(++paramIndex, microDeposit.getCustAccountVersionNbr());
			if (microDeposit.getStatus() != null)
			{
				cs.setString(
					++paramIndex,
					microDeposit.getStatus().getMdStatus());
			} else
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				MicroDeposit md = new MicroDeposit();
				md.setValidationId(rs.getInt("MIC_DEP_VLDN_ID"));
				md.setCustAccountId(rs.getInt("CUST_ACCT_ID"));
				md.setCustAccountVersionNbr(rs.getInt("CUST_ACCT_VER_NBR"));
				MicroDepositStatus status = new MicroDepositStatus();
				status.setMdStatus(rs.getString("MIC_DEP_STAT_CODE"));
				status.setMdStatusDesc(rs.getString("MIC_DEP_STAT_DESC"));
				md.setStatus(status);
				md.setValidationTryCount(rs.getInt("VLDN_TRY_CNT"));
				md.setStatUpdateDate(
					df.format(rs.getTimestamp("stat_update_date")));
				md.setCreateDate(rs.getTimestamp("CREATE_DATE").toString());
				accounts.add(md);
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return accounts;
	}

	public List getMDBatchList(String fromDate, String toDate, String userId)
		throws DataSourceException, SQLException
	{
		List batchList = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName =
			"pkg_em_cust_account_maint.prc_get_micro_deposit_batch";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");
		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters	
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, fromDate);
			cs.setString(++paramIndex, toDate);
			cs.setNull(++paramIndex, Types.FLOAT);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				MDBatch mdb = new MDBatch();
				mdb.setBatchId(String.valueOf(rs.getInt("file_cntl_seq_nbr")));
				mdb.setReceiveDate(df.format(rs.getTimestamp("receive_date")));
				mdb.setRecCnt(String.valueOf(rs.getInt("rec_cnt")));
				batchList.add(mdb);
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return batchList;
	}

	public List getMDTrans(
		Integer batchId,
		Integer tranId,
		String tranStat,
		String userId)
		throws DataSourceException, SQLException
	{
		List vldnList = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName =
			"pkg_em_cust_account_maint.prc_get_micro_deposit_tran";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");
		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters	
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			if (tranId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, tranId.intValue());
			}
			if (batchId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, batchId.intValue());
			}

			if (tranStat == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, tranStat);
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(rsIndex);

			int saveId = -1;
			MicroDeposit md = null;
			long count = 0;
			while (rs.next())
			{
			    count++;
				int newId = rs.getInt("cust_acct_id");
				if (newId != saveId)
				{
					md = new MicroDeposit();
					md.setValidationId(rs.getInt("mic_dep_vldn_id"));
					md.setCustAccountId(rs.getInt("cust_acct_id"));
					md.setCustAccountVersionNbr(rs.getInt("cust_acct_ver_nbr"));
					MicroDepositStatus mds = new MicroDepositStatus();
					mds.setMdStatus(rs.getString("mic_dep_stat_code"));
					mds.setMdStatusDesc(rs.getString("mic_dep_stat_desc"));
					md.setStatus(mds);
					md.setValidationTryCount(rs.getInt("vldn_try_cnt"));
					md.setStatUpdateDate(
						df.format(rs.getTimestamp("stat_update_date")));
					md.setCreateDate(df.format(rs.getTimestamp("create_date")));
					md.setCreateUser(rs.getString("create_userid"));
					md.setLastUpdateDate(
						df.format(rs.getTimestamp("last_update_date")));
					md.setLastUpdateUser(rs.getString("last_update_userid"));
					md.setDepositTranId1(rs.getInt("mic_dep_tran_id"));
					md.setFmtAmount1(nf.format(rs.getFloat("dep_amt")));
					md.setBatchId1(rs.getInt("ach_file_cntl_seq_nbr"));
					md.setProcessACR(false);
					md.setProcessDeactiveAcct(false);
					saveId = newId;
				} else
				{
					md.setDepositTranId2(rs.getInt("mic_dep_tran_id"));
					md.setFmtAmount2(nf.format(rs.getFloat("dep_amt")));
					md.setBatchId2(rs.getInt("ach_file_cntl_seq_nbr"));
					vldnList.add(md);
				}
			}
			logEndTime(dbLogId,count);
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return vldnList;
	}

	public void updateMDStatus(MicroDeposit microDeposit)
		throws DataSourceException
	{
		updateMicroDepositStatus(
			microDeposit.getDepositTranId1(),
			microDeposit.getCallerLoginId());
		updateMicroDepositStatus(
			microDeposit.getDepositTranId2(),
			microDeposit.getCallerLoginId());
	}

	private void updateMicroDepositStatus(int tranId, String userId)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_update_mic_dep_status";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);
			cs.setString(++paramIndex, MicroDepositStatus.ACH_RETURNED_CODE);
			cs.setInt(++paramIndex, 1);
			cs.setInt(++paramIndex, 8);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setString(++paramIndex, "Micro Deposit ACH Return");
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public void overrideMicroDepositStatus(MicroDeposit md)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_update_mic_dep_status";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, md.getCallerLoginId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, md.getValidationId());
			cs.setInt(++paramIndex, md.getCustAccountId());
			cs.setInt(++paramIndex, md.getCustAccountVersionNbr());
			cs.setString(++paramIndex, MicroDepositStatus.ACH_VALIDATED_CODE);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}
}