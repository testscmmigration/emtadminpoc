package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedIP;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.model.CustomerProfileAccount;
import emgshared.model.ElectronicTypeCode;
import emgshared.model.NegativeTermBean;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.StringHelper;

public class FraudDAO extends emgshared.dataaccessors.AbstractOracleDAO
{

	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public Collection getBlockedIPs(
		String userID,
		String beginIP,
		String endIP)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection blockedIPs = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_ip_addr_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (beginIP == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, beginIP);
			}

			if (endIP == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, endIP);

			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		
			while (rs.next())
			{
				BlockedIP blockedIP = new BlockedIP();
				blockedIP.setIpAddress1(rs.getString("BLKD_IP_ADDR_TEXT"));
				blockedIP.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				blockedIP.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				blockedIP.setComment(rs.getString("BLKD_CMNT_TEXT"));
				blockedIP.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				blockedIP.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				blockedIP.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				blockedIP.setStatusUpdateDate(formatter.format(rs.getTimestamp("STAT_UPDATE_DATE")));
				blockedIP.setCreateDate(formatter.format(rs.getTimestamp("CREATE_DATE")));
				
				blockedIP.setCreateUserId(rs.getString("CREATE_USERID"));
				blockedIP.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				blockedIPs.add(blockedIP);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return blockedIPs;
	}

	public Collection getBlockedIPs(String userID, BlockedIP blockedIPList)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection blockedIPs = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_ip_addr_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (blockedIPList.getIpAddress1() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedIPList.getIpAddress1());

			if (blockedIPList.getIpAddress2() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedIPList.getIpAddress2());

			if (blockedIPList.getReasonCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedIPList.getReasonCode());
			}
			if (blockedIPList.getStatusCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedIPList.getStatusCode());
			}

			if (blockedIPList.getFraudRiskLevelCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(
					++paramIndex,
					blockedIPList.getFraudRiskLevelCode());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedIP blockedIP = new BlockedIP();
				blockedIP.setIpAddress1(rs.getString("BLKD_IP_ADDR_TEXT"));
				blockedIP.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				blockedIP.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				blockedIP.setComment(rs.getString("BLKD_CMNT_TEXT"));
				blockedIP.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				blockedIP.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				blockedIP.setFraudRiskLevelCode(
					rs.getString("FRAUD_RISK_LVL_CODE"));
				blockedIP.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				blockedIP.setCreateDate(rs.getDate("CREATE_DATE").toString());
				blockedIP.setCreateUserId(rs.getString("CREATE_USERID"));
				blockedIP.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				blockedIPs.add(blockedIP);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return blockedIPs;
	}

	public Collection getBlockedReasons(String userID, String blockedTypeCode)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection blockedReasons = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_reason_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.VARCHAR);

			if (blockedTypeCode == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedTypeCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				BlockedReason blockedReason = new BlockedReason();
				blockedReason.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				blockedReason.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				blockedReason.setReasonType(rs.getString("BLKD_TYPE_CODE"));
				blockedReason.setCreateDate(
					rs.getDate("CREATE_DATE").toString());
				blockedReason.setCreateUserId(rs.getString("CREATE_USERID"));
				blockedReason.setUpdateDate(
					rs.getDate("LAST_UPDATE_DATE").toString());
				blockedReason.setUpdateUserId(
					rs.getString("LAST_UPDATE_USERID"));
				blockedReasons.add(blockedReason);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return blockedReasons;
	}

	public void setBlockedIPs(String userID, BlockedIP blockedIP)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_insrt_blocked_ip_addr";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedIP.getIpAddress1());

			if (blockedIP.getIpAddress2() == null
				|| blockedIP.getIpAddress2().equals(""))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedIP.getIpAddress2());

			cs.setString(++paramIndex, blockedIP.getReasonCode());
			cs.setString(++paramIndex, blockedIP.getComment());
			cs.setString(++paramIndex, blockedIP.getStatusCode());
			cs.setString(++paramIndex, blockedIP.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getBlockedStatus(String userID, String blockedType)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection blockedStatuss = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_status_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedType);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedStatus blockedStatus = new BlockedStatus();
				blockedStatus.setBlockedTypeCode(
					rs.getString("BLKD_TYPE_CODE"));
				blockedStatus.setBlockedStatusCode(
					rs.getString("BLKD_STAT_CODE"));
				blockedStatus.setBlockedStatusDesc(
					rs.getString("BLKD_STAT_DESC"));
				blockedStatuss.add(blockedStatus);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return blockedStatuss;
	}

	public void setBlockedSSN(String userID, BlockedBean blockedSsn)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_insrt_blocked_ssn";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedSsn.getHashText());
			cs.setString(++paramIndex, blockedSsn.getMaskText());
			cs.setString(++paramIndex, blockedSsn.getEncryptedNumber());
			cs.setString(++paramIndex, blockedSsn.getReasonCode());
			cs.setString(++paramIndex, blockedSsn.getComment());
			cs.setString(++paramIndex, blockedSsn.getStatusCode());
			cs.setString(++paramIndex, blockedSsn.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getBlockedSSN(
		String userID,
		String hashText,
		String maskText)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection ssns = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_ssn_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (hashText == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, hashText);

			if (maskText == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, maskText);

			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean ssn = new BlockedBean();
				ssn.setHashText(rs.getString("BLKD_SSN_HASH_TEXT"));
				ssn.setMaskText(rs.getString("BLKD_SSN_MASK_NBR"));
				ssn.setEncryptedNumber(rs.getString("BLKD_SSN_ENCRYP_NBR"));
				ssn.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				ssn.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				ssn.setComment(rs.getString("BLKD_CMNT_TEXT"));
				ssn.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				ssn.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				ssn.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				ssn.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				ssn.setCreateDate(rs.getDate("CREATE_DATE").toString());
				ssn.setCreateUserId(rs.getString("CREATE_USERID"));
				ssn.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				ssns.add(ssn);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return ssns;
	}

	public Collection getBlockedSSN(String userID, BlockedBean blockedBean)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection ssns = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_ssn_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (blockedBean.getHashText() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedBean.getHashText());

			if (blockedBean.getMaskText() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, blockedBean.getMaskText());

			if (blockedBean.getReasonCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getReasonCode());
			}
			if (blockedBean.getStatusCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getStatusCode());
			}
			if (blockedBean.getFraudRiskLevelCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getFraudRiskLevelCode());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean ssn = new BlockedBean();
				ssn.setHashText(rs.getString("BLKD_SSN_HASH_TEXT"));
				ssn.setMaskText(rs.getString("BLKD_SSN_MASK_NBR"));
				ssn.setEncryptedNumber(rs.getString("BLKD_SSN_ENCRYP_NBR"));
				ssn.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				ssn.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				ssn.setComment(rs.getString("BLKD_CMNT_TEXT"));
				ssn.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				ssn.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				ssn.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				ssn.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				ssn.setCreateDate(rs.getDate("CREATE_DATE").toString());
				ssn.setCreateUserId(rs.getString("CREATE_USERID"));
				ssn.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				ssns.add(ssn);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return ssns;
	}

	public Collection getBlockedCCAccounts(
		String userID,
		BlockedBean blockedBean)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection ccs = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_crcd_acct_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (StringHelper.isNullOrEmpty(blockedBean.getBlockedId()))
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			}
			else
			{
				cs.setInt(++paramIndex,Integer.valueOf(blockedBean.getBlockedId()));
			}

			if (blockedBean.getMaskText() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getMaskText());
			}

			if (blockedBean.getReasonCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getReasonCode());
			}
			if (blockedBean.getStatusCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getStatusCode());
			}
			if (blockedBean.getFraudRiskLevelCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getFraudRiskLevelCode());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean cc = new BlockedBean();
				cc.setBlockedId(rs.getString("BLKD_CRCD_ACCT_ID"));
				cc.setMaskText(rs.getString("BLKD_CRCD_MASK_NBR"));
				cc.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				cc.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				cc.setComment(rs.getString("BLKD_CMNT_TEXT"));
				cc.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				cc.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				cc.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				cc.setStatusUpdateDate(rs.getDate("STAT_UPDATE_DATE").toString());
				cc.setCreateDate(rs.getDate("CREATE_DATE").toString());
				cc.setCreateUserId(rs.getString("CREATE_USERID"));
				cc.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				cc.setBinText(rs.getString("BLKD_CRCD_FRST_6_MASK_NBR"));
				cc.setAcctNumberLength(rs.getInt("BLKD_CRCD_LGTH_CNT"));
				ccs.add(cc);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return ccs;
	}
	public Collection getBlockedCCAccounts(String userID, String blockedId,
		String maskText)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection ccs = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_crcd_acct_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			
			if (StringHelper.isNullOrEmpty(blockedId))
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			}
			else
			{
				cs.setInt(++paramIndex,Integer.valueOf(blockedId));
			}

			if (maskText == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			}
			else
			{
				cs.setString(++paramIndex, maskText);
			}

			// Code indicating the reason why identifier is blocked
			cs.setNull(++paramIndex, Types.VARCHAR);
			
			// Current "blocked" status of the Social Security
			cs.setNull(++paramIndex, Types.VARCHAR);
			
			// Code indicating the fraud risk associated with the IP Address
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			
			// The unique identifer to the log entry.
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			
			// Reference cursor that returns the result set
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean cc = new BlockedBean();
				cc.setBlockedId(rs.getString("BLKD_CRCD_ACCT_ID"));
				cc.setMaskText(rs.getString("BLKD_CRCD_MASK_NBR"));
				cc.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				cc.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				cc.setComment(rs.getString("BLKD_CMNT_TEXT"));
				cc.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				cc.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				cc.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				
				cc.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				cc.setCreateDate(rs.getDate("CREATE_DATE").toString());
				cc.setCreateUserId(rs.getString("CREATE_USERID"));
				cc.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				cc.setBinText(rs.getString("BLKD_CRCD_FRST_6_MASK_NBR"));
				cc.setAcctNumberLength(rs.getInt("BLKD_CRCD_LGTH_CNT"));
				ccs.add(cc);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return ccs;
	}

	public String setBlockedCC(String userID, BlockedBean blockedCC)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_insrt_blocked_crcd_acct";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?,?,?,?) }");
		
		long blockedId;

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			
			cs.setString(++paramIndex, blockedCC.getMaskText());

			cs.setString(++paramIndex, blockedCC.getReasonCode());
			cs.setString(++paramIndex, blockedCC.getComment());
			cs.setString(++paramIndex, blockedCC.getStatusCode());
			cs.setString(++paramIndex, blockedCC.getFraudRiskLevelCode());
			cs.setString(++paramIndex, blockedCC.getBinText());
			cs.setInt(++paramIndex, blockedCC.getAcctNumberLength());
			
			// IN/OUT blockedId parameter
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			if (!StringHelper.isNullOrEmpty(blockedCC.getBlockedId())) {
				cs.setInt(paramIndex, Integer.valueOf(blockedCC.getBlockedId()));
			}
			else {
				cs.setNull(paramIndex, Types.INTEGER);
			}
			int blockedIdIndex = paramIndex; 
			
			//  Output parameters
			
			// Process log id
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			
			int dbLogIdIndex = paramIndex;
			
			System.out.println("dbLogIdIndex" + String.valueOf(dbLogIdIndex));

			cs.execute();

			blockedId = cs.getLong(blockedIdIndex);
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return Long.toString(blockedId);
	}

	public void setBlockedBin(String userID, BlockedBean blockedBin)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_insrt_blocked_crcd_bin";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedBin.getHashText());
			cs.setString(++paramIndex, blockedBin.getEncryptedNumber());
			cs.setString(++paramIndex, blockedBin.getReasonCode());
			cs.setString(++paramIndex, blockedBin.getComment());
			cs.setString(++paramIndex, blockedBin.getStatusCode());
			cs.setString(++paramIndex, blockedBin.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getBlockedBins(String userID, String hashText)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection bins = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_bin_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (hashText == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, hashText);
			}
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean bin = new BlockedBean();
				bin.setHashText(rs.getString("BLKD_CRCD_BIN_HASH_TEXT"));
				bin.setEncryptedNumber(rs.getString("BLKD_CRCD_BIN_ENCRYP_TEXT"));
				bin.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				bin.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				bin.setComment(rs.getString("BLKD_CMNT_TEXT"));
				bin.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				bin.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				bin.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				bin.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				bin.setCreateDate(rs.getDate("CREATE_DATE").toString());
				bin.setCreateUserId(rs.getString("CREATE_USERID"));
				bin.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				bins.add(bin);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return bins;
	}

	public void setBlockedPhone(String userID, BlockedBean bean)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_insrt_blocked_phone";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, bean.getClearText());
			cs.setString(++paramIndex, bean.getReasonCode());
			cs.setString(++paramIndex, bean.getComment());
			cs.setString(++paramIndex, bean.getStatusCode());
			cs.setString(++paramIndex, bean.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getBlockedPhones(String userID, BlockedBean blockedBean)
		throws DataSourceException, TooManyResultException
	{
		Collection phones = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_phone_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedBean.getClearText());
			if (blockedBean.getReasonCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getReasonCode());
			}
			if (blockedBean.getStatusCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getStatusCode());
			}
			if (blockedBean.getFraudRiskLevelCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getFraudRiskLevelCode());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean bean = new BlockedBean();
				bean.setClearText(rs.getString("blkd_ph_nbr"));
				bean.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				bean.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				bean.setComment(rs.getString("BLKD_CMNT_TEXT"));
				bean.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				bean.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				bean.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				bean.setStatusUpdateDate(
					df.format(rs.getTimestamp("STAT_UPDATE_DATE")));
				bean.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				bean.setUpdateDate(
					df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				bean.setCreateUserId(rs.getString("CREATE_USERID"));
				bean.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				phones.add(bean);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return phones;
	}

	public Collection getBlockedPhones(String userID, String phone)
		throws DataSourceException, TooManyResultException
	{
		Collection phones = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_blocked_list.prc_get_blocked_phone_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (phone == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, phone);
			}
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean bean = new BlockedBean();
				bean.setClearText(rs.getString("blkd_ph_nbr"));
				bean.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				bean.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				bean.setComment(rs.getString("BLKD_CMNT_TEXT"));
				bean.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				bean.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				bean.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				bean.setStatusUpdateDate(
					df.format(rs.getTimestamp("STAT_UPDATE_DATE")));
				bean.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				bean.setUpdateDate(
					df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				bean.setCreateUserId(rs.getString("CREATE_USERID"));
				bean.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				phones.add(bean);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return phones;
	}

	public Collection getConsumersByPhone(String userID, String phone)
		throws DataSourceException, TooManyResultException
	{
		Collection consumers = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_customer_profile.prc_get_cust_by_phone_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setString(++paramIndex, phone);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);
			Map tmpMap = new HashMap();

			int cnt = 0;
			while (rs.next())
			{
				String custId = String.valueOf(rs.getInt("cust_id"));
				String logonId = rs.getString("cust_logon_id");
				String tmpLogonId = null;
				tmpLogonId = (String) tmpMap.get(custId);

				if (tmpLogonId == null
					|| !tmpLogonId.equalsIgnoreCase(logonId))
				{
					tmpMap.put(custId, logonId);
					ConsumerProfileSearchView view =
						new ConsumerProfileSearchView();
					view.setCustId(custId);
					view.setCustLogonId(logonId);
					view.setCustStatCode(rs.getString("cust_stat_code"));
					view.setCustStatDesc(rs.getString("cust_stat_desc"));
					view.setCustSubStatCode(rs.getString("cust_sub_stat_code"));
					view.setCustSubStatDesc(rs.getString("cust_sub_stat_desc"));
					view.setCustFrstName(rs.getString("cust_frst_name"));
					view.setCustLastName(rs.getString("cust_Last_name"));
					view.setCustSSNMask(rs.getString("cust_ssn_mask_nbr"));
					if (rs.getDate("cust_brth_date") == null)
					{
						view.setCustBrthDate("");
					} else
					{
						view.setCustBrthDate(
							df.format(rs.getDate("cust_brth_date")));
					}
					consumers.add(view);

					if (cnt++ > maxDownload)
					{
						throw new TooManyResultException(
							"Too many results and maximum allowed is "
								+ maxDownload);
					}
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return consumers;
	}

	public void setBlockedBankAccounts(String userID, BlockedBean blockedAcct)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_insrt_blocked_bank_acct";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, blockedAcct.getAbaNumber());

			if (blockedAcct.getHashText() == null)
			{
				cs.setString(++paramIndex, "*");
			} else
			{
				cs.setString(++paramIndex, blockedAcct.getHashText());
			}

			if (blockedAcct.getMaskText() == null)
			{
				cs.setString(++paramIndex, "*");
			} else
			{
				cs.setString(++paramIndex, blockedAcct.getMaskText());
			}

			cs.setString(++paramIndex, blockedAcct.getEncryptedNumber());
			cs.setString(++paramIndex, blockedAcct.getReasonCode());
			cs.setString(++paramIndex, blockedAcct.getComment());
			cs.setString(++paramIndex, blockedAcct.getStatusCode());
			cs.setString(++paramIndex, blockedAcct.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getBlockedBankAccounts(
		String userID,
		BlockedBean blockedBean)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection accounts = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_bank_acct_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (blockedBean.getAbaNumber() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getAbaNumber());
			}

			if (blockedBean.getHashText() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getHashText());
			}

			if (blockedBean.getMaskText() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getMaskText());
			}

			if (blockedBean.getReasonCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getReasonCode());
			}
			if (blockedBean.getStatusCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getStatusCode());
			}
			if (blockedBean.getFraudRiskLevelCode() == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, blockedBean.getFraudRiskLevelCode());
			}

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean account = new BlockedBean();
				account.setAbaNumber(rs.getString("BANK_ABA_NBR"));
				account.setHashText(rs.getString("BLKD_ACCT_HASH_TEXT"));
				account.setEncryptedNumber(
					rs.getString("BLKD_ACCT_ENCRYP_NBR"));
				account.setMaskText(rs.getString("BLKD_ACCT_MASK_NBR"));
				account.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				account.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				account.setComment(rs.getString("BLKD_CMNT_TEXT"));
				account.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				account.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				account.setFraudRiskLevelCode(
					rs.getString("FRAUD_RISK_LVL_CODE"));
				account.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				account.setCreateDate(rs.getDate("CREATE_DATE").toString());
				account.setCreateUserId(rs.getString("CREATE_USERID"));
				account.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				accounts.add(account);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return accounts;
	}

	public Collection getBlockedBankAccounts(
		String userID,
		String abaNumber,
		String hashText,
		String maskText)
		throws DataSourceException, SQLException, MaxRowsHashCryptoException
	{
		Collection accounts = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		int maxHashCrypto = dynProps.getMaxRowsHashCrypto();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_bank_acct_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (abaNumber == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, abaNumber);

			if (hashText == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, hashText);

			if (maskText == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, maskText);

			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean account = new BlockedBean();
				account.setAbaNumber(rs.getString("BANK_ABA_NBR"));
				account.setHashText(rs.getString("BLKD_ACCT_HASH_TEXT"));
				account.setEncryptedNumber(
					rs.getString("BLKD_ACCT_ENCRYP_NBR"));
				account.setMaskText(rs.getString("BLKD_ACCT_MASK_NBR"));
				account.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				account.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				account.setComment(rs.getString("BLKD_CMNT_TEXT"));
				account.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				account.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				account.setFraudRiskLevelCode(
					rs.getString("FRAUD_RISK_LVL_CODE"));
				account.setStatusUpdateDate(
					rs.getDate("STAT_UPDATE_DATE").toString());
				account.setCreateDate(rs.getDate("CREATE_DATE").toString());
				account.setCreateUserId(rs.getString("CREATE_USERID"));
				account.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				accounts.add(account);

				if (cnt++ > maxHashCrypto)
				{
					throw new MaxRowsHashCryptoException(
						"Too many results and maximum allowed is "
							+ maxHashCrypto);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return accounts;
	}

	public Collection getConsumersByAccounts(
		String userID,
		String abaOrBinHash,
		String maskNbr,
		boolean isCC, int acctId)
		throws DataSourceException, TooManyResultException
	{
		Collection customerProfileAccounts = new ArrayList();
		ResultSet resultSet = null;
		CallableStatement callableStatement = null;
		Connection connection = null;

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName = null;
		StringBuffer sqlBuffer = new StringBuffer(64);
		if (isCC){
			storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_crcd_info_cv";
			sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		} else{
			storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_bank_info_cv";
			sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		}
		
		
		try
		{
			connection = getConnection();
			callableStatement = connection.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			callableStatement.setString(++paramIndex, userID);
			callableStatement.setString(++paramIndex, dbCallTypeCode);

			callableStatement.setString(++paramIndex, abaOrBinHash);

			if (maskNbr == null)
				callableStatement.setNull(++paramIndex, Types.VARCHAR);
			else
				callableStatement.setString(++paramIndex, maskNbr);


			// super taint changes
		
			if (isCC) {
				if (acctId == 0)
					callableStatement.setNull(++paramIndex, Types.NULL);
				else
					callableStatement.setInt(++paramIndex, acctId);

			} 
			//  output parameters
			callableStatement.registerOutParameter(++paramIndex, Types.INTEGER);
			callableStatement.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			callableStatement.execute();
			resultSet = (ResultSet) callableStatement.getObject(rsIndex);

			int cnt = 0;
			while (resultSet.next())
			{
				CustomerProfileAccount cpa = new CustomerProfileAccount();
				cpa.setCustAcctId(resultSet.getInt("cust_acct_id"));
				cpa.setCustAcctVerNbr(resultSet.getInt("cust_acct_ver_nbr"));
				cpa.setCustId(resultSet.getInt("cust_id"));
				cpa.setAcctTypeCode(resultSet.getString("acct_type_code"));
				cpa.setAcctTypeDesc(resultSet.getString("acct_type_desc"));
				cpa.setAcctStatCode(resultSet.getString("acct_stat_code"));
				cpa.setAcctStatDesc(resultSet.getString("acct_stat_desc"));
				cpa.setAcctSubStatCode(resultSet.getString("acct_sub_stat_code"));
				cpa.setAcctSubStatDesc(resultSet.getString("acct_sub_stat_desc"));
				cpa.setAcctEncrypNbr(resultSet.getString("acct_encryp_nbr"));
				cpa.setAcctMaskNbr(resultSet.getString("acct_mask_nbr"));
				cpa.setAcctHoldrName(resultSet.getString("acct_holdr_name"));
				cpa.setAcctName(resultSet.getString("acct_name"));
				cpa.setBankAbaNbr(resultSet.getString("bank_aba_nbr"));
				cpa.setCrcdBinHashText(resultSet.getString("crcd_bin_hash_text"));
				cpa.setCustLogonId(resultSet.getString("cust_logon_id"));
				cpa.setCustStatCode(resultSet.getString("cust_stat_code"));
				cpa.setCustStatDesc(resultSet.getString("cust_stat_desc"));
				cpa.setCustSubStatCode(resultSet.getString("cust_sub_stat_code"));
				cpa.setCustSubStatDesc(resultSet.getString("cust_sub_stat_desc"));
				cpa.setCustLastName(resultSet.getString("cust_last_name"));
				cpa.setCustFrstName(resultSet.getString("cust_frst_name"));
				cpa.setCustSsnMaskNbr(resultSet.getString("cust_ssn_mask_nbr"));
				cpa.setCustBrthDate(resultSet.getString("cust_brth_date"));
				cpa.setBlkCrcdAcctId(resultSet.getString("blkd_crcd_acct_id"));
				System.out.println("BlkCrcdAcctId" + cpa.getBlkCrcdAcctId());
				cpa.setCrcdBinText(resultSet.getString("crcd_bin_mask_nbr"));
				customerProfileAccounts.add(cpa);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(resultSet);
			close(callableStatement);
		}

		return customerProfileAccounts;
	}

	public void setTaintedPhoneInd(
		String userId,
		int custId,
		String phoneNumber,
		String taintedInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_update_ph_blkd_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, phoneNumber);
			cs.setString(++paramIndex, taintedInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setTaintedSsnInd(String userId, int custId, String taintedInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_update_ssn_blkd_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setString(++paramIndex, taintedInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setTaintedAccountInd(
		String userId,
		String acctIds,
		String taintedInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_update_acct_blkd_code_arr";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, acctIds);
			cs.setString(++paramIndex, taintedInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setTaintedAbaInd(
		String userId,
		String abaNumber,
		String taintedInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_update_bank_aba_blkd_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?, ?) }");
		/*
				PROCEDURE prc_update_bank_aba_blkd_code
				   (iv_user_id                     IN customer.cust_logon_id%TYPE,
					iv_call_type_code              IN process_log_a.call_type_code%TYPE,
					iv_bank_aba_nbr                IN cust_account.bank_aba_nbr%TYPE,
					iv_bank_aba_blkd_code          IN cust_account.bank_aba_blkd_code%TYPE,
					ov_prcs_log_id                OUT process_log_a.prcs_log_id%TYPE
				   );
		*/
		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, abaNumber);
			cs.setString(++paramIndex, taintedInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setTaintedBinInd(
		String userId,
		String binHash,
		String taintedInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_update_crcd_bin_blkd_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, binHash);
			cs.setString(++paramIndex, taintedInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public Collection getConsumersByEmail(
		String userID,
		String emailAddress,
		String emailDomain,
		String emailType)
		throws DataSourceException, TooManyResultException
	{
		Collection consumers = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_customer_profile.prc_get_cust_by_elec_addr_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (emailAddress == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, emailAddress);

			if (emailDomain == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, emailDomain);

			cs.setString(++paramIndex, emailType);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);
			Map tmpMap = new HashMap();

			int cnt = 0;
			while (rs.next())
			{
				String custId = String.valueOf(rs.getInt("cust_id"));
				String logonId = rs.getString("cust_logon_id");
				String tmpLogonId = null;
				tmpLogonId = (String) tmpMap.get(custId);

				if (tmpLogonId == null
					|| !tmpLogonId.equalsIgnoreCase(logonId))
				{
					tmpMap.put(custId, logonId);
					ConsumerProfileSearchView view =
						new ConsumerProfileSearchView();
					view.setCustId(custId);
					view.setCustLogonId(logonId);
					view.setCustStatCode(rs.getString("cust_stat_code"));
					view.setCustStatDesc(rs.getString("cust_stat_desc"));
					view.setCustSubStatCode(rs.getString("cust_sub_stat_code"));
					view.setCustSubStatDesc(rs.getString("cust_sub_stat_desc"));
					view.setCustFrstName(rs.getString("cust_frst_name"));
					view.setCustLastName(rs.getString("cust_Last_name"));
					view.setCustSSNMask(rs.getString("cust_ssn_mask_nbr"));
					if (rs.getDate("cust_brth_date") == null)
					{
						view.setCustBrthDate("");
					} else
					{
						view.setCustBrthDate(
							df.format(rs.getDate("cust_brth_date")));
					}
					consumers.add(view);

					if (cnt++ > maxDownload)
					{
						throw new TooManyResultException(
							"Too many results and maximum allowed is "
								+ maxDownload);
					}
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return consumers;
	}

	public Collection getBlockedEmails(
		String userID,
		String emailAddress,
		String emailDomain)
		throws DataSourceException, TooManyResultException
	{
		Collection emails = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_elec_addr_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (StringHelper.isNullOrEmpty(emailAddress))
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, emailAddress);
			}

			if (StringHelper.isNullOrEmpty(emailDomain))
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, emailDomain);
			}

			cs.setString(++paramIndex, ElectronicTypeCode.EMAIL.getTypeCode());
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, BlockedStatus.BLOCKED_CODE);
			cs.setNull(++paramIndex, Types.VARCHAR);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				BlockedBean bean = new BlockedBean();
				bean.setClearText(rs.getString("BLKD_ELEC_ADDR_ID"));
				bean.setElecAddrTypeCode(rs.getString("ELEC_ADDR_TYPE_CODE"));
				bean.setAddrFmtCode(rs.getString("ADDR_FMT_CODE"));
				bean.setReasonCode(rs.getString("BLKD_REAS_CODE"));
				bean.setReasonDesc(rs.getString("BLKD_REAS_DESC"));
				bean.setComment(rs.getString("BLKD_CMNT_TEXT"));
				bean.setStatusCode(rs.getString("BLKD_STAT_CODE"));
				bean.setStatusDesc(rs.getString("BLKD_STAT_DESC"));
				bean.setFraudRiskLevelCode(rs.getString("FRAUD_RISK_LVL_CODE"));
				bean.setStatusUpdateDate(
					df.format(rs.getTimestamp("STAT_UPDATE_DATE")));
				bean.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				bean.setUpdateDate(
					df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				bean.setCreateUserId(rs.getString("CREATE_USERID"));
				bean.setUpdateUserId(rs.getString("LAST_UPDATE_USERID"));
				emails.add(bean);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return emails;
	}

	public void setBlockedEmail(String userID, BlockedBean bean)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_insrt_blocked_elec_addr";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (bean.getAddrFmtCode().equals("A"))
			{
				cs.setString(++paramIndex, bean.getClearText());
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setString(++paramIndex, bean.getClearText());
			}

			cs.setString(++paramIndex, bean.getElecAddrTypeCode());
			cs.setString(++paramIndex, bean.getReasonCode());
			cs.setString(++paramIndex, bean.getComment());
			cs.setString(++paramIndex, bean.getStatusCode());
			cs.setString(++paramIndex, bean.getFraudRiskLevelCode());

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setTaintedEmailInd(
		String userId,
		String custId,
		String emailAddress,
		String emailDomain,
		String elecAddrTypeCode,
		String taintedEmailInd,
		String taintedEmailDomainInd)
		throws DataSourceException
	{

		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_update_elec_addr_blkd_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (StringHelper.isNullOrEmpty(custId))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, custId);

			if (StringHelper.isNullOrEmpty(emailAddress))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, emailAddress);

			if (StringHelper.isNullOrEmpty(emailDomain))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, emailDomain);

			if (StringHelper.isNullOrEmpty(taintedEmailInd))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, taintedEmailInd);

			if (StringHelper.isNullOrEmpty(taintedEmailDomainInd))
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, taintedEmailDomainInd);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}
	
/*
PROCEDURE prc_get_char_string_list_cv
   (iv_user_id                    IN customer.cust_logon_id%TYPE,
    iv_call_type_code             IN process_log_a.call_type_code%TYPE,
    iv_strng_prps_code            IN character_string_list.strng_prps_code%TYPE,  -- Optional
    iv_char_strng_text            IN character_string_list.char_strng_text%TYPE,  -- Required
    iv_strng_actv_flag            IN character_string_list.strng_actv_flag%TYPE,  -- Optional  ('Y' or 'N' or NULL)
    iv_exact_match                IN NUMBER,                                      -- Required (0 - "Containing",  1 - Exact)
    ov_prcs_log_id               OUT process_log_a.prcs_log_id%TYPE,
    ov_char_string_list_cv       OUT char_string_list_cv_type
   )
*/
	
	public Collection getNegativeTerms(
			String userID,
			String negativeType,
			String negativeString,
			int flag)
			throws DataSourceException, TooManyResultException
	{
		Collection negativeTerms = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

		int maxDownload = dynProps.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_blocked_list.prc_get_char_string_list_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (StringHelper.isNullOrEmpty(negativeType))
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, negativeType);
			}

			if (StringHelper.isNullOrEmpty(negativeString))
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, negativeString);
			}
			
			cs.setString(++paramIndex, "Y");
			cs.setInt(++paramIndex, flag);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				NegativeTermBean bean = new NegativeTermBean();
				bean.setIdNumber(String.valueOf(rs.getInt("CHAR_STRNG_ID")));
				bean.setNegativeString(rs.getString("CHAR_STRNG_TEXT"));
				bean.setPurposeTypeCode(rs.getString("STRNG_PRPS_CODE"));
				bean.setCreateDate(
					df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				bean.setCreateUserId(rs.getString("LAST_UPDATE_USERID"));
				negativeTerms.add(bean);

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return negativeTerms;
	}

	public void setNegativeTerm(String userID, String negativeType, String negativeString)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_insrt_char_string_list";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, negativeType);
			cs.setString(++paramIndex, negativeString);
			cs.setString(++paramIndex, "Y");

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

/*
   PROCEDURE prc_del_char_string_list
   (iv_user_id         IN customer.cust_logon_id%TYPE,
    iv_call_type_code  IN process_log_a.call_type_code%TYPE,
    iv_char_strng_id   IN character_string_list.char_strng_id%TYPE,
    ov_prcs_log_id    OUT process_log_a.prcs_log_id%TYPE
   )
 */
	public void deleteNegativeTerm(String userID, String negativeNumber)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_del_char_string_list";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, Integer.parseInt(negativeNumber));

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}
}
