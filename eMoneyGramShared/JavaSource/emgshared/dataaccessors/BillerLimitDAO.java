/*
 * Created on Feb 23, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection; 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BillerLimit;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.StringHelper;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BillerLimitDAO extends AbstractOracleDAO
{
	public Collection getBillerLimits(String searchString, String filter)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection billerLimits = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		int maxDownload = dynProps.getMaxDownloadTransactions();

		StringBuffer sqlBuffer = new StringBuffer(64);
		String storedProcName = null;
		if (StringHelper.isNullOrEmpty(searchString))
		{
			storedProcName = "pkg_em_biller_and_country.prc_get_biller_cv";
			sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
		} else
		{
			storedProcName = "pkg_em_biller_and_country.prc_biller_search";
			sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");
		}

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			if (StringHelper.isNullOrEmpty(searchString))
			{
				cs.setNull(++paramIndex, Types.INTEGER);
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, searchString);
			}

			cs.setString(++paramIndex, filter);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
			rs = (ResultSet) cs.getObject(rsIndex);
			int cnt = 0;
			while (rs.next())
			{
				BillerLimit biller = new BillerLimit();
				biller.setId(rs.getInt("AGENT_ID") + "");
				biller.setLimitNbr(rs.getFloat("AGENT_RCV_LIM_AMT"));
				biller.setStatus(rs.getString("AGENT_STAT_CODE"));
				biller.setLagacyId(rs.getString("AGENT_LGCY_ID"));
				biller.setReceiveCode(rs.getString("RCV_AGCY_CODE"));
				biller.setName(rs.getString("AGENT_NAME"));
				biller.setCity(rs.getString("AGENT_CITY_NAME"));
				biller.setState(rs.getString("AGENT_STATEPROVINCE_NAME"));
				biller.setPaymentProfileId(rs.getString("MCC_PTNR_PRFL_ID"));
				billerLimits.add(biller);
				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException("Too many results and maximum download transaction is " + maxDownload);
				}
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return billerLimits;
	}

	public BillerLimit getBillerLimit(int agentId, String filter)
		throws DataSourceException, SQLException
	{
		return getBillerLimit("" + agentId, false, filter);
	}

	public BillerLimit getBillerLimit(String billerCode, String filter)
		throws DataSourceException, SQLException
	{
		return getBillerLimit(billerCode, true, filter);
	}

	private BillerLimit getBillerLimit(
		String id,
		boolean billerCode,
		String filter)
		throws DataSourceException, SQLException
	{
		BillerLimit billerLimit = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		StringBuffer sqlBuffer = new StringBuffer(64);
		String storedProcName = null;
		storedProcName = "pkg_em_biller_and_country.prc_get_biller_cv";
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters			
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			if (billerCode)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
				cs.setString(++paramIndex, id);
			} else
			{
				cs.setString(++paramIndex, id);
				cs.setNull(++paramIndex, Types.VARCHAR);
			}
			cs.setString(++paramIndex, filter);
			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);
			rs = (ResultSet) cs.getObject(rsIndex);

			//			NumberFormat nf = new DecimalFormat("#,##0.00");
			if (rs.next())
			{
				billerLimit = new BillerLimit();
				billerLimit.setId(rs.getInt("AGENT_ID") + "");
				billerLimit.setLimitNbr(rs.getFloat("AGENT_RCV_LIM_AMT"));
				billerLimit.setStatus(rs.getString("AGENT_STAT_CODE"));
				billerLimit.setLagacyId(rs.getString("AGENT_LGCY_ID"));
				billerLimit.setReceiveCode(rs.getString("RCV_AGCY_CODE"));
				billerLimit.setName(rs.getString("AGENT_NAME"));
				billerLimit.setCity(rs.getString("AGENT_CITY_NAME"));
				billerLimit.setState(rs.getString("AGENT_STATEPROVINCE_NAME"));
				billerLimit.setPaymentProfileId(rs.getString("MCC_PTNR_PRFL_ID"));
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return billerLimit;
	}

	public void setBillerLimit(BillerLimit billerLimit)
		throws DataSourceException, SQLException, ParseException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String storedProcName = "pkg_em_biller_and_country.prc_iu_biller";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?) }");
		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			//  input parameters			
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, Integer.parseInt(billerLimit.getId()));
			cs.setFloat(++paramIndex, billerLimit.getLimitNbr());
			cs.setString(++paramIndex, billerLimit.getStatus());
			cs.setString(++paramIndex, billerLimit.getReceiveCode());
			cs.setString(++paramIndex, billerLimit.getLagacyId());
			cs.setString(++paramIndex, billerLimit.getName());
			cs.setString(++paramIndex, billerLimit.getCity());
			cs.setString(++paramIndex, billerLimit.getState());
			cs.setString(++paramIndex, billerLimit.getPaymentProfileId());
			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic("dbLogId = " + dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void deleteBillerLimit(int id)
		throws DataSourceException, SQLException
	{
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_biller_and_country.prc_delete_biller";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters			
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, id);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return;
	}

}
