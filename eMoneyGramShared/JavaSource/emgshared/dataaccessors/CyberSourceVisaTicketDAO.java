/**
 * Copyright (c) 2004 MoneyGram Payment Systems, Inc. All rights reserved.
 * 
 * @author G.R.Svenddal Created on May 17, 2005
 * The table created for the VisaTicket uses the XID data field
 * as the primary key. I am not in favor of this choice, but it is so. 
 * Asked Fernando to write a getter proc that uses the foreign key TRAN_ID
 * as a value. Since our process currently assigns one and only one
 * VisaTicket per transaction, the getter should return one or zero
 * VisaTicket rows. Any more should throw. ( It would be possible for the
 * consumer to append more then one if they played with the browser 
 * back button, which we can block by keeping track of the appending in session.)
 * 
 * 
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.services.verifiedbyvisa.CyberSourceVisaTicket;
import emgshared.services.verifiedbyvisa.VBVLogger;

public class CyberSourceVisaTicketDAO extends AbstractOracleDAO
{

	private static final String PROC_INSERT =
		"pkg_em_transactions.prc_insert_tran_authentication";

	private static final String PROC_SELECT =
		"pkg_em_transactions.prc_get_tran_authentication_cv";

	public void insertTranAuthRecord(
		int transactionID,
		String xid,
		String commerceIndicator,
		String eciRaw,
		String cavv,
		String proofXml,
		String requestId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String logDetail = "";
		if (cavv != null && (cavv.equals("") == false))
			logDetail += " -isTicket ";
		if (proofXml != null && (proofXml.equals("") == false))
			logDetail += " -has proofXML";
		VBVLogger.info(
			"insert TranAuthRecord. tranId:" + transactionID + logDetail);

		try
		{
			conn = getConnection();
			cs =
				conn.prepareCall(
					"{ call " + PROC_INSERT + "(?,?,?,?,?,?,?,?,?,?,?,?) }");

			int paramIndex = 0;

			//iv_user_id  -  consumer side. Null dbUserId.
			cs.setNull(++paramIndex, Types.VARCHAR);

			// iv_call_type_code
			cs.setString(++paramIndex, dbCallTypeCode); // WEB

			// payer_authn_rqst_tran_id, what we call xid.
			// designed as primary key (!) -- this number comes from CyberSource
			// and is an encrypted, hashed id for our VBV tran. Better 
			// hope CyberSource is serious about uniqueness ;(
			cs.setString(++paramIndex, xid); // Which we call the XID.
			// iv_emg_tran_id: foreign key to Transaction
			// This should always be 1-1 and could easily be 
			// our primary key rather then the Xid data.
			// We will likely never have multiple VBV tickets per transaction.
			// What's the point of that? ( "I'm authentic". repeat n times ) 
			cs.setInt(++paramIndex, transactionID); // 
			// iv_authn_cmrc_code  - what we call the commerceIndicator.
			cs.setString(++paramIndex, commerceIndicator);
			// iv_eci_raw_code
			cs.setString(++paramIndex, eciRaw);
			// iv_vbv_cavv_tran_id
			cs.setString(++paramIndex, cavv); // the real meat. 

			// iv_prf_xml_text
			cs.setString(++paramIndex, proofXml); // at least we tried.
			// iv_cybersource_rqst_id
			cs.setString(++paramIndex, requestId);

			// iv_create_date - not usually included in an insert stordproc,
			// Fern sez set to null and the proc will set it as usual.
			cs.setNull(++paramIndex, Types.DATE);
			// iv_create_userid - same as above
			cs.setNull(++paramIndex, Types.VARCHAR);

			// ov_prcs_log_id - output. Used for log file marker to refer to this change. 
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(PROC_INSERT + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(PROC_INSERT + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
	}

	public CyberSourceVisaTicket getTicket(int transactionID)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		CyberSourceVisaTicket vvt = null;

		try
		{
			conn = getConnection();

			cs = conn.prepareCall("{ call " + PROC_SELECT + "(?,?,?,?,?) }");
			int paramIndex = 0;

			//  iv_user_id
			cs.setNull(++paramIndex, Types.VARCHAR);
			//  iv_call_type_code
			cs.setString(++paramIndex, dbCallTypeCode);
			//  iv_emg_tran_id
			cs.setInt(++paramIndex, transactionID);
			//  ov_prcs_log_id => :ov_prcs_log_id,
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;

			// ov_tran_authentication_cv - 
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(PROC_SELECT + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(PROC_SELECT + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				long dbLogId = cs.getLong(dbLogIdIndex);
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			if (rs.next())
			{
				// no cavv? no ticket for you! ( a record may include other things, for attemptsProcessing )
				String cavv = rs.getString("vbv_cavv_tran_id");
				if (cavv != null && (cavv.equals("") == false))
				{
					vvt =
						CyberSourceVisaTicket
							.createTicket(
								rs.getString("payer_authn_rqst_tran_id"),
						// xid
		cavv, // cavv
		rs.getString("authn_cmrc_code"), // commerceIndicator
		rs.getString("eci_raw_code"), //eciRaw
		rs.getString("prf_xml_text"), // proofXml
		rs.getString("cybersource_rqst_id") // requestId.  
	);
				}
			}
		} catch (SQLException e)
		{
			VBVLogger.error("DAO - getTicket. transactionId:" + transactionID);
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return vvt;
	}
}