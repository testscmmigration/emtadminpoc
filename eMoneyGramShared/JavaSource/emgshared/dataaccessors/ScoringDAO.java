package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import oracle.jdbc.driver.OracleTypes;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.model.ScoreValue;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionScoreCategory;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;

public class ScoringDAO extends emgshared.dataaccessors.AbstractOracleDAO
{
	private static Logger logger = LogFactory.getInstance().getLogger(ScoringDAO.class);

	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy hh:mm a", true);
	private static final NumberFormat nf = new DecimalFormat("#0.00");

	private final String ACTIVE_STATUS = "A";
	private final String INACTIVE_STATUS = "I";
	private final String DELETED_STATUS = "D";
	private final String INCLUDE_FLAG = "Y";
	private EMTSharedDynProperties esp = new EMTSharedDynProperties();

	public Collection getScoreConfigurationsList(String userID,String scoreConfigId,String tranType, String partnerSiteId)
			throws DataSourceException, SQLException, TooManyResultException
	{
		List scoreConfigurations = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = esp.getMaxDownloadTransactions();
		String storedProcName = "pkg_em_status_and_type.prc_get_score_cnfg_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			if (scoreConfigId == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setInt(++paramIndex, Integer.parseInt(scoreConfigId));
			if (tranType == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranType);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;
			logger.error("Before execute");
			cs.execute();
			logger.error("After execute");
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				logger.error("Building config " + cnt);
				ScoreConfiguration scoreConfiguration =	new ScoreConfiguration();
				scoreConfiguration.setConfigurationId(rs.getInt("SCORE_CNFG_ID"));
				scoreConfiguration.setTransactionType(rs.getString("EMG_TRAN_TYPE_CODE"));
				scoreConfiguration.setConfigurationVersionNumber(rs.getInt("SCORE_CNFG_VER_NBR"));
				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(ACTIVE_STATUS))
					scoreConfiguration.setStatus("Active");
				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(INACTIVE_STATUS))
					scoreConfiguration.setStatus("Inactive");
				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(DELETED_STATUS))
					scoreConfiguration.setStatus("Deleted");
				scoreConfiguration.setAcceptThreshold(rs.getInt("APRV_THRLD_QTY"));
				scoreConfiguration.setRejectThreshold(rs.getInt("REJ_THRLD_QTY"));
				scoreConfiguration.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				scoreConfiguration.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreConfiguration.setLastUpdateDate(df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				scoreConfiguration.setLastUpdateUserid(rs.getString("LAST_UPDATE_USERID"));
				scoreConfiguration.setPartnerSiteCode(rs.getInt("SRC_WEB_SITE_CODE"));
				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException("Too many results and maximum allowed is "	+ maxDownload);
				}
				scoreConfigurations.add(scoreConfiguration);
			}
		} catch (SQLException e)
		{
			logger.error("SQL Exception from DAO " + e.getMessage(), e);
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return scoreConfigurations;
	}


	public Collection getScoreConfigurations(
		String userID,
		String scoreConfigId,
		String tranType)
		throws DataSourceException, SQLException, TooManyResultException
	{
		List scoreConfigurations = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = esp.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_status_and_type.prc_get_score_cnfg_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (scoreConfigId == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setInt(++paramIndex, Integer.parseInt(scoreConfigId));

			if (tranType == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranType);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				ScoreConfiguration scoreConfiguration = new ScoreConfiguration();
				scoreConfiguration.setConfigurationId(rs.getInt("SCORE_CNFG_ID"));
				scoreConfiguration.setTransactionType(rs.getString("EMG_TRAN_TYPE_CODE"));
				scoreConfiguration.setConfigurationVersionNumber(rs.getInt("SCORE_CNFG_VER_NBR"));

				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(ACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Active");
				}
				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(INACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Inactive");
				}
				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(DELETED_STATUS)) {
					scoreConfiguration.setStatus("Deleted");
				}

				scoreConfiguration.setAcceptThreshold(rs.getInt("APRV_THRLD_QTY"));
				scoreConfiguration.setRejectThreshold(rs.getInt("REJ_THRLD_QTY"));
				scoreConfiguration.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				scoreConfiguration.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreConfiguration.setLastUpdateDate(df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				scoreConfiguration.setLastUpdateUserid(rs.getString("LAST_UPDATE_USERID"));
				scoreConfiguration.setPartnerSiteCode(rs.getInt("SRC_WEB_SITE_CODE"));

				int count = 0;

				for (Iterator it = getScoreCategories(userID, rs.getInt("SCORE_CNFG_ID"), null).values().iterator(); it.hasNext();) {
					ScoreCategory scoringCategory = (ScoreCategory) it.next();
					scoreConfiguration.addScoreCategory(scoringCategory);
					count++;
				}

				if (cnt++ > maxDownload) {
					throw new TooManyResultException("Too many results and maximum allowed is "	+ maxDownload);
				}
				scoreConfigurations.add(scoreConfiguration);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}

		return scoreConfigurations;
	}

	public Map getScoreCategories(
		String userID,
		int configurationId,
		String categoryCode)
		throws DataSourceException, SQLException, TooManyResultException
	{
		//		Collection scoreCategories = new ArrayList();
		Map scoreCategories = new HashMap();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = esp.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_status_and_type.prc_get_score_cnfg_category_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, configurationId);

			if (categoryCode == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, categoryCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				ScoreCategory scoreCategory = new ScoreCategory();
				scoreCategory.setScoreCategoryCode(
					rs.getString("SCORE_CAT_CODE"));
				scoreCategory.setScoreCategoryName(
					rs.getString("SCORE_CAT_NAME"));
				scoreCategory.setScoreCategoryDesc(
					rs.getString("SCORE_CAT_DESC"));
				scoreCategory.setListTypeCode(rs.getString("LIST_TYPE_CODE"));
				scoreCategory.setDataTypeCode(rs.getString("DATA_TYPE_CODE"));
				scoreCategory.setMaximumLengthQuantity(
					rs.getInt("MAX_LGTH_QTY"));

				if (rs.getString("INCL_FLAG").equals(INCLUDE_FLAG))
				{
					scoreCategory.setIncluded(true);
				} else
				{
					scoreCategory.setIncluded(false);
				}

				scoreCategory.setWeight(rs.getInt("CAT_WGT_NBR"));

				scoreCategory.setCreateDate(rs.getDate("CREATE_DATE"));
				scoreCategory.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreCategory.setLastUpdateDate(rs.getDate("LAST_UPDATE_DATE"));
				scoreCategory.setLastUpdateUserid(
					rs.getString("LAST_UPDATE_USERID"));

				int count = 0;
				for (Iterator it =
					getScoreValues(
						userID,
						configurationId,
						scoreCategory.getScoreCategoryCode(),
						scoreCategory.getListTypeCode())
						.iterator();
					it.hasNext();
					)
				{
					ScoreValue scoreValue = (ScoreValue) it.next();
					scoreCategory.addScoreValue(count, scoreValue);
					count++;
				}

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}

				//				scoreCategories.add(scoreCategory);
				scoreCategories.put(
					rs.getString("SCORE_CAT_NAME"),
					scoreCategory);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return scoreCategories;
	}

	public Collection getScoreValues(
		String userID,
		int configurationId,
		String categoryCode,
		String listTypeCode)
		throws DataSourceException, SQLException, TooManyResultException
	{
		ArrayList scoreValues = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = esp.getMaxDownloadTransactions();

		String storedProcName =
			"pkg_em_status_and_type.prc_get_score_cnfg_cat_val_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, configurationId);

			if (categoryCode == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, categoryCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			int previousRecordValue = -999;

			while (rs.next())
			{

				ScoreValue scoreValue = new ScoreValue();

				if (listTypeCode.equalsIgnoreCase("range"))
				{
					if (Integer.parseInt(rs.getString("SCORE_CAT_VAL")) == -1
						|| Integer.parseInt(rs.getString("SCORE_CAT_VAL")) == 0)
					{
						scoreValue.setMinimumValue(
							Integer.parseInt(rs.getString("SCORE_CAT_VAL")));
						scoreValue.setMaximumValue(
							Integer.parseInt(rs.getString("SCORE_CAT_VAL")));
					} else
					{
						if (previousRecordValue < 0)
							scoreValue.setMinimumValue(0);
						else
							scoreValue.setMinimumValue(previousRecordValue + 1);

						scoreValue.setMaximumValue(
							Integer.parseInt(rs.getString("SCORE_CAT_VAL")));
					}

					previousRecordValue =
						Integer.parseInt(rs.getString("SCORE_CAT_VAL"));

				} else
				{
					scoreValue.setValue(rs.getString("SCORE_CAT_VAL"));
				}

				scoreValue.setPoints(rs.getInt("VAL_PNT_QTY"));
				scoreValue.setCreateDate(rs.getDate("CREATE_DATE"));
				scoreValue.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreValue.setLastUpdateDate(rs.getDate("LAST_UPDATE_DATE"));
				scoreValue.setLastUpdateUserid(
					rs.getString("LAST_UPDATE_USERID"));

				if (cnt++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}

				scoreValues.add(scoreValue);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} catch (Exception ee)
		{
			throw new DataSourceException(ee);
		} finally
		{
			close(rs);
			close(cs);
		}

		return scoreValues;
	}

	public Collection getScoreCategories(String userID, String categoryCode)
		throws DataSourceException
	{
		Collection scoreCategories = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_status_and_type.prc_get_score_category_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (categoryCode == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, categoryCode);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				ScoreCategory scoreCategory = new ScoreCategory();
				scoreCategory.setScoreCategoryCode(
					rs.getString("SCORE_CAT_CODE"));
				scoreCategory.setScoreCategoryName(
					rs.getString("SCORE_CAT_NAME"));
				scoreCategory.setScoreCategoryDesc(
					rs.getString("SCORE_CAT_DESC"));
				scoreCategory.setListTypeCode(rs.getString("LIST_TYPE_CODE"));
				scoreCategory.setDataTypeCode(rs.getString("DATA_TYPE_CODE"));
				scoreCategory.setMaximumLengthQuantity(
					rs.getInt("MAX_LGTH_QTY"));

				scoreCategory.setCreateDate(rs.getDate("CREATE_DATE"));
				scoreCategory.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreCategory.setLastUpdateDate(rs.getDate("LAST_UPDATE_DATE"));
				scoreCategory.setLastUpdateUserid(
					rs.getString("LAST_UPDATE_USERID"));

				scoreCategories.add(scoreCategory);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return scoreCategories;
	}

	public int getBadTranCount(String userId, int custId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int cnt = 0;

		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_transactions.prc_get_returned_codes_cv";
			cs =
				conn.prepareCall("{ call " + storedProcName + "(?,?,?, ?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);

			while (rs.next())
			{
				cnt++;
			}
			logEndTime(dbLogId,cnt);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return cnt;
	}

	public java.util.Date getLastPasswordChangeDate(String userId, int custId)
		throws DataSourceException
	{
		java.util.Date date = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_comments.prc_get_last_pwd_change_date";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			if (rs.next())
			{
				date = rs.getTimestamp(1);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return date;
	}

	public java.util.Date getLastTransactionDate(
		String userId,
		int custId,
		int tranId)
		throws DataSourceException
	{
		java.util.Date date = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_last_tran_date_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);
			cs.setInt(++paramIndex, tranId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			if (rs.next())
			{
				date = rs.getTimestamp(1);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return date;
	}

	public Collection getActiveScoreConfigurations(
		String userID,
		String scoreConfigId,
		String tranType)
		throws DataSourceException, SQLException, TooManyResultException
	{
		Collection scoreConfigurations = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownload = esp.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_status_and_type.prc_get_active_score_cnfg_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (scoreConfigId == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setInt(++paramIndex, Integer.parseInt(scoreConfigId));

			if (tranType == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranType);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			int cnt = 0;
			while (rs.next())
			{
				ScoreConfiguration scoreConfiguration = new ScoreConfiguration();
				scoreConfiguration.setConfigurationId(rs.getInt("SCORE_CNFG_ID"));
				int partnerSiteCode = rs.getInt("SRC_WEB_SITE_CODE");
				scoreConfiguration.setTransactionType(rs.getString("EMG_TRAN_TYPE_CODE"));
				scoreConfiguration.setTransactionCodeLabel(partnerSiteCode == Constants.MGO_PARTNER_SITE_CODE
						? rs.getString("EMG_TRAN_TYPE_CODE") : rs.getString("EMG_TRAN_TYPE_CODE") + " (" + Constants.partnerSiteCodeToId.get(partnerSiteCode) +")");
				scoreConfiguration.setConfigurationVersionNumber(rs.getInt("SCORE_CNFG_VER_NBR"));

				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(ACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Active");
				} else if (rs.getString("SCORE_CNFG_STAT_CODE").equals(INACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Inactive");
				} else if (rs.getString("SCORE_CNFG_STAT_CODE").equals(DELETED_STATUS)) {
					scoreConfiguration.setStatus("Deleted");
				}

				scoreConfiguration.setAcceptThreshold(rs.getInt("APRV_THRLD_QTY"));
				scoreConfiguration.setRejectThreshold(rs.getInt("REJ_THRLD_QTY"));
				scoreConfiguration.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				scoreConfiguration.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreConfiguration.setLastUpdateDate(df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				scoreConfiguration.setLastUpdateUserid(rs.getString("LAST_UPDATE_USERID"));
				scoreConfiguration.setPartnerSiteCode(partnerSiteCode);

				int count = 0;
				for (Iterator it = getScoreCategories(userID, rs.getInt("SCORE_CNFG_ID"), null).values().iterator(); it.hasNext();)	{
					ScoreCategory scoringCategory = (ScoreCategory) it.next();
					scoreConfiguration.addScoreCategory(scoringCategory);
					count++;
				}

				if (cnt++ > maxDownload) {
					throw new TooManyResultException("Too many results and maximum allowed is " + maxDownload);
				}
				scoreConfigurations.add(scoreConfiguration);
			}
		} catch (SQLException e) {
			logger.error("SQL Exception from getActiveScoringConfigurations " + e.getMessage(), e);
			throw new DataSourceException(e);
		} finally {
			close(rs);
			close(cs);
		}
		return scoreConfigurations;
	}

	public int setScoreConfiguration(
		String userID,
		ScoreConfiguration scoreConfiguration)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int returnConfigId = 0;

		String storedProcName = "pkg_emg_type_stat_maint.prc_iu_score_cnfg";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (scoreConfiguration.getConfigurationId() == 0) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setInt(++paramIndex, scoreConfiguration.getConfigurationId());
			}

			cs.setString(++paramIndex, scoreConfiguration.getTransactionType());

			if (scoreConfiguration.getConfigurationId() == 0) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setInt(++paramIndex, scoreConfiguration.getConfigurationVersionNumber());
			}

			if (scoreConfiguration.getConfigurationId() == 0) {
				cs.setString(++paramIndex, INACTIVE_STATUS);
			} else {
				cs.setString(++paramIndex, scoreConfiguration.getStatus().substring(0, 1));
			}

			cs.setInt(++paramIndex, scoreConfiguration.getAcceptThreshold());
			cs.setInt(++paramIndex, scoreConfiguration.getRejectThreshold());
			cs.setInt(++paramIndex, scoreConfiguration.getPartnerSiteCode());

			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			cs.execute();

			int rsIndex = paramIndex;
			returnConfigId = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return returnConfigId;
	}

	public void insertAllCategories(String userID, int configId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_insert_score_cnfg_category";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, configId);
			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setScoreConfigurationCategory(
		String userID,
		int configId,
		ScoreCategory scoreCategory)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_update_score_cnfg_category";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, configId);
			cs.setString(++paramIndex, scoreCategory.getScoreCategoryCode());
			cs.setString(++paramIndex, scoreCategory.isIncluded() ? "Y" : "N");
			cs.setInt(++paramIndex, scoreCategory.getWeight());

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return;
	}

	public void setScoreValue(
		String userID,
		int configId,
		String catCode,
		ScoreValue scoreValue)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_iu_score_cnfg_category_val";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, configId);
			cs.setString(++paramIndex, catCode);

			if (scoreValue.getValue() != null)
				cs.setString(++paramIndex, scoreValue.getValue());
			else
				cs.setString(
					++paramIndex,
					String.valueOf(scoreValue.getMaximumValue()));

			cs.setInt(++paramIndex, scoreValue.getPoints());

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return;
	}

	public ScoreConfiguration getScoreConfigView(String userId, int configId)
		throws DataSourceException
	{
		ScoreConfiguration cnfg = null;
		ScoreCategory cat = null;
		ScoreValue value = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_status_and_type.prc_get_score_config_view_cv";
			cs =
				conn.prepareCall("{ call " + storedProcName + "(?,?,?, ?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, configId);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " started: " + System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);

			String saveCat = null;
			List catList = null;
			List valueList = null;
			long count = 0;
			while (rs.next())
			{
			    count++;
				if (cnfg == null)
				{
					cnfg = new ScoreConfiguration();
					cnfg.setConfigurationId(rs.getInt("score_cnfg_id"));
					cnfg.setConfigurationVersionNumber(
						rs.getInt("score_cnfg_ver_nbr"));
					cnfg.setStatus(rs.getString("score_cnfg_stat_code"));
					cnfg.setAcceptThreshold(rs.getInt("aprv_thrld_qty"));
					cnfg.setRejectThreshold(rs.getInt("rej_thrld_qty"));
					cnfg.setCreateDate(
						df.format(rs.getTimestamp("cnfg_create_date")));
					cnfg.setCreateUserid(rs.getString("cnfg_create_userid"));
					cnfg.setLastUpdateDate(
						df.format(rs.getTimestamp("cnfg_last_update_date")));
					cnfg.setLastUpdateUserid(
						rs.getString("cnfg_last_update_userid"));
					cnfg.setTransactionType(rs.getString("emg_tran_type_code"));
					cnfg.setEmgTranTypeDesc(rs.getString("emg_tran_type_desc"));
					cnfg.setScoreFlag(rs.getString("score_flag"));
					cnfg.setAutoAprvFlag(rs.getString("auto_aprv_flag"));
					cnfg.setTypeLastUpdateDate(
						df.format(rs.getTimestamp("type_last_update_date")));
					cnfg.setTypeLastUpdateUserid(
						rs.getString("type_last_update_userid"));
					catList = new ArrayList();
				}

				if (saveCat == null
					|| !saveCat.equals(rs.getString("score_cat_code")))
				{
					if (saveCat != null)
					{
						cat.setScoreValues(valueList);
						catList.add(cat);
					}

					saveCat = rs.getString("score_cat_code");
					cat = new ScoreCategory();
					cat.setScoreConfigId(rs.getInt("score_cnfg_id"));
					cat.setScoreCategoryCode(saveCat);
					cat.setScoreCategoryName(rs.getString("score_cat_name"));
					cat.setScoreCategoryDesc(rs.getString("score_cat_desc"));
					cat.setListTypeCode(rs.getString("list_type_code"));
					cat.setDataTypeCode(rs.getString("data_type_code"));
					cat.setMaximumLengthQuantity(rs.getInt("max_lgth_qty"));
					cat.setCatCreateDate(
						df.format(rs.getTimestamp("cat_create_date")));
					cat.setCatCreateUserid(rs.getString("cat_create_userid"));
					cat.setCatLastUpdateDate(
						df.format(rs.getTimestamp("cat_last_update_date")));
					cat.setCatLastUpdateUserid(
						rs.getString("cat_last_update_userid"));
					cat.setInclFlag(rs.getString("incl_flag"));
					cat.setWeight(rs.getInt("cat_wgt_nbr"));
					cat.setCatCnfgCreateDate(
						df.format(rs.getTimestamp("cat_cnfg_create_date")));
					cat.setCreateUserid(rs.getString("cat_cnfg_create_userid"));
					cat.setCatCnfgLastUpdateDate(
						df.format(
							rs.getTimestamp("cat_cnfg_last_update_date")));
					cat.setLastUpdateUserid(
						rs.getString("cat_cnfg_last_update_userid"));
					valueList = new ArrayList();
				}

				value = new ScoreValue();
				value.setScoreConfigId(rs.getInt("score_cnfg_id"));
				value.setScoreCatCode(rs.getString("score_cat_code"));
				value.setValue(rs.getString("score_cat_val"));
				value.setPoints(rs.getInt("val_pnt_qty"));
				value.setValCreateDate(
					df.format(rs.getTimestamp("val_create_date")));
				value.setCreateUserid(rs.getString("val_create_userid"));
				value.setValLastUpdateDate(
					df.format(rs.getTimestamp("val_last_update_date")));
				value.setLastUpdateUserid(
					rs.getString("val_last_update_userid"));
				valueList.add(value);
			}

			if (saveCat != null)
			{
				cat.setScoreValues(valueList);
				catList.add(cat);

				Iterator iter = catList.iterator();
				int totalWeight = 0;
				while (iter.hasNext())
				{
					ScoreCategory sc = (ScoreCategory) iter.next();
					if ("Y".equalsIgnoreCase(sc.getInclFlag()))
					{
						totalWeight += sc.getWeight();
					}
				}
				cnfg.setTotalWeight(totalWeight);

				iter = catList.iterator();
				while (iter.hasNext())
				{
					ScoreCategory sc = (ScoreCategory) iter.next();
					if ("Y".equalsIgnoreCase(sc.getInclFlag()))
					{
						sc.setWeightPercentage(
							nf.format(
								Math.round(
									sc.getWeight() * 10000F / totalWeight)
									/ 100F)
								+ " %");
					} else
					{
						sc.setWeightPercentage("n/a");
					}
				}
				cnfg.setCategoryList(catList);
			}
			logEndTime(dbLogId,count);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return cnfg;
	}

	public void setEmgTranScore(String userID, TransactionScore tranScore)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_iu_emg_tran_score";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			if (userID == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, userID);

			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, tranScore.getEmgTranId());
			cs.setInt(++paramIndex, tranScore.getScoreCnfgId());
			cs.setInt(++paramIndex, tranScore.getTranScoreNbr());
			cs.setString(++paramIndex, tranScore.getSysAutoRsltCode());

			if (tranScore.getScoreRvwCode() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranScore.getScoreRvwCode());

			if (tranScore.getTranScoreCmntText() == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranScore.getTranScoreCmntText());

			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return;
	}

	public void setEmgTranScoreCategory(
		String userID,
		TransactionScoreCategory tranScoreCategory)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_transactions.prc_iu_emg_tran_score_category";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append(
			"{ call " + storedProcName + "(?,?, ?,?,?,?,?,?, ?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			if (userID == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, userID);

			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, tranScoreCategory.getEmgTranId());
			cs.setInt(++paramIndex, tranScoreCategory.getScoreCnfgId());
			cs.setString(++paramIndex, tranScoreCategory.getScoreCatCode());
			cs.setInt(++paramIndex, tranScoreCategory.getRawScoreNbr());
			cs.setInt(++paramIndex, tranScoreCategory.getWgtScoreNbr());
			cs.setString(
				++paramIndex,
				tranScoreCategory.getScoreQueryRsltData());

			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return;
	}

	public void UpdateScoreConfigStatus(
		String userID,
		int scoreConfigId,
		String tranType,
		String scoreConfigStatus,
		String parentSiteId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_updt_score_cnfg_stat_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, scoreConfigId);
			cs.setString(++paramIndex, tranType);
			cs.setString(++paramIndex, scoreConfigStatus);
			cs.setString(++paramIndex, parentSiteId);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return;
	}

	public void removeScoreValue(String userID, int configId, String catCode)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_emg_type_stat_maint.prc_del_score_cnfg_cat_val";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			//  input parameters

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, configId);
			cs.setString(++paramIndex, catCode);

			cs.execute();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return;
	}

	public Collection getAllScoreConfigurations(String userID, String tranType, String partnerSiteId)
		throws DataSourceException, SQLException, TooManyResultException
	{
		List scoreConfigurations = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int count = 1;
		int maxDownload = esp.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_status_and_type.prc_get_score_cnfg_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setNull(++paramIndex, Types.VARCHAR);

			if (tranType == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, tranType);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			while (rs.next())
			{
				ScoreConfiguration scoreConfiguration = new ScoreConfiguration();
				scoreConfiguration.setConfigurationId(rs.getInt("SCORE_CNFG_ID"));
				scoreConfiguration.setTransactionType(rs.getString("EMG_TRAN_TYPE_CODE"));
				scoreConfiguration.setConfigurationVersionNumber(rs.getInt("SCORE_CNFG_VER_NBR"));

				if (rs.getString("SCORE_CNFG_STAT_CODE").equals(ACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Active");
				} else if (rs.getString("SCORE_CNFG_STAT_CODE").equals(INACTIVE_STATUS)) {
					scoreConfiguration.setStatus("Inactive");
				} else if (rs.getString("SCORE_CNFG_STAT_CODE").equals(DELETED_STATUS)) {
					scoreConfiguration.setStatus("Deleted");
				}

				scoreConfiguration.setAcceptThreshold(rs.getInt("APRV_THRLD_QTY"));
				scoreConfiguration.setRejectThreshold(rs.getInt("REJ_THRLD_QTY"));
				scoreConfiguration.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				scoreConfiguration.setCreateUserid(rs.getString("CREATE_USERID"));
				scoreConfiguration.setLastUpdateDate(df.format(rs.getTimestamp("LAST_UPDATE_DATE")));
				scoreConfiguration.setLastUpdateUserid(rs.getString("LAST_UPDATE_USERID"));
				int partnerSiteCode = rs.getInt("SRC_WEB_SITE_CODE");
				scoreConfiguration.setPartnerSiteCode(partnerSiteCode);
				if (partnerSiteCode == Constants.MGO_PARTNER_SITE_CODE) {
					scoreConfiguration.setTransactionCodeLabel(scoreConfiguration.getTransactionType());
				} else {
					String parentSiteId = (String)Constants.partnerSiteCodeToId.get(partnerSiteCode);
					scoreConfiguration.setTransactionCodeLabel(scoreConfiguration.getTransactionType() + " (" + parentSiteId + ")");
				}

				if (count++ > maxDownload)
				{
					throw new TooManyResultException(
						"Too many results and maximum allowed is "
							+ maxDownload);
				}
				scoreConfigurations.add(scoreConfiguration);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return scoreConfigurations;
	}

	public int getBlockedEmailMatch(String userId, int tranId)
		throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_get_blocked_email_match";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();

			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}

	public int getSeniorCitizenMatch(String userId, int tranId)
		throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_get_sr_citizen_match";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();

			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}

	public int getReceiveStateMatch(String userId, int tranId)
		throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_rcv_state_matches_profile";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();

			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}

	public int getMaxSendLimitExceeded(String userId, int tranId)
		throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_max_send_limit_exceeded";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();

			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}

	public int getAreaStateMatch(String userId, String countryCode,
							String areaCode, String stateName)
	throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_get_areacode_state_match";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, countryCode);
			cs.setString(++paramIndex, areaCode);
			cs.setString(++paramIndex, stateName);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();

			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}

	public String getStateForAreaCode(String userId, String countryCode,
            String areaCode) throws DataSourceException {
        CallableStatement cs = null;
        Connection conn = null;
        ResultSet rs = null;
        String stateCode = null;
        String storedProcName = "pkg_em_cust_account_maint.prc_get_phone_area_code_cv";

        StringBuffer sqlBuffer = new StringBuffer(64);
        sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?, ?,?) }");

        try {
            if ( (countryCode == null) || (areaCode == null) )
                throw new DataSourceException("Procedure getStateForAreaCode requires Country Code and Area Code");
            if ( (countryCode.length() != 3) || (areaCode.length() != 3) )
                throw new DataSourceException("Procedure getStateForAreaCode requires Country Code and Area Code, each must be 3 chars long");
            conn = getConnection();
            cs = conn.prepareCall(sqlBuffer.toString());

            int paramIndex = 0;

            cs.setString(++paramIndex, userId);
            cs.setString(++paramIndex, dbCallTypeCode);
            cs.setString(++paramIndex, countryCode);
            cs.setString(++paramIndex, areaCode);

            //  output parameters
            cs.registerOutParameter(++paramIndex, Types.INTEGER);
            cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
            int rsIndex = paramIndex;

            cs.execute();

        	rs = (ResultSet) cs.getObject(rsIndex);

        	if (rs.next())
			    stateCode = (String)rs.getString("STATEPROVINCE_NAME");
			else
			    stateCode = "";
		        } catch (SQLException e) {
            throw new DataSourceException(e);
        } finally {
            close(cs);
            close(rs);
        }
        return stateCode;
    }


	public int getZipStateMatch(String userId, String countryCode,
			String postalCode, String stateName)
	throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName = "pkg_em_blocked_list.prc_get_postal_match";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");

		try
		{
		conn = getConnection();
		cs = conn.prepareCall(sqlBuffer.toString());

		int paramIndex = 0;

		cs.setString(++paramIndex, userId);
		cs.setString(++paramIndex, dbCallTypeCode);
		cs.setString(++paramIndex, postalCode);
		cs.setString(++paramIndex, countryCode);
		cs.setString(++paramIndex, stateName);

		//  output parameters
		cs.registerOutParameter(++paramIndex, Types.INTEGER);
		cs.registerOutParameter(++paramIndex, Types.INTEGER);
		int rsIndex = paramIndex;

		cs.execute();

		returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
		throw new DataSourceException(e);
		} finally
		{
		close(cs);
		}

		return returnValue;
	}

	public int getNegativeAddressMatch(
			String userID,
			String negativeString)
		throws DataSourceException
	{
		ResultSet rs = null;
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =
			"pkg_em_blocked_list.prc_get_char_string_list_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, "ADD");
			cs.setString(++paramIndex, negativeString);
			cs.setString(++paramIndex, "Y");
			cs.setInt(++paramIndex, 1);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(rsIndex);

			if (rs.next())
				returnValue = 1;

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return returnValue;
	}

	public int getEmailKeywordsMatch(
			String userID,
			String email)
		throws DataSourceException
	{
		int returnValue = 0;
		CallableStatement cs = null;
		Connection conn = null;

		String storedProcName =	"pkg_em_blocked_list.prc_get_negative_word_match";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try
		{
			conn = getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			cs.setString(++paramIndex, userID);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, email);

			//  output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();
			returnValue = cs.getInt(rsIndex);

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}

		return returnValue;
	}
}
