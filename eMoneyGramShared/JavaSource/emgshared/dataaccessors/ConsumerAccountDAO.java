/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.AccountSearchRequest;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.CustomerProfileAccount;
import emgshared.model.NewConsumerBankAccount;
import emgshared.model.NewConsumerCreditCardAccount;
import emgshared.model.TaintIndicatorType;
import emgshared.util.StringHelper;
/**
 * @author A131
 *
 */
public class ConsumerAccountDAO extends AbstractOracleDAO
{
	/**
	 * @param newAccount
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int insertCreditCardAccount(NewConsumerCreditCardAccount newAccount)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;

		try
		{
			conn = getConnection();
			// PCI Project - added so card acct insert wouldn't commit until 'commit' actually called
			// Autocommit should probably be set in AbstractOracleDAO, but that would affect all DAO
			// calls which is a bit scary.
			conn.setAutoCommit(false);  
			String storedProcName =
				"pkg_em_cust_account_maint.prc_iu_cust_account";
			cs =
				conn.prepareCall(
					"{ call "
						+ storedProcName
						+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

			int paramIndex = 0;
			cs.setInt(++paramIndex, newAccount.getConsumerId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.INTEGER); // acct id
			cs.setNull(++paramIndex, Types.INTEGER); // version number
			cs.setInt(++paramIndex, newAccount.getConsumerId());
			cs.setInt(++paramIndex, newAccount.getAddressId());
			cs.setString(++paramIndex, newAccount.getAccountType().getCode());
			cs.setString(++paramIndex, newAccount.getStatusCode());
			cs.setString(++paramIndex, newAccount.getSubStatusCode());
			cs.setNull(++paramIndex, Types.VARCHAR); // MAS - PCI acct number
			cs.setString(++paramIndex, newAccount.getAccountNumberMask());
			cs.setString(++paramIndex, newAccount.getAccountHolderFullName());
			cs.setNull(++paramIndex, Types.VARCHAR); //acct name
			cs.setNull(++paramIndex, Types.VARCHAR); //bank aba nbr
			cs.setNull(++paramIndex, Types.VARCHAR);
			//financial institution name
			cs.setString(++paramIndex, newAccount.getAccountNumberBinHash());
			cs.setInt(++paramIndex, newAccount.getExpireMonth());
			cs.setInt(++paramIndex, newAccount.getExpireYear());			
			cs.setString(
				++paramIndex,
				newAccount.getAccountNumberTaintCode().toString());
			cs.setString(
				++paramIndex,
				newAccount.getBankAbaTaintCode().toString());
			cs.setString(
				++paramIndex,
				newAccount.getCreditCardBinTaintCode().toString());
			String tmpCode = newAccount.getCreditOrDebitCode();
			tmpCode =
				tmpCode == null
					? ConsumerCreditCardAccount.UNKNOWN_CODE
					: tmpCode;
			// crcd type code
			cs.setString(++paramIndex, tmpCode);
			cs.setString(++paramIndex, newAccount.getAccountNumberHash());
			cs.setString(++paramIndex, newAccount.getAccountNumberBin());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return newId;
	}

	/**
	 * @param accountId
	 * @param consumerId - may be null
	 * @param callerUserId - may be null
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		Integer consumerId,
		String callerUserId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ConsumerCreditCardAccount account = null;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_account";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerUserId);
			cs.setString(++paramIndex, "WEB");
			if (consumerId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, consumerId.intValue());
			}
			cs.setInt(++paramIndex, accountId);
			cs.setNull(++paramIndex, Types.INTEGER); //	version number
			cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code
			cs.setNull(++paramIndex, Types.VARCHAR); //	stat code
			cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			Set accounts = populateAccounts(rs);
			//TODO what if more than one account returned?
			if (accounts.size() > 0)
			{
				account =
					(ConsumerCreditCardAccount) accounts.iterator().next();
			}
			long count = accounts.size();
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return account;
	}

	/**
	 * @param accountId
	 * @param consumerId - may be null
	 * @param callerUserId - may be null
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getEncryptedAccountNumber(int accountId, String callerUserId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		String encryptedNumber = null;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_account";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerUserId);
			cs.setString(++paramIndex, "WEB");
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setInt(++paramIndex, accountId);
			cs.setNull(++paramIndex, Types.INTEGER); //	version number
			cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code
			cs.setNull(++paramIndex, Types.VARCHAR); //	stat code
			cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			if (rs.next())
			{
				encryptedNumber = rs.getString("ACCT_ENCRYP_NBR");
			}
			logEndTime(dbLogId,1);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return encryptedNumber;
	}

	/**
	 * @param newAccount
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public int insertBankAccount(NewConsumerBankAccount newAccount)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_iu_cust_account";
			cs =
				conn.prepareCall(
					"{ call "
						+ storedProcName
						+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

			int paramIndex = 0;
			cs.setInt(++paramIndex, newAccount.getConsumerId());
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setNull(++paramIndex, Types.INTEGER); // acct id
			cs.setNull(++paramIndex, Types.INTEGER); // version number
			cs.setInt(++paramIndex, newAccount.getConsumerId());
			cs.setInt(++paramIndex, newAccount.getAddressId());
			cs.setString(++paramIndex, newAccount.getAccountType().getCode());
			cs.setString(++paramIndex, newAccount.getStatusCode());
			cs.setString(++paramIndex, newAccount.getSubStatusCode());
			cs.setString(++paramIndex, newAccount.getAccountNumber());
			cs.setString(++paramIndex, newAccount.getAccountNumberMask());
			cs.setNull(++paramIndex, Types.VARCHAR); //acct holder full name
			cs.setNull(++paramIndex, Types.VARCHAR); //acct name?
			cs.setString(++paramIndex, newAccount.getABANumber());
			cs.setString(
				++paramIndex,
				newAccount.getFinancialInstitutionName());
			cs.setNull(++paramIndex, Types.VARCHAR);
			// credit card bin hash text
			cs.setNull(++paramIndex, Types.INTEGER); // expire month
			cs.setNull(++paramIndex, Types.INTEGER); // expire year			
			cs.setString(
				++paramIndex,
				newAccount.getAccountNumberTaintCode().toString());
			cs.setString(
				++paramIndex,
				newAccount.getBankAbaTaintCode().toString());
			cs.setString(
				++paramIndex,
				newAccount.getCreditCardBinTaintCode().toString());
			cs.setNull(++paramIndex, Types.VARCHAR); // crcd type code
			cs.setNull(++paramIndex, Types.VARCHAR); // account number hash, used for cc only
			cs.setNull(++paramIndex, Types.VARCHAR); // account number bin, used for cc only

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return newId;
	}

	/**
	 * @param accountId
	 * @param consumerId - may be null
	 * @param callerUserId - may be null
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public ConsumerBankAccount getBankAccount(
		int accountId,
		Integer consumerId,
		String callerUserId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		ConsumerBankAccount account = null;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_account";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerUserId);
			cs.setString(++paramIndex, "WEB");
			if (consumerId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, consumerId.intValue());
			}
			cs.setInt(++paramIndex, accountId);
			cs.setNull(++paramIndex, Types.INTEGER); //	version number
			cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code
			cs.setNull(++paramIndex, Types.VARCHAR); //	stat code
			cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			Set accounts = populateAccounts(rs);
			//TODO what if more than one account returned?
			if (accounts.size() > 0)
			{
				account = (ConsumerBankAccount) accounts.iterator().next();
			}
			long count = accounts.size();
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return account;
	}

	/**
	 * @param consumerId
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
		public Set getAccounts(
			AccountSearchRequest searchCriteria,
			String callerUserId)
			throws DataSourceException
		{
			ResultSet rs = null;
			CallableStatement cs = null;
			Connection conn = null;
			Set accounts = new HashSet(0);
	
			if (searchCriteria == null)
			{
				searchCriteria = new AccountSearchRequest();
			}
	
			try
			{
				conn = getConnection();
	
				String storedProcName =
					"pkg_em_cust_account_maint.prc_get_cust_account";
				cs =
					conn.prepareCall(
						"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
				int paramIndex = 0;
	
				cs.setString(++paramIndex, callerUserId);
				cs.setString(++paramIndex, "WEB");
	
				Integer custId = searchCriteria.getCustId();
				if (custId == null)
				{
					cs.setNull(++paramIndex, Types.INTEGER);
				} else
				{
					cs.setInt(++paramIndex, custId.intValue());
				}
	
				cs.setNull(++paramIndex, Types.INTEGER); // acct id
				cs.setNull(++paramIndex, Types.INTEGER); //	version number
				cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code
	
				String statusCode = searchCriteria.getStatusCode();
				if (statusCode == null)
				{
					cs.setNull(++paramIndex, Types.VARCHAR);
				} else
				{
					cs.setString(++paramIndex, statusCode);
				}
	
				cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code
	
				cs.registerOutParameter(++paramIndex, Types.BIGINT);
				int dbLogIdIndex = paramIndex;
				cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
				int cursorIndex = paramIndex;
	
				if (EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled())
				{
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
				}
	
				cs.execute();
				long dbLogId = cs.getLong(dbLogIdIndex);
				if (EMGSharedLogger
					.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled())
				{
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
					
					EMGSharedLogger.getLogger(
						this.getClass().getName().toString()).diagnostic(
						"dbLogId = " + dbLogId);
				}
	
				rs = (ResultSet) cs.getObject(cursorIndex);
				accounts = populateAccounts(rs);
				long count = accounts.size();
				logEndTime(dbLogId,count);
			} catch (SQLException e)
			{
				org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(e);
				System.out.println(org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(e));
				throw new DataSourceException(e);
				
			} finally
			{
				close(rs);
				close(cs);
			}
			return accounts;
		}
	/**
		 * @param consumerId
		 * @return
		 *
		 * Created on Jan 20, 2005
		 */
	public Set getAccount(
		AccountSearchRequest searchCriteria,
		int accountId,
		String callerUserId)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Set accounts = new HashSet(0);

		if (searchCriteria == null)
		{
			searchCriteria = new AccountSearchRequest();
		}

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_account";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerUserId);
			cs.setString(++paramIndex, "WEB");

			Integer custId = searchCriteria.getCustId();
			if (custId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, custId.intValue());
			}
			cs.setInt(++paramIndex, accountId); // acct id
			cs.setNull(++paramIndex, Types.INTEGER); //	version number
			cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code

			String statusCode = searchCriteria.getStatusCode();
			if (statusCode == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, statusCode);
			}

			cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			rs = (ResultSet) cs.getObject(cursorIndex);
			accounts = populateAccounts(rs);
			long count = accounts.size();
			logEndTime(dbLogId,count);			
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return accounts;
	}

	private Set populateAccounts(
		ResultSet rs)
		throws SQLException
	{
		Set accounts = new HashSet();
		if (rs != null)
		{
			while (rs.next())
			{
				int consumerId = rs.getInt("cust_id");
				ConsumerAddress billingAddress =
					new ConsumerAddress(rs.getInt("addr_id"), consumerId);

				ConsumerAccountType type =
					ConsumerAccountType.getInstance(
						rs.getString("acct_type_code"));

				AccountStatus status =
					AccountStatus.getInstance(
						rs.getString("acct_stat_code"),
						rs.getString("acct_sub_stat_code"));

				if (type.isCardAccount())
				{
					ConsumerCreditCardAccount account =
						new ConsumerCreditCardAccount(
							rs.getInt("cust_acct_id"),
							consumerId,
							status,
							type,
							rs.getString("acct_type_desc"),
							rs.getString("acct_mask_nbr"),
							rs.getString("CRCD_BIN_MASK_NBR"),
							rs.getString("crcd_bin_hash_text"),
							null,
							billingAddress,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED);
					account.setAccountHolderFullName(
						rs.getString("acct_holdr_name"));
					account.setExpireMonth(rs.getInt("crcd_exp_mth_nbr"));
					account.setExpireYear(rs.getInt("crcd_exp_yr_nbr"));
					account.setVersionNumber(rs.getInt("cust_acct_ver_nbr"));
					String tmpCode = rs.getString("crcd_type_code");
					tmpCode =
						tmpCode == null
							? ConsumerCreditCardAccount.UNKNOWN_CODE
							: tmpCode;
					account.setCreditOrDebitCode(tmpCode);
					// Check Taint Indicator.
					if (account.getAccountNumberTaintCode() != null
						&& rs.getString("acct_blkd_code") != null)
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.getInstance(
								rs.getString("acct_blkd_code").toUpperCase()));
					} else
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getBankAbaTaintCode() != null
						&& rs.getString("bank_aba_blkd_code") != null)
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("bank_aba_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getCreditCardBinTaintCode() != null
						&& rs.getString("crcd_bin_blkd_code") != null)
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("crcd_bin_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					accounts.add(account);
				} else if (type.isBankAccount())
				{
					ConsumerBankAccount account =
						new ConsumerBankAccount(
							rs.getInt("cust_acct_id"),
							consumerId,
							status,
							type,
							rs.getString("acct_type_desc"),
							rs.getString("acct_mask_nbr"),
							billingAddress,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED);
					account.setABANumber(rs.getString("bank_aba_nbr"));
					account.setEncryptedText(rs.getString("acct_encryp_nbr"));
					account.setFinancialInstitutionName(
						rs.getString("fin_instn_name"));
					account.setName(rs.getString("acct_name"));
					account.setVersionNumber(rs.getInt("cust_acct_ver_nbr"));
					// Check Taint Indicator.
					if (account.getAccountNumberTaintCode() != null
						&& rs.getString("acct_blkd_code") != null)
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.getInstance(
								rs.getString("acct_blkd_code").toUpperCase()));
					} else
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getBankAbaTaintCode() != null
						&& rs.getString("bank_aba_blkd_code") != null)
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("bank_aba_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getCreditCardBinTaintCode() != null
						&& rs.getString("crcd_bin_blkd_code") != null)
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("crcd_bin_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					accounts.add(account);
				}
			}
		}
		return accounts;
	}

	public Map getAccountTypeDescriptions() throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map descriptions = new HashMap();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_status_and_type.prc_get_cust_account_type_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, "WEB");
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				descriptions.put(
					rs.getString("acct_type_code"),
					rs.getString("acct_type_desc"));
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return descriptions;
	}

	public ConsumerBankAccount updateBankAccount(ConsumerBankAccount account)
		throws DataSourceException
	{
		ConsumerBankAccount updatedAccount = null;
		CallableStatement cs = null;
		Connection conn = null;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_iu_cust_account";
			cs =
				conn.prepareCall(
					"{ call "
						+ storedProcName
						+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, String.valueOf(account.getConsumerId()));
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, account.getId());
			cs.setInt(++paramIndex, account.getVersionNumber());
			cs.setInt(++paramIndex, account.getConsumerId());
			cs.setString(++paramIndex, account.getAccountType().getCode());
			cs.setString(++paramIndex, account.getStatusCode());
			cs.setString(++paramIndex, account.getSubStatusCode());
			cs.setNull(++paramIndex, Types.VARCHAR); // account number
			cs.setString(++paramIndex, account.getAccountNumberMask());
			cs.setNull(++paramIndex, Types.VARCHAR); //acct holder full name
			cs.setNull(++paramIndex, Types.VARCHAR); //acct name?
			cs.setString(++paramIndex, account.getABANumber());
			cs.setString(++paramIndex, account.getFinancialInstitutionName());
			cs.setNull(++paramIndex, Types.VARCHAR);  // account number bin hash
			cs.setNull(++paramIndex, Types.INTEGER); // expire month
			cs.setNull(++paramIndex, Types.INTEGER); // expire year			
			cs.setString(
				++paramIndex,
				account.getAccountNumberTaintCode().toString());
			cs.setString(
				++paramIndex,
				account.getBankAbaTaintCode().toString());
			cs.setString(
				++paramIndex,
				account.getCreditCardBinTaintCode().toString());
			cs.setNull(++paramIndex, Types.VARCHAR); // crcd type code
	        cs.setNull(++paramIndex, Types.BIGINT);     // iv_blkd_crcd_acct_id
			cs.setNull(++paramIndex, Types.VARCHAR); // account number bin, used for cc only

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newAccountIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			Integer newAccountId = ((Integer) cs.getObject(newAccountIdIndex));
			updatedAccount = getBankAccount((newAccountId == null ? account.getId(): newAccountId.intValue()), null, String.valueOf(account.getConsumerId()));
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return updatedAccount;
	}


   public ConsumerCreditCardAccount updateCreditCardAccount(
       ConsumerCreditCardAccount account) throws DataSourceException
   {
       ConsumerCreditCardAccount updatedAccount = null;
       CallableStatement cs = null;
       Connection conn = null;
       try {
           conn = getConnection();
           String storedProcName =
               "pkg_em_cust_account_maint.prc_iu_cust_account";
           cs = conn.prepareCall(
               "{ call " + storedProcName +
               "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

           /*
            * The stored procedure pkg_em_cust_account_maint.prc_iu_cust_account
            * only updates the account status and account substatus. Any other
            * parameters values are ignored. Of course we need the consumer id,
            * account id in order for the stored proc to find the account
            * record. Provided values may also be useful for debugging purposes.
            */

           int paramIndex = 0;
           cs.setString(++paramIndex, String.valueOf(account.getConsumerId()));
           cs.setString(++paramIndex, dbCallTypeCode);
           cs.setInt(++paramIndex, account.getId());
           cs.setInt(++paramIndex, account.getVersionNumber());  // iv_cust_acct_ver_nbr
           cs.setInt(++paramIndex, account.getConsumerId());
           cs.setString(++paramIndex, account.getAccountType().getCode());
           cs.setString(++paramIndex, account.getStatusCode());
           cs.setString(++paramIndex, account.getSubStatusCode());
           cs.setNull(++paramIndex, Types.VARCHAR); // account number
           cs.setString(++paramIndex, account.getAccountNumberMask());
           cs.setString(++paramIndex, account.getAccountHolderFullName()); // account holder full name
           cs.setNull(++paramIndex, Types.VARCHAR); // account name
           cs.setNull(++paramIndex, Types.VARCHAR); // aba number
           cs.setNull(++paramIndex, Types.VARCHAR); // financial institution name
           cs.setString(++paramIndex, account.getAccountNumberBinHash()); // account number bin hash
           cs.setInt(++paramIndex, account.getExpireMonth());
           cs.setInt(++paramIndex, account.getExpireYear());
           cs.setString(
               ++paramIndex,
               account.getAccountNumberTaintCode().toString());
           cs.setString(
               ++paramIndex,
               account.getBankAbaTaintCode().toString());
           cs.setString(
                ++paramIndex,
                account.getCreditCardBinTaintCode().toString());
           if (account.getCreditOrDebitCode() == null)
           {
               cs.setNull(++paramIndex, Types.VARCHAR);
           } else
           {
               cs.setString(++paramIndex, account.getCreditOrDebitCode());
           }
           cs.setNull(++paramIndex, Types.BIGINT);     // iv_blkd_crcd_acct_id
           cs.setNull(++paramIndex, Types.VARCHAR);    // iv_crcd_bin_mask_nbr
           cs.registerOutParameter(++paramIndex, Types.BIGINT);
           int dbLogIdIndex = paramIndex;
           cs.registerOutParameter(++paramIndex, Types.INTEGER);
           int newAccountIdIndex = paramIndex;
           cs.registerOutParameter(++paramIndex, Types.INTEGER);
           if (EMGSharedLogger
                   .getLogger(this.getClass().getName().toString())
                   .isDiagnosticEnabled())
           {
                   EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
           }

           cs.execute();
           long dbLogId = cs.getLong(dbLogIdIndex);
           if (EMGSharedLogger
               .getLogger(this.getClass().getName().toString())
               .isDiagnosticEnabled())
           {
               EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
               EMGSharedLogger.getLogger(
                   this.getClass().getName().toString()).diagnostic(
                   "dbLogId = " + dbLogId);
           }

           Integer newAccountId = ((Integer) cs.getObject(newAccountIdIndex));
           updatedAccount =
               getCreditCardAccount(
                   (newAccountId == null
                       ? account.getId()
                       : newAccountId.intValue()),
                   null,
                   String.valueOf(account.getConsumerId()));
           logEndTime(dbLogId);
       } catch (SQLException e)
       {
           throw new DataSourceException(e);
       } finally
       {
           close(cs);
       }
       return updatedAccount;
   }
	
	public void updateCreditCardAccount(CustomerProfileAccount consumer)
	throws DataSourceException
{
	ConsumerCreditCardAccount updatedAccount = null;
	CallableStatement cs = null;
	Connection conn = null;

	try
	{
		conn = getConnection();

		String storedProcName =
			"pkg_em_cust_account_maint.prc_iu_cust_account";
		cs =
			conn.prepareCall(
				"{ call "
					+ storedProcName
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

		int paramIndex = 0;
		cs.setString(++paramIndex, String.valueOf("emtadmin"));
		cs.setString(++paramIndex, dbCallTypeCode);
		cs.setInt(++paramIndex, consumer.getCustAcctId());
		cs.setInt(++paramIndex, consumer.getCustAcctVerNbr());
		cs.setInt(++paramIndex, consumer.getCustId());
		cs.setInt(++paramIndex, consumer.getAddrId());
		cs.setString(++paramIndex, consumer.getAcctTypeCode());
		cs.setString(++paramIndex, consumer.getAcctStatCode());
		cs.setString(++paramIndex, consumer.getAcctSubStatCode());
		cs.setNull(++paramIndex, Types.VARCHAR); // account number
		cs.setString(++paramIndex, consumer.getAcctMaskNbr());
		cs.setString(++paramIndex, consumer.getAcctHoldrName());
		cs.setNull(++paramIndex, Types.VARCHAR);
		cs.setNull(++paramIndex, Types.VARCHAR); // aba number
		cs.setNull(++paramIndex, Types.VARCHAR);
		// financial institution name
		cs.setString(++paramIndex, consumer.getCrcdBinHashText());
		cs.setNull(++paramIndex, Types.INTEGER);
		cs.setNull(++paramIndex, Types.INTEGER);
		cs.setNull(++paramIndex,Types.VARCHAR);  // account blk code
		cs.setNull(++paramIndex,Types.VARCHAR);
		cs.setNull(++paramIndex,Types.VARCHAR);
		cs.setNull(++paramIndex,Types.VARCHAR);
		cs.setString(++paramIndex, consumer.getBlkCrcdAcctId());
		cs.setString(++paramIndex, consumer.getCrcdBinText());
		cs.registerOutParameter(++paramIndex, Types.BIGINT);
		int dbLogIdIndex = paramIndex;
		cs.registerOutParameter(++paramIndex, Types.INTEGER);
		int newAccountIdIndex = paramIndex;
		cs.registerOutParameter(++paramIndex, Types.INTEGER);

		if (EMGSharedLogger
			.getLogger(this.getClass().getName().toString())
			.isDiagnosticEnabled())
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
		}

		cs.execute();
		long dbLogId = cs.getLong(dbLogIdIndex);
		if (EMGSharedLogger
			.getLogger(this.getClass().getName().toString())
			.isDiagnosticEnabled())
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).diagnostic(
				"dbLogId = " + dbLogId);
		}
	} catch (SQLException e)
	{
		throw new DataSourceException(e);
	} finally
	{
		close(cs);
	}
}

	public void updateCreditCardAccountType(
		String userId,
		int acctId,
		String acctType,
		String typeCode)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_update_crcd_type_info";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?, ?,?,?, ?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, String.valueOf(userId));
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setInt(++paramIndex, acctId);
			cs.setString(++paramIndex, acctType);
			cs.setString(++paramIndex, typeCode);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			logEndTime(dbLogId);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
	}

	public Map getAccountStatusDescriptions() throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map statusDescriptions = new HashMap();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_status_and_type.prc_get_cust_acct_sub_stat_cv";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, "WEB");
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				String statusCode = rs.getString("ACCT_STAT_CODE");
				String subStatusCode = rs.getString("ACCT_SUB_STAT_CODE");
				AccountStatus status =
					AccountStatus.getInstance(statusCode, subStatusCode);
				statusDescriptions.put(
					status,
					rs.getString("ACCT_STAT_DESC")
						+ ": "
						+ rs.getString("ACCT_SUB_STAT_DESC"));
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}

		return statusDescriptions;
	}

	public Map getAccountCommentReasons() throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Map accountCommentReasons = new HashMap();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_status_and_type.prc_get_cust_acct_cmnt_reas_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, "WEB");
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(cursorIndex);
			long count = 0;
			while (rs.next())
			{
			    count++;
				String code = rs.getString("CUST_ACCT_CMNT_REAS_CODE");
				String desc = rs.getString("CMNT_REAS_DESC");
				accountCommentReasons.put(code, desc);
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return accountCommentReasons;
	}

	public List getCmnts(int accountId, String callerLoginId)
		throws DataSourceException
	{
		Connection conn = getConnection();
		return getComments(conn, accountId, callerLoginId);
	}

	private List getComments(
		Connection conn,
		int accountId,
		String callerLoginId)
		throws DataSourceException
	{
		CallableStatement cs = null;
		ResultSet rsComments = null;
		List comments = new ArrayList();

		try
		{
			String storedProcName =
				"pkg_em_comments.prc_get_cust_acct_comment_cv";

			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, accountId);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramConsumerIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " started: " + System.currentTimeMillis());
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rsComments = (ResultSet) cs.getObject(paramConsumerIndex);
			long count = 0;
			while (rsComments.next())
			{
			    count++;
				AccountComment comment = new AccountComment();
				comment.setId(rsComments.getInt("cust_acct_cmnt_id"));
				comment.setAccountId(rsComments.getInt("cust_acct_id"));
				comment.setReasonCode(
					rsComments.getString("cust_acct_cmnt_reas_code"));
				comment.setReasonDescription(
					rsComments.getString("cmnt_reas_desc"));
				comment.setText(rsComments.getString("cmnt_text"));
				comment.setCreatedBy(rsComments.getString("create_userid"));
				Timestamp ts = rsComments.getTimestamp("create_date");
				if (ts != null)
				{
					comment.setCreateDate(new java.util.Date(ts.getTime()));
				}
				comments.add(comment);
			}
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rsComments);
			close(cs);
		}
		return comments;
	}

	public int insertComment(AccountComment comment, String callerLoginId)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		int newId = -1;

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_comments.prc_insrt_cust_acct_comment";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, comment.getAccountId());
			cs.setString(++paramIndex, comment.getReasonCode());
			cs.setString(++paramIndex, comment.getText());

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int newIdIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).diagnostic(storedProcName + " ended: " + System.currentTimeMillis()); //$NON-NLS-1$
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}
			newId = ((Integer) cs.getObject(newIdIndex)).intValue();
			logEndTime(dbLogId);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return newId;
	}

	public Collection getFilteredCCAccounts(
		String callerLoginId,
		String hashedText,
		String ccMask)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Collection ccAccounts = new ArrayList();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_crcd_accounts_cv";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?, ?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);

			if (!StringHelper.isNullOrEmpty(hashedText))
				cs.setString(++paramIndex, hashedText);
			else
				cs.setNull(++paramIndex, Types.VARCHAR);

			if (!StringHelper.isNullOrEmpty(ccMask))
				cs.setString(++paramIndex, ccMask);
			else
				cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			cs.execute();

			rs = (ResultSet) cs.getObject(cursorIndex);
			while (rs.next())
			{

				ConsumerAddress billingAddress =
					new ConsumerAddress(
						rs.getInt("addr_id"),
						rs.getInt("cust_id"));

				ConsumerAccountType type =
					ConsumerAccountType.getInstance(
						rs.getString("acct_type_code"));

				AccountStatus status =
					AccountStatus.getInstance(
						rs.getString("acct_stat_code"),
						rs.getString("acct_sub_stat_code"));

				ConsumerCreditCardAccount account =
					new ConsumerCreditCardAccount(
						rs.getInt("cust_acct_id"),
						rs.getInt("cust_id"),
						status,
						type,
						rs.getString("acct_type_desc"),
						rs.getString("acct_mask_nbr"),
						rs.getString("CRCD_BIN_MASK_NBR"),
						rs.getString("crcd_bin_hash_text"),
						null,
						billingAddress,
						TaintIndicatorType.NOT_BLOCKED,
						TaintIndicatorType.NOT_BLOCKED,
						TaintIndicatorType.NOT_BLOCKED);

				account.setEncryptedText(rs.getString("acct_encryp_nbr"));
				account.setAccountHolderFullName(
					rs.getString("acct_holdr_name"));
				account.setExpireMonth(rs.getInt("crcd_exp_mth_nbr"));
				account.setExpireYear(rs.getInt("crcd_exp_yr_nbr"));
				account.setVersionNumber(rs.getInt("cust_acct_ver_nbr"));
				String tmpCode = rs.getString("crcd_type_code");
				tmpCode =
					tmpCode == null
						? ConsumerCreditCardAccount.UNKNOWN_CODE
						: tmpCode;
				account.setCreditOrDebitCode(tmpCode);
				ccAccounts.add(account);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return ccAccounts;
	}

	public Collection getFilteredBankAccounts(
		String callerLoginId,
		String abaNumber,
		String maskText)
		throws DataSourceException
	{
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Collection bankAccounts = new ArrayList();

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_bank_accounts_cv";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?, ?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, abaNumber);

			if (!StringHelper.isNullOrEmpty(maskText))
				cs.setString(++paramIndex, maskText);
			else
				cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cursorIndex = paramIndex;

			cs.execute();

			rs = (ResultSet) cs.getObject(cursorIndex);
			while (rs.next())
			{

				ConsumerAddress billingAddress =
					new ConsumerAddress(
						rs.getInt("addr_id"),
						rs.getInt("cust_id"));

				ConsumerAccountType type =
					ConsumerAccountType.getInstance(
						rs.getString("acct_type_code"));

				AccountStatus status =
					AccountStatus.getInstance(
						rs.getString("acct_stat_code"),
						rs.getString("acct_sub_stat_code"));

				ConsumerBankAccount account =
					new ConsumerBankAccount(
						rs.getInt("cust_acct_id"),
						rs.getInt("cust_id"),
						status,
						type,
						rs.getString("acct_type_desc"),
						rs.getString("acct_mask_nbr"),
						billingAddress,
						TaintIndicatorType.NOT_BLOCKED,
						TaintIndicatorType.NOT_BLOCKED,
						TaintIndicatorType.NOT_BLOCKED);

				account.setEncryptedText(rs.getString("acct_encryp_nbr"));
				bankAccounts.add(account);
			}
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs);
			close(cs);
		}
		return bankAccounts;
	}

	public int getCreditCardCount(String userId, int id, String cardType)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		int creditCardCount = 0;

		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_accounts_count";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, id);
			cs.setString(++paramIndex, cardType);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			cs.registerOutParameter(++paramIndex, OracleTypes.NUMBER);
			int cursorIndex = paramIndex;

			cs.execute();

			creditCardCount = cs.getInt(cursorIndex);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return creditCardCount;
	}

	public String getCreditOrDebitCardType(String userId, String cardNumber)
		throws DataSourceException
	{
		CallableStatement cs = null;
		Connection conn = null;
		String creditOrDebitCode = ConsumerCreditCardAccount.UNKNOWN_CODE;

		try
		{
			conn = getConnection();
			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_credit_or_debit";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, cardNumber);
			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			cs.registerOutParameter(++paramIndex, Types.CHAR);
			int rsIndex = paramIndex;

			cs.execute();

			creditOrDebitCode = cs.getString(rsIndex);
			creditOrDebitCode =
				creditOrDebitCode == null
					? ConsumerCreditCardAccount.UNKNOWN_CODE
					: creditOrDebitCode.trim();
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(cs);
		}
		return creditOrDebitCode;
	}

	public Set getAccountsWithComments(
		AccountSearchRequest searchCriteria,
		String callerUserId)
		throws DataSourceException
	{
		ResultSet rs1 = null;
		ResultSet rs2 = null;
		CallableStatement cs = null;
		Connection conn = null;
		Set accounts = new HashSet(0);

		if (searchCriteria == null)
		{
			searchCriteria = new AccountSearchRequest();
		}

		try
		{
			conn = getConnection();

			String storedProcName =
				"pkg_em_cust_account_maint.prc_get_cust_acct_comments";
			cs =
				conn.prepareCall(
					"{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;

			cs.setString(++paramIndex, callerUserId);
			cs.setString(++paramIndex, "WEB");

			Integer custId = searchCriteria.getCustId();
			if (custId == null)
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, custId.intValue());
			}

			cs.setNull(++paramIndex, Types.INTEGER); // acct id
			cs.setNull(++paramIndex, Types.INTEGER); //	version number
			cs.setNull(++paramIndex, Types.VARCHAR); //	acct type code

			String statusCode = searchCriteria.getStatusCode();
			if (statusCode == null)
			{
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else
			{
				cs.setString(++paramIndex, statusCode);
			}

			cs.setNull(++paramIndex, Types.VARCHAR); //	sub stat code

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;

			//  Consumer Accounts cursor
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int acctCursorIndex = paramIndex;

			//  Account Comments cursor
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int cmntCursorIndex = paramIndex;

			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " started: " + System.currentTimeMillis());
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger
				.getLogger(this.getClass().getName().toString())
				.isDiagnosticEnabled())
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					storedProcName + " ended: " + System.currentTimeMillis());
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"dbLogId = " + dbLogId);
			}

			rs1 = (ResultSet) cs.getObject(cmntCursorIndex);
			Map cmntsMap = populateAcctCmnts(rs1);
			
			rs2 = (ResultSet) cs.getObject(acctCursorIndex);
			accounts = populateAccounts(rs2, cmntsMap);
			long count = accounts.size();
			logEndTime(dbLogId,count);
		} catch (SQLException e)
		{
			throw new DataSourceException(e);
		} finally
		{
			close(rs1);
			close(rs2);
			close(cs);
		}
		return accounts;
	}

	private Map populateAcctCmnts(ResultSet rs) throws SQLException
	{
		Map cmntMap = new HashMap();
		List cmntList = new ArrayList();
		int saveAcctId = -1;
		int acctId = -1;

		while (rs.next())
		{
			acctId = rs.getInt("cust_acct_id");
			if (saveAcctId != -1 && saveAcctId != acctId)
			{
				cmntMap.put(new Integer(saveAcctId), cmntList);
				cmntList = new ArrayList();
			}
			saveAcctId = acctId;
			AccountComment comment = new AccountComment();
			comment.setId(rs.getInt("cust_acct_cmnt_id"));
			comment.setAccountId(acctId);
			comment.setReasonCode(rs.getString("cust_acct_cmnt_reas_code"));
			comment.setReasonDescription(rs.getString("cmnt_reas_desc"));
			comment.setText(rs.getString("cmnt_text"));
			comment.setCreatedBy(rs.getString("create_userid"));
			Timestamp ts = rs.getTimestamp("create_date");
			if (ts != null)
			{
				comment.setCreateDate(new java.util.Date(ts.getTime()));
			}
			cmntList.add(comment);
		}

		if (acctId != -1)
		{
			cmntMap.put(new Integer(acctId), cmntList);
		}

		return cmntMap;
	}

	private Set populateAccounts(ResultSet rs, Map cmntsMap)
		throws SQLException
	{
		Set accounts = new HashSet();
		if (rs != null)
		{
			while (rs.next())
			{
				int consumerId = rs.getInt("cust_id");
				ConsumerAddress billingAddress =
					new ConsumerAddress(rs.getInt("addr_id"), consumerId);

				ConsumerAccountType type =
					ConsumerAccountType.getInstance(
						rs.getString("acct_type_code"));

				AccountStatus status =
					AccountStatus.getInstance(
						rs.getString("acct_stat_code"),
						rs.getString("acct_sub_stat_code"));

				if (type.isCardAccount())
				{
					ConsumerCreditCardAccount account =
						new ConsumerCreditCardAccount(
							rs.getInt("cust_acct_id"),
							consumerId,
							status,
							type,
							rs.getString("acct_type_desc"),
							rs.getString("acct_mask_nbr"),
							rs.getString("CRCD_BIN_MASK_NBR"),
							rs.getString("crcd_bin_hash_text"),
							null,
							billingAddress,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED);
					account.setAccountHolderFullName(
						rs.getString("acct_holdr_name"));
					account.setExpireMonth(rs.getInt("crcd_exp_mth_nbr"));
					account.setExpireYear(rs.getInt("crcd_exp_yr_nbr"));
					account.setVersionNumber(rs.getInt("cust_acct_ver_nbr"));
					String tmpCode = rs.getString("crcd_type_code");
					tmpCode =
						tmpCode == null
							? ConsumerCreditCardAccount.UNKNOWN_CODE
							: tmpCode;
					account.setCreditOrDebitCode(tmpCode);
					// Check Taint Indicator.
					if (account.getAccountNumberTaintCode() != null
						&& rs.getString("acct_blkd_code") != null)
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.getInstance(
								rs.getString("acct_blkd_code").toUpperCase()));
					} else
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getBankAbaTaintCode() != null
						&& rs.getString("bank_aba_blkd_code") != null)
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("bank_aba_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getCreditCardBinTaintCode() != null
						&& rs.getString("crcd_bin_blkd_code") != null)
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("crcd_bin_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					int acctId = rs.getInt("cust_acct_id");
					List cmntsList = (List) cmntsMap.get(new Integer(acctId));

					if (cmntsList != null)
						Collections.sort((ArrayList)cmntsList);
					
					account.setCmntList(
						cmntsList != null
							&& cmntsList.size() != 0 ? cmntsList : null);					
					accounts.add(account);
				} else if (type.isBankAccount())
				{
					ConsumerBankAccount account =
						new ConsumerBankAccount(
							rs.getInt("cust_acct_id"),
							consumerId,
							status,
							type,
							rs.getString("acct_type_desc"),
							rs.getString("acct_mask_nbr"),
							billingAddress,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED,
							TaintIndicatorType.NOT_BLOCKED);
					account.setABANumber(rs.getString("bank_aba_nbr"));
					account.setFinancialInstitutionName(
						rs.getString("fin_instn_name"));
					account.setName(rs.getString("acct_name"));
					account.setVersionNumber(rs.getInt("cust_acct_ver_nbr"));
					// Check Taint Indicator.
					if (account.getAccountNumberTaintCode() != null
						&& rs.getString("acct_blkd_code") != null)
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.getInstance(
								rs.getString("acct_blkd_code").toUpperCase()));
					} else
					{
						account.setAccountNumberTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getBankAbaTaintCode() != null
						&& rs.getString("bank_aba_blkd_code") != null)
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("bank_aba_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setBankAbaTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}
					if (account.getCreditCardBinTaintCode() != null
						&& rs.getString("crcd_bin_blkd_code") != null)
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.getInstance(
								rs
									.getString("crcd_bin_blkd_code")
									.toUpperCase()));
					} else
					{
						account.setCreditCardBinTaintCode(
							TaintIndicatorType.NOT_BLOCKED);
					}

					int acctId = rs.getInt("cust_acct_id");
					List cmntsList = (List) cmntsMap.get(new Integer(acctId));

					if (cmntsList != null)
						Collections.sort((ArrayList)cmntsList);
					
					account.setCmntList(
						cmntsList != null
							&& cmntsList.size() != 0 ? cmntsList : null);
					accounts.add(account);
				}
			}
		}
		return accounts;
	}
}
