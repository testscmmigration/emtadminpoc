package emgshared.dataaccessors;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import oracle.jdbc.driver.OracleTypes;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.TranSubReason;

public class TransSubReasonDAO extends
        emgshared.dataaccessors.AbstractOracleDAO {

    public List getTransSubReasonByReasonTypeCode(String userID,
            String reasonTypeCode) throws DataSourceException, SQLException,
            TooManyResultException {
        try {
            return getTransSubReasonByTypeCodeAndSubCode(userID,
                    reasonTypeCode, "");
        } catch (SQLException e) {
            throw new DataSourceException(e);
        }
    }

    public List getTransSubReasonByTypeCodeAndSubCode(String userID,
            String reasonTypeCode, String subReasonCode)
            throws DataSourceException, SQLException, TooManyResultException {
        ResultSet rs = null;
        CallableStatement cs = null;
        Connection conn = null;
        String storedProcName = "pkg_em_status_and_type.prc_get_emg_tran_subreason_cv";
        StringBuffer sqlBuffer = new StringBuffer(64);
        sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");
        List codeList = new ArrayList();
        try {
            conn = getConnection();
            cs = conn.prepareCall(sqlBuffer.toString());
            int paramIndex = 0;
            if (userID == null)
                userID = "EMGMGI";
            cs.setString(++paramIndex, userID);
            cs.setString(++paramIndex, dbCallTypeCode);
            if ((reasonTypeCode == null) || (reasonTypeCode.equals("")))
                cs.setNull(++paramIndex, Types.VARCHAR);
            else
                cs.setString(++paramIndex, reasonTypeCode);
            cs.setNull(++paramIndex, Types.VARCHAR);

            //  output parameters
            cs.registerOutParameter(++paramIndex, Types.INTEGER);
            cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
            int rsIndex = paramIndex;

            cs.execute();
            
            rs = (ResultSet) cs.getObject(rsIndex);
            while (rs.next()) {
                TranSubReason tsr = new TranSubReason();
                tsr.setTranReasonTypeCode(rs.getString("tran_reas_type_code"));
                tsr.setTranReasonTypeDesc(rs.getString("tran_reas_type_desc"));
                tsr.setTranSubReasonCode(rs.getString("tran_sub_reas_code"));
                tsr.setTranSubReasonDesc(rs.getString("tran_sub_reas_desc"));
                codeList.add(tsr);
            }

        } catch (SQLException e) {
            throw new DataSourceException(e);
        } finally {
            close(rs);
            close(cs);
        }
        return codeList;
    }

}
