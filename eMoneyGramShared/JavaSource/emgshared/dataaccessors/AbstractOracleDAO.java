/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.dataaccessors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.property.EMTSharedDynProperties;

/**
 * @author A131
 *
 */
public abstract class AbstractOracleDAO
{
	private Connection connection;
	// Used to control application logging in the database.  
	public static final String dbCallTypeCode = "WEB";

	protected AbstractOracleDAO()
	{
		try
		{
			this.connection = OracleAccess.getConnection();
		} catch (DataSourceException e)
		{
			throw new EMGRuntimeException(e);
		}

		try
		{
			enableSQLDebugging(this.connection);
		} catch (SQLException ignore)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).warn(
				"unable to enable sql debugging",
				ignore);
		}
	}

	public void logEndTime(long logId)
	{
	    try {
	        logEndTime(logId,-1);    
        } catch (Exception ignoreIt) {
            System.out.println("logEndTime exception:" + ignoreIt.getMessage());
            ignoreIt.printStackTrace();
        }
	    
	}
	public void logEndTime(long logId,long count)
	{
	    try {
	    	Connection conn = getConnection();
			// if a logId is passed, then log the end of the procedure call
			if (logId != 0)
			{
				CallableStatement cs = null;		
				String storedProcName = "pkg_proc_log.prc_iu_nvl_process_log_ui";
				cs = conn.prepareCall( "{ call " + storedProcName + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
				int paramIndex = 0;
				cs.setLong(++paramIndex, logId);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setString(++paramIndex, "SYSDATE");
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				if (count==-1)
				    cs.setNull(++paramIndex, Types.FLOAT);    
				else
				    cs.setLong(++paramIndex, count);				
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.execute();
			}  
        } catch (Exception ignoreIt) {
            System.out.println("logEndTime exception:" + ignoreIt.getMessage());
            ignoreIt.printStackTrace();
        }
	}

	public void commit() throws DataSourceException {
		try {
			getConnection().commit();
		} catch (SQLException e) {
			if (notPartOfGlobalTransaction(e)) {
				throw new DataSourceException(e);
			}
		}
	}

	public void rollback() throws DataSourceException {
		try {
			getConnection().rollback();
		} catch (SQLException e) {
			System.err.println("Rollback was not possible: " + e.getMessage());
			if (notPartOfGlobalTransaction(e)) { 
				throw new DataSourceException(e);
			}
		}
	}

	private boolean notPartOfGlobalTransaction(Exception e) {
		try {
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			return out.toString().toString().indexOf("during a global transaction") == -1;
		} catch (Exception e2) {
			return false;
		}
	}

	public void rollbackAndIgnoreException()
	{
		try
		{
			rollback();
		} catch (Throwable t)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				"Rollback failed.",
				t);
		}
	}

	public void close()
	{
		Connection conn = getConnection();
		if (conn != null)
		{
			try
			{
				logSQLDebug(conn);
				disableSQLDebugging(conn);
			} catch (SQLException ignore)
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).diagnostic(
					"Exception during sql debug logging",
					ignore);
			}
		}
		OracleAccess.close(conn);
	}

	protected void close(Statement stmt)
	{
		if (stmt != null)
		{
			try
			{
				stmt.close();
			} catch (SQLException ignore)
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).error(
					this.getClass().getName(),
					ignore);
			}
		}
	}

	protected void close(ResultSet rs)
	{
		if (rs != null)
		{
			try
			{
				rs.close();
			} catch (SQLException ignore)
			{
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).error(
					this.getClass().getName(),
					ignore);
			}
		}
	}

	protected Connection getConnection()
	{
		return this.connection;
	}

	public static void enableSQLDebugging(Connection cxn)
		throws SQLException
	{
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		int sqlDebugLevel = dynProps.getSqlDebugLevel();
		if (sqlDebugLevel > 0)
		{
			CallableStatement cs1 =
				cxn.prepareCall("{ call DBMS_OUTPUT.ENABLE(?)}");
			try
			{
				cs1.setInt(1, 1000000);
				cs1.execute();

				CallableStatement cs2 =
					cxn.prepareCall("{ call pkg_debug.set_debug_on(?)}");
				try
				{
					cs2.setInt(1, sqlDebugLevel);
					cs2.execute();
				} finally
				{
					cs2.close();
				}
			} finally
			{
				cs1.close();
			}
		}
	}

	public static void disableSQLDebugging(Connection cxn)
		throws SQLException
	{
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		int sqlDebugLevel = dynProps.getSqlDebugLevel();
		if (sqlDebugLevel > 0)
		{
			CallableStatement cs1 =
				cxn.prepareCall("{ call pkg_debug.set_debug_off()}");
			try
			{
				cs1.execute();

				CallableStatement cs2 =
					cxn.prepareCall("{ call DBMS_OUTPUT.DISABLE()}");
				try
				{
					cs2.execute();
				} finally
				{
					cs2.close();
				}
			} finally
			{
				cs1.close();
			}
		}
	}

	public static void logSQLDebug(Connection cxn) throws SQLException
	{
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		int sqlDebugLevel = dynProps.getSqlDebugLevel();
		if (sqlDebugLevel > 0)
		{
			CallableStatement cs =
				cxn.prepareCall("{ call DBMS_OUTPUT.GET_LINE(?, ?)}");
			try
			{
				cs.registerOutParameter(1, Types.VARCHAR);
				cs.registerOutParameter(2, Types.NUMERIC);

				int status = 0;
				while (status == 0)
				{
					cs.execute();

					String debugLine = cs.getString(1);
					status = cs.getInt(2);

					if (debugLine != null && debugLine.length() > 0)
					{
						EMGSharedLogger.getLogger(
							AbstractOracleDAO.class.toString()).debug(
							"SQL: " + debugLine);
					}
				}
			} finally
			{
				cs.close();
			}
		}
	}
}
