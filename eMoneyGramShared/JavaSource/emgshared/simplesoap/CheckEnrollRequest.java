package emgshared.simplesoap;

/**
 * @author G.R.Svenddal Created on Jun 22, 2005 SimpleSoap Verified by Visa
 *         Check Enrollment Request
 * 
 * forms a request to the admin server to check enrollment using the account's
 * credit card number, which is an admin server secret.
 * 
 * Since we're using the java.beans.XMLEncoder/Decoder, keep this a classic bean
 * please.
 *  
 */
public class CheckEnrollRequest {
    private int account;
    private String merchantReferenceCode;
    private String unitPrice; //optional value.
    /**
     * @return Returns the account.
     */
    public int getAccount() {
        return account;
    }

    /**
     * @param account
     *            The account to set.
     */
    public void setAccount(int account) {
        this.account = account;
    }

    /**
     * @return Returns the merchantReferenceCode.
     */
    public String getMerchantReferenceCode() {
        return merchantReferenceCode;
    }
    /**
     * @param merchantReferenceCode The merchantReferenceCode to set.
     */
    public void setMerchantReferenceCode(String merchantReferenceCode) {
        this.merchantReferenceCode = merchantReferenceCode;
    }
    public String getUnitPrice()
    {
        return unitPrice;
    }
    public void setUnitPrice(String unitPrice)
    {
        this.unitPrice = unitPrice;
    }
}
