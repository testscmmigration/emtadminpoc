package emgshared.simplesoap;

/**
 * @author G.R.Svenddal
 * Created on Jul 27, 2005
 * ServerAwakeRequestResponse.java
 *  
 * 
 *  */
public class ServerAwakeRequestResponse
{
  String message;

public String getMessage()
{
   return message;
}
public void setMessage(String message)
{
   this.message = message;
}
}
