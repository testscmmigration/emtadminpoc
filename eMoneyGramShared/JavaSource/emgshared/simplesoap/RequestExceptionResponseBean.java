package emgshared.simplesoap;

/**
 * @author G.R.Svenddal
 * Created on Jul 14, 2005
 * RequestExceptionResponseBean.java
 * An Exception is not a bean, so there's no way to return it
 * to the client. Besides, an Exception out of context is of little use.
 * Instead, grab the important Exception info and stuff it here.
 * 
 * 
 *  */
public class RequestExceptionResponseBean
{
   private Class exceptionType;
   private String exceptionStackDump;
   private String userMessage;
   private Object requestBean;
   
   
   public String getExceptionStackDump()
   {
      return exceptionStackDump;
   }
   public void setExceptionStackDump(String exceptionStackDump)
   {
      this.exceptionStackDump = exceptionStackDump;
   }
      
   public Object getRequestBean()
   {
      return requestBean;
   }
   public void setRequestBean(Object requestBean)
   {
      this.requestBean = requestBean;
   }
   public Class getExceptionType()
   {
      return exceptionType;
   }
   public void setExceptionType(Class exceptionType)
   {
      this.exceptionType = exceptionType;
   }
   public String getUserMessage()
   {
      return userMessage;
   }
   public void setUserMessage(String userMessage)
   {
      this.userMessage = userMessage;
   }
   
   public static String shortLogLine(RequestExceptionResponseBean bean){
      return "RequestExceptionResponseBean - " +
        ((bean.userMessage == null)?"":bean.userMessage) + " " + 
        ((bean.exceptionType == null)?"":bean.exceptionType.getName()) + " " + 
        ((bean.requestBean == null)?"":" ReqBean::" + bean.requestBean.getClass().getName());
   }
 }
   

