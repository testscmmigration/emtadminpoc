/*
 * Created on Jan 31, 2005
 *
 */
package emgshared.model;
import emgshared.services.verifiedbyvisa.CyberSourceVisaTicket;
/**
 * Simple value object.
 * @author A131
 *
 */
public class CreditCard
{
	private String accountNumber;
	private String cvv;
	private int expireMonth;
	private int expireYear;
    private CyberSourceVisaTicket csvTicket;

	public CreditCard()
	{
		super();
	}

	public CreditCard(
		String accountNumber,
		String cvv,
		int expireMonth,
		int expireYear)
	{
		super();
		this.accountNumber = accountNumber;
		this.cvv = cvv;
		this.expireMonth = expireMonth;
		this.expireYear = expireYear;
	}

	public CreditCard(
		String accountNumber,
		String cvv,
		int expireMonth,
		int expireYear,
		CyberSourceVisaTicket csvTicket)
	{
		super();
		this.accountNumber = accountNumber;
		this.cvv = cvv;
		this.expireMonth = expireMonth;
		this.expireYear = expireYear;
		this.csvTicket = csvTicket;
	}


	/**
	* @return
	* 
	* Created on Jan 31, 2005
	*/
	public String getAccountNumber()
	{
		return accountNumber;
	}

	/**
	* @return
	* 
	* Created on Jan 31, 2005
	*/
	public String getCvv()
	{
		return cvv;
	}

	/**
	* @return
	* 
	* Created on Jan 31, 2005
	*/
	public int getExpireMonth()
	{
		return expireMonth;
	}

	/**
	* @return
	* 
	* Created on Jan 31, 2005
	*/
	public int getExpireYear()
	{
		return expireYear;
	}

	/**
	* @param string
	* 
	* Created on Jan 31, 2005
	*/
	public void setAccountNumber(String string)
	{
		accountNumber = string;
	}

	/**
	* @param string
	* 
	* Created on Jan 31, 2005
	*/
	public void setCvv(String string)
	{
		cvv = string;
	}

	/**
	* @param i
	* 
	* Created on Jan 31, 2005
	*/
	public void setExpireMonth(int i)
	{
		expireMonth = i;
	}

	/**
	* @param i
	* 
	* Created on Jan 31, 2005
	*/
	public void setExpireYear(int i)
	{
		expireYear = i;
	}

	public CyberSourceVisaTicket getCsvTicket()
	{
      	return csvTicket;
	}
   
	public void setCsvTicket(CyberSourceVisaTicket csvTicket)
	{
      	this.csvTicket = csvTicket;
	}
}
