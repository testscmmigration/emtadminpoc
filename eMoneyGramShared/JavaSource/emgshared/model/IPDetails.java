/**
 * 
 */
package emgshared.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author wq09
 * 
 */
public class IPDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8286073293671528865L;

	private String consumerISP;
	private String consumerDomain;
	private String consumerCountry;
	private String countryCode;
	private String consumerRegion;
	private String consumerCity;
	private String consumerZipCode;
	private BigDecimal consumerLatitude;
	private BigDecimal consumerLongitude;

	public String getConsumerISP() {
		return consumerISP;
	}

	public void setConsumerISP(String consumerISP) {
		this.consumerISP = consumerISP;
	}

	public String getConsumerDomain() {
		return consumerDomain;
	}

	public void setConsumerDomain(String consumerDomain) {
		this.consumerDomain = consumerDomain;
	}

	public String getConsumerCountry() {
		return consumerCountry;
	}

	public void setConsumerCountry(String consumerCountry) {
		this.consumerCountry = consumerCountry;
	}

	public String getConsumerRegion() {
		return consumerRegion;
	}

	public void setConsumerRegion(String consumerRegion) {
		this.consumerRegion = consumerRegion;
	}

	public String getConsumerCity() {
		return consumerCity;
	}

	public void setConsumerCity(String consumerCity) {
		this.consumerCity = consumerCity;
	}

	public String getConsumerZipCode() {
		return consumerZipCode;
	}

	public void setConsumerZipCode(String consumerZipCode) {
		this.consumerZipCode = consumerZipCode;
	}

	public BigDecimal getConsumerLatitude() {
		return consumerLatitude;
	}

	public void setConsumerLatitude(BigDecimal consumerLatitude) {
		this.consumerLatitude = consumerLatitude;
	}

	public BigDecimal getConsumerLongitude() {
		return consumerLongitude;
	}

	public void setConsumerLongitude(BigDecimal consumerLongitude) {
		this.consumerLongitude = consumerLongitude;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}
