/*
 * Created on Jan 21, 2005
 *
 */
package emgshared.model;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class PhoneType
{
	private static final String PRIMARY_TYPE = "PRM";
	private static final String ALTERNATE_TYPE = "ALT";

	public static final PhoneType PRIMARY =
		new PhoneType(PRIMARY_TYPE);
	public static final PhoneType ALTERNATE =
		new PhoneType(ALTERNATE_TYPE);

	private final String code;

	private PhoneType(String code)
	{
		this.code = code;
	}

	public static PhoneType getInstance(String code)
	{
		PhoneType type;
		if ("Y".equalsIgnoreCase(code))
		{
			type = PRIMARY;
		} else if ("N".equalsIgnoreCase(code))
		{
			type = ALTERNATE;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { code, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (type);
	}

	public String getCode()
	{
		return code;
	}
}
