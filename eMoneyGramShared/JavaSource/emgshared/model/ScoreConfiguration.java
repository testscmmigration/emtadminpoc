package emgshared.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Collections;

public class ScoreConfiguration
{
	private String transactionType;
	private String transactionCodeLabel;
	private String emgTranTypeDesc;
	private String scoreFlag;
	private String autoAprvFlag;
	private String typeLastUpdateDate;
	private String typeLastUpdateUserid;

	private int configurationId;
	private int configurationVersionNumber;
	private String status;
	private int acceptThreshold;
	private int rejectThreshold;
	private String createDate;
	private String createUserid;
	private String lastUpdateUserid;
	private String lastUpdateDate;
	private int partnerSiteCode;
	
	private int totalWeight;
	private List categoryList;

	//	private ArrayList scoreCategories  = new ArrayList();
	private Map scoreCategories = new HashMap();
//	private int getTotalWeight;
	private boolean editable;

	public void addScoreCategory(ScoreCategory scoreCategory)
	{
		//		scoreCategories.add(arg, scoreCategory);
		scoreCategories.put(
			scoreCategory.getScoreCategoryName(),
			scoreCategory);
	}

	//	public void removeScoreCategory(int arg)
	//	{
	//		scoreCategories.remove(arg);
	//	}
	public void removeScoreCategory(String scoreCategoryName)
	{
		scoreCategories.remove(scoreCategoryName);
	}

	public int getConfigurationId()
	{
		return configurationId;
	}

	public int getConfigurationVersionNumber()
	{
		return configurationVersionNumber;
	}

	public String getCreateUserid()
	{
		return createUserid;
	}

	public String getLastUpdateUserid()
	{
		return lastUpdateUserid;
	}

	public int getRejectThreshold()
	{
		return rejectThreshold;
	}

	//	public ArrayList getScoreCategories()
	//	{
	//		return scoreCategories;
	//	}
	public Map getScoreCategories()
	{
		return scoreCategories;
	}

	public String getTransactionType()
	{
		return transactionType;
	}

	public void setConfigurationId(int i)
	{
		configurationId = i;
	}

	public void setConfigurationVersionNumber(int i)
	{
		configurationVersionNumber = i;
	}

	public void setCreateUserid(String string)
	{
		createUserid = string;
	}

	public void setLastUpdateUserid(String string)
	{
		lastUpdateUserid = string;
	}

	public void setRejectThreshold(int i)
	{
		rejectThreshold = i;
	}

	public void setTransactionType(String string)
	{
		transactionType = string;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String string)
	{
		status = string;
	}

	public int getAcceptThreshold()
	{
		return acceptThreshold;
	}

	public void setAcceptThreshold(int i)
	{
		acceptThreshold = i;
	}

	public int getGetTotalWeight()
	{
		int totalScore = 0;
		for (Iterator it = getScoreCategories().values().iterator();
			it.hasNext();
			)
		{
			ScoreCategory sc = (ScoreCategory) it.next();
			if (sc.isIncluded())
			{
				totalScore = totalScore + sc.getWeight();
			}
		}
		return totalScore;
	}

	public void setScoreCategories(Map list)
	{
		scoreCategories = list;
	}

	public String getCreateDate()
	{
		return createDate;
	}

	public String getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setCreateDate(String string)
	{
		createDate = string;
	}

	public void setLastUpdateDate(String string)
	{
		lastUpdateDate = string;
	}

	public boolean isEditable()
	{
		return editable;
	}

	public void setEditable(boolean b)
	{
		editable = b;
	}

//	public void setGetTotalWeight(int i)
//	{
//		getTotalWeight = i;
//	}

	public String getEmgTranTypeDesc()
	{
		return emgTranTypeDesc;
	}

	public void setEmgTranTypeDesc(String string)
	{
		emgTranTypeDesc = string;
	}

	public String getScoreFlag()
	{
		return scoreFlag;
	}

	public void setScoreFlag(String string)
	{
		scoreFlag = string;
	}

	public String getAutoAprvFlag()
	{
		return autoAprvFlag;
	}

	public void setAutoAprvFlag(String string)
	{
		autoAprvFlag = string;
	}

	public String getTypeLastUpdateDate()
	{
		return typeLastUpdateDate;
	}

	public void setTypeLastUpdateDate(String string)
	{
		typeLastUpdateDate = string;
	}

	public String getTypeLastUpdateUserid()
	{
		return typeLastUpdateUserid;
	}

	public void setTypeLastUpdateUserid(String string)
	{
		typeLastUpdateUserid = string;
	}

	public List getCategoryList()
	{
		return categoryList;
	}

	public void setCategoryList(List list)
	{
		categoryList = list;
		if (categoryList != null)
			Collections.sort(categoryList);
	}

	public int getTotalWeight()
	{
		return totalWeight;
	}

	public void setTotalWeight(int i)
	{
		totalWeight = i;
	}

	public void setPartnerSiteCode(int partnerSiteCode) {
		this.partnerSiteCode = partnerSiteCode;
	}

	public int getPartnerSiteCode() {
		return partnerSiteCode;
	}

	public String toString() {
		return emgTranTypeDesc + " (" + configurationId + ")";
	}

	public void setTransactionCodeLabel(String transactionCodeLabel) {
		this.transactionCodeLabel = transactionCodeLabel;
	}

	public String getTransactionCodeLabel() {
		return transactionCodeLabel;
	}

}
