/*
 * Created on June 06, 2005
 *
 */
package emgshared.model;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class TaintIndicatorStatus
{

	private static final String BLOCKED_INDICATOR_STATUS = "BLK";
	private static final String NOT_BLOCKED_INDICATOR_STATUS = "NBK";
	private static final String RISK_INDICATOR_STATUS = "RSK";

	public static final TaintIndicatorStatus BLOCKED_STATUS =
		new TaintIndicatorStatus(BLOCKED_INDICATOR_STATUS);
	public static final TaintIndicatorStatus NOT_BLOCKED_STATUS =
		new TaintIndicatorStatus(NOT_BLOCKED_INDICATOR_STATUS);
	public static final TaintIndicatorStatus RISK_STATUS =
		new TaintIndicatorStatus(RISK_INDICATOR_STATUS);

	private final String indicatorStatus;

	public TaintIndicatorStatus(String status)
	{
		this.indicatorStatus = status;
	}

	public static TaintIndicatorStatus getInstance(String indicatorStatus)
	{
		TaintIndicatorStatus status;
		if (BLOCKED_INDICATOR_STATUS.equalsIgnoreCase(indicatorStatus))
		{
			status = BLOCKED_STATUS;
		} else if (
			NOT_BLOCKED_INDICATOR_STATUS.equalsIgnoreCase(indicatorStatus)
				|| indicatorStatus == null)
		{
			status = NOT_BLOCKED_STATUS;
		} else if (RISK_INDICATOR_STATUS.equalsIgnoreCase(indicatorStatus))
		{
			status = RISK_STATUS;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { indicatorStatus, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (status);
	}

	public String getStatusIndicator()
	{
		return indicatorStatus;
	}

	public boolean isBlocked()
	{
		return BLOCKED_INDICATOR_STATUS.equalsIgnoreCase(this.indicatorStatus);
	}
	public boolean isNotBlocked()
	{
		return NOT_BLOCKED_INDICATOR_STATUS.equalsIgnoreCase(
			this.indicatorStatus);
	}
	public boolean isOverridden()
	{
		return RISK_INDICATOR_STATUS.equalsIgnoreCase(this.indicatorStatus);
	}

	public String toString()
	{
		return indicatorStatus;
	}
}
