/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class EmailStatus implements Serializable 
{

	private static final String ACTIVE_CODE = "ACT";
	private static final String NON_ACTIVE_CODE = "NAT";
	private static final String VERIFICATION_NOT_INITIATED_CODE = "NVF";
	private static final String PENDING_VERIFICATION_CODE = "PEN";

	public static final EmailStatus ACTIVE = new EmailStatus(ACTIVE_CODE);
	public static final EmailStatus NON_ACTIVE =
		new EmailStatus(NON_ACTIVE_CODE);
	public static final EmailStatus VERIFICATION_NOT_INITIATED =
		new EmailStatus(VERIFICATION_NOT_INITIATED_CODE);
	public static final EmailStatus PENDING_VERIFICATION =
		new EmailStatus(PENDING_VERIFICATION_CODE);

	private final String statusCode;

	public EmailStatus(String code)
	{
		this.statusCode = code;
	}

	public static EmailStatus getInstance(String statusCode)
	{
		EmailStatus status;
		if (ACTIVE_CODE.equalsIgnoreCase(statusCode))
		{
			status = ACTIVE;
		} else if (NON_ACTIVE_CODE.equalsIgnoreCase(statusCode))
		{
			status = NON_ACTIVE;
		} else if (
			VERIFICATION_NOT_INITIATED_CODE.equalsIgnoreCase(statusCode))
		{
			status = VERIFICATION_NOT_INITIATED;
		} else if (PENDING_VERIFICATION_CODE.equalsIgnoreCase(statusCode))
		{
			status = PENDING_VERIFICATION;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { statusCode, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (status);
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public boolean isActiveStatus()
	{
		return ACTIVE_CODE.equalsIgnoreCase(this.statusCode);
	}
	public boolean isPendingStatus()
	{
		return PENDING_VERIFICATION_CODE.equalsIgnoreCase(this.statusCode);
	}
	public boolean isNonActiveStatus()
	{
		return NON_ACTIVE_CODE.equalsIgnoreCase(this.statusCode);
	}
	public boolean isVerificationNotInitiatedStatus()
	{
		return VERIFICATION_NOT_INITIATED_CODE.equalsIgnoreCase(
			this.statusCode);
	}
	public String toString()
	{
		return statusCode;
	}
}
