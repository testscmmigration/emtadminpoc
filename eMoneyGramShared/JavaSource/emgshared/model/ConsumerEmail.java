/*
 * Created on April 26, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;

import org.apache.commons.lang.Validate;

/**
 * @author T349
 *
 */
public class ConsumerEmail implements Serializable
{
	private int consumerId;
	private String consumerEmail;
	private ElectronicTypeCode electronicTypeCode;
	private EmailStatus status;
	private TaintIndicatorType emailTaintCode;
	private TaintIndicatorType emailDomainTaintCode;

	public ConsumerEmail(
		int consumerId,
		String consumerEmail,
		ElectronicTypeCode typeCode,
		EmailStatus status,
		TaintIndicatorType emailTaintCode,
		TaintIndicatorType emailDomainTaintCode)
	{
		Validate.notNull(status, "status is a required parameter");
		Validate.notNull(typeCode, "type code is a required parameter");
		this.consumerId = consumerId;
		this.consumerEmail = consumerEmail;
		this.electronicTypeCode = typeCode;
		this.status = status;
		this.emailTaintCode = emailTaintCode;
		this.emailDomainTaintCode = emailDomainTaintCode;
	}
	/**
	 * @return
	 */
	public int getConsumerId()
	{
		return consumerId;
	}

	/**
	 * @return
	 */
	public String getConsumerEmail()
	{
		return consumerEmail;
	}

	/**
	 * @return
	 */
	public ElectronicTypeCode getElectronicTypeCode()
	{
		return electronicTypeCode;
	}

	/**
	 * @return
	 */
	public EmailStatus getStatus()
	{
		return status;
	}

	/**
	 * @param i
	 */
	public void setConsumerId(int i)
	{
		consumerId = i;
	}

	/**
	 * @param string
	 */
	public void setConsumerEmail(String string)
	{
		consumerEmail = string;
	}

	/**
	 * @param code
	 */
	public void setElectronicTypeCode(ElectronicTypeCode code)
	{
		electronicTypeCode = code;
	}

	/**
	 * @param status
	 */
	public void setStatus(EmailStatus status)
	{
		this.status = status;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getEmailTaintCode()
	{
		return emailTaintCode;
	}

	/**
	 * @param type
	 */
	public void setEmailTaintCode(TaintIndicatorType type)
	{
		emailTaintCode = type;
	}
	
	/*public boolean isEmailBlocked()
	{
		return emailTaintCode.isBlocked();
	}
	
	
	public boolean isEmailNotBlocked()
	{
		return emailTaintCode.isNotBlocked();
	}*/

	/**
	 * @return
	 */
	public TaintIndicatorType getEmailDomainTaintCode()
	{
		return emailDomainTaintCode;
	}

	/**
	 * @param type
	 */
	public void setEmailDomainTaintCode(TaintIndicatorType type)
	{
		emailDomainTaintCode = type;
	}

}
