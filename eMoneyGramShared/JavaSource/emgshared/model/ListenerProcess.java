/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

/**
 * @author Jawahar Varadhan
 * This bean holds information on the status of the Listener execution. This
 * bean is also used to send any information to the listener (like tranId)
 */
public class ListenerProcess
{
	private boolean status;
	private int tranId;
	private ListenerProcessType listenerProcessType;
	
	/**
	 * @return
	 */
	public boolean getStatus() {
		return status;
	}

	/**
	 * @param b
	 */
	public void setStatus(boolean b) {
		status = b;
	}

	/**
	 * @return
	 */
	public int getTranId() {
		return tranId;
	}

	/**
	 * @param i
	 */
	public void setTranId(int i) {
		tranId = i;
	}

	/**
	 * @return
	 */
	public ListenerProcessType getListenerProcessType()
	{
		return listenerProcessType;
	}

	/**
	 * @param type
	 */
	public void setListenerProcessType(ListenerProcessType type)
	{
		listenerProcessType = type;
	}

}
