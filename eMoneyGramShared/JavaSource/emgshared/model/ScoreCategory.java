package emgshared.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScoreCategory implements Comparable {

	public enum ScoreCategoryType {
		BOOLEAN,CONSUMER_STATUS,INTEGER,RANGE;
	}
	public static final String cat1 = "Password Reset in 24 Hrs";
	public static final String cat2 = "Profile Status";
	public static final String cat3 = "Negative Activity";
	public static final String cat4 = "Days since last transaction";
	public static final String cat5 = "Email Address in Blocked List";
	public static final String cat6 = "Country Threshold Exceeded";
	public static final String cat7 = "Rcv state matches profile state";
	public static final String cat8 = "Age Threshold";
	public static final String cat9 = "Zip To State";
	public static final String cat10 = "Area To State";
	public static final String cat11 = "Address In Negative File";
	public static final String cat12 = "Keywords In Email";
	public static final String cat13 = "Transaction Monitoring Score";
	public static final String cat14 = "Send Amount";
	public static final String cat15 = "Transaction Amount";
	public static final String[] catList = { cat1, cat2, cat3, cat4, cat5, cat6, cat7, cat8, cat9, cat10, cat11, cat12, cat13, cat14, cat15 };
	private int scoreConfigId;
	private String scoreCategoryCode;
	private String scoreCategoryName;
	private String scoreCategoryDesc;
	private String listTypeCode;
	private String DataTypeCode;
	private int maximumLengthQuantity;
	private String catCreateDate;
	private String catCreateUserid;
	private String catLastUpdateDate;
	private String catLastUpdateUserid;
	private boolean included;
	private String inclFlag;
	private int weight;
	private String wgtPct; // not a database field
	private String weightPercentage;
	private Date createDate;
	private String catCnfgCreateDate;
	private String createUserid;
	private String lastUpdateUserid;
	private Date lastUpdateDate;
	private String catCnfgLastUpdateDate;
	private List ScoreValues = new ArrayList();

	public void addScoreValue(int arg, ScoreValue scoreValue) {
		ScoreValues.add(arg, scoreValue);
	}

	public void removeScoreValue(int arg) {
		ScoreValues.remove(arg);
	}

	public Date getCreateDate() {
		return createDate;
	}

	public String getCreateUserid() {
		return createUserid;
	}

	public boolean isIncluded() {
		return included;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public String getLastUpdateUserid() {
		return lastUpdateUserid;
	}

	public String getScoreCategoryCode() {
		return scoreCategoryCode;
	}

	public List<ScoreValue> getScoreValues() {
		return ScoreValues;
	}

	public int getWeight() {
		return weight;
	}

	public void setCreateDate(Date date) {
		createDate = date;
	}

	public void setCreateUserid(String string) {
		createUserid = string;
	}

	public void setIncluded(boolean b) {
		included = b;
	}

	public void setLastUpdateDate(Date date) {
		lastUpdateDate = date;
	}

	public void setLastUpdateUserid(String string) {
		lastUpdateUserid = string;
	}

	public void setScoreCategoryCode(String string) {
		scoreCategoryCode = string;
	}

	public void setWeight(int i) {
		weight = i;
	}

	public String getDataTypeCode() {
		return DataTypeCode;
	}

	public String getListTypeCode() {
		return listTypeCode;
	}

	public int getMaximumLengthQuantity() {
		return maximumLengthQuantity;
	}

	public String getScoreCategoryDesc() {
		return scoreCategoryDesc;
	}

	public String getScoreCategoryName() {
		return scoreCategoryName;
	}

	public void setDataTypeCode(String string) {
		DataTypeCode = string;
	}

	public void setListTypeCode(String string) {
		listTypeCode = string;
	}

	public void setMaximumLengthQuantity(int i) {
		maximumLengthQuantity = i;
	}

	public void setScoreCategoryDesc(String string) {
		scoreCategoryDesc = string;
	}

	public void setScoreCategoryName(String string) {
		scoreCategoryName = string;
	}

	public void setScoreValues(ArrayList list) {
		ScoreValues = list;
	}

	public String getWeightPercentage() {
		return weightPercentage;
	}

	public void setWeightPercentage(String string) {
		weightPercentage = string;
	}

	public int getScoreConfigId() {
		return scoreConfigId;
	}

	public void setScoreConfigId(int i) {
		scoreConfigId = i;
	}

	public String getCatCreateDate() {
		return catCreateDate;
	}

	public void setCatCreateDate(String string) {
		catCreateDate = string;
	}

	public String getCatCreateUserid() {
		return catCreateUserid;
	}

	public void setCatCreateUserid(String string) {
		catCreateUserid = string;
	}

	public String getCatLastUpdateDate() {
		return catLastUpdateDate;
	}

	public void setCatLastUpdateDate(String string) {
		catLastUpdateDate = string;
	}

	public String getCatLastUpdateUserid() {
		return catLastUpdateUserid;
	}

	public void setCatLastUpdateUserid(String string) {
		catLastUpdateUserid = string;
	}

	public String getInclFlag() {
		return inclFlag;
	}

	public void setInclFlag(String string) {
		inclFlag = string;
	}

	public String getCatCnfgCreateDate() {
		return catCnfgCreateDate;
	}

	public void setCatCnfgCreateDate(String string) {
		catCnfgCreateDate = string;
	}

	public String getCatCnfgLastUpdateDate() {
		return catCnfgLastUpdateDate;
	}

	public void setCatCnfgLastUpdateDate(String string) {
		catCnfgLastUpdateDate = string;
	}

	public void setScoreValues(List list) {
		ScoreValues = list;
	}

	public String getWgtPct() {
		return wgtPct;
	}

	public void setWgtPct(String string) {
		wgtPct = string;
	}

	public TransactionScoreCategory computeTransactionScoreCategory(
			ScoreValue scoreValue, String checkValue, double dTotalWeight) {

		TransactionScoreCategory tsc = null;
		ScoreCategoryType category = null;
		try {
			category = determineCategory();
		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean check = false;

		switch(category) {
		case BOOLEAN:
			check = checkValue.equalsIgnoreCase(scoreValue.getValue());
			break;
		case CONSUMER_STATUS:
			check = checkValue.equals(scoreValue.getValue());
			break;
		case INTEGER:
			check = Integer.parseInt(checkValue) == Integer.parseInt(scoreValue.getValue());
			break;
		case RANGE:
			check = (Integer.parseInt(checkValue) >= scoreValue.getMinimumValue()
						&& Integer.parseInt(checkValue) <= scoreValue.getMaximumValue());
			break;
		default:
			break;
		}

		if(check) {
			int rawScore = scoreValue.getPoints();
			double dPoints = scoreValue.getPoints();
			double dWeight = getWeight();
			int weightedScore = (int) Math.round((dPoints * dWeight) / dTotalWeight);

			tsc = new TransactionScoreCategory();
			tsc.setScoreCatName(getScoreCategoryName());
			tsc.setScoreCatCode(getScoreCategoryCode());
			tsc.setRawScoreNbr(rawScore);
			tsc.setWgtScoreNbr(weightedScore);
			tsc.setScoreQueryRsltData(checkValue);
		}

		return tsc;
	}

	public ScoreCategoryType determineCategory() throws Exception {
		if (getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat1) || getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat5)
				|| getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat6) || getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat7)
				|| getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat8) || getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat9)
				|| getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat10) || getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat11)
				|| getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat12)) {
			return ScoreCategoryType.BOOLEAN;
		}
		else if (getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat2)) {
			return ScoreCategoryType.CONSUMER_STATUS;
		}
		else if (getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat3)) {
			return ScoreCategoryType.INTEGER;
		}
		else if ( (getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat4)) ||
				(getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat13)) ||
				(getScoreCategoryName().equalsIgnoreCase(ScoreCategory.cat15))) {
			return ScoreCategoryType.RANGE;
		}
		return null;
	}


	public int compareTo(Object o) {
		ScoreCategory other = (ScoreCategory) o;
		return Integer.parseInt(this.scoreCategoryCode) - Integer.parseInt(other.scoreCategoryCode);
	}
}
