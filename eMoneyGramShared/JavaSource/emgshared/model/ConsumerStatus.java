/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author A131
 *
 */
public class ConsumerStatus implements Serializable
{
	//TODO tom c. - implement equals and hashcode on ConsumerStatus enum
	public static final String DELIMITER = ":";

	private static final String PENDING_CODE = "PEN";
	private static final String NON_ACTIVE_CODE = "NAT";
	private static final String ACTIVE_CODE = "ACT";

	private static final String INITIAL_CODE = "INI";
	private static final String VALIDATION_LEVEL_0_CODE = "L00";
	private static final String VALIDATION_LEVEL_1_CODE = "L01";
	private static final String VALIDATION_LEVEL_2_CODE = "L02";
	private static final String VALIDATION_LEVEL_3_CODE = "L03";
	private static final String VALIDATION_LEVEL_4_CODE = "L04";
	private static final String VALIDATION_LEVEL_5_CODE = "L05";
	private static final String VALIDATION_LEVEL_6_CODE = "L06";
	private static final String VALIDATION_LEVEL_7_CODE = "L07";
	private static final String VALIDATION_LEVEL_8_CODE = "L08";
	private static final String VALIDATION_LEVEL_9_CODE = "L09";
	private static final String VALIDATION_LEVEL_10_CODE = "L10";
	private static final String VALIDATION_LEVEL_12_CODE = "L12";
    //MGO-11838: KBA By Pass
	private static final String VALIDATION_LEVEL_13_CODE = "L13";
	private static final String VALIDATION_LEVEL_14_CODE = "L14";
	private static final String VERID_ABANDON = "VAB";
	private static final String FRAUD_CODE = "FRD";
	private static final String VERID_FAIL_CODE = "VRF";
	private static final String SUSPICIOUS_ACTIVITY_CODE = "SUS";
	private static final String PASSWORD_FAILURES_CODE = "PWF";
	private static final String OTHER_CODE = "OTH";
	private static final String USER_REQUEST_CODE = "USR";
	private static final String CHARGEBACK_CODE = "CBK";
	private static final String ACH_RETURN_CODE = "CHR";
	private static final String ILLEGAL_ACTIVITY_CODE = "IAC";
	private static final String BOUNCED_EMAIL_CODE = "BEM";
	private static final String MICRO_DEPOSIT_FAIL_CODE = "MDF";
	private static final String RECEIVE_REGISTRATION_FAILURE = "RRN";
	private static final String NEW_ACCT_PROTECTION_FLR = "NAF";
	private static final String TELECHECK_RETURN_CODE = "TEL";
	
	public static final ConsumerStatus PENDING_INITIAL =
		new ConsumerStatus(
			PENDING_CODE,
			INITIAL_CODE,
			true,
			false,
			false,
			true);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_0 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_0_CODE,
			true,
			false,
			false,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_1 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_1_CODE,
			true,
			true,
			false,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_2 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_2_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_3 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_3_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_4 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_4_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_5 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_5_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_NEW_ACCT_PROTECTION_FLR =
		new ConsumerStatus(
			ACTIVE_CODE,
			NEW_ACCT_PROTECTION_FLR,
			false,
			true,
			true,
			false);

	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_6 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_6_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_7 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_7_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_8 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_8_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_9 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_9_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_10 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_10_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_12 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_12_CODE,
			true,
			true,
			true,
			false);
	public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_13 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_13_CODE,
			true,
			true,
			true,
			false);

public static final ConsumerStatus ACTIVE_VALIDATION_LEVEL_14 =
		new ConsumerStatus(
			ACTIVE_CODE,
			VALIDATION_LEVEL_14_CODE,
			true,
			true,
			true,
			false);

	public static final ConsumerStatus ACTIVE_VERID_ABANDON =
		new ConsumerStatus(
			ACTIVE_CODE,
			VERID_ABANDON,
			false,
			false,
			false,
			false);
public static final ConsumerStatus NON_ACTIVE_FRAUD =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			FRAUD_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_SUSPICIOUS_ACTIVITY =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			SUSPICIOUS_ACTIVITY_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_VERID_FAIL =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			VERID_FAIL_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_PASSWORD_FAILURES =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			PASSWORD_FAILURES_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_OTHER =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			OTHER_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_USER_REQUEST =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			USER_REQUEST_CODE,
			false,
			false,
			false,
			false);

	private static final ConsumerStatus NON_ACTIVE_CHARGEBACK =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			CHARGEBACK_CODE,
			false,
			false,
			false,
			false);
	private static final ConsumerStatus NON_ACTIVE_ACH_RETURN =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			ACH_RETURN_CODE,
			false,
			false,
			false,
			false);
	private static final ConsumerStatus NON_ACTIVE_ILLEGAL_ACTIVITY =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			ILLEGAL_ACTIVITY_CODE,
			false,
			false,
			false,
			false);
	private static final ConsumerStatus NON_ACTIVE_BOUNCED_EMAIL =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			BOUNCED_EMAIL_CODE,
			false,
			false,
			false,
			false);
	public static final ConsumerStatus NON_ACTIVE_MICRO_DEPOSIT_FAIL =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			MICRO_DEPOSIT_FAIL_CODE,
			false,
			false,
			false,
			false);

	public static final ConsumerStatus NON_ACTIVE_RRN_FAIL =
		new ConsumerStatus(
			NON_ACTIVE_CODE,
			RECEIVE_REGISTRATION_FAILURE,
			false,
			false,
			false,
			false);

	public static final ConsumerStatus TELECHECK_RETURN = new ConsumerStatus(NON_ACTIVE_CODE, TELECHECK_RETURN_CODE, false, false, false, false);
	
	private static final Map statusMap = new HashMap(7);

	private final String statusCode;
	private final String subStatusCode;
	private final String combinedCode;
	private final boolean loginAllowed;
	private final boolean pendingStatus;
	private final boolean expressPayAllowed;
	private final boolean personToPersonAllowed;

	static {
		statusMap.put(PENDING_INITIAL.getCombinedCode(), PENDING_INITIAL);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_0.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_0);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_1.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_1);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_2.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_2);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_3.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_3);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_4.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_4);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_5.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_5);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_6.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_6);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_7.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_7);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_8.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_8);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_9.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_9);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_10.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_10);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_12.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_12);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_13.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_13);
		statusMap.put(
				ACTIVE_VALIDATION_LEVEL_14.getCombinedCode(),
				ACTIVE_VALIDATION_LEVEL_14);
		statusMap.put(
				ACTIVE_NEW_ACCT_PROTECTION_FLR.getCombinedCode(),
				ACTIVE_NEW_ACCT_PROTECTION_FLR);
		statusMap.put(
				ACTIVE_VERID_ABANDON.getCombinedCode(),
				ACTIVE_VERID_ABANDON);
		statusMap.put(NON_ACTIVE_FRAUD.getCombinedCode(), NON_ACTIVE_FRAUD);
		statusMap.put(
			NON_ACTIVE_SUSPICIOUS_ACTIVITY.getCombinedCode(),
			NON_ACTIVE_SUSPICIOUS_ACTIVITY);
		statusMap.put(
			NON_ACTIVE_VERID_FAIL.getCombinedCode(),
			NON_ACTIVE_VERID_FAIL);
		statusMap.put(
			NON_ACTIVE_PASSWORD_FAILURES.getCombinedCode(),
			NON_ACTIVE_PASSWORD_FAILURES);
		statusMap.put(NON_ACTIVE_OTHER.getCombinedCode(), NON_ACTIVE_OTHER);
		statusMap.put(
			NON_ACTIVE_USER_REQUEST.getCombinedCode(),
			NON_ACTIVE_USER_REQUEST);
		statusMap.put(
			NON_ACTIVE_CHARGEBACK.getCombinedCode(),
			NON_ACTIVE_CHARGEBACK);
		statusMap.put(
			NON_ACTIVE_ACH_RETURN.getCombinedCode(),
			NON_ACTIVE_ACH_RETURN);
		statusMap.put(
			NON_ACTIVE_ILLEGAL_ACTIVITY.getCombinedCode(),
			NON_ACTIVE_ILLEGAL_ACTIVITY);
		statusMap.put(
			NON_ACTIVE_BOUNCED_EMAIL.getCombinedCode(),
			NON_ACTIVE_BOUNCED_EMAIL);
		statusMap.put(
			NON_ACTIVE_MICRO_DEPOSIT_FAIL.getCombinedCode(),
			NON_ACTIVE_MICRO_DEPOSIT_FAIL);
		statusMap.put(
		        NON_ACTIVE_RRN_FAIL.getCombinedCode(),
		        NON_ACTIVE_RRN_FAIL);
		statusMap.put(TELECHECK_RETURN.getCombinedCode(), TELECHECK_RETURN);
	}

	private ConsumerStatus(
		String statusCode,
		String subStatusCode,
		boolean loginAllowed,
		boolean expressPayAllowed,
		boolean personToPersonAllowed,
		boolean pendingStatus)
	{
		this.statusCode = statusCode;
		this.subStatusCode = subStatusCode;
		this.combinedCode = statusCode + DELIMITER + subStatusCode;
		this.loginAllowed = loginAllowed;
		this.expressPayAllowed = expressPayAllowed;
		this.personToPersonAllowed = personToPersonAllowed;
		this.pendingStatus = pendingStatus;
	}

	public static ConsumerStatus getInstance(
		String statusCode,
		String subStatusCode)
	{
		String key = statusCode + DELIMITER + subStatusCode;
		return getInstanceByCombinedCode(key);
	}

	/**
	 * 
	 * @param combinedCode	- statusCode + DELIMITER + subStatusCode
	 * @return
	 * 
	 * Created on Feb 28, 2005
	 */
	public static ConsumerStatus getInstanceByCombinedCode(String combinedCode)
	{
		ConsumerStatus status = (ConsumerStatus) statusMap.get(combinedCode);

/*		if (status == null)
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] {
						combinedCode,
						"statusCode" + DELIMITER + "subStatusCode" });
			throw new IllegalArgumentException(errorMessage);
		}*/

		return (status);
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public String getCombinedCode()
	{
		return combinedCode;
	}

	public boolean isLoginAllowed()
	{
		return loginAllowed;
	}

	public boolean isPendingStatus()
	{
		return pendingStatus;
	}

	public boolean isExpressPayAllowed()
	{
		return expressPayAllowed;
	}

	public boolean isPersonToPersonAllowed()
	{
		return personToPersonAllowed;
	}

	public boolean isActiveStatus()
	{
		return ACTIVE_CODE.equalsIgnoreCase(this.statusCode);
	}

	public String toString()
	{
		return combinedCode;
	}
}
