/*
 * Created on June 15, 2005
 *
 */
package emgshared.model;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class TransactionType 
{

	public static final String MONEY_TRANSFER_SEND_CODE = "MGSEND";
	public static final String EXPRESS_PAYMENT_SEND_CODE = "EPSEND";
	public static final String MONEY_TRANSFER_ECONOMY_SEND_CODE = "ESSEND";
	public static final String MONEY_TRANSFER_CASH_TRANSACTION_CODE = "MGCASH";
	public static final String DELAY_SEND_CODE = "DSSEND";

	public static final TransactionType MONEY_TRANSFER_SEND =
		new TransactionType(MONEY_TRANSFER_SEND_CODE);
	public static final TransactionType EXPRESS_PAYMENT_SEND =
		new TransactionType(EXPRESS_PAYMENT_SEND_CODE);
	public static final TransactionType MONEY_TRANSFER_ECONOMY_SEND =
		new TransactionType(MONEY_TRANSFER_ECONOMY_SEND_CODE);
	public static final TransactionType DELAY_SEND =
		new TransactionType(DELAY_SEND_CODE);

	private final String typeCode;

	public TransactionType(String type)
	{
		this.typeCode = type;
	}

	public static TransactionType getInstance(String typeCode)
	{
 		TransactionType type;
		if (MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(typeCode)) {
			type = MONEY_TRANSFER_SEND;
		} else if (EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(typeCode)) {
			type = EXPRESS_PAYMENT_SEND;
		} else if (MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(typeCode)) {
			type = MONEY_TRANSFER_ECONOMY_SEND;
		} else if (DELAY_SEND_CODE.equalsIgnoreCase(typeCode)) {
			type = DELAY_SEND;
		} else { 
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { typeCode, "type" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (type);
	}

	public String getStatusCode()
	{
		return typeCode;
	}

	public boolean isMoneyTransferSend()
	{
		return MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(this.typeCode);
	}
	public boolean isExpressPaymentSend()
	{
		return EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(this.typeCode);
	}
	public boolean isMoneyTransferEconomySend()
	{
		return MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(this.typeCode);
	}

	public String toString()
	{
		return typeCode;
	}

}