/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

/**
 * @author A131
 *
 */
public class TransactionComment extends Comment
{
	private int tranId;

	public int getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i;
	}
}
