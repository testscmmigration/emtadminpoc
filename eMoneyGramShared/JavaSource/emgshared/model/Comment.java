/*
 * Created on Mar 2, 2005
 *
 */
package emgshared.model;

import java.util.Date;

/**
 * @author A131
 *
 */
public class Comment implements Comparable
{
	private int id;
	private String reasonCode;
	private String reasonDescription;
	private String text;
	private Date createDate;
	private String createdBy;

	public Date getCreateDate()
	{
		return createDate;
	}

	public int getId()
	{
		return id;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public String getReasonDescription()
	{
		return reasonDescription;
	}

	public String getText()
	{
		return text;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreateDate(Date date)
	{
		createDate = date;
	}

	public void setId(int i)
	{
		id = i;
	}

	public void setReasonCode(String string)
	{
		reasonCode = string.trim();
	}

	public void setReasonDescription(String string)
	{
		reasonDescription = string;
	}

	public void setText(String string)
	{
		text = string;
	}

	public void setCreatedBy(String string)
	{
		createdBy = string;
	}

	public boolean equals(Object object)
	{
		Comment comment = (Comment) object;
		return comment.getCreateDate().equals(getCreateDate());
	}

	public int compareTo(Object object)
	{
		Comment comment = (Comment) object;
		return comment.getCreateDate().compareTo(getCreateDate());
	}
}
