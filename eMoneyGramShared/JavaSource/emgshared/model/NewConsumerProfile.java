/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.model;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author A131
 *
 */
public class NewConsumerProfile
{
	private static final int NEW_CONSUMER_ID = -1;
	private static final int NEW_CONSUMER_ADDRESS_ID = -1;
	private String password;
	private String verificationQuestionId;
	private String verificationAnswer;
	private TaintIndicatorType ssnTaintCode;
	private TaintIndicatorType phoneTaintCode;
	private TaintIndicatorType phoneAlternateTaintCode;
	private TaintIndicatorType emailTaintCode;
	private TaintIndicatorType emailDomainTaintCode;
	private String createIpAddrId;
	
	private ConsumerProfile profile;

	public NewConsumerProfile(String userId, String ssnLast4)
	{
		profile =
			new ConsumerProfile(
				NEW_CONSUMER_ID,
				ssnLast4,
				ConsumerStatus.ACTIVE_VALIDATION_LEVEL_1,
				new ConsumerAddress(NEW_CONSUMER_ADDRESS_ID, NEW_CONSUMER_ID),
				TaintIndicatorType.NOT_BLOCKED,
				TaintIndicatorType.NOT_BLOCKED,
				TaintIndicatorType.NOT_BLOCKED,
				TaintIndicatorType.NOT_BLOCKED,
				TaintIndicatorType.NOT_BLOCKED);
		profile.setUserId(userId);
	}

	public String getAddressLine1()
	{
		return profile.getAddressLine1();
	}

	public String getAddressLine2()
	{
		return profile.getAddressLine2();
	}

	public Date getBirthdate()
	{
		return profile.getBirthdate();
	}

	public String getGuid()
	{
		return profile.getGuid();
	}

	public String getCity()
	{
		return profile.getCity();
	}

	public Collection getEmails()
	{
		return profile.getEmails();
	}

	public String getFirstName()
	{
		return profile.getFirstName();
	}

	public int getId()
	{
		return profile.getId();
	}

	public String getIsoCountryCode()
	{
		return profile.getIsoCountryCode();
	}
	
	public String getCountyName() {
		return profile.getCountyName();
	}
	
	public String getBuildingName() {
		return profile.getBuildingName();
	}
	
	public String getPhoneNumberType() {
		return profile.getPhoneNumberType();
	}

	public String getLastName()
	{
		return profile.getLastName();
	}

	public String getMiddleName()
	{
		return profile.getMiddleName();
	}

	public String getPostalCode()
	{
		return profile.getPostalCode();
	}

	public String getPostalCodePlusZip4()
	{
		return profile.getPostalCodePlusZip4();
	}

	public String getState()
	{
		return profile.getState();
	}

	public String getUserId()
	{
		return profile.getUserId();
	}

	public ConsumerStatus getStatus()
	{
		return profile.getStatus();
	}

	public String getStatusCode()
	{
		return profile.getStatusCode();
	}

	public boolean isAcceptPromotionalEmail()
	{
		return profile.isAcceptPromotionalEmail();
	}

	public void setAcceptPromotionalEmail(boolean b)
	{
		profile.setAcceptPromotionalEmail(b);
	}

	public void setAddressLine1(String string)
	{
		profile.setAddressLine1(string);
	}

	public void setAddressLine2(String string)
	{
		profile.setAddressLine2(string);
	}

	public void setBirthdate(Date date)
	{
		profile.setBirthdate(date);
	}

	public void setGuid(String string)
	{
		profile.setGuid(string);
	}

	public void setCity(String string)
	{
		profile.setCity(string);
	}

	public void setEmails(Collection emails)
	{
		profile.setEmails(emails);
	}

	public void setFirstName(String string)
	{
		profile.setFirstName(string);
	}

	public void setIsoCountryCode(String code)
	{
		profile.setIsoCountryCode(code);
	}
	
	public void setCountyName(String string)
	{
		profile.setCountyName(string);
	}
	
	public void setBuildingName(String string)
	{
		profile.setBuildingName(string);
	}
	
	public void setPhoneNumberType(String string) {
		profile.setPhoneNumberType(string);
	}

	public void setLastName(String string)
	{
		profile.setLastName(string);
	}

	public void setMiddleName(String string)
	{
		profile.setMiddleName(string);
	}

	public void setPostalCode(String string)
	{
		profile.setPostalCode(string);
	}

	public void setState(String string)
	{
		profile.setState(string);
	}

	public void setUserId(String string)
	{
		profile.setUserId(string);
	}

	public String getPhoneNumber()
	{
		return profile.getPhoneNumber();
	}

	public String getPhoneNumberAlternate()
	{
		return profile.getPhoneNumberAlternate();
	}

	public void setPhoneNumber(String string)
	{
		profile.setPhoneNumber(string);
	}

	public void setPhoneNumberAlternate(String string)
	{
		profile.setPhoneNumberAlternate(string);
	}

	public String getZip4()
	{
		return profile.getZip4();
	}

	public void setZip4(String string)
	{
		profile.setZip4(string);
	}

	public ConsumerAddress getAddress()
	{
		return profile.getAddress();
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String string)
	{
		password = StringUtils.trimToNull(string);
	}

	public String getVerificationAnswer()
	{
		return verificationAnswer;
	}

	public String getVerificationQuestionId()
	{
		return verificationQuestionId;
	}

	public void setVerificationAnswer(String string)
	{
		verificationAnswer = StringUtils.trimToNull(string);
	}

	public void setVerificationQuestionId(String string)
	{
		verificationQuestionId = StringUtils.trimToNull(string);
	}

	public String getSubStatusCode()
	{
		return profile.getSubStatusCode();
	}

	public String getSsnLast4()
	{
		return profile.getSsnLast4();
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getPhoneTaintCode()
	{
		return phoneTaintCode;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getSsnTaintCode()
	{
		return ssnTaintCode;
	}

	/**
	 * @param type
	 */
	public void setPhoneTaintCode(TaintIndicatorType type)
	{
		phoneTaintCode = type;
	}

	/**
	 * @param type
	 */
	public void setSsnTaintCode(TaintIndicatorType type)
	{
		ssnTaintCode = type;
	}
			
	/**
	 * @return
	 */
	public TaintIndicatorType getEmailTaintCode()
	{
		return emailTaintCode;
	}

	/**
	 * @param type
	 */
	public void setEmailTaintCode(TaintIndicatorType type)
	{
		emailTaintCode = type;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getEmailDomainTaintCode()
	{
		return emailDomainTaintCode;
	}

	/**
	 * @param type
	 */
	public void setEmailDomainTaintCode(TaintIndicatorType type)
	{
		emailDomainTaintCode = type;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getPhoneAlternateTaintCode()
	{
		return phoneAlternateTaintCode;
	}

	/**
	 * @param type
	 */
	public void setPhoneAlternateTaintCode(TaintIndicatorType type)
	{
		phoneAlternateTaintCode = type;
	}

	/**
	 * @return
	 */
	public String getCreateIpAddrId()
	{
		return createIpAddrId;
	}

	/**
	 * @param string
	 */
	public void setCreateIpAddrId(String string)
	{
		createIpAddrId = string;
	}
	
	public String getEdirGuid(){
		return profile.getEdirGuid();
	}
	
	public void setEdirGuid(String edirGuid){
		profile.setEdirGuid(edirGuid);
	}

	public String getLoyaltyPgmMembershipId(){
		return profile.getLoyaltyPgmMembershipId();
	}
	
	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		profile.setLoyaltyPgmMembershipId(loyaltyPgmMembershipId);
	}

}
