/*
 * Created on Sept 12, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;

import emgshared.util.I18NHelper;

/**
 * @author T349
 *
 */
public class ExchangeRateType implements Serializable
{
//	TODO tom c. - IMPLEMENT EQUALS AND HASH CODE ON THIS ENUM
	public static final String USD_CODE = "USD";

	public static final ExchangeRateType USD = new ExchangeRateType(USD_CODE);

	private final String typeCode;

	public ExchangeRateType(String type)
	{
		this.typeCode = type;
	}

	public static ExchangeRateType getInstance(String statusCode)
	{
		ExchangeRateType exchangeRateType;
		if (USD_CODE.equalsIgnoreCase(statusCode))
		{
			exchangeRateType = USD;
		} else
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] { statusCode, "code" });
			throw new IllegalArgumentException(errorMessage);
		}
		return (exchangeRateType);
	}

	public String getTypeCode()
	{
		return typeCode;
	}

	public boolean isEmailType()
	{
		return USD_CODE.equalsIgnoreCase(this.typeCode);
	}

	public String toString()
	{
		return typeCode;
	}
}
