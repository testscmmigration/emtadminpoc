package emgshared.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

public class BillerLimit {

	private String id;
	private String receiveCode;
	private String name;
	private String city;
	private String state;
//	private String limit;
	private float limit;
	private String status;
	private String lagacyId;
	private String paymentProfileId;
	private static NumberFormat nf=new DecimalFormat("#0.00");

	public String getId() {
		return id == null ? "" : id;
	}

	public void setId(String string) {
		id = string;
	}

	public String getReceiveCode() {
		return receiveCode == null ? "" : receiveCode;
	}

	public void setReceiveCode(String string) {
		receiveCode = string;
	}

	public String getName() {
		return name == null ? "" : name;
	}

	public void setName(String string) {
		name = string;
	}

	public String getCity() {
		return city == null ? "" : city;
	}

	public void setCity(String string) {
		city = string;
	}

	public String getState() {
		return state == null ? "" : state;
	}

	public void setState(String string) {
		state = string;
	}

	public String getLimit() {
		return nf.format(limit);
	}

	public void setLimit(String string) throws ParseException {
		limit = nf.parse(string).floatValue();
	}

	public String getStatus() {
		return status == null ? "" : status;
	}

	public void setStatus(String string) {
		status = string;
	}

	public String getLagacyId() {
		return lagacyId == null ? "" : lagacyId;
	}

	public void setLagacyId(String string) {
		lagacyId = string;
	}
	/**
	 * @return
	 */
	public float getLimitNbr()
	{
		return limit;
	}
	
	/**
	 * @param f
	 */
	public void setLimitNbr(float f)
	{
		limit = f;
	}

	public String getPaymentProfileId() {
		return paymentProfileId;
	}

	public void setPaymentProfileId(String paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}

}
