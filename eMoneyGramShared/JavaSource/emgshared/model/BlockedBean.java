package emgshared.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlockedBean 
	implements Comparable {

	public static final String SORT_BY_CLEAR_TEXT = "clearText";
	public static final String SORT_BY_HASH_TEXT = "hashText";
	public static final String SORT_BY_ENCRYPTED_NUMBER = "encryptedNumber";
	public static final String SORT_BY_MASKTEXT = "maskText";
	public static final String SORT_BY_ABA_NUMBER = "abaNumber";
	public static String sortBy = "clearText";

	private String clearText;
	private String hashText;
	private String encryptedNumber;
	private String blockedId;
	private String maskText;
	private String binText;
	private int    acctNumberLength;
	private String abaNumber; //Only for Blocked Bank Account
	private String elecAddrTypeCode; //Only for Blocked Email Address
	private String addrFmtCode; //Only for Blocked Email Address
	private String reasonCode;	
	private String reasonDesc;
	private String fraudRiskLevelCode;
	private String comment;
	private String statusCode;
	private String statusDesc;
	private String createDate;
	private String createUserId;
	private String statusUpdateDate;
	private String updateDate;
	private String updateUserId;

	public String getAbaNumber() {
		return abaNumber;
	}

	public String getComment() {
		return comment;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public String getEncryptedNumber() {
		return encryptedNumber;
	}
	
	public String getBlockedId() {
		return blockedId;
	}

	public String getHashText() {
		return hashText;
	}

	public String getMaskText() {
		return maskText;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public String getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setAbaNumber(String string) {
		abaNumber = string;
	}

	public void setComment(String string) {
		comment = string;
	}

	public void setCreateDate(String string) {
		createDate = format(string);
	}

	public void setCreateUserId(String string) {
		createUserId = string;
	}

	public void setEncryptedNumber(String string) {
		encryptedNumber = string;
	}

	public void setBlockedId(String string) {
		blockedId = string;
	}

	public void setHashText(String string) {
		hashText = string;
	}

	public void setMaskText(String string) {
		maskText = string;
	}

	public void setReasonCode(String string) {
		reasonCode = string;
	}

	public void setReasonDesc(String string) {
		reasonDesc = string;
	}

	public void setStatusCode(String string) {
		statusCode = string;
	}

	public void setStatusUpdateDate(String string) {
		statusUpdateDate = format(string);
	}

	public void setUpdateUserId(String string) {
		updateUserId = string;
	}

	public String getFraudRiskLevelCode() {
		return fraudRiskLevelCode;
	}

	public void setFraudRiskLevelCode(String string) {
		fraudRiskLevelCode = string;
	}

	public String getClearText() {
		return clearText;
	}

	public void setClearText(String string) {
		clearText = string;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String string) {
		statusDesc = string;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String string) {
		updateDate = format(string);
	}

	public int compareTo(Object o) {
		BlockedBean other = (BlockedBean) o;
		
		if (sortBy.equals(SORT_BY_ABA_NUMBER)) {
			return compareStrings(abaNumber, other.abaNumber);
		} else if (sortBy.equals(SORT_BY_CLEAR_TEXT)) {
			return compareStrings(clearText, other.clearText);
		} else if (sortBy.equals(SORT_BY_ENCRYPTED_NUMBER)) {
			return compareStrings(encryptedNumber, other.encryptedNumber);
		} else if (sortBy.equals(SORT_BY_HASH_TEXT)) {
			return compareStrings(hashText, other.hashText);
		} else if (sortBy.equals(SORT_BY_MASKTEXT)) {
			return compareStrings(maskText, other.maskText);
		}
		
		return 0;
	}

	private int compareStrings(String str1, String str2)
	{
		if (str1 == null && str2 == null)
		{
			return 0;
		}
		
		if (str1 == null)
		{
			return -1;	
		}
		
		if (str2 == null)
		{
			return 1;
		}
		
		return str1.compareTo(str2);
	}
	
	/**
	 * @return
	 */
	public String getAddrFmtCode() {
		return addrFmtCode;
	}

	/**
	 * @return
	 */
	public String getElecAddrTypeCode() {
		return elecAddrTypeCode;
	}

	/**
	 * @param string
	 */
	public void setAddrFmtCode(String string) {
		addrFmtCode = string;
	}

	/**
	 * @param string
	 */
	public void setElecAddrTypeCode(String string) {
		elecAddrTypeCode = string;
	}

	public String getBinText() {
		return binText;
	}

	public void setBinText(String binText) {
		this.binText = binText;
	}

	public int getAcctNumberLength() {
		return acctNumberLength;
	}

	public void setAcctNumberLength(int acctNumberLength) {
		this.acctNumberLength = acctNumberLength;
	}

	public String getFormattedUpdateDate() {
		return format(updateDate);
	}

	public String getFormattedCreateDate() {
		return format(createDate) + "!!!";
	}

	public String getFormattedStatusUpdateDate() {
		return format(statusUpdateDate);
	}

	private String format(String date) {
		String[] formats = {"yyyy-mm-dd", "dd/mm/yyyy hh:mm a", "dd/mm/yyyy hh:mm:ss"};
		final int TOTAL_FORMATS = formats.length;
		DateFormat toFormat = new SimpleDateFormat("dd/MMM/yyyy");
		String formattedDate = null;
		for (int i = 0; formattedDate == null && i < TOTAL_FORMATS; ++i) {
			DateFormat fromFormat = new SimpleDateFormat(formats[i]);
			try {
				Date d = fromFormat.parse(date);
				formattedDate = toFormat.format(d);
			} catch (ParseException pde) {
				// try next format
			}
		}
		return formattedDate != null ? formattedDate : date;
	}

	@Override
	public String toString() {
		return "BlockedBean [clearText=" + clearText + ", hashText=" + hashText
				+ ", encryptedNumber=" + encryptedNumber + ", blockedId="
				+ blockedId + ", maskText=" + maskText + ", binText=" + binText
				+ ", acctNumberLength=" + acctNumberLength + ", abaNumber="
				+ abaNumber + ", elecAddrTypeCode=" + elecAddrTypeCode
				+ ", addrFmtCode=" + addrFmtCode + ", reasonCode=" + reasonCode
				+ ", reasonDesc=" + reasonDesc + ", fraudRiskLevelCode="
				+ fraudRiskLevelCode + ", comment=" + comment + ", statusCode="
				+ statusCode + ", statusDesc=" + statusDesc + ", createDate="
				+ createDate + ", createUserId=" + createUserId
				+ ", statusUpdateDate=" + statusUpdateDate + ", updateDate="
				+ updateDate + ", updateUserId=" + updateUserId + "]";
	}
	

}
