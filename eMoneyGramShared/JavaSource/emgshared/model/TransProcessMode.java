package emgshared.model;

public class TransProcessMode 
	{

	private String modeId;	
	private String modeDesc;
	
	public TransProcessMode(String id, String desc){
		this.modeId = id;
		this.modeDesc = desc;
	}
	
	/**
	 * @return
	 */
	public String getModeDesc()
	{
		return modeDesc;
	}

	/**
	 * @return
	 */
	public String getModeId()
	{
		return modeId;
	}

	/**
	 * @param string
	 */
	public void setModeDesc(String string)
	{
		modeDesc = string;
	}

	/**
	 * @param string
	 */
	public void setModeId(String string)
	{
		modeId = string;
	}

}
