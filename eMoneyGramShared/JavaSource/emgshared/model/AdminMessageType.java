package emgshared.model;

public class AdminMessageType
{
	private String adminMsgTypeCode;
	private String adminMsgTypeDesc;
	
	public AdminMessageType()
	{
	}

	public AdminMessageType(String typeCode, String typeDesc)
	{
		this.adminMsgTypeCode = typeCode;
		this.adminMsgTypeDesc = typeDesc;
	}

	public String getAdminMsgTypeCode()
	{
		return adminMsgTypeCode;
	}

	public void setAdminMsgTypeCode(String string)
	{
		adminMsgTypeCode = string;
	}

	public String getAdminMsgTypeDesc()
	{
		return adminMsgTypeDesc;
	}

	public void setAdminMsgTypeDesc(String string)
	{
		adminMsgTypeDesc = string;
	}
}
