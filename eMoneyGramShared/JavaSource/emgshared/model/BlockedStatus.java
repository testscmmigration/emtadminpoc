/*
 * Created on May 17, 2005
 *
 */
package emgshared.model;

/**
 * @author T004
 *
 */
public class BlockedStatus
{
	public static final String BLOCKED_CODE = "BLK";
	public static final String HIGH_RISK_CODE = "RSK";
	public static final String NOT_BLOCKED_CODE = "NBK";

	private String blockedTypeCode = null;
	private String blockedStatusCode = null;
	private String blockedStatusDesc = null;

	/**
	 * @return
	 */
	public String getBlockedStatusCode()
	{
		return blockedStatusCode;
	}

	/**
	 * @return
	 */
	public String getBlockedStatusDesc()
	{
		return blockedStatusDesc;
	}

	/**
	 * @return
	 */
	public String getBlockedTypeCode()
	{
		return blockedTypeCode;
	}

	/**
	 * @param string
	 */
	public void setBlockedStatusCode(String string)
	{
		blockedStatusCode = string;
	}

	/**
	 * @param string
	 */
	public void setBlockedStatusDesc(String string)
	{
		blockedStatusDesc = string;
	}

	/**
	 * @param string
	 */
	public void setBlockedTypeCode(String string)
	{
		blockedTypeCode = string;
	}

}
