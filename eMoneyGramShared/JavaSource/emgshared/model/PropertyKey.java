package emgshared.model;

public class PropertyKey
{
	private String propName;
	private String propType;
	private String propVal;

	public PropertyKey()
	{
	}

	public PropertyKey(String name, String type, String val)
	{
		this.propName = name;
		this.propType = type;
		this.propVal = val;
	}

	public String getPropName()
	{
		return propName;
	}

	public void setPropName(String string)
	{
		propName = string;
	}

	public String getPropType()
	{
		return propType;
	}

	public void setPropType(String string)
	{
		propType = string;
	}

	public String getPropVal()
	{
		return propVal == null ? "" : propVal;
	}

	public void setPropVal(String string)
	{
		propVal = string;
	}

	public int hashCode()
	{
		int total = 0;
		char[] tmp1 = propName.toLowerCase().toCharArray();
		for (int i = 0; i < tmp1.length; i++)
		{
			total += (int) tmp1[i];
		}

		char[] tmp2 = propType.toLowerCase().toCharArray();
		for (int i = 0; i < tmp2.length; i++)
		{
			total += (int) tmp2[i];
		}

		char[] tmp3 = propVal.toLowerCase().toCharArray();
		for (int i = 0; i < tmp3.length; i++)
		{
			total += (int) tmp3[i];
		}

		return total * 19 % 200;
	}

	public boolean equals(Object oth)
	{
		if (this == oth)
		{
			return true;
		}

		if (oth == null || oth.getClass() != this.getClass())
		{
			return false;
		}

		PropertyKey pk = (PropertyKey) oth;
		return this.propName.equalsIgnoreCase(pk.propName)
			&& this.propType.equalsIgnoreCase(pk.propType)
			&& getPropVal().equalsIgnoreCase(pk.getPropVal());
	}
}
