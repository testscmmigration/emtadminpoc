package emgshared.model;

/**
 * This class represents a consumer identification e.g. passport, driver license, etc.
 * @author up30
 *
 */
public class ConsumerIdentification {

	public static final String ECDL = "ECDL";
	public static final String SP = "SP";
	public static final String IPSP = "IPSP";
	public static final String SNID = "SNID";

	public static final String DRIVER_LICENSE = "Driver License";
	public static final String PASSPORT = "Passport";
	public static final String NATIONAL_ID = "National ID";
	public static final String CONSUMER_PASSPORT = "Consumer Passport Number";

	public String id;
	public String idNumber;
	public String idType;
	public String idTypeFriendlyName;
	public String docStatusCode;

	private ConsumerIdentification(String id, String idNumber, String idType, String idTypeFriendlyName, String docStatusCode) {
		super();
		this.id = id;
		this.idNumber = idNumber;
		this.idType = idType;
		this.idTypeFriendlyName = idTypeFriendlyName;
		this.docStatusCode = docStatusCode;
	}

	public static ConsumerIdentification getConsumer(String id, String idNumber, String type, String docStatusCode) {
		if (type.equalsIgnoreCase(ECDL)) {
			return new ConsumerIdentification(id, idNumber, ECDL, DRIVER_LICENSE, docStatusCode);
		} else if (type.equalsIgnoreCase(SP)) {
			return new ConsumerIdentification(id, idNumber, SP, PASSPORT, docStatusCode);
		} else if(type.equalsIgnoreCase(SNID)) {
			return new ConsumerIdentification(id, idNumber, SNID, NATIONAL_ID, docStatusCode);
		} else if (type.equalsIgnoreCase(IPSP)) {
			return new ConsumerIdentification(id, idNumber, IPSP, CONSUMER_PASSPORT, docStatusCode);
		} else {
			return new ConsumerIdentification(id, idNumber, type, type, docStatusCode);
		}
	}

	public String getIdNumber() {
		return idNumber;
	}

	public String getIdType() {
		return idType;
	}

	public String getIdTypeFriendlyName() {
		return idTypeFriendlyName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getDocStatusCode() {
		return docStatusCode;
	}

	public void setDocStatusCode(String docStatusCode) {
		this.docStatusCode = docStatusCode;
	}

}
