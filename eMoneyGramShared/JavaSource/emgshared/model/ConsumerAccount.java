/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.Validate;

/**
 * @author A131
 *
 */
public abstract class ConsumerAccount implements Serializable
{
	private int id;
	private int consumerId;
	protected ConsumerAccountType type;
	private String typeDescription;
	private int versionNumber;
	private String accountNumberMask;
	private ConsumerAddress billingAddress;
	private AccountStatus status;
	private TaintIndicatorType accountNumberTaintCode;
	private TaintIndicatorType bankAbaTaintCode;
	private TaintIndicatorType creditCardBinTaintCode;
	private List cmntList;

	public ConsumerAccount(
		int id,
		int consumerId,
		AccountStatus status,
		ConsumerAccountType type,
		String accountTypeDescription,
		String accountNumberMask,
		ConsumerAddress billingAddress,
		TaintIndicatorType accountNumberTaintCode,
		TaintIndicatorType bankAbaTaintCode,
		TaintIndicatorType creditCardBinTaintCode)
	{
		Validate.notNull(status, "status is a required parameter");
		Validate.notNull(type, "type is a required parameter");
		Validate.notNull(
			billingAddress,
			"billingAddress is a required parameter");
		this.id = id;
		this.consumerId = consumerId;
		this.status = status;
		this.type = type;
		this.typeDescription = accountTypeDescription;
		this.accountNumberMask = accountNumberMask;
		this.billingAddress = new ConsumerAddress(billingAddress);
		this.accountNumberTaintCode = accountNumberTaintCode;
		this.bankAbaTaintCode = bankAbaTaintCode;
		this.creditCardBinTaintCode = creditCardBinTaintCode;
	}

	public ConsumerAccountType getAccountType()
	{
		return type;
	}

	public boolean isCreditCardAccount()
	{
		return type.isCreditCardAccount();
	}

	public boolean isCardAccount()
	{
		return type.isCardAccount();
	}
	
	/* Commenting out as this won't be used due to CyberSource not 
	 * being able to provide us an accurate list of cards types.
	public boolean isDebitCardAccount()
	{
		return type.isDebitCardAccount();
	}
	*/
	public boolean isBankAccount()
	{
		return type.isBankAccount();
	}

	public boolean isAccountNumberBlocked()
	{
		return accountNumberTaintCode.isBlocked();
	}

	public boolean isAccountNumberOverridden()
	{
		return accountNumberTaintCode.isOverridden();
	}

	public boolean isAccountNumberNotBlocked()
	{
		return accountNumberTaintCode.isNotBlocked();
	}

	public boolean isBankAbaNumberBlocked()
	{
		return bankAbaTaintCode.isBlocked();
	}

	public boolean isBankAbaNumberOverridden()
	{
		return bankAbaTaintCode.isOverridden();
	}

	public boolean isBankAbaNumberNotBlocked()
	{
		return bankAbaTaintCode.isNotBlocked();
	}

	public boolean isCreditCardBinBlocked()
	{
		return creditCardBinTaintCode.isBlocked();
	}

	public boolean isCreditCardBinOverridden()
	{
		return creditCardBinTaintCode.isOverridden();
	}

	public boolean isCreditCardBinNotBlocked()
	{
		return creditCardBinTaintCode.isNotBlocked();
	}

	public int getConsumerId()
	{
		return consumerId;
	}

	public int getId()
	{
		return id;
	}

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public AccountStatus getStatus()
	{
		return status;
	}

	public int getVersionNumber()
	{
		return versionNumber;
	}

	public void setStatus(AccountStatus newStatus)
	{
		Validate.notNull(newStatus, "status is a required parameter");
		status = newStatus;
	}

	public void setVersionNumber(int i)
	{
		versionNumber = i;
	}

	public int getAddressId()
	{
		return billingAddress.getAddressId();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine1()
	{
		return billingAddress.getAddressLine1();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getAddressLine2()
	{
		return billingAddress.getAddressLine2();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getCity()
	{
		return billingAddress.getCity();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getIsoCountryCode()
	{
		return billingAddress.getIsoCountryCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getPostalCode()
	{
		return billingAddress.getPostalCode();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getPostalCodePlusZip4()
	{
		return billingAddress.getPostalCodePlusZip4();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getState()
	{
		return billingAddress.getState();
	}

	/**
	 * @return
	 *
	 * Created on Jan 20, 2005
	 */
	public String getZip4()
	{
		return billingAddress.getZip4();
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine1(String string)
	{
		billingAddress.setAddressLine1(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine2(String string)
	{
		billingAddress.setAddressLine2(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setAddressLine3(String string)
	{
		billingAddress.setAddressLine3(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setCity(String string)
	{
		billingAddress.setCity(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setIsoCountryCode(String string)
	{
		billingAddress.setIsoCountryCode(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setPostalCode(String string)
	{
		billingAddress.setPostalCode(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setState(String string)
	{
		billingAddress.setState(string);
	}

	/**
	 * @param string
	 *
	 * Created on Jan 20, 2005
	 */
	public void setZip4(String string)
	{
		billingAddress.setZip4(string);
	}

	/**
	 * @return
	 *
	 * Created on Jan 21, 2005
	 */
	public String getTypeDescription()
	{
		return typeDescription;
	}

	/**
	 *
	 * @return
	 */
	public String getDisplayName()
	{
		return "* * * * * " + this.getAccountNumberMask();
	}

	/**
	 * Subclasses might need to override this methide.
	 * @return
	 */
	public boolean isValid()
	{
		return true;
	}

	/**
	 * @return copy of billing address
	 *
	 * Created on Feb 8, 2005
	 */
	public ConsumerAddress getBillingAddress()
	{
		return new ConsumerAddress(this.billingAddress);
	}

	public String getStatusCode()
	{
		return status.getStatusCode();
	}

	public String getSubStatusCode()
	{
		return status.getSubStatusCode();
	}

	public ConsumerAccountType getType()
	{
		return type;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getAccountNumberTaintCode()
	{
		return accountNumberTaintCode;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getBankAbaTaintCode()
	{
		return bankAbaTaintCode;
	}

	/**
	 * @return
	 */
	public TaintIndicatorType getCreditCardBinTaintCode()
	{
		return creditCardBinTaintCode;
	}

	/**
	 * @param type
	 */
	public void setAccountNumberTaintCode(TaintIndicatorType type)
	{
		if (type == null)
		{
			accountNumberTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else
		{
			accountNumberTaintCode = type;
		}
	}

	/**
	 * @param type
	 */
	public void setBankAbaTaintCode(TaintIndicatorType type)
	{
		if (type == null)
		{
			bankAbaTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else
		{
			bankAbaTaintCode = type;
		}
	}

	/**
	 * @param type
	 */
	public void setCreditCardBinTaintCode(TaintIndicatorType type)
	{
		if (type == null)
		{
			creditCardBinTaintCode = TaintIndicatorType.NOT_BLOCKED;
		} else
		{
			creditCardBinTaintCode = type;
		}
	}

	/**
	 * @return
	 */
	public List getCmntList()
	{
		return cmntList;
	}

	/**
	 * @param list
	 */
	public void setCmntList(List list)
	{
		cmntList = list;
	}

}
