/*
 * Created on Aug 1, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package emgshared.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * @author A119
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserActivityLog {

	private UserActivityType userActivityType;
	private String userId;
	private String applicationName;
	private String IPAddress;
	private Date eventDate;
	private String detailText;
	private String dataKeyValue;
	public static final String dataKeyValueSeperator = "|";
	private int dataKeyValueMaxLength = 128;
	public static final String APPLICATION_CONSUMER = "CONSUMER";
	public static final String APPLICATION_ADMIN = "ADMIN";
	public static final String APPLICATION_BATCH = "BATCH";
    /**
     * @return Returns the detailText.
     */
    public String getDetailText() {
        return detailText;
    }
    /**
     * @param detailText The detailText to set.
     */
    public void setDetailText(String dt) {
        this.detailText = dt;
    }
    /**
     * @return Returns the eventDate.
     */
    public Date getEventDate() {
        return eventDate;
    }
    /**
     * @param eventDate The eventDate to set.
     */
    public void setEventDate(Date ed) {
        this.eventDate = ed;
    }
    /**
     * @return Returns the iPAddress.
     */
    public String getIPAddress() {
        return IPAddress;
    }
    /**
     * @param address The iPAddress to set.
     */
    public void setIPAddress(String ip) {
        IPAddress = ip;
    }
    /**
     * @return Returns the userActivityType.
     */
    public UserActivityType getUserActivityType() {
        return userActivityType;
    }
    /**
     * @param userActivityType The userActivityType to set.
     */
    public void setUserActivityType(UserActivityType uat) {
        this.userActivityType = uat;
    }
    /**
     * @return Returns the userId.
     */
    public String getUserId() {
        return userId;
    }
    /**
     * @param userId The userId to set.
     */
    public void setUserId(String uid) {
        this.userId = uid;
    }
    /**
     * @return Returns the applicationName.
     */
    public String getApplicationName() {
        return applicationName;
    }
    /**
     * @param applicationName The applicationName to set.
     */
    public void setApplicationName(String an) {
        if (! ((an.equals(APPLICATION_ADMIN)) ||
               (an.equals(APPLICATION_BATCH)) ||
               (an.equals(APPLICATION_CONSUMER))) ) 
            this.applicationName = APPLICATION_ADMIN;
        else
            this.applicationName = an;
    }
    /**
     * @return Returns the dataKeyValue.
     */
    public String getDataKeyValue() {
        return dataKeyValue;
    }
    
    
    /**
     * @param dataKeyValue The dataKeyValue to set.
     */
    public void setDataKeyValue(ArrayList dataKeyValues) throws Exception 
    {    
        StringBuffer keyBuffer = new StringBuffer(128);
        int j = 0;
        try {
            for (Iterator i = dataKeyValues.iterator(); i.hasNext();) {
                String keyValue = (String) i.next();
                j++;
                if (j > 1) keyBuffer.append(dataKeyValueSeperator);
                if (keyValue.indexOf(dataKeyValueSeperator) > -1)
                    throw new Exception("Data key contains seperator value.  Invalid key:" + keyValue);
                keyBuffer.append(keyValue);
            }
            if (keyBuffer.length() > this.dataKeyValueMaxLength)
                throw new Exception("Data Key Value exceeds max lenght of: " + this.dataKeyValueMaxLength);
            else
                this.dataKeyValue = keyBuffer.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * @param dataKeyValue The dataKeyValue to set.
     */
    public void setDataKeyValue(String dataKeyValues) throws Exception 
    {    
        this.dataKeyValue = dataKeyValues;
    }


}

