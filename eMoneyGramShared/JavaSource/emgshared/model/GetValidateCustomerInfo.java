package emgshared.model;

import java.util.Date;
import java.util.List;

/*
 * @author vy79
 */

public class GetValidateCustomerInfo {
	private Long custId;
	private String authnId;
	private Date authnDate;
	private String scoreNbr;
	private String deciBandText;
	private String remarksText;
	private String statText;
	private String chkTypeCode;
	private String chkTypeDesc;
	private String rsltCode;
	private String rsltDesc;
	private String rsltSvrtyText;
	private List<GetValidateCustomerInfo> validateCustomerInfo;
	private static final int COMMENT_RANGE[]={0,999};
	private static final int MATCH_RANGE[]={1000,3999};
	private static final int WARNING_RANGE[]={4000,6999};
	private static final int MISMATCH_RANGE[]={7000,9999};
	private static final int MIN=0;
	private static final int MAX=1;
	public Long getCustId() {
		return custId;
	}
	public void setCustId(Long custId) {
		this.custId = custId;
	}
	public String getAuthnId() {
		return authnId;
	}
	public void setAuthnId(String authnId) {
		this.authnId = authnId;
	}
	public Date getAuthnDate() {
		return authnDate;
	}
	public void setAuthnDate(Date authnDate) {
		this.authnDate = authnDate;
	}
	public String getScoreNbr() {
		return scoreNbr;
	}
	public void setScoreNbr(String scoreNbr) {
		this.scoreNbr = scoreNbr;
	}
	public String getDeciBandText() {
		return deciBandText;
	}
	public void setDeciBandText(String deciBandText) {
		this.deciBandText = deciBandText;
	}
	public String getRemarksText() {
		return remarksText;
	}
	public void setRemarksText(String remarksText) {
		this.remarksText = remarksText;
	}
	public String getStatText() {
		return statText;
	}
	public void setStatText(String statText) {
		this.statText = statText;
	}
	public String getChkTypeCode() {
		return chkTypeCode;
	}
	public void setChkTypeCode(String chkTypeCode) {
		this.chkTypeCode = chkTypeCode;
	}
	public String getChkTypeDesc() {
		return chkTypeDesc;
	}
	public void setChkTypeDesc(String chkTypeDesc) {
		this.chkTypeDesc = chkTypeDesc;
	}
	public String getRsltCode() {
		return rsltCode;
	}
	public void setRsltCode(String rsltCode) {
		this.rsltCode = rsltCode;
	}
	public String getRsltDesc() {
		return rsltDesc;
	}
	public void setRsltDesc(String rsltDesc) {
		this.rsltDesc = rsltDesc;
	}
	public String getRsltSvrtyText() {
		return rsltSvrtyText;
	}
	public void setRsltSvrtyText(String rsltSvrtyText) {
		if(rsltSvrtyText!=null){
			this.rsltSvrtyText = rsltSvrtyText.toUpperCase();
		}
	}
	public List<GetValidateCustomerInfo> getValidateCustomerInfo() {
		return validateCustomerInfo;
	}
	public void setValidateCustomerInfo(List<GetValidateCustomerInfo> getValidateCustomerInfo) {
		this.validateCustomerInfo = getValidateCustomerInfo;
	}
	public String getLinkText(){
		String result = "GB Group ID3|%d|AuthID: %s|Score: %s|Decision: %s";
		return String.format(result, getCustId()==null?"":getCustId(), getAuthnId()==null?"":getAuthnId(), getScoreNbr()==null?"":getScoreNbr(), 
				getDeciBandText()==null?"":getDeciBandText());
	}
	public boolean isInCommentRange(){
		int resultCode=Integer.valueOf(getRsltCode());
		if(resultCode>=COMMENT_RANGE[MIN]&&resultCode<=COMMENT_RANGE[MAX]){
			return true;
		}
		return false;
	}
	public boolean isInMatchRange(){
		int resultCode=Integer.valueOf(getRsltCode());
		if(resultCode>=MATCH_RANGE[MIN]&&resultCode<=MATCH_RANGE[MAX]){
			return true;
		}
		return false;
	}
	public boolean isInWarningRange(){
		int resultCode=Integer.valueOf(getRsltCode());
		if(resultCode>=WARNING_RANGE[MIN]&&resultCode<=WARNING_RANGE[MAX]){
			return true;
		}
		return false;
	}
	public boolean isInMismatchRange(){
		int resultCode=Integer.valueOf(getRsltCode());
		if(resultCode>=MISMATCH_RANGE[MIN]&&resultCode<=MISMATCH_RANGE[MAX]){
			return true;
		}
		return false;
	}
}
