/*
 * Created on Dec 31, 2004
 *
 */
package emgshared.model;

import java.util.Date;
import org.apache.commons.lang.StringUtils;

/**
 * @author A131
 *
 */
public class ConsumerAddress extends BaseAddress implements Comparable
{
	private int consumerId;
	private int addressId;
	private String statusCode;
	private Date created;
	private String createdBy;

	public ConsumerAddress(int addressId, int consumerId)
	{
		this(addressId, consumerId, null, null, null);
	}

	public ConsumerAddress(
		int addressId,
		int consumerId,
		String statusCode,
		Date created,
		String createdBy)
	{
		this.consumerId = consumerId;
		this.addressId = addressId;
		this.statusCode = StringUtils.trimToNull(statusCode);
		this.created = created == null ? null : new Date(created.getTime());
		this.createdBy = StringUtils.trimToNull(createdBy);
	}

	/**
	 * Copy constructor.
	 * 
	 * @param source	- must not be null
	 * @throws IllegalArgumentException
	 */
	public ConsumerAddress(ConsumerAddress source)
	{
		super(source);
		this.addressId = source.getAddressId();
		this.consumerId = source.getConsumerId();
		this.statusCode = source.getStatusCode();
		this.created = source.getCreated();
		this.createdBy = source.getCreatedBy();
	}

	public int getConsumerId()
	{
		return consumerId;
	}

	public Date getCreated()
	{
		return created == null ? null : new Date(this.created.getTime());
	}
	public void setCreated(Date createDate)
	{
		this.created = createDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}
	
	public void setCreatedBy(String createdby)
	{
		this.createdBy = createdby;
	}

	public int getAddressId()
	{
		return addressId;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;

		if (!(obj instanceof ConsumerAddress))
			return false;

		ConsumerAddress other = (ConsumerAddress) obj;

		return this.consumerId == other.consumerId
			&& this.addressId == other.addressId;
	}

	public int hashCode()
	{
		int hash = 7;
		hash = hash * 13 + this.consumerId;
		hash = hash * 13 + this.addressId;
		return hash;
	}
	
	public int compareTo(Object object)
	{
		ConsumerAddress ca = (ConsumerAddress) object;
		Integer addId = new Integer(ca.getAddressId());
		return addId.compareTo(new Integer(getAddressId()));
	}

}
