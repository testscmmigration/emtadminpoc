package emgshared.model;

import java.util.Date;

public class Activity implements Comparable
{
	public static final int FIRST_TRANSACTION_STARTED_CODE = 1;
	public static final int KBA_PASSED_CODE = 2;
	public static final int KBA_BAILER_1_SENT_CODE = 3;
	public static final int KBA_BAILER_2_SENT_CODE = 4;
	public static final int RSA_BAILER_1_SENT_CODE = 5;
	public static final int RSA_BAILER_2_SENT_CODE = 6;
	public static final int TRANSACTION_REVIEW_BAILER_1_SENT_CODE = 7;
	public static final int TRANSACTION_REVIEW_BAILER_2_SENT_CODE = 8;
	public static final int FIRST_TRANSACTION_COMPLETED_CODE = 9;
	public static final int PASSWORD_CHANGED_CODE = 10;
	public static final int PROFILE_UNLOCKED_CODE = 11;
	public static final int PROFILE_MASTER_TAINT_CLEAR_CODE = 12;
	public static final int PROFILE_RECREATE_LDAP_ENTRY_CODE = 13;
	private Date activityDate;
	private String activityBusinessDescription;
	private String activityBusinessCategory;
	private String createdBy;
	private Integer activityLogCode;
	

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getActivityBusinessDescription() {
		return activityBusinessDescription;
	}

	public void setActivityBusinessDescription(String activityBusinessDescription) {
		this.activityBusinessDescription = activityBusinessDescription;
	}

	public Integer getActivityLogCode() {
		return activityLogCode;
	}

	public void setActivityLogCode(Integer activityLogCode) {
		this.activityLogCode = activityLogCode;
	}

	public String getActivityBusinessCategory() {
		return activityBusinessCategory;
	}

	public void setActivityBusinessCategory(String activityBusinessCategory) {
		this.activityBusinessCategory = activityBusinessCategory;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean equals(Object object)
	{
		Activity activity = (Activity) object;
		return activity.getActivityDate().equals(getActivityDate());
	}

	public int compareTo(Object object)
	{
		Activity activity = (Activity) object;
		return activity.getActivityDate().compareTo(getActivityDate());
	}
}
