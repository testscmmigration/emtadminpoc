/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
public class AccountStatus implements Serializable
{
	public static final String DELIMITER = ":";

	public static final String ACTIVE_CODE = "ACT";
	private static final String NON_ACTIVE_CODE = "NAT";
	private static final String DELETED_CODE = "DEL";

	private static final String VALIDATION_LEVEL_0_CODE = "L00";
	private static final String VALIDATION_LEVEL_1_CODE = "L01";
	private static final String VALIDATION_LEVEL_2_CODE = "L02";
	private static final String FAILURE_CODE = "FLR";
	private static final String FRAUD_CODE = "FRD";
	private static final String DELETED_SUB_CODE = "DEL";

	public static final AccountStatus ACTIVE_VALIDATION_LEVEL_0 =
		new AccountStatus(ACTIVE_CODE, VALIDATION_LEVEL_0_CODE);
	public static final AccountStatus ACTIVE_VALIDATION_LEVEL_1 =
		new AccountStatus(ACTIVE_CODE, VALIDATION_LEVEL_1_CODE);
	public static final AccountStatus ACTIVE_VALIDATION_LEVEL_2 =
		new AccountStatus(ACTIVE_CODE, VALIDATION_LEVEL_2_CODE);

	public static final AccountStatus NON_ACTIVE_VALIDATION_LEVEL_0 =
		new AccountStatus(NON_ACTIVE_CODE, VALIDATION_LEVEL_0_CODE);
	public static final AccountStatus NON_ACTIVE_FRAUD =
		new AccountStatus(NON_ACTIVE_CODE, FRAUD_CODE);
	public static final AccountStatus NON_ACTIVE_FAILURE =
		new AccountStatus(NON_ACTIVE_CODE, FAILURE_CODE);

	public static final AccountStatus DELETED_DELETED =
		new AccountStatus(DELETED_CODE, DELETED_SUB_CODE);

	private static final Map statusMap = new HashMap(6);

	private final String statusCode;
	private final String subStatusCode;
	private final String combinedCode;

	static {
		statusMap.put(ACTIVE_VALIDATION_LEVEL_0.getCombinedCode(),ACTIVE_VALIDATION_LEVEL_0);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_1.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_1);
		statusMap.put(
			ACTIVE_VALIDATION_LEVEL_2.getCombinedCode(),
			ACTIVE_VALIDATION_LEVEL_2);
		statusMap.put(
			NON_ACTIVE_VALIDATION_LEVEL_0.getCombinedCode(),
			NON_ACTIVE_VALIDATION_LEVEL_0);
		statusMap.put(NON_ACTIVE_FRAUD.getCombinedCode(), NON_ACTIVE_FRAUD);
		statusMap.put(NON_ACTIVE_FAILURE.getCombinedCode(), NON_ACTIVE_FAILURE);
		statusMap.put(DELETED_DELETED.getCombinedCode(), DELETED_DELETED);
	}

	private AccountStatus(String statusCode, String subStatusCode)
	{
		this.statusCode = statusCode;
		this.subStatusCode = subStatusCode;
		this.combinedCode = statusCode + DELIMITER + subStatusCode;
	}

	public static AccountStatus getInstance(
		String statusCode,
		String subStatusCode)
	{
		String key = statusCode + DELIMITER + subStatusCode;
		return getInstanceByCombinedCode(key);
	}

	/**
	 * 
	 * @param combinedCode	- statusCode + DELIMITER + subStatusCode
	 * @return
	 * 
	 * Created on Feb 28, 2005
	 */
	public static AccountStatus getInstanceByCombinedCode(String combinedCode)
	{
		AccountStatus status = (AccountStatus) statusMap.get(combinedCode);

		if (status == null)
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] {
						combinedCode,
						"statusCode" + DELIMITER + "subStatusCode" });
			throw new IllegalArgumentException(errorMessage);
		}

		return (status);
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public String getCombinedCode()
	{
		return combinedCode;
	}

	public boolean isActiveStatus()
	{
		return ACTIVE_CODE.equalsIgnoreCase(this.statusCode);
	}

	public String toString()
	{
		return combinedCode;
	}
}
