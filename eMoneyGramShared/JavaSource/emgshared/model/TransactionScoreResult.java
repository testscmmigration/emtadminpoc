package emgshared.model;

public class TransactionScoreResult 
{
	private int score;
	private String resultCode;
	
			
	/**
	 * @return
	 */
	public String getResultCode()
	{
		return resultCode;
	}

	/**
	 * @return
	 */
	public int getScore()
	{
		return score;
	}

	/**
	 * @param string
	 */
	public void setResultCode(String string)
	{
		resultCode = string;
	}

	/**
	 * @param i
	 */
	public void setScore(int i)
	{
		score = i;
	}

}
