/*
 * Created on Jan 20, 2005
 *
 */
package emgshared.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
public class TransactionStatus implements Serializable
{
	//TODO tom c. - IMPLEMENT EQUALS AND HASH CODE ON THIS ENUM
	private static final String DELIMITER = ":";

	//	------------------------------------------
	//	doc status codes
	//	------------------------------------------

	public static final String DOC_STATUS_SUBMITTING_CODE = "SUB";
	public static final String DOC_STATUS_PENDING_CODE = "PEN";

	//	------------------------------------------
	//	status codes
	//	------------------------------------------

	public static final String AUTOMATIC_SCORING_PROCESS_CODE = "ASP";
	public static final String FORM_FREE_SEND_CODE = "FFS";
	public static final String PENDING_CODE = "PEN";
	public static final String APPROVED_CODE = "APP";
	public static final String DENIED_CODE = "DEN";
	public static final String SENT_CODE = "SEN";
	public static final String ERROR_CODE = "ERR";
	public static final String CANCELED_CODE = "CXL";
	public static final String MANUAL_CODE = "MAN";
	public static final String SAVE_CODE = "SAV";
	public static final String DEL_CODE = "DEL";
	public static final String CASH_CODE = "CSH";
	public static final String INCOMPLETE_CODE = "INC";

	//	------------------------------------------
	//	substatus codes, aka funding status
	//	------------------------------------------
	public static final String VBV_PENDING_AUTHENTICATION_CODE = "VBV";
	public static final String NOT_FUNDED_CODE = "NOF";
	public static final String CC_AUTH_CODE = "CCA";
	public static final String ACH_WAITING_CODE = "ACW";
	public static final String ACH_IN_PROCESS_CODE = "AIP";
	public static final String ACH_SENT_CODE = "ACS";
	public static final String CC_SALE_CODE = "CCS";
	public static final String ACH_IN_PROCESS_ERROR_CODE = "AIE";
	public static final String ACH_ERROR_CODE = "ACE";
	public static final String ACH_RETURNED_CODE = "ACR";
	public static final String ACH_CANCEL_CODE = "ACC";
	public static final String ACH_REFUND_REQUESTED_CODE = "ARR";
	public static final String FEE_CODE = "FEE";
	public static final String CC_REVERSE_AUTH_CODE = "CRA";
	public static final String CC_CANCEL_CODE = "CCC";
	public static final String CC_FAILED_CODE = "CCF";
	public static final String CC_CHARGEBACK_CODE = "CCR";
	public static final String CC_REFUND_REQUESTED_CODE = "CRR";
	public static final String MANUAL_SUB_CODE = "MAN";
	public static final String MANUAL_LOSS_ADJUSTMENT_CODE = "MLA";
	public static final String MANUAL_FEE_REVENUE_CODE = "MFA";
	public static final String MANUAL_CONSUMER_REFUND_OR_REMITTANCE_CODE =
		"MCA";
	public static final String MANUAL_MG_ACTIVITY_CODE = "MAA";
	public static final String WRITE_OFF_LOSS_CODE = "WOL";
	public static final String RECOVERY_OF_LOSS_CODE = "ROL"; // Consumer
	public static final String RECOVERY_OF_LOSS_CYBERSOURCE_CODE = "ROC";
	public static final String MANUAL_CORRECTING_ENTRY_CODE = "MCE";
	
	//Bank-Telecheck specific status (DelayedSends)
	public static final String BANK_REJECTED_CODE = "BKR";
	public static final String BANK_SETTLED_CODE = "BKS";
	public static final String BANK_WRITE_OFFLOSS_CODE = "BWL";
	public static final String BANK_REFUND_REQUEST_CODE = "BRR";
	public static final String BANK_CANCEL_CODE = "BKC";
	public static final String BANK_VOID_CODE = "BKV";
	public static final String BANK_RECOVER_OF_LOSS_CODE = "BRL";

	//	------------------------------------------
	//	Status objects
	//	------------------------------------------
	public static final String CC_VOID_CODE="CCV";

	public static final TransactionStatus CASH_TRANSACTION_NOT_FUNDED =
		new TransactionStatus(
			CASH_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus AUTOMATIC_SCORING_PROCESS_NOT_FUNDED =
		new TransactionStatus(
			AUTOMATIC_SCORING_PROCESS_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus FORM_FREE_SEND_NOT_FUNDED =
		new TransactionStatus(
			FORM_FREE_SEND_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus VBV_PENDING_AUTHENTICATION =
		new TransactionStatus(
			FORM_FREE_SEND_CODE,
			VBV_PENDING_AUTHENTICATION_CODE,
			false,
			false,
			false);

	public static final TransactionStatus PENDING_NOT_FUNDED =
		new TransactionStatus(
			PENDING_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_NOT_FUNDED =
		new TransactionStatus(
			CANCELED_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus APPROVED_NOT_FUNDED =
		new TransactionStatus(
			APPROVED_CODE,
			NOT_FUNDED_CODE,
			true,
			true,
			false);

	public static final TransactionStatus APPROVED_CC_AUTH =
		new TransactionStatus(APPROVED_CODE, CC_AUTH_CODE, true, false, false);

	public static final TransactionStatus APPROVED_CC_SALE =
		new TransactionStatus(APPROVED_CODE, CC_SALE_CODE, true, false, false);

	public static final TransactionStatus APPROVED_CC_FAILED =
		new TransactionStatus(
			APPROVED_CODE,
			CC_FAILED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus APPROVED_ACH_WAITING =
		new TransactionStatus(
			APPROVED_CODE,
			ACH_WAITING_CODE,
			false,
			false,
			false);

	public static final TransactionStatus APPROVED_ACH_IN_PROCESS =
		new TransactionStatus(
			APPROVED_CODE,
			ACH_IN_PROCESS_CODE,
			false,
			false,
			false);

	public static final TransactionStatus APPROVED_ACH_SENT =
		new TransactionStatus(APPROVED_CODE, ACH_SENT_CODE, true, false, false);

	public static final TransactionStatus APPROVED_ACH_RETURNED =
		new TransactionStatus(
			APPROVED_CODE,
			ACH_RETURNED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus DENIED_NOT_FUNDED =
		new TransactionStatus(
			DENIED_CODE,
			NOT_FUNDED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_NOT_FUNDED =
		new TransactionStatus(SENT_CODE, NOT_FUNDED_CODE, false, true, false);

	public static final TransactionStatus SENT_CC_AUTH =
		new TransactionStatus(SENT_CODE, CC_AUTH_CODE, false, true, false);

	public static final TransactionStatus SENT_ACH_WAITING =
		new TransactionStatus(SENT_CODE, ACH_WAITING_CODE, false, false, true);

	public static final TransactionStatus SENT_ACH_IN_PROCESS =
		new TransactionStatus(
			SENT_CODE,
			ACH_IN_PROCESS_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_ACH_SENT =
		new TransactionStatus(SENT_CODE, ACH_SENT_CODE, false, false, true);

	public static final TransactionStatus SENT_CC_SALE =
		new TransactionStatus(SENT_CODE, CC_SALE_CODE, false, false, true);

	public static final TransactionStatus SENT_ACH_IN_PROCESS_ERROR =
		new TransactionStatus(
			SENT_CODE,
			ACH_IN_PROCESS_ERROR_CODE,
			true,
			true,
			false);

	public static final TransactionStatus SENT_ACH_ERROR =
		new TransactionStatus(SENT_CODE, ACH_ERROR_CODE, true, true, false);

	public static final TransactionStatus SENT_ACH_RETURNED =
		new TransactionStatus(SENT_CODE, ACH_RETURNED_CODE, true, true, false);

	public static final TransactionStatus SENT_ACH_CANCELED =
		new TransactionStatus(SENT_CODE, ACH_CANCEL_CODE, false, false, false);

	public static final TransactionStatus SENT_FEE =
		new TransactionStatus(SENT_CODE, FEE_CODE, true, true, false);

	public static final TransactionStatus SENT_CC_AUTH_REVERSED =
		new TransactionStatus(
			SENT_CODE,
			CC_REVERSE_AUTH_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_CC_CANCELED =
		new TransactionStatus(SENT_CODE, CC_CANCEL_CODE, false, false, false);

	public static final TransactionStatus SENT_CC_FAILED =
		new TransactionStatus(SENT_CODE, CC_FAILED_CODE, false, false, false);

	public static final TransactionStatus SENT_CC_CHARGEBACK =
		new TransactionStatus(
			SENT_CODE,
			CC_CHARGEBACK_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_WRITE_OFF_LOSS =
		new TransactionStatus(
			SENT_CODE,
			WRITE_OFF_LOSS_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_RECOVERY_OF_LOSS =
		new TransactionStatus(
			SENT_CODE,
			RECOVERY_OF_LOSS_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_RECOVERY_OF_LOSS_CYBERSOURCE =
		new TransactionStatus(
			SENT_CODE,
			RECOVERY_OF_LOSS_CYBERSOURCE_CODE,
			false,
			false,
			false);

	public static final TransactionStatus SENT_MANUAL_CORRECTING_ENTRY =
		new TransactionStatus(
			SENT_CODE,
			MANUAL_CORRECTING_ENTRY_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_NOT_FUNDED =
		new TransactionStatus(ERROR_CODE, NOT_FUNDED_CODE, false, false, false);

	public static final TransactionStatus ERROR_CC_AUTH =
		new TransactionStatus(ERROR_CODE, CC_AUTH_CODE, false, false, false);

	public static final TransactionStatus ERROR_ACH_WAITING =
		new TransactionStatus(
			ERROR_CODE,
			ACH_WAITING_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_ACH_SENT =
		new TransactionStatus(ERROR_CODE, ACH_SENT_CODE, false, false, false);

	public static final TransactionStatus ERROR_CC_SALE =
		new TransactionStatus(ERROR_CODE, CC_SALE_CODE, false, false, false);

	public static final TransactionStatus ERROR_ACH_IN_PROCESS_ERROR =
		new TransactionStatus(
			ERROR_CODE,
			ACH_IN_PROCESS_ERROR_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_ACH_ERROR =
		new TransactionStatus(ERROR_CODE, ACH_ERROR_CODE, false, false, false);

	public static final TransactionStatus ERROR_ACH_RETURNED =
		new TransactionStatus(
			ERROR_CODE,
			ACH_RETURNED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_ACH_CANCELED =
		new TransactionStatus(ERROR_CODE, ACH_CANCEL_CODE, false, false, false);

	public static final TransactionStatus ERROR_CC_AUTH_REVERSED =
		new TransactionStatus(
			ERROR_CODE,
			CC_REVERSE_AUTH_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_CC_CANCELED =
		new TransactionStatus(ERROR_CODE, CC_CANCEL_CODE, false, false, false);

	public static final TransactionStatus ERROR_CC_FAILED =
		new TransactionStatus(ERROR_CODE, CC_FAILED_CODE, false, false, false);

	public static final TransactionStatus ERROR_CC_CHARGEBACK =
		new TransactionStatus(
			ERROR_CODE,
			CC_CHARGEBACK_CODE,
			false,
			false,
			false);

	public static final TransactionStatus ERROR_MANUAL_CORRECTING_ENTRY =
		new TransactionStatus(
			ERROR_CODE,
			MANUAL_CORRECTING_ENTRY_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_AUTH =
		new TransactionStatus(CANCELED_CODE, CC_AUTH_CODE, false, false, false);

	public static final TransactionStatus CANCELED_ACH_WAITING =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_WAITING_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_ACH_SENT =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_SENT_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_SALE =
		new TransactionStatus(CANCELED_CODE, CC_SALE_CODE, false, false, false);

	public static final TransactionStatus CANCELED_CC_REFUND_REQUESTED =
		new TransactionStatus(
			CANCELED_CODE,
			CC_REFUND_REQUESTED_CODE,
			false,
			false,
			false);
	
	public static final TransactionStatus CANCELED_CC_VOID = 
		new TransactionStatus(
				CANCELED_CODE,
				CC_VOID_CODE,
				false,
				false,
				false);

	public static final TransactionStatus CANCELED_ACH_IN_PROCESS_ERROR =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_IN_PROCESS_ERROR_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_ACH_ERROR =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_ERROR_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_ACH_RETURNED =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_RETURNED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_ACH_CANCELED =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_CANCEL_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_ACH_REFUND_REQUESTED =
		new TransactionStatus(
			CANCELED_CODE,
			ACH_REFUND_REQUESTED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_AUTH_REVERSED =
		new TransactionStatus(
			CANCELED_CODE,
			CC_REVERSE_AUTH_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_CANCELED =
		new TransactionStatus(
			CANCELED_CODE,
			CC_CANCEL_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_FAILED =
		new TransactionStatus(
			CANCELED_CODE,
			CC_FAILED_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_CC_CHARGEBACK =
		new TransactionStatus(
			CANCELED_CODE,
			CC_CHARGEBACK_CODE,
			false,
			false,
			false);

	public static final TransactionStatus CANCELED_MANUAL_CORRECTING_ENTRY =
		new TransactionStatus(
			CANCELED_CODE,
			MANUAL_CORRECTING_ENTRY_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_MANUAL =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_SUB_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_RECORD_LOSS_REVENUE =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_LOSS_ADJUSTMENT_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_RECORD_FEE_REVENUE =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_FEE_REVENUE_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_CONSUMER_REFUND_OR_REMITTANCE_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_RECORD_MG_ACTIVITY =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_MG_ACTIVITY_CODE,
			false,
			false,
			false);

	public static final TransactionStatus MANUAL_MANUAL_CORRECTING_ENTRY =
		new TransactionStatus(
			MANUAL_CODE,
			MANUAL_CORRECTING_ENTRY_CODE,
			false,
			false,
			false);
	
	public static final TransactionStatus SAVE_TRAN =
		new TransactionStatus(SAVE_CODE,NOT_FUNDED_CODE , false, false, false);

	public static final TransactionStatus DEL_TRAN =
		new TransactionStatus(DEL_CODE,NOT_FUNDED_CODE , false, false, false);
	public static final TransactionStatus APP_ACH_IPERROR =
		new TransactionStatus(APPROVED_CODE,ACH_IN_PROCESS_ERROR_CODE , false, false, false);
	
	//Bank-Telecheck Status info:
	public static final TransactionStatus APPROVED_BANK_SETTLED = new TransactionStatus(APPROVED_CODE, BANK_SETTLED_CODE, true,false,false);
	public static final TransactionStatus ERROR_BANK_SETTLED = new TransactionStatus(ERROR_CODE, BANK_SETTLED_CODE, true,false,false);
	public static final TransactionStatus SENT_BANK_SETTLED = new TransactionStatus(SENT_CODE, BANK_SETTLED_CODE, false,false,true);
	public static final TransactionStatus SENT_BANK_REJECTED = new TransactionStatus(SENT_CODE, BANK_REJECTED_CODE , false, false, true);
	public static final TransactionStatus SENT_BANK_WRITE_OFF_LOSS = new TransactionStatus(SENT_CODE, BANK_WRITE_OFFLOSS_CODE , false, false, false);
	public static final TransactionStatus CANCELLED_BANK_CANCEL = new TransactionStatus(CANCELED_CODE, BANK_CANCEL_CODE, false, false, false);
	public static final TransactionStatus CANCELLED_BANK_REJECTED = new TransactionStatus(CANCELED_CODE, BANK_REJECTED_CODE, false, false, false);
	public static final TransactionStatus CANCELLED_BANK_SETTLED = new TransactionStatus(CANCELED_CODE, BANK_SETTLED_CODE, false, false, true);
	public static final TransactionStatus CANCELLED_BANK_VOID = new TransactionStatus(CANCELED_CODE, BANK_VOID_CODE, false, false, false);
	public static final TransactionStatus CANCELLED_BANK_REFUND_REQUEST = new TransactionStatus(CANCELED_CODE, BANK_REFUND_REQUEST_CODE, false, false, false);
	public static final TransactionStatus BANK_RECOVER_OF_LOSS = new TransactionStatus(SENT_CODE, BANK_RECOVER_OF_LOSS_CODE, false, false, false);
	public static final TransactionStatus BANK_REFUND_REQUEST = new TransactionStatus(SENT_CODE, BANK_REFUND_REQUEST_CODE, false, false, false);

	//Maestro Incomplete Transaction
	public static final TransactionStatus INCOMPLETE_TRANSACTION = new TransactionStatus(INCOMPLETE_CODE, NOT_FUNDED_CODE, false, false, false);
	
	//	------------------------------------------
	//	other attributes
	//	------------------------------------------

	private static final Map statusMap = new HashMap(100);

	private final String statusCode;
	private final String subStatusCode;
	private final boolean sendable;
	private final boolean fundable;
	private final boolean refundable;

	//	------------------------------------------
	//	Build map of status objects
	//	------------------------------------------

	static {
		statusMap.put(
				CASH_TRANSACTION_NOT_FUNDED.getStatusCode()
					+ DELIMITER
					+ CASH_TRANSACTION_NOT_FUNDED.getSubStatusCode(),
					CASH_TRANSACTION_NOT_FUNDED);

		statusMap.put(
			AUTOMATIC_SCORING_PROCESS_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ AUTOMATIC_SCORING_PROCESS_NOT_FUNDED.getSubStatusCode(),
			AUTOMATIC_SCORING_PROCESS_NOT_FUNDED);

		statusMap.put(
			VBV_PENDING_AUTHENTICATION.getStatusCode()
				+ DELIMITER
				+ VBV_PENDING_AUTHENTICATION.getSubStatusCode(),
			VBV_PENDING_AUTHENTICATION);

		statusMap.put(
			FORM_FREE_SEND_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ FORM_FREE_SEND_NOT_FUNDED.getSubStatusCode(),
			FORM_FREE_SEND_NOT_FUNDED);

		statusMap.put(
			PENDING_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ PENDING_NOT_FUNDED.getSubStatusCode(),
			PENDING_NOT_FUNDED);

		statusMap.put(
			CANCELED_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ CANCELED_NOT_FUNDED.getSubStatusCode(),
			CANCELED_NOT_FUNDED);

		statusMap.put(
			APPROVED_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ APPROVED_NOT_FUNDED.getSubStatusCode(),
			APPROVED_NOT_FUNDED);

		statusMap.put(
			APPROVED_CC_AUTH.getStatusCode()
				+ DELIMITER
				+ APPROVED_CC_AUTH.getSubStatusCode(),
			APPROVED_CC_AUTH);

		statusMap.put(
			APPROVED_CC_SALE.getStatusCode()
				+ DELIMITER
				+ APPROVED_CC_SALE.getSubStatusCode(),
			APPROVED_CC_SALE);

		statusMap.put(
			APPROVED_CC_FAILED.getStatusCode()
				+ DELIMITER
				+ APPROVED_CC_FAILED.getSubStatusCode(),
			APPROVED_CC_FAILED);

		statusMap.put(
			APPROVED_ACH_WAITING.getStatusCode()
				+ DELIMITER
				+ APPROVED_ACH_WAITING.getSubStatusCode(),
			APPROVED_ACH_WAITING);

		statusMap.put(
			APPROVED_ACH_IN_PROCESS.getStatusCode()
				+ DELIMITER
				+ APPROVED_ACH_IN_PROCESS.getSubStatusCode(),
			APPROVED_ACH_IN_PROCESS);

		statusMap.put(
			APPROVED_ACH_SENT.getStatusCode()
				+ DELIMITER
				+ APPROVED_ACH_SENT.getSubStatusCode(),
			APPROVED_ACH_SENT);

		statusMap.put(
			APPROVED_ACH_RETURNED.getStatusCode()
				+ DELIMITER
				+ APPROVED_ACH_RETURNED.getSubStatusCode(),
			APPROVED_ACH_RETURNED);

		statusMap.put(
			DENIED_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ DENIED_NOT_FUNDED.getSubStatusCode(),
			DENIED_NOT_FUNDED);

		statusMap.put(
			SENT_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ SENT_NOT_FUNDED.getSubStatusCode(),
			SENT_NOT_FUNDED);

		statusMap.put(
			SENT_CC_AUTH.getStatusCode()
				+ DELIMITER
				+ SENT_CC_AUTH.getSubStatusCode(),
			SENT_CC_AUTH);

		statusMap.put(
			SENT_ACH_WAITING.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_WAITING.getSubStatusCode(),
			SENT_ACH_WAITING);

		statusMap.put(
			SENT_ACH_IN_PROCESS.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_IN_PROCESS.getSubStatusCode(),
			SENT_ACH_IN_PROCESS);

		statusMap.put(
			SENT_ACH_SENT.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_SENT.getSubStatusCode(),
			SENT_ACH_SENT);

		statusMap.put(
			SENT_CC_SALE.getStatusCode()
				+ DELIMITER
				+ SENT_CC_SALE.getSubStatusCode(),
			SENT_CC_SALE);

		statusMap.put(
			SENT_ACH_IN_PROCESS_ERROR.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_IN_PROCESS_ERROR.getSubStatusCode(),
			SENT_ACH_IN_PROCESS_ERROR);

		statusMap.put(
			SENT_ACH_ERROR.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_ERROR.getSubStatusCode(),
			SENT_ACH_ERROR);

		statusMap.put(
			SENT_ACH_RETURNED.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_RETURNED.getSubStatusCode(),
			SENT_ACH_RETURNED);

		statusMap.put(
			SENT_ACH_CANCELED.getStatusCode()
				+ DELIMITER
				+ SENT_ACH_CANCELED.getSubStatusCode(),
			SENT_ACH_CANCELED);

		statusMap.put(
			SENT_FEE.getStatusCode() + DELIMITER + SENT_FEE.getSubStatusCode(),
			SENT_FEE);

		statusMap.put(
			SENT_CC_AUTH_REVERSED.getStatusCode()
				+ DELIMITER
				+ SENT_CC_AUTH_REVERSED.getSubStatusCode(),
			SENT_CC_AUTH_REVERSED);

		statusMap.put(
			SENT_CC_CANCELED.getStatusCode()
				+ DELIMITER
				+ SENT_CC_CANCELED.getSubStatusCode(),
			SENT_CC_CANCELED);

		statusMap.put(
			SENT_CC_FAILED.getStatusCode()
				+ DELIMITER
				+ SENT_CC_FAILED.getSubStatusCode(),
			SENT_CC_FAILED);

		statusMap.put(
			SENT_CC_CHARGEBACK.getStatusCode()
				+ DELIMITER
				+ SENT_CC_CHARGEBACK.getSubStatusCode(),
			SENT_CC_CHARGEBACK);

		statusMap.put(
			SENT_WRITE_OFF_LOSS.getStatusCode()
				+ DELIMITER
				+ SENT_WRITE_OFF_LOSS.getSubStatusCode(),
			SENT_WRITE_OFF_LOSS);

		statusMap.put(
			SENT_RECOVERY_OF_LOSS.getStatusCode()
				+ DELIMITER
				+ SENT_RECOVERY_OF_LOSS.getSubStatusCode(),
			SENT_RECOVERY_OF_LOSS);

		statusMap.put(
			SENT_RECOVERY_OF_LOSS_CYBERSOURCE.getStatusCode()
				+ DELIMITER
				+ SENT_RECOVERY_OF_LOSS_CYBERSOURCE.getSubStatusCode(),
			SENT_RECOVERY_OF_LOSS_CYBERSOURCE);

		statusMap.put(
			SENT_MANUAL_CORRECTING_ENTRY.getStatusCode()
				+ DELIMITER
				+ SENT_MANUAL_CORRECTING_ENTRY.getSubStatusCode(),
			SENT_MANUAL_CORRECTING_ENTRY);

		statusMap.put(
			ERROR_NOT_FUNDED.getStatusCode()
				+ DELIMITER
				+ ERROR_NOT_FUNDED.getSubStatusCode(),
			ERROR_NOT_FUNDED);

		statusMap.put(
			ERROR_CC_AUTH.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_AUTH.getSubStatusCode(),
			ERROR_CC_AUTH);

		statusMap.put(
			ERROR_ACH_WAITING.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_WAITING.getSubStatusCode(),
			ERROR_ACH_WAITING);

		statusMap.put(
			ERROR_ACH_SENT.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_SENT.getSubStatusCode(),
			ERROR_ACH_SENT);

		statusMap.put(
			ERROR_CC_SALE.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_SALE.getSubStatusCode(),
			ERROR_CC_SALE);

		statusMap.put(
			ERROR_ACH_IN_PROCESS_ERROR.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_IN_PROCESS_ERROR.getSubStatusCode(),
			ERROR_ACH_IN_PROCESS_ERROR);

		statusMap.put(
			ERROR_ACH_ERROR.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_ERROR.getSubStatusCode(),
			ERROR_ACH_ERROR);

		statusMap.put(
			ERROR_ACH_RETURNED.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_RETURNED.getSubStatusCode(),
			ERROR_ACH_RETURNED);

		statusMap.put(
			ERROR_ACH_CANCELED.getStatusCode()
				+ DELIMITER
				+ ERROR_ACH_CANCELED.getSubStatusCode(),
			ERROR_ACH_CANCELED);

		statusMap.put(
			ERROR_CC_AUTH_REVERSED.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_AUTH_REVERSED.getSubStatusCode(),
			ERROR_CC_AUTH_REVERSED);

		statusMap.put(
			ERROR_CC_CANCELED.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_CANCELED.getSubStatusCode(),
			ERROR_CC_CANCELED);

		statusMap.put(
			ERROR_CC_FAILED.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_FAILED.getSubStatusCode(),
			ERROR_CC_FAILED);

		statusMap.put(
			ERROR_CC_CHARGEBACK.getStatusCode()
				+ DELIMITER
				+ ERROR_CC_CHARGEBACK.getSubStatusCode(),
			ERROR_CC_CHARGEBACK);

		statusMap.put(
			ERROR_MANUAL_CORRECTING_ENTRY.getStatusCode()
				+ DELIMITER
				+ ERROR_MANUAL_CORRECTING_ENTRY.getSubStatusCode(),
			ERROR_MANUAL_CORRECTING_ENTRY);

		statusMap.put(
			CANCELED_CC_AUTH.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_AUTH.getSubStatusCode(),
			CANCELED_CC_AUTH);

		statusMap.put(
			CANCELED_ACH_WAITING.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_WAITING.getSubStatusCode(),
			CANCELED_ACH_WAITING);

		statusMap.put(
			CANCELED_ACH_SENT.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_SENT.getSubStatusCode(),
			CANCELED_ACH_SENT);

		statusMap.put(
			CANCELED_CC_SALE.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_SALE.getSubStatusCode(),
			CANCELED_CC_SALE);

		statusMap.put(
			CANCELED_CC_REFUND_REQUESTED.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_REFUND_REQUESTED.getSubStatusCode(),
			CANCELED_CC_REFUND_REQUESTED);

		statusMap.put(
			CANCELED_ACH_IN_PROCESS_ERROR.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_IN_PROCESS_ERROR.getSubStatusCode(),
			CANCELED_ACH_IN_PROCESS_ERROR);

		statusMap.put(
			CANCELED_ACH_ERROR.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_ERROR.getSubStatusCode(),
			CANCELED_ACH_ERROR);

		statusMap.put(
			CANCELED_ACH_RETURNED.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_RETURNED.getSubStatusCode(),
			CANCELED_ACH_RETURNED);

		statusMap.put(
			CANCELED_ACH_CANCELED.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_CANCELED.getSubStatusCode(),
			CANCELED_ACH_CANCELED);

		statusMap.put(
			CANCELED_ACH_REFUND_REQUESTED.getStatusCode()
				+ DELIMITER
				+ CANCELED_ACH_REFUND_REQUESTED.getSubStatusCode(),
			CANCELED_ACH_REFUND_REQUESTED);

		statusMap.put(
			CANCELED_CC_AUTH_REVERSED.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_AUTH_REVERSED.getSubStatusCode(),
			CANCELED_CC_AUTH_REVERSED);

		statusMap.put(
			CANCELED_CC_CANCELED.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_CANCELED.getSubStatusCode(),
			CANCELED_CC_CANCELED);

		statusMap.put(
			CANCELED_CC_FAILED.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_FAILED.getSubStatusCode(),
			CANCELED_CC_FAILED);

		statusMap.put(
			CANCELED_CC_CHARGEBACK.getStatusCode()
				+ DELIMITER
				+ CANCELED_CC_CHARGEBACK.getSubStatusCode(),
			CANCELED_CC_CHARGEBACK);

		statusMap.put(
			CANCELED_MANUAL_CORRECTING_ENTRY.getStatusCode()
				+ DELIMITER
				+ CANCELED_MANUAL_CORRECTING_ENTRY.getSubStatusCode(),
			CANCELED_MANUAL_CORRECTING_ENTRY);

		statusMap.put(
			MANUAL_MANUAL.getStatusCode()
				+ DELIMITER
				+ MANUAL_MANUAL.getSubStatusCode(),
			MANUAL_MANUAL);

		statusMap.put(
			MANUAL_RECORD_LOSS_REVENUE.getStatusCode()
				+ DELIMITER
				+ MANUAL_RECORD_LOSS_REVENUE.getSubStatusCode(),
			MANUAL_RECORD_LOSS_REVENUE);

		statusMap.put(
			MANUAL_RECORD_FEE_REVENUE.getStatusCode()
				+ DELIMITER
				+ MANUAL_RECORD_FEE_REVENUE.getSubStatusCode(),
			MANUAL_RECORD_FEE_REVENUE);

		statusMap.put(
			MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE.getStatusCode()
				+ DELIMITER
				+ MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE.getSubStatusCode(),
			MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE);

		statusMap.put(
			MANUAL_RECORD_MG_ACTIVITY.getStatusCode()
				+ DELIMITER
				+ MANUAL_RECORD_MG_ACTIVITY.getSubStatusCode(),
			MANUAL_RECORD_MG_ACTIVITY);

		statusMap.put(
			MANUAL_MANUAL_CORRECTING_ENTRY.getStatusCode()
				+ DELIMITER
				+ MANUAL_MANUAL_CORRECTING_ENTRY.getSubStatusCode(),
			MANUAL_MANUAL_CORRECTING_ENTRY);
		
		statusMap.put(
				SAVE_TRAN.getStatusCode()
					+ DELIMITER
					+ SAVE_TRAN.getSubStatusCode(),
					SAVE_TRAN);

		statusMap.put(
				DEL_TRAN.getStatusCode()
					+ DELIMITER
					+ DEL_TRAN.getSubStatusCode(),
					DEL_TRAN);

		statusMap.put(APP_ACH_IPERROR.getStatusCode() + DELIMITER
					+ APP_ACH_IPERROR.getSubStatusCode(),APP_ACH_IPERROR);

		//Bank-Telecheck Status info:		
		statusMap.put(APPROVED_BANK_SETTLED.getStatusCode() + DELIMITER + APPROVED_BANK_SETTLED.getSubStatusCode(), APPROVED_BANK_SETTLED);
		statusMap.put(ERROR_BANK_SETTLED.getStatusCode() + DELIMITER + ERROR_BANK_SETTLED.getSubStatusCode(), ERROR_BANK_SETTLED);
		statusMap.put(SENT_BANK_SETTLED.getStatusCode() + DELIMITER + SENT_BANK_SETTLED.getSubStatusCode(), SENT_BANK_SETTLED);
		statusMap.put(SENT_BANK_REJECTED.getStatusCode() + DELIMITER + SENT_BANK_REJECTED.getSubStatusCode(), SENT_BANK_REJECTED);
		statusMap.put(SENT_BANK_WRITE_OFF_LOSS.getStatusCode() + DELIMITER + SENT_BANK_WRITE_OFF_LOSS.getSubStatusCode(), SENT_BANK_WRITE_OFF_LOSS);
		statusMap.put(CANCELLED_BANK_CANCEL.getStatusCode() + DELIMITER + CANCELLED_BANK_CANCEL.getSubStatusCode(), CANCELLED_BANK_CANCEL);
		statusMap.put(CANCELLED_BANK_REJECTED.getStatusCode() + DELIMITER + CANCELLED_BANK_REJECTED.getSubStatusCode(), CANCELLED_BANK_REJECTED);
		statusMap.put(CANCELLED_BANK_SETTLED.getStatusCode() + DELIMITER + CANCELLED_BANK_SETTLED.getSubStatusCode(), CANCELLED_BANK_SETTLED);
		statusMap.put(CANCELLED_BANK_VOID.getStatusCode() + DELIMITER + CANCELLED_BANK_VOID.getSubStatusCode(), CANCELLED_BANK_VOID);
		statusMap.put(CANCELLED_BANK_REFUND_REQUEST.getStatusCode() + DELIMITER + CANCELLED_BANK_REFUND_REQUEST.getSubStatusCode(), CANCELLED_BANK_REFUND_REQUEST);
		statusMap.put(BANK_RECOVER_OF_LOSS.getStatusCode() + DELIMITER + BANK_RECOVER_OF_LOSS.getSubStatusCode(), BANK_RECOVER_OF_LOSS);
		statusMap.put(BANK_REFUND_REQUEST.getStatusCode() + DELIMITER + BANK_REFUND_REQUEST.getSubStatusCode(), BANK_REFUND_REQUEST);
		statusMap.put(CANCELED_CC_VOID.getStatusCode() + DELIMITER + CANCELED_CC_VOID.getSubStatusCode(),CANCELED_CC_VOID);
		//Maestro Incomplete Transaction
		statusMap.put(INCOMPLETE_TRANSACTION.getStatusCode() + DELIMITER + INCOMPLETE_TRANSACTION.getSubStatusCode(), INCOMPLETE_TRANSACTION);
	}

	public TransactionStatus()
	{
		this.fundable = false;
		this.sendable = false;
		this.refundable = false;
		this.statusCode = "";
		this.subStatusCode = "";
	}

	private TransactionStatus(
		String statusCode,
		String subStatusCode,
		boolean sendable,
		boolean fundable,
		boolean refundable) //refundable = refundable to Mainframe/Agentconnect
	{
		this.statusCode = StringUtils.upperCase(statusCode);
		this.subStatusCode = StringUtils.upperCase(subStatusCode);
		this.sendable = sendable;
		this.fundable = fundable;
		this.refundable = refundable;
	}

	public static TransactionStatus getInstance(
		String statusCode,
		String subStatusCode)
	{
		String key =
			StringUtils.upperCase(statusCode)
				+ DELIMITER
				+ StringUtils.upperCase(subStatusCode);
		TransactionStatus status = (TransactionStatus) statusMap.get(key);

		if (status == null)
		{
			String errorMessage =
				I18NHelper.getFormattedMessage(
					"exception.invalid.param.value",
					new Object[] {
						key,
						"statusCode" + DELIMITER + "subStatusCode" });
			throw new IllegalArgumentException(errorMessage);
		}

		return (status);
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public String toString()
	{
		return statusCode + DELIMITER + subStatusCode;
	}

	public boolean isRefundable()
	{
		return refundable;
	}

	public boolean isFundable()
	{
		return fundable;
	}

	public boolean isSendable()
	{
		return sendable;
	}

	public boolean isManualAdjustable()
	{
		return MANUAL_CODE.equalsIgnoreCase(this.statusCode);
	}
}
