package emgshared.model;
import java.util.Date;

import emgshared.util.DateFormatter;

public class AdminMessage implements Comparable
{
	public static String orderBy = "adminMsgId";
	public static String orderSeq = "A";
	private int sortSeq = 1;

	private DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

	private Integer adminMsgId;
	private String msgText;
	private Date msgBegDate;
	private Date msgThruDate;
	private String adminMsgTypeCode;
	private String adminMsgTypeDesc;
	private String msgPrtyCode;
	private Date createDate;
	private String createUserid;
	private Date lastUpdateDate;
	private String lastUpdateUserid;
	private String readFlag;

	public Integer getAdminMsgId()
	{
		return adminMsgId;
	}

	public int getAdminMsgIdNbr()
	{
		return adminMsgId.intValue();
	}

	public void setAdminMsgId(Integer i)
	{
		adminMsgId = i;
	}

	public String getMsgText()
	{
		return msgText;
	}

	public void setMsgText(String string)
	{
		msgText = string;
	}

	public Date getMsgBegDate()
	{
		return msgBegDate;
	}

	public String getMsgBegDateText()
	{
		return df.format(msgBegDate);
	}

	public void setMsgBegDate(Date date)
	{
		msgBegDate = date;
	}

	public Date getMsgThruDate()
	{
		return msgThruDate;
	}

	public String getMsgThruDateText()
	{
		return df.format(msgThruDate);
	}

	public void setMsgThruDate(Date date)
	{
		msgThruDate = date;
	}

	public String getAdminMsgTypeCode()
	{
		return adminMsgTypeCode;
	}

	public void setAdminMsgTypeCode(String string)
	{
		adminMsgTypeCode = string;
	}

	public String getAdminMsgTypeDesc()
	{
		return adminMsgTypeDesc;
	}

	public void setAdminMsgTypeDesc(String string)
	{
		adminMsgTypeDesc = string;
	}

	public String getMsgPrtyCode()
	{
		return msgPrtyCode;
	}

	public void setMsgPrtyCode(String string)
	{
		msgPrtyCode = string;
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public String getCreateDateText()
	{
		return df.format(createDate);
	}

	public void setCreateDate(Date date)
	{
		createDate = date;
	}

	public String getCreateUserid()
	{
		return createUserid;
	}

	public void setCreateUserid(String string)
	{
		createUserid = string;
	}

	public Date getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public String getLastUpdateDateText()
	{
		return df.format(lastUpdateDate);
	}

	public void setLastUpdateDate(Date date)
	{
		lastUpdateDate = date;
	}

	public String getLastUpdateUserid()
	{
		return lastUpdateUserid;
	}

	public void setLastUpdateUserid(String string)
	{
		lastUpdateUserid = string;
	}

	public String getReadFlag()
	{
		return readFlag;
	}

	public void setReadFlag(String string)
	{
		readFlag = string;
	}

	public int compareTo(Object o)
	{
		AdminMessage oth = (AdminMessage) o;
		sortSeq = "D".equalsIgnoreCase(orderSeq) ? -1 : 1;

		if ("msgText".equalsIgnoreCase(orderBy))
		{
			return (msgText.compareTo(oth.msgText)) * sortSeq;
		} else if ("msgBegDate".equalsIgnoreCase(orderBy))
		{
			return (msgBegDate.after(oth.msgBegDate) ? 1 : -1) * sortSeq;
		} else if ("msgThruDate".equalsIgnoreCase(orderBy))
		{
			return (msgThruDate.after(oth.msgThruDate) ? 1 : -1) * sortSeq;
		} else if ("adminMsgTypeCode".equalsIgnoreCase(orderBy))
		{
			return (adminMsgTypeCode.compareTo(oth.adminMsgTypeCode)) * sortSeq;
		} else if ("adminMsgTypeDesc".equalsIgnoreCase(orderBy))
		{
			return (adminMsgTypeDesc.compareTo(oth.adminMsgTypeDesc)) * sortSeq;
		} else if ("msgPrtyCode".equalsIgnoreCase(orderBy))
		{
			return (msgPrtyCode.compareTo(oth.msgPrtyCode)) * sortSeq;
		} else if ("createDate".equalsIgnoreCase(orderBy))
		{
			return (createDate.after(oth.createDate) ? 1 : -1) * sortSeq;
		} else if ("createUserid".equalsIgnoreCase(orderBy))
		{
			return (createUserid.compareTo(oth.createUserid)) * sortSeq;
		} else if ("lastUpdateDate".equalsIgnoreCase(orderBy))
		{
			return (lastUpdateDate.after(oth.lastUpdateDate) ? 1 : -1)
				* sortSeq;
		} else if ("lastUpdateUserid".equalsIgnoreCase(orderBy))
		{
			return (lastUpdateUserid.compareTo(oth.lastUpdateUserid)) * sortSeq;
		}

		return (getAdminMsgIdNbr() - oth.getAdminMsgIdNbr()) * sortSeq;
	}
}