package emgshared.model;

import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;

public class ConsumerProfileSearchView implements Comparable
{
	private String custId;
	private String custLogonId;
	private String custStatCode;
	private String custStatDesc;
	private String custSubStatCode;
	private String custSubStatDesc;
	private String custFrstName;
	private String custLastName;
	private String custPhoneNbr;
	private String custSSNMask;
	private String custBrthDate;
	private String custCreateDate;
	private String custCreateIpAddrId;
	private String taintedText;
	private String custPrmrCode;
	private String custCreatSrcWebSite;

	public static String sortFieldName = null;
	private DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);

	public String getCustId()
	{
		return custId == null ? "" : custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public String getCustLogonId()
	{
		return custLogonId == null ? "" : custLogonId;
	}

	public void setCustLogonId(String string)
	{
		custLogonId = string;
	}

	public String getCustStatCode()
	{
		return custStatCode == null ? "" : custStatCode;
	}

	public void setCustStatCode(String string)
	{
		custStatCode = string;
	}

	public String getCustStatDesc()
	{
		return custStatDesc == null ? "" : custStatDesc;
	}

	public void setCustStatDesc(String string)
	{
		custStatDesc = string;
	}

	public String getCustSubStatCode()
	{
		return custSubStatCode == null ? "" : custSubStatCode;
	}

	public void setCustSubStatCode(String string)
	{
		custSubStatCode = string;
	}

	public String getCustSubStatDesc()
	{
		return custSubStatDesc == null ? "" : custSubStatDesc;
	}

	public void setCustSubStatDesc(String string)
	{
		custSubStatDesc = string;
	}

	public String getCustFrstName()
	{
		return custFrstName == null ? "" : custFrstName;
	}

	public void setCustFrstName(String string)
	{
		custFrstName = string;
	}

	public String getCustLastName()
	{
		return custLastName == null ? "" : custLastName;
	}

	public void setCustLastName(String string)
	{
		custLastName = string;
	}

	public String getCustPhoneNbr()
	{
		return custPhoneNbr == null ? "" : custPhoneNbr;
	}

	public void setCustPhoneNbr(String string)
	{
		custPhoneNbr = string;
	}

	public String getCustSSNMask()
	{
		return custSSNMask == null ? "" : custSSNMask;
	}

	public void setCustSSNMask(String string)
	{
		custSSNMask = string;
	}

	public String getCustBrthDate()
	{
		return custBrthDate;
	}

	public void setCustBrthDate(String string)
	{
		custBrthDate = string;
	}

	public String getCustCreateDate()
	{
		return custCreateDate;
	}

	public void setCustCreateDate(String string)
	{
		custCreateDate = string;
	}

	public String getCustCreateIpAddrId()
	{
		return custCreateIpAddrId;
	}

	public void setCustCreateIpAddrId(String string)
	{
		custCreateIpAddrId = string;
	}

	public String getTaintedText()
	{
		return taintedText;
	}

	public void setTaintedText(String string)
	{
		taintedText = string;
	}


    public String getCustPrmrCode() {
        return custPrmrCode;
    }

    public void setCustPrmrCode(String custPrmrCode) {
        this.custPrmrCode = custPrmrCode;
    }

    public String getCustCreatSrcWebSite() {
		return custCreatSrcWebSite;
	}

	public void setCustCreatSrcWebSite(String custCreatSrcWebSite) {
		this.custCreatSrcWebSite = custCreatSrcWebSite;
	}

    public int compareTo(Object obj)
	{
		String field = sortFieldName;
		if (!"custLogonId".equalsIgnoreCase(field)
			&& !"custStatCode".equalsIgnoreCase(field)
			&& !"custSubStatCode".equalsIgnoreCase(field)
			&& !"custFrstName".equalsIgnoreCase(field)
			&& !"custLastName".equalsIgnoreCase(field)
			&& !"custPhoneNbr".equalsIgnoreCase(field)
			&& !"custSSNMask".equalsIgnoreCase(field)
			&& !"custBrthDate".equalsIgnoreCase(field)
			&& !"custCreateDate".equalsIgnoreCase(field)
			&& !"custCreateIpAddrId".equalsIgnoreCase(field)
			&& !"custPrmrCode".equalsIgnoreCase(field))
		{
			field = "custId";
		}

		ConsumerProfileSearchView cpsv = (ConsumerProfileSearchView) obj;

		if ("custId".equalsIgnoreCase(field))
		{
			return Integer.parseInt(custId) - Integer.parseInt(cpsv.custId);
		}

		if ("custLogonId".equalsIgnoreCase(field))
		{
			return custLogonId.compareToIgnoreCase(cpsv.custLogonId);
		}

		if ("custStatCode".equalsIgnoreCase(field))
		{
			return custStatCode.compareToIgnoreCase(cpsv.custStatCode);
		}

		if ("custSubStatCode".equalsIgnoreCase(field))
		{
			return custSubStatCode.compareToIgnoreCase(cpsv.custSubStatCode);
		}

		if ("custFrstName".equalsIgnoreCase(field))
		{
			return custFrstName.compareToIgnoreCase(cpsv.custFrstName);
		}

		if ("custLastName".equalsIgnoreCase(field))
		{
			return custLastName.compareToIgnoreCase(cpsv.custLastName);
		}

		if ("custPhoneNbr".equalsIgnoreCase(field))
		{
			return custPhoneNbr.compareToIgnoreCase(cpsv.custPhoneNbr);
		}

		if ("custSSNMask".equalsIgnoreCase(field))
		{
			return custSSNMask.compareToIgnoreCase(cpsv.custSSNMask);
		}

		if ("custBrthDate".equalsIgnoreCase(field))
		{
			try
			{
				return df.parse(custBrthDate).after(
					df.parse(cpsv.custBrthDate))
					? 1
					: -1;
			} catch (ParseDateException e)
			{
				throw new EMGRuntimeException(e);
			}
		}

		if ("custCreateDate".equalsIgnoreCase(field))
		{
			try
			{
				return df.parse(custCreateDate).after(
					df.parse(cpsv.custCreateDate))
					? 1
					: -1;
			} catch (ParseDateException e)
			{
				throw new EMGRuntimeException(e);
			}
		}

		if ("custCreateIpAddrId".equalsIgnoreCase(field))
		{
			return StringHelper.compareIP(
				this.custCreateIpAddrId,
				cpsv.custCreateIpAddrId);
		}
		
		if ("custPrmrCode".equalsIgnoreCase(field))
		{
			return custPrmrCode.compareToIgnoreCase(cpsv.custPrmrCode);
		}
		
		return 0;
	}

    // TODO change for real code once DB changes are done!!!
    public String getProgram() {
    	if (custId.equals("1550844")) {
    		return "UK";
    	}
    	return "US";
    }

    // TODO change for real code once DB changes are done!!!
    public boolean isRewardProgram() {
    	String market = getProgram();
    	return market.equals("US");
    }

}
