package emgshared.services.verifiedbyvisa;



/**
 * 
 * @author G.R.Svenddal
 * 
 * This should be abstract enough to implement any service's vbvCheckEnroll
 * service reply. But only being familiar with CyberSource at the moment, it's
 * probably going to be more specific to them then it should be. If and when
 * another service vendor is added, refactoring may ( probably will ) be needed. 
 *  
 *  
 */

public abstract class VBVReplyValidateAuthentication extends VBVServiceReply
{
     
   public VBVReplyValidateAuthentication()
     {
     }
  
   private String  authenticationResult          = null; 
   private String authenticationStatusMessage = null;
   
   private String  cavv              = null;

   private String  eci               = null;

   private String  eciRaw            = null;
   
   // These booleans are business decisions and must be mutually exclusive. 
   public abstract boolean bdSuccess();// User has enrolled or has passed check.

   public abstract boolean bdFailed();// User cancelled or failed check.

   public abstract boolean bdAttemptsProtected();// Card not 
   
   public abstract boolean bdNotProtected();
   
   public abstract boolean bdError(); // system failure ( not ours, theirs. )

   public String getAuthenticationResult()
   {
      return authenticationResult;
   }
   public void setAuthenticationResult(String authenticationResult)
   {
      this.authenticationResult = authenticationResult;
   }
   public String getAuthenticationStatusMessage()
   {
      return authenticationStatusMessage;
   }
   public void setAuthenticationStatusMessage(String authenticationStatusMessage)
   {
      this.authenticationStatusMessage = authenticationStatusMessage;
   }
   public String getCavv()
   {
      return cavv;
   }
   public void setCavv(String cavv)
   {
      this.cavv = cavv;
   }
   public String getEci()
   {
      return eci;
   }
   public void setEci(String eci)
   {
      this.eci = eci;
   }
   public String getEciRaw()
   {
      return eciRaw;
   }
   public void setEciRaw(String eciRaw)
   {
      this.eciRaw = eciRaw;
   }
  

}