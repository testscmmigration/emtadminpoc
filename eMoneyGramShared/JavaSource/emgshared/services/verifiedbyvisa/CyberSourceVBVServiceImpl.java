package emgshared.services.verifiedbyvisa;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.dataaccessors.CyberSourceVisaTicketDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.CreditCard;
import emgshared.model.ExchangeRateType;
import emgshared.property.EMTSharedContainerProperties;

/**
 * 
 * @author G.R.Svenddal
 * CyberSourceAuthentication.java
 *  
 * This class implements Verified By Visa specific API calls to CyberSource,
 * specifically, payerAuthEnroll and payerAuthValidate. Neither of these 
 * calls perform any money actions.
 * Not all Verified By Visa connected CyberSource stuff is here.
 * For instance, the ccAuthService modifications to support Verified By Visa
 * are in the emgshared.serivces.Cybe
 *  
 *
 *  
 *
 */

public class CyberSourceVBVServiceImpl implements VBVService
{

	private static final CyberSourceVBVServiceImpl instance =
		new CyberSourceVBVServiceImpl();

	// because the current code uses an earlier API, we must override that setting with the
	// API version that matches our Cybersource VerifiedByVisa docs ( ImplGuide.pdf ) 
	// TODO - this should be refactored for long term configurability.
	private static final String VBV_CYBERSOURCE_TARGET_API = "1.14";

	private final Properties cybersourceConfig = new Properties();

	private final Logger logr =
		LogFactory.getInstance().getLogger(VBVService.class);

	public static final CyberSourceVBVServiceImpl getInstance()
	{
		return instance;
	}

	/**
	 * CheckEnrollment : Step 1 of Verified by Visa authentication process. 
	 * Pass card info, and if the card is enrolled we should receive an url
	 * to the issuing bank's authentication page ( most likely a password 
	 * dialog. ) If not enrolled, the same url might be an enrollment form,
	 * or we may not get any url, in which case we direct them to the standard
	 * Visa enrollment page. ( as of current business flow, 6/15. GRS ) 
	 * 
	 * @param card 
	 * @param emgTrackingValue
	 * @return 
	 */
	public VBVReplyCheckEnrollment checkEnrollment(
		CreditCard card,
		String emgTrackingValue,
		String unitPrice //optional parameter.
	)
	{
		HashMap csRequest = new HashMap();

		csRequest.put("payerAuthEnrollService_run", "true");
		csRequest.put("merchantId",	EMTSharedContainerProperties.getMerchantIdEMG());
		csRequest.put("merchantReferenceCode",	(emgTrackingValue == null || emgTrackingValue.equals(""))
				? "emg-default-merchRefCode"
				: emgTrackingValue);

		csRequest.put("card_accountNumber", card.getAccountNumber());
		csRequest.put("card_expirationMonth", "" + card.getExpireMonth());
		csRequest.put("card_expirationYear", "" + card.getExpireYear());
		csRequest.put("card_cardType", "001"); // hard coded Visa value. 
		csRequest.put(
			"item_0_unitPrice",
			((unitPrice == null) ? "1.00" : unitPrice));
		csRequest.put(
			"purchaseTotals_currency",
			ExchangeRateType.USD.getTypeCode());
		logHashMap("CybSrc CheckEnroll Request", csRequest);
		VBVReplyCheckEnrollment responseBean = null;

		try
		{
			// Calling CyberSource ...
			HashMap m = Client.runTransaction(csRequest, cybersourceConfig);
			logHashMap("CybSrc CheckEnroll Response", m);
			responseBean = new CyberSourceVBVReplyCheckEnrollment();
			responseBean.setRequestID((String) m.get("requestID"));
			// identifier
			responseBean.setDecision((String) m.get("decision"));
			// a word like ACCEPT
			responseBean.setReasonCode((String) m.get("reasonCode"));
			// reason code, number
			responseBean.setMerchantReferenceCode(
				(String) m.get("merchantReferenceCode"));
			responseBean.setPaReq((String) m.get("payerAuthEnrollReply_paReq"));
			responseBean.setAcsURL(
				(String) m.get("payerAuthEnrollReply_acsURL"));
			responseBean.setXid((String) m.get("payerAuthEnrollReply_xid"));
			responseBean.setProofXML(
				(String) m.get("payerAuthEnrollReply_proofXML"));
			responseBean.setProxyPAN(
				(String) m.get("payerAuthEnrollReply_proxyPAN"));
			responseBean.setCommerceIndicator(
				(String) m.get("payerAuthEnrollReply_commerceIndicator"));
		} catch (ClientException ex)
		{
			VBVLogger.getLogger().error(
				"VBVSrvImpl-CheckEnroll " + ex.getLogString(),
				ex);
			if (ex.isCritical())
			{
				handleCriticalException(ex, csRequest);
			}
		} catch (FaultException ex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				ex.getLogString(),
				ex);
			if (ex.isCritical())
			{
				handleCriticalException(ex, csRequest);
			}
		}
		return (responseBean);
	}

	/**
	 * ValidateIssuerPares - Step 2 of the VeriBiVisa process.
	 * The Issuing bank returns a signed paRes key that must be 
	 * validated by CyberSource before we can use it to capture funds.
	 * The authValidate call may be doubled-up with a capture operation, 
	 * but that's not done here. This is only a validation call. 
	 * 
	 * A successful call will return 
	 * @param card
	 * @param pares
	 * @param identifier
	 * @return
	 */

	public VBVReplyValidateAuthentication validateAuthentication(
		CreditCard card,
		String pares,
		String referenceCode)
	{
		HashMap csRequest = new HashMap();

		csRequest.put("payerAuthValidateService_run", "true");
		csRequest.put("merchantId",	EMTSharedContainerProperties.getMerchantIdEMG());
		csRequest.put("payerAuthValidateService_signedPARes", pares);
		csRequest.put("merchantReferenceCode", referenceCode);
		csRequest.put("card_accountNumber", card.getAccountNumber());
		csRequest.put(
			"card_expirationMonth",
			String.valueOf(card.getExpireMonth()));
		csRequest.put(
			"card_expirationYear",
			String.valueOf(card.getExpireYear()));
		csRequest.put("card_cardType", "001");
		// 001 - CyberSource code for Visa.

		try
		{
			// Make CyberSource Call
			logHashMap("CybSrc ValidateAuth Request", csRequest);
			HashMap m = Client.runTransaction(csRequest, cybersourceConfig);
			logHashMap("CybSrc ValidateAuth Response", m);
			VBVReplyValidateAuthentication responseBean =
				new CyberSourceVBVReplyValidateAuthentication();

			responseBean.setRequestID((String) m.get("requestID"));
			// identifier
			responseBean.setDecision((String) m.get("decision"));
			// a word like ACCEPT
			responseBean.setReasonCode((String) m.get("reasonCode"));
			// reason code, number
			responseBean.setMerchantReferenceCode(
				(String) m.get("merchantReferenceCode"));
			responseBean.setXid((String) m.get("payerAuthValidateReply_xid"));
			responseBean.setCommerceIndicator(
				(String) m.get("payerAuthValidateReply_commerceIndicator"));
			responseBean.setCavv((String) m.get("payerAuthValidateReply_cavv"));
			responseBean.setEci((String) m.get("payerAuthValidateReply_eci"));
			responseBean.setEciRaw(
				(String) m.get("payerAuthValidateReply_eciRaw"));
			responseBean.setAuthenticationResult(
				(String) m.get("payerAuthValidateReply_authenticationResult"));
			responseBean.setAuthenticationStatusMessage(
				(String) m.get(
					"payerAuthValidateReply_authenticationStatusMessage"));
			return responseBean;

		} catch (ClientException ex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				ex.getLogString(),
				ex);
			if (ex.isCritical())
			{
				handleCriticalException(ex, csRequest);
			}
		} catch (FaultException ex)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				ex.getLogString(),
				ex);
			if (ex.isCritical())
			{
				handleCriticalException(ex, csRequest);
			}
		}
		return null;
	}

	public void insertTicket(CyberSourceVisaTicket inVVT, int transactionID)
		throws DataSourceException
	{
		CyberSourceVisaTicketDAO dao = new CyberSourceVisaTicketDAO();

		try
		{
			dao.insertTranAuthRecord(
				transactionID,
				inVVT.getXid(),
				inVVT.getCommerceIndicator(),
				inVVT.getEciRaw(),
				inVVT.getCavv(),
				inVVT.getProofXML(),
				inVVT.getRequestID());
			dao.commit();
		} catch (DataSourceException e)
		{
			VBVLogger.error(
				"VBVServiceImpl - saveTicket - DataSrcException. tranId:"
					+ transactionID);
			dao.rollback();
			throw e;
		} catch (Throwable t)
		{

			dao.rollback();
			throw new EMGRuntimeException(t);
		} finally
		{
			dao.close();
		}
	}

	public void saveCheckEnrollData(
		int transactionID,
		VBVReplyCheckEnrollment reply)
		throws DataSourceException
	{
		// this service tolerates a null bean. No problemo.
		if (reply == null)
			return;

		CyberSourceVisaTicketDAO dao = null;
		try
		{
			dao = new CyberSourceVisaTicketDAO();
			dao
				.insertTranAuthRecord(
					transactionID,
					reply.getXid(),
					reply.getCommerceIndicator(),
					null,
					null,
			// no cavv
			reply.getProofXML(), reply.getRequestID());
			dao.commit();
		} catch (DataSourceException e)
		{
			VBVLogger.error(
				"VBVServiceImpl - saveCheckEnrollData - DataSrcException. tranId:"
					+ transactionID);
			dao.rollback();
			throw e;
		} catch (Throwable t)
		{
			dao.rollback();
			throw new EMGRuntimeException(t);
		} finally
		{
			dao.close();
		}
	}

	public CyberSourceVisaTicket getTicket(int transactionID)
		throws DataSourceException
	{
		CyberSourceVisaTicketDAO dao = new CyberSourceVisaTicketDAO();
		CyberSourceVisaTicket outVVT = null;
		try
		{
			outVVT = dao.getTicket(transactionID);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Throwable t)
		{
			dao.rollback();
			throw new EMGRuntimeException(t);
		} finally
		{
			dao.close();
		}
		return outVVT;
	}

	/**
	 * An exception is considered critical if some type of disconnect occurs
	 * between the client and server and the client can't determine whether the
	 * transaction was successful. If this happens, you might have a transaction
	 * in the CyberSource system that your order system is not aware of. Because
	 * the transaction may have been processed by CyberSource, you should not
	 * resend the transaction, but instead send the exception information and
	 * the order information (customer name, order number, etc.) to the
	 * appropriate personnel at your company to resolve the problem. They should
	 * use the information as search criteria within the CyberSource Transaction
	 * Search Screens to find the transaction and determine if it was
	 * successfully processed. If it was, you should update your order system
	 * with the transaction information. Note that this is only a
	 * recommendation; it may not apply to your business model.
	 * 
	 * @param e
	 *            Critical ClientException object.
	 * @param request
	 *            Request that was sent.
	 */
	private void handleCriticalException(ClientException e, Map request)
	{
		//TODO what should we do
		// send the exception and order information to the appropriate
		// personnel at your company using any suitable method, e.g. e-mail,
		// multicast log, etc.
		throw new EMGRuntimeException(e);
	}

	/**
	 * See header comment in the other version of handleCriticalException above.
	 * 
	 * @param e
	 *            Critical ClientException object.
	 * @param request
	 *            Request that was sent.
	 */
	private void handleCriticalException(FaultException e, Map request)
	{
		//TODO what should we do
		// send the exception and order information to the appropriate
		// personnel at your company using any suitable method, e.g. e-mail,
		// multicast log, etc.
		throw new EMGRuntimeException(e);

	}

	private CyberSourceVBVServiceImpl()
	{

		cybersourceConfig.put("merchantID",	EMTSharedContainerProperties.getMerchantIdEMG());
		cybersourceConfig.put(
			"keysDirectory",
			EMTSharedContainerProperties.getCyberSourceKeysDirectory());
		//cybersourceConfig.put("targetAPIVersion", EMTSharedEnvProperties
		//		.getCyberSourceApiVersion());
		// override the default target API to the one we're tied to.
		// TODO - make this configurable.
		cybersourceConfig.put("targetAPIVersion", VBV_CYBERSOURCE_TARGET_API);

		cybersourceConfig.put(
			"sendToProduction",
			EMTSharedContainerProperties.getCyberSourceUseProduction());
		cybersourceConfig.put(
			"enableLog",
			EMTSharedContainerProperties.getCyberSourceLogEnabled());
		cybersourceConfig.put(
			"logDirectory",
			EMTSharedContainerProperties.getCyberSourceLogDirectory());
		cybersourceConfig.put(
			"logMaximumSize",
			new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
	}

	public void logHashMap(String mapTitle, HashMap m)
	{
		String noShowKey = "card_accountNumber";

		if (logr.isInfoEnabled() == false || m == null || m.isEmpty())
			return;
		String logit = mapTitle;
		String key;
		String val;
		for (Iterator itr = m.keySet().iterator(); itr.hasNext();)
		{
			key = (String) itr.next();
			if (key.equals(noShowKey))
				val = " -not shown ";
			else
				val = (String) m.get(key);
			logit += "\n" + key + "=" + val;
		}
		logr.info(logit);
	}

}
