
package emgshared.services.verifiedbyvisa;



/**
 * @author G.R. Svenddal
 * Created on Jun 17, 2005
 * CyberSourceVisaTicket
 * Bean to hold Cardholder Authentication Verification Values which are
 * the product of a successful Verified by Visa authentication.
 * Typically created on Consumer side from payerAuthValidateReply reply. 
 * Read and used by Admin side at time of funds capture.
 * TODO - Must be stored and read through Oracle, perhaps in the TransactionDAO. 
 */
public class CyberSourceVisaTicket 
{
	private String xid; // encrypted transaction id.
    private String cavv; // cardholder authentication verification value - 
	private String commerceIndicator; // should always be 'vbv', but just-in-case.
	private String eciRaw; // looks just like eci, but may be different. Some payment processors require this field. Not CyberSource apparantly, but 'just in case,' save it.
	private String proofXML; // even though we will require VBV auth success, this field is neccesary for proof of attempt in case we should decide to avail ourselves of this functionality.
	private String requestID; // the requestID returned from CyberSource for the request that produced the values in this bean.
	
	/**
	 * 
	 *
	 */
	protected CyberSourceVisaTicket(){
	}
	
	/**
	 * @return Returns the cavv.
	 */
	public String getCavv() {
		return cavv;
	}
	
	/**
	 * @return Returns the commerceIndicator.
	 */
	public String getCommerceIndicator() {
		return commerceIndicator;
	}

	/**
	 * @return Returns the eciRaw.
	 */
	public String getEciRaw() {
		return eciRaw;
	}

	/**
	 * @return Returns the proofXML.
	 */
	public String getProofXML() {
		return proofXML;
	}
	
	/**
	 * @return Returns the requestID.
	 */
	public String getRequestID() {
		return requestID;
	}

	/**
	 * @param cavv The cavv to set.
	 */
	protected void setCavv(String cavv) {
		this.cavv = cavv;
	}
	
	/**
	 * @param commerceIndicator The commerceIndicator to set.
	 */
	protected void setCommerceIndicator(String commerceIndicator) {
		this.commerceIndicator = commerceIndicator;
	}

	/**
	 * @param eciRaw The eciRaw to set.
	 */
	protected void setEciRaw(String eciRaw) {
		this.eciRaw = eciRaw;
	}

	/**
	 * @param proofXML The proofXML to set.
	 */
	protected void setProofXML(String proofXML) {
		this.proofXML = proofXML;
	}
	
	/**
	 * @param requestID The requestID to set.
	 */
	protected void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	/**
	 * 
	 * @return xid - encrypted transaction id from vendor's perspective.
	 */
	public String getXid()
	   {
	      return xid;
	   }
	
	public void setXid(String xid)
	   {
	      this.xid = xid;
	   }
	
	// TODO - refactor when Hugh gets the table design right.
	/**
	 * @param vab - will be optional, required now. 
	 * @param ceb - optional. Should be kept in session until validation complete, 
	 * @return
	 */
	public static CyberSourceVisaTicket createTicket(
	         VBVReplyValidateAuthentication vab,
	         VBVReplyCheckEnrollment ceb
	         )
	{
	     CyberSourceVisaTicket ticket = null;
         ticket = new CyberSourceVisaTicket();
         ticket.setCavv(vab.getCavv());
         ticket.setEciRaw(vab.getEciRaw());
         ticket.setCommerceIndicator(vab.getCommerceIndicator());
         ticket.setRequestID(vab.getRequestID());
         ticket.setXid(vab.getXid());
         if ( ceb != null ){
            ticket.setProofXML(ceb.getProofXML());
         }
       return ticket;
	}
	      
   /**
    * Factory for the Ticket. Used only by the DAO. Try to keep it that way.
    * @param xid
    * @param cavv
    * @param commerceIndicator
    * @param eciRaw
    * @param proofXML
    * @param requestID
    * @return
    */
   public static CyberSourceVisaTicket createTicket(
         String xid,
         String cavv,
         String commerceIndicator,
         String eciRaw,
         String proofXML,
         String requestID
         )
   {
      CyberSourceVisaTicket tik = new CyberSourceVisaTicket();
      tik.xid = xid;
      tik.cavv = cavv;
      tik.commerceIndicator = commerceIndicator;
      tik.eciRaw = eciRaw;
      tik.proofXML = proofXML;
      tik.requestID = requestID;
      return tik;
   }
   // helper for logging.
   public static String TicketFieldSummary(CyberSourceVisaTicket vvt,String tranId){
      String ret = " RI:" + vvt.getRequestID()
      	+ " CI:" + vvt.getCommerceIndicator()
      	+ " ER:" + vvt.getEciRaw() 
      	+ " CV:" + vvt.getCavv()
      	+ " PX:" + (( vvt.getProofXML() == null || vvt.getProofXML().equals(""))
      	      ?"none":"len="+(vvt.getProofXML().length())
      	+ " XD:" + vvt.getXid());
      
      	if ( tranId != null ){
      	   return "TRAN:" + tranId + ret;
      	}
      	return ret;
   }
   
   
}
