package emgshared.services.verifiedbyvisa;



/**
 * 
 * @author G.R.Svenddal
 * 
 * This should be abstract enough to implement any service's vbvCheckEnroll
 * service reply. But only being familiar with CyberSource at the moment, it's
 * probably going to be more specific to them then it should be. If and when
 * another service vendor is added, refactoring may ( probably will ) be needed. 
 *  
 *  
 */

public abstract class VBVReplyCheckEnrollment extends VBVServiceReply
{
     
   public VBVReplyCheckEnrollment()
     {
     }
       
   private String  paReq             = null; // payerAuthEnrollReply_paReq

   private String  acsURL            = null; // payerAuthEnrollReply_acsURL

   private String  proxyPAN          = null; 

   private String  proofXML          = null; // proof of authentication attempt.

   
   // These booleans are business decisions and must be mutually exclusive. 
   public abstract boolean bdEnrolled();

   public abstract boolean bdAttemptsProtected();

   public abstract boolean bdNotProtected();
   
   public abstract boolean bdError();
   

   

   public String getAcsURL()
   {
      return acsURL;
   }

   public void setAcsURL(String acsURL)
   {
      this.acsURL = acsURL;
   }

  
   public String getPaReq()
   {
      return paReq;
   }

   public void setPaReq(String paReq)
   {
      this.paReq = paReq;
   }

   public String getProofXML()
   {
      return proofXML;
   }

   public void setProofXML(String proofXML)
   {
      this.proofXML = proofXML;
   }

   public String getProxyPAN()
   {
      return proxyPAN;
   }

   public void setProxyPAN(String proxyPAN)
   {
      this.proxyPAN = proxyPAN;
   }
 
}