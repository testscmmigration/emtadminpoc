package emgshared.services.verifiedbyvisa;



/**
 * 
 * @author G.R.Svenddal
 * 
 * This should be abstract enough to implement any service's vbvCheckEnroll
 * service reply. But only being familiar with CyberSource at the moment, it's
 * probably going to be more specific to them then it should be. If and when
 * another service vendor is added, refactoring may ( probably will ) be needed. 
 *  
 *  
 */

public class CyberSourceVBVReplyCheckEnrollment extends VBVReplyCheckEnrollment
{
     
   
   // These booleans are business decisions and must be mutually exclusive. 
   public  boolean bdEnrolled(){ eval();  return decisionState == 1; }

   public  boolean bdAttemptsProtected(){eval(); return decisionState==2; }

   public  boolean bdNotProtected(){ eval(); return decisionState==3; }
   
   public  boolean bdProtected(){ eval(); return decisionState==4; }
   
   public  boolean bdError(){ eval(); return decisionState==5; }
   
   private int decisionState = -1;
   
   // TODO Validate and Complete this code by build 11 after analysis of real-world return values.
   private void eval(){
      if ( decisionState != -1 ) return; // eval has run. No need to do it again.
      decisionState = 5; // start with "set error state"
      if ( validateCoreReplyFields(this) == false )
      {
         VBVLogger.error("core ReplyCheckEnroll field missing");
         return;
      }
      if ( getDecision().equals("ERROR") )  return; // error
      if ( getDecision().equals("ACCEPT")){
         if ((getCommerceIndicator() != null ) 
         && getCommerceIndicator().equals("vbv_attempted")){
            decisionState=2;
            return;
         }
      }
      if ( getDecision().equals("REJECT") 
            && getReasonCode().equals("475")){
            decisionState = 1; // enrolled or enrollable. Move on.
      }
      return;
   } 
 }