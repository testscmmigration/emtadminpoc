package emgshared.services.verifiedbyvisa;

import emgshared.exceptions.DataSourceException;
import emgshared.model.CreditCard;

/**
 * @author G.R.Svenddal
 * Created on Jul 21, 2005
 * VBVService.java
 * Defines methods that any Verified By Visa service vendor
 * should provide. Implementations may vary.  
 **/

public interface VBVService
{
   public VBVReplyCheckEnrollment checkEnrollment(
 		CreditCard card,
 		String emgTrackingValue,
 		String unitPrice // optional value.
 		);
   
   public VBVReplyValidateAuthentication validateAuthentication(
		CreditCard card,
		String pares,
		String identifier);
   
   public void insertTicket(
         CyberSourceVisaTicket inVVT,
         int transactionID)
   throws DataSourceException;
   
   public void saveCheckEnrollData(
   int transactionID,
   VBVReplyCheckEnrollment reply)
	   throws DataSourceException;
	   
   
   public CyberSourceVisaTicket getTicket(
         int transactionId)
   throws DataSourceException; 
   
}
