package emgshared.services.verifiedbyvisa;

/**
 * 
 * @author G.R.Svenddal
 * 
 * VBVServiceResponse 
 * Encapsulates the return fields from a VerifiedByVisa Service calls
 * Provides business logic booleans to determine the service decision.
 * Also is a transport bean for SimpleSoap client and servlet.
 * Can also be a bean for the presentation ( jsp, currently )
 * 
 * 
 * This should be abstract enough to implement any service's implemented 
 * service reply. But only being familiar with CyberSource at the moment, it's
 * probably going to be more specific to them then it should be. If and when
 * another service vendor is added, refactoring may ( probably will ) be needed. 
 */

public abstract class VBVServiceReply
{

   /** Response fields common to all Verified By Visa calls
    *  implemented by any and all service vendors 
    */ 
  
   private String requestID  = null;
   private String decision   = null;
   private String reasonCode = null;
   private String xid = null;
   private String commerceIndicator = null;
   private String merchantReferenceCode = null;
   
   public VBVServiceReply()
   {
   }

   public String getRequestID()
   {
      return requestID;
   }

   public void setRequestID(String requestID)
   {
      this.requestID = requestID;
   }
   
   public String getDecision()
   {
      return decision;
   }

   public void setDecision(String decision)
   {
      this.decision = decision;
   }

   public String getReasonCode()
   {
      return reasonCode;
   }

   public void setReasonCode(String reasonCode)
   {
      this.reasonCode = reasonCode;
   }

   public String getXid()
   {
      return xid;
   }
   public void setXid(String xid)
   {
      this.xid = xid;
   }
   public String getCommerceIndicator()
   {
      return commerceIndicator;
   }
   public void setCommerceIndicator(String commerceIndicator)
   {
      this.commerceIndicator = commerceIndicator;
   }
   
   public String getMerchantReferenceCode()
   {
      return merchantReferenceCode;
   }
   
   public void setMerchantReferenceCode(String merchantReferenceCode)
   {
      this.merchantReferenceCode = merchantReferenceCode;
   }
   public static boolean validateCoreReplyFields(VBVServiceReply reply){
      return 
      reply.requestID != null 
      && reply.requestID.length() != 0 
      && reply.decision != null
      && reply.decision.length() != 0 
      && reply.reasonCode != null
      && reply.reasonCode.length() != 0;
   }
   
}