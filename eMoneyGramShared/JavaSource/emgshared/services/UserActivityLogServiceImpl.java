/*
 * Created on Apr 15, 2005
 *
 */
package emgshared.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.UserActivityLogDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;

/**
 * @author A131
 *
 */
class UserActivityLogServiceImpl implements UserActivityLogService
{
    private static List userActivityTypeList;
	private static long userActivityLogTypesRefreshed = 0;
	// refresh Interval expressed in MilliSeconds  - 4 hours for now
	private static final long refreshInterval = 240 * 1000 * 60;
	

    private static final UserActivityLogServiceImpl instance = new UserActivityLogServiceImpl();

//	private static final UserActivityLogService userActivityLogService =
//		emgshared.services.ServiceFactory.getInstance().getUserActivityLogService();

	private UserActivityLogServiceImpl()
	{
	}

	public static final UserActivityLogService getInstance()
	{
		return instance;
	}

	public UserActivityType getUserActivityTypeByCode(String code) throws Exception
	{
	    try {
		    ArrayList at = (ArrayList)this.getAllUserActivityTypes();
	        for (Iterator i = at.iterator(); i.hasNext();) {
	            UserActivityType uat = (UserActivityType) i.next();
	            if (uat.getUserActivityEventCode().equals(code))
	                return uat;
	        }
            
        } catch (Exception e) {
            throw e;
        }
        throw new Exception("User Activity Code is not found: " + code);
	}
	
	public List getAllUserActivityTypes() throws Exception
	{
	    try {
            if (this.checkUserActivityTypesRefreshNeeded())
                cacheUserActivityLogTypes();
                
        } catch (Exception e) {
            throw e;
        }
        return userActivityTypeList;
	}
	
	private void cacheUserActivityLogTypes() throws Exception
	{
		UserActivityLogDAO dao = new UserActivityLogDAO();
		try
		{
			// cache the Activity Log Types
		    userActivityTypeList = dao.getUserActivityTypes(null);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CST"));
			// set the cache last refreshed to now
			userActivityLogTypesRefreshed = cal.getTime().getTime();
			dao.commit();
	        EMGSharedLogger.getLogger(this.getClass().getName().toString()).warn(
            "User Activity Log - recached");
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Throwable t)
		{
			dao.rollback();
			throw new EMGRuntimeException(t);
		} finally
		{
			dao.close();
		}

	}
	
	public void insertUserActivityLog(String userID, UserActivityLog ual) throws Exception
	{
		UserActivityLogDAO dao = new UserActivityLogDAO();
		try
		{
		    // insert the activity log
			dao.insertUserActivityLog(userID, ual);
			dao.commit();
		} catch (DataSourceException e)
		{
			dao.rollback();
			throw e;
		} catch (Throwable t)
		{
			dao.rollback();
			throw new EMGRuntimeException(t);
		} finally
		{
			dao.close();
		}

	}
	
	
	/**
	 * @throws DataSourceException
	 * Checks to see if the cache for the UserActivityLogTypes is old, if so then re-cache it.
	 */
	private boolean checkUserActivityTypesRefreshNeeded() throws DataSourceException {
	    try {
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("CST"));
			Date currentTime = cal.getTime();
			long diff = currentTime.getTime() - userActivityLogTypesRefreshed;
			if ( (userActivityTypeList == null) || (diff > refreshInterval)	)
				return true;
			else
			    return false;            
        } catch (Exception e) {
            return true;
        }
	}

}
