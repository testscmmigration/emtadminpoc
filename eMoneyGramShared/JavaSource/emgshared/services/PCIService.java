package emgshared.services;

import com.moneygram.www.PCIDecryptService_v2.SourceIdRecord;

import emgshared.model.ConsumerAccountType;

public interface PCIService
 {
	
	public static enum SourceUsageCategoryCode {TRANSACTION, PROFILE, BLOCKED}

	public String retrieveCardNumber(String systemId, boolean isProfile) 
		throws Exception;
	
	public String retrieveCardNumber(int systemId, boolean isProfile)
		throws Exception;
	
	public String retrieveBlockedId(String cardNbr) throws Exception;
	
	public String retrieveCardNumber(String systemId,
			SourceUsageCategoryCode sourceUsageCategoryCode) throws Exception;
	
	public void storeCardNumber(String systemId, String cardNbr,
		ConsumerAccountType accountType) throws Exception;

	public SourceIdRecord[] retrieveListOfBlockedId(java.lang.String cardNbr)
			throws Exception; 
	


}
