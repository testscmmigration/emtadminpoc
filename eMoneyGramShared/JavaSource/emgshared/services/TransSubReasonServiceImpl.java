/*
 * Created on Jul 20, 2006
 *
 */
package emgshared.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import emgshared.dataaccessors.TransSubReasonDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.TranSubReason;

/**
 * @author A119
 * 
 */
public class TransSubReasonServiceImpl implements TransSubReasonService {

    private static final TransSubReasonServiceImpl instance = new TransSubReasonServiceImpl();

    private static List tranSubReasonsCached = null;
    private static List tranSubReasonsCachedSave = null;
    private static int nbrMinutesToCache = 30;
    private static long lastCached = 0;
    
    private TransSubReasonServiceImpl() {
    }

    public static final TransSubReasonService getInstance() {
        
        try {
            if ( (System.currentTimeMillis() - lastCached) > (1000 * 60 * nbrMinutesToCache) )
            {
                tranSubReasonsCachedSave = tranSubReasonsCached;
                TransSubReasonServiceImpl.cacheTranSubReasons();
            }            
        } catch (Exception ignore) {
            tranSubReasonsCached = tranSubReasonsCachedSave;
        }
        return instance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see emgadm.services.TransSubReasonService#getTranSubReason(java.lang.String,
     *      java.lang.String)
     */
    public TranSubReason getTranSubReason(String reasonTypeCode,
            String subReasonCode) throws Exception {
        try {
            for (Iterator i = tranSubReasonsCached.iterator(); i.hasNext();) {
                TranSubReason tsr = (TranSubReason) i.next();
                if (tsr.getTranReasonTypeCode().equals(reasonTypeCode) && 
                    tsr.getTranSubReasonCode().equals(subReasonCode))
                    return tsr;
            }
        } catch (Exception e) {
            throw e;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see emgadm.services.TransSubReasonService#getTranSubReasonsForTypeCode(java.lang.String)
     */
    public List getTranSubReasonsForTypeCode(String reasonTypeCode)
            throws Exception {

        List returnList = new ArrayList();
        try {
            for (Iterator i = tranSubReasonsCached.iterator(); i.hasNext();) {
                TranSubReason tsr = (TranSubReason) i.next();
                if (tsr.getTranReasonTypeCode().equals(reasonTypeCode))
                    returnList.add(tsr);
            }            
        } catch (Exception e) {
            throw e;
        }
        return returnList;
    }

    /*
     * (non-Javadoc) returns a List of TranSubReason's, keyed by TranSubReason
     * Code.
     */
    public List getTranSubReasonsAll() throws Exception {
        return tranSubReasonsCached;
    }

    
    /*
     * (non-Javadoc)
     * 
     * @see emgadm.services.TransSubReasonService#cacheTranSubReasons()
     */
    private static void cacheTranSubReasons() throws Exception {

        TransSubReasonDAO dao = new TransSubReasonDAO();
        try {
            tranSubReasonsCached = (List) dao
                    .getTransSubReasonByReasonTypeCode("", "");
            dao.commit();
            lastCached = System.currentTimeMillis();
        } catch (DataSourceException e) {
            dao.rollback();
            throw e;
        } catch (Throwable t) {
            dao.rollback();
            throw new EMGRuntimeException(t);
        } finally {
            dao.close();
        }
        return;
    }

}
