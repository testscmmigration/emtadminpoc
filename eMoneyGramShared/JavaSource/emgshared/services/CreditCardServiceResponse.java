/*
 * Created on Jan 31, 2005
 *
 */
package emgshared.services;

/**
 * @author A131
 *
 */
public interface CreditCardServiceResponse
{
	/**
	 * Returns a String identifying the authorization.
	 *
	 * For example, a credit card authorization returns
	 * an identifier that can then be passed to a capture
	 * or a reversal.
	 *
	 * @return
	 *
	 * Created on Jan 31, 2005
	 */
	public String getIdentifier();

	/**
	 * Whether the authorization was accepted.
	 * @return
	 *
	 * Created on Jan 31, 2005
	 */
	public boolean isAccepted();

	/**
	 * Whether the authorization was rejected.
	 * @return
	 * @see {@link #getReason() getReason}
	 * Created on Jan 31, 2005
	 */
	public boolean isRejected();

	/**
	 * Whether an error occurred communicating with the service.
	 * @return
	 * @see {@link #getReason() getReason}
	 * Created on Jan 31, 2005
	 */
	public boolean isError();

	/**
	 * Returns a code and/or description of the rejection or error.
	 * @return
	 *
	 * Created on Jan 31, 2005
	 */
	public String getReason();

	boolean isReTriable();

	/**
	 * Returns Address Verification code
	 * @return
	 */
	public String getAVSCode();
	public void setAVSCode(String avsCode);

	public String getProcessorTraceNbr();
	public void setProcessorTraceNbr(String processorTraceNbr);
	public String getProcessorFormattedRespText();
	public void setProcessorFormattedRespText(String processorFormattedRespText);
	public long getEffortId();
	public void setEffortId(long effortId);
}
