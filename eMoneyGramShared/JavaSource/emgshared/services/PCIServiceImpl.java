package emgshared.services;

import java.net.URL;
import java.util.Calendar;

import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;

import com.moneygram.www.PCIDecryptService_v2.AccountBySourceIdRequest;
import com.moneygram.www.PCIDecryptService_v2.AccountBySourceIdResponse;
import com.moneygram.www.PCIDecryptService_v2.SourceIdByAccountRequest;
import com.moneygram.www.PCIDecryptService_v2.SourceIdRecord;
import com.moneygram.www.PCIDecryptService_v2.PCIDecryptService_PortType;
import com.moneygram.www.PCIDecryptService_v2.PCIDecryptService_ServiceLocator;
import com.moneygram.www.PCIEncryptService_v2.CardType;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountRequest;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountResponse;
import com.moneygram.www.PCIEncryptService_v2.EncryptAccountResponseCode;
import com.moneygram.www.PCIEncryptService_v2.PCIEncryptService_PortType;
import com.moneygram.www.PCIEncryptService_v2.PCIEncryptService_ServiceLocator;

import emgshared.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerAccountType;
import emgshared.property.EMTSharedContainerProperties;

/**
 * @author W162
 * 
 * Service which handles the communication between eMT and the Moneygram PCI
 * Network which stores PAN data (card account numbers).
 */
class PCIServiceImpl implements PCIService {

	private static final PCIService instance = new PCIServiceImpl();

	private PCIEncryptService_PortType encryptService;
	private PCIDecryptService_PortType decryptService;
	
	private PCIServiceImpl(){
	}

	public static final PCIService getInstance() {
		return instance;
	}

	public String retrieveCardNumber(int systemId, boolean isProfile) throws Exception{
		return retrieveCardNumber(Integer.toString(systemId), isProfile);
	}

	public String retrieveCardNumber(String systemId, boolean isProfile) throws Exception{
		
		AccountBySourceIdResponse response = null;
		try {
			URL endpoint = new URL(EMTSharedContainerProperties.getPciDecryptServiceURL());
			String userName = EMTSharedContainerProperties.getPciDecryptUserName();
			String password = EMTSharedContainerProperties.getPciDecryptPassword();

			decryptService = new PCIDecryptService_ServiceLocator()
					.getPCIDecryptServiceSOAP_v2(endpoint);

			// provide the service account userID & password for authentication
			((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
			((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);

			AccountBySourceIdRequest request = new AccountBySourceIdRequest();
			request.setSourceSystemID(systemId);

			com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode catCode;
			com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode typeCode;

			if (isProfile) {
				catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.PROFILE;
				typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;
			} else {
				catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.TRANSACTION;
				typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.FORM_FREE;
			}
			
			request.setSourceSystemID(systemId);
			request.setSourceUsageCategory(catCode);
			request.setSourceUsageType(typeCode);
			request.setLastUsedDate(Calendar.getInstance());
			request.setUserID("EMT");

			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
					"Calling PCI Decrypt Service to retrieve card number...");

			response = decryptService.getAccountBySourceID(request);
			
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
					"Exception Attempting to retrieve Card Number - Systemid=" + systemId
							+ ";isProfile=" + isProfile, e);
			throw e;
		}
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
		"PCI Decrypt Service call completed...");
        
        return response.getPrimaryAccountNumber();
	}

	public String retrieveBlockedId(String cardNbr) throws Exception {		

		SourceIdRecord[] response = new SourceIdRecord[0];
		
		try {
			URL endpoint = new URL(EMTSharedContainerProperties.getPciDecryptServiceURL());
			String userName = EMTSharedContainerProperties.getPciDecryptUserName();
			String password = EMTSharedContainerProperties.getPciDecryptPassword();
			
			decryptService = new PCIDecryptService_ServiceLocator()
			.getPCIDecryptServiceSOAP_v2(endpoint);
			
			// provide the service account userID & password for authentication
			((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
			((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);
			
			SourceIdByAccountRequest request = new SourceIdByAccountRequest();
			request.setPrimaryAccountNumber(cardNbr);
			
			com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode catCode;
			com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode typeCode;
			
			catCode = 
				com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.BLOCKED;
			typeCode = 
				com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;
			
			request.setSourceUsageCategory (catCode);
			request.setSourceUsageType(typeCode);
			request.setPrimaryAccountNumber(cardNbr);
			request.setUserID("EMT");

			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).debug(
					"Calling PCI Decrypt Service to retrieve blockedId...");
			
			response = decryptService.getSourceIdByAccount(request);

		} catch (Exception e) {
			
			// Credit card was not found in PCI encrypted PAN table
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
			  "Exception Attempting to retrieve Blocked ID - cardNbr=" + 
			   StringHelper.maskCc(cardNbr), e);
			return "";
		}
		
		return response[0].getSourceSystemID();
	}
			
	public String retrieveCardNumber(String systemId,
		SourceUsageCategoryCode categoryCode) throws Exception {
		AccountBySourceIdResponse response = null;
		try {
			URL endpoint = new URL(EMTSharedContainerProperties.getPciDecryptServiceURL());
			String userName = EMTSharedContainerProperties.getPciDecryptUserName();
			String password = EMTSharedContainerProperties.getPciDecryptPassword();
	
			decryptService = new PCIDecryptService_ServiceLocator()
				.getPCIDecryptServiceSOAP_v2(endpoint);

			// provide the service account userID & password for authentication
			((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
			((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);

			AccountBySourceIdRequest request = new AccountBySourceIdRequest();
			request.setSourceSystemID(systemId);


			com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode catCode;
			com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode typeCode;

			if (categoryCode == PCIService.SourceUsageCategoryCode.PROFILE) {
				catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.PROFILE;
				typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;
			} else if (categoryCode == 
			           PCIService.SourceUsageCategoryCode.BLOCKED) {
			    catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.BLOCKED;
			    typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;
			} else {
			    catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.TRANSACTION;
			    typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.FORM_FREE;
			}
	
			request.setSourceUsageCategory(catCode);
			request.setSourceUsageType(typeCode);
			request.setLastUsedDate(Calendar.getInstance());
			request.setUserID("EMT");

			EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
					"Calling PCI Decrypt Service to retrieve card number...");

			response = decryptService.getAccountBySourceID(request);
	
		} catch (Exception e) {
			boolean isProfile;
			isProfile = 
                (categoryCode == PCIService.SourceUsageCategoryCode.PROFILE);

			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
					"Exception Attempting to retrieve Card Number - Systemid=" +
					 systemId + ";isProfile=" + isProfile, e);
			throw e;
		}
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
		"PCI Decrypt Service call completed...");
        
		return response.getPrimaryAccountNumber();
		}
		

	public void storeCardNumber(String systemId, String cardNbr,
			ConsumerAccountType accountType) throws Exception{

		try {
		URL endpoint = new URL(EMTSharedContainerProperties.getPciEncryptServiceURL());	    

		encryptService = new PCIEncryptService_ServiceLocator()
				.getPCIEncryptServiceSOAP_v2(endpoint);
		EncryptAccountRequest request = new EncryptAccountRequest();
		request.setPrimaryAccountNumber(cardNbr);
		request.setSourceSystemID(systemId);
		request.setUserID("EMT");
		
		CardType cardType;
		if (accountType == null) {
			cardType = null;
		} else if (accountType.equals(ConsumerAccountType.CREDIT_CARD_VISA)) {
			cardType = CardType.VI;
		} else if (accountType.equals(ConsumerAccountType.CREDIT_CARD_DISCOVER)) {
			cardType = CardType.DS;
		} else {
			cardType = CardType.MC;
		}
		
		request.setSourceUsageCategory(com.moneygram.www.PCIEncryptService_v2.SourceUsageCategoryCode.BLOCKED);
		request.setSourceUsageType(com.moneygram.www.PCIEncryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER);
		request.setPrimaryAccountNumber(cardNbr);
        request.setLastUsedDate(Calendar.getInstance());
        request.setUserID("EMT");

        EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
				"Calling PCI Encrypt Service to store card number...");


        EncryptAccountResponse response = encryptService
				.encryptAccount(request);
		
        EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
		"PCI Encrypt Service call completed...");
        
		if (!response.getResult().equals(EncryptAccountResponseCode.SUCCESS)) {
			throw new ServiceException("encryptAccount failed - ResponseCode = " + response.getResult());
		}
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
					"Exception Attempting to store Card Number - Systemid=" + systemId, e);
			throw e;			
		}
	}	
	
	public SourceIdRecord[] retrieveListOfBlockedId(String cardNbr)
			throws Exception {

		SourceIdRecord[] response = null;

		URL endpoint = new URL(
				EMTSharedContainerProperties.getPciDecryptServiceURL());
		String userName = EMTSharedContainerProperties.getPciDecryptUserName();
		String password = EMTSharedContainerProperties.getPciDecryptPassword();

		decryptService = new PCIDecryptService_ServiceLocator()
				.getPCIDecryptServiceSOAP_v2(endpoint);

		// provide the service account userID & password for authentication
		((Stub) decryptService)._setProperty(Call.USERNAME_PROPERTY, userName);
		((Stub) decryptService)._setProperty(Call.PASSWORD_PROPERTY, password);

		SourceIdByAccountRequest request = new SourceIdByAccountRequest();
		request.setPrimaryAccountNumber(cardNbr);

		com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode catCode;
		com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode typeCode;

		catCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageCategoryCode.PROFILE;
		typeCode = com.moneygram.www.PCIDecryptService_v2.SourceUsageTypeCode.EMONEY_TRANSFER;

		request.setSourceUsageCategory(catCode);
		request.setSourceUsageType(typeCode);
		request.setPrimaryAccountNumber(cardNbr);
		request.setUserID("EMT");

		EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug(
				"Calling PCI Decrypt Service to retrieve blockedId...");
		response = decryptService.getSourceIdByAccount(request);

		return response;
	}

	
}
