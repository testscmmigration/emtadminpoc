/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.services;

/**
 * @author A131
 *
 */
public class ServiceFactory
{
	private static final ServiceFactory instance = new ServiceFactory();

	private ServiceFactory()
	{
	}

	public static final ServiceFactory getInstance()
	{
		return instance;
	}

	public ConsumerProfileService getConsumerProfileService()
	{
		return ConsumerProfileServiceImpl.getInstance();
	}

	public CountryExceptionService getCountryExceptionService()
	{
		return CountryExceptionServiceImpl.getInstance();
	}

	public BillerLimitService getBillerLimitService()
	{
		return BillerLimitServiceImpl.getInstance();
	}

	public AccountingService getAccountingService()
	{
		return AccountingServiceImpl.getInstance();
	}

	public ConsumerAccountService getConsumerAccountService()
	{
		return ConsumerAccountServiceImpl.getInstance();
	}

	public CreditCardService getCreditCardService()
	{
		return CyberSource.getInstance();
	}

	public ObfuscationService getObfuscationService()
	{
		return ObfuscationServiceImpl.getInstance();
	}

	public CommentService getCommentService()
	{
		return CommentServiceImpl.getInstance();
	}

	public FraudService getFraudService()
	{
		return FraudServiceImpl.getInstance();
	}
	public TransactionService getTransactionService()
	{
		return TransactionServiceImpl.getInstance();
	}
	public PCIService getPCIService()
	{
		return PCIServiceImpl.getInstance();
	}
	public ScoringService getScoringService()
	{
		return ScoringServiceImpl.getInstance();
	}

	public CacheService getCacheService()
	{
		return CacheServiceImpl.getInstance();
	}
	
	public MicroDepositService getMicroDepositService()
	{
		return MicroDepositServiceImpl.getInstance();
	}

	public MessageService getMessageService()
	{
		return MessageServiceImpl.getInstance();
	}
	
	public TransSubReasonService getTransSubReasonService()
	{
		return TransSubReasonServiceImpl.getInstance();
	}
	
	public UserActivityLogService getUserActivityLogService()
	{
		return UserActivityLogServiceImpl.getInstance();
	}
}
