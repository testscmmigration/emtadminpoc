/*
 * Created on Apr 15, 2005
 *
 */
package emgshared.services;

import java.util.List;

import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;


/**
 * @author A131
 *
 */
public interface UserActivityLogService
{
	/**
	 * Provides basic services for inserting and retrieving User Activity Data
	 */
	public void insertUserActivityLog(String userID, UserActivityLog log) throws Exception;
	public List getAllUserActivityTypes() throws Exception;
	public UserActivityType getUserActivityTypeByCode(String code) throws Exception;
	}
