package emgshared.services;

import java.util.Collection;
import java.util.Map;

import emgshared.exceptions.DataSourceException;

public interface TransactionService
{
	Map getHolidays(String userId) throws DataSourceException;
	Collection getTransactionTypes(int partnerSiteCode) throws DataSourceException;
}
