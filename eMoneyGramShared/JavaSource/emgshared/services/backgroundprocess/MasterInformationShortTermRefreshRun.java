/*
 * Created on Sep 22, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.services.backgroundprocess;

import java.util.TimerTask;

import shared.mgo.services.AgentConnectServiceDoddFrank;
import shared.mgo.services.AgentConnectServiceDoddFrankImpl;

/**
 * @author rlopez2@moneygram.com
 */
public class MasterInformationShortTermRefreshRun extends TimerTask {

	private AgentConnectServiceDoddFrank acs = AgentConnectServiceDoddFrankImpl.getInstance();

	public void run() {
		// refresh country exception only
		System.out.println("MasterInformationRefreshRun - refresh only country exception");
		try {
			acs.refreshExceptionCountry();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
