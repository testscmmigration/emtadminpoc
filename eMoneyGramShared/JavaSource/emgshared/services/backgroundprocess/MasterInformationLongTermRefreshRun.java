package emgshared.services.backgroundprocess;

import java.util.TimerTask;

import shared.mgo.services.AgentConnectServiceDoddFrank;
import shared.mgo.services.AgentConnectServiceDoddFrankImpl;

/**
 * @author rlopez2@moneygram.com
 */
public class MasterInformationLongTermRefreshRun extends TimerTask {
	
	private AgentConnectServiceDoddFrank acs = AgentConnectServiceDoddFrankImpl.getInstance();

	public void run() {
		System.out.println("MasterInformationRefreshRun - more than one day");
		// master refresh
		try {
			acs.refreshMasterFromAgentConnect();
		} catch (Exception e) {
			e.printStackTrace();  
		}
	}

}
