/*
 * Created on Jan 6, 2005
 *
 */
package emgshared.services;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.NewConsumerBankAccount;
import emgshared.model.NewConsumerCreditCardAccount;

/**
 * @author A131
 *
 */
public interface ConsumerAccountService
{
	public ConsumerCreditCardAccount addConsumerCreditCardAccount(
			NewConsumerCreditCardAccount newAccount)
		throws DataSourceException;

	public ConsumerBankAccount addConsumerBankAccount(NewConsumerBankAccount newAccount)
		throws DataSourceException;

	public Set getAllAccountsByConsumerId(int consumerId)
		throws DataSourceException;

	public ConsumerAccount getAccountByAccountId(int consumerId, int accountId)
		throws DataSourceException;

	public Set getActiveAccountsByConsumerId(int consumerId)
		throws DataSourceException;

	public ConsumerBankAccount getBankAccount(int accountId, int consumerId)
		throws DataSourceException;

	public ConsumerBankAccount getBankAccount(
		int accountId,
		String callerUserId)
		throws DataSourceException;

	public ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		int consumerId)
		throws DataSourceException;

	public ConsumerCreditCardAccount getCreditCardAccount(
		int accountId,
		String callerUserId)
		throws DataSourceException;

	public String getEncryptedAccountNumber(int accountId, String callerUserId)
		throws DataSourceException;

	public ConsumerCreditCardAccount updateConsumerCreditCardAccount(ConsumerCreditCardAccount account)
		throws DataSourceException;

	public void updateCreditCardAccountType(
		String userId,
		int acctId,
		String acctType,
		String typeCode)
		throws DataSourceException;

	public ConsumerBankAccount updateConsumerBankAccount(ConsumerBankAccount account)
		throws DataSourceException;

	public ConsumerAccount updateStatus(
		int accountId,
		ConsumerAccountType type,
		AccountStatus newStatus,
		String callerUserId)
		throws DataSourceException;

	public ConsumerAccount deleteAccount(ConsumerAccount account)
		throws DataSourceException;

	public Map getAccountTypeDescriptions() throws DataSourceException;

	public Map getAccountTypeDescriptionsBank() throws DataSourceException;

	public Map getAccountTypeDescriptionsCreditCard()
		throws DataSourceException;

	public String getAccountTypeDescription(ConsumerAccountType accountType)
		throws DataSourceException;

	public Map getAccountStatusDescriptions() throws DataSourceException;

	public List getAccountCmnts(int accountId, String callerLoginId)
		throws DataSourceException;

	public Map getAccountCommentReasons() throws DataSourceException;

	public int addAccountComment(AccountComment comment, String callerLoginId)
		throws DataSourceException;

	public Collection getFilteredCCAccounts(
		String callerLoginId,
		String hashedText,
		String ccMask)
		throws DataSourceException;

	public Collection getFilteredBankAccounts(
		String callerLoginId,
		String abaNumber,
		String maskText)
		throws DataSourceException;

	public int getCreditCardCount(String userId, int id, String cardType)
		throws DataSourceException;

	public Set getAllAccountsByConsumerIdWithCmnts(
		int consumerId)
		throws DataSourceException;
}
