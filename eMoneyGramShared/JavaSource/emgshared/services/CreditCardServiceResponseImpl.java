package emgshared.services;

public class CreditCardServiceResponseImpl implements CreditCardServiceResponse {
	public static final String ACCEPTED = "ACCEPT";

	public static final String REJECTED = "REJECT";

	public static final String ERROR = "ERROR";

	private String identifier;

	private String decision;

	private String reasonCode;

	private boolean canReTry = false;

	private String avsCode = "-";

	private String processorTraceNbr;

	private String processorFormattedRespText;

	private long effortId;

	public CreditCardServiceResponseImpl(String identifier, String decision, String reasonCode) {
		this(decision, reasonCode);
		this.identifier = identifier;
	}

	public CreditCardServiceResponseImpl(String decision, String reasonCode) {
		this.decision = decision;
		this.reasonCode = reasonCode;

		if ((this.reasonCode != null)
				&& ("101".equals(this.reasonCode) || "102".equals(this.reasonCode)
						|| "150".equals(this.reasonCode) || "151".equals(this.reasonCode)
						|| "234".equals(this.reasonCode) || "236".equals(this.reasonCode)
						|| "241".equals(this.reasonCode) || "250".equals(this.reasonCode))) {
			canReTry = true;
		}
	}

	public String getIdentifier() {
		return this.identifier;
	}

	public boolean isAccepted() {
		return ACCEPTED.equalsIgnoreCase(this.decision);
	}

	public boolean isRejected() {
		return REJECTED.equalsIgnoreCase(this.decision);
	}

	public boolean isError() {
		return ERROR.equalsIgnoreCase(this.decision);
	}

	public String getReason() {
		return this.reasonCode;
	}

	public boolean isReTriable() {
		return this.canReTry;
	}

	public String getAVSCode() {
		return avsCode;
	}

	public void setAVSCode(String avsCode) {
		this.avsCode = avsCode;

	}
	public String getProcessorTraceNbr() {
		return processorTraceNbr;
	}

	public void setProcessorTraceNbr(String processorTraceNbr) {
		this.processorTraceNbr = processorTraceNbr;
	}

	public String getProcessorFormattedRespText() {
		return processorFormattedRespText;
	}

	public void setProcessorFormattedRespText(String processorFormattedRespText) {
		this.processorFormattedRespText = processorFormattedRespText;
	}

	public long getEffortId() {
		return effortId;
	}

	public void setEffortId(long effortId) {
		this.effortId = effortId;
	}
}
