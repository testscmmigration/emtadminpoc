/*
 * Created on Apr 15, 2005
 *
 */
package emgshared.services;

import java.util.Locale;

import com.moneygram.service.CreditCardPaymentService_v1.AuthorizationRequest;
import com.moneygram.service.CreditCardPaymentService_v1.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v1.BaseCreditCardPaymentRequest;
import com.moneygram.service.CreditCardPaymentService_v1.BaseCreditCardPaymentResponse;
import com.moneygram.service.CreditCardPaymentService_v1.CCPError;
import com.moneygram.service.CreditCardPaymentService_v1.PostTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v1.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.RefundTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v1.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.StatusRequest;
import com.moneygram.service.CreditCardPaymentService_v1.StatusResponse;

import emgshared.dataaccessors.ConsumerAccountDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.TransactionDAO;
import emgshared.model.AccountComment;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerStatus;
import emgshared.model.MicroDeposit;
import emgshared.model.TransactionComment;
import emgshared.util.I18NHelper;
import emgshared.util.Constants;

/**
 * @author A131
 *
 */
class CommentServiceImpl implements CommentService {
	private static final CommentServiceImpl instance = new CommentServiceImpl();

	private static final ConsumerProfileService profileService = emgshared.services.ServiceFactory
			.getInstance().getConsumerProfileService();

	private CommentServiceImpl() {
	}

	public static final CommentService getInstance() {
		return instance;
	}

	public void insertCreditCardResponseComment(
			CreditCardServiceResponse response, Integer accountId,
			Integer tranId, String resourceFile, String callerLoginId) {
		String commentText = null;
		String ccResponseCode = null;
		if (response != null) {
			ccResponseCode = response.getReason();

			if (resourceFile != null) {
				try {
					String messageKey = "credit.card.response."
							+ ccResponseCode;
					commentText = I18NHelper.getFormattedMessage(messageKey,
							resourceFile, (Locale) null);
				} catch (Throwable ignore) {
				}
			} else {
				commentText = "Description not available.";
			}

			commentText += "  Response code = " + ccResponseCode;
		} else {
			commentText = "Network Error: No response received from Cybersource";
		}

		if (tranId != null) {
			TransactionDAO transactionDao = new TransactionDAO();
			try {
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(tranId.intValue());
				transactionComment.setReasonCode("OTH");
				transactionComment.setText(commentText);
				transactionDao.insertComment(transactionComment, callerLoginId);
				transactionDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert credit card comment for transaction",
								t);
				transactionDao.rollbackAndIgnoreException();
			} finally {
				transactionDao.close();
			}
		}

		if (accountId != null) {
			ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
			try {
				AccountComment accountComment = new AccountComment();
				accountComment.setAccountId(accountId.intValue());
				accountComment.setReasonCode("OTH");
				accountComment.setText(commentText);
				accountDao.insertComment(accountComment, callerLoginId);
				accountDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert credit card comment for account",
								t);
				accountDao.rollbackAndIgnoreException();
			} finally {
				accountDao.close();
			}
		}
	}

	public void addBlockedProfileComment(ConsumerProfile consumerProfile,
			String commentType) {
		String reasonText;
		if (consumerProfile == null) {
			reasonText = "User was denied while creating a transaction because their profile is invalid.";
		} else if (consumerProfile.isSsnBlocked()) {
			reasonText = "User was denied while creating a transaction because their SSN is blocked.";
		} else if (consumerProfile.isPhoneBlocked()) {
			reasonText = "User was denied while creating a transaction because their Phone is blocked.";
		} else if (consumerProfile.isPhoneAlternateBlocked()) {
			reasonText = "User was denied while creating a transaction because their Alternate Phone is blocked.";
		} else if (consumerProfile.isAccountBlocked()) {
			reasonText = "User was denied while creating a transaction because one or several of their Accounts are blocked.";
		} else if (!consumerProfile.isPersonToPersonAllowed()) {
			reasonText = "User was denied while creating a transaction because they are not authorized to do an eMoney Transfer.";
		} else {
			reasonText = "User was denied while creating a transaction because their IP Address is blocked.";
		}
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setReasonCode(commentType);
			comment.setText(reasonText);
			profileService.addConsumerProfileComment(comment, String
					.valueOf(consumerProfile.getId()));
		} catch (Exception e) {
			// Not a serious error, move on.
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(
							"UpdateCustomerProfileAction - Failed to insert comment: "
									+ e);
		}
	}

	public void addMicroDepositFailComment(ConsumerProfile consumerProfile,
			MicroDeposit md, String commentType) {
		ConsumerStatus status = ConsumerStatus.getInstance(consumerProfile
				.getStatusCode(), consumerProfile.getSubStatusCode());
		String reasonText = "User exceeded the number of Micro Deposit confirmation attempts.  Status changed from "
				+ status.getCombinedCode()
				+ " to "
				+ ConsumerStatus.NON_ACTIVE_MICRO_DEPOSIT_FAIL.toString();
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setReasonCode(commentType);
			comment.setText(reasonText);
			profileService.addConsumerProfileComment(comment, String
					.valueOf(consumerProfile.getId()));
		} catch (Exception e) {
			// Not a serious error, move on.
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(
							"AccountConfirmDeposit - Failed to insert comment: "
									+ e);
		}
	}

	public void addVeridErrorFailedTransactionProfileComment(
			ConsumerProfile consumerProfile, String commentType,
			String reasonText) {
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setReasonCode(commentType);
			comment.setText(reasonText);
			profileService.addConsumerProfileComment(comment, String
					.valueOf(consumerProfile.getId()));
		} catch (Exception e) {
			// Not a serious error, move on.
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(
							"addVeridErrorFailedTransactionProfileComment - Failed to insert comment: "
									+ e);
		}

	}

	public void addVeridInitiateTransactionProfileComment(
			ConsumerProfile consumerProfile, String commentType,
			String reasonText) {
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setReasonCode(commentType);
			comment.setText(reasonText);
			profileService.addConsumerProfileComment(comment, String
					.valueOf(consumerProfile.getId()));
		} catch (Exception e) {
			// Not a serious error, move on.
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(
							"addVeridInitiateTransactionProfileComment - Failed to insert comment: "
									+ e);
		}
	}

	public void addVeridContinueTransactionProfileComment(
			ConsumerProfile consumerProfile, String commentType,
			String reasonText) {
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setReasonCode(commentType);
			comment.setText(reasonText);
			profileService.addConsumerProfileComment(comment, String
					.valueOf(consumerProfile.getId()));
		} catch (Exception e) {
			// Not a serious error, move on.
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.error(
							"addVeridContinueTransactionProfileComment - Failed to insert comment: "
									+ e);
		}
	}

	/**
	 *
	 * @param response
	 * @param accountId
	 * @param tranId
	 * @param resourceFile
	 * @param callerLoginId
	 */
	public void insertBankPaymentServiceResponseComment(String msgType, Integer tranId, boolean successful, String callerLoginId,
			String respCode, String approvalCode, String denialRecNbr, String achStatus, String providerTraceId, String clientTraceId) {
		String commentText = null;
		StringBuffer buf = new StringBuffer();

		if (respCode == null) {
			buf.append("TC ");
			buf.append(msgType);
			buf.append(": **Exception from BankPaymentService**|");
			buf.append("TC Trace ID: ");
			buf.append(providerTraceId);
			buf.append("|client TraceID: ");
			buf.append(clientTraceId);
		} else {
			if (successful) {
				buf.append("TC ");
				buf.append(msgType);
				buf.append(" Approved:");
				buf.append("RespCode=");
				buf.append(respCode);
				buf.append("|achStatus=");
				buf.append(achStatus);
				buf.append("|approvalCode=");
				buf.append(approvalCode);
				buf.append("|TC Trace ID: ");
				buf.append(providerTraceId);
				buf.append("|client TraceID: ");
				buf.append(clientTraceId);
			} else {
				buf.append("TC ");
				buf.append(msgType);
				buf.append(" **DENIED**:");
				buf.append("RespCode=");
				buf.append(respCode);
				buf.append("|denialRecNbr=");
				buf.append(denialRecNbr);
				buf.append("|TC Trace ID: ");
				buf.append(providerTraceId);
				buf.append("|client TraceID: ");
				buf.append(clientTraceId);
			}
		}

	    commentText = buf.toString();

		if (tranId != null) {
			TransactionDAO transactionDao = new TransactionDAO();
			try {
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(tranId.intValue());
				transactionComment.setReasonCode("OTH");
				transactionComment.setText(commentText);
				transactionDao.insertComment(transactionComment, callerLoginId);
				transactionDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert bank service comment for transaction",
								t);
				transactionDao.rollbackAndIgnoreException();
			} finally {
				transactionDao.close();
			}
		}
	}

	/**
	 *
	 * @param response
	 * @param accountId
	 * @param tranId
	 * @param resourceFile
	 * @param callerLoginId
	 */
	public void insertTransactionComment(Integer tranId, String callerLoginId, String commentText) {
		insertTransactionComment(tranId,callerLoginId,commentText,Constants.COMMENT_CODE_OTH);
	}

	public void insertTransactionComment(Integer tranId, String callerLoginId, String commentText, String reasonCode)
	{
		if (tranId != null) {
			TransactionDAO transactionDao = new TransactionDAO();
			try {
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(tranId.intValue());
				transactionComment.setReasonCode(reasonCode);
				transactionComment.setText(commentText);
				transactionDao.insertComment(transactionComment, callerLoginId);
				transactionDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert comment for transaction",
								t);
				transactionDao.rollbackAndIgnoreException();
			} finally {
				transactionDao.close();
			}
		}
	}

	/**
	 *
	 * @param response
	 * @param accountId
	 * @param tranId
	 * @param resourceFile
	 * @param callerLoginId
	 */
	public void insertAVSCreditCardResponseComment(String avsCode,
			Integer accountId, Integer tranId, String resourceFile,
			String callerLoginId, String accountNum) {
		String commentText = null;


		if (resourceFile != null) {
			try {
				String messageKey = "credit.card.avsresponse." + avsCode;
				commentText = I18NHelper.getFormattedMessage(messageKey,
						resourceFile, (Locale) null);
			} catch (Throwable ignore) {
			}
		} else {
			commentText = "AVS Code Description not available.";
		}


		commentText = " AVS Code: " + avsCode + " - " + commentText;
		if (null != accountNum) {
			commentText = "*****" + accountNum + " " + commentText;
		}

		//If we have a tran id, then add a tran level comment.
		if (tranId != null) {
			TransactionDAO transactionDao = new TransactionDAO();
			try {
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(tranId.intValue());
				transactionComment.setReasonCode("AVS");
				transactionComment.setText(commentText);
				transactionDao.insertComment(transactionComment, callerLoginId);
				transactionDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert AVS Code comment for transaction",
								t);
				transactionDao.rollbackAndIgnoreException();
			} finally {
				transactionDao.close();
			}
		}

        // If we have an account number, add the
		if (accountId != null) {
			ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
			try {
				AccountComment accountComment = new AccountComment();
				accountComment.setAccountId(accountId.intValue());
				accountComment.setReasonCode("OTH");
				accountComment.setText(commentText);
				accountDao.insertComment(accountComment, callerLoginId);
				accountDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert credit card comment for account",
								t);
				accountDao.rollbackAndIgnoreException();
			} finally {
				accountDao.close();
			}
		}
	}

	public void insertGCCardResponseComment(CreditCardServiceResponse response, Integer accountId, Integer tranId, String resourceFile,
			String callerLoginId) {
		String commentText = null;
		String ccResponseCode = null;
		StringBuffer buf=null;
		if (response != null) {
			ccResponseCode = response.getReason();

			if (resourceFile != null) {
				try {
					buf=new StringBuffer();
					String messageKey = "credit.card.response."
							+ ccResponseCode;
					commentText = I18NHelper.getFormattedMessage(messageKey,
							resourceFile, (Locale) null);
					String[] params = {
							String.valueOf(tranId.intValue()),
							ccResponseCode, commentText };

					buf.append(I18NHelper.getFormattedMessage("error.partial.refund.fail.notice",params,
							resourceFile, (Locale) null));
				} catch (Throwable ignore) {
				}
			} else {
				commentText = "Description not available.";
			}
			commentText += "  Response code = " + ccResponseCode;
		} else {
			commentText = "Network Error: No response received from Cybersource";
		}

		if (tranId != null) {
			TransactionDAO transactionDao = new TransactionDAO();
			try {
				TransactionComment transactionComment = new TransactionComment();
				transactionComment.setTranId(tranId.intValue());
				transactionComment.setReasonCode("OTH");
				//Truncate to fit into DB column
				if (buf.length() > 255) {
					buf.delete(255, buf.length());
				}
				
				transactionComment.setText(buf!=null?buf.toString():commentText);
				transactionDao.insertComment(transactionComment, callerLoginId);
				transactionDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert credit card comment for transaction",
								t);
				transactionDao.rollbackAndIgnoreException();
			} finally {
				transactionDao.close();
			}
		}

		if (accountId != null) {
			ConsumerAccountDAO accountDao = new ConsumerAccountDAO();
			try {
				AccountComment accountComment = new AccountComment();
				accountComment.setAccountId(accountId.intValue());
				accountComment.setReasonCode("OTH");
				accountComment.setText(commentText);
				accountDao.insertComment(accountComment, callerLoginId);
				accountDao.commit();
			} catch (Throwable t) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.warn(
								"Failed to auto insert credit card comment for account",
								t);
				accountDao.rollbackAndIgnoreException();
			} finally {
				accountDao.close();
			}
		}		
	}

}
