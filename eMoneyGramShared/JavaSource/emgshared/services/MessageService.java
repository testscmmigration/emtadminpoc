package emgshared.services;

import java.util.List;
import java.util.Map;

import emgshared.exceptions.DataSourceException;
import emgshared.model.AdminMessage;

public interface MessageService
{
	List getAdminMessages(String userId, AdminMessage criteria)
		throws DataSourceException;

	Map getActiveUserMessages(String userId) throws DataSourceException;

	List getAdminMessageTypes(String userId) throws DataSourceException;

	AdminMessage insertAdminMessage(
		String userId,
		AdminMessage msg,
		String[] users)
		throws DataSourceException;

	List getUserAdminMessages(
		String userId,
		String recipntId,
		boolean unreadOnly)
		throws DataSourceException;

	void setMsgRecipnt(int msgId, String recipntId, boolean toUnread)
		throws DataSourceException;

	void purgeMessage(String userId, int msgId)
		throws DataSourceException;

	List getAdminMessageRecipients(String userId, int msgId)
		throws DataSourceException;
}
