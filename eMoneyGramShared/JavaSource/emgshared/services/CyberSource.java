package emgshared.services;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.moneygram.common.log.Logger;

import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.CreditCard;
import emgshared.model.ExchangeRateType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.services.verifiedbyvisa.CyberSourceVisaTicket;

/**
 * @author A131
 *
 */
class CyberSource implements CreditCardService
{
	private static final DecimalFormat amountFormatter = new DecimalFormat("#0.00");
	private static final String ACCOUNT_NUMBER_KEY = "card_accountNumber";
	private static final CreditCardService instance = new CyberSource();
	

	static {
		amountFormatter.setGroupingUsed(false);
	}

	public static final CreditCardService getInstance()
	{
		return instance;
	}

	private CyberSource()
	{
	}

	public CreditCardServiceResponse executeAuthorization(
		ConsumerProfile consumerProfile,
		CreditCard creditCard,
		ConsumerAddress billingAddress,
		double amount,
		boolean performAddressVerification,
		String emgTrackingValue,
		String merchantId)
	{
		return executeAuthorizationAndOptionalCapture(
			consumerProfile,
			creditCard,
			billingAddress,
			amount,
			performAddressVerification,
			false,
			emgTrackingValue,
			merchantId);
	}

	private CreditCardServiceResponse executeAuthorizationAndOptionalCapture(
		ConsumerProfile consumerProfile,
		CreditCard creditCard,
		ConsumerAddress billingAddress,
		double amount,
		boolean performAddressVerification,
		boolean performCapture,
		String emgTrackingValue,
		String merchantId)
	{
		
		CreditCardServiceResponse response = null;
		Properties cybersourceConfig = new Properties();
		cybersourceConfig.put("keysDirectory",EMTSharedContainerProperties.getCyberSourceKeysDirectory());
		cybersourceConfig.put("targetAPIVersion",EMTSharedContainerProperties.getCyberSourceApiVersion());
		cybersourceConfig.put("sendToProduction",EMTSharedContainerProperties.getCyberSourceUseProduction());
		cybersourceConfig.put("enableLog",EMTSharedContainerProperties.getCyberSourceLogEnabled());
		cybersourceConfig.put("logDirectory",EMTSharedContainerProperties.getCyberSourceLogDirectory());
		cybersourceConfig.put("logMaximumSize",	new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
		cybersourceConfig.put("timeout", Integer.parseInt(EMTSharedContainerProperties.getCyberSourceTimeoutSeconds()));
		cybersourceConfig.put("merchantID", merchantId);

		HashMap request = new HashMap();

		request.put("ccAuthService_run", "true");
		request.put("decisionManager_enabled", "false");
		EMGSharedLogger.getLogger("decisionManager_enabled:: false ");
		if (performCapture)
		{
			request.put("ccCaptureService_run", "true");
		}

		request.put("merchantId", merchantId);
		// this is your own tracking number.  CyberSource recommends that you
		// use a unique one for each order.
		request.put("merchantReferenceCode", emgTrackingValue == null ? "emg" : emgTrackingValue);
		request.put("billTo_firstName", consumerProfile.getFirstName());
		request.put("billTo_lastName", consumerProfile.getLastName());
		request.put("billTo_street1", billingAddress.getAddressLine1());
		request.put("billTo_city", billingAddress.getCity());
		request.put("billTo_state", billingAddress.getState());
		request.put("billTo_postalCode", billingAddress.getPostalCode());
		request.put("billTo_country", "US");
		ConsumerEmail consumerEmail = null;
		//TODO Future will allow for multiple emails.
		//Which email should then be assigned to the billTo_email.
		for (Iterator iter = consumerProfile.getEmails().iterator();
			iter.hasNext();)
		{
			consumerEmail = (ConsumerEmail) iter.next();
			request.put("billTo_email", consumerEmail.getConsumerEmail());
			break;
		}
		request.put(ACCOUNT_NUMBER_KEY, creditCard.getAccountNumber());
		String cvv = creditCard.getCvv();
		if (StringUtils.isNotBlank(cvv))
		{
			request.put("card_cvNumber", cvv);
		}

		request.put("card_expirationMonth",	String.valueOf(creditCard.getExpireMonth()));
		request.put("card_expirationYear",	String.valueOf(creditCard.getExpireYear()));
		request.put("purchaseTotals_currency",ExchangeRateType.USD.getTypeCode());
		request.put("purchaseTotals_grandTotalAmount",amountFormatter.format(amount));
		if (performAddressVerification == false)
		{
			request.put("businessRules_ignoreAVSResult", "true");
		} else
		{
			request.put("businessRules_declineAVSFlags",EMTSharedContainerProperties.getAvsDeclineFlags());
		}

		try
		{
			logProperties("CREDIT CARD REQUEST:", request);
			HashMap reply = Client.runTransaction(request, cybersourceConfig);
			logProperties("CREDIT CARD REPLY:", reply);
			String decision = (String) reply.get("decision");
			String reasonCode = (String) reply.get("reasonCode");
			String requestId = (String) reply.get("requestID");
			response = new CyberSourceResponse(requestId, decision, reasonCode);
			response.setAVSCode((String)reply.get("ccAuthReply_avsCode"));

			if (response.isRejected() || response.isError())
			{
				StringBuffer errorText = new StringBuffer(128);
				errorText.append("Credit card authorization failed.");
				errorText.append("emgTrackingValue = ");
				errorText.append(emgTrackingValue);
				errorText.append(".  CyberSource request id = ");
				errorText.append(response.getIdentifier());
				errorText.append(".  CyberSource decision = ");
				errorText.append(decision);
				errorText.append(".  CyberSource reason code = ");
				errorText.append(response.getReason());
				errorText.append(".");
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(errorText.toString());
			}
		} catch (ClientException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),	e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		} catch (FaultException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		}
		
		return (response);
	}

	public CreditCardServiceResponse executeCapture(Object obj,	double amount,	String emgTrackingValue, String merchantId)
	{
		
		CreditCardServiceResponse response = null;
		String authRequestId = (String) obj;
		HashMap request = new HashMap();
		request.put("ccCaptureService_run", "true");
		// reference the requestId returned by the previous auth.
		request.put("merchantId", merchantId);
		request.put("ccCaptureService_authRequestID", authRequestId);
		request.put("merchantReferenceCode",emgTrackingValue == null ? "emg" : emgTrackingValue);
		request.put("purchaseTotals_grandTotalAmount",	amountFormatter.format(amount));
		request.put("purchaseTotals_currency",ExchangeRateType.USD.getTypeCode());
		try
		{
			logProperties("FOLLOW-ON CAPTURE REQUEST:", request);
			Properties cybersourceConfig = new Properties();
			cybersourceConfig.put("keysDirectory",EMTSharedContainerProperties.getCyberSourceKeysDirectory());
			cybersourceConfig.put("targetAPIVersion",EMTSharedContainerProperties.getCyberSourceApiVersion());
			cybersourceConfig.put("sendToProduction",EMTSharedContainerProperties.getCyberSourceUseProduction());
			cybersourceConfig.put("enableLog",EMTSharedContainerProperties.getCyberSourceLogEnabled());
			cybersourceConfig.put("logDirectory",EMTSharedContainerProperties.getCyberSourceLogDirectory());
			cybersourceConfig.put("logMaximumSize",	new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
			cybersourceConfig.put("timeout", Integer.parseInt(EMTSharedContainerProperties.getCyberSourceTimeoutSeconds()));
			
			cybersourceConfig.put("merchantID", merchantId);
			HashMap reply = Client.runTransaction(request, cybersourceConfig);
			logProperties("FOLLOW-ON CAPTURE REPLY:", reply);

			String decision = (String) reply.get("decision");
			String reasonCode = (String) reply.get("reasonCode");
			String requestId = (String) reply.get("requestID");
			response = new CyberSourceResponse(requestId, decision, reasonCode);

			if (response.isRejected() || response.isError())
			{
				StringBuffer errorText = new StringBuffer(128);
				errorText.append("Credit card capture failed.");
				errorText.append("emgTrackingValue = ");
				errorText.append(emgTrackingValue);
				errorText.append(".  CyberSource request id = ");
				errorText.append(response.getIdentifier());
				errorText.append(".  CyberSource decision = ");
				errorText.append(decision);
				errorText.append(".  CyberSource reason code = ");
				errorText.append(response.getReason());
				errorText.append(".");
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(errorText.toString());
			}
		} catch (ClientException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),	e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		} catch (FaultException e)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(e.getLogString(),e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		}
		
		return response;
	}

	/* (non-Javadoc)
	 * @see emgshared.services.CreditCardService#executeCredit(emgshared.model.ConsumerProfile, emgshared.model.CreditCard, emgshared.model.ConsumerAddress, double, java.lang.String)
	 */
	public CreditCardServiceResponse executeCredit(
		ConsumerProfile consumerProfile,
		CreditCard creditCard,
		ConsumerAddress billingAddress,
		double amount,
		String emgTrackingValue,
		String merchantId)
	{
		
		CreditCardServiceResponse response = null;
		HashMap request = new HashMap();

		request.put("ccCreditService_run", "true");
		request.put("merchantId", merchantId);
		// this is your own tracking number.  CyberSource recommends that you
		// use a unique one for each order.
		request.put("merchantReferenceCode",emgTrackingValue == null ? "emg" : emgTrackingValue);
		request.put("billTo_firstName", consumerProfile.getFirstName());
		request.put("billTo_lastName", consumerProfile.getLastName());
		request.put("billTo_street1", billingAddress.getAddressLine1());
		request.put("billTo_city", billingAddress.getCity());
		request.put("billTo_state", billingAddress.getState());
		request.put("billTo_postalCode", billingAddress.getPostalCode());
		request.put("billTo_country", "US");
		ConsumerEmail consumerEmail = null;
		//TODO Future will allow for multiple emails.
		//Which email should then be assigned to the billTo_email.
		for (Iterator iter = consumerProfile.getEmails().iterator();
			iter.hasNext();
			)
		{
			consumerEmail = (ConsumerEmail) iter.next();
			request.put("billTo_email", consumerEmail.getConsumerEmail());
			break;
		}
		request.put(ACCOUNT_NUMBER_KEY, creditCard.getAccountNumber());
		String cvv = creditCard.getCvv();
		if (StringUtils.isNotBlank(cvv))
		{
			request.put("card_cvNumber", cvv);
		}

		request.put("card_expirationMonth",	String.valueOf(creditCard.getExpireMonth()));
		request.put("card_expirationYear", String.valueOf(creditCard.getExpireYear()));
		request.put("purchaseTotals_currency",	ExchangeRateType.USD.getTypeCode());
		request.put("purchaseTotals_grandTotalAmount",	amountFormatter.format(amount));
		try
		{
			logProperties("CREDIT CARD CREDIT REQUEST:", request);
			Properties cybersourceConfig = new Properties();
			cybersourceConfig.put("keysDirectory",EMTSharedContainerProperties.getCyberSourceKeysDirectory());
			cybersourceConfig.put("targetAPIVersion",EMTSharedContainerProperties.getCyberSourceApiVersion());
			cybersourceConfig.put("sendToProduction",EMTSharedContainerProperties.getCyberSourceUseProduction());
			cybersourceConfig.put("enableLog",EMTSharedContainerProperties.getCyberSourceLogEnabled());
			cybersourceConfig.put("logDirectory",EMTSharedContainerProperties.getCyberSourceLogDirectory());
			cybersourceConfig.put("logMaximumSize",	new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
			cybersourceConfig.put("timeout", Integer.parseInt(EMTSharedContainerProperties.getCyberSourceTimeoutSeconds()));
			
			cybersourceConfig.put("merchantID", merchantId);
			HashMap reply = Client.runTransaction(request, cybersourceConfig);
			logProperties("CREDIT CARD CREDIT REPLY:", reply);
			String decision = (String) reply.get("decision");
			String reasonCode = (String) reply.get("reasonCode");
			String requestId = (String) reply.get("requestID");
			response = new CyberSourceResponse(requestId, decision, reasonCode);

			if (response.isRejected() || response.isError())
			{
				StringBuffer errorText = new StringBuffer(128);
				errorText.append("Credit card credit failed.");
				errorText.append("emgTrackingValue = ");
				errorText.append(emgTrackingValue);
				errorText.append(".  CyberSource request id = ");
				errorText.append(response.getIdentifier());
				errorText.append(".  CyberSource decision = ");
				errorText.append(decision);
				errorText.append(".  CyberSource reason code = ");
				errorText.append(response.getReason());
				errorText.append(".");
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(errorText.toString());
			}
		} catch (ClientException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		} catch (FaultException e)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(e.getLogString(),e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		}
		
		return (response);
	}

	public CreditCardServiceResponse executeCredit(
		Object obj,
		double amount,
		String emgTrackingValue,
		String merchantId)
	{
		
		CreditCardServiceResponse response = null;
		HashMap request = new HashMap();

		request.put("ccCreditService_run", "true");
		request.put("merchantId", merchantId);
		// this is your own tracking number.  CyberSource recommends that you
		// use a unique one for each order.
		request.put("merchantReferenceCode", emgTrackingValue == null ? "emg" : emgTrackingValue);

		// reference the requestId returned by the previous auth.
		request.put("ccCreditService_captureRequestID", (String) obj);
		request.put("purchaseTotals_currency",	ExchangeRateType.USD.getTypeCode());
		request.put("purchaseTotals_grandTotalAmount", amountFormatter.format(amount));

		try
		{
			logProperties("CREDIT CARD CREDIT REQUEST:", request);
			Properties cybersourceConfig = new Properties();
			cybersourceConfig.put("keysDirectory",EMTSharedContainerProperties.getCyberSourceKeysDirectory());
			cybersourceConfig.put("targetAPIVersion",EMTSharedContainerProperties.getCyberSourceApiVersion());
			cybersourceConfig.put("sendToProduction",EMTSharedContainerProperties.getCyberSourceUseProduction());
			cybersourceConfig.put("enableLog",EMTSharedContainerProperties.getCyberSourceLogEnabled());
			cybersourceConfig.put("logDirectory",EMTSharedContainerProperties.getCyberSourceLogDirectory());
			cybersourceConfig.put("logMaximumSize",	new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
			cybersourceConfig.put("timeout", Integer.parseInt(EMTSharedContainerProperties.getCyberSourceTimeoutSeconds()));
			
			cybersourceConfig.put("merchantID", merchantId);
			HashMap reply = Client.runTransaction(request, cybersourceConfig);
			logProperties("CREDIT CARD CREDIT REPLY:", reply);
			String decision = (String) reply.get("decision");
			String reasonCode = (String) reply.get("reasonCode");
			String requestId = (String) reply.get("requestID");
			response = new CyberSourceResponse(requestId, decision, reasonCode);

			if (response.isRejected() || response.isError())
			{
				StringBuffer errorText = new StringBuffer(128);
				errorText.append("Credit card credit failed.");
				errorText.append("emgTrackingValue = ");
				errorText.append(emgTrackingValue);
				errorText.append(".  CyberSource request id = ");
				errorText.append(response.getIdentifier());
				errorText.append(".  CyberSource decision = ");
				errorText.append(decision);
				errorText.append(".  CyberSource reason code = ");
				errorText.append(response.getReason());
				errorText.append(".");
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(errorText.toString());
			}
		} catch (ClientException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),	e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		} catch (FaultException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		}
		
		return (response);
	}

	public void reverseAuthorization(
		Object obj,
		double amount,
		String emgTrackingValue,
		String merchantId)
	{
		
		String authRequestId = (String) obj;
		HashMap request = new HashMap();
		request.put("ccAuthReversalService_run", "true");
		request.put("merchantReferenceCode", emgTrackingValue == null ? "emg" : emgTrackingValue);
		// reference the requestId returned by the previous auth.
		request.put("ccAuthReversalService_authRequestID", authRequestId);
		request.put("purchaseTotals_currency",ExchangeRateType.USD.getTypeCode());
		request.put("purchaseTotals_grandTotalAmount",amountFormatter.format(amount));
		try
		{
			logProperties("FOLLOW-ON AUTH REVERSAL REQUEST:", request);
			Properties cybersourceConfig = new Properties();
			cybersourceConfig.put("keysDirectory",EMTSharedContainerProperties.getCyberSourceKeysDirectory());
			cybersourceConfig.put("targetAPIVersion",EMTSharedContainerProperties.getCyberSourceApiVersion());
			cybersourceConfig.put("sendToProduction",EMTSharedContainerProperties.getCyberSourceUseProduction());
			cybersourceConfig.put("enableLog",EMTSharedContainerProperties.getCyberSourceLogEnabled());
			cybersourceConfig.put("logDirectory",EMTSharedContainerProperties.getCyberSourceLogDirectory());
			cybersourceConfig.put("logMaximumSize",	new Integer(EMTSharedContainerProperties.getCyberSourceLogMaxSizeMb()));
			cybersourceConfig.put("timeout", Integer.parseInt(EMTSharedContainerProperties.getCyberSourceTimeoutSeconds()));
			
			cybersourceConfig.put("merchantID", merchantId);
			HashMap reply = Client.runTransaction(request, cybersourceConfig);
			logProperties("FOLLOW-ON AUTH REVERSAL REPLY:", reply);
		} catch (ClientException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(), e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		} catch (FaultException e)
		{
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getLogString(),	e);
			if (e.isCritical())
			{
				handleCriticalException(e, request);
			}
		}
	}

	/**
	 * Displays the content of the Map object.
	 *
	 * @param header	Header text.
	 * @param map		Map object to display.
	 */
	private void logProperties(String header, Map map)
	{
		Logger logger =	EMGSharedLogger.getLogger(this.getClass().getName().toString());
		if (logger.isDebugEnabled())
		{
			logger.debug(header);
			StringBuffer dest = new StringBuffer();
			if (map != null && !map.isEmpty())
			{
				Iterator iter = map.keySet().iterator();
				String key, val;
				while (iter.hasNext())
				{
					key = (String) iter.next();
					val = (String) map.get(key);
					if (ACCOUNT_NUMBER_KEY.equalsIgnoreCase(key))
					{
						val = "*****" + StringUtils.right(val, 4);
					}
					dest.append(key + "=" + val + "\n");
				}
			}

			logger.debug(dest.toString());
		}
	}

	/**
	 * An exception is considered critical if some type of disconnect occurs
	 * between the client and server and the client can't determine whether the
	 * transaction was successful. If this happens, you might have a
	 * transaction in the CyberSource system that your order system is not
	 * aware of. Because the transaction may have been processed by
	 * CyberSource, you should not resend the transaction, but instead send the
	 * exception information and the order information (customer name, order
	 * number, etc.) to the appropriate personnel at your company to resolve
	 * the problem. They should use the information as search criteria within
	 * the CyberSource Transaction Search Screens to find the transaction and
	 * determine if it was successfully processed. If it was, you should update
	 * your order system with the transaction information. Note that this is
	 * only a recommendation; it may not apply to your business model.
	 *
	 * @param e			Critical ClientException object.
	 * @param request	Request that was sent.
	 */
	private void handleCriticalException(ClientException e, Map request)
	{
		//TODO what should we do
		// send the exception and order information to the appropriate
		// personnel at your company using any suitable method, e.g. e-mail,
		// multicast log, etc.
		throw new EMGRuntimeException(e);
	}

	/**
	 * See header comment in the other version of handleCriticalException
	 * above.
	 *
	 * @param e			Critical ClientException object.
	 * @param request	Request that was sent.
	 */
	private void handleCriticalException(FaultException e, Map request)
	{
		//TODO what should we do
		// send the exception and order information to the appropriate
		// personnel at your company using any suitable method, e.g. e-mail,
		// multicast log, etc.
		throw new EMGRuntimeException(e);
	}
}
