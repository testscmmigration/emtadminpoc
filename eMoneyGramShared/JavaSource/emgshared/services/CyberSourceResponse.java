/*
 * Created on Jan 31, 2005
 *
 */
package emgshared.services;

/**
 * @author A131
 *
 */
public class CyberSourceResponse implements CreditCardServiceResponse
{
	public static final String ACCEPTED = "ACCEPT";
	public static final String REJECTED = "REJECT";
	public static final String ERROR = "ERROR";

	private String identifier;
	private String decision;
	private String reasonCode;
	private boolean canReTry = false;
	private String avsCode = "-";
	private String processorTraceNbr;

	private String processorFormattedRespText;

	public CyberSourceResponse(
		String identifier,
		String decision,
		String reasonCode)
	{
		this.identifier = identifier;
		this.decision = decision;
		this.reasonCode = reasonCode;

		if (this.reasonCode != null
			&& ("101".equals(this.reasonCode)
				|| "102".equals(this.reasonCode)
				|| "150".equals(this.reasonCode)
				|| "151".equals(this.reasonCode)
				|| "234".equals(this.reasonCode)
				|| "236".equals(this.reasonCode)
				|| "241".equals(this.reasonCode)
				|| "250".equals(this.reasonCode))) {
			canReTry = true;
		}
	}

	public String getIdentifier()
	{
		return this.identifier;
	}

	public boolean isAccepted()
	{
		return ACCEPTED.equalsIgnoreCase(this.decision);
	}

	public boolean isRejected()
	{
		return REJECTED.equalsIgnoreCase(this.decision);
	}

	public boolean isError()
	{
		return ERROR.equalsIgnoreCase(this.decision);
	}

	public String getReason()
	{
		return this.reasonCode;
	}

	public boolean isReTriable()
	{
		return this.canReTry;
	}
	public String getAVSCode() {
		return avsCode;
	}
	/* (non-Javadoc)
	 * @see emgshared.services.CreditCardServiceResponse#setAVSCode(java.lang.String)
	 */
	public void setAVSCode(String avsCode) {
		this.avsCode = avsCode;
	}

	public String getProcessorTraceNbr() {
		return processorTraceNbr;
	}

	public void setProcessorTraceNbr(String processorTraceNbr) {
		this.processorTraceNbr = processorTraceNbr;
	}

	public String getProcessorFormattedRespText() {
		return processorFormattedRespText;
	}

	public void setProcessorFormattedRespText(String processorFormattedRespText) {
		this.processorFormattedRespText = processorFormattedRespText;
	}

	public long getEffortId() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setEffortId(long effortId) {
		// TODO Auto-generated method stub

	}
}
