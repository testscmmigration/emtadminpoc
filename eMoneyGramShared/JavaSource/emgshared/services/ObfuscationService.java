/*
 * Created on Jan 14, 2005
 *
 */
package emgshared.services;

/**
 * @author T349
 *
 */
public interface ObfuscationService
{
	public String getCreditCardObfuscation(String creditCard);
	public String getBankAccountObfuscation(String bankAccount);
	public String getBinObfuscation(String bin);
	public String getPasswordObfuscation(String password);
	public String getBillerAccountObfuscation(String account);
	public String getCreditCardHash(String creditCard);
	public String getBankAccountHash(String bankAccount);
	public String getBinHash(String bin);
	public String getAdditionalIdObfuscation(String additionalId);

}
