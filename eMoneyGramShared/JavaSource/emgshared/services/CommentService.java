/*
 * Created on Apr 15, 2005
 *
 */
package emgshared.services;

import emgshared.model.ConsumerProfile;
import emgshared.model.MicroDeposit;

/**
 * @author A131
 *
 */
public interface CommentService
{
	/**
	 * Given a CreditCardServiceResponse, inserts an account comment
	 * if accountId is not null, inserts a transaction comment if
	 * tranId is not null.
	 *
	 * @param response	- if null, method does nothing.
	 * @param accountId	- if not null, method inserts account comment
	 * @param tranId	- if not null, method
	 * @param resourceFile	- if null, default comment is used.
	 * @param callerLoginId
	 *
	 * Created on Apr 15, 2005
	 */
	public void insertCreditCardResponseComment(
			CreditCardServiceResponse response,
			Integer accountId,
			Integer tranId,
			String resourceFile,
			String callerLoginId);

	public void insertGCCardResponseComment(
			CreditCardServiceResponse response,
			Integer accountId,
			Integer tranId,
			String resourceFile,
			String callerLoginId);

	public void addBlockedProfileComment(
		ConsumerProfile consumerProfile,
		String commentType);

	public void addMicroDepositFailComment(
		ConsumerProfile consumerProfile,
		MicroDeposit md,
		String commentType);

	public void addVeridInitiateTransactionProfileComment(
		ConsumerProfile consumerProfile,
		String commentType,
		String reasonText);

	public void addVeridContinueTransactionProfileComment(
		ConsumerProfile consumerProfile,
		String commentType,
		String reasonText);

	public void addVeridErrorFailedTransactionProfileComment(
		ConsumerProfile consumerProfile,
		String commentType,
		String reasonText);

	public void insertAVSCreditCardResponseComment(String avsCode,
			Integer accountId, Integer tranId, String resourceFile,
			String callerLoginId, String accountNum);

	public void insertBankPaymentServiceResponseComment(
			String msgType, Integer tranId, boolean successful, String callerLoginId,
			String respCode, String approvalCode, String denialRecNbr, String achStatus,
			String providerTraceId, String clientTraceId);

	public void insertTransactionComment(Integer tranId, String callerLoginId, String commentText);

	public void insertTransactionComment(Integer tranId, String callerLoginId, String commentText, String reasonCode);
}
