package emgshared.property;


public class EMTSharedEnvProperties {
	
	private EMTSharedEnvProperties(){}
	
	public static String getEnvironment() {
    	return System.getProperty("mgi.env");
    }
}
