package emgshared.property;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.moneygram.common.log.Logger;

public class PropertyLoader
{
	private Logger logr = null;
	private List vErrors = null;
	private Properties props = null;
	private String cTag = "";
	private PropertyLoader()
	{

	}
	/**
	 * This interface is for using accessing properties loaded from a traditional properties file.
	 * 
	 * @param propertyFile - classpath accessible properties file name
	 * @param contextTag   - optional string to prepend to System.out and log lines
	 * @param contextLogger -client's Logger, for validation error dumps.
	 * @throws IllegalArgumentException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static PropertyLoader getInstance(
		String propertyFile,
		String contextTag,
		Logger contextLogger,
		ClassLoader contextClassLoader)
	{
		PropertyLoader pl = new PropertyLoader();
		if (contextLogger == null)
		{
			System.out.println(
				(new Date()).toString()
					+ " ERROR: PropertyLoader parameter contextLogger is null");
			return null;
		}
		if (propertyFile == null || propertyFile.length() == 0)
		{
			String message =
				" ERROR: PropertyLoader parameter propertyFile is null or empty";
			System.out.println((new Date()).toString() + message);
			contextLogger.error(message);
			return null;
		}
		if (contextClassLoader == null)
		{
			String message =
				" ERROR: PropertyLoader parameter contextClassLoader is null";
			System.out.println((new Date()).toString() + message);
			contextLogger.error(message);
		}
		pl.logr = contextLogger;
		if (contextTag != null)
			pl.cTag = contextTag;

		InputStream is = contextClassLoader.getResourceAsStream(propertyFile);

		if (is == null)
		{
			String message =
				" ERROR: Property Loader. File "
					+ propertyFile
					+ " not Found in classpath ";
			System.out.println((new Date()).toString() + message);
			contextLogger.error(message);
			return null;
		}

		pl.props = new Properties();
		try
		{
			pl.props.load(is);
		} catch (IOException ioe)
		{
			String message =
				" ERROR: Property Loader IOException on load file:"
					+ propertyFile;
			System.out.println((new Date()).toString() + message);
			contextLogger.error(message, ioe);
			return null;
		}
		pl.vErrors = new ArrayList();

		return pl;
	}

	/**
	 * This interface is for using accessing properties loaded in the container.
	 * 
	 * @param propertyFile - classpath accessible properties file name
	 * @param contextTag   - optional string to prepend to System.out and log lines
	 * @param contextLogger -client's Logger, for validation error dumps.
	 * @throws IllegalArgumentException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static PropertyLoader getInstance(
			String resourceUri,
			String contextTag,
			Logger contextLogger)
		{
			PropertyLoader pl = new PropertyLoader();
			if (contextLogger == null)
			{
				System.out.println((new Date()).toString()
									+ " ERROR: PropertyLoader parameter contextLogger is null");
				return null;
			}
			pl.logr = contextLogger;
			if (contextTag != null)
				pl.cTag = contextTag;

			pl.props = new ContainerProperties(resourceUri);
			
			pl.vErrors = new ArrayList();

			return pl;
		}

	
	/**
	 * Checks for null string. Nothing more, same as original.
	 * @param propertyKey
	 * @return
	 */
	public String getString(String propertyKey)
	{
		String ret = props.getProperty(propertyKey);
		if (ret != null)
		{
			return props.getProperty(propertyKey).trim();
		}

		vErrors.add("property  " + propertyKey + " is null");
		return null;
	}

	public int getInteger(String propertyKey)
	{
		String pstr = getString(propertyKey);
		if (pstr != null)
		{
			try
			{
				return Integer.parseInt(pstr.trim());
			} catch (NumberFormatException nfe)
			{
				vErrors.add(
					"property " + propertyKey + " not a parsable integer");
			}
		}
		return -1;
	}
	public boolean getBoolean(String propertyKey)
	{
		String pstr = getString(propertyKey);
		if (pstr == null)
			return false;
		if (pstr.length() == 0)
		{
			vErrors.add(
				" boolean property " + propertyKey + " is empty string.");
			return false;
		}
		if (!(pstr.equals("true") || pstr.equals("false")))
		{
			vErrors.add(
				" boolean property " + propertyKey + " not 'true' or 'false'.");
			return false;
		}
		return Boolean.valueOf(pstr).booleanValue();
	}

	public double getDouble(String propertyKey)
	{
		String pstr = getString(propertyKey);
		if (pstr == null)
			return 0.0;
		if (pstr.length() == 0)
		{
			vErrors.add("double property " + propertyKey + " is empty string");
			return 0.0;
		}
		try
		{
			return Double.parseDouble(pstr);
		} catch (NumberFormatException nfe)
		{
			vErrors.add(
				"double property " + propertyKey + " is unparsable:" + pstr);
		}
		return 0.0;
	}

	public List getValidiationErrorListOfStrings()
	{
		return vErrors;
	}
	public boolean isValidated()
	{
		return (vErrors.size() == 0);
	}
	public Properties getRawProperties()
	{
		return props;
	}

	public void dumpErrors(boolean echoToSystemOut)
	{
		String header =
			(new Date())
				+ "  "
				+ cTag
				+ " Property Validation Errors \n"
				+ "===========================================================";
		if (echoToSystemOut)
			System.out.println(header);
		logr.error(header);

		Iterator eI = vErrors.iterator();
		while (eI.hasNext())
		{
			String oneComplaint = (String) eI.next();
			if (echoToSystemOut)
				System.out.println(oneComplaint);
			logr.error(oneComplaint);
		}
		if (echoToSystemOut)
			System.out.println("=======================================");
		logr.error("===========================================");
	}
}
