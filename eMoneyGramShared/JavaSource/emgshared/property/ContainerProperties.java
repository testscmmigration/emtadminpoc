package emgshared.property;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.moneygram.common.log.Logger;
import com.moneygram.ree.lib.Config;

import emgshared.dataaccessors.EMGSharedLogger;

public class ContainerProperties extends Properties {
	Logger logger = EMGSharedLogger.getLogger(this.getClass());
	private InitialContext ic;
	private Config config;
	
	public ContainerProperties(String resourceUri) {
		try {
			ic = new InitialContext();
			config = (Config)ic.lookup(resourceUri);
		} catch (NamingException e) {
			String errorMsg = "Could not connect to container defined properties with URI: " + resourceUri;
			logger.error(errorMsg, e);
			throw new RuntimeException(errorMsg, e);
		}		
	}
	
	public String getProperty(String key){ 
		return (String)config.getAttribute(key);
	}

	public void list(PrintStream out) {
		throw new RuntimeException("Unsupported method");
	}

	public void list(PrintWriter out) {
		throw new RuntimeException("Unsupported method");
	}

	public synchronized void load(InputStream inStream) throws IOException {
		throw new RuntimeException("Unsupported method");
	}

	public Enumeration propertyNames() {
		throw new RuntimeException("Unsupported method");
	}

	public synchronized void save(OutputStream out, String header) {
		throw new RuntimeException("Unsupported method");
	}

	public synchronized Object setProperty(String key, String value) {
		throw new RuntimeException("Unsupported method");
	}

	public synchronized void store(OutputStream out, String header) throws IOException {
		throw new RuntimeException("Unsupported method");
	}
}
