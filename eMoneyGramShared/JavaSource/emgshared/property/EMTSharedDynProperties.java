package emgshared.property;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.dataaccessors.OracleAccess;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;

/**
 *
 * @author GRS
 * This class can be instantiated and kept in session.
 * Most Properties are read on instantiation and remain immutable
 * for life of object ( session ). Some select properties will
 * be read from the database on every reference. file will provide only purely dynamic properties
 *
 * The only non-static aspect to this class is in the "cached" imMap properties, which
 *
 * alpha version intention was to instantiate ME once per web app session which
 * would read a consistant set of properties that wouldn't change for that session.
 * This would reduce the danger of changing properties leading to errors
 * ( since the project has been coded with the assumption of constant properties )
 * Due to the unforseen ( but not unforseeable :) problem of getting database
 * pooling properties from the database ( before the pooler is available, ) a
 * bootstrap connection had to be devised.
 *
 * Pending review of state consistancy, this dynamic properties object is not
 * very dynamic. The bootstrap loaded properties are used by all subsequent
 * constructors. Oh well.
 *
 *
 *
 * TODO - check for state consistancy so that the modification of values won't introduce run-time bugs.
 *
 */

public class EMTSharedDynProperties
{
	private static Logger logr =
		LogFactory.getInstance().getLogger(EMTSharedDynProperties.class);

	private static final String userID = "emgwww";
	public static final String AC4RETRYCOUNT = "AC4RetryCount";
    public static final String AC4RETRYWAITTIME = "AC4RetryWaitTime";
    public static final String AC4TIMEOUT = "AC4Timeout";
    public static final String ALLOWTRANSACTIONCANCEL = "AllowTransactionCancel";
    public static final String AUTOMATIONPERIODINHRS = "AutomationPeriodInHrs";
    public static final String BGPROCESSAUTOSTARTUP = "BGProcessAutoStartup";
    public static final String BGPROCESSMASTERTHROTTLE = "BGProcessMasterThrottle";
    public static final String BGPROCESSMASTERPRIORITY = "BGProcessMasterPriority";
    public static final String CCAVSTRANS = "CcAVSTrans";
    public static final String CCAVSINIT = "CcAVSInit";
    public static final String DISPLAYACTIONTAGS = "DisplayActionTags";
    public static final String DISPLAYMONEYGRAMDETAILS = "DisplayMoneyGramDetails";
    public static final String ESCLEARTIME = "EsClearTime";
    public static final String ESSENDCUTOFFTIME = "EsSendCutoffTime";
    public static final String ESSENDWAITDAYS = "EsSendWaitDays";
    public static final String ECONOMYCALIFORNIASENDER = "EconomyCaliforniaSender";
    public static final String EMAILVALIDATION = "EmailValidation";
    public static final String IPLOOKUP_MD = "IPLookup_MD";
    public static final String LOGINATTEMPTSTHRESHOLD = "LoginAttemptsThreshold";
    public static final String MAXCONSUMERTRANSSHOWN = "MaxConsumerTransShown";
    public static final String MAXDOWNLOADBILLERS = "MaxDownloadBillers";
    public static final String MAXDOWNLOADTRANSACTIONS = "MaxDownloadTransactions";
    public static final String MAXNUMBEROFCREDITCARDS = "MaxNumberOfCreditCards";
    public static final String MAXROWSHASHCRYPTO = "MaxRowsHashCrypto";
    public static final String MAXIMUMNUMBERAUTOMATEDPERPERIOD = "MaximumNumberAutomatedPerPeriod";
    public static final String MDACCOUNTLIMIT = "MDAccountLimit";
    public static final String MDACCOUNTRETRYTHRESHOLD = "MDAccountRetryThreshold";
    public static final String MDDAYSBEFOREEXPIRES = "MDDaysBeforeExpires";
    public static final String MDDAYSBEFOREUSERCONFIRMALLOWED = "MDDaysBeforeUserConfirmAllowed";
    public static final String MDMAXACCOUNTSINPROCESS = "MDMaxAccountsInProcess";
    public static final String MDMAXDEPOSITAMOUNT = "MDMaxDepositAmount";
    public static final String MDRETRYTHRESHOLD = "MDRetryThreshold";
    public static final String MDTIMEBEFOREUSERCONFIRMALLOWED = "MDTimeBeforeUserConfirmAllowed";
    public static final String MGACHLIMIT = "MGACHLimit";
    public static final String MGCCLIMIT_GB = "MGCCLimit_GB";
    public static final String MGCCLIMIT_DE = "MGCCLimit_DE";
    public static final String MGCCLIMIT = "MGCCLimit";
    public static final String MGWDSLIMIT = "MGWDSLimit";
    public static final String MONTHLYPROFILELIMIT = "MonthlyProfileLimit";
    public static final String NEWCREDITCARDAUTHAMOUNT = "NewCreditCardAuthAmount";
    public static final String PRMRDAYSCHECKHIGH = "PrmrDaysCheckHigh";
    public static final String PRMRDAYSCHECKLOW = "PrmrDaysCheckLow";
    public static final String PRMREPALLOWED = "PrmrEPAllowed";
    public static final String PRMRESALLOWED = "PrmrESAllowed";
    public static final String PRMRMTALLOWED = "PrmrMTAllowed";
    public static final String PRMRDSALLOWED = "PrmrDSAllowed";
    public static final String SCHEDULEMAINTENANCE = "ScheduleMaintenance";
    public static final String SPECIALRESTRICTION_ECONOMYSERVICE = "SpecialRestriction_EconomyService";
    public static final String SPECIALRESTRICTION_SAMEDAYSERVICE = "SpecialRestriction_SameDayService";
    public static final String SQLDEBUGLEVEL = "SqlDebugLevel";
    public static final String STANDARDNSFFEE = "StandardNsfFee";
    public static final String DSSEND_DELAY = "DsSendDelay";
    public static final String DS_START_ACH_WINDOW = "DsStartACHWindow";
    public static final String DS_END_ACH_WINDOW = "DsEndACHWindow";
    // private static long refreshDelay = 1*60;  //  for testing
    private static long refreshDelay = 15*60*1000;  // 15 minutes
	private static long lastCacheUpdate = System.currentTimeMillis();
	private static Map staticMap = null;
	private static Map imMap = null;


	/**
     * BootstrapProperties method call only at application startup time, NEVER
     * AGAIN. EMTSharedEnvProperties MUST be loaded and validated. no argument
     * checking,
     *
     * @return List Container of validation errors, or null if SQL Exception
     *         caught.
     */

	public static List bootstrapLoadAndValidate(Logger contextLogger)
	{
		Connection ork = null;
		try
		{
			ork = OracleAccess.getConnection();
		} catch (DataSourceException e) {
			System.out.println("Dynamic Properties Bootstrap DataSourceException");
			e.printStackTrace();
			contextLogger.error("FAILED: Dynamic Properties Bootstrap DataSourceException", e);
			return null;
		}
		staticMap =
			PropertyDAO.bootstrapPropertyGetter(ork, userID, contextLogger);
		if (staticMap == null)
			return null;
		imMap = staticMap;
		return validateStaticMap();
	}


	public static void checkForRefreshDynProperties(String userId) {
		long currentTime = System.currentTimeMillis();
		if ( ((currentTime - lastCacheUpdate) > refreshDelay) )
		{
			try {
				refreshDynProperties("userId");
				lastCacheUpdate=currentTime;
			} catch (Exception ignore) {
				ignore.printStackTrace();
			}
		}
	}

	public static void refreshDynProperties(String userId)
	{
		PropertyDAO dao = new PropertyDAO();
		staticMap = (Map) dao.getPropertyObject(null, userId, true);
		List vErrs = validateStaticMap();
		if (vErrs == null || vErrs.size() > 0)
			// TODO - find a better exception.
			throw new AssertionError("refresh validation failed.");
		synchronized (imMap) {
			imMap = staticMap;
		}
	}

	/***************************** OBJECT ******************************/


	public EMTSharedDynProperties() {}

	public int getAutomationPeriodInHrs()
	{
		return pInt(EMTSharedDynProperties.AUTOMATIONPERIODINHRS);
	}

	public int getMaximumNumberAutomatedPerPeriod()
	{
		return pInt(EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD);
	}

	public int getAC4RetryCount()
	{
		return pInt(EMTSharedDynProperties.AC4RETRYCOUNT);
	}

	public int getAC4RetryWaitTime()
	{
		return pInt(EMTSharedDynProperties.AC4RETRYWAITTIME);
	}

	public int getAC4Timeout()
	{
		return pInt(EMTSharedDynProperties.AC4TIMEOUT);
	}

	public boolean isAllowTransactionCancel()
	{
		return pBoolean(EMTSharedDynProperties.ALLOWTRANSACTIONCANCEL);
	}

	public boolean isBGProcessAutoStartup()
	{
		return pBoolean(EMTSharedDynProperties.BGPROCESSAUTOSTARTUP);
	}

	public int getBGProcessMasterThrottle()
	{
		return pInt(EMTSharedDynProperties.BGPROCESSMASTERTHROTTLE);
	}

	public int getBGProcessMasterPriority()
	{
		return pInt(EMTSharedDynProperties.BGPROCESSMASTERPRIORITY);
	}

	public boolean isDisplayActionTags()
	{
		return pBoolean(EMTSharedDynProperties.DISPLAYACTIONTAGS);
	}

	public boolean isDisplayMoneyGramDetails()
	{
		return pBoolean(EMTSharedDynProperties.DISPLAYMONEYGRAMDETAILS);
	}

	public int getEsClearTime()
	{
		return pInt(EMTSharedDynProperties.ESCLEARTIME);
	}

	public int getEsSendCutoffTime()
	{
		return pInt(EMTSharedDynProperties.ESSENDCUTOFFTIME);
	}

	public int getEsSendWaitDays()
	{
		return pInt(EMTSharedDynProperties.ESSENDWAITDAYS);
	}

	public boolean isEconomyCaliforniaSender()
	{
		return pBoolean(EMTSharedDynProperties.ECONOMYCALIFORNIASENDER);
	}

	/* replaces getValidateEmail */
	public boolean isEmailValidation()
	{
		return pBoolean(EMTSharedDynProperties.EMAILVALIDATION);
	}

	public String getIPLookup_MD() {
		return pString(EMTSharedDynProperties.IPLOOKUP_MD);
	}

	public int getLoginAttemptsThreshold()
	{
		return pInt(EMTSharedDynProperties.LOGINATTEMPTSTHRESHOLD);
	}

	/**
	 * Maximum amount for one Bank ( ACH ) transaction.
	 * @return
	 */
	public float getMgACHLimit()
	{
		return pFloat(EMTSharedDynProperties.MGACHLIMIT);
	}

	/**
	 * Maximum amount for one transaction in GB.
	 * @return
	 */
	public float getMGCCLimit_GB() {
		return pFloat(EMTSharedDynProperties.MGCCLIMIT_GB);
	}

	/**
	 * Maximum amount for one transaction in DE
	 * @return
	 */
	public float getMGCCLimit_DE() {
		return pFloat(EMTSharedDynProperties.MGCCLIMIT_DE);
	}

	/**
	 * Maximum amount for one Credit Card transaction
	 * @return
	 */
	public float getMgCCLimit()
	{
		return pFloat(EMTSharedDynProperties.MGCCLIMIT);
	}

	public float getMgWDSLimit() {
		return pFloat(EMTSharedDynProperties.MGWDSLIMIT);
	}

	/**
	 * Maximum amount a consumer can send in a 30 day period.
	 * @return
	 */
	public float getMonthlyProfileLimit()
	{
		return pFloat(EMTSharedDynProperties.MONTHLYPROFILELIMIT);
	}

	/**
	 * To turn on/off CyberSource Address Verification Service for
	 * transaction auth
	 *
	 * @return
	 */
	public boolean isCcAVSTrans()
	{
		return pBoolean(EMTSharedDynProperties.CCAVSTRANS);
	}

	/**
	 * To turn on/off CyberSource Address Verification Service for
	 * consumer initial $1 auth
	 *
	 * @return
	 */
	public boolean isCcAVSInit()
	{
		return pBoolean(EMTSharedDynProperties.CCAVSINIT);
	}

	public int getMaxDownloadTransactions()
	{
		return pInt(EMTSharedDynProperties.MAXDOWNLOADTRANSACTIONS);
	}

	public int getMaxDownloadBillers()
	{
		return pInt(EMTSharedDynProperties.MAXDOWNLOADBILLERS);
	}

	public int getMaxConsumerTransShown()
	{
		return pInt(EMTSharedDynProperties.MAXCONSUMERTRANSSHOWN);
	}

	public int getMaxRowsHashCrypto()
	{
		return pInt(EMTSharedDynProperties.MAXROWSHASHCRYPTO);
	}

	public float getNewCreditCardAuthAmount()
	{
		return pFloat(EMTSharedDynProperties.NEWCREDITCARDAUTHAMOUNT);
	}

	public int getMaxNumberOfCreditCards()
	{
		return pInt(EMTSharedDynProperties.MAXNUMBEROFCREDITCARDS);
	}

	public boolean isScheduleMaintenance()
	{
		return pBoolean(EMTSharedDynProperties.SCHEDULEMAINTENANCE);
	}

	public String getSpecialRestriction_EconomyService()
	{
		return pString(EMTSharedDynProperties.SPECIALRESTRICTION_ECONOMYSERVICE);
	}

	public String getSpecialRestriction_SameDayService()
	{
		return pString(EMTSharedDynProperties.SPECIALRESTRICTION_SAMEDAYSERVICE);
	}

	public int getSqlDebugLevel()
	{
		return pInt(EMTSharedDynProperties.SQLDEBUGLEVEL);
	}

	public float getStandardNsfFee()
	{
		return pFloat(EMTSharedDynProperties.STANDARDNSFFEE);
	}

	public String getDsStartAchWindow() {
		return pString(EMTSharedDynProperties.DS_START_ACH_WINDOW);
	}

	public String getDsEndAchWindow() {
		return pString(EMTSharedDynProperties.DS_END_ACH_WINDOW);
	}

	public int getDsSendDelay() {
		return pInt(EMTSharedDynProperties.DSSEND_DELAY);
	}

	public String[] getVBVConsumerIdList()
	{
		return (String[]) pArray("VBVConsumerIdList");
	}

	// Micro Deposit Parameters
	public int getMDMaxDepositAmount()
	{
		return pInt(EMTSharedDynProperties.MDMAXDEPOSITAMOUNT);
	}
	public int getMDRetryThreshold()
	{
		return pInt(EMTSharedDynProperties.MDRETRYTHRESHOLD);
	}
	public int getMDAccountLimit()
	{
		return pInt(EMTSharedDynProperties.MDACCOUNTLIMIT);
	}
	public int getMDDaysBeforeExpires()
	{
		return pInt(EMTSharedDynProperties.MDDAYSBEFOREEXPIRES);
	}
	public int getMDMaxAccountsInProcess()
	{
		return pInt(EMTSharedDynProperties.MDMAXACCOUNTSINPROCESS);
	}
	public int getMDDaysBeforeUserConfirmAllowed()
	{
		return pInt(EMTSharedDynProperties.MDDAYSBEFOREUSERCONFIRMALLOWED);
	}
	public String getMDTimeBeforeUserConfirmAllowed()
	{
		return pString(EMTSharedDynProperties.MDTIMEBEFOREUSERCONFIRMALLOWED);
	}
	public int getMDAccountRetryThreshold()
	{
		return pInt(EMTSharedDynProperties.MDACCOUNTRETRYTHRESHOLD);
	}

	// Premier Customer recent transactions check days properties.
	public int getPrmrDaysCheckHigh()
	{
	    return pInt(EMTSharedDynProperties.PRMRDAYSCHECKHIGH);
	}

	public int getPrmrDaysCheckLow()
	{
	    return pInt(EMTSharedDynProperties.PRMRDAYSCHECKLOW);
	}

	public boolean isPrmrEPAllowed()
	{
		return pBoolean(EMTSharedDynProperties.PRMREPALLOWED);
	}

	public boolean isPrmrESAllowed()
	{
		return pBoolean(EMTSharedDynProperties.PRMRESALLOWED);
	}

	public boolean isPrmrMTAllowed()
	{
		return pBoolean(EMTSharedDynProperties.PRMRMTALLOWED);
	}

	public boolean isPrmrDSAllowed()
	{
		return pBoolean(EMTSharedDynProperties.PRMRDSALLOWED);
	}

	// Internal casting getters for required properties.
	/*****************************************************/
	private int pInt(String name)
	{
		return ((Integer) getCachedObject(name)).intValue();
	}

	private float pFloat(String name)
	{
		return ((Float) getCachedObject(name)).floatValue();
	}

	private boolean pBoolean(String name)
	{
		return ((Boolean) getCachedObject(name)).booleanValue();
	}

	private String pString(String name)
	{
		return (String) getCachedObject(name);
	}

	private Object pArray(String name)
	{
		return getCachedObject(name);
	}

	/**************************************************/

	/**
	 * @param name
	 * @return - property object typed to table definition.
	 */
	private Object getCachedObject(String name)
		throws EMGRuntimeException, IllegalArgumentException
	{
		if (name == null) {
			throw new IllegalArgumentException("name parameter is null");
		}
		if (imMap == null) {
			bootstrapLoadAndValidate(logr);
		}
		return imMap.get(name);
	}

	/**
	 * Simple validation for now: is property null?
	 * @param dp
	 * @return List of validation errors.
	 */
	private static List validateStaticMap()
	{
		List valErrRet = new ArrayList();

		String[] expectedPropertyNames =
			{
		        EMTSharedDynProperties.AC4RETRYCOUNT,
		        EMTSharedDynProperties.AC4RETRYWAITTIME,
		        EMTSharedDynProperties.AC4TIMEOUT,
		        EMTSharedDynProperties.ALLOWTRANSACTIONCANCEL,
		        EMTSharedDynProperties.BGPROCESSAUTOSTARTUP,
		        EMTSharedDynProperties.BGPROCESSMASTERTHROTTLE,
		        EMTSharedDynProperties.BGPROCESSMASTERPRIORITY,
		        EMTSharedDynProperties.CCAVSTRANS,
		        EMTSharedDynProperties.CCAVSINIT,
		        EMTSharedDynProperties.DISPLAYACTIONTAGS,
		        EMTSharedDynProperties.DISPLAYMONEYGRAMDETAILS,
		        EMTSharedDynProperties.ESCLEARTIME,
		        EMTSharedDynProperties.ESSENDCUTOFFTIME,
		        EMTSharedDynProperties.ESSENDWAITDAYS,
		        EMTSharedDynProperties.ECONOMYCALIFORNIASENDER,
		        EMTSharedDynProperties.EMAILVALIDATION,
		        EMTSharedDynProperties.IPLOOKUP_MD,
		        EMTSharedDynProperties.LOGINATTEMPTSTHRESHOLD,
		        EMTSharedDynProperties.MAXCONSUMERTRANSSHOWN,
		        EMTSharedDynProperties.MAXDOWNLOADBILLERS,
		        EMTSharedDynProperties.MAXDOWNLOADTRANSACTIONS,
		        EMTSharedDynProperties.MAXNUMBEROFCREDITCARDS,
		        EMTSharedDynProperties.MAXROWSHASHCRYPTO,
		        EMTSharedDynProperties.MDACCOUNTLIMIT,
		        EMTSharedDynProperties.MDACCOUNTRETRYTHRESHOLD,
		        EMTSharedDynProperties.MDDAYSBEFOREEXPIRES,
		        EMTSharedDynProperties.MDDAYSBEFOREUSERCONFIRMALLOWED,
		        EMTSharedDynProperties.MDMAXACCOUNTSINPROCESS,
		        EMTSharedDynProperties.MDMAXDEPOSITAMOUNT,
		        EMTSharedDynProperties.MDRETRYTHRESHOLD,
		        EMTSharedDynProperties.MDTIMEBEFOREUSERCONFIRMALLOWED,
		        EMTSharedDynProperties.MGACHLIMIT,
		        EMTSharedDynProperties.MGCCLIMIT_GB,
		        EMTSharedDynProperties.MGCCLIMIT_DE,
		        EMTSharedDynProperties.MGCCLIMIT,
		        EMTSharedDynProperties.MGWDSLIMIT,
		        EMTSharedDynProperties.MONTHLYPROFILELIMIT,
		        EMTSharedDynProperties.NEWCREDITCARDAUTHAMOUNT,
		        EMTSharedDynProperties.PRMRDAYSCHECKHIGH,
		        EMTSharedDynProperties.PRMRDAYSCHECKLOW,
		        EMTSharedDynProperties.PRMREPALLOWED,
		        EMTSharedDynProperties.PRMRESALLOWED,
		        EMTSharedDynProperties.PRMRMTALLOWED,
		        EMTSharedDynProperties.PRMRDSALLOWED,
		        EMTSharedDynProperties.SCHEDULEMAINTENANCE,
		        EMTSharedDynProperties.SPECIALRESTRICTION_ECONOMYSERVICE,
		        EMTSharedDynProperties.SPECIALRESTRICTION_SAMEDAYSERVICE,
		        EMTSharedDynProperties.SQLDEBUGLEVEL,
		        EMTSharedDynProperties.STANDARDNSFFEE,
		        EMTSharedDynProperties.AUTOMATIONPERIODINHRS,
		        EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD,
		        EMTSharedDynProperties.DSSEND_DELAY,
		        EMTSharedDynProperties.DS_START_ACH_WINDOW,
		        EMTSharedDynProperties.DS_END_ACH_WINDOW
		        };

		for (int i = 0; i < expectedPropertyNames.length; i++) {
			String n = expectedPropertyNames[i];
			Object o = staticMap.get(n);
//			System.out.println("Dynamic Property:" + n + " value:" + String.valueOf(o));
			if (o == null) {
				valErrRet.add("missing database dynamic property " + n);
			}
		}
		return valErrRet;
	}
}
