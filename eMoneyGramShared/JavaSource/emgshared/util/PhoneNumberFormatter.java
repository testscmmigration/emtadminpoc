/*
 * Created on Jan 24, 2005
 *
 */
package emgshared.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author A131
 *
 */
public class PhoneNumberFormatter
{
	public static final String TWELVE_DIGIT_PHONE_FORMAT = "XX XXX XXX XXXX";
	public static final String TEN_DIGIT_PHONE_FORMAT = "(XXX) XXX-XXXX";
	public static final String SEVEN_DIGIT_PHONE_FORMAT = "XXX-XXXX";

	public static final String HOME_PHONE_MARKER = "{H}";
	public static final String MOBILE_PHONE_MARKER = "{M}";

	public static String format(String phoneNumber) {
		if (phoneNumber != null) {
			switch(phoneNumber.length()) {
			case 10:
				return format(phoneNumber, TEN_DIGIT_PHONE_FORMAT);
			case 7:
				return format(phoneNumber, SEVEN_DIGIT_PHONE_FORMAT);
			case 12:
				return format(phoneNumber, TWELVE_DIGIT_PHONE_FORMAT);
			}
		}
		return phoneNumber;
	}

	/**
	 * This function gives a flexible way to format phone numbers.
	 * It uses a "pure" phone number (i.e. with only digits) and
	 * a string format in which the digits are represented with an x
	 * or an X. The function returns the format string replacing each
	 * x character with its corresponding digit from the phone number.
	 * For example given "1234567890" and "(XXX) XXX-XXXX" it will return
	 * "(123) 456-7890". To work correctly the number of digits in the phoneNumber
	 * must match the exact number of x's in the format. If that isn't true in the best
	 * case the resulting formatted phoneNumber will look odd in the worst case it will
	 * throw an exception (possibly an arrayIndexOutOfBounds exception) Use in conjuction
	 * with pre established formats as public static variables.
	 * @param phoneNumber a string containing only digits.
	 * @param format the format to use.
	 * @return a formatted phone number as explained above.
	 * @author cobregon
	 * @version 1.0 01/Jun/2011
	 */
	public static String format(String phoneNumber, String format) {
		StringBuffer buffer = new StringBuffer();
		int phoneIndex = 0;
		final int SIZE = format.length();
		for (int i = 0; i < SIZE; ++i) {
			char digit = format.charAt(i); 
			if (digit == 'X' || digit == 'x') {
				buffer.append(phoneNumber.charAt(phoneIndex++));
			} else {
				buffer.append(digit);
			}
		}
		return buffer.toString();
	}

}
