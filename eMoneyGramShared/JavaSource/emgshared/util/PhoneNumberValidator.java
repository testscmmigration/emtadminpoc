/*
 * Created on Jan 4, 2005
 *
 */
package emgshared.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author A131
 *
 */
public class PhoneNumberValidator
{
	private String phoneNumber;
	boolean phoneNumberBlank;
	boolean phoneNumberInvalid;

	public PhoneNumberValidator(String phoneNumber)
	{
		this.phoneNumber = StringUtils.trimToNull(phoneNumber);
		validate();
	}

	public boolean isValid()
	{
		return this.phoneNumberInvalid == false;
	}

	public boolean isPhoneNumberBlank()
	{
		return this.phoneNumberBlank;
	}

	public boolean isPhoneNumberInvalid()
	{
		return this.phoneNumberInvalid;
	}

	private final void validate()
	{
		this.phoneNumberBlank = (this.phoneNumber == null);
		this.phoneNumberInvalid =
			this.phoneNumberBlank || meetsRequirements(this.phoneNumber) == false;
	}

	private final boolean meetsRequirements(String s)
	{
		if (s == null)
			return false;

		StringBuffer unformattedPhoneNumber = new StringBuffer();
		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			if (Character.isDigit(c)) {
				unformattedPhoneNumber.append(c);
			}
		}

		if (unformattedPhoneNumber.length() != 10)
			return false;

		return true;
	}
}
