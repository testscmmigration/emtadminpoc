/*
 * Created on Jun 29, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgshared.util;


/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PhoneNumberHelper {
	public PhoneNumberHelper() 
	{
	}

	/*
	 * 	substract area code from phone number
	 */
	public static String areaCode(String phoneNumber)
	{
		String areaCode = "";

		if (phoneNumber != null)
		{
			int len = phoneNumber.length();
			if (len >= 10)
			{
				areaCode = phoneNumber.substring(len - 10, len - 10 + 3);
			}
		}
		return areaCode;
	}
	
	/*
	 * 	substract prefix from phone number
	 */
	public static String prefix(String phoneNumber)
	{
		String prefix = "";
		
		if (phoneNumber != null)
		{
			int len = phoneNumber.length();
			if (len >= 7)
			{
				prefix = phoneNumber.substring(len - 7, len - 7 + 3);
			}
		}
		return prefix;
	}
	
}
