package emgshared.util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.moneygram.ree.lib.Config;

public class ResourceConfigFactory 
{
    public Config createResourceConfiguration(String jndi) throws Exception 
    {
        Config config;
        try 
        {
            Context ctx = new InitialContext();
            config = (Config) ctx.lookup(jndi);
        } 
        catch(Exception e) 
        {
            throw e;
        }
        
        if(config == null) 
        {
            throw new IllegalArgumentException("Failed to initialize configuration values using jndi name: " + jndi +". Configuration can not be null.");
        }
        
        return config;
    }
}
