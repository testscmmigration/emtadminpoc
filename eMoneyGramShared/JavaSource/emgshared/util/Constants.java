package emgshared.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {

	public static final String COMMENT_CODE_OTH="OTH";
	public static final String COMMENT_CODE_AVS="AVS";

	public static final String ENCRYPT_PASSWORD_FILLER="EMTSECRET";
	public static final String WAP_SITE_IDENTIFIER = "WAP";

	public static final int MGO_PARTNER_SITE_CODE = 1;
	public static final int WAP_PARTNER_SITE_CODE = 2;
	public static final int MGOUK_PARTNER_SITE_CODE = 3;
	public static final int MGODE_PARTNER_SITE_CODE = 4;

	public static final String MGO_PARTNER_SITE_ID = "MGO";
	public static final String MGOUK_PARTNER_SITE_ID = "MGOUK";
	public static final String MGODE_PARTNER_SITE_ID = "MGODE";
	public static final String WAP_PARTNER_SITE_ID = "WAP";
	public static final String ALL_PARTNER_SITE_ID = "ALL";
	public static final int PYMT_PROVIDER_CODE_GLOBAL_COLLECT = 2;

	public static final String APPROVED_DOC_STATUS = "APP";
	public static final String PENDING_DOC_STATUS = "PEN";
	public static final String DENIED_DOC_STATUS = "DEN";
	public static final String SUBMITTED_DOC_STATUS = "SUB";
	public static final String REQUESTED_DOC_STATUS = "REQ";
	
	
	public static Map<Integer, String> partnerSiteCodeToId;
	public static Map<String, Integer> partnerSiteIdToCode;
	
	public static final long PHL_SMARTMONEY_AGENTID = 65599187;

	// Should come from the DB
	static {
		Map<Integer, String> tempCodeToId = new HashMap<Integer, String>();
		tempCodeToId.put(MGO_PARTNER_SITE_CODE, MGO_PARTNER_SITE_ID);
		tempCodeToId.put(WAP_PARTNER_SITE_CODE, WAP_PARTNER_SITE_ID);
		tempCodeToId.put(MGOUK_PARTNER_SITE_CODE, MGOUK_PARTNER_SITE_ID);
		tempCodeToId.put(MGODE_PARTNER_SITE_CODE, MGODE_PARTNER_SITE_ID);
		partnerSiteCodeToId = Collections.unmodifiableMap(tempCodeToId);
		Map<String, Integer> tempIdToCode = new HashMap<String, Integer>();
		tempIdToCode.put(MGO_PARTNER_SITE_ID, MGO_PARTNER_SITE_CODE);
		tempIdToCode.put(WAP_PARTNER_SITE_ID, WAP_PARTNER_SITE_CODE);
		tempIdToCode.put(MGOUK_PARTNER_SITE_ID, MGOUK_PARTNER_SITE_CODE);
		tempIdToCode.put(MGODE_PARTNER_SITE_ID, MGODE_PARTNER_SITE_CODE);
		partnerSiteIdToCode = Collections.unmodifiableMap(tempIdToCode);
	}

	//SITES INDENTIFIERS
	public static final String SITE_IDENTIFIER = "MGO";
	public static final String SITE_IDENTIFIER_UK = "MGOUK";
	public static final String SITE_IDENTIFIER_DE = "MGODE";	
    public static final String SITE_IDENTIFIER_INLANE = "INLANE";
    public static final String SITE_IDENTIFIER_WAP = "WAP";

    public static final String COUNTRY_IDENTIFIER_DE = "GER";
    
    
    public static String MIME_XLS="application/vnd.ms-excel";
    public static String MIME_XLXS="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    
    public static final String ACCT_MASK="****";
    public static final String STR_EMPTY="";
}
