package emgshared.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.ParseDateException;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ServiceFactory;
import emgshared.services.TransactionService;

public class DateFormatter
{
	private SimpleDateFormat formatter;
	private static DateFormatter df = new DateFormatter("yyyyMMdd", true);

	public DateFormatter(String formatSpec, boolean beLenient)
	{
		formatter = new SimpleDateFormat(formatSpec);
		formatter.setLenient(beLenient);
	}

	public Date parse(String src) throws ParseDateException
	{
		try
		{
			src = StringHelper.trim(src);
			if (src.length() == 0)
			{
				return null;
			}

			synchronized (this)
			{
				return formatter.parse(src);
			}
		} catch (Exception x)
		{
			throw new ParseDateException("Malformed date: '" + src + "'");
		}
	}

	public String format(Date date)
	{
		if (date == null)
		{
			return null;
		}

		synchronized (this)
		{
			return formatter.format(date);
		}
	}

	public String format(Date date, TimeZone timeZone)
	{
		if (date == null)
		{
			return null;
		}

		synchronized (this)
		{
			TimeZone oldTimeZone = formatter.getTimeZone();
			if (!oldTimeZone.equals(timeZone)) {
				formatter.setTimeZone(timeZone);
				String format = formatter.format(date);
				formatter.setTimeZone(oldTimeZone);
				return format;
			}
			return formatter.format(date);
		}
	}
	
	/**
	 * 
	 * @param date			Effective Date 
	 * @param holidayMap    Holiday hash map from the system cache
	 * @param days	        Number of days to do MG Send to the customer
	 * @param backward      go backward business days if it is true, otherwise,
	 * 						go forward business days if it is false		
	 * @return				The ES MG send date for foward lookup or
	 * 						the transaction status date for backward loopup
	 */

	private static Date getAchDay(
		Date date,
		Map holidayMap,
		int days,
		boolean backward)
	{

		if (date == null)
		{
			throw new EMGRuntimeException("Input date parameter is a null value");
		}

		int val = 1; // going forward
		if (backward)
		{ // going backward
			val = -1;
		}

		DateFormatter df = new DateFormatter("HHmm", true);
		int passedTime = Integer.parseInt(df.format(date));
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		int clearTime = dynProps.getEsClearTime();
		int cutoffTime = dynProps.getEsSendCutoffTime();
		Map tmpMap = checkMap(holidayMap);

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		if (!backward)
		{
			if (passedTime > cutoffTime)
			{
				cal.add(Calendar.DATE, val); // after cutoff time
			}
			cal.add(Calendar.DATE, val); // Day 0
		}

		if (backward && passedTime < clearTime)
		{
			cal.add(Calendar.DATE, val); //  before clear time
		}

		for (int i = 1; i <= days; i++)
		{
			boolean didNotSkip = true;
			while (isNotBusinessDay(tmpMap, cal))
			{
				didNotSkip = false;
				cal.add(Calendar.DATE, val);
			}
			if (didNotSkip)
			{
				cal.add(Calendar.DATE, val);
				if (i == days)
				{
					while (isNotBusinessDay(tmpMap, cal))
					{
						cal.add(Calendar.DATE, val);
					}
				}
			}
		}

		if (backward)
		{
			while (isNotBusinessDay(tmpMap, cal))
			{
				cal.add(Calendar.DATE, val); // backward to a business day
			}
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
		} else
		{
			cal.set(Calendar.HOUR_OF_DAY, clearTime / 100);
			cal.set(Calendar.MINUTE, clearTime % 100);
			cal.set(Calendar.SECOND, 0);
		}

		return cal.getTime();
	}

	private static boolean isNotBusinessDay(Map holidayMap, Calendar cal)
	{
		return holidayMap.get(df.format(cal.getTime())) != null
			|| cal.get(Calendar.DAY_OF_WEEK) == 1
			|| cal.get(Calendar.DAY_OF_WEEK) == 7;
	}

	//  pass the ES MG send date and return the status day that is eligibilible
	//  for ES MG send.
	//  Any transaction with status (APP/ACS) date that is eariler than the returned 
	//  date is eligibilible to do ES MG Send  
	public static Date getAchStatusDay(
		Date inputDate,
		Map holidayMap,
		int days)
	{
		return getAchDay(inputDate, holidayMap, days, true);
	}

	//  pass the transaction status (APP/ACS) date and return the ES MG send date
	public static Date getAchSendDay(Date inputDate, Map holidayMap, int days)
	{
		return getAchDay(inputDate, holidayMap, days, false);
	}

	public static Date getEstimatedAchSendDay(
		Date inputDate,
		Map holidayMap,
		int days)
	{
		return getAchDay(inputDate, holidayMap, days, false);
	}

	public static String getEstimatedPickupDate(Date inputDate, Map holidayMap)
	{
		EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
		return getEstimatedAchSendDay(
			inputDate,
			holidayMap,
			dynProps.getEsSendWaitDays())
			.toString();
	}
	
	public static Calendar setCalendarTimeFromHHMMString(String hhmm, Calendar cal)
		{
			Logger log = LogFactory.getInstance().getLogger(DateFormatter.class);

			if (hhmm == null || hhmm.length() != 5) {
				return null;
			}
			String[] sepTime = hhmm.split(":");
			// If I don't get an hour and minute then I cannot set the time.
			if (sepTime == null || sepTime.length != 2)
			{
				return null;
			}
			
			try
			{
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sepTime[0]));
				cal.set(Calendar.MINUTE, Integer.parseInt(sepTime[1]));
			} catch (Exception e)
			{

				log.error(
					"Received error parsing database field "
						+ "Should be XX:XX format: "
						+ e);
				// Hour or Minute is not an integer field, determine time to set.
				return null;
			}
			cal.set(Calendar.SECOND, 00);
			cal.set(Calendar.MILLISECOND, 00);
			return cal;
		}
	
	public static boolean canMicroDepositBeConfirmed(
		int mdDays,
		String mdTime,
		Date dbDate)
	{
		Logger log = LogFactory.getInstance().getLogger(DateFormatter.class);
		/* The user is not allowed to key in the deposit amounts until a certain days/time
		 * represented as mdDays and mdTime have elapsed.  This method adds the number of 
		 * days (mdDays) and sets the time to (mdTime) and then compares this to the 
		 * date from the database.
		 */
		Calendar cal = Calendar.getInstance();
		cal.setTime(dbDate);
		cal.add(Calendar.DATE, mdDays);
		Calendar currentCalendar = Calendar.getInstance();
		/* Separate mdTime
		 * Time should come in as Hours and Minutes.
		 * Seconds and Milliseconds will be added as 00.
		 */
		String[] sepTime = mdTime.split(":");
		// If I don't get an hour and minute then I cannot set the time.
		if (sepTime == null || sepTime.length != 2)
		{
			return false;
		}
		try
		{
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sepTime[0]));
			cal.set(Calendar.MINUTE, Integer.parseInt(sepTime[1]));
		} catch (Exception e)
		{

			log.error(
				"Method canMicroDepositBeConfirmed.  Received error parsing database field "
					+ "MDTimeBeforeUserConfirmAllowed.  Should be XX:XX format: "
					+ e);
			// Hour or Minute is not an integer field, determine time to set.
			return false;
		}
		cal.set(Calendar.SECOND, 00);
		cal.set(Calendar.MILLISECOND, 00);
		Date mdDate = cal.getTime();
		Date now = currentCalendar.getTime();
		// Compare modified database date/time with current date/time.
		if (now.compareTo(mdDate) >= 0)
		{
			return true;
		} else
		{
			return false;
		}
	}

	private static Map checkMap(Map map)
	{
		if (map != null)
		{
			return map;
		}

		ServiceFactory sf = ServiceFactory.getInstance();
		TransactionService ts = sf.getTransactionService();
		try
		{
			return ts.getHolidays("emgwww");
		} catch (DataSourceException e)
		{
			throw new EMGRuntimeException(e);
		}
	}
}
