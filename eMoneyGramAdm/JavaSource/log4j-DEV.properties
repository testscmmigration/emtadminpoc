
###############################################################################
##                                                                           ##
##  PRIORITIES                                                               ##
##                                                                           ##
##  These are the priorities of errors logged in code, in order of priority. ##
##  Each category can have a priority level set, meaning that messages of    ##
##  that priority or higher are logged, and messages of lower priority are   ##
##  suppressed.  Categories are hierarchal (by dot notation), and inherit    ##
##  priority and appender information from their parent.                     ##
##                                                                           ##
##  DETAIL:  This level is intended primarily for localized debugging        ##
##           information, not generally considered useful outside of its     ##
##           immediate context.  Developers can add debug message to test    ##
##           components they are working on, and adjust log level filters    ##
##           to locally enable debug level logging on these components only. ##
##                                                                           ##
##  DEBUG:   This level is intended primarily for development and            ##
##           integration environments, to be used to print high level        ##
##           debugging information.  Messages written to this log level      ##
##           can be captured for development environments.                   ##
##                                                                           ##
##  DIAGNOSTIC: This level will indicate low-level diagnostic information    ##
##           captured during normal processing.  This log level is intended  ##
##           for informational messages that are too verbose to write to a   ##
##           production log.  Information logged at this level will include  ##
##           XML of client request and response, all SQL executed, mainframe ##
##           communication byte streams, etc.  It is intended that this      ##
##           information will be included in a secondary production, stored  ##
##           in zipped format, to be used for forensic purposes during       ##
##           problem investigation.                                          ##
##                                                                           ##
##  INFO:    This level indicates high level diagnostic information captured ##
##           during normal processing.  Messages will include summary        ##
##           information about a request, type of request, status of         ##
##           response, etc.                                                  ##
##                                                                           ##
##  MONITOR: This level indicates course-grained informational messages      ##
##           that may be logged to an external system (Unicenter).  This     ##
##           level should include high level summary messages only.          ##
##           Messages about start and stop of application server and comm.   ##
##           Processes are good candidates for this level.  Transaction      ##
##           related messages (other than errors) are generally not          ##
##           appropriate here.                                               ##
##                                                                           ##
##  WARNING: This level is used to indicate abnormal application             ##
##           states that aren't necessarily error conditions.  For           ##
##           example, Problems with PrintedMoneyOrder transactions, POS      ##
##           de-authorization, etc. should be logged at this level.          ##
##                                                                           ##
##  ERROR:   This level should indicate messages regarding application       ##
##           errors affecting user operation.  Generally, all                ##
##           unanticipated runtime exceptions are logged at this level,      ##
##           including all unchecked exceptions.                             ##
##                                                                           ##
##  SEVERE:  This level will be used to indicate an unrecoverable system     ##
##           failure.  Aside from fatal server errors (most of which         ##
##           are logged by the application server itself, and not through    ##
##           Log4J), this includes conditions that prevent normal            ##
##           operation of the application.  For example, messages about      ##
##           unavailability of mainframe or database may be logged at        ##
##           this level.                                                     ##
##                                                                           ##
###############################################################################

##
##  The default logging level is specified at the bottom of this property
##  file.  Currently, it is set to INFO.  Change specific logging levels
##  here:
##
##    log4j.category.<class or package name>=<LEVEL>
##
##    For Example:
##
##       log4j.category.com.moneygram.deltaworks.util=DEBUG
##

log4j.category.emgadm=DEBUG
log4j.category.emgshared=DEBUG
log4j.category.VBVLOG=DEBUG
log4j.rootCategory=DEBUG, LogFile, Console

## Verid Logger
log4j.category.verid=DEBUG, Verid
## Set to only log to the Verid log file and not both.
log4j.additivity.verid=false

###############################################################################
##                                                                           ##
##  Appender configuration                                                   ##
##                                                                           ##
###############################################################################
##
##  Websphere Log File Appender Configuration
##
###############################################################################

log4j.appender.LogFile=org.apache.log4j.DailyRollingFileAppender
log4j.appender.LogFile.file=logs/eMoneyGramAdm.log
log4j.appender.LogFile.layout=com.moneygram.common.log.log4j.GDCPatternLayout
log4j.appender.LogFile.DatePattern='.'yyyy-MM-dd

# This is designed for standalone logging
log4j.appender.LogFile.layout.ConversionPattern=[%d{M/d/yy HH:mm:ss:SSS zzz}][eMTAdm][%p][%c][%t] %m%n

###############################################################################
##
##  Websphere Console Appender Configuration
##
###############################################################################

log4j.appender.Console=org.apache.log4j.ConsoleAppender
log4j.appender.Console.Threshold=DEBUG
log4j.appender.Console.layout=com.moneygram.common.log.log4j.GDCPatternLayout
log4j.appender.Console.layout.ConversionPattern=[%d{M/d/yy HH:mm:ss:SSS zzz}][eMT] %m%n

###############################################################################
##
##  Verid Log File Appender Configuration
##
###############################################################################
log4j.appender.Verid=org.apache.log4j.DailyRollingFileAppender
log4j.appender.Verid.file=logs/eMoneyGramAdm-Verid.log
log4j.appender.Verid.layout=com.moneygram.common.log.log4j.GDCPatternLayout
log4j.appender.Verid.DatePattern='.'yyyy-MM-dd

# This is designed for standalone logging
log4j.appender.Verid.layout.ConversionPattern=[%d{M/d/yy HH:mm:ss:SSS zzz}][eMTAdm-Verid][%p][%c][%t] %m%n

###############################################################################
##
##  Unicenter Appender Configuration
##
###############################################################################

log4j.appender.Unicenter=com.moneygram.common.log.log4j.UnicenterAppender
log4j.appender.Unicenter.layout=com.moneygram.common.log.log4j.UnicenterPatternLayout
log4j.appender.Unicenter.layout.ConversionPattern=! [%d{M/d/yy-HH:mm:ss:SSS-zzz}] [Deltaworks] [%Z{serverName}] [%p] [%c] %m

# log only MONITOR priority and higher, as defined in TELevel
log4j.appender.Unicenter.threshold=MONITOR#com.moneygram.common.log.log4j.TELevel

#
# this appender logs to Unicenter at the given node.  If
# messages are at a certain priority, the message is logged to
# Unicenter AND a page is sent.

# developer workstation
#log4j.appender.Unicenter.unicenterNode=mglkwnt17

# development
log4j.appender.Unicenter.unicenterNode=inaz2k04

# qa
#log4j.appender.Unicenter.unicenterNode=inaz2k05

# production
#log4j.appender.Unicenter.unicenterNode=inaz2k076

#
# colors used for each level in Unicenter
#
log4j.appender.Unicenter.detailColor=default
log4j.appender.Unicenter.debugColor=default
log4j.appender.Unicenter.diagnosticColor=default
log4j.appender.Unicenter.infoColor=default
log4j.appender.Unicenter.monitorColor=default
log4j.appender.Unicenter.warnColor=blue
log4j.appender.Unicenter.errorColor=red
log4j.appender.Unicenter.severeColor=red
log4j.appender.Unicenter.fatalColor=red

#
# cawto parameters
#
log4j.appender.Unicenter.maxMessageSize=255
log4j.appender.Unicenter.maxQueueSize=500
log4j.appender.Unicenter.waitInterval=5000
log4j.appender.Unicenter.removeSize=100
