package emgadm.message;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class ViewMessagesForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy", true);
	private static final String[] RESET_ARRAY = new String[0];

	private String searchTypeCode;
	private String searchPrtyCode;
	private String useDateSearch;
	private String searchMsgText;
	private String beginDate;
	private String beginHour;
	private String beginMinute;
	private String endDate;
	private String endHour;
	private String endMinute;
	private Date beginTime;
	private Date endTime;
	private String orderBy;
	private String orderSeq;
	private String saveAction;
	private String saveAction1;
	private String[] selMsgs;
	private String currentUser;
	private String submitAllMsgs;
	private String submitUnread;
	private String submitSearch;
	private String submitMarkRead;
	private String submitMarkUnread;
	private String submitPurge;
	private String submitPoster;

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		selMsgs = RESET_ARRAY;
		searchTypeCode = "";
		searchPrtyCode = "";
		useDateSearch = "Y";
		searchMsgText = "";

		if (request.getSession().getAttribute("priorities") == null)
		{
			List priorities = new ArrayList();
			String label = null;
			for (byte i = 1; i < 10; i++)
			{
				label = String.valueOf(i);
				label = i == 1 ? label + " - Low Priority" : label;
				label = i == 5 ? label + " - Medium Priority" : label;
				label = i == 9 ? label + " - High Priority" : label;
				priorities.add(new LabelValueBean(label, String.valueOf(i)));
			}
			request.getSession().setAttribute("priorities", priorities);
		}

		long oneDay = 24L * 60L * 60L * 1000L;
		long now = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(now));
		beginDate = df.format(new Date(now));
		beginHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		beginMinute = String.valueOf(cal.get(Calendar.MINUTE) / 5 * 5);
		endDate = df.format(new Date(now + oneDay));
		endHour = "24";
		endMinute = "0";

		if (request.getSession().getAttribute("hours") == null)
		{
			List hours = new ArrayList();
			for (byte i = 1; i < 25; i++)
			{
				String label = "";
				if (i < 12)
				{
					label = String.valueOf(i) + " AM";
				} else
				{
				    if (i==12)
				        label = "Noon";
				    else if (i==24)
				        label = "Midnight";
				    else
				        label = String.valueOf(i - 12) + " PM";
				}
				hours.add(new LabelValueBean(label, String.valueOf(i)));
			}
			request.getSession().setAttribute("hours", hours);
		}

		if (request.getSession().getAttribute("minutes") == null)
		{
			List minutes = new ArrayList();
			for (byte i = 0; i < 12; i++)
			{
				minutes.add(
					new LabelValueBean(
						String.valueOf(i * 5L),
						String.valueOf(i * 5L)));
			}
			request.getSession().setAttribute("minutes", minutes);
		}

		orderBy = "adminMsgId";
		orderSeq = "A";
		saveAction = "";
		saveAction1 = "";
		currentUser = "";
		submitAllMsgs = null;
		submitUnread = null;
		submitSearch = null;
		submitMarkRead = null;
		submitMarkUnread = null;
		submitPurge = null;
		submitPoster = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		if (!StringHelper.isNullOrEmpty(submitSearch))
		{
			Date tmpDate1 = null;
			Date tmpDate2 = null;
			if ("Y".equals(useDateSearch))
			{
				if (StringHelper.isNullOrEmpty(beginDate))
				{
					errors.add(
						"beginDate",
						new ActionError("errors.required", "Post Begin Date"));
				} else
				{
					try
					{
						tmpDate1 = df.parse(beginDate);
					} catch (Exception e)
					{
						errors.add(
							"beginDate",
							new ActionError(
								"error.mal.form.date",
								"Post Begin Date"));
					}
				}

				if (StringHelper.isNullOrEmpty(endDate))
				{
					errors.add(
						"endDate",
						new ActionError("errors.required", "Post End Date"));
				} else
				{
					try
					{
						tmpDate2 = df.parse(endDate);
					} catch (Exception e)
					{
						errors.add(
							"endDate",
							new ActionError(
								"error.mal.form.date",
								"Post End Date"));
					}
				}
			}

			if (errors.size() == 0 && "Y".equals(useDateSearch))
			{
				tmpDate2 =
					new Date(
						tmpDate2.getTime()
							+ Integer.parseInt(endHour) * 60L * 60L * 1000L
							+ Integer.parseInt(endMinute) * 60L * 1000L);
				if (tmpDate1.after(tmpDate2))
				{
					String[] parm =
						{ "Starting Post Time", "Ending Post Time" };
					errors.add(
						"endDate",
						new ActionError("errors.seq.wrong", parm));
				}

				if (errors.size() == 0)
				{
					beginTime = tmpDate1;
					endTime = tmpDate2;
				}
			}
		}

		if ((!StringHelper.isNullOrEmpty(submitMarkRead)
			|| !StringHelper.isNullOrEmpty(submitMarkRead)
			|| !StringHelper.isNullOrEmpty(submitPurge))
			&& selMsgs.length == 0)
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("error.no.record.selected", "Admin Message"));
		}

		return errors;
	}

	public String getSearchTypeCode()
	{
		return searchTypeCode;
	}

	public void setSearchTypeCode(String string)
	{
		searchTypeCode = string;
	}

	public String getSearchPrtyCode()
	{
		return searchPrtyCode;
	}

	public void setSearchPrtyCode(String string)
	{
		searchPrtyCode = string;
	}

	public String getUseDateSearch()
	{
		return useDateSearch;
	}

	public void setUseDateSearch(String string)
	{
		useDateSearch = string;
	}

	public String getSearchMsgText()
	{
		return searchMsgText;
	}

	public void setSearchMsgText(String string)
	{
		searchMsgText = string;
	}

	public String getBeginDate()
	{
		return beginDate;
	}

	public void setBeginDate(String string)
	{
		beginDate = string;
	}

	public String getBeginHour()
	{
		return beginHour;
	}

	public void setBeginHour(String string)
	{
		beginHour = string;
	}

	public String getBeginMinute()
	{
		return beginMinute;
	}

	public void setBeginMinute(String string)
	{
		beginMinute = string;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String string)
	{
		endDate = string;
	}

	public String getEndHour()
	{
		return endHour;
	}

	public void setEndHour(String string)
	{
		endHour = string;
	}

	public String getEndMinute()
	{
		return endMinute;
	}

	public void setEndMinute(String string)
	{
		endMinute = string;
	}

	public Date getBeginTime()
	{
		return beginTime;
	}

	public Date getEndTime()
	{
		return endTime;
	}

	public String getOrderBy()
	{
		return orderBy;
	}

	public void setOrderBy(String string)
	{
		orderBy = string;
	}

	public String getOrderSeq()
	{
		return orderSeq;
	}

	public void setOrderSeq(String string)
	{
		orderSeq = string;
	}

	public String getSaveAction()
	{
		return saveAction;
	}

	public void setSaveAction(String string)
	{
		saveAction = string;
	}

	public String getSaveAction1()
	{
		return saveAction1;
	}

	public void setSaveAction1(String string)
	{
		saveAction1 = string;
	}

	public String[] getSelMsgs()
	{
		return selMsgs;
	}

	public void setSelMsgs(String[] strings)
	{
		selMsgs = strings;
	}

	public String getCurrentUser()
	{
		return currentUser;
	}

	public void setCurrentUser(String string)
	{
		currentUser = string;
	}

	public String getSubmitAllMsgs()
	{
		return submitAllMsgs;
	}

	public void setSubmitAllMsgs(String string)
	{
		submitAllMsgs = string;
	}

	public String getSubmitUnread()
	{
		return submitUnread;
	}

	public void setSubmitUnread(String string)
	{
		submitUnread = string;
	}

	public String getSubmitSearch()
	{
		return submitSearch;
	}

	public void setSubmitSearch(String string)
	{
		submitSearch = string;
	}

	public String getSubmitMarkRead()
	{
		return submitMarkRead;
	}

	public void setSubmitMarkRead(String string)
	{
		submitMarkRead = string;
	}

	public String getSubmitMarkUnread()
	{
		return submitMarkUnread;
	}

	public void setSubmitMarkUnread(String string)
	{
		submitMarkUnread = string;
	}

	public String getSubmitPurge()
	{
		return submitPurge;
	}

	public void setSubmitPurge(String string)
	{
		submitPurge = string;
	}

	public String getSubmitPoster()
	{
		return submitPoster;
	}

	public void setSubmitPoster(String string)
	{
		submitPoster = string;
	}
}