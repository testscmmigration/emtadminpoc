package emgadm.message;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class PostAdminMessageForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy", true);
	private static final String[] RESET_ARRAY = new String[0];

	private String[] roleList;
	private String[] userList;
	private String typeCode;
	private String prtyCode;
	private String beginDate;
	private String beginHour;
	private String beginMinute;
	private String endDate;
	private String endHour;
	private String endMinute;
	private String msgText;
	private Date beginTime;
	private Date endTime;
	private String entryState;
	private String submitPost;
	private String submitReset;

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		roleList = RESET_ARRAY;
		userList = RESET_ARRAY;
		typeCode = "";
		prtyCode = "";

		if (request.getSession().getAttribute("priorities") == null)
		{
			List priorities = new ArrayList();
			String label = null;
			for (byte i = 1; i < 10; i++)
			{
				label = String.valueOf(i);
				label = i == 1 ? label + " - Low Priority" : label;
				label = i == 5 ? label + " - Medium Priority" : label;
				label = i == 9 ? label + " - High Priority" : label;
				priorities.add(new LabelValueBean(label, String.valueOf(i)));
			}
			request.getSession().setAttribute("priorities", priorities);
		}

		long endDays = 24L * 60L * 60L * 1000L * 14;  // *14 for 2 weeks default expiration
		long now = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(now));
		beginDate = df.format(new Date(now));
		beginHour = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		beginMinute = String.valueOf(cal.get(Calendar.MINUTE) / 5 * 5);
		endDate = df.format(new Date(now + endDays));
		endHour = "24";
		endMinute = "0";

		if (request.getSession().getAttribute("hours") == null)
		{
			List hours = new ArrayList();
			for (byte i = 1; i < 25; i++)
			{
				String label = "";
				if (i < 12)
				{
					label = String.valueOf(i) + " AM";
				} else
				{
				    if (i==12)
				        label = "Noon";
				    else if (i==24)
				        label = "Midnight";
				    else
				        label = String.valueOf(i - 12) + " PM";
				}
				hours.add(new LabelValueBean(label, String.valueOf(i)));
			}
			request.getSession().setAttribute("hours", hours);
		}

		if (request.getSession().getAttribute("minutes") == null)
		{
			List minutes = new ArrayList();
			for (byte i = 0; i < 12; i++)
			{
				minutes.add(
					new LabelValueBean(
						String.valueOf(i * 5L),
						String.valueOf(i * 5L)));
			}
			request.getSession().setAttribute("minutes", minutes);
		}

		msgText = "";
		entryState = "post";
		submitPost = null;
		submitReset = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		if (!StringHelper.isNullOrEmpty(submitPost))
		{
			boolean noRole = roleList.length == 0 ? true : false;
			if (!noRole)
			{
				noRole = true;
				for (byte i = 0; i < roleList.length; i++)
				{
					if (!StringHelper.isNullOrEmpty(roleList[i]))
					{
						noRole = false;
						break;
					}
				}
			}

			boolean noUser = userList.length == 0 ? true : false;
			if (!noUser)
			{
				noUser = true;
				for (byte i = 0; i < userList.length; i++)
				{
					if (!StringHelper.isNullOrEmpty(userList[i]))
					{
						noUser = false;
						break;
					}
				}
			}

			if (noRole && noUser)
			{
				errors.add(
					"roleList",
					new ActionError(
						"errors.required",
						"A selected role and/or user"));
			}

			if (StringHelper.isNullOrEmpty(typeCode))
			{
				errors.add(
					"typeCode",
					new ActionError("errors.required", "Message Type"));
			}

			if (StringHelper.isNullOrEmpty(prtyCode))
			{
				errors.add(
					"prtyCode",
					new ActionError("errors.required", "Priority Code"));
			}

			Date tmpDate1 = null;
			if (StringHelper.isNullOrEmpty(beginDate))
			{
				errors.add(
					"beginDate",
					new ActionError("errors.required", "Post Begin Date"));
			} else
			{
				try
				{
					tmpDate1 = df.parse(beginDate);
				} catch (Exception e)
				{
					errors.add(
						"beginDate",
						new ActionError(
							"error.mal.form.date",
							"Post Begin Date"));
				}
			}

			Date tmpDate2 = null;
			if (StringHelper.isNullOrEmpty(endDate))
			{
				errors.add(
					"endDate",
					new ActionError("errors.required", "Post End Date"));
			} else
			{
				try
				{
					tmpDate2 = df.parse(endDate);
				} catch (Exception e)
				{
					errors.add(
						"endDate",
						new ActionError(
							"error.mal.form.date",
							"Post End Date"));
				}
			}

			if (StringHelper.isNullOrEmpty(msgText))
			{
				errors.add(
					"msgText",
					new ActionError("errors.required", "Admin Message Text"));
			} else if (msgText.length() > 255)
			{
				String[] parm = { "Admin Message Text", "255" };
				errors.add(
					"msgText",
					new ActionError("errors.length.too.long", parm));
			}

			if (errors.size() == 0)
			{
				tmpDate1 =
					new Date(
						tmpDate1.getTime()
							+ Integer.parseInt(beginHour) * 60L * 60L * 1000L
							+ Integer.parseInt(beginMinute) * 60L * 1000L);

				tmpDate2 =
					new Date(
						tmpDate2.getTime()
							+ Integer.parseInt(endHour) * 60L * 60L * 1000L
							+ Integer.parseInt(endMinute) * 60L * 1000L);
				if (tmpDate1.after(tmpDate2))
				{
					String[] parm =
						{ "Starting Post Time", "Ending Post Time" };
					errors.add(
						"endDate",
						new ActionError("errors.seq.wrong", parm));
				}

				if (errors.size() == 0)
				{
					beginTime = tmpDate1;
					endTime = tmpDate2;
				}

			}
		}

		return errors;
	}

	public String[] getRoleList()
	{
		return roleList;
	}

	public void setRoleList(String[] strings)
	{
		roleList = strings;
	}

	public String[] getUserList()
	{
		return userList;
	}

	public void setUserList(String[] strings)
	{
		userList = strings;
	}

	public String getTypeCode()
	{
		return typeCode;
	}

	public void setTypeCode(String string)
	{
		typeCode = string;
	}

	public String getPrtyCode()
	{
		return prtyCode;
	}

	public void setPrtyCode(String string)
	{
		prtyCode = string;
	}

	public String getBeginDate()
	{
		return beginDate;
	}

	public void setBeginDate(String string)
	{
		beginDate = string;
	}

	public String getBeginHour()
	{
		return beginHour;
	}

	public void setBeginHour(String string)
	{
		beginHour = string;
	}

	public String getBeginMinute()
	{
		return beginMinute;
	}

	public void setBeginMinute(String string)
	{
		beginMinute = string;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String string)
	{
		endDate = string;
	}

	public String getEndHour()
	{
		return endHour;
	}

	public void setEndHour(String string)
	{
		endHour = string;
	}

	public String getEndMinute()
	{
		return endMinute;
	}

	public void setEndMinute(String string)
	{
		endMinute = string;
	}

	public String getMsgText()
	{
		return msgText;
	}

	public void setMsgText(String string)
	{
		msgText = string;
	}

	public Date getBeginTime()
	{
		return beginTime;
	}

	public Date getEndTime()
	{
		return endTime;
	}

	public String getEntryState()
	{
		return entryState;
	}

	public void setEntryState(String string)
	{
		entryState = string;
	}

	public String getSubmitPost()
	{
		return submitPost;
	}

	public void setSubmitPost(String string)
	{
		submitPost = string;
	}

	public String getSubmitReset()
	{
		return submitReset;
	}

	public void setSubmitReset(String string)
	{
		submitReset = string;
	}

}