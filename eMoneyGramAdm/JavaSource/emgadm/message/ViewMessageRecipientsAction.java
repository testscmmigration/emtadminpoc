package emgadm.message;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.AdminMessage;
import emgshared.model.MessageRecipientBean;
import emgshared.services.MessageService;
import emgshared.services.ServiceFactory;

public class ViewMessageRecipientsAction extends EMoneyGramAdmBaseAction
{
//	private static final DateFormatter df =
//		new DateFormatter("MM/dd/yyyy", true);
	private static final MessageService ms =
		ServiceFactory.getInstance().getMessageService();
	private static final UserProfileManager upm =
		ManagerFactory.createUserManager();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ViewMessageRecipientsForm form = (ViewMessageRecipientsForm) f;
		UserProfile up = getUserProfile(request);

		if (!StringHelper.isNullOrEmpty(form.getSubmitExit()))
		{
			request.getSession().removeAttribute("rcvList");
			return mapping.findForward("viewMessages");
		}

		if (StringHelper.isNullOrEmpty(form.getMsgId()))
		{
			throw new EMGRuntimeException("Message id is missing");
		}

		int messageId = Integer.parseInt(form.getMsgId());
		AdminMessage am = new AdminMessage();
		am.setAdminMsgId(new Integer(messageId));
		List list = ms.getAdminMessages(up.getUID(), am);
		Collections.sort(list);
		if (list == null || list.size() == 0)
		{
			throw new EMGRuntimeException(
				"Message " + messageId + " not found");
		}

		am = (AdminMessage) list.iterator().next();
		form.setMsgId(String.valueOf(am.getAdminMsgIdNbr()));
		form.setTypeCode(am.getAdminMsgTypeDesc());
		form.setPrtyCode(am.getMsgPrtyCode());
		form.setMsgText(am.getMsgText());
		form.setBeginTime(am.getMsgBegDateText());
		form.setEndTime(am.getMsgThruDateText());
		form.setPoster(am.getCreateUserid());
		form.setPostTime(am.getCreateDateText());

		list = ms.getAdminMessageRecipients(up.getUID(), messageId);

		Iterator<MessageRecipientBean> iter = list.iterator();
		while (iter.hasNext()) {
			MessageRecipientBean mrb = iter.next();
			UserProfile recipnt = upm.getUser(mrb.getRecipientId());
			mrb.setFirstName(recipnt.getFirstName());
			mrb.setLastName(recipnt.getLastName());
			List<Role> roles = recipnt.getRoles();
			StringBuilder roleNames = new StringBuilder();
			boolean afterFirst = false;
			for (Role role : roles) {
				if (afterFirst) {
					roleNames.append(", ");
				} else {
					afterFirst = true;
				}
				roleNames.append(role.getDisplayName());
			}
			mrb.setRoleName((roleNames.length() > 0) ? roleNames.toString() : "No roles defined");
			mrb.setEmail(recipnt.getEmailAddress());
//			mrb.setLastAccessTime(recipnt.getLastAccess().getTime());
		}

		request.getSession().setAttribute("rcvList", list);

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
