package emgadm.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.RoleManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;
import emgadm.util.StringHelper;
import emgshared.model.AdminMessage;
import emgshared.model.AdminMessageType;
import emgshared.services.MessageService;
import emgshared.services.ServiceFactory;

public class PostAdminMessageAction extends EMoneyGramAdmBaseAction
{
	private static final MessageService ms =
		ServiceFactory.getInstance().getMessageService();
	private static final UserProfileManager upm =
		ManagerFactory.createUserManager();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		PostAdminMessageForm form = (PostAdminMessageForm) f;
		UserProfile up = getUserProfile(request);
		ActionErrors errors = new ActionErrors();

		//  Get a list of all system roles
		if (request.getSession().getAttribute("sysRoles") == null)
		{
			List sysRoles = new ArrayList();
			List roles = RoleManager.getRoles();
			Collections.sort(roles);
			Iterator iter = roles.iterator();
			while (iter.hasNext())
			{
				Role role = (Role) iter.next();
				sysRoles.add(
					new LabelValueBean(role.getDisplayName(), role.getId()));
			}
			request.getSession().setAttribute("sysRoles", sysRoles);
		}

		//  get a list of all system admin users
		if (request.getSession().getAttribute("sysUsers") == null)
		{
			List sysUsers = new ArrayList();
			UserQueryCriterion criteria = new UserQueryCriterion();
			criteria.setInternalUser(Boolean.valueOf("true"));
			List users = upm.findHollowUsers(criteria);
			Collections.sort(users);
			Iterator iter = users.iterator();
			while (iter.hasNext())
			{
				UserProfile user = (UserProfile) iter.next();
				sysUsers.add(
					new LabelValueBean(
						user.getUID()
							+ " - "
							+ user.getFirstName()
							+ " "
							+ user.getLastName(),
						user.getUID()));
			}
			request.getSession().setAttribute("sysUsers", sysUsers);
		}

		//  get a list of admin message types
		if (request.getSession().getAttribute("msgTypes") == null)
		{
			List msgTypes = new ArrayList();
			Iterator iter = ms.getAdminMessageTypes(up.getUID()).iterator();
			while (iter.hasNext())
			{
				AdminMessageType amt = (AdminMessageType) iter.next();
				msgTypes.add(
					new LabelValueBean(
						amt.getAdminMsgTypeDesc(),
						amt.getAdminMsgTypeCode()));
			}
			request.getSession().setAttribute("msgTypes", msgTypes);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitReset()))
		{
			form.reset(null, request);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitPost()))
		{
			Map userMap = new HashMap();

			for (byte i = 0; i < form.getRoleList().length; i++)
			{
				if (!StringHelper.isNullOrEmpty(form.getRoleList()[i]))
				{
					UserQueryCriterion criteria = new UserQueryCriterion();
					criteria.setRoleId(form.getRoleList()[i]);
					Iterator iter = upm.findHollowUsers(criteria).iterator();
					while (iter.hasNext())
					{
						UserProfile user = (UserProfile) iter.next();
						userMap.put(user.getUID(), user.getUID());
					}
				}
			}

			for (byte i = 0; i < form.getUserList().length; i++)
			{
				if (!StringHelper.isNullOrEmpty(form.getUserList()[i]))
				{
					userMap.put(form.getUserList()[i], form.getUserList()[i]);
				}
			}

			String[] users = (String[]) userMap.values().toArray(new String[0]);

			AdminMessage am = new AdminMessage();
			am.setMsgText(form.getMsgText());
			am.setMsgBegDate(form.getBeginTime());
			am.setMsgThruDate(form.getEndTime());
			am.setAdminMsgTypeCode(form.getTypeCode());
			am.setMsgPrtyCode(form.getPrtyCode());
			am = ms.insertAdminMessage(up.getUID(), am, users);

			String[] parms =
				{ "Admin Message", String.valueOf(am.getAdminMsgIdNbr())};
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("msg.added", parms));
			saveErrors(request, errors);
			form.setEntryState("reset");
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
