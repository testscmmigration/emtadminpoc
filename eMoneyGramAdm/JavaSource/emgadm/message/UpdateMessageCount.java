package emgadm.message;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.MessageCacheBean;
import emgshared.model.UserMessages;

public class UpdateMessageCount extends HttpServlet {

    public void init(ServletConfig config) throws ServletException {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserProfile up = (UserProfile) request.getSession().getAttribute(
                EMoneyGramAdmSessionConstants.USER_PROFILE);
        if (up == null) {
            throw new EMGRuntimeException(
                    "User profile does not exist anymore!");
        }

        try {
            Map msgMap = MessageCacheBean.getInstance().getMessageMap();
            if (msgMap != null && msgMap.size() != 0) {
                String cnt = "0";
                UserMessages um = (UserMessages) msgMap.get(up.getUID());
                if (um != null) {
                    cnt = String.valueOf(um.getUnreadCount());
                }

                response.setContentType("text/xml");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().write(cnt);
            }
        } catch (DataSourceException e) {
            throw new EMGRuntimeException(e);
        }

    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
