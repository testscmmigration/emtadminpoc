package emgadm.message;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ViewMessageRecipientsForm extends EMoneyGramAdmBaseValidatorForm
{
	private String msgId;
	private String poster;
	private String postTime;
	private String typeCode;
	private String prtyCode;
	private String msgText;
	private String beginTime;
	private String endTime;
	private String saveAction;
	private String saveAction1;
	private String submitExit;

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		msgId = "";
		poster = "";
		postTime = "";
		typeCode = "";
		prtyCode = "";
		msgText = "";
		beginTime = "";
		endTime = "";
		saveAction = "";
		saveAction1 = "";
		submitExit = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		return errors;
	}

	public String getMsgId()
	{
		return msgId;
	}

	public void setMsgId(String string)
	{
		msgId = string;
	}

	public String getPoster()
	{
		return poster;
	}

	public void setPoster(String string)
	{
		poster = string;
	}

	public String getPostTime()
	{
		return postTime;
	}

	public void setPostTime(String string)
	{
		postTime = string;
	}

	public String getTypeCode()
	{
		return typeCode;
	}

	public void setTypeCode(String string)
	{
		typeCode = string;
	}

	public String getPrtyCode()
	{
		return prtyCode;
	}

	public void setPrtyCode(String string)
	{
		prtyCode = string;
	}

	public String getMsgText()
	{
		return msgText;
	}

	public void setMsgText(String string)
	{
		msgText = string;
	}

	public String getBeginTime()
	{
		return beginTime;
	}

	public void setBeginTime(String string)
	{
		beginTime = string;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String string)
	{
		endTime = string;
	}

	public String getSaveAction()
	{
		return saveAction;
	}

	public void setSaveAction(String string)
	{
		saveAction = string;
	}

	public String getSaveAction1()
	{
		return saveAction1;
	}

	public void setSaveAction1(String string)
	{
		saveAction1 = string;
	}

	public String getSubmitExit()
	{
		return submitExit;
	}

	public void setSubmitExit(String string)
	{
		submitExit = string;
	}
}