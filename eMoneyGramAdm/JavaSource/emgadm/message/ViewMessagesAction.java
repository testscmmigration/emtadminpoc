package emgadm.message;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.AdminMessage;
import emgshared.model.AdminMessageType;
import emgshared.services.MessageService;
import emgshared.services.ServiceFactory;

public class ViewMessagesAction extends EMoneyGramAdmBaseAction
{
//	private static final DateFormatter df =
//		new DateFormatter("MM/dd/yyyy", true);
	private static final MessageService ms =
		ServiceFactory.getInstance().getMessageService();
//	private static final UserProfileManager upm =
//		ManagerFactory.createUserManager();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ViewMessagesForm form = (ViewMessagesForm) f;
		UserProfile up = getUserProfile(request);
		ActionErrors errors = new ActionErrors();
		form.setCurrentUser(up.getUID());
		if (up.hasPermission("postAdminMessage"))
		{
			request.setAttribute("allowPostMsg", "Y");
		}

		if (up.hasPermission("viewMessageRecipients"))
		{
			request.setAttribute("allowViewRecipnt", "Y");
		}

		request.getSession().setAttribute("msgList", new ArrayList());

		// check if the user has purge message permission
		if (request.getSession().getAttribute("purgeMsgs") == null)
		{
			String flag =
				up.hasPermission("purgeAdminMessages") ? "Y" : "N";
			request.getSession().setAttribute("purgeMsgs", flag);
		}

		//  get a list of admin message types
		if (request.getSession().getAttribute("msgTypes") == null)
		{
			List msgTypes = new ArrayList();
			Iterator iter = ms.getAdminMessageTypes(up.getUID()).iterator();
			while (iter.hasNext())
			{
				AdminMessageType amt = (AdminMessageType) iter.next();
				msgTypes.add(
					new LabelValueBean(
						amt.getAdminMsgTypeDesc(),
						amt.getAdminMsgTypeCode()));
			}
			request.getSession().setAttribute("msgTypes", msgTypes);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitMarkRead()))
		{
			for (int i = 0; i < form.getSelMsgs().length; i++)
			{
				ms.setMsgRecipnt(
					Integer.parseInt(form.getSelMsgs()[i]),
					up.getUID(),
					false);
			}
			form.setSelMsgs(new String[0]);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitMarkUnread()))
		{
			for (int i = 0; i < form.getSelMsgs().length; i++)
			{
				ms.setMsgRecipnt(
					Integer.parseInt(form.getSelMsgs()[i]),
					up.getUID(),
					true);
			}
			form.setSelMsgs(new String[0]);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitPurge()))
		{
			for (int i = 0; i < form.getSelMsgs().length; i++)
			{
				ms.purgeMessage(
					up.getUID(),
					Integer.parseInt(form.getSelMsgs()[i]));
			}
			form.setSelMsgs(new String[0]);
		}

		String action = "";

		if (StringHelper.isNullOrEmpty(form.getSubmitAllMsgs())
			&& StringHelper.isNullOrEmpty(form.getSubmitUnread())
			&& StringHelper.isNullOrEmpty(form.getSubmitSearch()))
		{
			action = form.getSaveAction();
			action =
				StringHelper.isNullOrEmpty(form.getSaveAction1())
					? action
					: form.getSaveAction1();
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitAllMsgs())
			|| "all".equals(action))
		{
			form.setSaveAction("list");
			form.setSaveAction1("all");
			List msgList =
				ms.getUserAdminMessages(up.getUID(), up.getUID(), false);
			if (msgList == null || msgList.size() == 0)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.no.record.found", "Admin Message"));
				saveErrors(request, errors);
			}
			request.getSession().setAttribute("msgList", msgList);
		} else if (
			!StringHelper.isNullOrEmpty(form.getSubmitUnread())
				|| "unread".equals(action))
		{
			form.setSaveAction("list");
			form.setSaveAction1("unread");
			List msgList =
				ms.getUserAdminMessages(up.getUID(), up.getUID(), true);
			if (msgList == null || msgList.size() == 0)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.no.record.found", "Admin Message"));
				saveErrors(request, errors);
			}
			request.getSession().setAttribute("msgList", msgList);
		} else if (
			!StringHelper.isNullOrEmpty(form.getSubmitPoster())
				|| ("poster".equals(action)))
		{
			form.setSaveAction("search");
			form.setSaveAction1("poster");
			AdminMessage am = new AdminMessage();
			am.setAdminMsgId(null);
			am.setMsgText(null);
			am.setMsgBegDate(null);
			am.setMsgThruDate(null);
			am.setAdminMsgTypeCode(null);
			am.setMsgPrtyCode(null);
			am.setMsgText(null);
			am.setCreateUserid(up.getUID());
			List msgList = ms.getAdminMessages(up.getUID(), am);
			if ((msgList == null || msgList.size() == 0) & StringHelper.isNullOrEmpty(form.getSubmitPurge()))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"errors.no.match.record.found",
						"Admin Message"));
				saveErrors(request, errors);
			} else
			{
				request.getSession().setAttribute("msgList", msgList);
			}
		} else if (
			!StringHelper.isNullOrEmpty(form.getSubmitSearch())
				|| "search".equals(action))
		{
			form.setSaveAction("search");
			form.setSaveAction1("");
			AdminMessage am = new AdminMessage();
			am.setAdminMsgId(null);
			am.setMsgText(null);
			if ("Y".equals(form.getUseDateSearch()))
			{
				am.setMsgBegDate(form.getBeginTime());
				am.setMsgThruDate(form.getEndTime());
			} else
			{
				am.setMsgBegDate(null);
				am.setMsgThruDate(null);
			}
			am.setAdminMsgTypeCode(null);
			if (!"All".equals(form.getSearchTypeCode()))
			{
				am.setAdminMsgTypeCode(form.getSearchTypeCode());
			}
			am.setMsgPrtyCode(null);
			if (!"All".equals(form.getSearchPrtyCode()))
			{
				am.setMsgPrtyCode(form.getSearchPrtyCode());
			}
			am.setMsgText(null);
			if (!StringHelper.isNullOrEmpty(form.getSearchMsgText()))
			{
				am.setMsgText(form.getSearchMsgText());
			}
			am.setCreateUserid(null);
			List msgList = ms.getAdminMessages(up.getUID(), am);
			if (msgList == null || msgList.size() == 0)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"errors.no.match.record.found",
						"Admin Message"));
				saveErrors(request, errors);
			} else
			{
				request.getSession().setAttribute("msgList", msgList);
			}
		}

		if (errors != null && errors.size() == 0)
		{
			List msgList = (List) request.getSession().getAttribute("msgList");
			if (msgList != null && msgList.size() != 0)
			{
				AdminMessage.orderBy = form.getOrderBy();
				AdminMessage.orderSeq = form.getOrderSeq();
				Collections.sort(msgList);
				request.getSession().setAttribute("msgList", msgList);
			}
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
