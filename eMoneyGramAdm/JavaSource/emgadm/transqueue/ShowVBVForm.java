package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.internaluseradmin.EMoneyGramAdmBaseUserAdminForm;

public class ShowVBVForm extends EMoneyGramAdmBaseUserAdminForm
{
	private String tranId;

	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1)
	{
		super.reset(arg0, arg1);
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		return errors;
	}
}
