package emgadm.transqueue;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import shared.mgo.services.AgentConnectService;
import shared.mgo.services.MGOServiceFactory;

import com.ibm.wsspi.sib.exitpoint.ra.HashMap;
import com.moneygram.acclient.CurrencyInfo;
import com.moneygram.common.util.StringUtility;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.CustAddress;
import emgadm.model.EPartnerSite;
import emgadm.model.GlAccount;
import emgadm.model.TranAction;
import emgadm.model.TranActionView;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmDynProperties;
import emgadm.security.TranSecurityMatrix;
import emgadm.services.DecryptionService;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionScoring;
import emgadm.services.TransactionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgadm.sysmon.EventListConstant;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.DeliveryOption;
import emgshared.model.IPDetails;
import emgshared.model.TranSubReason;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.model.UserActivityType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.StringHelper;
import eventmon.eventmonitor.EventMonitor;

public class TransactionDetailAction extends EMoneyGramAdmBaseAction {
	public static final NumberFormat df = new DecimalFormat("#,##0.00");
	public static final NumberFormat rateNumberFormat = new DecimalFormat("#,##0.0000");


	private static final TransactionService tranService = ServiceFactory.getInstance().getTransactionService();
	protected static final DecryptionService decryptService = emgadm.services.ServiceFactory.getInstance().getDecryptionService();

	static String TRANSID="transactionId";
	
	private BigDecimal recoveredAmt = null;
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	// private static final MailService mailService =
	// ServiceFactory.getInstance().getMailService();
	// TODO Remove hardcoded resource file.
	// private static final String resourceFile =
	// "emgadm.resources.ApplicationResources";
	public ActionForward execute(ActionMapping mapping, ActionForm f, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		TransactionDetailForm form = (TransactionDetailForm) f;
		UserProfile up = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		AgentConnectService acs = MGOServiceFactory.getInstance().getAgentConnectService();

		String lookUp = form.getDispatch();
		String ipAddress = form.getTranCustIP();
		boolean firstTimePageLoad= false;

		if (lookUp != null) {
			boolean isIpLookuponScoring= false;
			IPDetails ipDetails = getIPDetailsFromLookupService(ipAddress, isIpLookuponScoring);
			Transaction transaction = tm.getTransaction(Integer.parseInt(form.getTranId()));
			transaction.setIpDetails(ipDetails);
			tm.updateTransaction(transaction, up.getUID());
			form.setConsumerCity(ipDetails.getConsumerCity());
			form.setConsumerCountry(ipDetails.getConsumerCountry());
			form.setConsumerDomain(ipDetails.getConsumerDomain());
			form.setConsumerISP(ipDetails.getConsumerISP());
			form.setConsumerRegion(ipDetails.getConsumerRegion());
			form.setConsumerZipCode(ipDetails.getConsumerZipCode());
			String countryCode = ipDetails.getCountryCode();
			
			if (countryCode.equalsIgnoreCase("US")) {
				countryCode = "USA";
			} else if (countryCode.equalsIgnoreCase("UK")) {
				countryCode = "GBR";

			} else if (countryCode.equalsIgnoreCase("DE")) {
				countryCode = "DEU";
			} 

			form.setConsumerCountryCode(countryCode);
			if (form.getConsumerCountryCode() != null
					&& form.getTranSendCountry() != null
					&& form.getConsumerCountryCode().equalsIgnoreCase(
							form.getTranSendCountry())) {
				form.setCountrySameFlag(true);
			} else {
				form.setCountrySameFlag(false);
			}
			
		} else {
			firstTimePageLoad = true;
			// code changed for story 471.
			String tranId = form.getTranId();
			Map<String, String> transferMap = new HashMap();
			if (!StringUtility.isNullOrEmpty(tranId)) {
				if (NumberUtils.isNumber(tranId.trim())) {
					transferMap.put(TRANSID, tranId);
					request.getSession().setAttribute(TRANSID, transferMap);
				}
			} else {
				transferMap = (Map<String, String>) request.getSession()
						.getAttribute(TRANSID);
				if (transferMap != null)
					if (transferMap.containsKey(TRANSID))
						tranId = transferMap.get(TRANSID);
			}

			Transaction transaction = tm.getTransaction(Integer.parseInt(tranId));
			
			IPDetails ipDetails =transaction.getIpDetails();
			
			form.setConsumerCity(ipDetails.getConsumerCity());
			form.setConsumerCountry(ipDetails.getConsumerCountry());
			form.setConsumerDomain(ipDetails.getConsumerDomain());
			form.setConsumerISP(ipDetails.getConsumerISP());
			form.setConsumerRegion(ipDetails.getConsumerRegion());
			form.setConsumerZipCode(ipDetails.getConsumerZipCode());
			String countryName = ipDetails.getConsumerCountry();
			if (countryName != null) {
				if (countryName.equalsIgnoreCase("United States")) {
					form.setConsumerCountryCode("USA");
				} else if (countryName.equalsIgnoreCase("United Kingdom")) {
					form.setConsumerCountryCode("GBR");

				} else if (countryName.equalsIgnoreCase("Germany")) {
					form.setConsumerCountryCode("DEU");
				} else {
					form.setConsumerCountryCode("");
				}
			} else {
				form.setConsumerCountryCode("");
			}
			
			

		}
		if (!StringHelper.isNullOrEmpty(form.getSubmitExit())) {
			request.getSession().removeAttribute("tranId");
			request.setAttribute("partnerSiteId",form.getPartnerSiteId());
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
		}
		// check to see if the status/substatus of the transaction has changed
		if (!StringHelper.isNullOrEmpty(form.getTranStatusCode()) && !StringHelper.isNullOrEmpty(form.getTranSubStatusCode())) {
			Transaction tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
			if (!tmp.getTranStatCode().equals(form.getTranStatusCode()) || !tmp.getTranSubStatCode().equals(form.getTranSubStatusCode())) {
				String[] parm = { form.getTranStatusCode() + "/" + form.getTranSubStatusCode(),
						tmp.getTranStatCode() + "/" + tmp.getTranSubStatCode() };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.transaction.status.changed.already", parm));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}
		}

		Transaction tmp = null;
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService();
		if (!StringUtility.isNullOrEmpty(form.getTranId())) {
			System.out.println("********** For tranId = " + form.getTranId());
		}
		TransactionScoring tranScoring = ServiceFactory.getInstance().getTransactionScoring();

		if (!StringHelper.isNullOrEmpty(form.getTranId())) {
			tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
			tranService = ServiceFactory.getInstance().getTransactionService(tmp.getPartnerSiteId());
		}

		try {
			List<GlAccount> glAccounts = new ArrayList<GlAccount>(tranService.getGLAccounts(up.getUID()));
			request.getSession().setAttribute("glAccounts", glAccounts);

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}

		String confirmAction = null;
		// user requests a transaction action and needs a confirmation
		confirmAction = (String) request.getAttribute("confirm");
		if ((confirmAction == null)
				&& !(StringHelper.isNullOrEmpty(form.getSubmitApproveProcess()) && StringHelper.isNullOrEmpty(form.getSubmitApprove())
						&& StringHelper.isNullOrEmpty(form.getSubmitProcess()) && StringHelper.isNullOrEmpty(form.getSubmitUndoApprove())
						&& StringHelper.isNullOrEmpty(form.getSubmitUndoDeny())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecAchAutoRef())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecAchCxlRef())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecAchRefundReq())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecCcAutoRef())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecCcCxlRef())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecCcRefundReq())
						&& StringHelper.isNullOrEmpty(form.getSubmitAutoWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form.getSubmitManWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecordMgCancelRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitAutoChargebackAndWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecordAchChargeback())
						&& StringHelper.isNullOrEmpty(form.getSubmitCancelAchWaiting())
						&& StringHelper.isNullOrEmpty(form.getSubmitEsMGSend())
						&& StringHelper.isNullOrEmpty(form.getSubmitEsMGSendCancel())
						&& StringHelper.isNullOrEmpty(form.getSubmitRecordTranError())
						&& StringHelper.isNullOrEmpty(form.getSubmitCancelBankRqstRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankRqstRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitAutoCancelBankRqstRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankRepresentRejDebit())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankRejectedDebit())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankRecordVoid())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankIssueRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitBankWriteOffLoss())
						&& StringHelper.isNullOrEmpty(form.getSubmitRetrievalRequest())
						&& StringHelper.isNullOrEmpty(form.getSubmitReleaseTransaction())
						&& StringHelper.isNullOrEmpty(form.getSubmitUndoDSError())
						&& StringHelper.isNullOrEmpty(form.getSubmitCcManualRefund())
						&& StringHelper.isNullOrEmpty(form.getSubmitCcManualVoid()))) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRAN_CONFIRM);
		}
		// user's select an action that will invoke another form action
		if (!StringHelper.isNullOrEmpty(form.getSubmitResubmitACH())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RESUBMIT_ACH);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitCcSale())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_CHARGE_BACKUP_CREDIT_CARD);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManCollectFee())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_COLLECT_FEE);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManRefundFee())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_REFUND_FEE);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitManReturnFunding())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_MAN_RETURN_FUNDING);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecoveryOfLoss())) {
			List tranSubReasons = this.buildSubReasonList(TransactionStatus.RECOVERY_OF_LOSS_CODE);
			request.getSession().setAttribute("TranSubReasons", tranSubReasons);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECOVERY_OF_LOSS);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordCcChargeback())) {
			ChargebackTransactionHeader cbth = null;
			try {
				cbth = tm.getChargebackTransactionHeader(up.getUID(), Integer.parseInt(form.getTranId()));
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Error attempting to get ChargebackHeader", e);
			}
			if (cbth == null) {
				String[] parms = { form.getTranId() };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.chargeback.tracking.required", parms));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			} else {
				buildSubReasonListBox(request, "CHB");
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_CC_CHARGEBACK);
			}
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitDeny())) {
			buildSubReasonListBox(request, TransactionStatus.DENIED_CODE);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_DENY);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitMoveToPending())) {
			buildSubReasonListBox(request, TransactionStatus.PENDING_CODE);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_PEND);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordLossAdjustment())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_LOASS_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordFeeAdjustment())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_FEE_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordConsumerRefundOrRemittanceAdjustment())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordMgActivityAdjustment())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RECORD_MG_ACTIVITY_ADJUSTMENT);
		}
		String id = null;
		id = form.getTranId();
		// Pass the id as a queryString for transaction ownership
		if (request.getParameter("id") != null) {
			id = request.getParameter("id");
			request.getSession().setAttribute("tranId", id);
		}
		int tranId = 0;
		if (StringHelper.isNullOrEmpty(id) && StringHelper.containsNonDigits(id)) {
			tranId = Integer.parseInt(form.getTranId());
		} else {
			tranId = Integer.parseInt(id);
		}
		// user click the add comment button
		if (!StringHelper.isNullOrEmpty(form.getSubmitAddComment())) {
			if(StringHelper.isNullOrEmpty(form.getCmntText())) {
				form.setCmntText("");
			}
			tm.setTransactionComment(Integer.parseInt(id), form.getTranCmntReasCode(), form.getCmntText(), up.getUID());
			form.reset(mapping, request);
		} else {
			form.setCmntText("");
		}
		// user click on the generate transaction score button
		if (!StringHelper.isNullOrEmpty(form.getSubmitGenTranScore())) {
			tranScoring.getTransactionScore(up.getUID(), tranId);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.tran.score.generated", form.getTranId()));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(form.getTelecheckAction())) {
			Transaction tran = tm.getTransaction(tranId);
			tran.setStatus(TransactionStatus.BANK_RECOVER_OF_LOSS);
			String amount = request.getParameter("amount");
			tm.updateTransaction(tran, up.getUID(), null, new Float(amount));
			tran.setStatus(TransactionStatus.SENT_BANK_WRITE_OFF_LOSS);
			String fee = request.getParameter("fee");
			tm.updateTransaction(tran, up.getUID(), null, new Float(fee));
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("wap.recovery.loss.successful", form.getTranId()));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitCcPartialRefund())) {

			try {

				Transaction tran = tm.getTransaction(tranId);

				tranService.doCreditCardPartialRefund(tranId, form.getRefundAmount(), up.getUID(), request.getContextPath());

				TranAction ta = new TranAction();

				int debitAccount = Integer.parseInt(form.getDebitAcct());

				ta.setTranId(tranId);
				ta.setDrGLAcctId(debitAccount);
				ta.setCrGLAcctId(2);
				ta.setTranPostAmt(new BigDecimal(form.getRefundAmount()));
				ta.setTranReasDesc(EMoneyGramAdmApplicationConstants.glAccountCodeToAction.get(debitAccount));

				tm.insertGLAdjustment(ta,tran.getSndAgentId(),up.getUID());

				ta.setDrGLAcctId(2);
				ta.setCrGLAcctId(1);
				ta.setTranReasDesc("CC Refund");

				tm.insertGLAdjustment(ta,tran.getSndAgentId(),up.getUID());

				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("partial.refund.successful", form.getTranId()));
				saveErrors(request, errors);

			} catch (EMGRuntimeException e) {

				if(e.getRootCause() instanceof CreditCardRefundException) {
					CreditCardRefundException refundException = (CreditCardRefundException) e.getRootCause();
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("partial.refund.failed",
							refundException.getCode(), refundException.getDescription() ));
					saveErrors(request, errors);
				}
				else {
					throw new EMGRuntimeException(e);
				}

			} catch (Exception e) {
				throw new EMGRuntimeException(e);
			}
		}

		String tranStatus = form.getTranStatusCode();
		String fundStatus = form.getTranSubStatusCode();
		boolean changeStatus = false;

		List<String> gea = null; // some service methods return a list of error
		// strings to append as global errors. This keeps Struts out of the
		// service.
		// perform transaction update only if confirm is O.K.
		if ("ok".equalsIgnoreCase(confirmAction)) { // perform update only if
			// the transaction taken is
			// valid
			if (isTokenValid(request)) {

				String IPLookup_MDValue = dynProps.getIPLookup_MD();
				if (IPLookup_MDValue != null && IPLookup_MDValue.equals("manualtxns"))
				{
					boolean isIpLookuponScoring = false;
					String comment = IPLocatorService.getFormattedStringForIpLookup(form.getTranCustIP(), isIpLookuponScoring);
					tm.setTransactionComment(Integer.parseInt(id), "OTH", comment, up.getUID());
				}

				try {
					if (!StringHelper.isNullOrEmpty(form.getSubmitApprove()) || !StringHelper.isNullOrEmpty(form.getSubmitApproveProcess())) {
						if (TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(form.getTranType())) {
							long tmpTime = System.currentTimeMillis();
							gea = tranService.approveDelayedSendTransaction(Integer.parseInt(form.getTranId()), up.getUID(), form
									.getTranStatusCode(), form.getTranSubStatusCode(), request.getContextPath());
							EventMonitor.getInstance().recordEvent(EventListConstant.DS_PROCESS, tmpTime, System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
							//release ownership if no errors so that automatic processing will pick this up
							if (errors.size() == 0) {
								tranService.releaseOwnership(Integer.parseInt(form.getTranId()), up.getUID());
								try {
									super.insertActivityLog(this.getClass(),request,null,
											UserActivityType.EVENT_TYPE_TRANSACTIONRELEASEOWNERSHIP,String.valueOf(tranId));
								} catch (Exception e) {}

							}
						} else {
							if (tranService.failCaliforniaCompliance(tranId, up.getUID())) {
								errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.california.compliance.failed"));
								saveErrors(request, errors);
							} else {
								changeTranStatus(tranId, TransactionStatus.APPROVED_CODE, form.getTranSubStatusCode(),
										up.getUID(), tm.getTransaction(tranId).getPartnerSiteId());
								form.setTranStatusCode(TransactionStatus.APPROVED_CODE);
							}
						}
					}
					if (errors.size() == 0
							&& (!StringHelper.isNullOrEmpty(form.getSubmitProcess()) || !StringHelper.isNullOrEmpty(form
									.getSubmitApproveProcess()))) {
						String tranType = form.getTranType();
						if (TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(tranType)) {
							long tmpTime = System.currentTimeMillis();
							gea = tranService.processExpressPayTransaction(Integer.parseInt(form.getTranId()), up.getUID(), form
									.getTranStatusCode(), form.getTranSubStatusCode(), request.getContextPath());
							EventMonitor.getInstance().recordEvent(EventListConstant.EP_PROCESS, tmpTime, System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
						} else if (TransactionType.MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(tranType)) {
							long tmpTime = System.currentTimeMillis();
							int transactionId = Integer.parseInt(form.getTranId());
							String transactionStatus = form.getTranStatusCode();
							String transactionSubStatus = form.getTranSubStatusCode();
							String contextPath = request.getContextPath();
							String UID = up.getUID();
							gea = tranService.processMoneyGramTransaction(transactionId, UID, transactionStatus, transactionSubStatus, contextPath);
							EventMonitor.getInstance().recordEvent(EventListConstant.P2P_PROCESS, tmpTime, System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
						} else if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(tranType)) {
							long tmpTime = System.currentTimeMillis();
							tranService.sendEconomyServicePendingEmail(tm.getTransaction(tranId),
									up.getUID());
							EventMonitor.getInstance().recordEvent(EventListConstant.ES_PROCESS, tmpTime, System.currentTimeMillis());
						} else if (TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(tranType)) {
							long tmpTime = System.currentTimeMillis();
							gea = tranService.processDelayedSendTransaction(Integer.parseInt(form.getTranId()), up.getUID(), form
									.getTranStatusCode(), form.getTranSubStatusCode(), request.getContextPath());
							EventMonitor.getInstance().recordEvent(EventListConstant.DS_PROCESS, tmpTime, System.currentTimeMillis());
							appendGlobalErrors(gea, errors);
						} else {
							throw new IllegalStateException("Unable to process transaction type (" + tranType + ")");
						}
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordMgCancelRefund())) {
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = form.getTranSubStatusCode();
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitUndoApprove())
							|| !StringHelper.isNullOrEmpty(form.getSubmitUndoDeny())) {
						List tmpList = getTranActions(request, tm, tranId, up.getUID(),form.getTransPartnerSiteId());
						tranStatus = TransactionStatus.FORM_FREE_SEND_CODE;
						if (tmpList.size() > 0) {
							TranActionView tav = (TranActionView) tmpList.get(tmpList.size() - 1);
							tranStatus = tav.getTranOldStatCode();
						}
						fundStatus = form.getTranSubStatusCode();
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchAutoRef())) {
						doAutoRefund(form, up, tm, errors, request, true);						
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchCxlRef())) {
						if (TransactionStatus.SENT_CODE.equals(form.getTranStatusCode())) {
							tranStatus = TransactionStatus.CANCELED_CODE;
							changeTranStatus(tranId, TransactionStatus.CANCELED_CODE, form.getTranSubStatusCode(), up.getUID(), null);
						} else {
							tranStatus = form.getTranStatusCode();
						}
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran, primaryAccountType);
						fundStatus = TransactionStatus.ACH_REFUND_REQUESTED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecAchRefundReq())) {
						tranStatus = form.getTranStatusCode();
						fundStatus = TransactionStatus.ACH_CANCEL_CODE;
						changeStatus = true;
			          } else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcAutoRef())) {
			              doAutoRefund(form, up, tm, errors, request, true);
						//doAutoRefund(form, up, tm, errors, request);
			          } else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcCxlRef())) {
			              if (TransactionStatus.SENT_CODE.equals(form.getTranStatusCode())) {
			            		//call the new method to change status. Testing for now
								changeTranStatusRefundManual(tranId, TransactionStatus.CANCELED_CODE, form.getTranSubStatusCode(), up.getUID(), null ,tm);
								//call the new method to change status. Testing for now
								changeTranStatusRefundManual(tranId, TransactionStatus.CANCELED_CODE, TransactionStatus.CC_REFUND_REQUESTED_CODE, up
									.getUID(), null, tm);
								tranStatus = TransactionStatus.CANCELED_CODE;
			  
						} else {
							changeTranStatusRefundManual(tranId, form.getTranStatusCode(), TransactionStatus.CC_REFUND_REQUESTED_CODE, up.getUID(), null, tm);
							tranStatus = form.getTranStatusCode();
						}
			            //Now do the CC funding reversal
			            doAutoRefund(form, up, tm, errors, request, false); //false = don't do Mainframe reversal

						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
						//Call again the service for the transaction according the pymt_prvd_srv_code
						tranService = ServiceFactory.getInstance().getTransactionServicePymtProvider(tran.getTCProviderCode(), tran.getPartnerSiteId());
						tranService.refundPersonToPersonSuccessEmail(tran, primaryAccountType);
						/*fundStatus = TransactionStatus.CC_CANCEL_CODE;
						// set the transaction service again for another process
						tranService = ServiceFactory.getInstance().getTransactionService();
						changeStatus = true;*/
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecCcRefundReq())) {
						tranService.doCreditCardReversal(tranId, up.getUID(), request.getContextPath());
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoChargebackAndWriteOffLoss())) {
						if (form.getTranSubStatusCode().equals(TransactionStatus.ACH_SENT_CODE)) {
							fundStatus = TransactionStatus.ACH_RETURNED_CODE;
						} else {
							fundStatus = TransactionStatus.CC_CHARGEBACK_CODE;
						}
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordAchChargeback())) {
						if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(form.getTranType())) {
							tranService.sendEconomyServiceAchReturnEmail(
									tm.getTransaction(tranId), up.getUID());
						} else {
							tranStatus = form.getTranStatusCode();
							fundStatus = TransactionStatus.ACH_RETURNED_CODE;
							changeStatus = true;
						}
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordCcChargeback())) {
						tranStatus = form.getTranStatusCode();
						fundStatus = TransactionStatus.CC_CHARGEBACK_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelAchWaiting())) {
						changeTranStatus(tranId, TransactionStatus.CANCELED_CODE, TransactionStatus.ACH_WAITING_CODE, up.getUID(), null);
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran, primaryAccountType);
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.ACH_CANCEL_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitManWriteOffLoss())) {
						tranStatus = TransactionStatus.MANUAL_CODE;
						fundStatus = TransactionStatus.MANUAL_SUB_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoWriteOffLoss())) {
						fundStatus = TransactionStatus.WRITE_OFF_LOSS_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitEsMGSend())) {
						List<String> erl = tranService.processEconomyServiceTransaction(tranId, up.getUID(), form.getTranStatusCode(), form
								.getTranSubStatusCode(), request.getContextPath());
						appendGlobalErrors(erl, errors);
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitEsMGSendCancel())) {
						changeTranStatus(tranId, TransactionStatus.CANCELED_CODE, TransactionStatus.ACH_SENT_CODE, up.getUID(), null);
						Transaction tran = tm.getTransaction(tranId);
						ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
						tranService.refundPersonToPersonSuccessEmail(tran, primaryAccountType);
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.ACH_REFUND_REQUESTED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRecordTranError())) {
						tranStatus = TransactionStatus.ERROR_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitReleaseTransaction())) {
						tm.setTransactionComment(tranId, "OTH", "Release transaction from 'Verified By Visa In Process'.", up.getUID());
						fundStatus = TransactionStatus.NOT_FUNDED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitRetrievalRequest())) {
						Transaction tran = tm.getTransaction(tranId);
						tran.setFundRetrievalRequestDate(new Date(System.currentTimeMillis()));
						tm.updateTransaction(tran, up.getUID());
						changeStatus = false;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitCancelBankRqstRefund())) {
						//Handle the non automated requestrefund (no MF reversal)
						Transaction tran = tm.getTransaction(tranId);

						//Now do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(Integer.parseInt(form.getTranId()), up.getUID(), request.getContextPath());
						EventMonitor.getInstance().recordEvent(EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT, tmpTime, System.currentTimeMillis());

						//Change 1st status (SEN/BKS -> SEN/BRR, SEN/BKR -> CXL/BKR)
						if (tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
							tran.setTranSubStatCode(TransactionStatus.BANK_REFUND_REQUEST_CODE);
						} else {
							tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						}
						tran.setTranStatDate(new Date());
						tm.updateTransaction(tran, up.getUID());

						//Set up txn state transition for SEN/BRR status
						changeStatus = false;
						if (tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)
							&& tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
							tranStatus = TransactionStatus.CANCELED_CODE;
							fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
							changeStatus = true;
						}
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitAutoCancelBankRqstRefund())) {
						//Handle the automated request refund (will send MF reversal)

						//send Reversal to Mainframe via AgentConnect
						doAutoRefund(form, up, tm, errors, request, true);

						if (errors.isEmpty()) {
							//Now do the Bank reversal/refund
							long tmpTime = System.currentTimeMillis();
							tranService.doBankReversal(Integer.parseInt(form.getTranId()), up.getUID(), request.getContextPath());
							EventMonitor.getInstance().recordEvent(EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT, tmpTime, System.currentTimeMillis());

							//Set up txn state transition for SEN/BRR status
							Transaction tran = tm.getTransaction(tranId);
							changeStatus = false;
							if (tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)
								&& tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REFUND_REQUEST_CODE)) {
								tranStatus = TransactionStatus.CANCELED_CODE;
								fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
								changeStatus = true;
							}
						}
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRqstRefund())) {
						//Handle a request for bank refund
						Transaction tran = tm.getTransaction(tranId);

						//Change 1st status (APP/BKS -> CXL/BKS)
						tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
						tran.setTranStatDate(new Date());
						tm.updateTransaction(tran, up.getUID());

						//Now do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(Integer.parseInt(form.getTranId()), up.getUID(), request.getContextPath());
						EventMonitor.getInstance().recordEvent(EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT, tmpTime, System.currentTimeMillis());

						//Set up txn state transition to CXL/BRR
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRejectedDebit())) {
						Transaction tran = tm.getTransaction(tranId);
						//Set up txn state transition to SEN/BKR
						if (tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)) {
							tranStatus = TransactionStatus.SENT_CODE;
						} else {
							//Do a bank refund/reversal if in CXL/BRR state
							long tmpTime = System.currentTimeMillis();
							tranService.doBankReversal(Integer.parseInt(form.getTranId()), up.getUID(), request.getContextPath());
							EventMonitor.getInstance().recordEvent(EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT, tmpTime, System.currentTimeMillis());
							tranStatus = TransactionStatus.CANCELED_CODE;
						}
						fundStatus = TransactionStatus.BANK_REJECTED_CODE;
						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRepresentRejDebit())) {
						Transaction tran = tm.getTransaction(tranId);

						if (tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.CANCELED_CODE) &&
							tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_REJECTED_CODE)) {
							//Change 1st status (CXL/BKR -> CXL/BKS)
							tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
							tran.setTranSubStatCode(TransactionStatus.BANK_SETTLED_CODE);
							tran.setTranStatDate(new Date());
							tm.updateTransaction(tran, up.getUID());

							//set up for 2nd state transition
							tranStatus = TransactionStatus.CANCELED_CODE;
							fundStatus = TransactionStatus.BANK_REFUND_REQUEST_CODE;
						} else { //SEN/BKR -> SEN/BKS
							tranStatus = TransactionStatus.SENT_CODE;
							fundStatus = TransactionStatus.BANK_SETTLED_CODE;
						}

						changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankWriteOffLoss())) {
						//Set up txn state transition to SEN/BWL
						tranStatus = TransactionStatus.SENT_CODE;
						fundStatus = TransactionStatus.BANK_WRITE_OFFLOSS_CODE;
						changeStatus = true;

					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankRecordVoid())) {
						//Set up txn state transition to CXL/BKV
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_VOID_CODE;
						changeStatus = true;

					} else if (!StringHelper.isNullOrEmpty(form.getSubmitBankIssueRefund())) {
						//Handle a request for issuing bank refund

						//Do the Bank reversal/refund
						long tmpTime = System.currentTimeMillis();
						tranService.doBankReversal(Integer.parseInt(form.getTranId()), up.getUID(), request.getContextPath());
						EventMonitor.getInstance().recordEvent(EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT, tmpTime, System.currentTimeMillis());

						//Set up txn state transition to CXL/BRR
						tranStatus = TransactionStatus.CANCELED_CODE;
						fundStatus = TransactionStatus.BANK_CANCEL_CODE;
						changeStatus = true;
					}  else if (!StringHelper.isNullOrEmpty(form.getSubmitCcManualRefund())) {
			            //Set up txn state transition to CXL/CCC
			            fundStatus = TransactionStatus.CC_CANCEL_CODE;
			            changeStatus = true;
					} else if (!StringHelper.isNullOrEmpty(form.getSubmitCcManualVoid())) {
			            //Set up txn state transition to CXL/CCV
			            fundStatus = TransactionStatus.CC_VOID_CODE;
			            changeStatus = true;
				    } else if (!StringHelper.isNullOrEmpty(form.getSubmitUndoDSError())) {
			            //Set up txn state transition to APP/BKS
			            tranStatus = TransactionStatus.APPROVED_CODE;
			            fundStatus = TransactionStatus.BANK_SETTLED_CODE;
			            changeStatus = true;
			          } 

					if (changeStatus) {
						Transaction oldTran = tm.getTransaction(tranId);
						if (oldTran.getTranStatCode().equalsIgnoreCase(tranStatus)
								&& oldTran.getTranSubStatCode().equalsIgnoreCase(fundStatus)) {
							errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.transaction.token.invalid"));
							saveErrors(request, errors);
						} else {
							tranService.setTransactionStatus(tranId, tranStatus, fundStatus, up.getUID());
							if (!StringHelper.isNullOrEmpty(form.getSubmitAutoChargebackAndWriteOffLoss())) {
								tranService.setTransactionStatus(tranId, tranStatus, TransactionStatus.WRITE_OFF_LOSS_CODE, up.getUID());
							}
						}
					}
				} catch (TransactionAlreadyInProcessException e) {
					String[] parm = { id, e.getCurrentOwner() };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.already.actively.being.processed", parm));
					saveErrors(request, errors);
				} catch (TransactionOwnershipException e) {
					String[] parm = { e.getCurrentOwner(), id };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.already.owned.by.other.user", parm));
					saveErrors(request, errors);
				} catch (InvalidRRNException e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("exception.invalid.param.value", e.getRRN(), "Receiver RRN"));
					saveErrors(request, errors);
				}
			} else {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.transaction.token.invalid"));
				saveErrors(request, errors);
			}
		}
		Transaction trans = tm.getTransaction(tranId);
		if (trans == null) {
			throw new EMGRuntimeException("Transaction " + tranId + " no longer exists");
		}
		// get the transaction
		addToQuickList(request, "tranList", String.valueOf(tranId), 8);
		form.setTranId(trans.getEmgTranId() + "");
		form.setCustId(String.valueOf(trans.getCustId()));
		form.setTranStatusCode(trans.getTranStatCode());
		form.setTranStatusDesc(trans.getTranStatDesc());
		form.setTranSubStatusCode(trans.getTranSubStatCode());
		form.setTranSubStatusDesc(trans.getTranSubStatDesc());
		form.setTranLgcyRefNbr(trans.getLgcyRefNbr());
		if (trans.getReceiptTextInfoEng() != null) {
			request.getSession().setAttribute(Transaction.RCPT_TXT_INFO_SESSION,
					trans.getReceiptTextInfoEng());
		} else {
			request.getSession().removeAttribute(Transaction.RCPT_TXT_INFO_SESSION);
		}
		if (trans.getInternetPurchase() == null) {
			form.setInetPurchFlag("Not Available");
		} else if (trans.getInternetPurchase().equalsIgnoreCase("Y")) {
			form.setInetPurchFlag("Yes");
		} else if (trans.getInternetPurchase().equalsIgnoreCase("N")) {
			form.setInetPurchFlag("No");
		}
		//ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(trans.getSndAcctTypeCode());
		if (trans.getTranRvwPrcsCode() == null) {
			form.setTranRvwPrcsCode("MAN");
		} else {
			form.setTranRvwPrcsCode(trans.getTranRvwPrcsCode());
		}
		//
		if ((trans.getLgcyRefNbr() != null) && EMTAdmDynProperties.getDPO(null).isDisplayMoneyGramDetails())
			form.setDisplayMoneygramDetails(true);
		else
			form.setDisplayMoneygramDetails(false);
		form.setTranOwnerId(trans.getCsrPrcsUserid());
		form.setTranSenderName(trans.getSndCustFrstName() + " " + trans.getSndCustLastName());
		form.setTranSenderSecondLastName(trans.getSndCustScndLastName());
		if (trans.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
			form.setTranReceiverName(trans.getRcvAgcyCode());
			form.setTranRcvAgcyAcctMask("*****" + trans.getRcvAgcyAcctMask());
		} else {
			form.setTranReceiverName(trans.getRcvCustFrstName() + " " + trans.getRcvCustLastName());
			form.setRcvCustFrstName(trans.getRcvCustFrstName());
			form.setRcvCustLastName(trans.getRcvCustLastName());
		}
		if(trans.getRcvAcctMaskNbr()!=null){			
			if((trans.getDlvrOptnId()==DeliveryOption.CARD_DEPOSIT_ID || trans.getDlvrOptnId()==DeliveryOption.BANK_DEPOSIT_ID)&&
					!(trans.getRcvAgentId()==Constants.PHL_SMARTMONEY_AGENTID))
				form.setRcvAcctMaskNbr(Constants.ACCT_MASK +trans.getRcvAcctMaskNbr());
			else
				form.setRcvAcctMaskNbr(trans.getRcvAcctMaskNbr());				
		}else{
			form.setRcvAcctMaskNbr(Constants.STR_EMPTY);
		}

		String currency = " " + EPartnerSite.getPartnerByPartnerSiteId(trans.getPartnerSiteId()).getPartnerSiteCurrency(); 
		String receiveCurrency = (!trans.getRcvISOCrncyId().equals(
				trans.getRcvPayoutISOCrncyId())) ? trans
				.getRcvPayoutISOCrncyId() : trans.getRcvISOCrncyId();

		form.setTranFaceAmt(df.format(trans.getSndFaceAmt() != null ? trans.getSndFaceAmt().doubleValue() : 0) + currency );
		form.setTranSendFee(df.format(trans.getSndFeeAmt() != null ? trans.getSndFeeAmt().doubleValue() : 0) + currency);
		form.setTranReturnFee(df.format(trans.getRtnFeeAmt() != null ? trans.getRtnFeeAmt().doubleValue() : 0) + currency);
		form.setTranSendAmt(df.format(trans.getSndTotAmt() != null ? trans.getSndTotAmt().doubleValue() : 0) + currency);
		form.setExchangeRate(rateNumberFormat.format(trans.getSndFxCnsmrRate() != null ? trans.getSndFxCnsmrRate().doubleValue() : 0));
		form.setOtherFees(df.format(trans.getRcvNonMgiFeeAmt()!=null ? trans.getRcvNonMgiFeeAmt():0)+ " " + ((receiveCurrency!=null && receiveCurrency.trim().length()>0)? receiveCurrency : currency));
		form.setOtherTaxes(df.format(trans.getRcvNonMgiTaxAmt()!=null ? trans.getRcvNonMgiTaxAmt():0)+ " " + ((receiveCurrency!=null && receiveCurrency.trim().length()>0)? receiveCurrency : currency));
		form.setTotalReceiveAmount(df.format(trans.getRcvPayoutAmt()!=null ? trans.getRcvPayoutAmt():0)+ " " + ((receiveCurrency!=null && receiveCurrency.trim().length()>0)? receiveCurrency : currency));

		form.setSndThrldWarnAmt(df.format(trans.getSndThrldWarnAmt().doubleValue()));
		form.setSndFeeAmtNoDiscountAmt(df.format(trans.getSndFeeAmtNoDiscountAmt() != null ? trans.getSndFeeAmtNoDiscountAmt().doubleValue(): 0)  + currency);

		form.setOverThrldAmt(false);
		if (trans.getSndThrldWarnAmt().doubleValue() != 0) {
			form.setOverThrldAmt(trans.getSndFaceAmt().doubleValue() > trans.getSndThrldWarnAmt().doubleValue());
		}
		form.setTranType(trans.getEmgTranTypeCode());
		form.setTranTypeDesc(trans.getEmgTranTypeDesc());
		form.setTranSendCountry(trans.getSndISOCntryCode());
		form.setTranSendCurrency(trans.getSndISOCrncyId());
		form.setTranRecvCountry(trans.getRcvISOCntryCode());
		form.setTranPrimAcctType(trans.getSndAcctTypeCode());
		form.setTranPrimAcctMask(trans.getSndAcctMaskNbr());
		form.setTranSecAcctType(trans.getSndBkupAcctTypeCode());
		form.setTranSecAcctMask(trans.getSndBkupAcctMaskNbr());
		form.setTranCustIP(trans.getSndCustIPAddrId());
		form.setTranSndMsg1Text(trans.getSndMsg1Text());
		form.setTranSndMsg2Text(trans.getSndMsg2Text());
		form.setSendCustAcctId(String.valueOf(trans.getSndCustAcctId()));
		form.setSendCustBkupAcctId(String.valueOf(trans.getSndCustBkupAcctId()));
		form.setTransScores(trans.getTransScores());
		form.setTranRiskScore(trans.getTranRiskScore());
		form.setTranRiskScoreDefaulted(trans.isTranRiskScoreDefaulted());
		form.setSysAutoRsltCode(trans.getSysAutoRsltCode());
		form.setScoreRvwCode("");
		form.setTranScoreCmntText("");
		if (trans.getTranScore() != null) {
			form.setScoreRvwCode(trans.getTranScore().getScoreRvwCode());
			form.setTranScoreCmntText(trans.getTranScore().getTranScoreCmntText());
		}
		String deliveryOptionName = null;
		try {
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo delOptInfo = null;
			if (trans.getDlvrOptnId() == 15) { // FIXME Hardcoded for delivery option 15 (WAP)
				delOptInfo = new com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo();
				delOptInfo.setDeliveryOptionID(new BigInteger("15"));
				delOptInfo.setDeliveryOption("ONLY_AT");
				delOptInfo.setDeliveryOptionName("ONLY_AT");
			} else {
				delOptInfo = acs.getDeliveryOptionInfo(trans.getDlvrOptnId());
			}
			deliveryOptionName = delOptInfo.getDeliveryOption();
		} catch (Exception ignore) {
			//
		}
		if (deliveryOptionName == null) {
			deliveryOptionName = "";
		}
		form.setDeliveryOptionName(deliveryOptionName);
		form.setRcvRRN(trans.getRRN());
		if(trans.getRcvPayoutISOCrncyId() != null && !(trans.getRcvPayoutISOCrncyId().equals("")))
		{
			form.setTranRecvCurrency(trans.getRcvPayoutISOCrncyId());
		} else {
			form.setTranRecvCurrency(trans.getRcvISOCrncyId());
		}
		String currencyName = "";
		try {
			
//			CurrencyInfo ci = acs.getCurrencyInfo(trans.getRcvISOCrncyId());
			CurrencyInfo ci = acs.getCurrencyInfo(trans.getRcvPayoutISOCrncyId());
			currencyName = ci.getCurrencyName();
		} catch (Exception ignore) {
			//
		}
		if (currencyName == null)
			currencyName = "";
		form.setTranRecvCurrencyName(currencyName);
		form.setRcvCustMatrnlName(trans.getRcvCustMatrnlName());
		form.setRcvCustMidName(trans.getRcvCustMidName());
		form.setRcvCustAddrStateName(trans.getRcvCustAddrStateName());
		form.setRcvAgentRefNbr(trans.getRcvAgentRefNbr());
		form.setRcvCustAddrLine1Text(trans.getRcvCustAddrLine1Text());
		form.setRcvCustAddrLine2Text(trans.getRcvCustAddrLine2Text());
		form.setRcvCustAddrLine3Text(trans.getRcvCustAddrLine3Text());
		form.setRcvCustAddrCityName(trans.getRcvCustAddrCityName());
		form.setRcvCustAddrPostalCode(trans.getRcvCustAddrPostalCode());
		form.setRcvCustDlvrInstr1Text(trans.getRcvCustDlvrInstr1Text());
		form.setRcvCustDlvrInstr2Text(trans.getRcvCustDlvrInstr2Text());
		form.setRcvCustDlvrInstr3Text(trans.getRcvCustDlvrInstr3Text());
		form.setRcvCustPhNbr(trans.getRcvCustPhNbr());
		form.setRcvAgtCity(trans.getRcvAgtCity());
		form.setRcvAgtState(trans.getRcvAgtState());
		form.setRcvAgtCntry(trans.getRcvAgtCntry());
		form.setRcvDate(null);
		form.setRetrievalRequestDate(trans.getFundRetrievalRequestDate());
		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

		DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy hh:mm a z");
		dateFormat.setTimeZone(EPartnerSite.getPartnerByCountry(trans.getSndISOCntryCode()).getTimeZone());
		form.setCreateDate(dateFormat.format(trans.getCreateDate()));
		
		form.setSndCustOccupationText(trans.getSndCustOccupationText());
		//Changes for Defect 711 starts
		//form.setCustomerAutoEnrollFlag(trans.getCustomerAutoEnrollFlag());
		if (trans.getLoyaltyPgmMembershipId()!= null && !(trans.getLoyaltyPgmMembershipId().equals("")))
		{
			form.setCustomerAutoEnrollFlag("Y");
		}
		else
		{
			form.setCustomerAutoEnrollFlag("N");
		}
		//changes for Defect 711 ends
		form.setTestQuestion(trans.getTestQuestion());
		form.setTestQuestionAnswer(trans.getTestQuestionAnswer());
		form.setLoyaltyPgmMembershipId(trans.getLoyaltyPgmMembershipId());
		form.setSndCustPhotoId(trans.getSndCustPhotoId());
		form.setSndCustPhotoIdCntryCode(trans.getSndCustPhotoIdCntryCode());
		form.setSndCustPhotoIdStateCode(trans.getSndCustPhotoIdStateCode());
		form.setSndCustPhotoIdTypeCode(trans.getSndCustPhotoIdTypeCode());
		if(trans.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID)
				||trans.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID)){
			if(!StringUtility.isNullOrEmpty(form.getSndCustPhotoIdTypeCode())){
				if(EMoneyGramAdmApplicationConstants.photoIdStringToIdType.get(form.getSndCustPhotoIdTypeCode()).equals(EMoneyGramAdmApplicationConstants.photoIdTypeDriverLicense)
						||EMoneyGramAdmApplicationConstants.photoIdStringToIdType.get(form.getSndCustPhotoIdTypeCode()).equals(EMoneyGramAdmApplicationConstants.photoIdTypeStateId)){
					form.setSndCustPhotoIdStateCode(EMoneyGramAdmApplicationConstants.photoIdState);
					form.setSndCustPhotoIdStateCode(trans.getSndCustPhotoIdStateCode());
				} else {
					form.setSndCustPhotoIdStateCode("");
				}
			}
		} else {
		    if (!StringUtility.isNullOrEmpty(form.getSndCustPhotoIdTypeCode())){
		        String photoIdTypeCode =
		            EMoneyGramAdmApplicationConstants
		                .photoIdStringToIdType
		                .get(form.getSndCustPhotoIdTypeCode());
		        if (photoIdTypeCode.equals(
		                EMoneyGramAdmApplicationConstants.photoIdTypeAlienId) ||
		            photoIdTypeCode.equals(
		                EMoneyGramAdmApplicationConstants.photoIdTypePassport) ||
		            photoIdTypeCode.equals(
		                EMoneyGramAdmApplicationConstants.photoIdTypeGovernmentId)) {
	                form.setSndCustPhotoIdStateCode("");
		        }
		    }
		}
		form.setSndCustPhotoIdExpDate(trans.getSndCustPhotoIdExpDate());
		String legalId = trans.getSndCustLegalId();
		/*if(trans.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {
			legalId = decryptService.decryptSSN(legalId);
		}*/
		
		form.setSndCustLegalId(legalId);
		form.setSndCustLegalIdTypeCode(trans.getSndCustLegalIdTypeCode());
		form.setPartnerSiteId((String) request.getParameter("partnerSiteId"));
		form.setTransPartnerSiteId(trans.getPartnerSiteId());
		if (StringHelper.isNullOrEmpty(trans.getMccPartnerProfileId())) {
			form.setMccPartnerProfileId("N/A");
		} else {
			//form.setMccPartnerProfileId(EMoneyGramMerchantIds.getMerchantMap().get(trans.getMccPartnerProfileId()));
			form.setMccPartnerProfileId(trans.getMccPartnerProfileId());
		}
		if (trans.getRcvDate() != null) {
			form.setRcvDate(df.format(trans.getRcvDate()));
		}
		CustAddress ca = tm.getCustAddress(trans.getSndCustAddrId());
		if (ca != null) {
			form.setTranSendCity(ca.getAddrCityName());
			form.setTranSendState(ca.getAddrStateName());
		}
		if (tm.getVBVBeans(tranId).size() != 0) {
			request.getSession().setAttribute("vbvExist", "y");
		} else {
			request.getSession().removeAttribute("vbvExist");
		}
		// setup application scope comment reason codes
		ServletContext sc = request.getSession().getServletContext();
		if (sc.getAttribute("reasons") == null) {
			sc.setAttribute("reasons", tm.getTransactionCommentReasons());
		}
		// get transaction actions
		request.getSession().setAttribute("tranActions", getTranActions(request, tm, tranId, up.getUID(),form.getTransPartnerSiteId()));
		// get transaction comments in session scope
		request.getSession().setAttribute("comments", tm.getTransactionComments(tranId));
		// set message for ACH Refund Requested type of fund status
		if (TransactionStatus.ACH_REFUND_REQUESTED_CODE.equals(trans.getTranSubStatCode())) {
			setARRMsg(request, trans);
		}
		// transaction security matrix setup in session scope
		TranSecurityMatrix matrix = new TranSecurityMatrix(request, up, trans, recoveredAmt);
		// see if we should look for a chargeback tracking record or allow
		// creation of one
		// must be in appropriate funding status.
		// boolean chgBckTrackingAllowed = false;
		String chgBckTrackingViewInd = "VIEW";
		String chargeBackTrackingHyperLink = null;
		// if the user has permission and it's not ESSEND, which doesn't allow
		// CC funding,
		// then check to see if Tracking record exists, if so provide a link to
		// it, else
		// if the trans is in a funding status that allows a Tracking record,
		// then provide
		// a link to add the Tracking record
		if ((up.hasPermission("ccChargebackTracking"))
				&& (!trans.getEmgTranTypeCode().equals(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE))) {
			// chgBckTrackingAllowed = true;
			// see if an record already exists
			ChargebackTransactionHeader cbth = null;
			try {
				cbth = tm.getChargebackTransactionHeader(up.getUID(), tranId);
			} catch (Exception e) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Error attempting to get ChargebackHeader", e);
			}
			if (cbth != null) {
				chargeBackTrackingHyperLink = "ccChargebackTracking.do?tranId=" + tranId;
				chgBckTrackingViewInd = "VIEW";
			} else if ((trans.getTranSubStatCode().equals(TransactionStatus.CC_CHARGEBACK_CODE))
					|| (trans.getTranSubStatCode().equals(TransactionStatus.CC_SALE_CODE))
					|| (trans.getTranSubStatCode().equals(TransactionStatus.RECOVERY_OF_LOSS_CODE))
					|| (trans.getTranSubStatCode().equals(TransactionStatus.RECOVERY_OF_LOSS_CYBERSOURCE_CODE))
					|| (trans.getTranSubStatCode().equals(TransactionStatus.WRITE_OFF_LOSS_CODE))) {
				chargeBackTrackingHyperLink = "ccChargebackTracking.do?tranId=" + tranId;
				chgBckTrackingViewInd = "ADD";
			}
		}
		if (up.hasPermission("showTranScoreDetail")) {
			request.setAttribute("showTranScoreDetail", "Y");
		}
		request.getSession().setAttribute("chargeBackTrackingHyperLink", chargeBackTrackingHyperLink);
		request.getSession().setAttribute("chgBckTrackingViewInd", chgBckTrackingViewInd);
		request.getSession().setAttribute("tranAuth", matrix);
		form.setEsMGSendFlag(matrix.getEsSendMG());
		
		// for story 4051.
		if (firstTimePageLoad) {
			if (form.getConsumerCountryCode() != null
					&& form.getTranSendCountry() != null
					&& form.getConsumerCountryCode().equalsIgnoreCase(
							form.getTranSendCountry())) {
				form.setCountrySameFlag(true);
			} else {
				form.setCountrySameFlag(false);
			}
		}
		// remove transaction ownership if the user checked the release
		// option at the confirmation screen
		if (request.getAttribute("action") != null && request.getAttribute("actionDone") == null) {
			request.setAttribute("actionDone", "Y");
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANS_OWNERSHIP);
		} else {
			saveToken(request);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}
	}

	private void doAutoRefund(TransactionDetailForm form, UserProfile up, TransactionManager tm, ActionErrors errors,
			HttpServletRequest request,boolean cancelMainframe) throws TransactionAlreadyInProcessException, TransactionOwnershipException, DataSourceException,
			SQLException {

		Transaction tmp = tm.getTransaction(Integer.parseInt(form.getTranId()));
		TransactionService tranService = ServiceFactory.getInstance()
				.getTransactionServicePymtProvider(tmp.getTCProviderCode(),
						tmp.getPartnerSiteId());

		int tranId = Integer.parseInt(form.getTranId());
		if (cancelMainframe) {
			try {
				tranService.refundPersonToPerson(tranId, up.getUID());
			} catch (AgentConnectException e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.agent.connect.failed"));
				saveErrors(request, errors);
				return;
			} catch (EMGRuntimeException rt){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.cancel.mainframe.failed"));
				saveErrors(request, errors);
				return;
			}
		}
		if (form.getTranSubStatusCode().equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) {
			tranService.doCreditCardReversal(tranId, up.getUID(), request.getContextPath());
		}
	}

	private List getTranActions(HttpServletRequest request, TransactionManager tm, int tranId, String logonId, String partnerSiteID) throws DataSourceException,
			SQLException {
		
		float amount = 0F;
		TranAction appTranAction = null;
		TranAction senTranAction = null;
		List tas = tm.getTranActions(tranId, logonId);
		List tavs = new ArrayList();
		Iterator iter = tas.iterator();
		while (iter.hasNext()) {
			TranAction ta = (TranAction) iter.next();
			if (ta.getTranStatCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)
					&& ta.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.RECOVERY_OF_LOSS_CODE)) {
				amount = +ta.getTranPostAmt().floatValue();
			}
			TranActionView tav = new TranActionView();
			tav.setTranActionId(ta.getTranActionId());
			tav.setTranId(ta.getTranId());
			//MGO-3313: When Action is Agent AR/AP transaction description should be Refund
			if(ta.getDrGLAcctId()==EMoneyGramAdmApplicationConstants.GL_ACCOUNT_CODE_AGENT_AR_AP &&
					ta.getTranReasDesc().equals("Fee Refund")){
				tav.setTranReasDesc("Refund");
			}else{
				tav.setTranReasDesc(ta.getTranReasDesc());
			}
			tav.setTranReasCode(ta.getTranReasCode());
			tav.setTranActnDate(ta.getTranActionDate());
			tav.setTranOldStatCode(ta.getTranOldStatCode());
			tav.setTranOldSubStatCode(ta.getTranOldSubStatCode());
			tav.setTranStatCode(ta.getTranStatCode());
			tav.setTranSubStatCode(ta.getTranSubStatCode());
			tav.setTranConfId(ta.getTranConfId());
			tav.setPartnerSiteId(partnerSiteID);
			tav.setTranPostAmt(ta.getTranPostAmt());
			tav.setTranDrGlAcctId(ta.getDrGLAcctId());
			tav.setTranCrGlAcctId(ta.getCrGLAcctId());
			if (StringHelper.isNullOrEmpty(ta.getTranCreateUserid()) || ta.getTranCreateUserid().equalsIgnoreCase("java")) {
				tav.setTranCreateUserid("emgwww");
			} else {
				tav.setTranCreateUserid(ta.getTranCreateUserid());
			}
			tav.setTranDrGlAcctDesc(ta.getDrGLAcctDesc());
			tav.setTranCrGlAcctDesc(ta.getCrGLAcctDesc());
			tav.setSubReasonCode(ta.getSubReasonCode());
			tav.setTranSubReasDesc(ta.getTranSubReasDesc());
			tav.setTranReasTypeCode(ta.getTranReasTypeCode());
			tavs.add(tav);
			if (TransactionStatus.APPROVED_CODE.equals(ta.getTranStatCode())
					&& TransactionStatus.ACH_SENT_CODE.equals(ta.getTranSubStatCode())) {
				appTranAction = ta;
			}
			if (TransactionStatus.SENT_CODE.equals(ta.getTranStatCode()) && TransactionStatus.ACH_SENT_CODE.equals(ta.getTranSubStatCode())) {
				senTranAction = ta;
			}
		}
		if (tavs.size() > 1) {
			Collections.sort(tavs);
		}
		recoveredAmt = new BigDecimal(Math.round(amount * 100F) / 100F);
		request.setAttribute("appTranAction", appTranAction);
		request.setAttribute("senTranAction", senTranAction);
		return tavs;
	}

	private void changeTranStatus(int tranId, String tranStatus, String fundStatus, String userId, String partnerSiteId)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(partnerSiteId);
		tranService.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
	}

	/**
	 * This method make the manual record for not same day cancellation
	 *
	 * @param tranId
	 * @param tranStatus
	 * @param fundStatus
	 * @param userId
	 * @param partnerSiteId
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 */
	private void changeTranStatusRefundManual(int tranId, String tranStatus, String fundStatus, String userId, String partnerSiteId, TransactionManager tm)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		//Get the transaction
		Transaction transaction = tm.getTransaction(tranId);
		//Create the transaction service according the pymt_prvdr_srv_code
		TransactionService tranService = ServiceFactory.getInstance().getTransactionServicePymtProvider(transaction.getTCProviderCode(), transaction.getPartnerSiteId());
		//Set the transaction status for the transaction
		tranService.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
	}

	private List buildSubReasonList(String subReasonType) throws Exception {
		try {
			List subReasons = emgshared.services.ServiceFactory.getInstance().getTransSubReasonService().getTranSubReasonsForTypeCode(
					subReasonType);
			List tranSubReasons = new ArrayList();
			for (Iterator i = subReasons.iterator(); i.hasNext();) {
				TranSubReason tsr = (TranSubReason) i.next();
				tranSubReasons.add(new LabelValueBean(tsr.getTranSubReasonDesc() + " - " + tsr.getTranSubReasonCode(), tsr
						.getTranSubReasonCode()));
			}
			return tranSubReasons;
		} catch (Exception e) {
			throw e;
		}
	}

	private void buildSubReasonListBox(HttpServletRequest request, String buildForStatusType) throws Exception {
		try {
			List subReasons = emgshared.services.ServiceFactory.getInstance().getTransSubReasonService().getTranSubReasonsForTypeCode(
					buildForStatusType);
			List tranSubReasons = new ArrayList();
			for (Iterator i = subReasons.iterator(); i.hasNext();) {
				TranSubReason tsr = (TranSubReason) i.next();
				tranSubReasons.add(new LabelValueBean(tsr.getTranSubReasonDesc() + " - " + tsr.getTranSubReasonCode(), tsr
						.getTranSubReasonCode()));
			}
			request.getSession().setAttribute("TranSubReasons", tranSubReasons);
		} catch (Exception e) {
			throw e;
		}
	}

	private void setARRMsg(HttpServletRequest request, Transaction trans) {
		if (!TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equals(trans.getEmgTranTypeCode())
				&& !TransactionType.MONEY_TRANSFER_SEND_CODE.equals(trans.getEmgTranTypeCode())) {
			return;
		}
		TranAction appTranAction = null;
		TranAction senTranAction = null;
		TranAction ta = null;
		if (request.getAttribute("appTranAction") != null) {
			appTranAction = (TranAction) request.getAttribute("appTranAction");
			ta = appTranAction;
			request.removeAttribute("appTranAction");
		}
		if (request.getAttribute("senTranAction") != null) {
			senTranAction = (TranAction) request.getAttribute("senTranAction");
			ta = senTranAction;
			request.removeAttribute("senTranAction");
		}
		// Cancel after money gram send, no message
		if (appTranAction != null && senTranAction != null) {
			return;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(ta.getTranActionDate());
		cal.add(Calendar.HOUR, -5);
		Date recordDate = DateFormatter.getAchSendDay(cal.getTime(), (Map) request.getSession().getServletContext().getAttribute(
				"holidayMap"), EMTAdmDynProperties.getDPO(null).getEsSendWaitDays());
		DateFormatter df = new DateFormatter("yyyyMMdd", true);
		String recDate = df.format(recordDate);
		if (df.format(new Date(System.currentTimeMillis())).compareTo(recDate) < 0) {
			cal.setTime(recordDate);
			int clearTime = EMTAdmDynProperties.getDPO(null).getEsClearTime();
			cal.set(Calendar.HOUR_OF_DAY, clearTime / 100);
			cal.set(Calendar.MINUTE, clearTime % 100);
			recordDate = cal.getTime();
			df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
			request.setAttribute("ARRDate", df.format(recordDate));
		}
	}

	private static IPDetails getIPDetailsFromLookupService(String ipAddress, boolean isIpLookuponScoring) {

		IPDetails ipDetails = new IPDetails();
		
		ipDetails = IPLocatorService.getIPDetailsFromLookupService(ipAddress, isIpLookuponScoring);
	
		return ipDetails;
	}
}
