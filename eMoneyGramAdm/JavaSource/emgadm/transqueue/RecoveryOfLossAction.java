package emgadm.transqueue;

import java.text.MessageFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.TransactionStatus;

/**
 * @author A131
 *
 */
public class RecoveryOfLossAction extends ManualAdjustmentBaseAction
{
	protected ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException
	{
//		ActionErrors errors = new ActionErrors();
		UserProfile userProfile = getUserProfile(request);

		TransactionManager tm = getTransactionManager(request);
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService();

		int tranId = Integer.parseInt(form.getTranId());
//		Transaction tran = tm.getTransaction(tranId);

//		List tas;
		try
		{
			tm.getTranActions(tranId, userProfile.getUID());
		} catch (Exception e)
		{
			throw new EMGRuntimeException(e);
		}

		float recoveredAmt = 0;
//		Iterator iter = tas.iterator();

		tranService.recoveryOfLoss(
				Integer.parseInt(form.getTranId()),
				Float.parseFloat(form.getAmount()),
				recoveredAmt,
				userProfile.getUID(),
				TransactionStatus.RECOVERY_OF_LOSS_CODE,
				form.getSubReasonCode());

		return mapping.findForward(FORWARD_SUCCESS);
	}

	protected void getCommandDescription(HttpServletRequest request)
	{
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.recovery.of.loss"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);
		
	}
}
