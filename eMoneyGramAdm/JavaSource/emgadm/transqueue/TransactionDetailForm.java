package emgadm.transqueue;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class TransactionDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String tranId;
	private String custId;
	private String tranStatusCode;
	private String tranStatusDesc;
	private String tranSubStatusCode;
	private String tranSubStatusDesc;
	private String tranLgcyRefNbr;
	private String tranOwnerId;
	private String tranSenderName;
	private String tranSenderSecondLastName;
	private String tranReceiverName;
	private String rcvCustFrstName;
	private String rcvCustLastName;
	private String tranRcvAgcyAcctMask;
	private String tranFaceAmt;
	private String tranSendFee;
	private String tranReturnFee;
	private String tranSendAmt;
	private String sndThrldWarnAmt;
	private boolean overThrldAmt;
	private String tranType;
	private String tranTypeDesc;
	private String tranSendCountry;
	private String tranSendCurrency;
	private String tranSendCity;
	private String tranSendState;
	private String tranRecvCountry;
	private String tranPrimAcctType;
	private String tranPrimAcctMask;
	private String tranSecAcctType;
	private String tranSecAcctMask;
	private String tranCustIP;
	private String tranSndMsg1Text;
	private String tranSndMsg2Text;
	private String sendCustAcctId;
	private String sendCustBkupAcctId;
	private String createDate;
	private String submitMoveToPending;
	private String submitApprove;
	private String submitApproveProcess;
	private String submitUndoApprove;
	private String submitDeny;
	private String submitUndoDeny;
	private String submitProcess;
	private String submitResubmitACH;
	private String submitCcSale;
	private String submitRecAchAutoRef;
	private String submitRecAchCxlRef;
	private String submitRecAchRefundReq;
	private String submitRecCcAutoRef;
	private String submitRecCcCxlRef;
	private String submitRecCcRefundReq;
	private String submitRecordAchChargeback;
	private String submitRecordCcChargeback;
	private String submitManWriteOffLoss;
	private String submitManCollectFee;
	private String submitManRefundFee;
	private String submitManReturnFunding;
	private String submitRecordLossAdjustment;
	private String submitRecordFeeAdjustment;
	private String submitRecordConsumerRefundOrRemittanceAdjustment;
	private String submitRecordMgActivityAdjustment;
	private String submitRecordMgCancelRefund;
	private String submitCancelAchWaiting;
	private String submitAutoWriteOffLoss;
	private String submitAutoChargebackAndWriteOffLoss;
	private String submitRecoveryOfLoss;
	private String submitEsMGSend;
	private String submitEsMGSendCancel;
	private String submitRecordTranError;
	private String submitReleaseTransaction;
	private String submitRetrievalRequest;
	private String submitCancelBankRqstRefund;
	private String submitBankRqstRefund;
	private String submitAutoCancelBankRqstRefund;
	private String submitBankRepresentRejDebit;
	private String submitBankRejectedDebit;
	private String submitBankRecordVoid;
	private String submitBankIssueRefund;
	private String submitCcPartialRefund;
	private String submitBankWriteOffLoss;
	private String submitBankRecoverLoss;
	private String submitExit;
	private String tranCmntReasCode;
	private String CmntText;
	private String submitAddComment;
	private String buttonName;
	private String releaseOwnership;
	private boolean displayMoneygramDetails;
	private String esMGSendFlag;
	private String rcvCustMatrnlName;
	private String rcvCustMidName;
	private String rcvCustAddrStateName;
	private String rcvAgentRefNbr;
	private String rcvCustAddrLine1Text;
	private String rcvCustAddrLine2Text;
	private String rcvCustAddrLine3Text;
	private String rcvCustAddrCityName;
	private String rcvCustAddrPostalCode;
	private String rcvCustDlvrInstr1Text;
	private String rcvCustDlvrInstr2Text;
	private String rcvCustDlvrInstr3Text;
	private String rcvCustPhNbr;
	private String rcvAgtCity;
	private String rcvAgtState;
	private String rcvAgtCntry;
	private String rcvDate;
	private int transScores;
	private String sysAutoRsltCode;
	private String scoreRvwCode;
	private String tranScoreCmntText;
	private String submitGenTranScore;
	private String inetPurchFlag;
	private String tranRvwPrcsCode;
	private Date retrievalRequestDate;
	private String rcvRRN;
	private String deliveryOptionName;
	private String tranRecvCurrency;
	private String tranRecvCurrencyName;
	private String customerAutoEnrollFlag;
	private String testQuestion;
	private String testQuestionAnswer;
	private String loyaltyPgmMembershipId;
	private String sndCustPhotoIdTypeCode;
	private String sndCustPhotoIdCntryCode;
	private String sndCustPhotoIdStateCode;
	private String sndCustPhotoId;
	private String sndCustLegalIdTypeCode;
	private String sndCustLegalId;
	private Date sndCustDOB;
	private String sndCustOccupationText;
	private String sndFeeAmtNoDiscountAmt;
	private String mccPartnerProfileId;
	private String partnerSiteId;
	private String transPartnerSiteId;
	private Integer tranRiskScore;
	private boolean tranRiskScoreDefaulted;
	private String telecheckAction;
	private String amount;
	private String fee;
	private double refundAmount;
	private String debitAcct;
	private String exchangeRate;
	private String otherFees;
	private String otherTaxes;
	private String totalReceiveAmount;
	private String submitCcManualRefund;
	private String submitCcManualVoid;
	private String submitUndoDSError;
	private Date sndCustPhotoIdExpDate;
	
	//For Req 4052
	private String consumerISP;
	private String consumerDomain;
	private String consumerCountry;
	private String consumerRegion;
	private String consumerCity;
	private String consumerZipCode;
	private long consumerLatitude;
	private long consumerLongitude;
	private String dispatch;
	private boolean countrySameFlag;
	private String consumerCountryCode;
	
	
	private String rcvAcctMaskNbr; 
	
	/**
	 * @return
	 */
	public String getTranId()
	{
		return tranId;
	}

	/**
	 * @param string
	 */
	public void setTranId(String string)
	{
		tranId = string;
	}

	/**
	 * @return
	 */
	public String getTranStatusCode()
	{
		return tranStatusCode == null ? "" : tranStatusCode;
	}

	/**
	 * @param string
	 */
	public void setTranStatusCode(String string)
	{
		tranStatusCode = string;
	}

	/**
	 * @return
	 */
	public String getTranStatusDesc()
	{
		return tranStatusDesc == null ? "" : tranStatusDesc;
	}

	/**
	 * @param string
	 */
	public void setTranStatusDesc(String string)
	{
		tranStatusDesc = string;
	}

	/**
	 * @return
	 */
	public String getTranSubStatusCode()
	{
		return tranSubStatusCode == null ? "" : tranSubStatusCode;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatusCode(String string)
	{
		tranSubStatusCode = string;
	}

	/**
	 * @return
	 */
	public String getTranSubStatusDesc()
	{
		return tranSubStatusDesc == null ? "" : tranSubStatusDesc;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatusDesc(String string)
	{
		tranSubStatusDesc = string;
	}

	/**
	 * @return
	 */
	public String getTranLgcyRefNbr()
	{
		return tranLgcyRefNbr == null ? "" : tranLgcyRefNbr;
	}

	/**
	 * @param string
	 */
	public void setTranLgcyRefNbr(String string)
	{
		tranLgcyRefNbr = string;
	}

	/**
	 * @return
	 */
	public String getTranOwnerId()
	{
		return tranOwnerId == null ? "" : tranOwnerId;
	}

	/**
	 * @param string
	 */
	public void setTranOwnerId(String string)
	{
		tranOwnerId = string;
	}

	/**
	 * @return
	 */
	public String getTranSenderName()
	{
		return tranSenderName == null ? "" : tranSenderName;
	}

	/**
	 * @param string
	 */
	public void setTranSenderName(String string)
	{
		tranSenderName = string;
	}

	/**
	 * @return
	 */
	public String getTranReceiverName()
	{
		return tranReceiverName == null ? "" : tranReceiverName;
	}

	/**
	 * @param string
	 */
	public void setTranReceiverName(String string)
	{
		tranReceiverName = string;
	}

	/**
	 * @return
	 */
	public String getTranFaceAmt()
	{
		return tranFaceAmt;
	}

	/**
	 * @param string
	 */
	public void setTranFaceAmt(String string)
	{
		tranFaceAmt = string;
	}

	/**
	 * @return
	 */
	public String getTranSendFee()
	{
		return tranSendFee;
	}

	/**
	 * @param string
	 */
	public void setTranSendFee(String string)
	{
		tranSendFee = string;
	}

	/**
	 * @return
	 */
	public String getTranReturnFee()
	{
		return tranReturnFee;
	}

	/**
	 * @param string
	 */
	public void setTranReturnFee(String string)
	{
		tranReturnFee = string;
	}

	/**
	 * @return
	 */
	public String getTranSendAmt()
	{
		return tranSendAmt;
	}

	/**
	 * @param string
	 */
	public void setTranSendAmt(String string)
	{
		tranSendAmt = string;
	}

	/**
	 * @return
	 */
	public String getTranType()
	{
		return tranType == null ? "" : tranType;
	}

	/**
	 * @param string
	 */
	public void setTranType(String string)
	{
		tranType = string;
	}

	/**
	 * @return
	 */
	public String getTranSendCountry()
	{
		return tranSendCountry == null ? "" : tranSendCountry;
	}

	/**
	 * @param string
	 */
	public void setTranSendCountry(String string)
	{
		tranSendCountry = string;
	}

	/**
	 * @return
	 */
	public String getTranRecvCountry()
	{
		return tranRecvCountry == null ? "" : tranRecvCountry;
	}

	/**
	 * @param string
	 */
	public void setTranRecvCountry(String string)
	{
		tranRecvCountry = string;
	}

	/**
	 * @return
	 */
	public String getTranPrimAcctType()
	{
		return tranPrimAcctType == null ? "" : tranPrimAcctType;
	}

	/**
	 * @param string
	 */
	public void setTranPrimAcctType(String string)
	{
		tranPrimAcctType = string;
	}

	/**
	 * @return
	 */
	public String getTranTypeDesc()
	{
		return tranTypeDesc == null ? "" : tranTypeDesc;
	}

	/**
	 * @param string
	 */
	public void setTranTypeDesc(String string)
	{
		tranTypeDesc = string;
	}

	/**
	 * @return
	 */
	public String getTranPrimAcctMask()
	{
		return tranPrimAcctMask == null ? "" : tranPrimAcctMask;
	}

	/**
	 * @param string
	 */
	public void setTranPrimAcctMask(String string)
	{
		tranPrimAcctMask = string;
	}

	/**
	 * @return
	 */
	public String getTranSecAcctType()
	{
		return tranSecAcctType == null ? "" : tranSecAcctType;
	}

	/**
	 * @param string
	 */
	public void setTranSecAcctType(String string)
	{
		tranSecAcctType = string;
	}

	/**
	 * @return
	 */
	public String getTranSecAcctMask()
	{
		return tranSecAcctMask == null ? "" : tranSecAcctMask;
	}

	/**
	 * @param string
	 */
	public void setTranSecAcctMask(String string)
	{
		tranSecAcctMask = string;
	}

	/**
	 * @return
	 */
	public String getTranCustIP()
	{
		return tranCustIP == null ? "" : tranCustIP;
	}

	/**
	 * @param string
	 */
	public void setTranCustIP(String string)
	{
		tranCustIP = string;
	}

	/**
	 * @return
	 */
	public String getSubmitApprove()
	{
		return submitApprove;
	}

	/**
	 * @param string
	 */
	public void setSubmitApprove(String string)
	{
		submitApprove = string;
	}

	/**
	 * @return
	 */
	public String getSubmitUndoApprove()
	{
		return submitUndoApprove;
	}

	/**
	 * @param string
	 */
	public void setSubmitUndoApprove(String string)
	{
		submitUndoApprove = string;
	}

	/**
	 * @return
	 */
	public String getSubmitProcess()
	{
		return submitProcess;
	}

	/**
	 * @param string
	 */
	public void setSubmitProcess(String string)
	{
		submitProcess = string;
	}

	/**
	 * @return
	 */
	public String getSubmitUndoDeny()
	{
		return submitUndoDeny;
	}

	/**
	 * @param string
	 */
	public void setSubmitUndoDeny(String string)
	{
		submitUndoDeny = string;
	}

	/**
	 * @return
	 */
	public String getSubmitDeny()
	{
		return submitDeny;
	}

	/**
	 * @param string
	 */
	public void setSubmitDeny(String string)
	{
		submitDeny = string;
	}


	/**
	 * @return
	 */
	public String getSubmitRetrievalRequest()
	{
		return submitRetrievalRequest;
	}

	/**
	 * @param string
	 */
	public void setSubmitRetrievalRequest(String string)
	{
		submitRetrievalRequest = string;
	}

	public String getSubmitCancelBankRqstRefund() {
		return submitCancelBankRqstRefund;
	}

	public void setSubmitCancelBankRqstRefund(String submitCancelBankRqstRefund) {
		this.submitCancelBankRqstRefund = submitCancelBankRqstRefund;
	}

	public String getSubmitBankRqstRefund() {
		return submitBankRqstRefund;
	}

	public void setSubmitBankRqstRefund(String submitBankRqstRefund) {
		this.submitBankRqstRefund = submitBankRqstRefund;
	}

	public String getSubmitAutoCancelBankRqstRefund() {
		return submitAutoCancelBankRqstRefund;
	}

	public void setSubmitAutoCancelBankRqstRefund(
			String submitAutoCancelBankRqstRefund) {
		this.submitAutoCancelBankRqstRefund = submitAutoCancelBankRqstRefund;
	}

	public String getSubmitBankRepresentRejDebit() {
		return submitBankRepresentRejDebit;
	}

	public void setSubmitBankRepresentRejDebit(String submitBankRepresentRejDebit) {
		this.submitBankRepresentRejDebit = submitBankRepresentRejDebit;
	}

	public String getSubmitBankRejectedDebit() {
		return submitBankRejectedDebit;
	}

	public void setSubmitBankRejectedDebit(String submitBankRejectedDebit) {
		this.submitBankRejectedDebit = submitBankRejectedDebit;
	}

	public String getSubmitBankRecordVoid() {
		return submitBankRecordVoid;
	}

	public void setSubmitBankRecordVoid(String submitBankRecordVoid) {
		this.submitBankRecordVoid = submitBankRecordVoid;
	}

	public String getSubmitBankIssueRefund() {
		return submitBankIssueRefund;
	}

	public void setSubmitBankIssueRefund(String submitBankIssueRefund) {
		this.submitBankIssueRefund = submitBankIssueRefund;
	}

	public String getSubmitBankWriteOffLoss() {
		return submitBankWriteOffLoss;
	}

	public void setSubmitBankWriteOffLoss(String submitBankWriteOffLoss) {
		this.submitBankWriteOffLoss = submitBankWriteOffLoss;
	}

	public String getSubmitBankRecoverLoss() {
		return submitBankRecoverLoss;
	}

	public void setSubmitBankRecoverLoss(String submitBankRecoverLoss) {
		this.submitBankRecoverLoss = submitBankRecoverLoss;
	}

	/**
	 * @return
	 */
	public String getSubmitExit()
	{
		return submitExit;
	}

	/**
	 * @param string
	 */
	public void setSubmitExit(String string)
	{
		submitExit = string;
	}

	/**
	 * @return
	 */
	public String getTranCmntReasCode()
	{
		return tranCmntReasCode == null ? "" : tranCmntReasCode;
	}

	/**
	 * @param string
	 */
	public void setTranCmntReasCode(String string)
	{
		tranCmntReasCode = string;
	}

	/**
	 * @return
	 */
	public String getCmntText()
	{
		return CmntText == null ? "" : CmntText;
	}

	/**
	 * @param string
	 */
	public void setCmntText(String string)
	{
		CmntText = string;
	}

	/**
	 * @return
	 */
	public String getSubmitAddComment()
	{
		return submitAddComment;
	}

	/**
	 * @param string
	 */
	public void setSubmitAddComment(String string)
	{
		submitAddComment = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		tranId = "";
		tranStatusCode = "";
		tranStatusDesc = "";
		tranSubStatusCode = "";
		tranSubStatusDesc = "";
		tranLgcyRefNbr = "";
		tranOwnerId = "";
		tranSenderName = "";
		tranReceiverName = "";
		tranSenderSecondLastName = "";
		tranFaceAmt = "";
		tranSendFee = "";
		tranReturnFee = "";
		tranSendAmt = "";
		tranType = "";
		tranTypeDesc = "";
		tranSendCountry = "";
		tranRecvCountry = "";
		tranPrimAcctType = "";
		tranPrimAcctMask = "";
		tranSecAcctType = "";
		tranSecAcctMask = "";
		tranCustIP = "";
		sendCustAcctId = "";
		sendCustBkupAcctId = "";
		tranSndMsg1Text = "";
		tranSndMsg2Text = "";
		createDate = "";
		transScores = 0;
		sysAutoRsltCode = "";

		submitMoveToPending = null;
		submitApprove = null;
		submitApproveProcess = null;
		submitDeny = null;
		submitProcess = null;
		submitResubmitACH = null;
		submitCcSale = null;
		submitRecAchAutoRef = null;
		submitRecAchCxlRef = null;
		submitRecAchRefundReq = null;
		submitRecCcAutoRef = null;
		submitRecCcCxlRef = null;
		submitRecCcRefundReq = null;
		submitRecordAchChargeback = null;
		submitRecordCcChargeback = null;
		submitManWriteOffLoss = null;
		submitManCollectFee = null;
		submitManRefundFee = null;
		submitManReturnFunding = null;
		submitRecordLossAdjustment = null;
		submitRecordFeeAdjustment = null;
		submitRecordConsumerRefundOrRemittanceAdjustment = null;
		submitRecordMgActivityAdjustment = null;
		submitRecordMgCancelRefund = null;
		submitCancelAchWaiting = null;
		submitAutoWriteOffLoss = null;
		submitAutoChargebackAndWriteOffLoss = null;
		submitRecoveryOfLoss = null;
		submitEsMGSend = null;
		submitEsMGSendCancel = null;
		submitRecordTranError = null;
		submitReleaseTransaction = null;
		submitGenTranScore = null;
		submitCancelBankRqstRefund = null;
		submitBankRqstRefund = null;
		submitAutoCancelBankRqstRefund = null;
		submitBankRepresentRejDebit = null;
		submitBankRejectedDebit = null;
		submitBankRecordVoid = null;
		submitBankIssueRefund = null;
		submitBankWriteOffLoss = null;
		submitBankRecoverLoss = null;
		submitCcPartialRefund = null;
		submitCcManualRefund = null;
		submitCcManualVoid = null;
		submitUndoDSError = null;
		
		submitExit = null;
		tranCmntReasCode = "";
		CmntText = "";
		submitAddComment = null;
		buttonName = null;
		releaseOwnership = "off";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		if (submitAddComment != null ) {
			if (StringHelper.isNullOrEmpty(CmntText)) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "Transaction Comments"));
			} else if (CmntText.length() > 255) {
				String[] parm = {"Transaction Comments", "255"};
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.length.too.long", parm));
			}
		}
		return errors;
	}

	public String getSendCustAcctId()
	{
		return sendCustAcctId;
	}

	public void setSendCustAcctId(String string)
	{
		sendCustAcctId = string;
	}

	public String getSendCustBkupAcctId()
	{
		return sendCustBkupAcctId;
	}

	public void setSendCustBkupAcctId(String string)
	{
		sendCustBkupAcctId = string;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String string) {
		buttonName = string;
	}

	public String getReleaseOwnership() {
		return releaseOwnership;
	}

	public void setReleaseOwnership(String s) {
		releaseOwnership = s;
	}

	public String getSubmitMoveToPending() {
		return submitMoveToPending;
	}

	public void setSubmitMoveToPending(String string) {
		submitMoveToPending = string;
	}

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

 	public String getSubmitResubmitACH() {
		return submitResubmitACH;
	}

	public void setSubmitResubmitACH(String string) {
		submitResubmitACH = string;
	}

	public String getSubmitCcSale() {
		return submitCcSale;
	}

	public void setSubmitCcSale(String string) {
		submitCcSale = string;
	}

	public String getSubmitRecAchCxlRef() {
		return submitRecAchCxlRef;
	}

	public void setSubmitRecAchCxlRef(String string) {
		submitRecAchCxlRef = string;
	}

	public String getSubmitRecCcCxlRef() {
		return submitRecCcCxlRef;
	}

	public void setSubmitRecCcCxlRef(String string) {
		submitRecCcCxlRef = string;
	}

	public String getSubmitManWriteOffLoss() {
		return submitManWriteOffLoss;
	}

	public void setSubmitManWriteOffLoss(String string) {
		submitManWriteOffLoss = string;
	}

	public String getSubmitManCollectFee() {
		return submitManCollectFee;
	}

	public void setSubmitManCollectFee(String string) {
		submitManCollectFee = string;
	}

	public String getSubmitManRefundFee() {
		return submitManRefundFee;
	}

	public void setSubmitManRefundFee(String string) {
		submitManRefundFee = string;
	}

	public String getSubmitManReturnFunding() {
		return submitManReturnFunding;
	}

	public void setSubmitManReturnFunding(String string) {
		submitManReturnFunding = string;
	}

	public String getSubmitRecordLossAdjustment() {
		return submitRecordLossAdjustment;
	}

	public void setSubmitRecordLossAdjustment(String string) {
		submitRecordLossAdjustment = string;
	}

	public String getSubmitRecordFeeAdjustment() {
		return submitRecordFeeAdjustment;
	}

	public void setSubmitRecordFeeAdjustment(String string) {
		submitRecordFeeAdjustment = string;
	}

	public String getSubmitRecordConsumerRefundOrRemittanceAdjustment() {
		return submitRecordConsumerRefundOrRemittanceAdjustment;
	}

	public void setSubmitRecordConsumerRefundOrRemittanceAdjustment(String string) {
		submitRecordConsumerRefundOrRemittanceAdjustment = string;
	}

	public String getSubmitRecordMgActivityAdjustment() {
		return submitRecordMgActivityAdjustment;
	}

	public void setSubmitRecordMgActivityAdjustment(String string) {
		submitRecordMgActivityAdjustment = string;
	}

	public String getTranSndMsg1Text() {
		return tranSndMsg1Text == null ? "" : tranSndMsg1Text;
	}

	public void setTranSndMsg1Text(String string) {
		tranSndMsg1Text = string;
	}

	public String getTranSndMsg2Text() {
		return tranSndMsg2Text == null ? "" : tranSndMsg2Text;
	}

	public void setTranSndMsg2Text(String string) {
		tranSndMsg2Text = string;
	}

	public String getSubmitRecordAchChargeback() {
		return submitRecordAchChargeback;
	}

	public void setSubmitRecordAchChargeback(String string) {
		submitRecordAchChargeback = string;
	}

	public String getSubmitRecordCcChargeback() {
		return submitRecordCcChargeback;
	}

	public void setSubmitRecordCcChargeback(String string) {
		submitRecordCcChargeback = string;
	}

	public String getSubmitRecordMgCancelRefund() {
		return submitRecordMgCancelRefund;
	}

	public void setSubmitRecordMgCancelRefund(String string) {
		submitRecordMgCancelRefund = string;
	}

	public String getSubmitCancelAchWaiting() {
		return submitCancelAchWaiting;
	}

	public void setSubmitCancelAchWaiting(String string) {
		submitCancelAchWaiting = string;
	}

	public boolean isDisplayMoneygramDetails() {
		return displayMoneygramDetails;
	}

	public void setDisplayMoneygramDetails(boolean b) {
		displayMoneygramDetails = b;
	}

	/**
	 * @return
	 */
	public String getSubmitRecAchAutoRef() {
		return submitRecAchAutoRef;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecAchAutoRef(String string) {
		submitRecAchAutoRef = string;
	}

	/**
	 * @return
	 */
	public String getSubmitRecCcAutoRef() {
		return submitRecCcAutoRef;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecCcAutoRef(String string) {
		submitRecCcAutoRef = string;
	}

	/**
	 * @return
	 */
	public String getSubmitRecAchRefundReq() {
		return submitRecAchRefundReq;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecAchRefundReq(String string) {
		submitRecAchRefundReq = string;
	}

	/**
	 * @return
	 */
	public String getSubmitRecCcRefundReq() {
		return submitRecCcRefundReq;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecCcRefundReq(String string) {
		submitRecCcRefundReq = string;
	}

	/**
	 * @return
	 */
	public String getSubmitAutoWriteOffLoss() {
		return submitAutoWriteOffLoss;
	}

	/**
	 * @param string
	 */
	public void setSubmitAutoWriteOffLoss(String string) {
		submitAutoWriteOffLoss = string;
	}

	/**
	 * @return
	 */
	public String getSubmitAutoChargebackAndWriteOffLoss() {
		return submitAutoChargebackAndWriteOffLoss;
	}

	/**
	 * @param string
	 */
	public void setSubmitAutoChargebackAndWriteOffLoss(String string) {
		submitAutoChargebackAndWriteOffLoss = string;
	}

	/**
	 * @return
	 */
	public String getSubmitRecoveryOfLoss() {
		return submitRecoveryOfLoss;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecoveryOfLoss(String string) {
		submitRecoveryOfLoss = string;
	}

	/**
	 * @return
	 */
	public String getTranRcvAgcyAcctMask() {
		return tranRcvAgcyAcctMask;
	}

	/**
	 * @param string
	 */
	public void setTranRcvAgcyAcctMask(String string) {
		tranRcvAgcyAcctMask = string;
	}

	/**
	 * @return
	 */
	public String getSubmitEsMGSend() {
		return submitEsMGSend;
	}

	/**
	 * @param string
	 */
	public void setSubmitEsMGSend(String string) {
		submitEsMGSend = string;
	}

	/**
	 * @return
	 */
	public String getSubmitEsMGSendCancel() {
		return submitEsMGSendCancel;
	}

	/**
	 * @param string
	 */
	public void setSubmitEsMGSendCancel(String string) {
		submitEsMGSendCancel = string;
	}

	/**
	 * @return
	 */
	public String getEsMGSendFlag() {
		return esMGSendFlag;
	}

	/**
	 * @param string
	 */
	public void setEsMGSendFlag(String string) {
		esMGSendFlag = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustMatrnlName() {
		return rcvCustMatrnlName == null ? "" : rcvCustMatrnlName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustMatrnlName(String string) {
		rcvCustMatrnlName = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustMidName() {
		return rcvCustMidName == null ? "" : rcvCustMidName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustMidName(String string) {
		rcvCustMidName = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrStateName() {
		return rcvCustAddrStateName == null ? "" : rcvCustAddrStateName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrStateName(String string) {
		rcvCustAddrStateName = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgentRefNbr() {
		return rcvAgentRefNbr == null ? "" : rcvAgentRefNbr;
	}

	/**
	 * @param string
	 */
	public void setRcvAgentRefNbr(String string) {
		rcvAgentRefNbr = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrLine1Text() {
		return rcvCustAddrLine1Text == null ? "" : rcvCustAddrLine1Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrLine1Text(String string) {
		rcvCustAddrLine1Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrLine2Text() {
		return rcvCustAddrLine2Text == null ? "" : rcvCustAddrLine2Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrLine2Text(String string) {
		rcvCustAddrLine2Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrLine3Text() {
		return rcvCustAddrLine3Text == null ? "" : rcvCustAddrLine3Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrLine3Text(String string) {
		rcvCustAddrLine3Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrCityName() {
		return rcvCustAddrCityName == null ? "" : rcvCustAddrCityName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrCityName(String string) {
		rcvCustAddrCityName = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustAddrPostalCode() {
		return rcvCustAddrPostalCode == null ? "" : rcvCustAddrPostalCode;
	}

	/**
	 * @param string
	 */
	public void setRcvCustAddrPostalCode(String string) {
		rcvCustAddrPostalCode = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustDlvrInstr1Text() {
		return rcvCustDlvrInstr1Text == null ? "" : rcvCustDlvrInstr1Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustDlvrInstr1Text(String string) {
		rcvCustDlvrInstr1Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustDlvrInstr2Text() {
		return rcvCustDlvrInstr2Text == null ? "" : rcvCustDlvrInstr2Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustDlvrInstr2Text(String string) {
		rcvCustDlvrInstr2Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustDlvrInstr3Text() {
		return rcvCustDlvrInstr3Text == null ? "" : rcvCustDlvrInstr3Text;
	}

	/**
	 * @param string
	 */
	public void setRcvCustDlvrInstr3Text(String string) {
		rcvCustDlvrInstr3Text = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustPhNbr() {
		return rcvCustPhNbr == null ? "" : rcvCustPhNbr;
	}

	/**
	 * @param string
	 */
	public void setRcvCustPhNbr(String string) {
		rcvCustPhNbr = string;
	}

	/**
	 * @return
	 */
	public String getTranSendCity() {
		return tranSendCity;
	}

	/**
	 * @param string
	 */
	public void setTranSendCity(String string) {
		tranSendCity = string;
	}

	/**
	 * @return
	 */
	public String getTranSendState() {
		return tranSendState;
	}

	/**
	 * @param string
	 */
	public void setTranSendState(String string) {
		tranSendState = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgtCity() {
		return rcvAgtCity == null ? "" : rcvAgtCity;
	}

	/**
	 * @param string
	 */
	public void setRcvAgtCity(String string) {
		rcvAgtCity = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgtState() {
		return rcvAgtState == null ? "" : rcvAgtState;
	}

	/**
	 * @param string
	 */
	public void setRcvAgtState(String string) {
		rcvAgtState = string;
	}

	/**
	 * @return
	 */
	public String getRcvAgtCntry() {
		return rcvAgtCntry == null ? "" : rcvAgtCntry;
	}

	/**
	 * @param string
	 */
	public void setRcvAgtCntry(String string) {
		rcvAgtCntry = string;
	}

	public String getRcvAgtAddr() {
		if ((getRcvAgtCity() + getRcvAgtState() + getRcvAgtCntry()).equals("")) {
			return "(Unknown)";
		} else {
			return getRcvAgtCity() + "," + getRcvAgtState() + "," + getRcvAgtCntry();
		}
	}

	/**
	 * @return
	 */
	public String getRcvDate() {
		return rcvDate == null ? "" : rcvDate;
	}

	/**
	 * @param string
	 */
	public void setRcvDate(String string) {
		rcvDate = string;
	}

	/**
	 * @return
	 */
	public String getSubmitRecordTranError()
	{
		return submitRecordTranError;
	}

	/**
	 * @param string
	 */
	public void setSubmitRecordTranError(String string)
	{
		submitRecordTranError = string;
	}

	/**
	 * @return
	 */
	public String getSubmitReleaseTransaction()
	{
		return submitReleaseTransaction;
	}

	/**
	 * @param string
	 */
	public void setSubmitReleaseTransaction(String string)
	{
		submitReleaseTransaction = string;
	}

	/**
	 * @return
	 */
	public String getCreateDate()
	{
		return createDate;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string)
	{
		createDate = string;
	}

	/**
	 * @return
	 */
	public int getTransScores()
	{
		return transScores;
	}

	/**
	 * @param i
	 */
	public void setTransScores(int i)
	{
		transScores = i;
	}

	/**
	 * @return
	 */
	public String getSysAutoRsltCode()
	{
		return sysAutoRsltCode;
	}

	/**
	 * @param string
	 */
	public void setSysAutoRsltCode(String string)
	{
		sysAutoRsltCode = string;
	}

	/**
	 * @return
	 */
	public String getScoreRvwCode()
	{
		return scoreRvwCode == null ? "" : scoreRvwCode;
	}

	/**
	 * @param string
	 */
	public void setScoreRvwCode(String string)
	{
		scoreRvwCode = string;
	}

	/**
	 * @return
	 */
	public String getTranScoreCmntText()
	{
		return tranScoreCmntText == null ? "" : tranScoreCmntText;
	}

	/**
	 * @param string
	 */
	public void setTranScoreCmntText(String string)
	{
		tranScoreCmntText = string;
	}

	/**
	 * @return
	 */
	public String getSndThrldWarnAmt()
	{
		return sndThrldWarnAmt;
	}

	/**
	 * @param string
	 */
	public void setSndThrldWarnAmt(String string)
	{
		sndThrldWarnAmt = string;
	}

	/**
	 * @return
	 */
	public boolean isOverThrldAmt()
	{
		return overThrldAmt;
	}

	/**
	 * @param b
	 */
	public void setOverThrldAmt(boolean b)
	{
		overThrldAmt = b;
	}

	/**
	 * @return
	 */
	public String getSubmitGenTranScore()
	{
		return submitGenTranScore;
	}

	/**
	 * @param string
	 */
	public void setSubmitGenTranScore(String string)
	{
		submitGenTranScore = string;
	}

	/**
	 * @return
	 */
	public String getSubmitApproveProcess()
	{
		return submitApproveProcess;
	}

	/**
	 * @param string
	 */
	public void setSubmitApproveProcess(String string)
	{
		submitApproveProcess = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustLastName()
	{
		return rcvCustLastName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustLastName(String string)
	{
		rcvCustLastName = string;
	}

	/**
	 * @return
	 */
	public String getRcvCustFrstName()
	{
		return rcvCustFrstName;
	}

	/**
	 * @param string
	 */
	public void setRcvCustFrstName(String string)
	{
		rcvCustFrstName = string;
	}

	/**
	 * @return
	 */
	public String getInetPurchFlag()
	{
		return inetPurchFlag;
	}

	/**
	 * @param string
	 */
	public void setInetPurchFlag(String string)
	{
		inetPurchFlag = string;
	}

    /**
     * @return Returns the tranRvwPrcsCode.
     */
    public String getTranRvwPrcsCode() {
        return tranRvwPrcsCode;
    }
    /**
     * @param tranRvwPrcsCode The tranRvwPrcsCode to set.
     */
    public void setTranRvwPrcsCode(String rvwPrcsCode) {
        this.tranRvwPrcsCode = rvwPrcsCode;
    }
    /**
     * @return Returns the retrievalRequestDate.
     */
    public Date getRetrievalRequestDate() {
        return retrievalRequestDate;
    }
    /**
     * @param retrievalRequestDate The retrievalRequestDate to set.
     */
    public void setRetrievalRequestDate(Date rrd) {
        this.retrievalRequestDate = rrd;
    }

	/**
	 * @return Returns the rcvRRN.
	 */
	public String getRcvRRN() {
		return rcvRRN;
	}
	/**
	 * @param rcvRRN The rcvRRN to set.
	 */
	public void setRcvRRN(String rcvRRN) {
		this.rcvRRN = rcvRRN;
	}
	/**
	 * @return Returns the deliveryOptionName.
	 */
	public String getDeliveryOptionName() {
		return deliveryOptionName;
	}
	/**
	 * @param deliveryOptionName The deliveryOptionName to set.
	 */
	public void setDeliveryOptionName(String deliveryOptionName) {
		this.deliveryOptionName = deliveryOptionName;
	}
	/**
	 * @return Returns the tranRecvCurrency.
	 */
	public String getTranRecvCurrency() {
		return tranRecvCurrency;
	}
	/**
	 * @param tranRecvCurrency The tranRecvCurrency to set.
	 */
	public void setTranRecvCurrency(String tranRecvCurrency) {
		this.tranRecvCurrency = tranRecvCurrency;
	}
	/**
	 * @return Returns the tranRecvCurrencyName.
	 */
	public String getTranRecvCurrencyName() {
		return tranRecvCurrencyName;
	}
	/**
	 * @param tranRecvCurrencyName The tranRecvCurrencyName to set.
	 */
	public void setTranRecvCurrencyName(String tranRecvCurrencyName) {
		this.tranRecvCurrencyName = tranRecvCurrencyName;
	}

	public String getCustomerAutoEnrollFlag() {
		return customerAutoEnrollFlag == null ? "" : customerAutoEnrollFlag;
	}

	public void setCustomerAutoEnrollFlag(String customerAutoEnrollFlag) {
		this.customerAutoEnrollFlag = customerAutoEnrollFlag;
	}

	public String getTestQuestion() {
		return testQuestion == null ? "" : testQuestion;
	}

	public void setTestQuestion(String testQuestion) {
		this.testQuestion = testQuestion;
	}

	public String getTestQuestionAnswer() {
		return testQuestionAnswer == null ? "" : testQuestionAnswer;
	}

	public void setTestQuestionAnswer(String testQuestionAnswer) {
		this.testQuestionAnswer = testQuestionAnswer;
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId == null ? "" : loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId;
	}

	public String getSndCustPhotoIdTypeCode() {
		return sndCustPhotoIdTypeCode == null ? "" : sndCustPhotoIdTypeCode;
	}

	public void setSndCustPhotoIdTypeCode(String sndCustPhotoIdTypeCode) {
		this.sndCustPhotoIdTypeCode = sndCustPhotoIdTypeCode;
	}

	public String getSndCustPhotoIdCntryCode() {
		return sndCustPhotoIdCntryCode == null ? "" : sndCustPhotoIdCntryCode;
	}

	public void setSndCustPhotoIdCntryCode(String sndCustPhotoIdCntryCode) {
		this.sndCustPhotoIdCntryCode = sndCustPhotoIdCntryCode;
	}

	public String getSndCustPhotoIdStateCode() {
		return sndCustPhotoIdStateCode == null ? "" : sndCustPhotoIdStateCode;
	}

	public void setSndCustPhotoIdStateCode(String sndCustPhotoIdStateCode) {
		this.sndCustPhotoIdStateCode = sndCustPhotoIdStateCode;
	}

	public String getSndCustPhotoId() {
		return sndCustPhotoId == null ? "" : sndCustPhotoId;
	}

	public void setSndCustPhotoId(String sndCustPhotoId) {
		this.sndCustPhotoId = sndCustPhotoId;
	}

	public String getSndCustLegalIdTypeCode() {
		return sndCustLegalIdTypeCode == null ? "" : sndCustLegalIdTypeCode;
	}

	public void setSndCustLegalIdTypeCode(String sndCustLegalIdTypeCode) {
		this.sndCustLegalIdTypeCode = sndCustLegalIdTypeCode;
	}

	public String getSndCustLegalId() {
		return sndCustLegalId == null ? "" : sndCustLegalId;
	}

	public void setSndCustLegalId(String sndCustLegalId) {
		this.sndCustLegalId = sndCustLegalId;
	}

	public Date getSndCustDOB() {
		return sndCustDOB;
	}

	public void setSndCustDOB(Date sndCustDOB) {
		this.sndCustDOB = sndCustDOB;
	}

	public String getSndCustOccupationText() {
		return sndCustOccupationText == null ? "" : sndCustOccupationText;
	}

	public void setSndCustOccupationText(String sndCustOccupationText) {
		this.sndCustOccupationText = sndCustOccupationText;
	}

	public String getSndFeeAmtNoDiscountAmt() {
		return sndFeeAmtNoDiscountAmt;
	}

	public void setSndFeeAmtNoDiscountAmt(String sndFeeAmtNoDiscountAmt) {
		this.sndFeeAmtNoDiscountAmt = sndFeeAmtNoDiscountAmt;
	}
	public String getMccPartnerProfileId() {
		return mccPartnerProfileId;
	}

	public void setMccPartnerProfileId(String mccPartnerProfileId) {
		this.mccPartnerProfileId = mccPartnerProfileId;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public Integer getTranRiskScore() {
		return tranRiskScore;
	}

	public void setTranRiskScore(Integer tranRiskScore) {
		this.tranRiskScore = tranRiskScore;
	}

	public boolean isTranRiskScoreDefaulted() {
		return tranRiskScoreDefaulted;
	}

	public void setTranRiskScoreDefaulted(boolean tranRiskScoreDefaulted) {
		this.tranRiskScoreDefaulted = tranRiskScoreDefaulted;
	}

	public String getTranSendCurrency() {
		return tranSendCurrency;
	}

	public void setTranSendCurrency(String tranSendCurrency) {
		this.tranSendCurrency = tranSendCurrency;
	}

	public void setTelecheckAction(String telecheckAction) {
		this.telecheckAction = telecheckAction;
	}

	public String getTelecheckAction() {
		return telecheckAction;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getAmount() {
		return amount;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getFee() {
		return fee;
	}

	public String getTransPartnerSiteId() {
		return transPartnerSiteId;
	}

	public void setTransPartnerSiteId(String transPartnerSiteId) {
		this.transPartnerSiteId = transPartnerSiteId;
	}

	public String getDebitAcct() {
		return debitAcct;
	}

	public void setDebitAcct(String debitAcct) {
		this.debitAcct = debitAcct;
	}

	public String getSubmitCcPartialRefund() {
		return submitCcPartialRefund;
	}

	public void setSubmitCcPartialRefund(String submitCcPartialRefund) {
		this.submitCcPartialRefund = submitCcPartialRefund;
	}

	public double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getOtherFees() {
		return otherFees;
	}

	public void setOtherFees(String otherFees) {
		this.otherFees = otherFees;
	}

	public String getOtherTaxes() {
		return otherTaxes;
	}

	public void setOtherTaxes(String otherTaxes) {
		this.otherTaxes = otherTaxes;
	}

	public String getTotalReceiveAmount() {
		return totalReceiveAmount;
	}

	public void setTotalReceiveAmount(String totalReceiveAmount) {
		this.totalReceiveAmount = totalReceiveAmount;
	}

	public String getTranSenderSecondLastName() {
		return tranSenderSecondLastName;
	}

	public void setTranSenderSecondLastName(String tranSenderSecondLastName) {
		this.tranSenderSecondLastName = tranSenderSecondLastName;
	}

	public Date getSndCustPhotoIdExpDate() {
		return sndCustPhotoIdExpDate;
	}

	public void setSndCustPhotoIdExpDate(Date sndCustPhotoIdExpDate) {
		this.sndCustPhotoIdExpDate = sndCustPhotoIdExpDate;
	}

	public String getSubmitCcManualRefund() {
		return submitCcManualRefund;
	}

	public void setSubmitCcManualRefund(String submitCcManualRefund) {
		this.submitCcManualRefund = submitCcManualRefund;
	}

	public String getSubmitCcManualVoid() {
		return submitCcManualVoid;
	}

	public void setSubmitCcManualVoid(String submitCcManualVoid) {
		this.submitCcManualVoid = submitCcManualVoid;
	}

	public String getSubmitUndoDSError() {
		return submitUndoDSError;
	}

	public void setSubmitUndoDSError(String submitUndoDSError) {
		this.submitUndoDSError = submitUndoDSError;
	}

	public String getConsumerISP() {
		return consumerISP;
	}

	public void setConsumerISP(String consumerISP) {
		this.consumerISP = consumerISP;
	}

	public String getConsumerDomain() {
		return consumerDomain;
	}

	public void setConsumerDomain(String consumerDomain) {
		this.consumerDomain = consumerDomain;
	}

	public String getConsumerCountry() {
		return consumerCountry;
	}

	public void setConsumerCountry(String consumerCountry) {
		this.consumerCountry = consumerCountry;
	}

	public String getConsumerRegion() {
		return consumerRegion;
	}

	public void setConsumerRegion(String consumerRegion) {
		this.consumerRegion = consumerRegion;
	}

	public String getConsumerCity() {
		return consumerCity;
	}

	public void setConsumerCity(String consumerCity) {
		this.consumerCity = consumerCity;
	}

	public String getConsumerZipCode() {
		return consumerZipCode;
	}

	public void setConsumerZipCode(String consumerZipCode) {
		this.consumerZipCode = consumerZipCode;
	}

	public long getConsumerLatitude() {
		return consumerLatitude;
	}

	public void setConsumerLatitude(long consumerLatitude) {
		this.consumerLatitude = consumerLatitude;
	}

	public long getConsumerLongitude() {
		return consumerLongitude;
	}

	public void setConsumerLongitude(long consumerLongitude) {
		this.consumerLongitude = consumerLongitude;
	}

	public String getDispatch() {
		return dispatch;
	}

	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	public boolean isCountrySameFlag() {
		return countrySameFlag;
	}

	public void setCountrySameFlag(boolean countrySameFlag) {
		this.countrySameFlag = countrySameFlag;
	}

	public String getConsumerCountryCode() {
		return consumerCountryCode;
	}

	public void setConsumerCountryCode(String consumerCountryCode) {
		this.consumerCountryCode = consumerCountryCode;
	}

	public String getRcvAcctMaskNbr() {
		return rcvAcctMaskNbr;
	}

	public void setRcvAcctMaskNbr(String rcvAcctMaskNbr) {
		this.rcvAcctMaskNbr = rcvAcctMaskNbr;
	}
	
	
}
