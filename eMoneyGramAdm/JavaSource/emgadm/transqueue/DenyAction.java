package emgadm.transqueue;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.MailService;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;

/**
 * @author A131
 *
 */
public class DenyAction extends ConfirmSubReasonBaseAction {

    private static final String resourceFile = "emgadm.resources.ApplicationResources";
    private static final MailService mailService = ServiceFactory.getInstance()
            .getMailService();

    protected ActionForward processConfirmation(ActionMapping mapping,
            ConfirmSubReasonForm form, HttpServletRequest request,
            HttpServletResponse response) throws NumberFormatException,
            TransactionAlreadyInProcessException, TransactionOwnershipException, DataSourceException, SQLException {

        ActionErrors errors = new ActionErrors();
        UserProfile userProfile = getUserProfile(request);
        TransactionManager tm = getTransactionManager(request);
        TransactionService tranService = ServiceFactory.getInstance()
                .getTransactionService();

        int tranId = Integer.parseInt(form.getTranId());
        Transaction tran = tm.getTransaction(tranId);
//        List tas;
        tranService.confirmStatusChangeWithSubReason(
                Integer.parseInt(form.getTranId()),
                userProfile.getUID(),
                String.valueOf(TransactionStatus.DENIED_NOT_FUNDED.getStatusCode()),
                String.valueOf(TransactionStatus.DENIED_NOT_FUNDED.getSubStatusCode()),
                form.getSubReasonCode());

        // Here is the code to add a transaction comment automatically
        tm.setTransactionComment(tranId, EMoneyGramAdmApplicationConstants.DENIAL_CODE, "Subreason: " + form.getSubReasonDescription(), userProfile.getUID());

        try {
            this.sendDenyEmail(tran);
        } catch (Exception e) {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "error.send.mail.failed"));
            saveErrors(request, errors);
        }

        return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
    }

    protected void getCommandDescription(HttpServletRequest request) {
        Locale locale = Locale.getDefault();
        MessageResources msgRes = MessageResources
                .getMessageResources("emgadm.resources.TransactionConfirmationDescription");
        MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.deny"));
        formatter.setLocale(locale);
        String msgText = formatter.toPattern();
        request.setAttribute("msgText", msgText);

    }

    private void sendDenyEmail(Transaction tran)
            throws Exception {
    	ConsumerProfile profile = emgshared.services.ServiceFactory.getInstance()
		.getConsumerProfileService().getConsumerProfile(new Integer(tran.getCustId()), tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyDeniedTransaction(tran, consumerEmail,preferedlanguage);
			}
		}
    }

}
