/*
 * Created on July 18, 2005
 *
 */
package emgadm.transqueue;

import java.text.ParseException;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.services.processing.GlobalCollectAuthorize;
import emgadm.services.processing.VendorOperation;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ListenerProcess;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;

/**
 * This class contains 'convenient' static methods
 * for automatically setting a transaction to a
 * different status and processing a transaction
 * so that transactions can be processed automatically
 * without an administrator hitting the buttons.
 *
 * @author T004
 *
 */

public class TransactionProcess {

	private static final String uid = EMoneyGramAdmApplicationConstants.AUTOMATED_ADMIN_USER;
	private static final TransactionManager tranManager = ManagerFactory.createTransactionManager();

	public static ListenerProcess AutoApproveEP(Transaction transaction, String contextRoot) {
		ListenerProcess processReturn = new ListenerProcess();
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(transaction.getPartnerSiteId());

		try {
			tranService.takeOverOwnership(transaction.getEmgTranId(), uid);
			ApproveTransaction(transaction);
			ProcessExpressPayTransaction(transaction.getEmgTranId(), contextRoot);
			tranService.releaseOwnership(transaction.getEmgTranId(), uid);
			processReturn.setStatus(true);
			return processReturn;
		} catch (Exception e) {
			try {
				tranService.releaseOwnership(transaction.getEmgTranId(), uid);
				processReturn.setStatus(false);
				return processReturn;
			} catch (Exception ee) {
				processReturn.setStatus(false);
				return processReturn;
			}
		}
	}

	public static boolean ProcessExpressPayTransaction(int tranId, String contextRoot)
		throws
			ParseException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException,
			AgentConnectException {
		Transaction tran = tranManager.getTransaction(tranId);
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(tran.getPartnerSiteId());

		try {
			VendorOperation globalCollectAuthorize = new GlobalCollectAuthorize();
			tran = globalCollectAuthorize.execute(tranId, uid,
					contextRoot);
		} catch (EMGRuntimeException e) {
			e.printStackTrace();
		}

		if (tran.getTranStatCode().equals(TransactionStatus.APPROVED_CODE)
			&& tran.getTranSubStatCode().equals(TransactionStatus.CC_AUTH_CODE)
			&& tran.isSendable()) {
			try {
				tranService.sendExpressPayment(tranId, uid, contextRoot);
				return true;
			} catch (AgentConnectException e) {
				throw e;
			}
		} else {
			return false;
		}
	}

	public static boolean ApproveTransaction(Transaction tran)
		throws TransactionAlreadyInProcessException, TransactionOwnershipException {

		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(tran.getPartnerSiteId());
		try {

			tranService.setTransactionStatus(
				tran.getEmgTranId(),
				TransactionStatus.APPROVED_CODE,
				tran.getTranSubStatCode(),
				uid);

			return true;
		} catch (TransactionAlreadyInProcessException e) {
			throw e;
		} catch (TransactionOwnershipException e) {
			throw e;
		}
	}

	public static ListenerProcess consumerCancelTransaction(int tranId, String contextRoot) {
		ListenerProcess processReturn = new ListenerProcess();
		Transaction tran = tranManager.getTransaction(tranId);
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(tran.getPartnerSiteId());

		try {
			tranService.takeOverOwnership(tranId, uid);
			processCancelTransaction(tranId, contextRoot);
			tranService.releaseOwnership(tranId, uid);
			processReturn.setStatus(true);
			return processReturn;
		} catch (Exception e) {
			try {
				tranService.releaseOwnership(tranId, uid);
				processReturn.setStatus(false);
				return processReturn;
			} catch (Exception ee) {
				processReturn.setStatus(false);
				return processReturn;
			}
		}
	}

	private static boolean processCancelTransaction(int tranId, String contextRoot)
		throws
			TransactionAlreadyInProcessException,
			TransactionOwnershipException,
			AgentConnectException {
		//  this method must match with the showCancel() method in the
		//  Transaction bean for all possible status/sub-status codes
		Transaction tran = tranManager.getTransaction(tranId);
		TransactionService tranService = ServiceFactory.getInstance().getTransactionService(tran.getPartnerSiteId());

		try {
			Transaction trans =
				ManagerFactory.createTransactionManager().getTransaction(
					tranId);

			String statCode = trans.getTranStatCode();
			String fundCode = trans.getTranSubStatCode();

			// change FFS/NOF, PEN/NOF, APP/NOF to CXL/NOF
			if ((TransactionStatus.FORM_FREE_SEND_CODE.equals(statCode)
				|| TransactionStatus.PENDING_CODE.equals(statCode)
				|| TransactionStatus.APPROVED_CODE.equals(statCode))
				&& TransactionStatus.NOT_FUNDED_CODE.equals(fundCode)) {
				tranService.setTransactionStatus(
					tranId,
					TransactionStatus.CANCELED_CODE,
					fundCode,
					uid);
				return true;
			}

			//  change from APP/ACW to CXL/ACC
			if (TransactionStatus.APPROVED_CODE.equalsIgnoreCase(statCode)
				&& TransactionStatus.ACH_WAITING_CODE.equalsIgnoreCase(fundCode)) {
				tranService.setTransactionStatus(
					tranId,
					TransactionStatus.CANCELED_CODE,
					fundCode,
					uid);
				tranService.setTransactionStatus(
					tranId,
					TransactionStatus.CANCELED_CODE,
					TransactionStatus.ACH_CANCEL_CODE,
					uid);
				return true;
			}

			//  change from APP/ACS to CXL/ARR
			if (TransactionStatus.APPROVED_CODE.equalsIgnoreCase(statCode)
				&& TransactionStatus.ACH_SENT_CODE.equalsIgnoreCase(fundCode)) {
				tranService.setTransactionStatus(
					tranId,
					TransactionStatus.CANCELED_CODE,
					fundCode,
					uid);
				trans =
					ManagerFactory.createTransactionManager().getTransaction(
						tranId);
				tranService.setTransactionStatus(
					tranId,
					TransactionStatus.CANCELED_CODE,
					TransactionStatus.ACH_REFUND_REQUESTED_CODE,
					uid);

				ConsumerAccountType primaryAccountType =
					ConsumerAccountType.getInstance(trans.getSndAcctTypeCode());
				try {
					tranService.refundPersonToPersonSuccessEmail(
						trans,
						primaryAccountType);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				return true;
			}

			//  change from SEN/ACW and SEN/ACS to CXL/ARR and
			//  change from SEN/CCS to CXL/CCC
			if (TransactionStatus.SENT_CODE.equalsIgnoreCase(statCode)) {
				tranService.refundPersonToPerson(tranId, uid);
			}

			if (TransactionStatus.CC_SALE_CODE.equalsIgnoreCase(fundCode)) {
				//  CyberSource reversal
				tranService.doCreditCardReversal(tranId, uid, contextRoot);
			}

		} catch (AgentConnectException e) {
			throw e;
		}

		return true;
	}
}
