package emgadm.transqueue;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.GlAccount;
import emgadm.model.TranAction;
import emgadm.model.TranActionView;
import emgadm.model.UserProfile;
import emgadm.security.TranSecurityMatrix;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;

public class ManualCorrectingEntryAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
	{

		ManualCorrectingEntryForm form = (ManualCorrectingEntryForm) f;
		ServiceFactory sf = ServiceFactory.getInstance();
		TransactionService ts = sf.getTransactionService();
		ActionErrors errors = new ActionErrors();

		boolean firstTime = false;
		if (form.getFirst() != null)
		{
			firstTime = true;
		}

		if (!firstTime && !StringHelper.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANSCTION_DETAIL);
		}

		UserProfile up = getUserProfile(request);
		TransactionManager tm = getTransactionManager(request);
		int tranId = Integer.parseInt(form.getTranId());

		//  get transaction actions
		try
		{
			request.getSession().setAttribute("tranActions",getTranActions(tm, tranId, up.getUID()));
			Collection glAccounts = (Collection) ts.getGLAccounts(up.getUID());
			Iterator iter = glAccounts.iterator();
			while (iter.hasNext())
			{
				GlAccount ga = (GlAccount) iter.next();
				if (ga.getGlAccountId() == 8)
				{
					glAccounts.remove(ga);
					break;
				}
			}
			request.getSession().setAttribute("glAccounts", glAccounts);

		} catch (Exception e)
		{
			throw new EMGRuntimeException(e);
		}

		if (!firstTime
			&& !StringHelper.isNullOrEmpty(form.getSubmitCorrection()))
		{

			Transaction tran = tm.getTransaction(tranId);

			TranSecurityMatrix matrix =
				new TranSecurityMatrix(request, up, tran, new BigDecimal(0.0));

			if (matrix.isManualCorrectingEntry())
			{
				try
				{
					TranAction ta = new TranAction();
					ta.setTranId(tranId);
					ta.setDrGLAcctId(Integer.parseInt(form.getDebitAcct()));
					ta.setCrGLAcctId(Integer.parseInt(form.getCreditAcct()));
					ta.setTranPostAmt(new BigDecimal(form.getAmount()));
					ta.setTranReasDesc(form.getCmntText());
					tm.insertGLAdjustment(
						ta,
						tran.getSndAgentId(), //was using form selected agent, just use sndagt from txn
						up.getUID());
				} catch (Exception e)
				{
					throw new EMGRuntimeException(e);
				}

				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_TRANSCTION_DETAIL);
			} else
			{
				String[] parm = { matrix.getOwnerId(), String.valueOf(tranId)};
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.already.owned.by.other.user", parm));
				saveErrors(request, errors);
			}
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private List getTranActions(
		TransactionManager tm,
		int tranId,
		String logonId)
		throws DataSourceException, SQLException
	{
		List tas = tm.getTranActions(tranId, logonId);
		List tavs = new ArrayList();

		Iterator iter = tas.iterator();
		while (iter.hasNext())
		{
			TranAction ta = (TranAction) iter.next();
			TranActionView tav = new TranActionView();
			tav.setTranActionId(ta.getTranActionId());
			tav.setTranId(ta.getTranId());
			tav.setTranReasCode(ta.getTranReasCode());
			tav.setTranReasDesc(ta.getTranReasDesc());
			tav.setTranActnDate(ta.getTranActionDate());
			tav.setTranOldStatCode(ta.getTranOldStatCode());
			tav.setTranOldSubStatCode(ta.getTranOldSubStatCode());
			tav.setTranStatCode(ta.getTranStatCode());
			tav.setTranSubStatCode(ta.getTranSubStatCode());
			tav.setTranConfId(ta.getTranConfId());
			tav.setTranPostAmt(ta.getTranPostAmt());
			tav.setTranDrGlAcctId(ta.getDrGLAcctId());
			tav.setTranCrGlAcctId(ta.getCrGLAcctId());
			if (StringHelper.isNullOrEmpty(ta.getTranCreateUserid())
				|| ta.getTranCreateUserid().equalsIgnoreCase("java"))
			{
				tav.setTranCreateUserid("emgwww");
			} else
			{
				tav.setTranCreateUserid(ta.getTranCreateUserid());
			}
			tav.setTranDrGlAcctDesc(ta.getDrGLAcctDesc());
			tav.setTranCrGlAcctDesc(ta.getCrGLAcctDesc());
			tavs.add(tav);
		}

		if (tavs.size() > 1)
		{
			Collections.sort(tavs);
		}

		return tavs;
	}
}
