package emgadm.transqueue;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.BatchTranView;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.util.StringHelper;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;
import emgshared.util.DateFormatter;

public class BatchESProcessAction extends EMoneyGramAdmBaseAction
{
	private TransactionManager tm = null;
	private TransactionService ts =
		ServiceFactory.getInstance().getTransactionService();
	private BatchTranView view = null;

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		BatchESProcessForm form = (BatchESProcessForm) f;
		UserProfile up = getUserProfile(request);

		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel())
			|| !StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			request.getSession().removeAttribute("batchTrans");
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
		}

		tm = getTransactionManager(request);
		if (!StringHelper.isNullOrEmpty(form.getSubmitConfirm())
			&& !"Y".equalsIgnoreCase(form.getDone()))
		{
			boolean err = false;
			List list = (List) request.getSession().getAttribute("batchTrans");
			Iterator iter = list.iterator();
			while (iter.hasNext())
			{
				view = (BatchTranView) iter.next();
				boolean rtn = batchTranProcess(form, up.getUID(), request.getContextPath());
				err = rtn == true ? true : err;
			}

			if (err)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.there.are.transaction.errors"));
				saveErrors(request, errors);
			} else
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("msg.batch.process.successful"));
				saveErrors(request, errors);
			}
			form.setDone("Y");

			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		NumberFormat nf = new DecimalFormat("\u00A4#,##0.00");
		//		boolean warn = false;
		form.setActionType((String) request.getAttribute("processType"));
		List tranList = new ArrayList();
		String[] batchList = form.getSelTrans();
		for (int i = 0; i < batchList.length; i++)
		{
			Transaction tran =
				tm.getTransaction(Integer.parseInt(batchList[i]));
			BatchTranView btv =
				new BatchTranView(
					tran.getEmgTranId(),
					tran.getCsrPrcsUserid(),
					tran.getSndCustFrstName() + " " + tran.getSndCustLastName(),
					tran.getRcvCustFrstName() + " " + tran.getRcvCustLastName(),
					tran.getRcvISOCntryCode(),
					nf.format(tran.getSndFaceAmt()),
					nf.format(tran.getSndTotAmt()),
					df.format(tran.getCreateDate()),
					tran.getTransScores(),
					tran.getSysAutoRsltCode());

			btv.setTran(tran);
			btv.setMsg("");

			if (!StringHelper.isNullOrEmpty(btv.getOwner())
				&& !btv.getOwner().equalsIgnoreCase(up.getUID()))
			{
				btv.setMsg(
					"Warning: Owned by "
						+ btv.getOwner()
						+ ", please verify it's o.k. to takeover.");
				//				warn = true;
			}
			tranList.add(btv);
		}

		if (!"Y".equalsIgnoreCase(form.getDone()))
		{
			form.setDone("N");
		}
		request.getSession().setAttribute("batchTrans", tranList);

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private boolean batchTranProcess(BatchESProcessForm form, String userId, String contextRoot)
	{
		Transaction tran = view.getTran();
		view.setMsg("");

		try
		{
			if (StringHelper.isNullOrEmpty(tran.getCsrPrcsUserid()))
			{
				ts.requestOwnership(tran.getEmgTranId(), userId);
			} else if (!userId.equalsIgnoreCase(tran.getCsrPrcsUserid()))
			{
				ts.takeOverOwnership(tran.getEmgTranId(), userId);
			}
		} catch (TransactionAlreadyInProcessException e)
		{
			view.setMsg("Take Tran Ownership Failed: Transaction is busy.");
			return true;
		} catch (TransactionOwnershipException e)
		{
			view.setMsg(
				"Take Tran Ownership Failed: Transaction just owned by another user.");
			return true;
		}

		if ("Send MoneyGram".equalsIgnoreCase(form.getActionType()))
		{
			try
			{
				List<String> erl =
					ts.processEconomyServiceTransaction(
						tran.getEmgTranId(),
						userId,
						tran.getTranStatCode(),
						tran.getTranSubStatCode(), contextRoot);
				if (erl != null && erl.size() != 0)
				{
					for (String msg : erl) {
						if ("error.california.compliance.failed".equals(msg))
						{
							view.setMsg(
								"California Compliance Failed: Please try again later.");
							return true;
						}
						if ("error.agent.connect.failed".equals(msg))
						{
							view.setMsg(
								"Agent Connect Failed: Please see transaction comment for more detail information.");
							return true;
						}
					}
				}
			} catch (TransactionAlreadyInProcessException e1)
			{
				view.setMsg("Send Process Failed: Transaction is busy.");
				return true;
			} catch (TransactionOwnershipException e1)
			{
				view.setMsg(
					"Send Process Failed: Transaction just owned by another user.");
				return true;
			} catch (InvalidRRNException e) {
				view.setMsg(
						e.getRRN() + " is an invalid RRN");
				return true;
			} catch (AgentConnectException e1)
			{
				view.setMsg(
					"Agent Connect Failed: Please see transaction comment for more detail information.");
				return true;
			} catch (Exception e1)
			{
				throw new EMGRuntimeException(e1);
			}

		} else if (
			"ACH Return and Cancel".equalsIgnoreCase(form.getActionType()))
		{
			try
			{
				ts.sendEconomyServiceAchReturnEmail(tran, userId);
			} catch (Exception e1)
			{
				throw new EMGRuntimeException(e1);
			}
		}

		try
		{
			ts.releaseOwnership(tran.getEmgTranId(), userId);
		} catch (TransactionAlreadyInProcessException e)
		{
			view.setMsg("Release Tran Ownership Failed: Transaction is busy.");
			return true;
		} catch (TransactionOwnershipException e)
		{
			view.setMsg(
				"Release Tran Ownership Failed: Transaction just owned by another user.");
			return true;
		}

		Transaction newTran = tm.getTransaction(tran.getEmgTranId());
		view.setTran(newTran);
		view.setOwner(newTran.getCsrPrcsUserid());
		view.setStatus(newTran.getTranStatCode());
		view.setSubStatus(newTran.getTranSubStatCode());
		view.setMsg(form.getActionType() + " Process succcessful.");
		return false;
	}
}
