package emgadm.transqueue;


import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse;
import com.moneygram.security.ldap.LdapProperty;
import com.moneygram.security.ldap.LdapService;
import com.moneygram.security.ldap.LdapServiceImpl;
import com.moneygram.security.ldap.LdapUser;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ConsumerProfileDashboard;
import emgadm.model.EPartnerSite;
import emgadm.model.IPHistorySummary;
import emgadm.model.UserProfile;
import emgadm.security.ConsumerSecurityMatrix;
import emgadm.services.ConsumerServiceProxy;
import emgadm.services.ConsumerServiceProxyImpl;
import emgadm.services.DecryptionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgadm.services.iplookup.melissadata.client.ResponseArray;
import emgadm.services.iplookup.melissadata.client.ResponseRecord;
import emgadm.sysmon.EventListConstant;
import emgadm.util.DateHelper;
import emgadm.util.LDAPAttributeUtil;
import emgadm.view.ConsumerBankAccountView;
import emgadm.view.ConsumerCreditCardAccountView;
import emgadm.view.ConsumerProfileCommentView;
import emgadm.view.TransactionView;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.BlockedIP;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.GetValidateCustomerInfo;
import emgshared.model.MicroDeposit;
import emgshared.model.TaintIndicatorStatus;
import emgshared.model.TaintIndicatorType;
import emgshared.model.Transaction;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionType;
import emgshared.model.UserActivityType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;
import eventmon.eventmonitor.EventMonitor;

public class ShowCustomerProfileAction extends EMoneyGramAdmBaseAction {

	private static final String FORWARD_SUCCESS = "success";
    private static final String FORWARD_FAILURE = "failure";
    private static final Calendar IPLOOKUP_STARTDATE = new GregorianCalendar(2011, Calendar.AUGUST, 18);

    private TransactionManager tm = null;
    private MicroDepositService mds = ServiceFactory.getInstance().getMicroDepositService();
    private ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
    private ConsumerAccountService accountService = ServiceFactory.getInstance().getConsumerAccountService();
    private FraudService fraudService = ServiceFactory.getInstance().getFraudService();
    private DecryptionService decryptionService = emgadm.services.ServiceFactory.getInstance().getDecryptionService();
    private ConsumerBankAccountView[] bankAccountArr = null;
    private ConsumerCreditCardAccountView[] creditAccountArr = null;
    private ConsumerCreditCardAccountView[] debitAccountArr = null;
    private ConsumerCreditCardAccountView[] maestroAccountArr = null;
    private List custPrmrCodes = null;
    private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

    public ActionForward execute(ActionMapping mapping, ActionForm f,
            HttpServletRequest request, HttpServletResponse response) {

        ActionForward forward = null;
        ShowCustomerProfileForm form = (ShowCustomerProfileForm) f;
        UserProfile up = getUserProfile(request);
        String consumerIdText = form.getCustId();
        tm = getTransactionManager(request);
        setFlag(request, form);
        custPrmrCodes = (List) request.getSession().getServletContext().getAttribute("custPrmrCodes");

		String act = null;
		if (request.getParameter("action") == null)
		{
			act = (String) request.getAttribute("action");
		} else
		{
			act = (String) request.getParameter("action");
		}

        if (StringUtils.isNotBlank(consumerIdText) && StringUtils.isNumeric(consumerIdText)) {
            long beginTime = System.currentTimeMillis();
            int consumerId = Integer.parseInt(consumerIdText);
            String loginFailureAttempts = "0";
    		String userProfileLocked = "";
            ConsumerProfile consumerProfile = null;
            try {
                consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), up.getUID(), null);
                //GetValidateCustomerInfo customerInfoLog=profileService.getValidateCustomerRegistrationInfo(consumerId, false);
                GetValidateCustomerInfo customerInfoLog = null;
                GetValidateCustomerInfo customerInfoLogDetail = null;
                if(consumerProfile.getPartnerSiteId().equalsIgnoreCase(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)){
	                customerInfoLog = profileService.getValidateCustomerRegistrationInfo(consumerId, false);
	                customerInfoLogDetail = profileService.getValidateCustomerRegistrationInfo(consumerId, true);
	                if(customerInfoLogDetail.getValidateCustomerInfo().size()>0){
		                form.setViewGBGroupLogLinkString(customerInfoLog.getLinkText());
	                }


                }
                request.setAttribute("gbGroupLog", customerInfoLog);
                request.setAttribute("gbGroupLogDetails", (customerInfoLogDetail != null) ? customerInfoLogDetail.getValidateCustomerInfo() : null);
                request.setAttribute("userTimeZone", EPartnerSite.getPartnerByPartnerSiteId(consumerProfile.getPartnerSiteId()).getTimeZone());

                LdapService ldapService = new LdapServiceImpl();
                LdapUser lu = ldapService.findUser(consumerProfile.getUserId());
                if (lu!=null) {
                	ArrayList attributes = ldapService.getUserAttributes(consumerProfile.getUserId());
                	loginFailureAttempts = LDAPAttributeUtil.getAttributeValue(attributes, LdapProperty.LDAP_INTRUDERLOCKOUT_ATTEMPTS_ATTR);
                	if (loginFailureAttempts.equals("")) loginFailureAttempts="0";
                	consumerProfile.setInvlLoginTryCnt(Integer.parseInt(loginFailureAttempts));
                	userProfileLocked = LDAPAttributeUtil.getAttributeValue(attributes, LdapProperty.LDAP_INTRUDERLOCKOUT_ATTR);
                	if (userProfileLocked.equals("")) userProfileLocked="false";
                	consumerProfile.setProfileLoginLocked(Boolean.parseBoolean(userProfileLocked.toLowerCase()));
                } else {
                	loginFailureAttempts="0";
                	consumerProfile.setInvlLoginTryCnt(new Integer(0));
                	consumerProfile.setProfileLoginLocked(new Boolean(false));
                	consumerProfile.setProfileDeletedFromLdap(true);
                }
             } catch (Exception e) {
            	e.printStackTrace();
                forward = mapping.findForward(FORWARD_FAILURE);
            }

     		if (act != null && act.equalsIgnoreCase("ViewIdImage") &&
    				consumerProfile.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {

     			try {
					ConsumerServiceProxy proxy = ConsumerServiceProxyImpl.getInstance();
					GetConsumerIDImageResponse resp = null;
					resp = proxy.getConsumerIDImageResponse(consumerId, consumerProfile.getPartnerSiteId());

					// We NEED to verify that in fact ids are being uploaded with extension
					int dotPos = resp.getImageName().lastIndexOf(".");
			        String extension = resp.getImageName().substring(dotPos);

			        if (extension.equals("jpg")) {
	         			response.setContentType("image/jpeg");
			        } else {
			        	response.setContentType(extension);
			        }

			        // Content-Disposition is needed, otherwise IE 7+ throws a
			        // fit (HTTP status code 404), and will not recognize the output stream.
			        response.setHeader("Content-Disposition", "inline; filename=id-image" + extension);

         			OutputStream out = response.getOutputStream();
         			out.write(resp.getImageBytes());
         			out.close();

         			// A null must be returned, otherwise the following exception is caused:
         			// java.lang.IllegalStateException: OutputStream already obtained
         			return null;
     			} catch (Exception e) {
     				e.printStackTrace();
     			}
    		}

            if (forward == null) {
                addToQuickList(request, "custList", consumerIdText, 5);
                populateForm(form, consumerProfile, up);
                populateIPHistory(request, consumerProfile, up);
                setTaintFlag(request, consumerProfile);
                if ( (consumerProfile.getSsnTaintCode() != null) &&
                	 (consumerProfile.getSsnTaintCode().isBlocked()))
                	request.setAttribute("isSSNTainted", "Y");
                else
                	request.setAttribute("isSSNTainted", "N");
                if (consumerProfile.getProfileLoginLocked())
                	request.setAttribute("profileLocked", "Y");
                else
                	request.setAttribute("profileLocked", "N");
                if (consumerProfile.isProfileDeletedFromLdap())
                	request.setAttribute("profileDeletedFromLdap", "Y");
                else
                	request.setAttribute("profileDeletedFromLdap", "N");
                request.setAttribute("profileMasterOnlyTaint",checkMasterOnlyTaint(consumerProfile));
                request.getSession().setAttribute("findProfiles", consumerProfile);
                ConsumerProfileDashboard cpd = tm.getConsumerProfileDashboard(consumerProfile, up.getUID());
                cpd.setBankAccounts(bankAccountArr);
                cpd.setCreditCardAccounts(creditAccountArr);
                cpd.setDebitCardAccounts(debitAccountArr);
                cpd.setMaestroAccounts(maestroAccountArr);
                cpd.setParentSiteId(consumerProfile.getPartnerSiteId());
                request.getSession().setAttribute("dashboard", cpd);
                //consumerProfile.setDocStatus(form.getDocStatus());
                request.setAttribute("consumerAuth", new ConsumerSecurityMatrix(request, up, consumerProfile));

                setSecurityFlags(request, up, form);
                EventMonitor.getInstance().recordEvent(
                        EventListConstant.SHOW_CONSUMER_PROFILE, beginTime,
                        System.currentTimeMillis());
                super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_CUSTOMERPROFILEACCESS,String.valueOf(consumerProfile.getId()));
                forward = mapping.findForward(FORWARD_SUCCESS);
            }
        } else {
            forward = mapping.findForward(FORWARD_FAILURE);
        }

        return forward;
    }

    private String checkMasterOnlyTaint(ConsumerProfile cp) {

        if (cp.isSsnBlocked() || cp.isPhoneBlocked() || cp.isPhoneAlternateBlocked() || cp.isAccountBlocked() ||
       		cp.isEmailBlocked() || cp.isEmailDomainBlocked() || cp.isPhoneBlocked() ||
       		cp.isPhoneAlternateBlocked()) {
            return "N";
        }
        else if (cp.getConsumerMasterTaintCode().isBlocked()) return "Y";

    	return "N";
    }

    private void populateForm(ShowCustomerProfileForm form,
            ConsumerProfile consumerProfile, UserProfile userProfile) {
        int custId = consumerProfile.getId();
        form.setCustId(String.valueOf(custId));
        form.setIdExtnlId(consumerProfile.getIdExtnlId());
        form.setIdNumber(consumerProfile.getIdNumber());
        {
        	if(consumerProfile.getIdNumber() != null){//Wo74: Added for MGO 6579
	        	try {
	        		DecryptionService ds = emgadm.services.ServiceFactory.getInstance().getDecryptionService();
	        		String id = ds.decryptAdditionalId(consumerProfile.getIdNumber());
	        		form.setIdNumber(id);
	        	} catch (Exception e){
	        		// There was an error, for now just show the id as it is.
	        		System.err.println("The decryption of the idNumber for customer " + consumerProfile.getId() + " failed. " + e.getMessage());
	        		form.setIdNumber(consumerProfile.getIdNumber());
	        	}
        	}
        }
        form.setDocStatus(consumerProfile.getDocStatus());

        if (consumerProfile.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {

 			try {
				ConsumerServiceProxy proxy = ConsumerServiceProxyImpl.getInstance();
				GetConsumerIDApprovalStatusResponse resp = null;
		        String consumerIdText = form.getCustId();
				resp = proxy.getConsumerIDApprovalStatusResponse(Long.parseLong(consumerIdText),
						consumerProfile.getPartnerSiteId());

	    		form.setIsIdImageAvailable(false);
	    		// Linked is displayed when document is in the following states:
	    		// SUBmitted, APProved, PENding, or DENied.
	    		if (resp.getStatus().equals("SUB") ||
	    			resp.getStatus().equals("APP") ||
	    			resp.getStatus().equals("PEN") ||
	    			resp.getStatus().equals("DEN")) {

	    			form.setIsIdImageAvailable(true);

	    		}
 			} catch (Exception e) {
 				e.printStackTrace();
 			}
        }

        form.setCustFirstName(consumerProfile.getFirstName());
        form.setIdType(consumerProfile.getIdType());
        form.setIdTypeFriendlyName(consumerProfile.getIdTypeFriendlyName());
        form.setCustLastName(consumerProfile.getLastName());
        form.setCustSecondLastName(consumerProfile.getSecondLastName());
        form.setCustLogonId(consumerProfile.getUserId());
        form.setCustMiddleName(consumerProfile.getMiddleName());
        form.setCustHashedPassword(consumerProfile.getPswdText());
        form.setAdaptiveAuthProfileCompleteDate(consumerProfile.getAdaptiveAuthProfileCompleteDate());
        form.setCustStatus(consumerProfile.getStatus().getCombinedCode());
        form.setCreatedIPAddress(consumerProfile.getCreateIpAddrId());
        form.setPrmrCode(consumerProfile.getCustPrmrCode());
        form.setPrmrCodeDate(consumerProfile.getCustPrmrDate());
        form.setCustPrmrCodes(custPrmrCodes);
        form.setProfileLoginLocked(consumerProfile.getProfileLoginLocked());
        form.setEDirectoryGuid(consumerProfile.getEdirGuid());
        form.setGender(consumerProfile.getGender());
        DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
        if (consumerProfile.getBirthdate() != null) {
            form.setBrthDate(df.format(consumerProfile.getBirthdate()));
        } else {
            form.setBrthDate("");
        }
        form.setCustAge(DateHelper.calcAge(consumerProfile.getBirthdate()));
        form.setAddressLine1(consumerProfile.getAddressLine1());
        form.setAddressLine2(consumerProfile.getAddressLine2());
        form.setBuildingName(consumerProfile.getBuildingName());
        form.setCity(consumerProfile.getCity());
        form.setIsoCountryCode(consumerProfile.getIsoCountryCode());
        form.setLoyaltyPgmMembershipId(consumerProfile.getLoyaltyPgmMembershipId());
        form.setPhoneNumber(consumerProfile.getPhoneNumber());

        if (consumerProfile.isPhoneBlocked()) {
            form.setPhoneNumberTaintText(TaintIndicatorType.BLOCKED_TEXT);
        } else if (consumerProfile.isPhoneOverridden()) {
            form.setPhoneNumberTaintText(TaintIndicatorType.OVERRIDE_TEXT);
        } else if (consumerProfile.isPhoneNotBlocked()) {
            form.setPhoneNumberTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
        } else {
            form.setPhoneNumberTaintText("Unknown");
        }

        form.setPhoneNumberAlternate(consumerProfile.getPhoneNumberAlternate());

        if (consumerProfile.isPhoneAlternateBlocked()) {
            form
                    .setPhoneNumberAlternateTaintText(TaintIndicatorType.BLOCKED_TEXT);
        } else if (consumerProfile.isPhoneAlternateOverridden()) {
            form
                    .setPhoneNumberAlternateTaintText(TaintIndicatorType.OVERRIDE_TEXT);
        } else if (consumerProfile.isPhoneAlternateNotBlocked()) {
            form
                    .setPhoneNumberAlternateTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
        } else {
            form.setPhoneNumberAlternateTaintText("Unknown");
        }

        form.setPostalCode(consumerProfile.getPostalCode());
        form.setState(consumerProfile.getState());
        form.setZip4(consumerProfile.getZip4());
        form.setCountyName(consumerProfile.getCountyName());
        form.setInvlLoginTryCnt(consumerProfile.getInvlLoginTryCnt());

        form.setNegativeAddress(null);

        ConsumerEmail consumerEmail = null;
        //		Set emails = new HashSet();
        for (Iterator iter = consumerProfile.getEmails().iterator(); iter
                .hasNext();) {
            consumerEmail = (ConsumerEmail) iter.next();
            form.setEmailAddress(consumerEmail.getConsumerEmail());
        }
        if (form.getEmailAddress() == null) {
            form.setEmailAddress("Unknown Email");
        }
        form.setAcceptPromotionalEmail(consumerProfile
                .isAcceptPromotionalEmail());

        if (consumerProfile.isEmailBlocked()) {
            form.setEmailAddressTaintText(TaintIndicatorType.BLOCKED_TEXT);
        } else if (consumerProfile.isEmailNotBlocked()) {
            form.setEmailAddressTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
        } else {
            form.setEmailAddressTaintText("Unknown");
        }

        if (consumerProfile.isEmailDomainBlocked()) {
            form.setEmailDomainTaintText(TaintIndicatorType.BLOCKED_TEXT);
        } else if (consumerProfile.isEmailDomainNotBlocked()) {
            form.setEmailDomainTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
        } else {
            form.setEmailDomainTaintText("Unknown");
        }

        if (consumerProfile.isEmailActive()) {
            form.setIsEmailActive(true);
        } else {
            form.setIsEmailActive(false);
        }

        form.setCustSsn("XXX-XX-" + consumerProfile.getSsnLast4());

        if (consumerProfile.isSsnBlocked()) {
            form.setCustSsnTaintText(TaintIndicatorType.BLOCKED_TEXT);
        } else if (consumerProfile.isSsnOverridden()) {
            form.setCustSsnTaintText(TaintIndicatorType.OVERRIDE_TEXT);
        } else if (consumerProfile.isSsnNotBlocked()) {
            form.setCustSsnTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
        } else {
            form.setCustSsnTaintText("Unknown");
        }

        try {
            Map m = new HashMap(profileService.getConsumerStatusDescriptions());
            List custStatusOptions = convertMapToLabelValueListSorted(m);
            for (Object obj : custStatusOptions) {
            	String s = obj.toString();
            	s.toString();
            }
            form.setCustStatusOptions(custStatusOptions);
            form.setAccountStatusOptions(convertMapToLabelValueListSorted(accountService.getAccountStatusDescriptions()));
        } catch (Exception e2) {
            throw new RuntimeException("DataSource Exception!");
        }

        List bankAccounts = new ArrayList();
        List creditCardAccounts = new ArrayList();
        List debitCardAccounts = new ArrayList();
        List maestroAccounts = new ArrayList();
        Set accounts;
        try {
            //  also get account comments
            accounts = accountService.getAllAccountsByConsumerIdWithCmnts(custId);
        } catch (Exception e3) {
            throw new RuntimeException(
                    "Exception for getting all consumer accounts");
        }
        for (Iterator iter = accounts.iterator(); iter.hasNext();) {
            ConsumerAccount account = (ConsumerAccount) iter.next();
            ConsumerAccountType type = account.getAccountType();
            if (type.isBankAccount()) {
                bankAccounts.add(account);
            } else if (type.isCreditCardAccount()) {
                creditCardAccounts.add(account);
            } else if (type.isMaestroDebitCardAccount()){
            	maestroAccounts.add(account);            	
            }
            /*
             * Commenting out as this won't be used due to CyberSource not being
             * able to provide us an accurate list of cards types. else if
             * (type.isDebitCardAccount()) { debitCardAccounts.add(account); }
             */
            if (!consumerProfile.isAccountBlocked()) {
                if (account.isAccountNumberBlocked()
                        || account.isBankAbaNumberBlocked()
                        || account.isCreditCardBinBlocked()) {
                    consumerProfile.setAccountTaintCode(TaintIndicatorType.BLOCKED);
                } else if (account.isAccountNumberOverridden()
                        || account.isBankAbaNumberOverridden()
                        || account.isCreditCardBinOverridden()) {
                    consumerProfile.setAccountTaintCode(TaintIndicatorType.OVERRIDE);
                } else {
                    if (!consumerProfile.getAccountTaintCode().isOverridden()) {
                        consumerProfile.setAccountTaintCode(TaintIndicatorType.NOT_BLOCKED);
                    }
                }
            }
        }

        bankAccountArr = createBankAccountViews(bankAccounts);
        form.setBankAccounts(bankAccountArr);

        creditAccountArr = createCreditCardViews(creditCardAccounts);
        form.setCreditCardAccounts(creditAccountArr);

        debitAccountArr = createCreditCardViews(debitCardAccounts);
        form.setDebitCardAccounts(debitAccountArr);
        
        maestroAccountArr = createCreditCardViews(maestroAccounts);
        form.setMaestroAccounts(maestroAccountArr);

        try {
            form.setCustomerCommentReasons(convertMapToLabelValueListSorted(profileService.getConsumerCommentReasons()));
        } catch (Exception e4) {
            throw new RuntimeException("Exception in getting comment reasons");
        }

        // Comments
        try {
        	// Get the comments
        	List currentComments = profileService.getConsumerComments(custId, userProfile.getUID());

        	// Get the create date from the DB
        	Calendar dbCreateDate = Calendar.getInstance();
        	dbCreateDate.setTime(consumerProfile.getCreateDate());

        	// If the create date is equal to or greater than the date of the solution
        	// and there is not a comment for the IP lookup process (Added by: Jorge.Sayago, sprint 25, defect 255, 18/08/2011)
        	if( (dbCreateDate. after(IPLOOKUP_STARTDATE) || dbCreateDate.equals(IPLOOKUP_STARTDATE))
        		&& !isThereAnIpLookupReg(currentComments)) { // Add a comment with the IP Lookup result
        		// Call
            	String IPLookup_MDValue = dynProps.getIPLookup_MD();
            	// Test
    			if (IPLookup_MDValue != null && !IPLookup_MDValue.equals("disabled"))
    			{
    				// Create a new comment object
    				boolean isIpLookuponScoring = false;
    				ConsumerProfileComment comment = new ConsumerProfileComment();
    				comment.setCustId(consumerProfile.getId());
    				comment.setReasonCode("OTH");
    				comment.setCreateDate(new Date());
    				comment.setCreatedBy("emgwww");
    				String cmmnt = IPLocatorService.getFormattedStringForIpLookup(consumerProfile.getCreateIpAddrId(), isIpLookuponScoring);
    				comment.setText(cmmnt);
    				// Persist
    				ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
    				profileService.addConsumerProfileComment(comment, "emgwww");
    				// Add to the object
    				boolean add = currentComments.add(comment);
    			}
        	}
            form.setComments(createConsumerProfileCommentViews(currentComments));
        } catch (Exception e5) {
            throw new RuntimeException("Exception in getting consumer comments");
        }
        try {
            form.setConsumerProfileActivity(createConsumerProfileActivityView(profileService.getConsumerActivityLogs(custId, userProfile.getUID(), null, null, null)));
        } catch (Exception e6) {
            throw new RuntimeException("Exception in getting consumer activities");
        }

        List epTransactions = new LinkedList();
        List mgTransactions = new LinkedList();
        List esTransactions = new LinkedList();
        List cashTransactions = new LinkedList();
        List dsTransactions = new LinkedList();
        TransactionSearchRequest searchCriteria = new TransactionSearchRequest();
        searchCriteria.setConsumerId(new Integer(custId));
        List transactions;

        try {
            transactions = tm.getTransactionsWithCmnts(searchCriteria, userProfile.getUID());
        } catch (Exception e1) {
            throw new EMGRuntimeException(e1);
        }

        for (Iterator iter = transactions.iterator(); iter.hasNext();) {
            Transaction transaction = (Transaction) iter.next();
            String type = transaction.getEmgTranTypeCode();
            transaction.setSndCustFrstName(consumerProfile.getFirstName());
            transaction.setSndCustLastName(consumerProfile.getLastName());
            if (TransactionType.EXPRESS_PAYMENT_SEND_CODE.equalsIgnoreCase(type)) {
                epTransactions.add(transaction);
            } else if (TransactionType.MONEY_TRANSFER_SEND_CODE.equalsIgnoreCase(type)) {
                mgTransactions.add(transaction);
            } else if (TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equalsIgnoreCase(type)) {
                esTransactions.add(transaction);
            } else if (TransactionType.MONEY_TRANSFER_CASH_TRANSACTION_CODE.equalsIgnoreCase(type)) {
                cashTransactions.add(transaction);
            } else if (TransactionType.DELAY_SEND_CODE.equalsIgnoreCase(type)) {
            	dsTransactions.add(transaction);
            }
        }
        form.setEpTransactions(createTransactionViews(epTransactions));
        form.setMgTransactions(createTransactionViews(mgTransactions));
        form.setEsTransactions(createTransactionViews(esTransactions));
        form.setCashTransactions(createTransactionViews(cashTransactions));
        form.setDsTransactions(createTransactionViews(dsTransactions));
        form.setPartnerSiteId(consumerProfile.getPartnerSiteId());
    }

//    private GetConsumerIDImageRequest createConsumerIDImageRequest(int imageId) throws Exception {
//    	GetConsumerIDImageRequest idImageRequest = new GetConsumerIDImageRequest();
//
//    	idImageRequest.setCustomerExternalId(imageId);
//
//		Header header = new Header();
//		ProcessingInstruction pi = new ProcessingInstruction();
//		pi.setAction("getConsumerIDImage_v2");
//		pi.setReadOnlyFlag(false);
//
//		header.setProcessingInstruction(pi);
//		ClientHeader ch = new ClientHeader();
//
//		//FIXMe how do we generate the request Id?
//		ch.setClientRequestID("123");
//		header.setClientHeader(ch);
//		idImageRequest.setHeader(header);
//		return idImageRequest;
//    }

    private ConsumerBankAccountView[] createBankAccountViews(List bankAccounts) {
        ConsumerBankAccountView[] views = new ConsumerBankAccountView[bankAccounts == null ? 0
                : bankAccounts.size()];
        if (bankAccounts != null) {
            int i = 0;
            for (Iterator iter = bankAccounts.iterator(); iter.hasNext(); ++i) {
                ConsumerBankAccount account = (ConsumerBankAccount) iter.next();
                views[i] = new ConsumerBankAccountView(account);
                views[i].setMicroDeposit(setMDBean(account.getId()));
            }
        }
        return views;
    }

    private MicroDeposit setMDBean(int acctId) {
        MicroDeposit md = new MicroDeposit();
        md.setCustAccountId(acctId);
        md.setCustAccountVersionNbr(1);
        Iterator iter;
        try {
            iter = mds.getMicroDepositValidations(md).iterator();
        } catch (DataSourceException e) {
            throw new EMGRuntimeException(e);
        }
        md = null;
        while (iter.hasNext()) {
            md = (MicroDeposit) iter.next();
        }
        return md;
    }

    private ConsumerCreditCardAccountView[] createCreditCardViews(
            List creditCardAccounts) {
        ConsumerCreditCardAccountView[] views = new ConsumerCreditCardAccountView[creditCardAccounts == null ? 0
                : creditCardAccounts.size()];
        if (creditCardAccounts != null) {
            int i = 0;
            for (Iterator iter = creditCardAccounts.iterator(); iter.hasNext(); ++i) {
                ConsumerCreditCardAccount account = (ConsumerCreditCardAccount) iter
                        .next();
                views[i] = new ConsumerCreditCardAccountView(account);
            }
        }
        return views;
    }

    private ConsumerProfileCommentView[] createConsumerProfileCommentViews(
            List consumerProfileComments) {
        ConsumerProfileCommentView[] views = new ConsumerProfileCommentView[consumerProfileComments == null ? 0
                : consumerProfileComments.size()];
        if (consumerProfileComments != null) {
            int i = 0;
            for (Iterator iter = consumerProfileComments.iterator(); iter
                    .hasNext(); ++i) {
                ConsumerProfileComment comment = (ConsumerProfileComment) iter
                        .next();
                views[i] = new ConsumerProfileCommentView(comment);
            }
        }
        return views;
    }


    private ConsumerProfileActivity[] createConsumerProfileActivityView(List cpActy) {
    	ConsumerProfileActivity[] views = new ConsumerProfileActivity[cpActy == null ? 0 : cpActy.size()];
        if (cpActy != null) {
            int i = 0;
            for (Iterator iter = cpActy.iterator(); iter.hasNext(); ++i) {
                ConsumerProfileActivity acty = (ConsumerProfileActivity) iter.next();
                views[i] = acty;
            }
        }
        return views;
    }

    private void doIpLookup(IPHistorySummary iphs) {
    	try {
    		boolean isIpLookuponScoring = false;
        	ResponseArray response = IPLocatorService.ipLocate(isIpLookuponScoring,iphs.getIpAddress());
        	ResponseRecord record = response.getRecord(0);
        	iphs.setCity(record.getIP().getCity().getName());
        	iphs.setCountry(record.getIP().getCountry().getName());
        	iphs.setISP(record.getIP().getISP().getName());
        	iphs.setState(record.getIP().getRegion());
        	iphs.setZipCode(record.getIP().getZip());
        } catch(Exception e) {
        	String message = e.getMessage();
        	// Some info could not be retrieved, move on.
        }
    }

    private TransactionView[] createTransactionViews(List transactions) {
        TransactionView[] views = new TransactionView[transactions == null ? 0
                : transactions.size()];
        if (transactions != null) {
            int i = 0;
            for (Iterator iter = transactions.iterator(); iter.hasNext(); ++i) {
                Transaction transaction = (Transaction) iter.next();
                views[i] = new TransactionView(transaction);
            }
        }
        return views;
    }

    private void populateIPHistory(HttpServletRequest request,
            ConsumerProfile cp, UserProfile up) {

        List newList = new ArrayList();
        Iterator iter;
        try {
            iter = tm.getIpHistorySummaryList(cp.getUserId(), "WEB").iterator();
        } catch (Exception e) {
            throw new RuntimeException("Exception in getting IP history");
        }

        while (iter.hasNext()) {
            IPHistorySummary iphs = (IPHistorySummary) iter.next();
            Collection col;
            try {
                col = fraudService.getBlockedIPs(up.getUID(), iphs
                        .getIpAddress(), null);
            } catch (Exception e1) {
                throw new RuntimeException("Exception in getting blocked IPs");
            }

            iphs.setIpAddressTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
            if (col != null && col.size() != 0) {
                BlockedIP bip = (BlockedIP) col.iterator().next();
                if (TaintIndicatorStatus.BLOCKED_STATUS.toString().equals(
                        bip.getStatusCode())) {
                    iphs.setIpAddressTaintText(TaintIndicatorType.BLOCKED_TEXT);
                }
            }

            //*********************
            //AJP: Prod bug where user has a lot of IP history - page takes huge amt of time to load
            //THis action should be based on EOC user clicking something to get the ISP info
            //doIpLookup(iphs);
            //*******************************************
            newList.add(iphs);
        }

        request.setAttribute("ipHistorySummaryList", newList);
        request.setAttribute("url",
                request.getContextPath() + "/showIPHistoryDetail.do?logonId="
                        + cp.getUserId() + "&custId="
                        + String.valueOf(cp.getId()));
    }

    private void setTaintFlag(HttpServletRequest request, ConsumerProfile cp) {

        request.setAttribute("phoneTainted", "N");
        request.setAttribute("isTainted", "N");

        if (cp.isSsnBlocked() || cp.isPhoneBlocked()
                || cp.isPhoneAlternateBlocked() || cp.isAccountBlocked()
                || cp.isEmailBlocked() || cp.isEmailDomainBlocked()) {
            request.setAttribute("isTainted", "Y");
        }

        if (cp.isPhoneBlocked() || cp.isPhoneAlternateBlocked()) {
            request.setAttribute("phoneTainted", "Y");
        }

        return;
    }

    private void setSecurityFlags(HttpServletRequest request, UserProfile up,
            ShowCustomerProfileForm form) {
        if (up.hasPermission("updateCustomerPassword")) {
            request.setAttribute("updatePw", "Y");
        }

        if (up.hasPermission("duplicateProfileOverride"))
        	request.setAttribute("duplicateProfileOverride", "Y");

        if (up.hasPermission("clearSSNTaint")) {
            request.setAttribute("clearSSNTaint", "Y");
        }
        if (up.hasPermission("clearMasterTaint")) {
            request.setAttribute("clearMasterTaint", "Y");
        }

        if (up.hasPermission("recreateLDAPEntry")) {
            request.setAttribute("recreateLDAPEntry", "Y");
        }

        if (up.hasPermission("updateCustomerSsn")) {
            request.setAttribute("updateSSN", "Y");
        }

        if (up.hasPermission("addSSNBlockedList")) {
            request.setAttribute("addBlockedSSN", "Y");
        }

        if (up.hasPermission("updateCustomerStatus")) {
            request.setAttribute("updateStatus", "Y");
        }

        if (up.hasPermission("updateCustomerProfile")) {
            request.setAttribute("updateProfile", "Y");
        }

        if (up.hasPermission("updateCustomerEmail")) {
            request.setAttribute("updateEmail", "Y");
        }

        if (up.hasPermission("updateCustomerPromo")) {
            request.setAttribute("updatePromo", "Y");
        }

        if (up.hasPermission("addCustomerComment")) {
            request.setAttribute("addCustCmt", "Y");
        }

        if (up.hasPermission("updateAccountStatus")) {
            request.setAttribute("updateAcct", "Y");
        }

        if (up.hasPermission("findSimilarProfiles")) {
            request.setAttribute("searchAuth", "Y");
        }

        if (form.getComments().length == 0) {
            request.setAttribute("noComments", "Y");
        }

        if (form.getConsumerProfileActivity().length == 0) {
            request.setAttribute("noActivities", "Y");
        }

        if (form.getBankAccounts().length == 0) {
            request.setAttribute("noBankAccts", "Y");
        }

        if (form.getCreditCardAccounts().length == 0) {
            request.setAttribute("noCreditAccts", "Y");
        }

        if (form.getMaestroAccounts().length == 0) {
            request.setAttribute("noMaestroAccts", "Y");
        }

        if (form.getDebitCardAccounts().length == 0) {
            request.setAttribute("noDebitAccts", "Y");
        }

        if (form.getEpTransactions().length == 0) {
            request.setAttribute("noEPTrans", "Y");
        }

        if (form.getEsTransactions().length == 0) {
            request.setAttribute("noESTrans", "Y");
        }

        if (form.getCashTransactions().length == 0) {
            request.setAttribute("noCashTrans", "Y");
        }

        if (form.getMgTransactions().length == 0) {
            request.setAttribute("noMGTrans", "Y");
        }

        if (form.getDsTransactions().length == 0) {
            request.setAttribute("noDSTrans", "Y");
        }

        if (up.hasPermission("editPhoneBlockedList")) {
            request.setAttribute("addBlockedPhone", "Y");
        }

        if (up.hasPermission("addEmailBlockedList")) {
            request.setAttribute("addBlockedEmail", "Y");
        }

        if (up.hasPermission("addIPBlockedList")) {
            request.setAttribute("addBlockedIp", "Y");
        }

        if (up.hasPermission("validateMicroDeposit")) {
            request.setAttribute("validateMD", "Y");
        }

        if (up.hasPermission("showDashboardDetail")) {
            request.setAttribute("dashDetail", "Y");
        }

        if (up.hasPermission("updateCustPremierCode")) {
            request.setAttribute("updatePrmrCode", "Y");
        }

        if (up.hasPermission("approveDocument")) {
            request.setAttribute("appDocument", "Y");
        }

        if (up.hasPermission("denyDocument")) {
            request.setAttribute("denDocument", "Y");
        }

        if (up.hasPermission("pendDocument")) {
            request.setAttribute("penDocument", "Y");
        }

        if (up.hasPermission(EMoneyGramAdmForwardConstants
				.LOCAL_FORWARD_ADD_NEGATIVE_TERM_LIST)) {
            request.setAttribute("addNegativeAddress", "Y");
        }
	}

	static String BASIC_FLAG = "basicFlag";

	private void setFlag(HttpServletRequest request,
			ShowCustomerProfileForm form) {
		if (form.getBasicFlag() == null) {
			Map<String, String> transferMap = (Map<String, String>) request
					.getSession().getAttribute(BASIC_FLAG);
			String tmpFlag = null;
			if (transferMap != null)
				if (transferMap.containsKey(BASIC_FLAG))
					tmpFlag = transferMap.get(BASIC_FLAG);
			if (tmpFlag == null) {
				form.setBasicFlag("N");
			} else {
				form.setBasicFlag(tmpFlag);
			}
		}

		if (form.getBasicFlag() != null) {
			Map<String, String> transferMap = new HashMap();
			transferMap.put(BASIC_FLAG, form.getBasicFlag());
			request.getSession().setAttribute(BASIC_FLAG, transferMap);
		}
	}

    /***
     *
     * @param currentComments
     * @return
     */
    private boolean isThereAnIpLookupReg(List<ConsumerProfileComment> currentComments) {
    	// Variables
    	boolean found = false;
    	// Loop
    	for(ConsumerProfileComment cPC : currentComments) {
    		// Test the reasonCode
    		if(cPC.getReasonCode().equals("OTH")) {
    			// Get the text
    			String txt = cPC.getText();
    			// Test
    			if(txt.indexOf("IP Address:") == 0) {
    				// There is a register, then exit
    				found = true;
    				break;
    			}
    		}
    	}
    	// Return
    	return found;
    }
}