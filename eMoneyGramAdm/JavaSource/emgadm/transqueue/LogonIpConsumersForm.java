package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.internaluseradmin.EMoneyGramAdmBaseUserAdminForm;

public class LogonIpConsumersForm extends EMoneyGramAdmBaseUserAdminForm {
	private String ipAddress;

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String string) {
		ipAddress = string;
	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1) {
		super.reset(arg0, arg1);
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if (getAdd() != null || getEdit() != null) {
			return super.validate(mapping, request);
		}
		
		return errors;
	}
}
