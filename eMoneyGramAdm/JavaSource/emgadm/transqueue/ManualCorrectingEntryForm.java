package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ManualCorrectingEntryForm extends EMoneyGramAdmBaseValidatorForm {

	private String tranId;
	private String first;
	private String debitAcct;
	private String creditAcct;
	private String amount;
	private String cmntText;
	private String submitCorrection;
	private String submitCancel;

	public String getTranId() {
		return tranId;
	}

	public void setTranId(String string) {
		tranId = string;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String string) {
		first = string;
	}

	public String getDebitAcct() {
		return debitAcct;
	}

	public void setDebitAcct(String string) {
		debitAcct = string;
	}

	public String getCreditAcct() {
		return creditAcct;
	}

	public void setCreditAcct(String string) {
		creditAcct = string;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String string) {
		amount = string;
	}

	public String getCmntText() {
		return cmntText == null ? "" : cmntText;
	}

	public void setCmntText(String string) {
		cmntText = string;
	}

	public String getSubmitCorrection() {
		return submitCorrection;
	}

	public void setSubmitCorrection(String string) {
		submitCorrection = string;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public ActionErrors validate(ActionMapping arg0, HttpServletRequest arg1) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(arg0, arg1);

		if (StringUtils.isNotBlank(submitCorrection)) {
			if (StringHelper.isNullOrEmpty(amount)) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "amount"));
			}
			
			try {
				float amt = Float.parseFloat(amount);
				if (amt == 0F) {
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("errors.invalid.amount", "Zero"));					
				}
				if (amt < 0F) {
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("errors.invalid.amount", "Negative amount"));					
				}
				
			} catch (Exception e) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.float", "amount"));				
			}
			
			if (debitAcct.equals("0")) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.must.select.an.account", "debit"));
			}
			
			if (creditAcct.equals("0")) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.must.select.an.account", "credit"));
			}
			
			if ((!debitAcct.equals("0") && !creditAcct.equals("0")) &&
				creditAcct.equals(debitAcct)) {
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.cannot.be.same.account"));
			}
			
			if (!StringHelper.isNullOrEmpty(cmntText)
				&& cmntText.length() > 255) {
				String[] parm = { "Comments", "255" };
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.length.too.long", parm));
			}
			
			if (StringHelper.isNullOrEmpty(cmntText)){
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "Comment"));
			}
			
		}
		
		return errors;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		debitAcct = "0";
		creditAcct = "0";
		amount = "0.00";
		cmntText = "";
		submitCorrection = null;
		submitCancel = null;
	}
}
