package emgadm.transqueue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.IPHistoryDetail;
import emgadm.util.StringHelper;

/**
 * @version 	1.0
 * @author
 */
public class ShowIPHistoryDetailAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
			
		ShowIPHistoryDetailForm form = (ShowIPHistoryDetailForm) f;
		TransactionManager tm = getTransactionManager(request);
		List list = tm.getIpHistoryDetailList(form.getLogonId(), "WEB");
		if (!StringHelper.isNullOrEmpty(form.getIpAddress())) {
			list = filter(list, form.getIpAddress());
		}
		request.setAttribute("ipHistoryDetailList", list);		
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
	
	private List filter(List list, String ip) {
		List newList = new ArrayList();
		Iterator iter = list.iterator();
		while (iter.hasNext()) {
			IPHistoryDetail ihd = (IPHistoryDetail) iter.next();
			if (ihd.getIpAddress().equals(ip)) {
				newList.add(ihd);
			}
		}
		return newList;
	}
}
