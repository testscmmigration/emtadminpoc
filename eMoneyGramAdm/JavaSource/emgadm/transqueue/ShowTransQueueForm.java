package emgadm.transqueue;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.UserProfile;
import emgadm.util.DateHelper;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class ShowTransQueueForm extends EMoneyGramAdmBaseValidatorForm {
	private String appP2PCount = null;
	private String appEPCount = null;
	private String appESCount = null;
	private String procP2PCount = null;
	private String procEPCount = null;
	private String procESCount = null;
	private String procDOCCount = null;
	private String procDOCPENCount = null;
	private String docStatus = null;
	private String docStatusString = null;
	private String sendESCount = null;
	private String appDSCount = null;
	private String procDSCount = null;
	private String sendDSCount = null;
	private String inProgressCount = null;
	private String exceptionCount = null;
	private String beginDateText = null;
	private String endDateText = null;
	private String type = null;
	private String status = null;
	private String fundStatus = null;
	private String ownedBy = null;
	private String sortBy = null;
	private String sortSeq = null;
	private String useScore = null;
	private String lowScore = null;
	private String highScore = null;
	private String submitRefresh = null;
	private String submitESSend = null;
	private String submitESDeny = null;
	private String partnerSiteId = null;
	private String recordCount = "0";
	private String[] selTrans;
	private final String color = "light-blue";

	public String getAppP2PCount() {
		return appP2PCount;
	}

	public void setAppP2PCount(String string) {
		appP2PCount = string;
	}

	public String getAppEPCount() {
		return appEPCount;
	}

	public void setAppEPCount(String string) {
		appEPCount = string;
	}

	public String getProcP2PCount() {
		return procP2PCount;
	}

	public void setProcP2PCount(String string) {
		procP2PCount = string;
	}

	public String getProcEPCount() {
		return procEPCount;
	}

	public void setProcEPCount(String string) {
		procEPCount = string;
	}

	public String getInProgressCount() {
		return inProgressCount;
	}

	public void setInProgressCount(String string) {
		inProgressCount = string;
	}

	public String getExceptionCount() {
		return exceptionCount;
	}

	public void setExceptionCount(String string) {
		exceptionCount = string;
	}

	public String getBeginDateText() {

		return beginDateText;
	}

	public String getEndDateText() {
		return endDateText;
	}

	public String getFundStatus() {
		return fundStatus;
	}

	public String getOwnedBy() {
		if (ownedBy != null && ownedBy.equalsIgnoreCase("<None>")) {
			return "";
		}

		return ownedBy;
	}

	public String getSortBy() {
		return sortBy;
	}

	public String getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

	public void setType(String string) {
		type = string;
	}

	public String getSubmitRefresh() {
		return submitRefresh;
	}

	public void setBeginDateText(String string) {
		beginDateText = string;
	}

	public void setEndDateText(String string) {
		endDateText = string;
	}

	public void setFundStatus(String string) {
		fundStatus = string;
	}

	public void setOwnedBy(String string) {
		ownedBy = string;
	}

	public void setSortBy(String string) {
		sortBy = string;
	}

	public void setStatus(String string) {
		status = string;
	}

	public void setSubmitRefresh(String string) {
		submitRefresh = string;
	}

	public String getSortSeq() {
		return sortSeq;
	}

	public void setSortSeq(String string) {
		sortSeq = string;
	}

	public String getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(String string) {
		recordCount = string;
	}

	public String getAppESCount() {
		return appESCount;
	}

	public void setAppESCount(String string) {
		appESCount = string;
	}

	public String getProcESCount() {
		return procESCount;
	}

	public void setProcESCount(String string) {
		procESCount = string;
	}

	public String getSendESCount() {
		return sendESCount;
	}

	public void setSendESCount(String string) {
		sendESCount = string;
	}

	public String getColor() {
		return color;
	}

	public String getUseScore() {
		return useScore;
	}

	public void setUseScore(String string) {
		useScore = string;
	}

	public String getLowScore() {
		return lowScore;
	}

	public void setLowScore(String string) {
		lowScore = string;
	}

	public String getHighScore() {
		return highScore;
	}

	public void setHighScore(String string) {
		highScore = string;
	}

	public String[] getSelTrans() {
		return selTrans;
	}

	public void setSelTrans(String[] strings) {
		selTrans = strings;
	}

	public String getSubmitESSend() {
		return submitESSend;
	}

	public void setSubmitESSend(String string) {
		submitESSend = string;
	}

	public String getSubmitESDeny() {
		return submitESDeny;
	}

	public void setSubmitESDeny(String string) {
		submitESDeny = string;
	}

	public String getAppDSCount() {
		return appDSCount;
	}

	public void setAppDSCount(String appDSCount) {
		this.appDSCount = appDSCount;
	}

	public String getProcDSCount() {
		return procDSCount;
	}

	public void setProcDSCount(String procDSCount) {
		this.procDSCount = procDSCount;
	}

	public String getSendDSCount() {
		return sendDSCount;
	}

	public void setSendDSCount(String sendDSCount) {
		this.sendDSCount = sendDSCount;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		UserProfile up = (UserProfile) request.getSession().getAttribute(
				EMoneyGramAdmSessionConstants.USER_PROFILE);
		// for thread-safe, DateFormat is not a thread-safe class
		synchronized (this) {
			DateFormat dateFormatter = DateHelper.getFormatter("dd/MMM/yyyy",
					up.getTimeZone());
			if (docStatus != null && !docStatus.equals("")) {
				long now = System.currentTimeMillis();
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				setBeginDateText(df.format(new Date(now)));
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(now));
				cal.add(Calendar.DATE, 2);
				setEndDateText(df.format(cal.getTime()));
			} else {
				beginDateText = dateFormatter.format(new Date(System
						.currentTimeMillis() - 7 * 24 * 60 * 60 * 1000));
				// one month before today
				endDateText = dateFormatter.format(new Date(System
						.currentTimeMillis() + 18 * 60 * 60 * 1000));
				// today
			}
		}
		type = "All";
		status = "FFS";
		fundStatus = "All";
		ownedBy = "All";
		sortBy = "transStatusDate";
		sortSeq = "A";
		useScore = "N";
		lowScore = "0";
		highScore = "0";
		submitRefresh = null;
		submitESSend = null;
		submitESDeny = null;
		recordCount = "0";
		selTrans = null;
		partnerSiteId = null;
		docStatus = null;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if ((errors == null || errors.size() == 0)
				&& (!StringHelper.isNullOrEmpty(submitESSend) || !StringHelper
						.isNullOrEmpty(submitESDeny))
				&& (selTrans == null || selTrans.length == 0)) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
					"error.no.transaction.checked"));
		}

		if ((errors == null || errors.size() == 0)
				&& !StringHelper.isNullOrEmpty(submitRefresh)) {
			DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy", true);
			try {
				if (dfmt.parse(beginDateText).after(dfmt.parse(endDateText))) {
					String[] parms = { beginDateText, endDateText };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"error.begin.date.later.than.end.date", parms));
				}
			} catch (ParseDateException e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
						"error.malformed.date"));
			}

			if ("Y".equalsIgnoreCase(useScore)) {
				if (StringHelper.isNullOrEmpty(lowScore)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.required", "Low score"));
				} else if (StringHelper.containsNonDigits(lowScore)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.integer", "Low score"));
				} else if (lowScore.length() > 4) {
					String[] parms = { "Low score", "4" };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.maxlength", parms));
				}

				if (StringHelper.containsNonDigits(highScore)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.required", "High score"));
				} else if (StringHelper.containsNonDigits(highScore)) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.integer", "High score"));
				} else if (highScore.length() > 4) {
					String[] parms = { "High score", "4" };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.maxlength", parms));
				}

				if ((errors == null || errors.size() == 0)
						&& Integer.parseInt(lowScore) > Integer
								.parseInt(highScore)) {

					String[] parms = { "Low score", "High Score" };
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
							"errors.smaller.greater.than.larger", parms));
				}
			}
		}

		return errors;
	}

	public String getProcDOCCount() {
		return procDOCCount;
	}

	public void setProcDOCCount(String procDOCCount) {
		this.procDOCCount = procDOCCount;
	}

	public String getProcDOCPENCount() {
		return procDOCPENCount;
	}

	public void setProcDOCPENCount(String procDOCPENCount) {
		this.procDOCPENCount = procDOCPENCount;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocStatusString() {
		return EMoneyGramAdmApplicationConstants.docStatusMap
				.get(getDocStatus());
	}

	public void setDocStatusString(String docStatusString) {
		this.docStatusString = docStatusString;
	}
}
