package emgadm.transqueue;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ACHReturnManager;
import emgadm.model.AchReturn;
import emgadm.model.AchReturnSearchRequest;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class ShowACHReturnQueueAction extends EMoneyGramAdmBaseAction
{
//	private static EMTSharedDynProperties dynProps =
//		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

//		ActionErrors errors = new ActionErrors();
		ShowACHReturnQueueForm form = (ShowACHReturnQueueForm) f;
		ACHReturnManager achMgr = getACHReturnManager(request);
		UserProfile up = getUserProfile(request);
		
		
		if (!StringHelper.isNullOrEmpty(form.getSubmitCloseSelected()))
		{
			String [] selectedItems = form.getSelectedItems();
			for (int i=0; i<selectedItems.length; i++){
				achMgr.setAchReturnStatus(up.getUID(), Integer.parseInt(selectedItems[i]), 
						     EMoneyGramAdmApplicationConstants.ACH_RETURN_CLOSED_STATUS);
			}
		}
		request.setAttribute("closedStatus", EMoneyGramAdmApplicationConstants.ACH_RETURN_CLOSED_STATUS);

		
		if (StringHelper.isNullOrEmpty(form.getBeginDateText()))
		{
			DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
			long now = System.currentTimeMillis();
			form.setBeginDateText(
				df.format(new Date(now - 7L * 24L * 60L * 60L * 1000L)));
			form.setEndDateText(
				df.format(new Date(now + 24L * 60L * 60L * 1000L)));
		}
		
		if (request.getAttribute("achTranStatuses") == null) {
			request.setAttribute("achTranStatuses", achMgr.getAchReturnStatus(up.getUID()));
		}
		
		
		AchReturnSearchRequest achsr = new AchReturnSearchRequest();

		achsr.setAchEffDateBegin( form.getBeginDateText() );
		achsr.setAchEffDateEnd( form.getEndDateText() );

		if (form.getType().equals("ALL"))
			achsr.setAchTranType(null);
		else
			achsr.setAchTranType(form.getType());
		
		if (form.getStatus().equals("ALL"))
			achsr.setAchTranStatusCode(null);
		else
			achsr.setAchTranStatusCode(form.getStatus());

		Collection achReturnQueue =  achMgr.getAchReturnQueue(up.getUID(), achsr);
		
		
		if (!StringHelper.isNullOrEmpty(form.getSortBy()))
		{
			AchReturn.sortBy = form.getSortBy();
		}

		if (form.getSortSeq().equalsIgnoreCase("A"))
		{
			AchReturn.sortSeq = 1;
		} else
		{
			AchReturn.sortSeq = -1;
		}
		
		if (achReturnQueue.size() > 1)
		{
			Collections.sort((ArrayList)achReturnQueue);
		}
		
		request.getSession().setAttribute("achTranTypes", achMgr.getAchTranTypes());
		request.getSession().setAttribute("achReturnQueue", achReturnQueue);
		
		form.setRecordCount(achReturnQueue.size() + "");
		form.setSelectedItems(new String[achReturnQueue.size()]);
		
		
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
