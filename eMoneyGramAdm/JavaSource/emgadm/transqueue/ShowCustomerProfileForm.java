package emgadm.transqueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.view.ConsumerBankAccountView;
import emgadm.view.ConsumerCreditCardAccountView;
import emgadm.view.ConsumerProfileCommentView;
import emgadm.view.TransactionView;
import emgadm.util.AddressHelper;
import emgshared.model.ConsumerProfileActivity;
import emgshared.util.PhoneNumberFormatter;

public class ShowCustomerProfileForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final int DEFAULT_COMMENT_DISPLAY_LIMIT = 5;
	private String custId;
	private String custLogonId;
	private String custStatus;
	private String custLastName;
    private String custSecondLastName;
	private String custFirstName;
	private String custMiddleName;
	private String custHashedPassword;
	private String custSsn;
	private String saveCustSsn;
	private String custSsnTaintText;
	private String brthDate;
	private String custAge;
	private String addressLine1;
	private String addressLine2;
	private String buildingName;
	private String city;
	private String state;
	private String isoCountryCode;
	private String postalCode;
	private String zip4;
	private String countyName; 
	private String phoneNumber;
	private String phoneNumberTaintText;
	private String phoneNumberAlternate;
	private String phoneNumberAlternateTaintText;
	private String emailAddress;
	private String emailAddressTaintText;
	private String emailDomainTaintText;
	private String createdIPAddress;
	private boolean isEmailActive;
	private ConsumerBankAccountView[] bankAccounts = new ConsumerBankAccountView[0];
	private ConsumerCreditCardAccountView[] creditCardAccounts = new ConsumerCreditCardAccountView[0];
	private ConsumerCreditCardAccountView[] debitCardAccounts = new ConsumerCreditCardAccountView[0];
	private ConsumerCreditCardAccountView[] maestroAccounts = new ConsumerCreditCardAccountView[0];
	private List custStatusOptions = new ArrayList(0);
	private List accountStatusOptions = new ArrayList(0);
	private List customerCommentReasons = new ArrayList(0);
	private ConsumerProfileCommentView[] comments = new ConsumerProfileCommentView[0];
	private ConsumerProfileActivity[] consumerProfileActivity = new ConsumerProfileActivity[0];
	private int commentDisplayLimit = DEFAULT_COMMENT_DISPLAY_LIMIT;
	private TransactionView[] epTransactions = new TransactionView[0];
	private TransactionView[] mgTransactions = new TransactionView[0];
	private TransactionView[] esTransactions = new TransactionView[0];
	private TransactionView[] cashTransactions = new TransactionView[0];
	private TransactionView[] dsTransactions = new TransactionView[0];
	private int invlLoginTryCnt;
	private boolean profileLoginLocked; 
	private boolean profileDeletedFromLdap;
	private boolean acceptPromotionalEmail=false;
	private String basicFlag;
	private String prmrCode;
	private Date prmrCodeDate;
	private List custPrmrCodes = new ArrayList(0);
	private String negativeAddress = null;
	private String loyaltyPgmMembershipId;
	private String custAutoEnrollFlag;
	private String eDirectoryGuid;
	private Date adaptiveAuthProfileCompleteDate;
	private String partnerSiteId;
	private String idExtnlId;
	private String idNumber;
	private String idType;
	private String idTypeFriendlyName;
	private String gender;
	private String docStatus;
	private String submitApproveDocument;
	private String submitDenyDocument;
	private String submitPendDocument;
	private boolean isIdImageAvailable;
	private String viewGBGroupLogLinkString;
	private String gbGroupDetailLink;
	
	private void clear()
	{
		this.custId = null;
		this.custLogonId = null;
		this.custStatus = null;
		this.custLastName = null;
		this.custFirstName = null;
		this.custMiddleName = null;
		this.custHashedPassword = null;
		this.custSsn = null;
		this.brthDate = null;
		this.custAge = null;
		this.addressLine1 = null;
		this.addressLine2 = null;
		this.buildingName = null;
		this.emailAddress = null;
		this.isEmailActive = false;
		this.city = null;
		this.state = null;
		this.isoCountryCode = null;
		this.postalCode = null;
		this.zip4 = null;
		this.phoneNumber = null;
		this.phoneNumberAlternate = null;
		this.bankAccounts = new ConsumerBankAccountView[0];
		this.creditCardAccounts = new ConsumerCreditCardAccountView[0];
		this.maestroAccounts = new ConsumerCreditCardAccountView[0];
		this.comments = new ConsumerProfileCommentView[0];
		this.commentDisplayLimit = DEFAULT_COMMENT_DISPLAY_LIMIT;
		this.epTransactions = new TransactionView[0];
		this.mgTransactions = new TransactionView[0];
		this.esTransactions = new TransactionView[0];
		this.cashTransactions = new TransactionView[0];
		this.invlLoginTryCnt = 0;
		this.createdIPAddress = null;
		this.acceptPromotionalEmail=false;
		this.prmrCode = "S";
		this.loyaltyPgmMembershipId = null;
		this.custAutoEnrollFlag = null;
		this.adaptiveAuthProfileCompleteDate = null;
		this.eDirectoryGuid = null;
		this.partnerSiteId = null;
		this.isIdImageAvailable = false;
	}

	public String getEDirectoryGuid() {
		return eDirectoryGuid;
	}

	public void setEDirectoryGuid(String directoryGuid) {
		eDirectoryGuid = directoryGuid.trim();
	}

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = StringUtils.trimToNull(string);
	}

	public String getCustLogonId()
	{
		return custLogonId;
	}

	public void setCustLogonId(String string)
	{
		custLogonId = StringUtils.trimToNull(string);
	}

	public String getCustLastName()
	{
		return custLastName;
	}

	public void setCustLastName(String string)
	{
		custLastName = StringUtils.trimToNull(string);
	}

	public String getCustFirstName()
	{
		return custFirstName;
	}

	public void setCustFirstName(String string)
	{
		custFirstName = StringUtils.trimToNull(string);
	}

	public String getCustMiddleName()
	{
		return custMiddleName;
	}

	public void setCustMiddleName(String string)
	{
		custMiddleName = StringUtils.trimToNull(string);
	}

	public String getCustSsn()
	{
		return custSsn;
	}

	public void setCustSsn(String string)
	{
		custSsn = StringUtils.trimToNull(string);
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		clear();
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getAddressLine1()
	{
		return addressLine1;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getAddressLine2()
	{
		return addressLine2;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getIsoCountryCode()
	{
		return isoCountryCode;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPhoneNumberAlternate()
	{
		return phoneNumberAlternate;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @return
	 * 
	 * Created on Feb 16, 2005
	 */
	public String getZip4()
	{
		return zip4;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setAddressLine1(String string)
	{
		addressLine1 = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setAddressLine2(String string)
	{
		addressLine2 = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setCity(String string)
	{
		city = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setIsoCountryCode(String string)
	{
		isoCountryCode = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPhoneNumber(String string)
	{
		phoneNumber = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPhoneNumberAlternate(String string)
	{
		phoneNumberAlternate = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setPostalCode(String string)
	{
		postalCode = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setState(String string)
	{
		state = string;
	}

	/**
	 * @param string
	 * 
	 * Created on Feb 16, 2005
	 */
	public void setZip4(String string)
	{
		zip4 = string;
	}

	public String getPhoneNumberFormatted() {
		String formattedPhone = PhoneNumberFormatter.format(phoneNumber);
		return formattedPhone;
	}

	public String getPhoneNumberAlternateFormatted() {
		String formattedPhone = PhoneNumberFormatter.format(phoneNumberAlternate); 
		return formattedPhone;
	}

	public String getCustStatus()
	{
		return custStatus;
	}

	public List getCustStatusOptions()
	{
		return custStatusOptions;
	}

	public List getAccountStatusOptions()
	{
		return accountStatusOptions;
	}

	public List getCustomerCommentReasons()
	{
		return customerCommentReasons;
	}

	public void setCustStatus(String string)
	{
		custStatus = string;
	}

	public void setCustStatusOptions(List list)
	{
		custStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public void setCustomerCommentReasons(List list)
	{
		customerCommentReasons = (list == null ? new ArrayList(0) : list);
	}

	public void setAccountStatusOptions(List list)
	{
		accountStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	public ConsumerBankAccountView[] getBankAccounts()
	{
		return bankAccounts;
	}

	public ConsumerCreditCardAccountView[] getCreditCardAccounts()
	{
		return creditCardAccounts;
	}

	public void setBankAccounts(ConsumerBankAccountView[] accounts)
	{
		bankAccounts =
			accounts == null ? new ConsumerBankAccountView[0] : accounts;
	}

	public void setCreditCardAccounts(ConsumerCreditCardAccountView[] accounts)
	{
		creditCardAccounts =
			accounts == null ? new ConsumerCreditCardAccountView[0] : accounts;
	}

	public ConsumerProfileActivity[] getConsumerProfileActivity() {
		return consumerProfileActivity;
	}

	public void setConsumerProfileActivity(ConsumerProfileActivity[] cpActy) {
		this.consumerProfileActivity = cpActy;
	}
	
	public ConsumerProfileCommentView[] getComments()
	{
		return comments;
	}

	public void setComments(ConsumerProfileCommentView[] views)
	{
		comments = views;
	}

	public int getCommentDisplayLimit()
	{
		return commentDisplayLimit;
	}

	public void setCommentDisplayLimit(int i)
	{
		commentDisplayLimit = i;
	}

	public TransactionView[] getEpTransactions()
	{
		return epTransactions;
	}

	public TransactionView[] getMgTransactions()
	{
		return mgTransactions;
	}

	public void setEpTransactions(TransactionView[] views)
	{
		epTransactions = views;
	}

	public void setMgTransactions(TransactionView[] views)
	{
		mgTransactions = views;
	}
	/**
	 * @return
	 */
	public String getBrthDate() {
		return brthDate;
	}

	/**
	 * @param string
	 */
	public void setBrthDate(String string) {
		brthDate = string;
	}

	public String getCustAge() {
		return custAge;
	}

	public void setCustAge(String custAge) {
		this.custAge = custAge;
	}


	/**
	 * @return
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string)
	{
		emailAddress = string;
	}

	/**
	 * @return
	 */
	public boolean getIsEmailActive()
	{
		return isEmailActive;
	}

	/**
	 * @param boolean1
	 */
	public void setIsEmailActive(boolean b)
	{
		isEmailActive = b;
	}

	/**
	 * @return
	 */
	public String getCustSsnTaintText() {
		return custSsnTaintText;
	}

	/**
	 * @param string
	 */
	public void setCustSsnTaintText(String string) {
		custSsnTaintText = string;
	}

	/**
	 * @return
	 */
	public String getPhoneNumberTaintText() {
		return phoneNumberTaintText == null ? "" : phoneNumberTaintText;
	}

	/**
	 * @param string
	 */
	public void setPhoneNumberTaintText(String string) {
		phoneNumberTaintText = string;
	}

	/**
	 * @return
	 */
	public String getSaveCustSsn() {
		return saveCustSsn;
	}

	/**
	 * @param string
	 */
	public void setSaveCustSsn(String string) {
		saveCustSsn = string;
	}

	/**
	 * @return
	 */
	public String getPhoneNumberAlternateTaintText() {
		return phoneNumberAlternateTaintText == null ? "" : phoneNumberAlternateTaintText;
	}

	/**
	 * @param string
	 */
	public void setPhoneNumberAlternateTaintText(String string) {
		phoneNumberAlternateTaintText = string;
	}

	/**
	 * @return
	 */
	public TransactionView[] getEsTransactions() {
		return esTransactions;
	}

	/**
	 * @param views
	 */
	public void setEsTransactions(TransactionView[] views) {
		esTransactions = views;
	}

	public TransactionView[] getCashTransactions() {
		return cashTransactions;
	}

	/**
	 * @param views
	 */
	public void setCashTransactions(TransactionView[] views) {
		cashTransactions = views;
	}

	public TransactionView[] getDsTransactions() {
		return dsTransactions;
	}

	public void setDsTransactions(TransactionView[] dsTransactions) {
		this.dsTransactions = dsTransactions;
	}

	/**
	 * @return
	 */
	public String getCustHashedPassword() {
		return custHashedPassword;
	}

	/**
	 * @param string
	 */
	public void setCustHashedPassword(String string) {
		custHashedPassword = string;
	}
	/**
	 * @return
	 */
	public String getEmailAddressTaintText() {
		return emailAddressTaintText;
	}

	/**
	 * @param string
	 */
	public void setEmailAddressTaintText(String string) {
		emailAddressTaintText = string;
	}

	/**
	 * @return
	 */
	public int getInvlLoginTryCnt()
	{
		return invlLoginTryCnt;
	}

	/**
	 * @param i
	 */
	public void setInvlLoginTryCnt(int i)
	{
		invlLoginTryCnt = i;
	}

	/**
	 * @return
	 */
	public String getEmailDomainTaintText()
	{
		return emailDomainTaintText;
	}

	/**
	 * @param string
	 */
	public void setEmailDomainTaintText(String string)
	{
		emailDomainTaintText = string;
	}

	/**
	 * @return
	 */
	public String getCreatedIPAddress()
	{
		return createdIPAddress == null ? "" : createdIPAddress;
	}

	/**
	 * @param string
	 */
	public void setCreatedIPAddress(String string)
	{
		createdIPAddress = string;
	}

	/**
	 * @return
	 */
	public ConsumerCreditCardAccountView[] getDebitCardAccounts()
	{
		return debitCardAccounts;
	}

	/**
	 * @param views
	 */
	public void setDebitCardAccounts(ConsumerCreditCardAccountView[] views)
	{
		debitCardAccounts = views;
	}

	public ConsumerCreditCardAccountView[] getMaestroAccounts() {
		return maestroAccounts;
	}

	public void setMaestroAccounts(ConsumerCreditCardAccountView[] maestroAccounts) {
		this.maestroAccounts = maestroAccounts;
	}

	/**
	 * @return
	 */
	public boolean isAcceptPromotionalEmail()
	{
		return acceptPromotionalEmail;
	}

	/**
	 * @param b
	 */
	public void setAcceptPromotionalEmail(boolean b)
	{
		acceptPromotionalEmail = b;
	}

	/**
	 * @return
	 */
	public String getBasicFlag()
	{
		return basicFlag;
	}

	public void setBasicFlag(String string)
	{
		basicFlag = string;
	}

	public String getPrmrCode() {
        return prmrCode;
    }

    public void setPrmrCode(String prmrCode) {
        this.prmrCode = prmrCode;
    }

	public Date getPrmrCodeDate() {
        return this.prmrCodeDate;
    }

    public void setPrmrCodeDate(Date prmrCodeDate) {
        this.prmrCodeDate = prmrCodeDate;
    }

    public List getCustPrmrCodes() {
        return custPrmrCodes;
    }


    public void setCustPrmrCodes(List custPrmrCodes) {
        this.custPrmrCodes = custPrmrCodes;
    }
     
	public String getNegativeAddress() {
		return negativeAddress;
	}

	public void setNegativeAddress(String negativeAddress) {
		this.negativeAddress = AddressHelper.negativeAddressFormat(this.addressLine1,
				this.postalCode, this.state);
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId;
	}

	public String getCustAutoEnrollFlag() {
		return custAutoEnrollFlag;
	}

	public void setCustAutoEnrollFlag(String custAutoEnrollFlag) {
		this.custAutoEnrollFlag = custAutoEnrollFlag;
	}
	public Date getAdaptiveAuthProfileCompleteDate() {
		return adaptiveAuthProfileCompleteDate;
	}

	public void setAdaptiveAuthProfileCompleteDate(Date adaptiveAuthProfileCompleteDate) {
		this.adaptiveAuthProfileCompleteDate = adaptiveAuthProfileCompleteDate;
	}

	public boolean getProfileLoginLocked() {
		return profileLoginLocked;
	}

	public void setProfileLoginLocked(boolean profileLoginLocked) {
		this.profileLoginLocked = profileLoginLocked;
	}
	
	public boolean isProfileDeletedFromLdap() {
		return profileDeletedFromLdap;
	}

	public void setProfileDeletedFromLdap(boolean profileDeletedFromLdap) {
		this.profileDeletedFromLdap = profileDeletedFromLdap;
	}
	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdTypeFriendlyName(String idTypeFriendlyName) {
		this.idTypeFriendlyName = idTypeFriendlyName;
	}

	public String getIdTypeFriendlyName() {
		return idTypeFriendlyName;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getDocStatus() {
		return docStatus;
	}

	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}

	public String getDocStatusString() {
		return EMoneyGramAdmApplicationConstants.docStatusMap.get(getDocStatus());
	}

	public String getSubmitApproveDocument() {
		return submitApproveDocument;
	}

	public void setSubmitApproveDocument(String submitApproveDocument) {
		this.submitApproveDocument = submitApproveDocument;
	}

	public String getSubmitDenyDocument() {
		return submitDenyDocument;
	}

	public void setSubmitDenyDocument(String submitDenyDocument) {
		this.submitDenyDocument = submitDenyDocument;
	}

	public String getSubmitPendDocument() {
		return submitPendDocument;
	}

	public void setSubmitPendDocument(String submitPendDocument) {
		this.submitPendDocument = submitPendDocument;
	}

	public String getIdExtnlId() {
		return idExtnlId;
	}

	public void setIdExtnlId(String idExtnlId) {
		this.idExtnlId = idExtnlId;
	}
	
	public boolean getIsIdImageAvailable(){
		return isIdImageAvailable;
	}
	
	public void setIsIdImageAvailable(boolean b) {
		this.isIdImageAvailable = b;
	}

	public String getViewGBGroupLogLinkString() {
		return viewGBGroupLogLinkString;
	}

	public void setViewGBGroupLogLinkString(String viewGBGroupLogLinkString) {
		this.viewGBGroupLogLinkString = viewGBGroupLogLinkString;
	}

	public String getGbGroupDetailLink() {
		return gbGroupDetailLink;
	}

	public void setGbGroupDetailLink(String gbGroupDetailLink) {
		this.gbGroupDetailLink = gbGroupDetailLink;
	}
	
    public String getCustSecondLastName() {
	    return custSecondLastName;
	}

    public void setCustSecondLastName(String custSecondLastName) {
	    this.custSecondLastName = custSecondLastName!= null ? custSecondLastName.trim() : "";
	}
}
