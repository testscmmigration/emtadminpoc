package emgadm.transqueue;

import java.text.DateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.UserProfile;
import emgadm.util.DateHelper;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class ShowACHReturnQueueForm extends EMoneyGramAdmBaseValidatorForm
{
	private String beginDateText = null;
	private String endDateText = null;
	private String status = "ALL";
	private String type = "ALL";
	private String sortBy = "achReturnTranId";
	private String sortSeq = "A";
	private String submitRefresh = null;
	private String recordCount = "0";
	//private final String color = "light-blue";
	private String[] selectedItems = {}; 
	//private String[] items = {}; 
	private String submitCloseSelected = null;

	
	/**
	 * @return Returns the submitCloseSelected.
	 */
	public String getSubmitCloseSelected() {
		return submitCloseSelected;
	}
	/**
	 * @param submitCloseSelected The submitCloseSelected to set.
	 */
	public void setSubmitCloseSelected(String submitCloseSelected) {
		this.submitCloseSelected = submitCloseSelected;
	}
	public String[] getSelectedItems() { 
	  return this.selectedItems; 
	} 
	
	public void setSelectedItems(String[] selectedItems) { 
	  this.selectedItems = selectedItems; 
	}
	
	public String getBeginDateText()
	{
		return beginDateText;
	}

	public String getEndDateText()
	{
		return endDateText;
	}

	public String getSortBy()
	{
		return sortBy;
	}

	public String getStatus()
	{
		return status;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String string)
	{
		type = string;
	}
	public String getSubmitRefresh()
	{
		return submitRefresh;
	}

	public void setBeginDateText(String string)
	{
		beginDateText = string;
	}

	public void setEndDateText(String string)
	{
		endDateText = string;
	}

	public void setSortBy(String string)
	{
		sortBy = string;
	}

	public void setStatus(String string)
	{
		status = string;
	}

	public void setSubmitRefresh(String string)
	{
		submitRefresh = string;
	}

	public String getSortSeq()
	{
		return sortSeq;
	}

	public void setSortSeq(String string)
	{
		sortSeq = string;
	}

	public String getRecordCount()
	{
		return recordCount;
	}

	public void setRecordCount(String string)
	{
		recordCount = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		UserProfile up =
			(UserProfile) request.getSession().getAttribute(
				EMoneyGramAdmSessionConstants.USER_PROFILE);
		//  for thread-safe, DateFormat is not a thread-safe class
		{
			DateFormat dateFormatter =
				DateHelper.getFormatter("dd/MMM/yyyy", up.getTimeZone());
			beginDateText =
				dateFormatter.format(
					new Date(
						System.currentTimeMillis() - 7L * 24L * 60L * 60L * 1000L));
			endDateText =
				dateFormatter.format(
					new Date(System.currentTimeMillis() + 24L * 60L * 60L * 1000L));
		}		

		status = "ALL";
		type = "ALL";
		sortBy = "achReturnTranId";
		sortSeq = "A";
		submitRefresh = null;
		recordCount = "0";
		
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if ((errors == null || errors.size() == 0)
			&& !StringHelper.isNullOrEmpty(submitRefresh))
		{
			DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
			try
			{
				if (dfmt.parse(beginDateText).after(dfmt.parse(endDateText)))
				{
					String[] parms = { beginDateText, endDateText };
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError(
							"error.begin.date.later.than.end.date",
							parms));
				}
			} catch (ParseDateException e)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.malformed.date"));
			}

		}

		return errors;
	}
}
