/*
 * Created on Mar 9, 2005
 *
 */
package emgadm.transqueue;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.UserProfile;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;

/**
 * @author A131
 *
 */
public class ReturnFundingAction extends ManualAdjustmentBaseAction {
	protected ActionForward processAdjustment(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UserProfile userProfile = getUserProfile(request);
		TransactionService tranService =
			ServiceFactory.getInstance().getTransactionService();
		tranService.returnFunding(
			Integer.parseInt(form.getTranId()),
			Float.parseFloat(form.getAmount()),
			userProfile.getUID());
		return mapping.findForward(FORWARD_SUCCESS);
	}

	protected ActionForward initialize(
		ActionMapping mapping,
		ManualAdjustmentForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws
			NumberFormatException,
			TransactionAlreadyInProcessException,
			TransactionOwnershipException {

		//	Amount should default to face amount if possible.
		String faceAmountText = form.getTranFaceAmt();
		if (StringUtils.isNotBlank(faceAmountText)) {
			Number faceAmount;
			try {
				faceAmount = TransactionDetailAction.df.parse(faceAmountText);
			} catch (ParseException ignore) {
				faceAmount = null;
			}

			if (faceAmount != null) {
				form.setAmount(faceAmount.toString());
			}
		}
		return mapping.findForward(FORWARD_INIT);
	}

	protected void getCommandDescription(HttpServletRequest request) {

		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.TransactionConfirmationDescription");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage("confirmation.manual.return.funding"));
		formatter.setLocale(locale);
		String msgText = formatter.toPattern();
		request.setAttribute("msgText", msgText);

	}
}
