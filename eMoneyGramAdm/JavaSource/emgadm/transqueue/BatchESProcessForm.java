package emgadm.transqueue;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class BatchESProcessForm extends EMoneyGramAdmBaseValidatorForm
{
	private String[] selTrans;
	private String actionType;
	private String done;
	private String submitConfirm = null;
	private String submitCancel = null;
	private String submitReturn = null;

	public String[] getSelTrans()
	{
		return selTrans;
	}

	public void setSelTrans(String[] strings)
	{
		selTrans = strings;
	}

	public String getActionType()
	{
		return actionType;
	}

	public void setActionType(String string)
	{
		actionType = string;
	}

	public String getDone()
	{
		return done;
	}

	public void setDone(String string)
	{
		done = string;
	}

	public String getSubmitConfirm()
	{
		return submitConfirm;
	}

	public void setSubmitConfirm(String string)
	{
		submitConfirm = string;
	}

	public String getSubmitCancel()
	{
		return submitCancel;
	}

	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	public String getSubmitReturn()
	{
		return submitReturn;
	}

	public void setSubmitReturn(String string)
	{
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		actionType = "";
		done = "";
		submitConfirm = null;
		submitCancel = null;
		submitReturn = null;
		selTrans = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		return errors;
	}
}
