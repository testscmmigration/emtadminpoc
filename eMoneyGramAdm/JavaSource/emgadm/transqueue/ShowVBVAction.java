package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.util.StringHelper;

public class ShowVBVAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ShowVBVForm form = (ShowVBVForm) f;
		
		TransactionManager tm = getTransactionManager(request);

		if (!StringHelper.isNullOrEmpty(form.getTranId())
			&& !StringHelper.containsNonDigits(form.getTranId()))
		{

			int tranId = Integer.parseInt(form.getTranId());
			request.setAttribute("vbvList", tm.getVBVBeans(tranId));
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
