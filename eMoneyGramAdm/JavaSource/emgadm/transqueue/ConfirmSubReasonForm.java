package emgadm.transqueue;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ConfirmSubReasonForm extends EMoneyGramAdmBaseValidatorForm
{
	private String tranId;
	private String subReasonCode;
	private String submitRecord;
	private String submitCancel;
	private String subReasonDescription;
	
	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public String getSubmitRecord()
	{
		return submitRecord;
	}

	public void setSubmitRecord(String string)
	{
		submitRecord = string;
	}

	public String getSubmitCancel()
	{
		return submitCancel;
	}

	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		subReasonCode = "";
		tranId = "";
		submitRecord = "";
		submitCancel = "";

	}
	
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors;
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;

		if (!StringHelper.isNullOrEmpty(submitRecord))
		{
//			float tmpAmt = 0F;
			if (StringHelper.isNullOrEmpty(subReasonCode))
			{
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Sub-Reason Type"));
			}
		}
		return errors;
	}
    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode;
    }
    /**
     * @param subReasonCode The subReasonCode to set.
     */
    public void setSubReasonCode(String srCode) {
        this.subReasonCode = srCode;
    }
    
    /**
     * 
     * @return Returns the subReasonDescription
     */
    public String getSubReasonDescription() {
		return subReasonDescription;
	}

    /**
     * subReasonDescription The subReasonDescrption to set.
     * @param subReasonDescription
     */
	public void setSubReasonDescription(String subReasonDescription) {
		this.subReasonDescription = subReasonDescription;
	}
}
