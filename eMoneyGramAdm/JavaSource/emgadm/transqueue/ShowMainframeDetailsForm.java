/*
 * Created on Apr 11, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.transqueue;

import java.util.Date;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author T348
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ShowMainframeDetailsForm extends EMoneyGramAdmBaseValidatorForm {

	private String id;
	private String tranType;
	private String transactionStatus;
	private java.util.Calendar dateTimeSent;
	private java.math.BigDecimal feeAmount;
	private java.math.BigDecimal sendAmount;
	private java.lang.String sendCurrency;
	private java.math.BigDecimal receiveAmount;
	private java.lang.String receiveCurrency;
	private java.lang.String receiveCountry;
	private String deliveryOption;
	private java.lang.String senderFirstName;
	private java.lang.String senderMiddleInitial;
	private java.lang.String senderLastName;
	private java.lang.String senderAddress;
	private java.lang.String senderCity;
	private java.lang.String senderState;
	private java.lang.String senderZipCode;
	private java.lang.String senderCountry;
	private java.lang.String senderHomePhone;
	private java.lang.String receiverFirstName;
	private java.lang.String receiverMiddleInitial;
	private java.lang.String receiverLastName;
	private java.lang.String receiverLastName2;
	private java.lang.String receiverAddress;
	private java.lang.String receiverCity;
	private java.lang.String receiverState;
	private java.lang.String receiverZipCode;
	private java.lang.String receiverCountry;
	private java.lang.String receiverPhone;
	private java.lang.String direction1;
	private java.lang.String direction2;
	private java.lang.String direction3;
	private java.lang.String messageField1;
	private java.lang.String messageField2;
	private java.lang.String senderBirthCity;
	private java.lang.String senderBirthCountry;
	private java.lang.String receiverColonia;
	private java.lang.String receiverMunicipio;
	private java.lang.String operatorName;
	private boolean validIndicator;
	private java.lang.String agentUseSendData;
	private java.lang.String agentUseReceiveData;
	private String partnerCustomerReceiveNumber;
	private Date expectedDateOfDelivery;
	private String mgCustomerReceiveNumber;
	private String receiptTextInfoEng;

	/**
	 * @return
	 */
	public java.lang.String getAgentUseReceiveData() {
		return agentUseReceiveData;
	}

	/**
	 * @return
	 */
	public java.lang.String getAgentUseSendData() {
		return agentUseSendData;
	}

	/**
	 * @return
	 */
	public java.util.Date getDateTimeSent() {
		return dateTimeSent.getTime();
	}

	/**
	 * @return
	 */
	public String getDeliveryOption() {
		return deliveryOption;
	}

	/**
	 * @return
	 */
	public java.lang.String getDirection1() {
		return direction1;
	}

	/**
	 * @return
	 */
	public java.lang.String getDirection2() {
		return direction2;
	}

	/**
	 * @return
	 */
	public java.lang.String getDirection3() {
		return direction3;
	}

	/**
	 * @return
	 */
	public java.math.BigDecimal getFeeAmount() {
		return feeAmount;
	}

	/**
	 * @return
	 */
	public java.lang.String getMessageField1() {
		return messageField1;
	}

	/**
	 * @return
	 */
	public java.lang.String getMessageField2() {
		return messageField2;
	}

	/**
	 * @return
	 */
	public java.lang.String getOperatorName() {
		return operatorName;
	}

	/**
	 * @return
	 */
	public java.math.BigDecimal getReceiveAmount() {
		return receiveAmount;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiveCountry() {
		return receiveCountry;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiveCurrency() {
		return receiveCurrency;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverAddress() {
		return receiverAddress;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverCity() {
		return receiverCity;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverColonia() {
		return receiverColonia;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverCountry() {
		return receiverCountry;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverFirstName() {
		return receiverFirstName;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverLastName() {
		return receiverLastName;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverLastName2() {
		return receiverLastName2;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverMiddleInitial() {
		return receiverMiddleInitial;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverMunicipio() {
		return receiverMunicipio;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverPhone() {
		return receiverPhone;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverState() {
		return receiverState;
	}

	/**
	 * @return
	 */
	public java.lang.String getReceiverZipCode() {
		return receiverZipCode;
	}

	/**
	 * @return
	 */
	public java.math.BigDecimal getSendAmount() {
		return sendAmount;
	}

	/**
	 * @return
	 */
	public java.lang.String getSendCurrency() {
		return sendCurrency;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderAddress() {
		return senderAddress;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderBirthCity() {
		return senderBirthCity;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderBirthCountry() {
		return senderBirthCountry;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderCity() {
		return senderCity;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderCountry() {
		return senderCountry;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderFirstName() {
		return senderFirstName;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderHomePhone() {
		return senderHomePhone;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderLastName() {
		return senderLastName;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderMiddleInitial() {
		return senderMiddleInitial;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderState() {
		return senderState;
	}

	/**
	 * @return
	 */
	public java.lang.String getSenderZipCode() {
		return senderZipCode;
	}

	/**
	 * @return
	 */
	public String getTransactionStatus() {
		return transactionStatus;
	}

	/**
	 * @return
	 */
	public boolean isValidIndicator() {
		return validIndicator;
	}

	/**
	 * @param string
	 */
	public void setAgentUseReceiveData(java.lang.String string) {
		agentUseReceiveData = string;
	}

	/**
	 * @param string
	 */
	public void setAgentUseSendData(java.lang.String string) {
		agentUseSendData = string;
	}

	/**
	 * @param calendar
	 */
	public void setDateTimeSent(java.util.Calendar calendar) {
		dateTimeSent = calendar;
	}

	/**
	 * @param string
	 */
	public void setDeliveryOption(String string) {
		deliveryOption = string;
	}

	/**
	 * @param string
	 */
	public void setDirection1(java.lang.String string) {
		direction1 = string;
	}

	/**
	 * @param string
	 */
	public void setDirection2(java.lang.String string) {
		direction2 = string;
	}

	/**
	 * @param string
	 */
	public void setDirection3(java.lang.String string) {
		direction3 = string;
	}

	/**
	 * @param decimal
	 */
	public void setFeeAmount(java.math.BigDecimal decimal) {
		feeAmount = decimal;
	}

	/**
	 * @param string
	 */
	public void setMessageField1(java.lang.String string) {
		messageField1 = string;
	}

	/**
	 * @param string
	 */
	public void setMessageField2(java.lang.String string) {
		messageField2 = string;
	}

	/**
	 * @param string
	 */
	public void setOperatorName(java.lang.String string) {
		operatorName = string;
	}

	/**
	 * @param decimal
	 */
	public void setReceiveAmount(java.math.BigDecimal decimal) {
		receiveAmount = decimal;
	}

	/**
	 * @param string
	 */
	public void setReceiveCountry(java.lang.String string) {
		receiveCountry = string;
	}

	/**
	 * @param string
	 */
	public void setReceiveCurrency(java.lang.String string) {
		receiveCurrency = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverAddress(java.lang.String string) {
		receiverAddress = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverCity(java.lang.String string) {
		receiverCity = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverColonia(java.lang.String string) {
		receiverColonia = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverCountry(java.lang.String string) {
		receiverCountry = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverFirstName(java.lang.String string) {
		receiverFirstName = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverLastName(java.lang.String string) {
		receiverLastName = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverLastName2(java.lang.String string) {
		receiverLastName2 = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverMiddleInitial(java.lang.String string) {
		receiverMiddleInitial = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverMunicipio(java.lang.String string) {
		receiverMunicipio = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverPhone(java.lang.String string) {
		receiverPhone = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverState(java.lang.String string) {
		receiverState = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverZipCode(java.lang.String string) {
		receiverZipCode = string;
	}

	/**
	 * @param decimal
	 */
	public void setSendAmount(java.math.BigDecimal decimal) {
		sendAmount = decimal;
	}

	/**
	 * @param string
	 */
	public void setSendCurrency(java.lang.String string) {
		sendCurrency = string;
	}

	/**
	 * @param string
	 */
	public void setSenderAddress(java.lang.String string) {
		senderAddress = string;
	}

	/**
	 * @param string
	 */
	public void setSenderBirthCity(java.lang.String string) {
		senderBirthCity = string;
	}

	/**
	 * @param string
	 */
	public void setSenderBirthCountry(java.lang.String string) {
		senderBirthCountry = string;
	}

	/**
	 * @param string
	 */
	public void setSenderCity(java.lang.String string) {
		senderCity = string;
	}

	/**
	 * @param string
	 */
	public void setSenderCountry(java.lang.String string) {
		senderCountry = string;
	}

	/**
	 * @param string
	 */
	public void setSenderFirstName(java.lang.String string) {
		senderFirstName = string;
	}

	/**
	 * @param string
	 */
	public void setSenderHomePhone(java.lang.String string) {
		senderHomePhone = string;
	}

	/**
	 * @param string
	 */
	public void setSenderLastName(java.lang.String string) {
		senderLastName = string;
	}

	/**
	 * @param string
	 */
	public void setSenderMiddleInitial(java.lang.String string) {
		senderMiddleInitial = string;
	}

	/**
	 * @param string
	 */
	public void setSenderState(java.lang.String string) {
		senderState = string;
	}

	/**
	 * @param string
	 */
	public void setSenderZipCode(java.lang.String string) {
		senderZipCode = string;
	}

	/**
	 * @param string
	 */
	public void setTransactionStatus(String string) {
		transactionStatus = string;
	}

	/**
	 * @param b
	 */
	public void setValidIndicator(boolean b) {
		validIndicator = b;
	}

	/**
	 * @return
	 */
	public String getTranType() {
		return tranType;
	}

	/**
	 * @param string
	 */
	public void setTranType(String string) {
		tranType = string;
	}

	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param string
	 */
	public void setId(String string) {
		id = string;
	}

	/**
	 * @return Returns the expectedDateOfDelivery.
	 */
	public Date getExpectedDateOfDelivery() {
		return expectedDateOfDelivery;
	}

	/**
	 * @param expectedDateOfDelivery
	 *            The expectedDateOfDelivery to set.
	 */
	public void setExpectedDateOfDelivery(Date date) {
		this.expectedDateOfDelivery = date;
	}

	/**
	 * @return Returns the partnerCustomerReceiveNumber.
	 */
	public String getPartnerCustomerReceiveNumber() {
		return partnerCustomerReceiveNumber;
	}

	/**
	 * @param partnerCustomerReceiveNumber
	 *            The partnerCustomerReceiveNumber to set.
	 */

	public void setPartnerCustomerReceiveNumber(String string) {
		this.partnerCustomerReceiveNumber = string;
	}

	/**
	 * @return Returns the mgCustomerReceiveNumber.
	 */
	public String getMgCustomerReceiveNumber() {
		return mgCustomerReceiveNumber;
	}

	/**
	 * @param mgCustomerReceiveNumber
	 *            The mgCustomerReceiveNumber to set.
	 */
	public void setMgCustomerReceiveNumber(String string) {
		this.mgCustomerReceiveNumber = string;
	}

	public String getReceiptTextInfoEng() {
		return receiptTextInfoEng;
	}

	/**
	 * 
	 * @param receiptTextInfoEng
	 */
	public void setReceiptTextInfoEng(String receiptTextInfoEng) {
		this.receiptTextInfoEng = receiptTextInfoEng;
	}
}
