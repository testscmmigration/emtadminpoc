package emgadm.microdeposit;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccountType;
import emgshared.model.MicroDeposit;
import emgshared.model.MicroDepositStatus;
import emgshared.services.ConsumerAccountService;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;

public class RecordMDACHReturnAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException, SQLException
	{

		ActionErrors errors = new ActionErrors();
		RecordMDACHReturnForm form = (RecordMDACHReturnForm) f;

		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			request.setAttribute("mdReturn", "Y");
			request.getSession().setAttribute("mdType", "b");
			
			String achReturnCall = (String)request.getSession().getAttribute("AchReturnCall");
			
			if (achReturnCall != null && achReturnCall.equals("true")){
				return mapping.findForward(
						EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_ACHRETURN_QUEUE);
			}else{
				request.getSession().removeAttribute("AchReturnCall");
				return mapping.findForward(
						EMoneyGramAdmForwardConstants.LOCAL_FORWARD_LIST_MD_BATCHES);
			}
		}

		UserProfile up = getUserProfile(request);
		List vldnList = null;
		MicroDeposit md = null;
		MicroDepositService mds =
			ServiceFactory.getInstance().getMicroDepositService();
		ConsumerAccountService cas =
			ServiceFactory.getInstance().getConsumerAccountService();
		TransactionManager tm = getTransactionManager(request);

		if (!StringHelper.isNullOrEmpty(form.getSubmitUpdate()))
		{
			form.setMode("U");
			vldnList = (List) request.getSession().getAttribute("vldnList");
			Map map = new HashMap();
			Iterator iter = vldnList.iterator();
			while (iter.hasNext())
			{
				md = (MicroDeposit) iter.next();
				map.put(new Integer(md.getValidationId()), md);
			}

			for (int i = 0; i < form.getSelectedVldn().length; i++)
			{
				Integer vldn =
					new Integer(Integer.parseInt(form.getSelectedVldn()[i]));
				md = (MicroDeposit) map.get(vldn);
				if (md != null)
				{
					md.setCallerLoginId(up.getUID());
					mds.updateMDStatus(md);
					MicroDepositStatus status = new MicroDepositStatus();
					status.setMdStatus(MicroDepositStatus.ACH_RETURNED_CODE);
					status.setMdStatusDesc("ACH Return");
					md.setStatus(status);
					md.setProcessACR(true);
					md.setMsg("Change MD Status to ACR;");
				}
			}

			for (int i = 0; i < form.getSelectedDeAct().length; i++)
			{
				Integer vldn =
					new Integer(Integer.parseInt(form.getSelectedDeAct()[i]));
				md = (MicroDeposit) map.get(vldn);
				if (md != null)
				{
					md.setCallerLoginId(up.getUID());
					cas.updateStatus(
						md.getCustAccountId(),
						ConsumerAccountType.BANK_SAVINGS,
						AccountStatus.NON_ACTIVE_FAILURE,
						up.getUID());

					AccountComment cmnt = new AccountComment();
					cmnt.setReasonCode("R006");
					cmnt.setText(
						"Micro Deposit ACH returned and validation failed");
					cmnt.setAccountId(md.getCustAccountId());
					cmnt.setCreatedBy(up.getUID());
					cmnt.setCreateDate(new Date());
					cas.addAccountComment(cmnt, up.getUID());

					//  add send email to the consumer ????
					//  to-do

					md.setProcessDeactiveAcct(true);
					md.setMsg(
						md.getMsg() + " Customer Account became deactive.");
				}
			}

			String[] parms = { "All", "Micro Deposit Transactions" };
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("msg.updated", parms));
			saveErrors(request, errors);
		}

		if (!StringHelper.isNullOrEmpty(form.getBatchId()))
		{
			vldnList =
				mds.getMDTrans(
					new Integer(Integer.parseInt(form.getBatchId())),
					null,
					null,
					up.getUID());
			request.getSession().setAttribute("vldnList", vldnList);
			request.getSession().removeAttribute("AchReturnCall");
		} else{
			if (request.getParameter("micVldnId") != null)
			{
				String micVldnId = (String)request.getParameter("micVldnId");
				MicroDeposit microDeposit = tm.getMicroDepTrans(up.getUID(), Integer.parseInt(micVldnId));
				vldnList =
					mds.getMDTrans(
						null,
						new Integer(microDeposit.getDepositTranId1()),
						null,
						up.getUID());
				request.getSession().setAttribute("vldnList", vldnList);
				request.getSession().setAttribute("AchReturnCall", "true");
			}else{
				vldnList = (List) request.getSession().getAttribute("vldnList");
				request.getSession().removeAttribute("AchReturnCall");
			}	
		}

		if (vldnList == null || vldnList.size() == 0)
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"errors.no.match.record.found",
					"Micro Deposit ACH Transaction"));
			saveErrors(request, errors);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
