package emgadm.microdeposit;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgshared.exceptions.ParseDateException;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class ListMDBatchesForm extends EMoneyGramAdmBaseValidatorForm
{
	private DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
	private String tranId;

	private String batchBeginDate;
	private String batchEndDate;

	private String submitGo;
	private String submitSearch;

	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(String string)
	{
		tranId = string;
	}

	public String getBatchBeginDate()
	{
		return batchBeginDate;
	}

	public void setBatchBeginDate(String string)
	{
		batchBeginDate = string;
	}

	public String getBatchEndDate()
	{
		return batchEndDate;
	}

	public void setBatchEndDate(String string)
	{
		batchEndDate = string;
	}

	public String getSubmitGo()
	{
		return submitGo;
	}

	public void setSubmitGo(String string)
	{
		submitGo = string;
	}

	public String getSubmitSearch()
	{
		return submitSearch;
	}

	public void setSubmitSearch(String string)
	{
		submitSearch = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		long begin = System.currentTimeMillis() - 10L * 24 * 60 * 60 * 1000;
		long end = System.currentTimeMillis() + 24L * 60 * 60 * 1000;
		batchBeginDate = df.format(new Date(begin));
		batchEndDate = df.format(new Date(end));
		submitGo = null;
		submitSearch = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitGo))
		{
			if (StringHelper.isNullOrEmpty(tranId))
			{
				errors.add(
					"tranId",
					new ActionError(
						"errors.required",
						"Micro Deposit Transaction Id"));
			}

			if (errors.size() == 0 && StringHelper.containsNonDigits(tranId))
			{
				errors.add(
					"tranId",
					new ActionError(
						"errors.integer",
						"Micro Deposit Transaction Id"));
			}
		}

		if (!StringHelper.isNullOrEmpty(submitSearch))
		{
			if (StringHelper.isNullOrEmpty(batchBeginDate))
			{
				errors.add(
					"batchBeginDate",
					new ActionError("errors.required", "Batch Begin Date"));
			}

			if (StringHelper.isNullOrEmpty(batchEndDate))
			{
				errors.add(
					"batchEndDate",
					new ActionError("errors.required", "Batch End Date"));
			}

			Date beginDate = null;
			try
			{
				beginDate = df.parse(batchBeginDate);
			} catch (ParseDateException e)
			{
				errors.add(
					"batcgBeginDate",
					new ActionError("error.malformed.date"));
			}

			Date endDate = null;
			try
			{
				endDate = df.parse(batchEndDate);
			} catch (ParseDateException e1)
			{
				errors.add(
					"batcgEndDate",
					new ActionError("error.malformed.date"));
			}

			if (beginDate.after(endDate))
			{
				String[] parms = { batchBeginDate, batchEndDate };
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"error.begin.date.later.than.end.date",
						parms));
			}
		}

		return errors;
	}
}