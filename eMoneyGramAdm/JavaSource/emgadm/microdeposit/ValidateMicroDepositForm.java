package emgadm.microdeposit;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgadm.view.AccountCommentView;
import emgshared.model.AccountStatus;
import emgshared.model.MicroDeposit;

public class ValidateMicroDepositForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String accountId;
	private String accountNumberMask;
	private String routingNumber;
	private String financialInstitutionName;
	private String statusCode;
	private String subStatusCode;
	private String accountTypeCode;
	private String accountTypeDesc;
	private String accountTaintText;
	private String abaTaintText;
	private AccountCommentView[] comments = new AccountCommentView[0];
	private MicroDeposit microDeposit;
	private String amt1;
	private String amt2;
	private String submitValidate;
	private String submitCancel;
	private String submitOverride;

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public void setAccountNumberMask(String string)
	{
		accountNumberMask = string;
	}

	public String getRoutingNumber()
	{
		return routingNumber;
	}

	public void setRoutingNumber(String string)
	{
		routingNumber = string;
	}

	public String getFinancialInstitutionName()
	{
		return financialInstitutionName;
	}

	public void setFinancialInstitutionName(String string)
	{
		financialInstitutionName = string;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public void setStatusCode(String string)
	{
		statusCode = string;
	}

	public void setSubStatusCode(String string)
	{
		subStatusCode = string;
	}

	public String getCombinedStatusCode()
	{
		return this.statusCode + AccountStatus.DELIMITER + this.subStatusCode;
	}

	public void setCombinedStatusCode(String combinedStatusCode)
	{
		String[] codes =
			StringUtils.split(combinedStatusCode, AccountStatus.DELIMITER);
		this.statusCode = (codes.length > 0 ? codes[0] : null);
		this.subStatusCode = (codes.length > 1 ? codes[1] : null);
	}

	public String getAccountTypeDesc()
	{
		return accountTypeDesc;
	}

	public void setAccountTypeDesc(String string)
	{
		accountTypeDesc = string;
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}

	public AccountCommentView[] getComments()
	{
		return comments;
	}

	public void setComments(AccountCommentView[] views)
	{
		comments = views;
	}

	public String getAccountTaintText()
	{
		return accountTaintText;
	}

	public void setAccountTaintText(String string)
	{
		accountTaintText = string;
	}

	public String getAbaTaintText()
	{
		return abaTaintText;
	}

	public void setAbaTaintText(String string)
	{
		abaTaintText = string;
	}

	public MicroDeposit getMicroDeposit()
	{
		return microDeposit;
	}

	public void setMicroDeposit(MicroDeposit deposit)
	{
		microDeposit = deposit;
	}

	public String getAmt1()
	{
		return amt1;
	}

	public void setAmt1(String string)
	{
		amt1 = string;
	}

	public String getAmt2()
	{
		return amt2;
	}

	public void setAmt2(String string)
	{
		amt2 = string;
	}

	public String getSubmitValidate()
	{
		return submitValidate;
	}

	public void setSubmitValidate(String string)
	{
		submitValidate = string;
	}

	public String getSubmitCancel()
	{
		return submitCancel;
	}

	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	public String getSubmitOverride()
	{
		return submitOverride;
	}

	public void setSubmitOverride(String string)
	{
		submitOverride = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		submitValidate = null;
		submitCancel = null;
		submitOverride = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitValidate))
		{
			errors = validateAmt(errors, amt1, "amt1");
			errors = validateAmt(errors, amt2, "amt2");
		}

		return errors;
	}

	private ActionErrors validateAmt(
		ActionErrors errors,
		String amt,
		String fldName)
	{
		if (StringHelper.isNullOrEmpty(amt))
		{
			errors.add(
				fldName,
				new ActionError("errors.required", "Deposit Amount"));
			return errors;
		}

		if (StringHelper.containsNonDigits(amt))
		{
			errors.add(
				fldName,
				new ActionError("errors.integer", "Deposit Amount"));
			return errors;
		}

		if (amt.length() != 2)
		{
			errors.add(
				fldName,
				new ActionError("errors.amt.length.wrong", "Deposit Amount"));
			return errors;
		}

		int tmpAmt = Integer.parseInt(amt);
		if (tmpAmt < 1 || tmpAmt > 20)
		{
			String[] parms = { "Deposit Amount", "1", "20" };
			errors.add(fldName, new ActionError("errors.range", parms));
		}

		return errors;
	}
}
