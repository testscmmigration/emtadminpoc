package emgadm.microdeposit;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmDynProperties;
import emgadm.services.DecryptionService;
import emgadm.util.StringHelper;
import emgadm.view.AccountCommentView;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.MicroDeposit;
import emgshared.model.MicroDepositStatus;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerAccountService;
import emgshared.services.MicroDepositService;
import emgshared.services.ServiceFactory;

public class ValidateMicroDepositAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ValidateMicroDepositForm form = (ValidateMicroDepositForm) f;
		UserProfile up = getUserProfile(request);
		ActionErrors errors = new ActionErrors();
		int accountId = Integer.parseInt(form.getAccountId());
		MicroDepositService mds =
			ServiceFactory.getInstance().getMicroDepositService();
		int tryCnt = EMTAdmDynProperties.getDPO(request).getMDRetryThreshold();

		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_CUSTOMER_PROFILE);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitOverride()))
		{
			MicroDeposit md =
				(MicroDeposit) request.getSession().getAttribute("saveMD");
			mds.overrideMicroDepositStatus(md);
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("msg.override.mic.dep.status"));
			try {
			    String detailText = "<FROM_STATUS>" + md.getStatus().getMdStatus() + "</FROM_STATUS><TO_STATUS>VAL</TO_STATUS>";
		    	super.insertActivityLog(this.getClass(),request,detailText,UserActivityType.EVENT_TYPE_CONSUMERPROFILEMDVALIDATIONSTATUSCHG,md.getCustAccountId() + UserActivityLog.dataKeyValueSeperator + md.getCustAccountVersionNbr());    
			} catch (Exception e) { }

			saveErrors(request, errors);
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitValidate()))
		{
			MicroDeposit md =
				(MicroDeposit) request.getSession().getAttribute("saveMD");
			md.setCallerLoginId(up.getUID());
			md.setDepositAmount1(
				(float) Integer.parseInt(form.getAmt1()) / 100L);
			md.setDepositAmount2(
				(float) Integer.parseInt(form.getAmt2()) / 100L);
			md = mds.validateDepositAmounts(md);

			if (md.getValidationTryCount() == 0)
			{
				MicroDepositStatus mdStatus = new MicroDepositStatus();
				mdStatus.setMdStatus(MicroDepositStatus.ACH_VALIDATED_CODE);
				md.setStatus(mdStatus);
				mds.updateMicroDepositValidation(md);
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("msg.mic.dep.validated"));
				try {
				    String detailText = "<FROM_STATUS>" + md.getStatus().getMdStatus() + "</FROM_STATUS><TO_STATUS>VAL</TO_STATUS>";
			    	super.insertActivityLog(this.getClass(),request,detailText,UserActivityType.EVENT_TYPE_CONSUMERPROFILEMDVALIDATIONSTATUSCHG,md.getCustAccountId() + UserActivityLog.dataKeyValueSeperator + md.getCustAccountVersionNbr());    
				} catch (Exception e) { }

			} else
			{
				if (md.getValidationTryCount() < tryCnt)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("msg.mic.dep.validate.fail"));
				} else
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("msg.over.max.retry"));
				}
				try {
				    String detailText = "<RESULT>Failure</Failure>";
			    	super.insertActivityLog(this.getClass(),request,detailText,UserActivityType.EVENT_TYPE_CONSUMERPROFILEMDVALIDATIONSTATUSCHG,md.getCustAccountId() + UserActivityLog.dataKeyValueSeperator + md.getCustAccountVersionNbr());    
				} catch (Exception e) { }

			}

			saveErrors(request, errors);
		}

		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();
		ConsumerBankAccount account =
			accountService.getBankAccount(accountId, up.getUID());
		populateForm(form, account, up);

		MicroDeposit md = new MicroDeposit();
		md.setCustAccountId(accountId);
		md.setCustAccountVersionNbr(1);
		Collection col = mds.getMicroDepositValidations(md);
		if (col == null || col.size() == 0)
		{
			form.setMicroDeposit(null);
		} else
		{
			Iterator iter = col.iterator();
			while (iter.hasNext())
			{
				md = (MicroDeposit) iter.next();
			}
			form.setMicroDeposit(md);
		}
		request.getSession().setAttribute("saveMD", md);
		request.getSession().setAttribute("acctCmnt", form.getComments());
		request.getSession().setAttribute(
			"showBttn",
			md.getValidationTryCount() < tryCnt ? "Y" : "N");

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private void populateForm(
		ValidateMicroDepositForm form,
		ConsumerBankAccount account,
		UserProfile userProfile)
		throws DataSourceException
	{
		int accountId = account.getId();
		form.setAccountId(String.valueOf(accountId));
		form.setAccountTypeCode(account.getType().getCode());
		form.setAccountTypeDesc(account.getTypeDescription());
		form.setCombinedStatusCode(account.getStatus().getCombinedCode());
		form.setCustId(String.valueOf(account.getConsumerId()));

		if (account.isAccountNumberBlocked())
		{
			form.setAccountTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isAccountNumberNotBlocked())
		{
			form.setAccountTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isAccountNumberOverridden())
		{
			form.setAccountTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else
		{
			form.setAccountTaintText("");
		}

		if (account.isBankAbaNumberBlocked())
		{
			form.setAbaTaintText(TaintIndicatorType.BLOCKED_TEXT);
		} else if (account.isBankAbaNumberNotBlocked())
		{
			form.setAbaTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
		} else if (account.isBankAbaNumberOverridden())
		{
			form.setAbaTaintText(TaintIndicatorType.OVERRIDE_TEXT);
		} else
		{
			form.setAbaTaintText("");
		}

		form.setFinancialInstitutionName(account.getFinancialInstitutionName());
		form.setRoutingNumber(account.getABANumber());

		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();

		if (userProfile.hasPermission("viewClearAccountNumbers"))
		{
			DecryptionService decryptionService =
				emgadm
					.services
					.ServiceFactory
					.getInstance()
					.getDecryptionService();
			String encryptedAccountNumber =
				accountService.getEncryptedAccountNumber(
					accountId,
					userProfile.getUID());
			String decryptedAccountNumber =
				decryptionService.decryptBankAccountNumber(
					encryptedAccountNumber);
			form.setAccountNumberMask(decryptedAccountNumber);
		} else
		{
			form.setAccountNumberMask("*****" + account.getAccountNumberMask());
		}

		form.setComments(
			createAccountProfileCommentViews(
				accountService.getAccountCmnts(
					accountId,
					userProfile.getUID())));
	}

	private AccountCommentView[] createAccountProfileCommentViews(List accountComments)
	{
		AccountCommentView[] views =
			new AccountCommentView[accountComments == null
				? 0
				: accountComments.size()];
		if (accountComments != null)
		{
			int i = 0;
			for (Iterator iter = accountComments.iterator();
				iter.hasNext();
				++i)
			{
				AccountComment comment = (AccountComment) iter.next();
				views[i] = new AccountCommentView(comment);
			}
		}
		return views;
	}
}
