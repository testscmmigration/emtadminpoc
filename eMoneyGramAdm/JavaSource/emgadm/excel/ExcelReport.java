package emgadm.excel;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import emgshared.exceptions.EMGRuntimeException;

public class ExcelReport extends HttpServlet
{
	String textFormat = "mso-number-format:\\@";
	String generalFormat = "mso-number-format:General";
	  String color = "white";
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		PrintWriter out = response.getWriter();
		response.setContentType("application/vnd.ms-excel");

		Collection col = null;
		String collectionName = "excelCollection";
		if (request.getParameter("id") != null)
		{
			collectionName = request.getParameter("id");
		}

		if (request.getSession().getAttribute(collectionName) == null)
		{
			throw new EMGRuntimeException(
				"No bean collection specified "
					+ "for generating Excel spreadsheet");
		}

		col = (Collection) request.getSession().getAttribute(collectionName);
		Iterator iter = col.iterator();

		out.println("<table border='1' cellPadding='2' cellSpacing='2'>");
		boolean firstTime = true;
		while (iter.hasNext())
		{
			Object obj = iter.next();
			if (firstTime)
			{
				setHeading(obj, out);
				firstTime = false;
			}
			setRow(obj, out);
		}
		
		out.println("</table");

		out.flush();
		out.close();
	}

	private void setRow(Object obj, PrintWriter out)
	{
		out.println("\t<tr>");
		Class cls = obj.getClass();
		Method[] methodList = cls.getDeclaredMethods();
		String format = null;
		
		for (int i = 0; i < methodList.length; i++)
		{
			Method method = methodList[i];
			String fldName = method.getName();
			boolean canInvoke = false;

			//  remove prefix from the method name
			if (fldName.substring(0, 3).equals("get"))
			{
				fldName = fldName.substring(3);
				canInvoke = true;
			} else if (fldName.substring(0, 2).equals("is"))
			{
				fldName = fldName.substring(2);
				canInvoke = true;
			}

			if (canInvoke && method.getParameterTypes().length == 0)
			{
				String value = "";
				try
				{
					value = method.invoke(obj, null).toString();
				} catch (Exception e)
				{
				}

				//Excel will convert nbrs > 11 length into Scientific notation & drop precision
				//Change numbers this long into text fields
				if (StringUtils.isNumeric(value) && value.length() > 11) {
					format = textFormat;
				} else {
					format = generalFormat;
				}
				out.print("\t\t<td  nowrap='nowrap' style='" + format + "; BACKGROUND-COLOR: "
					+ color + "; COLOR: black;  FONT: 10pt Arial, Helvetica'>");
				out.println(value + "</td>");
			}
		}
		out.println("\t</tr>");
		synchronized (color) {
			if ("white".equals(color)) {
				color = "#DDDDDD";
			} else {
				color = "white";
			}
		}
	}

	private void setHeading(Object obj, PrintWriter out)
	{
		out.println("\t<tr>");

		Class cls = obj.getClass();
		Method[] methodList = cls.getDeclaredMethods();

		for (int i = 0; i < methodList.length; i++)
		{
			Method method = methodList[i];
			String fldName = method.getName();
			boolean canInvoke = false;

			//  remove prefix from the method name
			if (fldName.substring(0, 3).equals("get"))
			{
				fldName = fldName.substring(3);
				canInvoke = true;
			} else if (fldName.substring(0, 2).equals("is"))
			{
				fldName = fldName.substring(2);
				canInvoke = true;
			}

			if (canInvoke && method.getParameterTypes().length == 0)
			{
				out.print("\t\t<td  nowrap='nowrap' style='BACKGROUND-COLOR: #8FB8E6;  COLOR: black;  FONT: BOLD 9pt Arial, Helvetica'>");
				out.println(fldName + "</td>");
			}
		}
		out.println("\t</tr>");
	}

	protected void doPost(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException
	{
		doGet(request, response);
	}
}
