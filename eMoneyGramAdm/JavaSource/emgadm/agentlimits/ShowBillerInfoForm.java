package emgadm.agentlimits;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowBillerInfoForm extends EMoneyGramAdmBaseValidatorForm
{
	private String id = null;
	private String detail = null;
	private String agentId = null;
	private String agentName = null;
	private String agentCity = null;
	private String receiveCode = null;
	private String limitAmt = null;
	private String submitReturn = null;
	private String paymentProfileId;

	public String getId() {
		return id;
	}

	public void setId(String string) {
		id = string;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String string) {
		detail = string;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String string) {
		agentId = string;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String string) {
		agentName = string;
	}

	public String getAgentCity() {
		return agentCity;
	}

	public void setAgentCity(String string) {
		agentCity = string;
	}

	public String getReceiveCode() {
		return receiveCode;
	}

	public void setReceiveCode(String string) {
		receiveCode = string;
	}

	public String getLimitAmt() {
		return limitAmt;
	}

	public void setLimitAmt(String amt) {
		this.limitAmt = amt;
	}

	public String getSubmitReturn() {
		return this.submitReturn;
	}

	public void setSubmitReturn(String s) {
		this.submitReturn = s;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		limitAmt = null;
	}

	public String getPaymentProfileId() {
		return paymentProfileId;
	}

	public void setPaymentProfileId(String paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		return errors;
	}
}
