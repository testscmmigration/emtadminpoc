package emgadm.agentlimits;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.constants.EMoneyGramMerchantIds;
import emgadm.util.StringHelper;
import emgshared.model.BillerLimit;
import emgshared.services.BillerLimitService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowBillerInfoAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ActionErrors errors = new ActionErrors();
		ShowBillerInfoForm form = (ShowBillerInfoForm) f;
		ActionForward forward = null;
		if ("Y".equalsIgnoreCase(form.getDetail()))
		{
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANSCTION_DETAIL);
		} else
		{
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
		}
		//  remove all session variables and exit the current screen
		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			return forward;
		}
		ServiceFactory sf = ServiceFactory.getInstance();
		BillerLimitService bls = sf.getBillerLimitService();
		BillerLimit billerLimit = null;
		billerLimit = bls.getBillerLimit(form.getAgentId());
		if (billerLimit != null)
		{
			form.setAgentId(billerLimit.getId());
			form.setAgentName(billerLimit.getName());
			form.setAgentCity(billerLimit.getCity());
			form.setReceiveCode(billerLimit.getReceiveCode());
			form.setLimitAmt(billerLimit.getLimit());
			form.setPaymentProfileId(billerLimit.getPaymentProfileId());
			form.setPaymentProfileId(EMoneyGramMerchantIds.getMerchantMap().get(billerLimit.getPaymentProfileId()));
		} else
		{
			errors.add( ActionErrors.GLOBAL_ERROR,new ActionError("error.biller.no.longer.exist", form.getId()));
			saveErrors(request, errors);
			return forward;
		}
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
