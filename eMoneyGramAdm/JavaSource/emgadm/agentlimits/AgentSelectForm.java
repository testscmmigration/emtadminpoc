package emgadm.agentlimits;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class AgentSelectForm extends EMoneyGramAdmBaseValidatorForm
{
	private String corpSelection = null;
	private Collection agents = new ArrayList();
	private String agentSelection = null;
	private String agentName = null;
	private String submitAgentFilter = null;
	private String submitAgent = null;
	private String submitClearFilter = null;
	private String submitReturn = null;
	private String submitExit = null;

	public String getCorpSelection() {
		return corpSelection;
	}

	public void setCorpSelection(String string) {
		corpSelection = string;
	}

	public Collection getAgents() {
		return agents;
	}

	public void setAgents(Collection collection) {
		agents = collection;
	}

	public String getAgentSelection() {
		return agentSelection == null ? "" : agentSelection;
	}

	public void setAgentSelection(String string) {
		agentSelection = string;
	}

	public String getAgentName() {
		return agentName == null ? "" : agentName;
	}

	public void setAgentName(String string) {
		agentName = string;
	}

	public String getSubmitAgentFilter() {
		return submitAgentFilter;
	}

	public void setSubmitAgentFilter(String string) {
		submitAgentFilter = string;
	}
	
	public String getSubmitAgent() {
		return submitAgent;
	}

	public void setSubmitAgent(String string) {
		submitAgent = string;
	}

	public String getSubmitClearFilter() {
		return submitClearFilter;
	}

	public void setSubmitClearFilter(String string)
	{
		submitClearFilter = string;
	}

	public String getSubmitExit() {
		return submitExit;
	}

	public void setSubmitExit(String string) {
		submitExit = string;
	}

	public String getSubmitReturn() {
		return submitReturn;
	}

	public void setSubmitReturn(String string) {
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		agentSelection = "";
		agentName = "";
		submitAgentFilter = null;
		submitAgent = null;
		submitClearFilter = null;
		submitReturn = null;
		submitExit = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		agents = (Collection) request.getSession().getAttribute("agents");
		if (submitAgentFilter != null &&
			StringHelper.isNullOrEmpty(agentName)) {
			errors.add(ActionErrors.GLOBAL_ERROR, 
				new ActionError("errors.required", "Biller Locations Search Key(s)")); 			
			}
		if (submitAgent != null &&
			StringHelper.isNullOrEmpty(agentSelection)) {
			errors.add(ActionErrors.GLOBAL_ERROR, 
				new ActionError("error.no.location.biller.selected")); 					
			}
		return errors;
	}
}
