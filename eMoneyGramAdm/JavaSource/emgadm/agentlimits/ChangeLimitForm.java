package emgadm.agentlimits;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgshared.util.StringHelper;

public class ChangeLimitForm extends EMoneyGramAdmBaseValidatorForm
{
	private String limitAmt = null;
	private String paymentProfileId = null;
	private String submitSave = null;
	private String submitCancel = null;
	
	public String getPaymentProfileId() {
		return paymentProfileId;
	}

	public void setPaymentProfileId(String paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}

	public String getLimitAmt() {
		return this.limitAmt == null ? "0.000" : limitAmt;
	}

	public void setLimitAmt(String amt) {
		this.limitAmt = amt;
	}

	public String getSubmitSave() {
		return this.submitSave;
	}

	public void setSubmitSave(String s) {
		this.submitSave = s;
	}

	public String getSubmitCancel() {
		return this.submitCancel;
	}

	public void setSubmitCancel(String s) {
		this.submitCancel = s;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		limitAmt = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		if (submitSave != null) {
			if (StringHelper.isNullOrEmpty(getPaymentProfileId()))
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Payment Profile ID"));
			else if (getPaymentProfileId().equalsIgnoreCase("(Select)"))
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Payment Profile ID"));
			if(StringHelper.isNullOrEmpty(getLimitAmt())){
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.required", "Dollar Amount Limit"));
			} else if(StringHelper.containsNonDigits(getLimitAmt(), new char[]{'.'})){
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.invalid", "Dollar Amount Limit"));
			} else if(Double.parseDouble(getLimitAmt())>2500||Double.parseDouble(getLimitAmt())<1){
				errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("errors.amount.limit.not.in.range", 1,"2500"));
			}
			//errors = super.validate(mapping, request);
		}
		return errors;
	}
}
