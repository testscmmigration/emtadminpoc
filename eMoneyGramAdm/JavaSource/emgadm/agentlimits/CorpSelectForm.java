package emgadm.agentlimits;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class CorpSelectForm extends EMoneyGramAdmBaseValidatorForm {
	private String corpSelection = null;
	private Collection corps = new ArrayList();
	private String corporationName = null;
	private String submitCorpFilter = null;
	private String submitCorp = null;
	private String submitClearFilter = null;
	private String submitCancel = null;
	private String submitReturn = null;

	public String getCorpSelection() {
		return corpSelection;
	}

	public void setCorpSelection(String c) {
		this.corpSelection = c;
	}

	public Collection getCorps() {
		return corps;
	}

	public void setCorps(Collection collection) {
		corps = collection;
	}

	public String getCorporationName() {
		return corporationName == null ? "" : corporationName;
	}

	public void setCorporationName(String c) {
		this.corporationName = c;
	}

	public String getSubmitCorpFilter() {
		return submitCorpFilter;
	}

	public void setSubmitCorpFilter(String s) {
		this.submitCorpFilter = s;
	}

	public String getSubmitCorp() {
		return submitCorp;
	}

	public void setSubmitCorp(String s) {
		this.submitCorp = s;
	}

	public String getSubmitClearFilter() {
		return submitClearFilter;
	}

	public void setSubmitClearFilter(String string) {
		submitClearFilter = string;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public String getSubmitReturn() {
		return submitReturn;
	}

	public void setSubmitReturn(String string) {
		submitReturn = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		corpSelection = "";
		corporationName = "";
		submitCorpFilter = null;
		submitCorp = null;
		submitClearFilter = null;
		submitCancel = null;
		submitReturn = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		if (submitCorpFilter != null &&
			StringHelper.isNullOrEmpty(corporationName)) {
			corps = (Collection) request.getSession().getAttribute("corps");
			errors.add(ActionErrors.GLOBAL_ERROR, 
				new ActionError("errors.required", "Biller Main Offices Search Key(s)")); 			
			}
		return errors;
	}
}