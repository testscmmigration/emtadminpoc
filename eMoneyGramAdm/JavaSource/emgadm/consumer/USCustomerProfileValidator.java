package emgadm.consumer;

import emgadm.consumer.BaseCustomerProfileValidator.StrictNumericField;
import emgadm.model.UserProfile;

public class USCustomerProfileValidator extends BaseCustomerProfileValidator {

	public USCustomerProfileValidator(UpdateCustomerProfileForm form, UserProfile userProfile) {
		super(form, userProfile);
		fields.add(new StrictNumericField(form.getPostalCode(), "Zip Code").setMaxLength(5));
		fields.add(new StrictNumericField(form.getZip4(), "Zip Code").setMaxLength(4).isRequired(false));
		fields.add(new AlphaField(form.getState(), "State"));
		fields.add(new StrictNumericField(form.getLoyaltyPgmMembershipId(), "Plus #").setMinLength(9).setMaxLength(12).isRequired(false));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), "Phone Number").setMinLength(10).setMaxLength(10).setUID(userProfile.getUID()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(), "Phone Number Alternate").isRequired(false).setMinLength(10).setMaxLength(10).setUID(userProfile.getUID()));
	}

}
