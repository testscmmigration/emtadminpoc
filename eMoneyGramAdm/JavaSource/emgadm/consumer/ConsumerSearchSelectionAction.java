package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.util.StringHelper;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.util.Constants;

public class ConsumerSearchSelectionAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws AgentConnectException, DataSourceException
	{

		ConsumerSearchSelectionForm form = (ConsumerSearchSelectionForm) f;

		ConsumerProfile cf =
			(ConsumerProfile) request.getSession().getAttribute("findProfiles");
		form.setCustId(String.valueOf(cf.getId()));
		form.setCustLogonId(cf.getUserId());
		form.setCustFirstName(cf.getFirstName());
		form.setCustLastName(cf.getLastName());
		form.setPartnerSiteId(cf.getPartnerSiteId());

		if (!StringHelper.isNullOrEmpty(form.getSubmitGo()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADVANCED_CONSUMER_SEARCH);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitSelAll())) {
			form.setIncludeFirstName(true);
			form.setIncludeLastName(true);
			form.setIncludeLoginId(true);
			form.setIncludePhone(true);
			form.setIncludeAddr(true);
			form.setIncludeCity(true);
			if(!cf.getPartnerSiteId().equals(Constants.MGODE_PARTNER_SITE_ID)){
				form.setIncludeState(true);
			} else {
				form.setIncludeState(false);
			}
			form.setIncludeZip(true);
			form.setIncludeProfileStatus(true);
			form.setIncludeCreateDate(true);
			form.setIncludePasswordHash(true);
			form.setIncludeCreateIpAddrId(true);
			form.setIncludePrmrCode(true);
		} else if (!StringHelper.isNullOrEmpty(form.getSubmitDeselAll()))
		{
			form.setIncludeFirstName(false);
			form.setIncludeLastName(false);
			form.setIncludeLoginId(false);
			form.setIncludePhone(false);
			form.setIncludeAddr(false);
			form.setIncludeCity(false);
			form.setIncludeState(false);
			form.setIncludeZip(false);
			form.setIncludeProfileStatus(false);
			form.setIncludeCreateDate(false);
			form.setIncludePasswordHash(false);
			form.setIncludeCreateIpAddrId(false);
			form.setIncludePrmrCode(false);
		}

		form.setFindProfiles("Y");

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
