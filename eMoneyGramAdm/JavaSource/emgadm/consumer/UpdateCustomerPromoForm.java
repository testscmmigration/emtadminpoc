/*
 * Created on Dec 23, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A135
 *
 */
public class UpdateCustomerPromoForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private boolean acceptPromotionalEmail;

	public boolean isAcceptPromotionalEmail()
	{
		return acceptPromotionalEmail;
	}

	public String getCustId()
	{
		return custId;
	}

	public void setAcceptPromotionalEmail(boolean b)
	{
		acceptPromotionalEmail = b;
	}

	public void setCustId(String string)
	{
		custId = string;
	}
}
