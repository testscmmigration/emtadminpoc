/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.BlockedBean;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.util.StringHelper;

/**
 * @author A131
 *
 */
public class ClearSSNTaintAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
	private static final FraudService fraudService = emgshared.services.ServiceFactory.getInstance().getFraudService();


	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException, MaxRowsHashCryptoException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		String userId = getUserProfile(request).getUID();
		ClearSSNTaintForm inputForm = (ClearSSNTaintForm) form;
		String custId = inputForm.getCustId();
        long beginTime = System.currentTimeMillis();
        int consumerId = Integer.parseInt(custId);
        ConsumerProfile consumerProfile = null;
        forward = mapping.findForward(FORWARD_SUCCESS);
        try {
            consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), userId, null);
        } catch (Exception e) {
            forward = mapping.findForward(FORWARD_FAILURE);
        }
        consumerProfile.setSsnTaintCode(TaintIndicatorType.NOT_BLOCKED);
        if (consumerProfile.isPhoneBlocked() || consumerProfile.isPhoneAlternateBlocked() || consumerProfile.isAccountBlocked() ||
        		consumerProfile.isEmailBlocked() || consumerProfile.isEmailDomainBlocked()) {
        	fraudService.clearSSNBlockIndicator(custId, false, userId);
		}
        else {
        	fraudService.clearSSNBlockIndicator(custId, true, userId);
		}

		if (errors.size() > 0) {
			saveErrors(request, errors);
		}

		return forward;
	}
}
