/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.EPartnerSite;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerAddress;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;
import emgshared.util.DateManager;

/**
 * @author A131
 *
 */
public class ViewAddressHistoryAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		ViewAddressHistoryForm inputForm = (ViewAddressHistoryForm) form;
		UserProfile up = getUserProfile(request);
		int customerId = Integer.parseInt(request.getParameter("custId"));
		inputForm.setCountry(request.getParameter("country"));
		request.setAttribute("timeZone", EPartnerSite.getPartnerByCountry(inputForm.getCountry()).getTimeZone());

		ConsumerProfileService consumerProfileService =
			ServiceFactory.getInstance().getConsumerProfileService();
		HashSet<ConsumerAddress> addresses = (HashSet<ConsumerAddress>) consumerProfileService.getConsumerAddressHistory(customerId, up.getUID());
		TreeSet<ConsumerAddress> ts = new TreeSet<ConsumerAddress>();
		for (Iterator iterator = addresses.iterator(); iterator.hasNext();) {
			ConsumerAddress consumerAddress = (ConsumerAddress) iterator.next();
			ts.add(consumerAddress);
		}
		request.setAttribute("custId", customerId);
		request.setAttribute("addresses", ts);
		request.setAttribute("addressCount", addresses.size());
		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}



}
