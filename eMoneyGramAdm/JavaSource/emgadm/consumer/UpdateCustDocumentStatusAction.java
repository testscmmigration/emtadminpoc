/*
 * Created on Oct 23, 2012
 *
 */
package emgadm.consumer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.ConsumerProfileDashboard;
import emgadm.model.UserProfile;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.SameStatusException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerStatus;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import emgshared.services.TransactionService;

/**
 * @author vy79
 * 
 */
public class UpdateCustDocumentStatusAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, DataSourceException {
		ActionForward forward = null;
		ActionErrors errors = new ActionErrors();
		UpdateCustDocumentStatusForm inputForm = (UpdateCustDocumentStatusForm) form;
		UserProfile up = getUserProfile(request);
		String consumerIdText = inputForm.getCustId();
		int consumerId = Integer.parseInt(consumerIdText);

		ConsumerProfileDashboard cpd = (ConsumerProfileDashboard) request.getSession().getAttribute("dashboard");
		List<Transaction> trans = (List<Transaction>) cpd.getTotalTranList();
		ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
		TransactionManager tm = getTransactionManager(request);

		String path = mapping.getPath();
		if (path.equals("/" + EMoneyGramAdmForwardConstants.LOCAL_FORWARD_APPROVE_DOCUMENT)) {
			if (inputForm.getSubmitApproveDocument() != null) {
				request.setAttribute("docAction", "approve");
			} else if (inputForm.getSubmitDenyDocument() != null) {
				request.setAttribute("docAction", "deny");
			} else if (inputForm.getSubmitPendDocument() != null) {
				request.setAttribute("docAction", "pend");
			}
			request.setAttribute("tranIdList", trans);
			request.setAttribute("action", "TakeOwnership");
			request.setAttribute("releaseTransactionDocumentStatus", "Y");
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANS_OWNERSHIP);
		} else if (path.equals("/" + EMoneyGramAdmForwardConstants.LOCAL_FORWARD_RELEASE_TRANSACTIONS_DOCUMENT_STATUS)) {
			ConsumerProfile consumerProfile = null;
			// do stuff to transactions here
			try {
				consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), up.getUID(), null);
				Transaction lastTrans=null;
				if (inputForm.getSubmitApproveDocument() != null) {
					int changedTransNumber =0;
					for (Transaction tran : trans) {
						if (tran.getStatus().toString().equals(TransactionStatus.PENDING_NOT_FUNDED.toString())
								|| tran.getStatus().toString().equals(TransactionStatus.SAVE_TRAN.toString())) {
							tran.setStatus(TransactionStatus.AUTOMATIC_SCORING_PROCESS_NOT_FUNDED);
							tm.updateTransaction(tran, up.getUID());
							changedTransNumber++;
						}
						lastTrans=tran;
					}
					profileService.updateCustDocumentStatus(consumerProfile, EMoneyGramAdmApplicationConstants.APPROVED_DOC_STATUS);
					ConsumerStatus currStatus =	ConsumerStatus.getInstanceByCombinedCode(inputForm.getConsumerStatus());
					if(currStatus.getCombinedCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7.getCombinedCode())&&changedTransNumber==0){
						profileService.updateStatus(consumerId, ConsumerStatus.ACTIVE_VALIDATION_LEVEL_8, consumerProfile.getUserId(), false);
					} else if(currStatus.getCombinedCode().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7.getCombinedCode())){
						profileService.updateStatus(consumerId, ConsumerStatus.ACTIVE_VALIDATION_LEVEL_9, consumerProfile.getUserId(), false);
					}
					if(lastTrans!=null){
						Transaction tran = tm.getTransaction(lastTrans.getEmgTranId());
						sendNotifications(consumerProfile, tran, TransactionStatus.APPROVED_CODE);
					}
				} else if (inputForm.getSubmitPendDocument() != null) {
					for (Transaction tran : trans) {
						if (tran.getStatus().toString().equals(TransactionStatus.SAVE_TRAN.toString())) {
							tran.setStatus(TransactionStatus.PENDING_NOT_FUNDED);
							tm.updateTransaction(tran, up.getUID());
						}
						lastTrans=tran;
					}
					consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), up.getUID(), null);
					profileService.updateCustDocumentStatus(consumerProfile, EMoneyGramAdmApplicationConstants.PENDING_DOC_STATUS);
					if(lastTrans!=null){
						Transaction tran = tm.getTransaction(lastTrans.getEmgTranId());
						sendNotifications(consumerProfile, tran, TransactionStatus.PENDING_CODE);
					}

				} else if (inputForm.getSubmitDenyDocument() != null) {
					for (Transaction tran : trans) {
						if (tran.getStatus().toString().equals(TransactionStatus.PENDING_NOT_FUNDED.toString())
								|| tran.getStatus().toString().equals(TransactionStatus.SAVE_TRAN.toString())) {
							tran.setStatus(TransactionStatus.DENIED_NOT_FUNDED);
							tm.updateTransaction(tran, up.getUID());
						}
						lastTrans=tran;
					}
					consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), up.getUID(), null);
					profileService.updateCustDocumentStatus(consumerProfile, EMoneyGramAdmApplicationConstants.DENIED_DOC_STATUS);
					if(!consumerProfile.getStatus().toString().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7.toString())){
						profileService.updateStatus(consumerId, ConsumerStatus.ACTIVE_VALIDATION_LEVEL_7, consumerProfile.getUserId(), false);
					}
					if(lastTrans!=null){
						Transaction tran = tm.getTransaction(lastTrans.getEmgTranId());
						sendNotifications(consumerProfile, tran, TransactionStatus.DENIED_CODE);
					}
				}

				request.setAttribute("tranIdList", trans);
				request.setAttribute("action", "Release");
				request.setAttribute("documentStatus", "Y");
				forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TRANS_OWNERSHIP);
			} catch (SameStatusException e) {
				errors.add("headerMessage", new ActionMessage("error.status.no.change"));
				forward = mapping.findForward(FORWARD_FAILURE);
			} catch (DuplicateActiveProfileException e) {
				request.setAttribute("duplicateOverride", "Y");
				errors.add("custStatus", new ActionMessage("errors.duplicate.profile"));
				forward = mapping.findForward(FORWARD_FAILURE);
			}

		} else {
			forward = mapping.findForward(FORWARD_SUCCESS);
		}
		return forward;
	}

	private void sendNotifications(ConsumerProfile consumerProfile, Transaction tran,String status) {
		String preferedlanguage = consumerProfile.getPreferedLanguage();
		NotificationAccess na = new NotificationAccessImpl();
		Set<ConsumerEmail> eMails=(HashSet)consumerProfile.getEmails();
		if(status.equals(TransactionStatus.PENDING_CODE)){
			for (ConsumerEmail email : eMails){
				na.notifyIdPendingStatus(tran, email,preferedlanguage);
			}
		} else if(status.equals(TransactionStatus.DENIED_CODE)){
			for (ConsumerEmail email : eMails){
				na.notifyIdDeniedStatus(tran, email,preferedlanguage);
			}
		} else if(status.equals(TransactionStatus.APPROVED_CODE)){
			for (ConsumerEmail email : eMails){
				na.notifyApprovedWithTX(tran, email,preferedlanguage);
			}
		}
		
	}
}
