package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateCustPremierCodeForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String prmrCode;

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public String getPrmrCode()
	{
		return prmrCode;
	}

	public void setPrmrCode(String string)
	{
		prmrCode = string;
	}
}
