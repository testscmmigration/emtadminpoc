package emgadm.consumer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import shared.mgo.services.MGOServiceFactory;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.model.PartnerSite;
import emgadm.model.UserProfile;
import emgadm.services.TransactionService;
import emgadm.sysmon.EventListConstant;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.model.CustPremierType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;
import eventmon.eventmonitor.EventMonitor;

public class AdvancedConsumerSearchAction extends EMoneyGramAdmBaseAction {
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	private List custPrmrCodes = null;

	public ActionForward execute(ActionMapping mapping, ActionForm f, HttpServletRequest request, HttpServletResponse response)
			throws AgentConnectException, DataSourceException {
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		AdvancedConsumerSearchForm form = (AdvancedConsumerSearchForm) f;
		UserProfile up = getUserProfile(request);
		custPrmrCodes = (List) request.getSession().getServletContext().getAttribute("custPrmrCodes");
		Collection<ConsumerProfileSearchView> consumerProfileList = null;
		ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
		emgadm.services.ServiceFactory sf = emgadm.services.ServiceFactory.getInstance();
		TransactionService ts = sf.getTransactionService();
		if ("Y".equals(form.getFindProfiles()) && request.getSession().getAttribute("findProfiles") != null) {
			ConsumerProfile cf = (ConsumerProfile) request.getSession().getAttribute("findProfiles");
			// request.getSession().removeAttribute("findProfiles");
			if (form.isIncludeLoginId()) {
				form.setSearchLoginId(cf.getUserId());
			}
			if (form.isIncludeProfileStatus()) {
				form.setSearchProfileStatus(cf.getStatus().getCombinedCode());
			}
			if (form.isIncludeFirstName()) {
				form.setSearchFirstName(cf.getFirstName());
			}
			if (form.isIncludeLastName()) {
				form.setSearchLastName(cf.getLastName());
			}
			if (form.isIncludeAddr()) {
				form.setSearchAddr(cf.getAddressLine1());
			}
			if (form.isIncludeCity()) {
				form.setSearchCity(cf.getCity());
			}
			if (form.isIncludeState()) {
				form.setSearchState(cf.getState());
			}
			if (form.isIncludeZip()) {
				form.setSearchZip(cf.getPostalCodePlusZip4());
			}
			if (form.isIncludePhone()) {
				form.setSearchPhone(cf.getPhoneNumber());
			}
			if (form.isIncludePasswordHash()) {
				form.setSearchPasswordHash(cf.getPswdText());
			}
			if (form.isIncludeCreateDate()) {
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				form.setSearchCreateDate(df.format(cf.getCreateDate()));
			}
			
			if (form.isIncludeCreateIpAddrId()) {
				form.setSearchCreateIpAddrId(cf.getCreateIpAddrId());
			}
			if (form.isIncludePrmrCode()) {
				form.setSearchPrmrCode(cf.getCustPrmrCode());
			}
			form.setPartnerSiteId("ALL");
			form.setSubmitSearch("Search");
		} else if (StringHelper.isNullOrEmpty(form.getFirstTime1())) {
			form.setSubmitSearch(null);
			request.getSession().removeAttribute("profileList");
		}
		form.setFirstTime1("Y");
		if (!StringHelper.isNullOrEmpty(form.getSubmitSearch())) {
			long beginTime = System.currentTimeMillis();
			ConsumerProfileSearchCriteria criteria = new ConsumerProfileSearchCriteria();
			if (!StringHelper.isNullOrEmpty(form.getSearchLoginId())) {
				criteria.setCustLogonId(form.getSearchLoginId());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchFirstName())) {
				criteria.setCustFrstName(form.getSearchFirstName());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchLastName())) {
				criteria.setCustLastName(form.getSearchLastName());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchSecondLastName())) {
				criteria.setCustScndLastName(form.getSearchSecondLastName());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchPhone())) {
				criteria.setCustPhoneNumber(form.getSearchPhone());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchSSN())) {
				criteria.setCustSSNMask(form.getSearchSSN());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCC4())) {
				criteria.setCustCCMask(form.getSearchCC4());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchBankAcct())) {
				criteria.setCustBankMask(form.getSearchBankAcct());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchAddr())) {
				criteria.setCustAddrLine1Text(form.getSearchAddr());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCity())) {
				criteria.setCustAddrCityName(form.getSearchCity());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchState())) {
				criteria.setCustAddrStateName(form.getSearchState());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchZip())) {
				criteria.setCustAddrPostalCode(form.getSearchZip());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCreateDate())) {
				criteria.setCustCreateDate(form.getSearchCreateDate());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCreateDateTo())) {
				criteria.setCustCreateDateTo(form.getSearchCreateDateTo());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCreateIpAddrId())) {
				criteria.setCustCreateIpAddrId(form.getSearchCreateIpAddrId());
			}
			if (form.isRemovePasswordSearch()) {
				form.setSearchPasswordHash(null);
			} else {
				criteria.setCustPswdText(form.getSearchPasswordHash());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchPasswordHash())) {
				criteria.setCustPswdText(form.getSearchPasswordHash());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchPrmrCode())) {
				criteria.setCustPrmrCode(form.getSearchPrmrCode());
			}

			if (!StringHelper.isNullOrEmpty(form.getSearchId())) {

				if(form.getSearchId().length() != 4) {
					criteria.setCustAdditionalIdEncrypted(form.getSearchId());
				}
				else {
					criteria.setCustAdditionalIdMask(form.getSearchId());
				}
			}

			if (!StringHelper.isNullOrEmpty(form.getAdditionalIdType())) {
				criteria.setCustAdditionalIdType(String.valueOf(form.getAdditionalIdType()));
			}

			if (!StringHelper.isNullOrEmpty(form.getSearchProfileStatus())) {
				String status = form.getSearchProfileStatus();
				int pos = status.indexOf(":");
				criteria.setCustStatCode(status.substring(0, pos));
				criteria.setCustSubstatCode(status.substring(pos + 1));
			}
			criteria.setCustProgramId(form.getPartnerSiteId());
			if (form.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)
					&&!StringHelper.isNullOrEmpty(form.getAdditionalDocStatus())) {
				criteria.setCustAdditionalDocStatus(String.valueOf(form.getAdditionalDocStatus()));
			}

			criteria.setSortBy(form.getSortBy());
			try {
				Collection<PartnerSite> partSites = ManagerFactory.createTransactionManager().getPartnerSiteIds("B");
				HashMap<String, String> partnerSites = new HashMap<String, String>();
				for (Iterator iterator = partSites.iterator(); iterator.hasNext();) {
					PartnerSite partnerSite = (PartnerSite) iterator.next();
					partnerSites.put(partnerSite.getPartnetSideIdCode(), partnerSite.getPartnerSiteId());
				}

				emgshared.services.ServiceFactory serviceFactory = ServiceFactory.getInstance();
				ObfuscationService obfuscationService = serviceFactory.getObfuscationService();

				// encrypting id if the search is over the total id number
				if(!StringHelper.isNullOrEmpty(criteria.getCustAdditionalIdEncrypted())) {
					String encryptedId = obfuscationService.getAdditionalIdObfuscation(criteria.getCustAdditionalIdEncrypted());
					criteria.setCustAdditionalIdEncrypted(encryptedId);
				}

				consumerProfileList = profileService.searchConsumerProfile(criteria, false, getUserProfile(request).getUID(), partnerSites);
				/*
				 * request.setAttribute("taint", "N"); if (form.isShowTainted())
				 * { Iterator iter = consumerProfileList.iterator(); while
				 * (iter.hasNext()) { ConsumerProfileSearchView cpsv =
				 * (ConsumerProfileSearchView) iter .next(); if
				 * ("Tainted".equalsIgnoreCase(cpsv.getTaintedText())) {
				 * request.setAttribute("taint", "Y"); break; } } }
				 */
			} catch (SQLException sqle) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("exception.unexpected"));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
			} catch (DataSourceException e) {
				e.printStackTrace();
			} catch (TooManyResultException e) {
				ts.sendConsumerAdhocSearchTraceMail(criteria, up.getUID());
				errors
						.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.too.many.results", dynProps.getMaxDownloadTransactions()
								+ ""));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			} finally {
				EventMonitor.getInstance().recordEvent(EventListConstant.CONSUMER_SEARCH, beginTime, System.currentTimeMillis());
			}

			//If doing 'search similar profiles', no partner site is on dropdown
			if (form.getPartnerSiteId() == null) {
				form.setPartnerSiteId("ALL");
				form.setAdditionalDocStatus("");
			}
			// TODO Remove once the SP is updated so the collection is filtered in the response of the DB
			if (!form.getPartnerSiteId().equals("ALL")) {
				List<ConsumerProfileSearchView> filteredProfiles = new LinkedList<ConsumerProfileSearchView>();
				for (ConsumerProfileSearchView profile : consumerProfileList) {
					if (profile.getCustCreatSrcWebSite().equals(form.getPartnerSiteId())) {
						filteredProfiles.add(profile);
					}
				}
				consumerProfileList = filteredProfiles;
			}

			if (consumerProfileList == null || consumerProfileList.size() != 0) {
				consumerProfileList = addPrmrDesc((List) consumerProfileList);
				request.getSession().setAttribute("profileList", consumerProfileList);
				ActionMessage myMsg = new ActionMessage("message.number.consumers.found", String.valueOf(consumerProfileList.size()));
				messages.add("nbrFound", myMsg);
				saveMessages(request, messages);
			} else {
				request.getSession().removeAttribute("profileList");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.consumer.profile.search.notfound"));
				saveErrors(request, errors);
			}
		}
		setStates(request, up.getUID());
		if (request.getSession().getAttribute("custStatusOptions") == null) {
			request.getSession().setAttribute("custStatusOptions",
					convertMapToLabelValueListSorted(profileService.getConsumerStatusDescriptions()));
		}

		if (request.getSession().getAttribute("additionalIdTypes") == null) {
			request.getSession().setAttribute("additionalIdTypes",
					convertMapToLabelValueList(EMoneyGramAdmApplicationConstants.additionalIdMap));
		}

		if (request.getSession().getAttribute("additionalDocStatuses") == null) {
			request.getSession().setAttribute("additionalDocStatuses",
					convertMapToLabelValueList(EMoneyGramAdmApplicationConstants.docStatusMap));
		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	/**
	 * This method to get the states from AC State object and to sort the states for USA in Ascending order.
	 *
	 * @param request
	 * @param userId
	 */
	
	private void setStates(HttpServletRequest request, String userId) {
		ArrayList<com.moneygram.agentconnect1305.wsclient.StateProvinceInfo> stateInfoList = new ArrayList<com.moneygram.agentconnect1305.wsclient.StateProvinceInfo>();
		try {
			stateInfoList = (ArrayList<com.moneygram.agentconnect1305.wsclient.StateProvinceInfo>) MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank().getStateProvinceInfo("USA");
			
			Collections.sort(stateInfoList, new Comparator<com.moneygram.agentconnect1305.wsclient.StateProvinceInfo>() {
				 public int compare(com.moneygram.agentconnect1305.wsclient.StateProvinceInfo state1, com.moneygram.agentconnect1305.wsclient.StateProvinceInfo state2)
	             {
	                 return state1.getStateProvinceName().compareTo(state2.getStateProvinceName());
	             }   
			});
			
		} catch (Exception e) {
			String msg = "Failed getStateProvinceInfo from AgentConnectService - setStates:";
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(msg, e);
		}
		request.getSession().setAttribute("states", stateInfoList);
		return;
	}

	private List addPrmrDesc(List profileList) {
		Iterator iter = profileList.iterator();
		while (iter.hasNext()) {
			ConsumerProfileSearchView view = (ConsumerProfileSearchView) iter.next();
			if (!StringHelper.isNullOrEmpty(view.getCustPrmrCode())) {
				view.setCustPrmrCode(getPrmrDesc(view.getCustPrmrCode()));
			}
		}
		return profileList;
	}

	private String getPrmrDesc(String key) {
		Iterator iter = custPrmrCodes.iterator();
		while (iter.hasNext()) {
			CustPremierType cpt = (CustPremierType) iter.next();
			if (cpt.getCustPrmrCode().equalsIgnoreCase(key)) {
				return key + " (" + cpt.getCustPrmrDesc() + ")";
			}
		}
		return key;
	}
}
