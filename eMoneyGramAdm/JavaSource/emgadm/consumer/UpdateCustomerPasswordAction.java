/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.moneygram.security.ldap.LdapService;
import com.moneygram.security.ldap.LdapServiceImpl;
import com.moneygram.security.ldap.LdapUser;
import com.moneygram.security.ldap.exceptions.LdapBadPasswordException;
import com.moneygram.security.ldap.exceptions.LdapGeneralException;
import com.moneygram.security.ldap.exceptions.LdapUserNotFoundException;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class UpdateCustomerPasswordAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException, LdapGeneralException
	{
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UpdateCustomerPasswordForm inputForm =
			(UpdateCustomerPasswordForm) form;
		String password = inputForm.getNewValue();
		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(inputForm.getCustId());
		
		ConsumerProfileService profileService =
			ServiceFactory.getInstance().getConsumerProfileService();
		ConsumerProfile cp =
			(ConsumerProfile) request.getSession().getAttribute(
					"findProfiles");
		
		ObfuscationService obfuscationService =
			emgshared.services.ServiceFactory.getInstance().getObfuscationService();

		String encryptedPassword = obfuscationService.getPasswordObfuscation(password);
		Date expirationDate = new Date();
		profileService.updatePassword(userId,custId,encryptedPassword,	cp.getPswdText(),expirationDate);	
		LdapService ldapService = new LdapServiceImpl();
		LdapUser ldapUser = ldapService.findUser(cp.getUserId());
		if (ldapUser != null){
			if (ldapUser.getGuid() != null && cp.getEdirGuid() != null && ldapUser.getGuid().equals(cp.getEdirGuid())){
				try {
					ldapService.updatePassword(cp.getUserId(), password);
				} catch (LdapUserNotFoundException e){
					errors.add("headerMessage", new ActionMessage("error.edir.user.not.found", cp.getUserId()));
				} catch (LdapBadPasswordException e){
					errors.add("headerMessage", new ActionMessage("error.edir.bad.password"));
				} catch (Exception e){
					errors.add("headerMessage", new ActionMessage("error.edir.ldap.general"));
				}
			}
			else {
				errors.add("headerMessage", new ActionMessage("error.edir.guid.not.matched", ldapUser.getGuid(), cp.getEdirGuid()));
			}
		}
		else {
			errors.add("headerMessage", new ActionMessage("error.edir.user.not.found", cp.getUserId()));
		}
		
		if (!errors.isEmpty()){
			saveErrors(request, errors);
			forward = mapping.findForward(FORWARD_FAILURE);
		}
		else {
			try {
				ConsumerProfileActivity cpa = new ConsumerProfileActivity();
				cpa.setCustId(cp.getId());
				cpa.setActivityLogCode(ConsumerProfileActivity.PASSWORD_CHANGED_CODE);
				profileService.insertConsumerActivityLog(cpa, userId);
				super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_CONSUMERPROFILEPASSWORDCHG,String.valueOf(custId));    
			} catch (Exception ignore) { }

			//Add status change message next to "Reset Password" button				
			errors.add(
				"headerMessage",
				new ActionMessage("msg.password.changed"));
			saveErrors(request, errors);
			forward = mapping.findForward(FORWARD_SUCCESS);
		}
		return forward;
	}
}
