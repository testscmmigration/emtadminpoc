package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;
import emgshared.util.IPAddressValidator;


public class AdvancedConsumerSearchForm extends EMoneyGramAdmBaseValidatorForm {
	private static final String PASSPORT_FORMAT = "%s|%s|%s|%s|%s|%s|%s|%s";
	private static final String DRIVERS_LICENSE_FORMAT = "%s|%s|%s|%s";
	private static final String EU_ID_CARD_FORMAT = "%s|%s|%s|%s|%s|%s|%s";

	private String firstTime1;
	private String searchId;
	private String searchFirstName;
	private String searchMiddleName;
	private String searchSecondLastName;
	private String searchLastName;
	private String searchLoginId;
	private String searchPhone;
	private String searchSSN;
	private String searchCC4;
	private String searchBankAcct;
	private String searchAddr;
	private String searchCity;
	private String searchState;
	private String searchZip;
	private String searchProfileStatus;
	private String searchCreateDate;
	private String searchCreateDateTo;
	private String searchCreateIpAddrId;
	private String searchPasswordHash;
	private String searchPrmrCode;
	private String sortBy;
	private boolean removePasswordSearch;
	private boolean includeFirstName;
	private boolean includeLastName;
	private boolean includeLoginId;
	private boolean includePhone;
	private boolean includeAddr;
	private boolean includeCity;
	private boolean includeState;
	private boolean includeZip;
	private boolean includeProfileStatus;
	private boolean includeCreateDate;
	private boolean includeCreateIpAddrId;
	private boolean includePasswordHash;
	private boolean includePrmrCode;
	private String findProfiles;
	private String submitSearch;
	private String partnerSiteId;
	//	private ImageButtonBean submitSearch = new ImageButtonBean();

	// private ImageButtonBean submitSearch = new ImageButtonBean();

	private String programId;

	private String additionalIdType;
	private String additionalDocStatus;


	public String getFirstTime1() {
		return firstTime1;
	}

	public void setFirstTime1(String string)
	{
		firstTime1 = string;
	}

	public String getSearchFirstName()
	{
		return searchFirstName;
	}

	public void setSearchFirstName(String string)
	{
		searchFirstName = string;
	}

	public String getSearchLastName()
	{
		return searchLastName;
	}

	public void setSearchLastName(String string)
	{
		searchLastName = string;
	}

	public String getSearchLoginId()
	{
		return searchLoginId;
	}

	public void setSearchLoginId(String string)
	{
		searchLoginId = string;
	}

	public String getSearchPhone()
	{
		return searchPhone;
	}

	public void setSearchPhone(String string)
	{
		searchPhone = string;
	}

	public String getSearchSSN()
	{
		return searchSSN;
	}

	public void setSearchSSN(String string)
	{
		searchSSN = string;
	}

	public String getSearchCC4()
	{
		return searchCC4;
	}

	public void setSearchCC4(String string)
	{
		searchCC4 = string;
	}

	public String getSearchBankAcct()
	{
		return searchBankAcct;
	}

	public void setSearchBankAcct(String string)
	{
		searchBankAcct = string;
	}

	public String getSubmitSearch()
	{
		return submitSearch;
	}

	public void setSubmitSearch(String s)
	{
		submitSearch = s;
	}

	public String getSearchAddr()
	{
		return searchAddr;
	}

	public void setSearchAddr(String string)
	{
		searchAddr = string;
	}

	public String getSearchCity()
	{
		return searchCity;
	}

	public void setSearchCity(String string)
	{
		searchCity = string;
	}

	public String getSearchState()
	{
		return searchState;
	}

	public void setSearchState(String string)
	{
		searchState = string;
	}

	public String getSearchZip()
	{
		return searchZip;
	}

	public void setSearchZip(String string)
	{
		searchZip = string;
	}

	public String getSearchProfileStatus()
	{
		return searchProfileStatus;
	}

	public void setSearchProfileStatus(String string)
	{
		searchProfileStatus = string;
	}

	public String getSearchCreateDate()
	{
		return searchCreateDate;
	}

	public void setSearchCreateDate(String string)
	{
		searchCreateDate = string;
	}

	public String getSearchCreateIpAddrId()
	{
		return searchCreateIpAddrId;
	}

	public void setSearchCreateIpAddrId(String string)
	{
		searchCreateIpAddrId = string;
	}

	public String getSearchPasswordHash()
	{
		return searchPasswordHash;
	}

	public void setSearchPasswordHash(String string)
	{
		searchPasswordHash = string;
	}

	public boolean isRemovePasswordSearch()
	{
		return removePasswordSearch;
	}

	public void setRemovePasswordSearch(boolean b)
	{
		removePasswordSearch = b;
	}

	public boolean isIncludeFirstName()
	{
		return includeFirstName;
	}

	public void setIncludeFirstName(boolean b)
	{
		includeFirstName = b;
	}

	public boolean isIncludeLastName()
	{
		return includeLastName;
	}

	public void setIncludeLastName(boolean b)
	{
		includeLastName = b;
	}

	public boolean isIncludeLoginId()
	{
		return includeLoginId;
	}

	public void setIncludeLoginId(boolean b)
	{
		includeLoginId = b;
	}

	public boolean isIncludePhone()
	{
		return includePhone;
	}

	public void setIncludePhone(boolean b)
	{
		includePhone = b;
	}

	public boolean isIncludeAddr()
	{
		return includeAddr;
	}

	public void setIncludeAddr(boolean b)
	{
		includeAddr = b;
	}

	public boolean isIncludeCity()
	{
		return includeCity;
	}

	public void setIncludeCity(boolean b)
	{
		includeCity = b;
	}

	public boolean isIncludeState()
	{
		return includeState;
	}

	public void setIncludeState(boolean b)
	{
		includeState = b;
	}

	public boolean isIncludeZip()
	{
		return includeZip;
	}

	public void setIncludeZip(boolean b)
	{
		includeZip = b;
	}

	public boolean isIncludeProfileStatus()
	{
		return includeProfileStatus;
	}

	public void setIncludeProfileStatus(boolean b)
	{
		includeProfileStatus = b;
	}

	public boolean isIncludeCreateDate()
	{
		return includeCreateDate;
	}

	public void setIncludeCreateDate(boolean b)
	{
		includeCreateDate = b;
	}

	public boolean isIncludeCreateIpAddrId()
	{
		return includeCreateIpAddrId;
	}

	public void setIncludeCreateIpAddrId(boolean b)
	{
		includeCreateIpAddrId = b;
	}

	public boolean isIncludePasswordHash()
	{
		return includePasswordHash;
	}

	public void setIncludePasswordHash(boolean b)
	{
		includePasswordHash = b;
	}

	public String getFindProfiles()
	{
		return findProfiles;
	}

	public void setFindProfiles(String string)
	{
		findProfiles = string;
	}

	public String getSortBy()
	{
		return sortBy;
	}

	public void setSortBy(String string)
	{
		sortBy = string;
	}

	public String getSearchPrmrCode() {
		return searchPrmrCode;
	}

	public void setSearchPrmrCode(String searchPrmrCode) {
		this.searchPrmrCode = searchPrmrCode;
	}

	public boolean isIncludePrmrCode() {
		return includePrmrCode;
	}

	public void setIncludePrmrCode(boolean includePrmrCode) {
		this.includePrmrCode = includePrmrCode;
	}

	public String getProgramId() {
		return programId;
	}

	public void setProgramId(String programId) {
		this.programId = programId;
	}

	public String getAdditionalIdType() {
		return additionalIdType;
	}

	public void setAdditionalIdType(String additionalIdType) {
		this.additionalIdType = additionalIdType;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public String getSearchId() {
		return searchId;
	}

	public void setSearchId(String searchId) {
		this.searchId = searchId;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		searchId = "";
		searchFirstName = "";
		searchLastName = "";
		searchSecondLastName= "";
		searchMiddleName= "";
		searchLoginId = "";
		searchPhone = "";
		searchSSN = "";
		searchCC4 = "";
		searchBankAcct = "";
		searchAddr = "";
		searchCity = "";
		searchState = "";
		searchZip = "";
		searchProfileStatus = "";
		searchCreateDate = "";
		searchCreateDateTo = "";
		searchCreateIpAddrId = "";
		searchPasswordHash = "";
		searchPrmrCode = "";
		sortBy = "custId";
		removePasswordSearch = false;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();

		if (isRemovePasswordSearch())
		{
			searchPasswordHash = "";
		}

		if (!StringHelper.isNullOrEmpty(submitSearch))
		{
			errors = super.validate(mapping, request);

			if (StringHelper.isNullOrEmpty(searchFirstName)
				&& StringHelper.isNullOrEmpty(searchLastName)
				&& StringHelper.isNullOrEmpty(searchLoginId)
				&& StringHelper.isNullOrEmpty(searchPhone)
				&& StringHelper.isNullOrEmpty(searchSSN)
				&& StringHelper.isNullOrEmpty(searchCC4)
				&& StringHelper.isNullOrEmpty(searchBankAcct)
				&& StringHelper.isNullOrEmpty(searchAddr)
				&& StringHelper.isNullOrEmpty(searchCity)
				&& StringHelper.isNullOrEmpty(searchState)
				&& StringHelper.isNullOrEmpty(searchZip)
				&& StringHelper.isNullOrEmpty(searchProfileStatus)
				&& StringHelper.isNullOrEmpty(searchCreateDate)
				&& StringHelper.isNullOrEmpty(searchCreateDateTo)
				&& StringHelper.isNullOrEmpty(searchCreateIpAddrId)
				&& StringHelper.isNullOrEmpty(searchPasswordHash)
				&& StringHelper.isNullOrEmpty(searchPrmrCode)
				&& StringHelper.isNullOrEmpty(searchId)
				&& StringHelper.isNullOrEmpty(additionalIdType))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.must.specify.search.criteria"));
			}
			if (!StringHelper.isNullOrEmpty(searchPhone))
			{
				if (StringHelper.containsNonDigits(searchPhone))
				{
					errors.add(
						"searchPhone",
						new ActionError("errors.integer", "Phone Number"));
				} 
				else if (searchPhone.length() < 2)
				{
					String[] parm = { "Phone Number", "2" };
					errors.add(
						"searchPhone",
						new ActionError("errors.invalid.minlength.phone.number", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchCC4))
			{
				if (StringHelper.containsNonDigits(searchCC4))
				{
					errors.add(
						"searchCC4",
						new ActionError("errors.integer", "Credit Card Mask"));
				} else if (searchCC4.length() > 10)
				{
					String[] parm = { "Credit Card Mask", "10" };
					errors.add(
						"searchCC4",
						new ActionError("error.must.be.n.digits.long", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchSSN))
			{
				if (StringHelper.containsNonDigits(searchSSN))
				{
					errors.add(
						"searchSSN",
						new ActionError("errors.integer", "SSN Mask"));
				} else if (searchSSN.length() < 4)
				{
					String[] parm = { "SSN Mask", "4" };
					errors.add(
						"searchSSN",
						new ActionError("error.must.be.n.digits.long", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchBankAcct))
			{
				if (StringHelper.containsNonDigits(searchBankAcct))
				{
					errors.add(
						"searchBankAcct",
						new ActionError("errors.integer", "Bank Account Mask"));
				} else if (searchBankAcct.length() < 4)
				{
					String[] parm = { "Bank Account Mask", "4" };
					errors.add(
						"searchBankAcct",
						new ActionError("error.must.be.n.digits.long", parm));
				}
			}


			if (!StringHelper.isNullOrEmpty(searchCreateDate))
			{
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				try
				{
					df.parse(searchCreateDate);
				} catch (ParseDateException e)
				{
					errors.add(
						"searchCreateDate",
						new ActionError("errors.invalid", "Create Date from"));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchCreateDateTo))
			{
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				try
				{
					df.parse(searchCreateDateTo);
				} catch (ParseDateException e)
				{
					errors.add(
						"searchCreateDateTo",
						new ActionError("errors.invalid", "Create Date to"));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchCreateIpAddrId))
			{
				IPAddressValidator ipValidator =
					new IPAddressValidator(searchCreateIpAddrId);
				if (!ipValidator.formatIpString())
				{
					errors.add(
						"searchCreateIpAddrId",
						new ActionError(
							"errors.invalid",
							"IP Address search string"));
				} else
				{
					setSearchCreateIpAddrId(searchCreateIpAddrId);
				}
			}
		}

		return errors;
	}

	public String getSearchCreateDateTo() {
		return searchCreateDateTo;
	}

	public void setSearchCreateDateTo(String searchCreateDateTo) {
		this.searchCreateDateTo = searchCreateDateTo;
	}

	public String getAdditionalDocStatus() {
		return additionalDocStatus;
	}

	public void setAdditionalDocStatus(String additionalDocStatus) {
		this.additionalDocStatus = additionalDocStatus;
	}

	public String getSearchMiddleName() {
		return searchMiddleName;
	}

	public void setSearchMiddleName(String searchMiddleName) {
		this.searchMiddleName = searchMiddleName;
	}

	public String getSearchSecondLastName() {
		return searchSecondLastName;
	}

	public void setSearchSecondLastName(String searchSecondLastName) {
		this.searchSecondLastName = searchSecondLastName;
	}
}
