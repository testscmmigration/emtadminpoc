/*
 * Created on May 09, 2005
 *
 */
package emgadm.consumer;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.EmailStatus;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author T349
 *
 */
public class UpdateCustomerEmailAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private static final ServiceFactory sharedServiceFactory =
		ServiceFactory.getInstance();
	private static final ConsumerProfileService profileService =
		sharedServiceFactory.getConsumerProfileService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{

		ActionForward forward = mapping.findForward(FORWARD_FAILURE);
		UpdateCustomerEmailForm inputForm = (UpdateCustomerEmailForm) form;
		ConsumerProfile consumerProfile = null;
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		try
		{
			int custId = inputForm.getCustId();
			String userId = getUserProfile(request).getUID();

			consumerProfile =
				profileService.getConsumerProfile(
					new Integer(custId),
					userId,
					null);
			if (consumerProfile != null)
			{
				//TODO For now there is only one email.  Future will have multiple emails addresses.
				for (Iterator iter = consumerProfile.getEmails().iterator();
					iter.hasNext();
					)
				{
					ConsumerEmail consumerEmail = (ConsumerEmail) iter.next();
					// Call to change to Active Validated Email.
					consumerEmail.setStatus(EmailStatus.ACTIVE);
					profileService.updateEmailStatus(
						consumerProfile.getUserId(),
						consumerEmail);
				}
				// Add profile change comment
				ConsumerProfileComment comment = new ConsumerProfileComment();
				comment.setCustId(custId);
				comment.setReasonCode("OTH");
				comment.setText(buildCommentText(consumerProfile, inputForm));
				if (comment.getText().length() > 0)
				{
					profileService.updateConsumerProfile(consumerProfile,true,userId);
					try
					{
						profileService.addConsumerProfileComment(
							comment,
							userId);
					} catch (Exception e)
					{
						// Not a serious error, log and move on.
						EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).error(
							"Failed to insert comment: " + e);
					}
					errors.add(
						"emailStatus",
						new ActionMessage("msg.email.activated"));
				} else
				{
					errors.add(
						"emailStatus",
						new ActionMessage("msg.profile.not.changed"));
				}
				forward = mapping.findForward(FORWARD_SUCCESS);
				session.setAttribute("consumerProfile", null);
			} else
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionMessage("error.update.consumer.profile.failed"));
			}
		} catch (Exception e)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				"Update consumer profile failed",
				e);
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionMessage("error.update.consumer.profile.failed"));
		}

		if (errors != null && errors.size() != 0) {
			saveErrors(request, errors);
		}

		return forward;
	}

	private String buildCommentText(
		ConsumerProfile profile,
		UpdateCustomerEmailForm form)
	{
		StringBuffer strBuf = new StringBuffer();
		strBuf.append("Email Status was changed to Active");
		return strBuf.toString();
	}
}