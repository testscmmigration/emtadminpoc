/*
 * Created on April 07, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class UpdateCustomerEmailForm extends EMoneyGramAdmBaseValidatorForm
{
	private int custId;
	private String emailAddress;
	/**
	 * @return
	 */
	public int getCustId()
	{
		return custId;
	}

	/**
	 * @return
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}

	/**
	 * @param i
	 */
	public void setCustId(int i)
	{
		custId = i;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string)
	{
		emailAddress = string;
	}

}
