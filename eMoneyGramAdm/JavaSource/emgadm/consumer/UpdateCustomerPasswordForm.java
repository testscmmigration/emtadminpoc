/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class UpdateCustomerPasswordForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String newValue;

	public String getCustId()
	{
		return custId;
	}

	public String getNewValue()
	{
		return newValue;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setNewValue(String string)
	{
		newValue = string;
	}
}
