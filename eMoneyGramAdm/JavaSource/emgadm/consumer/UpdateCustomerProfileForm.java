/*
 * Created on April 07, 2005
 *
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

/**
 * @author A131
 * 
 */
public class UpdateCustomerProfileForm extends EMoneyGramAdmBaseValidatorForm {

	private int custId;
	private String custLastName;
	private String custSecondLastName;
	private String custFirstName;
	private String middleName;
	private String birthDateText;
	private String addressLine1;
	private String addressLine2;
	private String addressBuildingName;
	private String city;
	private String county;
	private String state;
	private String isoCountryCode;
	private String postalCode;
	private String postalMini1;
	private String postalMini2;
	private String zip4;
	private String phoneNumber;
	private String phoneNumberType;
	private String phoneNumberAlternate;
	private String phoneNumberAlternateType;
	private String savePhoneNumber;
	private String phoneNumberTaintText;
	private String savePhoneNumberAlternate;
	private String phoneNumberAlternateTaintText;
	private String loyaltyPgmMembershipId;
	private String updateSubmit;
	private String dvsValidate;
	private String updateCancel;
	private String partnerSiteId;
	private boolean allowOverride;
	private CustomerProfileValidator validator;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors actionErrors = new ActionErrors();

		if (!StringHelper.isNullOrEmpty(updateSubmit)) {
		}

		return actionErrors;
	}

	public boolean getAllowOverride() {
		return allowOverride;
	}

	public void setAllowOverride(boolean allowOverride) {
		this.allowOverride = allowOverride;
	}

	public String getBirthDateText() {
		return birthDateText;
	}

	public String getAddressLine1() {
		return addressLine1 == null ? "" : addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2 == null ? "" : addressLine2;
	}

	public String getCity() {
		return city == null ? "" : city;
	}

	public String getCustFirstName() {
		return custFirstName == null ? "" : custFirstName;
	}

	public String getCustLastName() {
		return custLastName == null ? "" : custLastName;
	}

	public String getPhoneNumber() {
		return phoneNumber == null ? "" : phoneNumber;
	}

	public String getPhoneNumberAlternate() {
		return phoneNumberAlternate == null ? "" : phoneNumberAlternate;
	}

	public String getPostalCode() {
		return postalCode == null ? "" : postalCode;
	}

	public String getState() {
		return state == null ? "" : state;
	}

	public String getIsoCountryCode() {
		return isoCountryCode == null ? "" : isoCountryCode;
	}

	public String getZip4() {
		return zip4 == null ? "" : zip4;
	}

	public void setBirthDateText(String birthDateText) {
		this.birthDateText = birthDateText != null ? birthDateText.trim() : "";
	}

	public void setAddressLine1(String string) {
		addressLine1 = string != null ? string.trim() : "";
	}

	public void setAddressLine2(String string) {
		addressLine2 = string != null ? string.trim() : "";
	}

	public void setCity(String string) {
		city = string != null ? string.trim() : "";
	}

	public void setCustFirstName(String string) {
		custFirstName = string != null ? string.trim() : "";
	}

	public void setCustLastName(String string) {
		custLastName = string != null ? string.trim() : "";
	}

	public void setPhoneNumber(String string) {
		phoneNumber = string != null ? string.trim() : "";
	}

	public void setPhoneNumberAlternate(String string) {
		phoneNumberAlternate = string != null ? string.trim() : "";
	}

	public void setPostalCode(String string) {
		postalCode = string != null ? string.trim() : "";
	}

	public void setState(String string) {
		state = string != null ? string.trim() : "";
	}

	public void setIsoCountryCode(String string) {
		isoCountryCode = string != null ? string.trim() : "";
	}

	public void setZip4(String string) {
		zip4 = string != null ? string.trim() : "";
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int i) {
		custId = i;
	}

	public String getUpdateSubmit() {
		return updateSubmit;
	}

	public void setUpdateSubmit(String string) {
		updateSubmit = string;
	}

	public String getDvsValidate() {
		return dvsValidate;
	}

	public void setDvsValidate(String string) {
		dvsValidate = string;
	}

	public String getUpdateCancel() {
		return updateCancel;
	}

	public void setUpdateCancel(String string) {
		updateCancel = string;
	}

	public String getSavePhoneNumber() {
		return savePhoneNumber;
	}

	public void setSavePhoneNumber(String string) {
		savePhoneNumber = string;
	}

	public String getPhoneNumberTaintText() {
		return phoneNumberTaintText;
	}

	public void setPhoneNumberTaintText(String string) {
		phoneNumberTaintText = string != null ? string.trim() : "";
	}

	public String getSavePhoneNumberAlternate() {
		return savePhoneNumberAlternate;
	}

	public void setSavePhoneNumberAlternate(String string) {
		savePhoneNumberAlternate = string;
	}

	public String getPhoneNumberAlternateTaintText() {
		return phoneNumberAlternateTaintText;
	}

	public void setPhoneNumberAlternateTaintText(String string) {
		phoneNumberAlternateTaintText = string;
	}

	public String getLoyaltyPgmMembershipId() {
		return loyaltyPgmMembershipId;
	}

	public void setLoyaltyPgmMembershipId(String loyaltyPgmMembershipId) {
		this.loyaltyPgmMembershipId = loyaltyPgmMembershipId != null ? loyaltyPgmMembershipId.trim() : "";
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName != null ? middleName.trim() : "";
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setPhoneNumberType(String phoneNumberType) {
		this.phoneNumberType = phoneNumberType != null ? phoneNumberType.trim() : "";
	}

	public String getPhoneNumberType() {
		return phoneNumberType;
	}

	public void setPhoneNumberAlternateType(String phoneNumberAlternateType) {
		this.phoneNumberAlternateType = phoneNumberAlternateType != null ? phoneNumberAlternateType.trim() : "";
	}

	public String getPhoneNumberAlternateType() {
		return phoneNumberAlternateType;
	}

	public void setCounty(String county) {
		this.county = county != null ? county.trim() : "";
	}

	public String getCounty() {
		return county;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId != null ? partnerSiteId.trim() : "";
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPostalMini1(String postalMini1) {
		this.postalMini1 = postalMini1 != null ? postalMini1.trim() : "";
	}

	public String getPostalMini1() {
		return postalMini1;
	}

	public void setPostalMini2(String postalMini2) {
		this.postalMini2 = postalMini2 != null ? postalMini2.trim() : "";
	}

	public String getPostalMini2() {
		return postalMini2;
	}

	public void setAddressBuildingName(String addressBuildingName) {
		this.addressBuildingName = addressBuildingName != null ? addressBuildingName.trim() : "";

	}

	public String getAddressBuildingName() {
		return addressBuildingName == null ? "" : this.addressBuildingName;
	}

	public String getCustSecondLastName() {
		return custSecondLastName;
	}

	public void setCustSecondLastName(String custSecondLastName) {
		this.custSecondLastName = custSecondLastName!= null ? custSecondLastName.trim() : "";
	}

}
