/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class UpdateCustomerStatusForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String custStatus;
	private String currentCustStatus;
	private boolean allowOverride;

	public boolean getAllowOverride() {
		return allowOverride;
	}

	public void setAllowOverride(boolean allowOverride) {
		this.allowOverride = allowOverride;
	}

	public String getCustId()
	{
		return custId;
	}

	public String getCustStatus()
	{
		return custStatus;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setCustStatus(String string)
	{
		custStatus = string;
	}
    /**
     * @return Returns the currentCustStatus.
     */
    public String getCurrentCustStatus() {
        return currentCustStatus;
    }
    /**
     * @param currentCustStatus The currentCustStatus to set.
     */
    public void setCurrentCustStatus(String ccs) {
        this.currentCustStatus = ccs;
    }
}
