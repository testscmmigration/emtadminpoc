package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A119
 *
 */
public class ClearMasterTaintForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String ssnTaintIndicator;

	public String getSsnTaintIndicator() {
		return ssnTaintIndicator;
	}

	public void setSsnTaintIndicator(String ssnTaintIndicator) {
		this.ssnTaintIndicator = ssnTaintIndicator;
	}
	
	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}
}
