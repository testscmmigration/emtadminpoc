package emgadm.consumer;

import emgadm.consumer.BaseCustomerProfileValidator.UntaintedPhoneNumber;
import emgadm.model.UserProfile;


public class UKCustomerProfileValidator extends BaseCustomerProfileValidator {

	public UKCustomerProfileValidator(UpdateCustomerProfileForm form, UserProfile userProfile) {
		super(form, userProfile);
		fields.add(new AlphaField(form.getMiddleName(), "Middle Name").setMaxLength(20).isRequired(false));
		fields.add(new AlphaField(form.getCustSecondLastName(), "Second Last Name").setMaxLength(30).isRequired(false));
		fields.add(new AlphaNumericField(form.getPostalMini1(), "First part of Postcode").setMaxLength(4));
		fields.add(new AlphaNumericField(form.getPostalMini2(), "Second part of Postcode").setMaxLength(8));
		fields.add(new AlphaField(form.getCounty(), "County").setMaxLength(40).isRequired(false));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), "Phone Number").setMinLength(7).setMaxLength(14).setUID(userProfile.getUID()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(), "Phone Number Alternate").isRequired(false).setMinLength(7).setMaxLength(14).setUID(userProfile.getUID()));
	}

}
