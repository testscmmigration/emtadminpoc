package emgadm.consumer;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.CustPremierType;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

public class UpdateCustPremierCodeAction extends EMoneyGramAdmBaseAction {
    private static final String FORWARD_SUCCESS = "success";

    private static final String FORWARD_FAILURE = "failure";

//    private static final ConsumerProfileService profileService = ServiceFactory
//            .getInstance().getConsumerProfileService();

    private List custPrmrCodes = null;

    public ActionForward execute(ActionMapping mapping, ActionForm f,
            HttpServletRequest request, HttpServletResponse response)
            throws NumberFormatException, DataSourceException, DuplicateActiveProfileException {
        ActionForward forward;
        ActionErrors errors = new ActionErrors();
        UpdateCustPremierCodeForm form = (UpdateCustPremierCodeForm) f;
        custPrmrCodes = (List) request.getSession().getServletContext()
                .getAttribute("custPrmrCodes");

        String userId = getUserProfile(request).getUID();
        int custId = Integer.parseInt(form.getCustId());
        ConsumerProfileService profileService = ServiceFactory.getInstance()
                .getConsumerProfileService();
        ConsumerProfile cp = profileService.getConsumerProfile(new Integer(
                custId), userId, "wwwemg");

        if (cp.getCustPrmrCode().equalsIgnoreCase(form.getPrmrCode())) {
            errors
                    .add("prmrCode", new ActionMessage(
                            "error.premier.no.change"));
            forward = mapping.findForward(FORWARD_FAILURE);
        } else {
            String oldCode = cp.getCustPrmrCode();
            cp.setCustPrmrCode(form.getPrmrCode());
            profileService.updateConsumerProfile(cp,true,userId);

            //Add status change comment
            ConsumerProfileComment comment = new ConsumerProfileComment();
            comment.setCustId(custId);
            comment.setReasonCode("OTH");
            comment.setText("Premier code changed from '"
                    + getPrmrDesc(oldCode) + "' to '"
                    + getPrmrDesc(form.getPrmrCode()) + "'");
            profileService.addConsumerProfileComment(comment, userId);

            try {
                StringBuffer detailText = new StringBuffer(512);
                detailText.append("<FROM_PREMIERE_CODE>" + oldCode + "</FROM_PREMIERE_CODE>");
                detailText.append("<TO_PREMIERE_CODE>" + form.getPrmrCode() + "</TO_PREMIERE_CODE>");
                super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILEPREMIERECODECHG,String.valueOf(custId));
			} catch (Exception ignore) { }

            //Add premier code change message next to "Update Premier Code"
            // button
            errors.add("prmrCode",
                    new ActionMessage("msg.premier.code.changed"));

            forward = mapping.findForward(FORWARD_SUCCESS);
        }

        if (errors != null || errors.size() != 0) {
        	saveErrors(request, errors);
        }

        return forward;
    }

    private String getPrmrDesc(String code) {
        String desc = "Unknown";
        Iterator iter = custPrmrCodes.iterator();
        while (iter.hasNext()) {
            CustPremierType cpt = (CustPremierType) iter.next();
            if (cpt.getCustPrmrCode().equalsIgnoreCase(code)) {
                desc = cpt.getCustPrmrDesc();
                break;
            }
        }

        return desc;
    }
}
