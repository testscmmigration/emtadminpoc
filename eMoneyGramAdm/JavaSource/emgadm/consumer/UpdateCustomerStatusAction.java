/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.SameStatusException;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.ConsumerStatus;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class UpdateCustomerStatusAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	private static final ConsumerProfileService profileService =
		ServiceFactory.getInstance().getConsumerProfileService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UpdateCustomerStatusForm inputForm = (UpdateCustomerStatusForm) form;
		if (inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_0.toString()) 
			|| inputForm.getCustStatus().equals(ConsumerStatus.NON_ACTIVE_VERID_FAIL.toString())
			|| inputForm.getCustStatus().equals(ConsumerStatus.PENDING_INITIAL.toString())
			|| inputForm.getCustStatus().equals(ConsumerStatus.ACTIVE_VALIDATION_LEVEL_6.toString()))
		{
			Map m = profileService.getConsumerStatusDescriptions();
			ConsumerStatus status =	ConsumerStatus.getInstanceByCombinedCode(inputForm.getCustStatus());
			errors.add("custStatus",new ActionMessage("error.status.not.allowed", m.get(status).toString()));
			forward = mapping.findForward(FORWARD_FAILURE);
		} else
		{
			ConsumerStatus newStatus = ConsumerStatus.getInstanceByCombinedCode(inputForm.getCustStatus());
			ConsumerStatus currStatus =	ConsumerStatus.getInstanceByCombinedCode(inputForm.getCurrentCustStatus());
			ConsumerProfileService profileService =	ServiceFactory.getInstance().getConsumerProfileService();
			String userId = getUserProfile(request).getUID();
			int custId = Integer.parseInt(inputForm.getCustId());
			try
			{
				if (inputForm.getAllowOverride())
				{
					profileService.updateStatus(custId, newStatus, userId,true);
				}
				else
					profileService.updateStatus(custId, newStatus, userId,false);
				//Look up status description
				Map Descriptions = profileService.getConsumerStatusDescriptions();
				String StatusDesc = (String) Descriptions.get(newStatus);
				String origStatusDesc = (String) Descriptions.get(currStatus);
				
				//Add status change comment
				ConsumerProfileComment comment = new ConsumerProfileComment();
				comment.setCustId(custId);
				comment.setReasonCode("OTH");
				comment.setText("Status Changed  from '" + origStatusDesc 
						+ "' to '" + StatusDesc + "'");
				if (inputForm.getAllowOverride())
					comment.setText(comment.getText() + " - Duplicate Profile Override applied.");
				profileService.addConsumerProfileComment(comment, userId);
	            try {
	                StringBuffer detailText = new StringBuffer(512);
	                detailText.append("<FROM_STATUS>" + inputForm.getCurrentCustStatus() + "</FROM_STATUS>");
	                detailText.append("<TO_STATUS>" + newStatus + "</TO_STATUS>");
	                super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILESTATUSCHG,String.valueOf(custId));    
				} catch (Exception ignore) { }
				request.setAttribute("duplicateOverride", "N");
				//Add status change message next to "Update Status" button
				errors.add("custStatus", new ActionMessage("msg.status.changed"));
				forward = mapping.findForward(FORWARD_SUCCESS);
			} catch (SameStatusException e)
			{
				errors.add("headerMessage",	new ActionMessage("error.status.no.change"));
				forward = mapping.findForward(FORWARD_FAILURE);
			} catch (DuplicateActiveProfileException e)
			{
				request.setAttribute("duplicateOverride", "Y");
				errors.add("custStatus", new ActionMessage("errors.duplicate.profile"));
				forward = mapping.findForward(FORWARD_FAILURE);
			}
		}

		if (errors.size() > 0)
		{
			saveErrors(request, errors);
		}

		return forward;
	}
}
