package emgadm.consumer;

import emgadm.model.UserProfile;

/**
 * Validator used for Germany (DEU, the ISO 3166-1 alpha-3 code )
 * @author vz70
 *
 */
public class DEUCustomerProfileValidator extends BaseCustomerProfileValidator {

	public DEUCustomerProfileValidator(UpdateCustomerProfileForm form, 
			UserProfile userProfile) {
		fields.add(new DEUAlphaField(form.getCustFirstName(), "First Name").setMaxLength(30));
		fields.add(new DEUAlphaField(form.getCustLastName(), "Last Name").setMaxLength(40));
		fields.add(new DEUAlphaField(form.getMiddleName(), "Middle Name").setMaxLength(20).isRequired(false));
		fields.add(new DEUAlphaField(form.getCustSecondLastName(), "Second Last Name").setMaxLength(30).isRequired(false));
		fields.add(new DEUAlphaField(form.getCity(), "Town/City").setMaxLength(40));
		fields.add(new StrictNumericField(form.getPostalCode(), "Postcode").setMinLength(5).setMaxLength(5));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumber(), "Phone Number").setMinLength(2).setMaxLength(20).setUID(userProfile.getUID()));
		fields.add(new UntaintedPhoneNumber(form.getPhoneNumberAlternate(), "Phone Number Alternate").setMinLength(2).setMaxLength(20).isRequired(false).setUID(userProfile.getUID()));
		fields.add(new BirthDateField(form.getBirthDateText(), "Birth Date"));
		
		// Note: Not all addresses have street names, Baltrum for example does not have any.
		fields.add(new DEUAddressField(form.getAddressLine1(), "Street").setMaxLength(80));
		
		// The are addresses that have a street name but no house number.
		fields.add(new AddressBuildingField(form.getAddressBuildingName(),
				"House Number").setMaxLength(30).isRequired(false));
	}
	
	private class DEUAlphaField extends AlphaField {
		DEUAlphaField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z �������]";
			keyStringMessage = "errors.non.alpha";
		}
	}
	
	private class DEUAddressField extends AlphaField {
		DEUAddressField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z -�������]";
			keyStringMessage = "errors.non.alpha";
		}
	}
	
	private class AddressBuildingField extends AlphaNumericField {
		AddressBuildingField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^0-9a-zA-Z -.]";
			keyStringMessage = "errors.non.alpha";
		}
	}

}
