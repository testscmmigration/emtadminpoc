package emgadm.consumer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;

import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

public abstract class BaseCustomerProfileValidator implements CustomerProfileValidator {

	protected List<Field> fields = new ArrayList<Field>();
	protected UserProfile userProfile;
	
	protected BaseCustomerProfileValidator() {}

	protected BaseCustomerProfileValidator(UpdateCustomerProfileForm form, UserProfile userProfile) { 
		fields.add(new AlphaField(form.getCustFirstName(), "First Name").setMaxLength(30)); 
		fields.add(new AlphaField(form.getCustLastName(), "Last Name").setMaxLength(40));
		fields.add(new AlphaField(form.getMiddleName(), "Middle Name").setMaxLength(20).isRequired(false)); 
		fields.add(new AlphaField(form.getCustSecondLastName(), "Second Last Name").setMaxLength(30).isRequired(false)); 
		fields.add(new AlphaField(form.getCity(), "Town/City").setMaxLength(40));
		fields.add(new BirthDateField(form.getBirthDateText(), "Birth Date"));
		fields.add(new AlphaNumericField(form.getAddressLine1(), "Address Line 1").setMaxLength(80));
		fields.add(new AlphaNumericField(form.getAddressLine2(), "Address Line 2").setMaxLength(60).isRequired(false));
	}

	public ActionErrors validate(ActionErrors errors) {
		for (Field field : fields) {
			errors = field.validate(errors);
		}
		return errors;
	}

	abstract class Field {
		String value;
		String friendlyName;
		boolean required;
		boolean valid;
		Field(String value, String friendlyName) {
			this.value = value;
			this.friendlyName = friendlyName;
			required = true;
			valid = true;
		}
		Field isRequired(boolean required) {
			this.required = required;
			return this;
		}
		boolean isValid() {
			return value != null && value.length() > 0;
		}
		public String toString() {
			return friendlyName;
		}
		ActionErrors validate(ActionErrors errors) {
			if (!(valid = isValid())) {
				if (required) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", friendlyName));
				}
			}
			return errors;
		}
	}

	abstract class StringField extends Field {
		int minLength;
		int maxLength;
		String pattern;
		String keyStringMessage;
		StringField(String value, String friendlyName) {
			super(value, friendlyName);
			this.minLength = 1;
			this.maxLength = Integer.MAX_VALUE;
		}
		StringField setMinLength(int length) {
			this.minLength = length;
			return this;
		}
		StringField setMaxLength(int length) {
			this.maxLength = length;
			return this;
		}
		boolean isValidExpression() {
			Pattern regex = Pattern.compile(pattern);
			Matcher matcher = regex.matcher(value);
			return !matcher.find();
		}
		boolean hasCorrectLength() {
			return value.length() >= minLength && value.length() <= maxLength;
		}
		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = isValidExpression())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(keyStringMessage, friendlyName));
			}
			if (valid && !(valid = hasCorrectLength())) {
				if (minLength != maxLength) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.range.length", friendlyName, minLength, maxLength));
				} else {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.exact.length", friendlyName, minLength));
				}
			}
			return errors;
		}
	}

	class AlphaNumericField extends StringField {
		AlphaNumericField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z0-9 ]";
			keyStringMessage = "errors.non.alphadigit";
		}
		AlphaNumericField setMinLength(int length) {
			return (AlphaNumericField)super.setMinLength(length);
		}
		AlphaNumericField setMaxLength(int length) {
			return (AlphaNumericField)super.setMaxLength(length);
		}
		AlphaNumericField isRequired(boolean required) {
			return (AlphaNumericField)super.isRequired(required);
		}
	}

	class AlphaField extends StringField {
		AlphaField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^a-zA-Z ]";
			keyStringMessage = "errors.non.alpha";
		}
		AlphaField setMinLength(int length) {
			return (AlphaField)super.setMinLength(length);
		}
		AlphaField setMaxLength(int length) {
			return (AlphaField)super.setMaxLength(length);
		}
		AlphaField isRequired(boolean required) {
			return (AlphaField)super.isRequired(required);
		}
	}

	class StrictNumericField extends StringField {
		StrictNumericField(String value, String friendlyName) {
			super(value, friendlyName);
			pattern = "[^0-9]";
			keyStringMessage = "errors.non.digit";
		}
		StrictNumericField setMinLength(int length) {
			return (StrictNumericField)super.setMinLength(length);
		}
		StrictNumericField setMaxLength(int length) {
			return (StrictNumericField)super.setMaxLength(length);
		}
		StrictNumericField isRequired(boolean required) {
			return (StrictNumericField)super.isRequired(required);
		}
	}

	class DateField extends Field {
		Date date;
		String expectedFormat;
		DateFormat sf;
		Calendar now;
		DateField(String value, String friendlyName) {
			super(value, friendlyName);
			expectedFormat = "dd/MMM/yyyy";
			sf = new SimpleDateFormat(expectedFormat);
		}
		DateField setExpectedFormat(String format) {
			expectedFormat = format;
			return this;
		}
		boolean hasValidFormat() {
			try {
				date = sf.parse(value);
				return true;
			} catch(ParseException e) {
				return false;
			}
		}
		boolean isValidDate() {
			String formattedDate = sf.format(date);
			return formattedDate.equals(value) || (now = Calendar.getInstance()).getTime().compareTo(date) < 0;
		}
		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = hasValidFormat())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.date.format", friendlyName, expectedFormat));
			}
			if (valid && !(valid = isValidDate())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.date.invalid", value));
			}
			return errors;
		}
	}

	class BirthDateField extends DateField {
		int minimumValidAge;
		int maximumValidAge;
		BirthDateField(String value, String name) {
			super(value, name);
			minimumValidAge = 18;
			maximumValidAge = 108;
		}
		BirthDateField setMinimumAge(int age) {
			minimumValidAge = age;
			return this;
		}
		BirthDateField setMaximumAge(int age) {
			maximumValidAge = age;
			return this;
		}
		BirthDateField setExpectedFormat(String format) {
			return (BirthDateField)super.setExpectedFormat(format);
		}
		boolean hasValidAge() {
			int age = getAge();
			return age >= minimumValidAge && age <= maximumValidAge;
		}
		protected int getAge() {
			Calendar dob = Calendar.getInstance();
			dob.setTime(date);
			int year1 = now.get(Calendar.YEAR);
			int year2 = dob.get(Calendar.YEAR);
			int age = year1 - year2;
			int month1 = now.get(Calendar.MONTH);
			int month2 = dob.get(Calendar.MONTH);
			if (month2 > month1) {
			  age--;
			} else if (month1 == month2) {
			  int day1 = now.get(Calendar.DAY_OF_MONTH);
			  int day2 = dob.get(Calendar.DAY_OF_MONTH);
			  if (day2 > day1) {
			    age--;
			  }
			}
			return age;
		}
		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = hasValidFormat())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.date.age", minimumValidAge, maximumValidAge));
			}
			return errors;
		}
	}
	class UntaintedPhoneNumber extends StrictNumericField {
		String uid;
		UntaintedPhoneNumber(String value, String friendlyName) {
			super(value, friendlyName);
			uid = null;
		}
		UntaintedPhoneNumber setMinLength(int length) {
			return (UntaintedPhoneNumber)super.setMinLength(length);
		}
		UntaintedPhoneNumber setMaxLength(int length) {
			return (UntaintedPhoneNumber)super.setMaxLength(length);
		}
		UntaintedPhoneNumber isRequired(boolean required) {
			return (UntaintedPhoneNumber)super.isRequired(required);
		}
		UntaintedPhoneNumber setUID(String uid) {
			this.uid = uid;
			return this;
		}
		boolean isNotTainted() {
			try {
				boolean tainted = checkTainted();
				return !tainted;
			} catch (DataSourceException e) {
				// Something went wrong, assume is not tainted
				return true;
			}
		}
		boolean checkTainted() throws DataSourceException {
			ServiceFactory sf = ServiceFactory.getInstance();
			FraudService fs = sf.getFraudService();
			return fs.checkBlockedPhone(uid, value);
		}
		ActionErrors validate(ActionErrors errors) {
			errors = super.validate(errors);
			if (valid && !(valid = isNotTainted())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.phone.is.blocked", value));
			}
			return errors;
		}
	}
}
