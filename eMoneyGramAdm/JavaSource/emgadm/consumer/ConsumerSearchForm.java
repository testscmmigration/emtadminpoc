package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class ConsumerSearchForm extends EMoneyGramAdmBaseValidatorForm
{
	private String firstTime;
	private String searchFirstName;
	private String searchMiddleName;
	private String searchLastName;
	private String searchLoginId;
	private String searchSSN;
	private String searchCreateDate;
	private String sortBy;
	private String submitSearch;

	public String getFirstTime()
	{
		return firstTime;
	}

	public void setFirstTime(String string)
	{
		firstTime = string;
	}

	public String getSearchFirstName()
	{
		return searchFirstName;
	}

	public void setSearchFirstName(String string)
	{
		searchFirstName = string;
	}

	public String getSearchLastName()
	{
		return searchLastName;
	}

	public void setSearchLastName(String string)
	{
		searchLastName = string;
	}

	public String getSearchLoginId()
	{
		return searchLoginId;
	}

	public void setSearchLoginId(String string)
	{
		searchLoginId = string;
	}

	public String getSearchSSN()
	{
		return searchSSN;
	}

	public void setSearchSSN(String string)
	{
		searchSSN = string;
	}

	public String getSearchCreateDate()
	{
		return searchCreateDate;
	}

	public void setSearchCreateDate(String string)
	{
		searchCreateDate = string;
	}

	public String getSortBy()
	{
		return sortBy;
	}

	public void setSortBy(String string)
	{
		sortBy = string;
	}

	public String getSubmitSearch()
	{
		return submitSearch;
	}

	public void setSubmitSearch(String s)
	{
		submitSearch = s;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		searchFirstName = "";
		searchMiddleName = "";
		searchLastName = "";
		searchLoginId = "";
		searchSSN = "";
		searchCreateDate = "";
		sortBy = "custId";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();

		if (!StringHelper.isNullOrEmpty(submitSearch))
		{
			errors = super.validate(mapping, request);

			if (StringHelper.isNullOrEmpty(searchFirstName)
				&& StringHelper.isNullOrEmpty(searchLastName)
				&& StringHelper.isNullOrEmpty(searchLoginId)
				&& StringHelper.isNullOrEmpty(searchSSN)
				&& StringHelper.isNullOrEmpty(searchCreateDate))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.must.specify.search.criteria"));
			}

			if (!StringHelper.isNullOrEmpty(searchSSN))
			{
				if (StringHelper.containsNonDigits(searchSSN))
				{
					errors.add(
						"searchSSN",
						new ActionError("errors.integer", "SSN Mask"));
				} else if (searchSSN.length() < 4)
				{
					String[] parm = { "SSN Mask", "4" };
					errors.add(
						"searchSSN",
						new ActionError("error.must.be.n.digits.long", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(searchCreateDate))
			{
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				try
				{
					df.parse(searchCreateDate);
				} catch (ParseDateException e)
				{
					errors.add(
						"searchCreateDate",
						new ActionError("errors.invalid", "Create Date"));
				}
			}
		}

		return errors;
	}

	public String getSearchMiddleName() {
		return searchMiddleName;
	}

	public void setSearchMiddleName(String searchMiddleName) {
		this.searchMiddleName = searchMiddleName;
	}
}
