/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class AddCustomerCommentForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String commentReasonCode;
	private String commentText;

	public String getCustId()
	{
		return custId;
	}

	public String getCommentReasonCode()
	{
		return commentReasonCode;
	}

	public String getCommentText()
	{
		return commentText;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public void setCommentReasonCode(String string)
	{
		commentReasonCode = string;
	}

	public void setCommentText(String string)
	{
		commentText = string;
	}
}
