package emgadm.consumer;

import emgadm.model.UserProfile;
import emgshared.util.Constants;

public class CustomerProfileValidatorFactory {

	public static CustomerProfileValidator create(String parentSiteId, UpdateCustomerProfileForm form, UserProfile userProfile) {
		if (Constants.MGO_PARTNER_SITE_ID.equals(parentSiteId)
				|| Constants.WAP_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new USCustomerProfileValidator(form, userProfile);
		} else if (Constants.MGOUK_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new UKCustomerProfileValidator(form, userProfile);
		} else if (Constants.MGODE_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new DEUCustomerProfileValidator(form, userProfile);
		} else {
			throw new IllegalArgumentException(parentSiteId + " is not a valid parent site id");
		}
	}

}
