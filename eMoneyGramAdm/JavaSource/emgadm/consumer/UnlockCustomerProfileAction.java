package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import com.moneygram.security.ldap.LdapService;
import com.moneygram.security.ldap.LdapServiceImpl;
import com.moneygram.security.ldap.LdapUser;
import com.moneygram.security.ldap.exceptions.LdapGeneralException;
import com.moneygram.security.ldap.exceptions.LdapUserNotFoundException;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.model.Activity;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.Activity;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A119
 *
 */
public class UnlockCustomerProfileAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException, LdapGeneralException
	{
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		UnlockCustomerProfileForm inputForm =(UnlockCustomerProfileForm) form;
		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(inputForm.getCustId());
		ConsumerProfileService profileService =
			ServiceFactory.getInstance().getConsumerProfileService();
		ConsumerProfile cp = (ConsumerProfile) request.getSession().getAttribute("findProfiles");
		
		LdapService ldapService = new LdapServiceImpl();
		LdapUser ldapUser = ldapService.findUser(cp.getUserId());
		if (ldapUser != null){
			if (ldapUser.getGuid() != null && cp.getEdirGuid() != null && ldapUser.getGuid().equals(cp.getEdirGuid())){
				try {
					ldapService.unlockUserProfile(cp.getUserId());
				} catch (LdapUserNotFoundException e){
					errors.add("headerMessage", new ActionMessage("error.edir.user.not.found", cp.getUserId()));
				} catch (LdapGeneralException e){
					errors.add("headerMessage", new ActionMessage("error.edir.ldap.general"));
				}
			}
			else {
				errors.add("headerMessage", new ActionMessage("error.edir.guid.not.matched", ldapUser.getGuid(), cp.getEdirGuid()));
			}
		}
		else {
			errors.add("headerMessage", new ActionMessage("error.edir.user.not.found", cp.getUserId()));
		}
		
		if (!errors.isEmpty()){
			saveErrors(request, errors);
			forward = mapping.findForward(FORWARD_FAILURE);
		}
		else {
			try {
				// add unlock log
				ConsumerProfileActivity cpa = new ConsumerProfileActivity();
				cpa.setCustId(cp.getId());
				cpa.setActivityLogCode(ConsumerProfileActivity.PROFILE_UNLOCKED_CODE);
				profileService.insertConsumerActivityLog(cpa, userId);
                super.insertActivityLog(this.getClass(),request,"Profile Unlocked: " + cp.getUserId(),UserActivityType.EVENT_TYPE_CONSUMERPROFILECHGCCSTATUS,inputForm.getCustId());
			} catch (Exception ignore) { }

			//Add status change message next to "Reset Password" button				
			errors.add(	"headerMessage",new ActionMessage("msg.profile.unlocked"));
			saveErrors(request, errors);
			forward = mapping.findForward(FORWARD_SUCCESS);
		}
		return forward;
	}
}
