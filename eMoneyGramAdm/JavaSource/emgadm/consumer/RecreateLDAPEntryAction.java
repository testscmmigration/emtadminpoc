/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.consumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.moneygram.security.ldap.LdapService;
import com.moneygram.security.ldap.LdapServiceImpl;
import com.moneygram.security.ldap.LdapUser;
import com.moneygram.security.ldap.exceptions.LdapBadPasswordException;
import com.moneygram.security.ldap.exceptions.LdapGeneralException;
import com.moneygram.security.ldap.exceptions.LdapUserExistException;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileActivity;
import emgshared.model.ConsumerProfileComment;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A119
 *
 */
public class RecreateLDAPEntryAction extends EMoneyGramAdmBaseAction {
	private static final String FORWARD_SUCCESS = "success";

	private static final String FORWARD_FAILURE = "failure";

	private ConsumerProfileService profileService = ServiceFactory.getInstance()
			.getConsumerProfileService();

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws NumberFormatException,
			DataSourceException, MaxRowsHashCryptoException {
		ActionForward forward;
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		String userId = getUserProfile(request).getUID();
		RecreateLDAPEntryForm inputForm = (RecreateLDAPEntryForm) form;
		String custId = inputForm.getCustId();
		long beginTime = System.currentTimeMillis();
		int consumerId = Integer.parseInt(custId);
		ConsumerProfile consumerProfile = null;
		forward = mapping.findForward(FORWARD_SUCCESS);
		String pwd = "";
		String eDirGuidOld = null;
		try {
			consumerProfile = profileService.getConsumerProfile(new Integer(consumerId), userId,null);
			eDirGuidOld = consumerProfile.getEdirGuid();
		} catch (Exception e) {
			forward = mapping.findForward(FORWARD_FAILURE);
		}

		LdapService ldapService = new LdapServiceImpl();
		try {
			LdapUser ldapUser = new LdapUser(consumerProfile.getUserId(), consumerProfile.getLastName(), consumerProfile.getFirstName());
			ConsumerEmail consumerEmail = null;
			for (Iterator iter = consumerProfile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				ldapUser.setEmail(consumerEmail.getConsumerEmail());
				break;
			}
			ArrayList<String> accessList = new ArrayList();
			accessList.add("MGO");
			ldapUser.setUserAccess(accessList);
			pwd = emgadm.util.StringHelper.genRandomPassword(8);
			ldapUser.setPassword(pwd);
			ldapUser.setGroupMemberships(new HashMap());
			ldapService.addUser(ldapUser);
			ldapUser = ldapService.findUser(ldapUser.getId());
			pwd = emgadm.util.StringHelper.genRandomPassword(8);
			// have to chg pwd now to force user pwd chg upon login
			ldapService.updatePassword(ldapUser.getUserId(),pwd);
			consumerProfile.setEdirGuid(ldapUser.getGuid());
			profileService.updateConsumerProfile(consumerProfile, true, userId);
			ConsumerProfileActivity cpa = new ConsumerProfileActivity();
			cpa.setCustId(consumerProfile.getId());
			cpa.setActivityLogCode(ConsumerProfileActivity.PROFILE_RECREATE_LDAP_ENTRY_CODE);
			profileService.insertConsumerActivityLog(cpa, userId);
			ConsumerProfileComment comment = new ConsumerProfileComment();
			comment.setCustId(consumerProfile.getId());
			comment.setText("Edir GUID modified old value: " + eDirGuidOld );
			comment.setReasonCode("OTH");
			profileService.addConsumerProfileComment(comment, userId);
		} catch (LdapUserExistException uee) {
			errors.add("headerMessage", new ActionMessage("error.edir.user.already.exists",
					consumerProfile.getUserId()));
		} catch (LdapBadPasswordException pe) {
			errors.add("headerMessage", new ActionMessage("error.edir.bad.password",
					consumerProfile.getUserId()));
		} catch (LdapGeneralException ge) {
			errors.add("headerMessage", new ActionMessage("error.edir.ldap.general",
					consumerProfile.getUserId()));
		} catch (Exception ex) {
				errors.add("headerMessage", new ActionMessage("error.edir.ldap.general", consumerProfile.getUserId()));
		}

		if (errors.size() > 0) {
			saveErrors(request, errors);
		}
		else{
			errors.add("headerMessage", new ActionMessage("message.ldap.entry.recreated", consumerProfile.getUserId(), pwd));
			saveErrors(request, errors);
		}
		return forward;
	}
}
