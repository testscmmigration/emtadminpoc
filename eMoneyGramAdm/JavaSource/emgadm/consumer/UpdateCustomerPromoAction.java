/*
 * Created on Dec 23, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.consumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;

/**
 * @author A135
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UpdateCustomerPromoAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";

//	private static final ConsumerProfileService profileService =
//		ServiceFactory.getInstance().getConsumerProfileService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		UpdateCustomerPromoForm inputForm = (UpdateCustomerPromoForm) form;
		boolean acceptPromoEmails =	!inputForm.isAcceptPromotionalEmail();
		ConsumerProfileService profileService =
			ServiceFactory.getInstance().getConsumerProfileService();
		String userId = getUserProfile(request).getUID();
		int custId = Integer.parseInt(inputForm.getCustId());

		//FIXME: Hardcode for MGO right now
		profileService.updateUserPromo(custId, acceptPromoEmails, userId, "MGO"); 

		//Add status change comment
		ConsumerProfileComment comment = new ConsumerProfileComment();
		comment.setCustId(custId);
		comment.setReasonCode("OTH");
		comment.setText("Accept Promotional Emails Changed to " + acceptPromoEmails);
		profileService.addConsumerProfileComment(comment, userId);
        try {
            StringBuffer detailText = new StringBuffer(100);
            detailText.append("<ACCEPT_PROMO>" + acceptPromoEmails + "</ACCEPT_PROMO>");
            super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILEPROMOEMAILCHG,String.valueOf(custId));    
		} catch (Exception ignore) { }

		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}
}
