package emgadm.consumer;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.model.PartnerSite;
import emgadm.sysmon.EventListConstant;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ServiceFactory;
import eventmon.eventmonitor.EventMonitor;

public class ConsumerSearchAction extends EMoneyGramAdmBaseAction {
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	public ActionForward execute(ActionMapping mapping, ActionForm f, HttpServletRequest request, HttpServletResponse response)
			throws AgentConnectException, DataSourceException {
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		ConsumerSearchForm form = (ConsumerSearchForm) f;
		// UserProfile up = getUserProfile(request);
		if (StringHelper.isNullOrEmpty(form.getFirstTime())) {
			form.setSubmitSearch(null);
			form.setFirstTime("N");
			request.getSession().removeAttribute("profileList");
		}
		Collection consumerProfileList = null;
		ConsumerProfileService profileService = ServiceFactory.getInstance().getConsumerProfileService();
		if (!StringHelper.isNullOrEmpty(form.getSubmitSearch())) {
			long beginTime = System.currentTimeMillis();
			ConsumerProfileSearchCriteria criteria = new ConsumerProfileSearchCriteria();
			if (!StringHelper.isNullOrEmpty(form.getSearchLoginId())) {
				criteria.setCustLogonId(form.getSearchLoginId());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchFirstName())) {
				criteria.setCustFrstName(form.getSearchFirstName());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchLastName())) {
				criteria.setCustLastName(form.getSearchLastName());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchSSN())) {
				criteria.setCustSSNMask(form.getSearchSSN());
			}
			if (!StringHelper.isNullOrEmpty(form.getSearchCreateDate())) {
				criteria.setCustCreateDate(form.getSearchCreateDate());
			}
			try {
				criteria.setSortBy(form.getSortBy());
				Collection<PartnerSite> partSites = ManagerFactory.createTransactionManager().getPartnerSiteIds("B");
				HashMap<String, String> partnerSites = new HashMap<String, String>();
				for (Iterator iterator = partSites.iterator(); iterator.hasNext();) {
					PartnerSite partnerSite = (PartnerSite) iterator.next();
					partnerSites.put(partnerSite.getPartnetSideIdCode(), partnerSite.getPartnerSiteId());
				}
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Start Search : " + new Date());				
				consumerProfileList = profileService.searchConsumerProfile(criteria, false, getUserProfile(request).getUID(), partnerSites);
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("End Search : " + new Date());
			
			} catch (SQLException sqle) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("exception.unexpected"));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
			} catch (DataSourceException e) {
				e.printStackTrace();
			} catch (TooManyResultException e) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.too.many.results", dynProps.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			} finally {
				EventMonitor.getInstance().recordEvent(EventListConstant.CONSUMER_SEARCH, beginTime, System.currentTimeMillis());
			}
			if ((consumerProfileList != null) && (consumerProfileList.size() != 0)) {
				request.getSession().setAttribute("profileList", consumerProfileList);
				ActionMessage myMsg;
				int maxCount = dynProps.getMaxDownloadTransactions();
				if (consumerProfileList.size() > maxCount) {
					myMsg = new ActionMessage("message.max.results.exceeded", String.valueOf(maxCount));
				} else {
					myMsg = new ActionMessage("message.number.consumers.found", String.valueOf(consumerProfileList.size()));
				}
				messages.add("nbrFound", myMsg);
				saveMessages(request, messages);
			} else {
				request.getSession().removeAttribute("profileList");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.consumer.profile.search.notfound"));
				saveErrors(request, errors);
			}
		}
		if (request.getSession().getAttribute("custStatusOptions") == null) {
			request.getSession().setAttribute("custStatusOptions",
					convertMapToLabelValueListSorted(profileService.getConsumerStatusDescriptions()));
		}
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
