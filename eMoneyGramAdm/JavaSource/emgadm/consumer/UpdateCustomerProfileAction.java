/*
 * Created on April 08, 2005
 *
 */
package emgadm.consumer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.MessageResources;
import org.jfree.util.Log;

import com.moneygram.dvs.OffendingElement;
import com.moneygram.mgo.service.consumer_v2.Consumer;
import com.moneygram.mgo.service.consumer_v2.PersonalInfo;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileTask;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.UserProfile;
import emgadm.services.ConsumerServiceProxy;
import emgadm.services.ConsumerServiceProxyImpl;
import emgadm.services.DVSService;
import emgadm.services.DVSServiceImpl;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.DuplicateActiveProfileException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileComment;
import emgshared.model.TaintIndicatorType;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerProfileService;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.PhoneNumberValidator;
import emgshared.util.StringHelper;

/**
 * @author T349
 *
 */
public class UpdateCustomerProfileAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_FAILURE = "failure";
	private static final ServiceFactory sharedServiceFactory = ServiceFactory.getInstance();
	private static final ConsumerProfileService profileService = sharedServiceFactory.getConsumerProfileService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{

		ActionForward forward = mapping.findForward(FORWARD_FAILURE);
		UpdateCustomerProfileForm inputForm = (UpdateCustomerProfileForm) form;
		String id = (String) request.getAttribute("custId");
		ConsumerProfile consumerProfile = null;
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		int custId = 0;
		String userId = getUserProfile(request).getUID();
		ArrayList commentList = new ArrayList();
		try
		{
			custId = inputForm.getCustId();
			if (inputForm.getUpdateCancel() != null) {
				forward = mapping.findForward(FORWARD_SUCCESS);
				return forward;
			} // cancel
			/*
			 *  validate profile against DVS service, no updates.
			 */

			if (inputForm.getDvsValidate() != null) {
			//if (true) {
				initializeInputForm(consumerProfile, inputForm);
				errors=validateProfileDVS(consumerProfile,request);
				if (errors.isEmpty())
					errors.add(	ActionErrors.GLOBAL_ERROR, new ActionError("msg.dvs.success"));
				saveErrors(request, errors);
				return forward;
			} // dvs validate

			consumerProfile = profileService.getConsumerProfile(new Integer(custId),userId,	null);

			if (inputForm.getUpdateSubmit() != null)
			{
				UserProfile up = getUserProfile(request);
		        if (up.hasPermission("duplicateProfileOverride")) {
		        	request.setAttribute("duplicateProfileOverride", "Y");
		        }

			    String parentSiteId = consumerProfile.getPartnerSiteId();
			    CustomerProfileValidator validator = CustomerProfileValidatorFactory.create(parentSiteId, inputForm, getUserProfile(request));
			    errors = validator.validate(errors);

				request.setAttribute("allowDuplicate", "N");
				if (errors.isEmpty()) {
					if (consumerProfile != null) {
						// Add profile change comment
						commentList = buildCommentText(request, consumerProfile, inputForm);
						if (!commentList.isEmpty())
						{
							
							// modified for MGO-9024
							UpdateConsumerProfileRequest updateConsumerProfileRequest = new UpdateConsumerProfileRequest();

							PersonalInfo personalInfo = new PersonalInfo();
							personalInfo.setFirstName(inputForm
									.getCustFirstName());
							personalInfo.setMiddleName(inputForm
									.getMiddleName());
							personalInfo.setLastName(inputForm
									.getCustLastName());

							Consumer consumer = new Consumer();
							consumer.setSourceSite(inputForm.getPartnerSiteId());
							consumer.setPersonal(personalInfo);

							consumer.setConsumerId(new Long(custId));
							updateConsumerProfileRequest.setConsumer(consumer);

							UpdateConsumerProfileTask[] updateConsumerProfileTaskArray = { UpdateConsumerProfileTask
									.fromValue("UpdateName") };

							updateConsumerProfileRequest
									.setUpdateTasks(updateConsumerProfileTaskArray);
							ConsumerServiceProxy consumerServiceProxy = ConsumerServiceProxyImpl
									.getInstance();
							consumerServiceProxy.updateConsumerProfile(
									updateConsumerProfileRequest, parentSiteId);
							//-----------
							
							String name = inputForm.getCustFirstName();
							consumerProfile.setFirstName(name);
							consumerProfile.setLastName(inputForm.getCustLastName());
							consumerProfile.setMiddleName(inputForm.getMiddleName());
							consumerProfile.setSecondLastName(inputForm.getCustSecondLastName());
							DateFormatter formatter = new DateFormatter("dd/MMM/yyyy", true);
							Date date = formatter.parse(inputForm.getBirthDateText());
							consumerProfile.setBirthdate(date);
							consumerProfile.setAddressLine1(inputForm.getAddressLine1());
							consumerProfile.setAddressLine2(inputForm.getAddressLine2());
							consumerProfile.setBuildingName(inputForm.getAddressBuildingName());
							consumerProfile.setLoyaltyPgmMembershipId(inputForm.getLoyaltyPgmMembershipId());
							consumerProfile.setCity(inputForm.getCity());
							consumerProfile.setState(inputForm.getState());
							// EOC should not be able to update country
							if (consumerProfile.getPartnerSiteId().equals(Constants.MGOUK_PARTNER_SITE_ID)) {
								consumerProfile.setPostalCode(inputForm.getPostalMini1() + "-" + inputForm.getPostalMini2());
							} else {
								consumerProfile.setPostalCode(inputForm.getPostalCode());
							}
							consumerProfile.setZip4(inputForm.getZip4());
							consumerProfile.setPhoneNumber(StringHelper.removeNonDigits(inputForm.getPhoneNumber()));
							consumerProfile.setPhoneNumberType(inputForm.getPhoneNumberType());
							String countyName = inputForm.getCounty();
							consumerProfile.setCountyName(countyName);
							consumerProfile.getAddress().setCountyName(countyName);

							if (TaintIndicatorType.BLOCKED_TEXT.equals(inputForm.getPhoneNumberTaintText())) {
								consumerProfile.setPhoneTaintCode(TaintIndicatorType.BLOCKED);
							} else if (TaintIndicatorType.NOT_BLOCKED_TEXT.equals(inputForm.getPhoneNumberTaintText())) {
								consumerProfile.setPhoneTaintCode(TaintIndicatorType.NOT_BLOCKED);
							} else if (TaintIndicatorType.OVERRIDE_TEXT.equals(inputForm.getPhoneNumberTaintText())) {
								consumerProfile.setPhoneTaintCode(TaintIndicatorType.OVERRIDE);
							} else {
								consumerProfile.setPhoneTaintCode(null);
							}

							String alternatePhoneNumber = StringHelper.removeNonDigits(inputForm.getPhoneNumberAlternate());
							if (!StringHelper.isNullOrEmpty(alternatePhoneNumber)) {
								consumerProfile.setPhoneNumberAlternate(alternatePhoneNumber);
								consumerProfile.setPhoneNumberAlternateType(inputForm.getPhoneNumberAlternateType());
							} else {
								consumerProfile.setPhoneNumberAlternate(null);
								consumerProfile.setPhoneNumberAlternateType(null);
							}

							if (!StringHelper.isNullOrEmpty(inputForm.getPhoneNumberAlternate())) {
								if (TaintIndicatorType.BLOCKED_TEXT.equals(inputForm.getPhoneNumberAlternateTaintText())) {
									consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.BLOCKED);
								} else if (TaintIndicatorType.NOT_BLOCKED_TEXT.equals(inputForm.getPhoneNumberAlternateTaintText())) {
									consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.NOT_BLOCKED);
								} else if (TaintIndicatorType.OVERRIDE_TEXT.equals(inputForm.getPhoneNumberAlternateTaintText())) {
									consumerProfile.setPhoneAlternateTaintCode(TaintIndicatorType.OVERRIDE);
								} else {
									consumerProfile.setPhoneAlternateTaintCode(null);
								}
							} else {
								consumerProfile.setPhoneAlternateTaintCode(null);
							}
							if (Constants.MGO_PARTNER_SITE_ID.equals(inputForm.getPartnerSiteId())) {
								errors= validateProfileDVS(consumerProfile, request);
							}
							if (errors.isEmpty()) {
								if (inputForm.getAllowOverride()) {
									profileService.updateConsumerProfile(consumerProfile,true,userId);
									commentList.add("Duplicate Override - Profile Edit");
								} else {
									profileService.updateConsumerProfile(consumerProfile,false,userId);
								}
								insertComments(commentList, custId,  userId);
								errors.add("headerMessage",	new ActionMessage("msg.profile.changed"));
								forward = mapping.findForward(FORWARD_SUCCESS);
							}
						} else	{
							errors.add("headerMessage",	new ActionMessage("msg.profile.not.changed"));
							forward = mapping.findForward(FORWARD_SUCCESS);
						}
						session.setAttribute("consumerProfile", null);
					} else 	{
						errors.add(	ActionErrors.GLOBAL_ERROR, new ActionError("error.update.consumer.profile.failed"));
					}
				}
			} else {
				if (consumerProfile != null) {
					inputForm = initializeInputForm(consumerProfile,inputForm);
				}
				if (inputForm!=null) {
					session.setAttribute("consumerProfile", consumerProfile);
					request.setAttribute("updateCustomerProfileForm", inputForm);
					request.setAttribute("partnerSiteId", inputForm.getPartnerSiteId());
				} else {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.update.consumer.profile.failed"));
				}
			}
		} catch (DuplicateActiveProfileException dup) {
			request.setAttribute("commentList", commentList);
			request.setAttribute("allowDuplicate", "Y");
			EMGSharedLogger.getLogger( this.getClass().getName().toString()).error("Update consumer profile failed", dup);
			errors.add("duplicateProfileOverrideErr", new ActionError("errors.duplicate.profile.update"));
			consumerProfile = (ConsumerProfile) session.getAttribute("consumerProfile");
			inputForm = initializeInputForm(consumerProfile,inputForm);
		}
		  catch (Exception e)
		{
			EMGSharedLogger.getLogger( this.getClass().getName().toString()).error("Update consumer profile failed", e);
			errors.add(	ActionErrors.GLOBAL_ERROR,	new ActionError("error.update.consumer.profile.failed"));
		}
		if (!errors.isEmpty())
		{
			saveErrors(request, errors);
		}
		return forward;
	}

	private UpdateCustomerProfileForm initializeInputForm(ConsumerProfile consumerProfile,UpdateCustomerProfileForm inputForm)
	{
		try {
			inputForm = new UpdateCustomerProfileForm();
			inputForm.setCustId(consumerProfile.getId());
			inputForm.setCustFirstName(consumerProfile.getFirstName());
			inputForm.setCustLastName(consumerProfile.getLastName());
			inputForm.setMiddleName(consumerProfile.getMiddleName());
			inputForm.setCustSecondLastName(consumerProfile.getSecondLastName());
			DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
			inputForm.setBirthDateText(df.format(consumerProfile.getBirthdate()));
			inputForm.setAddressLine1(consumerProfile.getAddressLine1());
			inputForm.setAddressLine2(consumerProfile.getAddressLine2());
			inputForm.setAddressBuildingName(consumerProfile.getBuildingName());
			inputForm.setCity(consumerProfile.getCity());
			inputForm.setState(consumerProfile.getState());
			inputForm.setIsoCountryCode(consumerProfile.getIsoCountryCode());
			inputForm.setCounty(consumerProfile.getCountyName());
			inputForm.setPostalCode(consumerProfile.getPostalCode());
			String postalCode = consumerProfile.getPostalCode() + consumerProfile.getZip4();
			int dashPosition = postalCode.indexOf('-');
			if (dashPosition != -1) {
				inputForm.setPostalMini1(postalCode.substring(0, dashPosition));
				inputForm.setPostalMini2(postalCode.substring(dashPosition + 1));
			} else {
				inputForm.setPostalMini1(postalCode.substring(0, postalCode.length() / 2));
				inputForm.setPostalMini2(postalCode.substring(postalCode.length() / 2));
			}
			inputForm.setZip4(consumerProfile.getZip4());
			inputForm.setPhoneNumber(consumerProfile.getPhoneNumber());
			inputForm.setLoyaltyPgmMembershipId(consumerProfile.getLoyaltyPgmMembershipId());
			inputForm.setSavePhoneNumber(consumerProfile.getPhoneNumber());
			if (!StringHelper.isNullOrEmpty(consumerProfile.getPhoneNumber()))
			{
				inputForm.setPhoneNumberType(consumerProfile.getPhoneNumberType());
				if (consumerProfile.isPhoneBlocked())
				{
					inputForm.setPhoneNumberTaintText(TaintIndicatorType.BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneNotBlocked())
				{
					inputForm.setPhoneNumberTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
				} else if (consumerProfile.isPhoneOverridden())
				{
					inputForm.setPhoneNumberTaintText(TaintIndicatorType.OVERRIDE_TEXT);
				} else
				{
					inputForm.setPhoneNumberTaintText("Unknown");
				}
			}

			inputForm.setPhoneNumberAlternate(consumerProfile.getPhoneNumberAlternate());
			inputForm.setPhoneNumberAlternateType(consumerProfile.getPhoneNumberAlternateType());

			inputForm.setSavePhoneNumberAlternate(consumerProfile.getPhoneNumberAlternate());
			if (!StringHelper.isNullOrEmpty(consumerProfile.getPhoneNumberAlternate()))
			{
				if (consumerProfile.isPhoneAlternateBlocked())
				{
					inputForm.setPhoneNumberAlternateTaintText(TaintIndicatorType.BLOCKED_TEXT);
				} else if (
					consumerProfile.isPhoneAlternateNotBlocked())
				{
					inputForm.setPhoneNumberAlternateTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
				} else if (
					consumerProfile.isPhoneAlternateOverridden())
				{
					inputForm.setPhoneNumberAlternateTaintText(TaintIndicatorType.OVERRIDE_TEXT);
				} else
				{
					inputForm.setPhoneNumberAlternateTaintText(	"Unknown");
				}
			}
			inputForm.setPartnerSiteId(consumerProfile.getPartnerSiteId());
		} catch (Exception e) {
			return null;
		}
		return inputForm;
	}


	private void insertComments(ArrayList commentList, int custId, String userId)
	{
		try {
			ConsumerProfileComment comment = new ConsumerProfileComment();
			for (Iterator iterator = commentList.iterator(); iterator.hasNext();) {
				String commentText = (String) iterator.next();
				comment.setText(commentText);
				comment.setCustId(custId);
				comment.setReasonCode("OTH");
				if (!commentText.equalsIgnoreCase("ADDRESS"))
				profileService.addConsumerProfileComment(comment, userId);
			}
		} catch (Exception ignore) {

		}
	}

	private StringBuffer addCommentText(StringBuffer strBuf, String comment)
	{
		if (strBuf != null && strBuf.length() > 0)
		{
			strBuf.append(", ");
		}
		return strBuf.append(comment);
	}
	private ArrayList buildCommentText(HttpServletRequest request,	ConsumerProfile profile,UpdateCustomerProfileForm form)
	{
		StringBuffer strBuf = new StringBuffer();
		StringBuffer detailComment = new StringBuffer(255);
        StringBuffer detailText = new StringBuffer(100);
        ArrayList commentList = new ArrayList();
        boolean delimiterFlag = false;

        int custId = form.getCustId();
		ConsumerProfileComment comment = new ConsumerProfileComment();
		comment.setCustId(custId);
		comment.setReasonCode("OTH");

		if (profile.getFirstName() != null
			&& profile.getLastName() != null
			&& (!profile.getFirstName().equals(form.getCustFirstName().trim())
				|| !profile.getLastName().equals(form.getCustLastName().trim())))
		{
			strBuf.append(
				"Name Changed from "
					+ profile.getFirstName() + " " + profile.getLastName()
					+ " to "
					+ form.getCustFirstName() + " " + form.getCustLastName());

			detailComment.append(
					"Name Changed from '"
						+ profile.getFirstName() + " " + profile.getLastName()
						+ "' to '"
						+ form.getCustFirstName() + " " + form.getCustLastName() + "'");

            detailText.append(
	            	"<FROM_FIRSTNAME>" + profile.getFirstName() + "</FROM_FIRSTNAME>"
	            		+ "<TO_FIRSTNAME>" + form.getCustFirstName() + "</TO_FIRSTNAME>"
	            		+ "<FROM_LASTNAME>" + profile.getLastName() + "</FROM_LASTNAME>"
	            		+ "<TO_LASTNAME>" + form.getCustLastName() + "</TO_LASTNAME>");
		}

		if (detailComment.length()>0)
			commentList.add(detailComment.toString());
		detailComment = new StringBuffer(255);

		if (profile.getMiddleName() == null)
		{
			if (form.getMiddleName() != null
				&& !form.getMiddleName().equals(""))
			{
                delimiterFlag = true;
                addCommentText(strBuf, "Middle Name");
                detailComment.append("Added Middle Name - '"
                            + form.getMiddleName() + "'");
                detailText.append("<FROM_MIDDLENAME>" + "" + "</FROM_MIDDLENAME>"
                        + "<TO_MIDDLENAME>" + form.getMiddleName() + "</TO_MIDDLENAME>");
			}
		} else
		{
			if (!profile.getMiddleName().equals(form.getMiddleName().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Middle Name");
				detailComment.append("Middle Name Changed from '"
							+ profile.getMiddleName() + "' to '" + form.getMiddleName() + "'");
	            detailText.append("<FROM_MIDDLENAME>" + profile.getMiddleName() + "</FROM_MIDDLENAME>"
		            		+ "<TO_MIDDLENAME>" + form.getMiddleName() + "</TO_MIDDLENAME>");
			}
		}

		if (detailComment.length()>0)
			commentList.add(detailComment.toString());
		detailComment = new StringBuffer(255);

		if (profile.getSecondLastName()== null)
		{
			if (form.getCustSecondLastName() != null
				&& !form.getCustSecondLastName().equals(""))
			{
                delimiterFlag = true;
                addCommentText(strBuf, "Second Last Name");
                detailComment.append("Added Second Last Name - '"
                            + form.getCustSecondLastName() + "'");
                detailText.append("<FROM_SECONDLASTNAME>" + "" + "</FROM_SECONDLASTNAME>"
                        + "<TO_SECONDLASTNAME>" + form.getCustSecondLastName() + "</TO_SECONDLASTNAME>");
			}
		} else
		{
			if (!profile.getSecondLastName().equals(form.getCustSecondLastName().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Second Last Name");
				detailComment.append("Second Last Name Changed from '"
							+ profile.getSecondLastName() + "' to '" + form.getCustSecondLastName() + "'");
	            detailText.append("<FROM_SECONDLASTNAME>" + profile.getSecondLastName() + "</FROM_SECONDLASTNAME>"
		            		+ "<TO_SECONDLASTNAME>" + form.getCustSecondLastName() + "</TO_SECONDLASTNAME>");
			}
		}
		
		
		if (detailComment.length()>0)
			commentList.add(detailComment.toString());
		detailComment = new StringBuffer(255);

		if (profile.getAddressLine1() == null)
		{
			if (form.getAddressLine1() != null
				&& !form.getAddressLine1().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Address");
				detailComment.append("Address Line1 Changed from '"
							+ "" + "' to '" + form.getAddressLine1() + "'");
	            detailText.append("<FROM_ADDRESS>" + "" + "</FROM_ADDRESS>"
		            		+ "<TO_ADDRESS>" + form.getAddressLine1() + "</TO_ADDRESS>");
			}
		} else
		{
			if (!profile.getAddressLine1().equals(form.getAddressLine1().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Address");
				detailComment.append("Address Line1 Changed from '"
							+ profile.getAddressLine1() + "' to '" + form.getAddressLine1() + "'");
	            detailText.append("<FROM_ADDRESS>" + profile.getAddressLine1() + "</FROM_ADDRESS>"
		            		+ "<TO_ADDRESS>" + form.getAddressLine1() + "</TO_ADDRESS>");
			}
		}

		if (delimiterFlag == true) {detailComment.append(", "); delimiterFlag = false;}

		if (profile.getAddressLine2() == null)
		{
			if (form.getAddressLine2() != null
				&& !form.getAddressLine2().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Address 2");
				detailComment.append("Address Line2 Changed from '"
							+ "" + "' to '" + form.getAddressLine2() + "'");
	            detailText.append("<FROM_ADDRESS2>" + "" + "</FROM_ADDRESS2>"
		            		+ "<TO_ADDRESS2>" + form.getAddressLine2() + "</TO_ADDRESS2>");
			}
		} else
		{
			if (!profile.getAddressLine2().equals(form.getAddressLine2().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Address 2");
				detailComment.append("Address Line2 Changed from '"
							+ profile.getAddressLine2() + "' to '" + form.getAddressLine2() + "'");
	            detailText.append("<FROM_ADDRESS2>" + profile.getAddressLine2() + "</FROM_ADDRESS2>"
		            		+ "<TO_ADDRESS2>" + form.getAddressLine2() + "</TO_ADDRESS2>");
			}
		}
		if (detailComment.length()>0)
			//commentList.add(detailComment.toString());
			commentList.add("ADDRESS");
		detailComment = new StringBuffer(255);
		
		if (profile.getBuildingName() == null)
		{
			if (form.getAddressBuildingName() != null
				&& !form.getAddressBuildingName().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "House Number");
				detailComment.append("House Number Changed from '"
							+ "" + "' to '" + form.getAddressBuildingName() + "'");
	            detailText.append("<FROM_HOUSE_NUMBER>" + "" + "</FROM_HOUSE_NUMBER>"
		            		+ "<TO_HOUSE_NUMBER>" + form.getAddressBuildingName() + "</TO_HOUSE_NUMBER>");
			}
		} else
		{
			if (!profile.getBuildingName().equals(form.getAddressBuildingName().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "House Number");
				detailComment.append("House Number Changed from '"
							+ profile.getBuildingName() + "' to '" + form.getAddressBuildingName() + "'");
	            detailText.append("<FROM_HOUSE_NUMBER>" + profile.getBuildingName() + "</FROM_HOUSE_NUMBER>"
		            		+ "<TO_HOUSE_NUMBER>" + form.getAddressBuildingName() + "</TO_HOUSE_NUMBER>");
			}
		}
		if (detailComment.length()>0)
			//commentList.add(detailComment.toString());
			commentList.add("House Number");
		detailComment = new StringBuffer(255);

		if (profile.getCity() == null)
		{
			if (form.getCity() != null && !form.getCity().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "City");
				detailComment.append("City Changed from '" + "" + "' to '" + form.getCity() + "'");
	            detailText.append("<FROM_CITY>" + "" + "</FROM_CITY><TO_CITY>" + form.getCity() + "</TO_CITY>");
			}
		} else
		{
			if (!profile.getCity().equals(form.getCity().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "City");
				detailComment.append("City Changed from '"
							+ profile.getCity() + "' to '" + form.getCity() + "'");
	            detailText.append("<FROM_CITY>" + profile.getCity() + "</FROM_CITY>"
		            		+ "<TO_CITY>" + form.getCity() + "</TO_CITY>");
			}
		}
		if (delimiterFlag == true) {detailComment.append(", "); delimiterFlag = false;}
		if (profile.getState() == null)
		{
			if (form.getState() != null && !form.getState().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "State");
				detailComment.append("State Changed from '"
							+ "" + "' to '" + form.getState() + "'");
	            detailText.append("<FROM_STATE>" + "" + "</FROM_STATE>"
		            		+ "<TO_STATE>" + form.getState() + "</TO_STATE>");
			}
		} else
		{
			if (!profile.getState().equals(form.getState().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "State");
				detailComment.append("State Changed from '"
							+ profile.getState() + "' to '" + form.getState() + "'");
	            detailText.append("<FROM_STATE>" + profile.getState() + "</FROM_STATE>"
		            		+ "<TO_STATE>" + form.getState() + "</TO_STATE>");
			}
		}

		if (delimiterFlag == true) {detailComment.append(", "); delimiterFlag = false;}

		if (profile.getPostalCode() == null)
		{
			if (form.getPostalCode() != null && !form.getPostalCode().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Zip");
				detailComment.append("Zip Changed from '"
							+ "" + "' to '" + form.getPostalCode() + "'");
	            detailText.append("<FROM_ZIP>" + "" + "</FROM_ZIP>"
		            		+ "<TO_ZIP>" + form.getPostalCode() + "</TO_ZIP>");
			}
		} else
		{
			if (!profile.getPostalCode().equals(form.getPostalCode().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Zip");
				detailComment.append("Zip Changed from '"
							+ profile.getPostalCode() + "' to '" + form.getPostalCode() + "'");
	            detailText.append("<FROM_ZIP>" + profile.getPostalCode() + "</FROM_ZIP>"
		            		+ "<TO_ZIP>" + form.getPostalCode() + "</TO_ZIP>");
			}
		}
		if (delimiterFlag == true) {detailComment.append(", "); delimiterFlag = false;}
		if (profile.getZip4() == null)
		{
			if (form.getZip4() != null && !form.getZip4().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Zip4");
				detailComment.append("Zip4 Changed from '"
							+ "" + "' to '" + form.getZip4() + "'");
	            detailText.append("<FROM_ZIP4>" + "" + "</FROM_ZIP4>"
		            		+ "<TO_ZIP4>" + form.getZip4() + "</TO_ZIP4>");
			}
		} else
		{
			if (!profile.getZip4().equals(form.getZip4().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Zip4");
				detailComment.append("Zip4 Changed from '" + profile.getZip4() + "' to '" + form.getZip4() + "'");
	            detailText.append("<FROM_ZIP4>" + profile.getZip4() + "</FROM_ZIP4>"
		            		+ "<TO_ZIP4>" + form.getZip4() + "</TO_ZIP4>");
			}
		}
		if (detailComment.length()>0)
			commentList.add("ADDRESS");
		detailComment = new StringBuffer(255);
		delimiterFlag = false;
		try {
			DateFormatter formatter = new DateFormatter("dd/MMM/yyyy", true);
			String profDateText = formatter.format(profile.getBirthdate());
			if (!profDateText.equals(form.getBirthDateText()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "BirthDate ");
				detailComment.append("BirthDate Changed from '" + profDateText +  "' to '" + form.getBirthDateText() + "'");
	            detailText.append("<FROM_BIRTHDATE>" + profDateText + "</FROM_BIRTHDATE>"
		            		+ "<TO_BIRTHDATE>" + form.getBirthDateText() + "</TO_BIRTHDATE>");

			}
		} catch (Exception ignore) {}

		if (profile.getPhoneNumber() == null)
		{
			if (form.getPhoneNumber() != null
				&& !form.getPhoneNumber().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Phone");
				detailComment.append("Phone Changed from '"
							+ "" + "' to '" + form.getPhoneNumber() + "'");
	            detailText.append("<FROM_PHONE>" + "" + "</FROM_PHONE>"
		            		+ "<TO_PHONE>" + form.getPhoneNumber() + "</TO_PHONE>");
			}
		} else
		{
			if (!profile.getPhoneNumber().equals(form.getPhoneNumber().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Phone");
				detailComment.append("Phone Changed from '"
							+ profile.getPhoneNumber() + "' to '" + form.getPhoneNumber() + "'");
	            detailText.append("<FROM_PHONE>" + profile.getPhoneNumber() + "</FROM_PHONE>"
		            		+ "<TO_PHONE>" + form.getPhoneNumber() + "</TO_PHONE>");
			}
		}

		if (delimiterFlag == true) {detailComment.append(", "); delimiterFlag = false;}

		if (profile.getPhoneNumberAlternate() == null)
		{
			if (form.getPhoneNumberAlternate() != null
				&& !form.getPhoneNumberAlternate().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Alternate Phone");
				detailComment.append("Alternate Phone Changed from '"
							+ "" + "' to '" + form.getPhoneNumberAlternate() + "'");
	            detailText.append("<FROM_ALT_PHONE>" + "" + "</FROM_ALT_PHONE>"
		            		+ "<TO_ALT_PHONE>" + form.getPhoneNumberAlternate() + "</TO_ALT_PHONE>");
			}
		} else
		{
			if (!profile
				.getPhoneNumberAlternate()
				.equals(form.getPhoneNumberAlternate().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Alternate Phone");
				detailComment.append("Alternate Phone Changed from '"
							+ profile.getPhoneNumberAlternate() + "' to '" + form.getPhoneNumberAlternate() + "'");
	            detailText.append(
		            	"<FROM_ALT_PHONE>" + profile.getPhoneNumberAlternate() + "</FROM_ALT_PHONE>"
		            		+ "<TO_ALT_PHONE>" + form.getPhoneNumberAlternate() + "</TO_ALT_PHONE>");
			}
		}


		if (profile.getLoyaltyPgmMembershipId() == null)
		{
			if (form.getLoyaltyPgmMembershipId() != null
				&& !form.getLoyaltyPgmMembershipId().equals(""))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Plus #");
				detailComment.append("Plus # Changed from '"	+ "" + "' to '" + form.getLoyaltyPgmMembershipId() + "'");
				detailText.append("<FROM_REWARDS_NUM>" + "" + "</FROM_REWARDS_NUM>"
		            		+ "<TO_REWARDS_NUM>" + form.getLoyaltyPgmMembershipId() + "</TO_REWARDS_NUM>");
			}
		} else
		{
			if (!profile.getLoyaltyPgmMembershipId().equals(form.getLoyaltyPgmMembershipId().trim()))
			{
				delimiterFlag = true;
				addCommentText(strBuf, "Plus #");
				detailComment.append("Plus # Changed from '" 	+ profile.getLoyaltyPgmMembershipId() + "' to '" + form.getLoyaltyPgmMembershipId() + "'");
	            detailText.append("<FROM_REWARDS_NUM>" + profile.getLoyaltyPgmMembershipId() + "</FROM_REWARDS_NUM>"
		            		+ "<TO_REWARDS_NUM>" + form.getLoyaltyPgmMembershipId() + "</TO_REWARDS_NUM>");
			}
		}

		if (detailComment.length()>0)
			commentList.add(detailComment.toString());
		detailComment = new StringBuffer(255);

        try {
        	super.insertActivityLog(this.getClass(), request, detailText.toString(), UserActivityType.EVENT_TYPE_CONSUMERPROFILEUPDATE, String.valueOf(custId));
		} catch (Exception ignore) { }

		return commentList;

	}

	protected ActionErrors validateProfileInfo(
		UpdateCustomerProfileForm form,
		HttpServletRequest request)
		throws DataSourceException, TooManyResultException
	{
		ActionErrors errors = new ActionErrors();
		MessageResources messageResources = getResources(request);

		if (StringUtils.isBlank(form.getCustFirstName()))
		{
			String fieldName = messageResources.getMessage("label.first.name");
			errors.add("firstName",	new ActionMessage("errors.required", fieldName));
		} else // '/'not allowed in fisrt name
			if (StringUtils.indexOf(form.getCustFirstName(), "/") > -1)
			{
				String fieldName = messageResources.getMessage("label.first.name");
				errors.add("firstName",new ActionMessage("errors.noslashes", fieldName));
			}

		if (StringUtils.isBlank(form.getCustLastName()))
		{
			String fieldName = messageResources.getMessage("label.last.name");
			errors.add("lastName",new ActionMessage("errors.required", fieldName));
		} else // '/'not allowed in last name
			if (StringUtils.indexOf(form.getCustLastName(), "/") > -1)
			{
				String fieldName = messageResources.getMessage("label.last.name");
				errors.add("lastName",new ActionMessage("errors.noslashes", fieldName));
			}

		if (StringUtils.isBlank(form.getAddressLine1()))
		{
			String fieldName =messageResources.getMessage("label.street.address");
			errors.add("addressLine1",new ActionMessage("errors.required", fieldName));
		}

		if (StringUtils.isBlank(form.getCity()))
		{
			String fieldName = messageResources.getMessage("label.city");
			errors.add("city", new ActionMessage("errors.required", fieldName));
		}

		if (StringUtils.isBlank(form.getState()))
		{
			String fieldName = messageResources.getMessage("label.state");
			errors.add(
				"state",
				new ActionMessage("errors.required", fieldName));
		}

		if (!StringUtils.isBlank(form.getLoyaltyPgmMembershipId()))
		{
			if (StringHelper.containsAlpha(form.getLoyaltyPgmMembershipId()))
			{
				String fieldName = messageResources.getMessage("label.loyaltyId");
				errors.add("loyaltyId",	new ActionMessage("errors.invalid.loyaltyid", fieldName));
			}
			else {
				String nonDigitString = StringHelper.removeNonDigits(form.getLoyaltyPgmMembershipId());
				if (!(nonDigitString.length()>8)&&(nonDigitString.length()<13)){
					String fieldName = messageResources.getMessage("label.loyaltyId");
					errors.add("loyaltyId",	new ActionMessage("errors.invalid.loyaltyid", fieldName));
				}
				else
					form.setLoyaltyPgmMembershipId(nonDigitString);
			}
		}


		if (StringUtils.isBlank(form.getPostalCode()))
		{
			String fieldName = messageResources.getMessage("label.zip.code");
			errors.add("postalCode",new ActionMessage("errors.required", fieldName));
		}

		{
			String phoneNumber = StringHelper.removeNonDigits(form.getPhoneNumber());
			PhoneNumberValidator phoneNumberValidator =	new PhoneNumberValidator(phoneNumber);
			if (phoneNumberValidator.isPhoneNumberBlank())
			{
				String fieldName =	messageResources.getMessage("label.phone.number");
				errors.add("phoneNumber",new ActionMessage("errors.required", fieldName));
			} else if (phoneNumberValidator.isPhoneNumberInvalid())
			{
				errors.add("phoneNumber",new ActionMessage("errors.invalid.phone.number",phoneNumber));
			} else
			{
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();
				if (fs.checkBlockedPhone(getUserProfile(request).getUID(),phoneNumber))
				{
					errors.add("phoneNumber",new ActionMessage("error.phone.is.blocked",form.getPhoneNumber()));
					form.setPhoneNumberTaintText(TaintIndicatorType.BLOCKED_TEXT);
				} else
				{
					form.setPhoneNumberTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
				}
			}
		}

		{
			String phoneNumberAlternate = StringHelper.removeNonDigits(form.getPhoneNumberAlternate());
			PhoneNumberValidator phoneNumberValidator =	new PhoneNumberValidator(phoneNumberAlternate);
			if (!phoneNumberValidator.isPhoneNumberBlank())
			{
				if (phoneNumberValidator.isPhoneNumberInvalid())
				{
					errors.add("phoneNumberAlternate",new ActionMessage("errors.invalid.phone.number",phoneNumberAlternate));
				} else
				{
					ServiceFactory sf = ServiceFactory.getInstance();
					FraudService fs = sf.getFraudService();
					if (fs.checkBlockedPhone(getUserProfile(request).getUID(),phoneNumberAlternate))
					{
						errors.add("phoneNumberAlternate",	new ActionMessage("error.phone.is.blocked",	form.getPhoneNumberAlternate()));
						form.setPhoneNumberAlternateTaintText(TaintIndicatorType.BLOCKED_TEXT);
					} else
					{
						form.setPhoneNumberAlternateTaintText(TaintIndicatorType.NOT_BLOCKED_TEXT);
					}
				}
			}
			return (errors);
		}
	}

	private ActionErrors validateProfileDVS(ConsumerProfile consumerProfile,HttpServletRequest request) throws Exception
	{
		ActionErrors errors = new ActionErrors();
		try {
			DVSService dvss = DVSServiceImpl.getInstance();
			HashMap dvsErrors = (HashMap) dvss.validateConsumer(consumerProfile);
			if (!dvsErrors.isEmpty())
			{
				Iterator itr = dvsErrors.keySet().iterator();
				while (itr.hasNext()){
					OffendingElement[] oe = (OffendingElement[])dvsErrors.get(itr.next());
					StringBuffer sb = new StringBuffer();
					sb.append(" Offending fields:[ ");
					for (int i = 0; i < oe.length; i++) {
						sb.append(oe[i].getElementName());
						if (i<oe.length-1) sb.append(", ");
					}
					sb.append(" ]");
					errors.add(	ActionErrors.GLOBAL_ERROR, new ActionError("msg.dvs.failure",sb.toString()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.error(e.getLocalizedMessage());
			throw e;
		}
		return errors;
	}
}