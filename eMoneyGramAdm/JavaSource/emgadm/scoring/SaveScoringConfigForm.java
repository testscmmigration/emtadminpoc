package emgadm.scoring;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class SaveScoringConfigForm extends EMoneyGramAdmBaseValidatorForm
{

	private String submitDone = null;
	private String currentTranType = null;
	private String hiddenSaveAction = null;
	private String hiddenConfigId = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		submitDone = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		return errors;
	}

	/**
	 * @return
	 */
	public String getSubmitDone()
	{
		return submitDone;
	}

	/**
	 * @param string
	 */
	public void setSubmitDone(String string)
	{
		submitDone = string;
	}

	/**
	 * @return
	 */
	public String getCurrentTranType()
	{
		return currentTranType;
	}

	/**
	 * @param string
	 */
	public void setCurrentTranType(String string)
	{
		currentTranType = string;
	}


	/**
	 * @return
	 */
	public String getHiddenConfigId()
	{
		return hiddenConfigId;
	}


	/**
	 * @param string
	 */
	public void setHiddenConfigId(String string)
	{
		hiddenConfigId = string;
	}

	/**
	 * @return
	 */
	public String getHiddenSaveAction()
	{
		return hiddenSaveAction;
	}

	/**
	 * @param string
	 */
	public void setHiddenSaveAction(String string)
	{
		hiddenSaveAction = string;
	}

}
