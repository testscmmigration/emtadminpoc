package emgadm.scoring;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class EditScoringConfigForm extends EMoneyGramAdmBaseValidatorForm {
	private String submitSave = null;
	private String submitCancel = null;
	private String currentTranType = null;
	private String cloneConfiguration = null;
	private String submitClone = null;
	private String acceptThreshold;
	private String rejectThreshold;
	private String weightText1;
	private String value1 = null;
	private String pointsText1;
	private String submitAdd1 = null;
	private String disable1 = null;
	private String hiddenCategoryName1 = null;
	private String weightText2;
	private String value2 = null;
	private String pointsText2;
	private String submitAdd2 = null;
	private String disable2 = null;
	private String hiddenCategoryName2 = null;
	private String weightText3;
	private String value3 = null;
	private String pointsText3;
	private String submitAdd3 = null;
	private String disable3 = null;
	private String hiddenCategoryName3 = null;
	private String weightText4;
	private String minimumValueText4;
	private String maximumValueText4;
	private String pointsText4;
	private String submitAdd4 = null;
	private String disable4 = null;
	private String hiddenCategoryName4 = null;
	private String weightText5;
	private String value5 = null;
	private String pointsText5;
	private String submitAdd5 = null;
	private String disable5 = null;
	private String hiddenCategoryName5 = null;
	private String weightText6;
	private String value6 = null;
	private String pointsText6;
	private String submitAdd6 = null;
	private String disable6 = null;
	private String hiddenCategoryName6 = null;
	private String weightText7;
	private String value7 = null;
	private String pointsText7;
	private String submitAdd7 = null;
	private String disable7 = null;
	private String hiddenCategoryName7 = null;
	private String weightText8;
	private String value8 = null;
	private String pointsText8;
	private String submitAdd8 = null;
	private String disable8 = null;
	private String hiddenCategoryName8 = null;
	private String weightText9;
	private String value9 = null;
	private String pointsText9;
	private String submitAdd9 = null;
	private String disable9 = null;
	private String hiddenCategoryName9 = null;
	private String weightText10;
	private String value10 = null;
	private String pointsText10;
	private String submitAdd10 = null;
	private String disable10 = null;
	private String hiddenCategoryName10 = null;
	private String weightText11;
	private String value11 = null;
	private String pointsText11;
	private String submitAdd11 = null;
	private String disable11 = null;
	private String hiddenCategoryName11 = null;
	private String weightText12;
	private String value12 = null;
	private String pointsText12;
	private String submitAdd12 = null;
	private String disable12 = null;
	private String hiddenCategoryName12 = null;
	private String weightText13;
	private String value13 = null;
	private String pointsText13;
	private String submitAdd13 = null;
	private String disable13 = null;
	private String hiddenCategoryName13 = null;
	private String minimumValueText13;
	private String maximumValueText13;
	private String weightText15;
	private String minimumValueText15;
	private String maximumValueText15;
	private String pointsText15;
	private String submitAdd15 = null;
	private String disable15 = null;
	private String hiddenCategoryName15 = null;
	private String hiddenEditAction = null;
	private String hiddenCategory = null;
	private String hiddenValue = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		submitSave = null;
		submitCancel = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		errors = errors == null ? new ActionErrors() : errors;
		boolean globalMsg = false;
		if (!StringHelper.isNullOrEmpty(submitSave)) {
			if (StringHelper.isNullOrEmpty(acceptThreshold)) {
				errors.add("acceptThreshold", new ActionError("errors.required", "Threshold"));
			} else if (StringHelper.containsNonDigits(acceptThreshold)) {
				errors.add("acceptThreshold", new ActionError("errors.integer", "Threshold"));
			} else if (Integer.parseInt(acceptThreshold) < 0) {
				errors.add("acceptThreshold", new ActionError("error.scoring.threshold.range"));
			}
			if (StringHelper.isNullOrEmpty(rejectThreshold)) {
				errors.add("rejectThreshold", new ActionError("errors.required", "Threshold"));
			} else if (StringHelper.containsNonDigits(rejectThreshold)) {
				errors.add("rejectThreshold", new ActionError("errors.integer", "Threshold"));
			} else if (Integer.parseInt(rejectThreshold) < 0) {
				errors.add("rejectThreshold", new ActionError("error.scoring.threshold.range"));
			}
			if (errors.size() == 0 && Integer.parseInt(acceptThreshold) > Integer.parseInt(rejectThreshold)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.invalid.threshold"));
				globalMsg = true;
			}
			if (errors.size() == 0
					&& (!StringHelper.isNullOrEmpty(disable1) && !StringHelper.isNullOrEmpty(disable2)
							&& !StringHelper.isNullOrEmpty(disable3) && !StringHelper.isNullOrEmpty(disable4)
							&& !StringHelper.isNullOrEmpty(disable5) && !StringHelper.isNullOrEmpty(disable6)
							&& !StringHelper.isNullOrEmpty(disable7) && !StringHelper.isNullOrEmpty(disable8)
							&& !StringHelper.isNullOrEmpty(disable9) && !StringHelper.isNullOrEmpty(disable10)
							&& !StringHelper.isNullOrEmpty(disable11) && !StringHelper.isNullOrEmpty(disable12)
							&& !StringHelper.isNullOrEmpty(disable13) && !StringHelper.isNullOrEmpty(disable15))) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.no.active.category"));
				globalMsg = true;
			}
			if (errors.size() == 0) {
				String jumpTo = null;
				ActionError error = null;
				
				error = checkWeight(disable15, weightText15, "Category Weight");
				if (error != null) {
					errors.add("weightText15", error);
					jumpTo = "cat15";
				}
				error = checkWeight(disable13, weightText13, "Category Weight");
				if (error != null) {
					errors.add("weightText13", error);
					jumpTo = "cat13";
				}
				error = checkWeight(disable12, weightText12, "Category Weight");
				if (error != null) {
					errors.add("weightText12", error);
					jumpTo = "cat12";
				}
				error = checkWeight(disable11, weightText11, "Category Weight");
				if (error != null) {
					errors.add("weightText11", error);
					jumpTo = "cat11";
				}
				error = checkWeight(disable10, weightText10, "Category Weight");
				if (error != null) {
					errors.add("weightText10", error);
					jumpTo = "cat10";
				}
				error = checkWeight(disable9, weightText9, "Category Weight");
				if (error != null) {
					errors.add("weightText9", error);
					jumpTo = "cat9";
				}
				error = checkWeight(disable8, weightText8, "Category Weight");
				if (error != null) {
					errors.add("weightText8", error);
					jumpTo = "cat8";
				}
				error = checkWeight(disable7, weightText7, "Category Weight");
				if (error != null) {
					errors.add("weightText7", error);
					jumpTo = "cat7";
				}
				error = checkWeight(disable6, weightText6, "Category Weight");
				if (error != null) {
					errors.add("weightText6", error);
					jumpTo = "cat6";
				}
				error = checkWeight(disable5, weightText5, "Category Weight");
				if (error != null) {
					errors.add("weightText5", error);
					jumpTo = "cat5";
				}
				error = checkWeight(disable4, weightText4, "Category Weight");
				if (error != null) {
					errors.add("weightText4", error);
					jumpTo = "cat4";
				}
				error = checkWeight(disable3, weightText3, "Category Weight");
				if (error != null) {
					errors.add("weightText3", error);
					jumpTo = "cat3";
				}
				error = checkWeight(disable2, weightText2, "Category Weight");
				if (error != null) {
					errors.add("weightText2", error);
					jumpTo = "cat2";
				}
				error = checkWeight(disable1, weightText1, "Category Weight");
				if (error != null) {
					errors.add("weightText1", error);
					jumpTo = "cat1";
				}
				if (jumpTo != null) {
					request.setAttribute("jumpTo", jumpTo);
				}
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd1)) {
			if (StringHelper.isNullOrEmpty(value1)) {
				errors.add("Category1Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText1, "Scoring Points");
			if (error != null) {
				errors.add("Category1Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat1");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd2)) {
			if (StringHelper.isNullOrEmpty(value2)) {
				errors.add("Category2Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText2, "Scoring Points");
			if (error != null) {
				errors.add("Category2Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat2");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd3)) {
			ActionError error = checkValue(value3, "Category Value");
			if (error != null) {
				errors.add("Category3Error", error);
			}
			error = checkPoints(pointsText3, "Scoring Points");
			if (error != null) {
				errors.add("Category3Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat3");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd4)) {
			ActionError error = checkValue(minimumValueText4, "Minimum Value");
			if (error != null) {
				errors.add("Category4Error", error);
			}
			error = checkValue(maximumValueText4, "Maximum Value");
			if (error != null) {
				errors.add("Category4Error", error);
			}
			error = checkPoints(pointsText4, "Scoring Points");
			if (error != null) {
				errors.add("Category4Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat4");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd5)) {
			if (StringHelper.isNullOrEmpty(value5)) {
				errors.add("Category5Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText5, "Scoring Points");
			if (error != null) {
				errors.add("Category5Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat5");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd6)) {
			if (StringHelper.isNullOrEmpty(value6)) {
				errors.add("Category6Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText6, "Scoring Points");
			if (error != null) {
				errors.add("Category6Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat6");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd7)) {
			if (StringHelper.isNullOrEmpty(value7)) {
				errors.add("Category7Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText7, "Scoring Points");
			if (error != null) {
				errors.add("Category7Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat7");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd8)) {
			if (StringHelper.isNullOrEmpty(value8)) {
				errors.add("Category8Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText8, "Scoring Points");
			if (error != null) {
				errors.add("Category8Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat8");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd9)) {
			if (StringHelper.isNullOrEmpty(value9)) {
				errors.add("Category9Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText9, "Scoring Points");
			if (error != null) {
				errors.add("Category9Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat9");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd10)) {
			if (StringHelper.isNullOrEmpty(value10)) {
				errors.add("Category10Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText10, "Scoring Points");
			if (error != null) {
				errors.add("Category10Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat10");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd11)) {
			if (StringHelper.isNullOrEmpty(value11)) {
				errors.add("Category11Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText11, "Scoring Points");
			if (error != null) {
				errors.add("Category11Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat11");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd12)) {
			if (StringHelper.isNullOrEmpty(value12)) {
				errors.add("Category12Error", new ActionError("error.scoring.no.value"));
			}
			ActionError error = checkPoints(pointsText12, "Scoring Points");
			if (error != null) {
				errors.add("Category12Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat12");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd13)) {
			ActionError error = checkValue(minimumValueText13, "Minimum Value");
			if (error != null) {
				errors.add("Category13Error", error);
			}
			error = checkValue(maximumValueText13, "Maximum Value");
			if (error != null) {
				errors.add("Category13Error", error);
			}
			error = checkPoints(pointsText13, "Scoring Points");
			if (error != null) {
				errors.add("Category13Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat13");
			}
		}
		if (!StringHelper.isNullOrEmpty(submitAdd15)) {
			ActionError error = checkValue(minimumValueText15, "Minimum Value");
			if (error != null) {
				errors.add("Category15Error", error);
			}
			error = checkValue(maximumValueText15, "Maximum Value");
			if (error != null) {
				errors.add("Category15Error", error);
			}
			error = checkPoints(pointsText15, "Scoring Points");
			if (error != null) {
				errors.add("Category15Error", error);
			}
			if (errors.size() != 0) {
				request.setAttribute("jumpTo", "cat15");
			}
		}
		if (errors.size() != 0 && !globalMsg) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.page.contains.error"));
		}
		return errors;
	}

	public String getDisable1() {
		return disable1 = disable1 == null ? "" : disable1;
	}

	public String getDisable2() {
		return disable2 = disable2 == null ? "" : disable2;
	}

	public String getDisable3() {
		return disable3 = disable3 == null ? "" : disable3;
	}

	public String getDisable4() {
		return disable4 = disable4 == null ? "" : disable4;
	}

	public String getSubmitAdd1() {
		return submitAdd1;
	}

	public String getSubmitAdd2() {
		return submitAdd2;
	}

	public String getSubmitAdd3() {
		return submitAdd3;
	}

	public String getSubmitAdd4() {
		return submitAdd4;
	}

	public String getValue1() {
		return value1;
	}

	public String getValue2() {
		return value2;
	}

	public String getValue3() {
		return value3;
	}

	public void setDisable1(String string) {
		weightText1 = "0";
		disable1 = string;
	}

	public void setDisable2(String string) {
		weightText2 = "0";
		disable2 = string;
	}

	public void setDisable3(String string) {
		weightText3 = "0";
		disable3 = string;
	}

	public void setDisable4(String string) {
		weightText4 = "0";
		disable4 = string;
	}

	public void setSubmitAdd1(String string) {
		submitAdd1 = string;
	}

	public void setSubmitAdd2(String string) {
		submitAdd2 = string;
	}

	public void setSubmitAdd3(String string) {
		submitAdd3 = string;
	}

	public void setSubmitAdd4(String string) {
		submitAdd4 = string;
	}

	public void setValue1(String string) {
		value1 = string;
	}

	public void setValue2(String string) {
		value2 = string;
	}

	public void setValue3(String string) {
		value3 = string;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public String getSubmitSave() {
		return submitSave;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public void setSubmitSave(String string) {
		submitSave = string;
	}

	public String getCurrentTranType() {
		return currentTranType;
	}

	public void setCurrentTranType(String string) {
		currentTranType = string;
	}

	public String getHiddenCategory() {
		return hiddenCategory;
	}

	public String getHiddenValue() {
		return hiddenValue;
	}

	public void setHiddenCategory(String string) {
		hiddenCategory = string;
	}

	public void setHiddenValue(String string) {
		hiddenValue = string;
	}

	public String getHiddenCategoryName1() {
		return hiddenCategoryName1;
	}

	public String getHiddenCategoryName2() {
		return hiddenCategoryName2;
	}

	public String getHiddenCategoryName3() {
		return hiddenCategoryName3;
	}

	public String getHiddenCategoryName4() {
		return hiddenCategoryName4;
	}

	public void setHiddenCategoryName1(String string) {
		hiddenCategoryName1 = string;
	}

	public void setHiddenCategoryName2(String string) {
		hiddenCategoryName2 = string;
	}

	public void setHiddenCategoryName3(String string) {
		hiddenCategoryName3 = string;
	}

	public void setHiddenCategoryName4(String string) {
		hiddenCategoryName4 = string;
	}

	public String getCloneConfiguration() {
		return cloneConfiguration;
	}

	public String getSubmitClone() {
		return submitClone;
	}

	public void setCloneConfiguration(String string) {
		cloneConfiguration = string;
	}

	public void setSubmitClone(String string) {
		submitClone = string;
	}

	public String getMaximumValueText4() {
		return maximumValueText4;
	}

	public String getMaximumValueText13() {
		return maximumValueText13;
	}

	public String getMinimumValueText4() {
		return minimumValueText4;
	}
	
	public String getMinimumValueText13() {
		return minimumValueText13;
	}
	public void setMinimumValueText13(String s) {
		minimumValueText13 = s;
	}

	public void setMaximumValueText13(String s) {
		maximumValueText13 = s;
	}
	public void setMaximumValueText4(String s) {
		maximumValueText4 = s;
	}

	public void setMinimumValueText4(String s) {
		minimumValueText4 = s;
	}

	public String getHiddenEditAction() {
		return hiddenEditAction;
	}

	public void setHiddenEditAction(String string) {
		hiddenEditAction = string;
	}

	public String getAcceptThreshold() {
		return acceptThreshold;
	}

	public String getRejectThreshold() {
		return rejectThreshold;
	}

	public void setAcceptThreshold(String string) {
		acceptThreshold = string;
	}

	public void setRejectThreshold(String string) {
		rejectThreshold = string;
	}

	public String getDisable5() {
		return disable5 = disable5 == null ? "" : disable5;
	}

	public String getDisable6() {
		return disable6 = disable6 == null ? "" : disable6;
	}

	public String getDisable7() {
		return disable7 = disable7 == null ? "" : disable7;
	}

	public String getDisable8() {
		return disable8 = disable8 == null ? "" : disable8;
	}

	public String getDisable9() {
		return disable9 = disable9 == null ? "" : disable9;
	}

	public String getDisable10() {
		return disable10 = disable10 == null ? "" : disable10;
	}

	public String getDisable11() {
		return disable11 = disable11 == null ? "" : disable11;
	}

	public String getDisable12() {
		return disable12 = disable12 == null ? "" : disable12;
	}

	public String getHiddenCategoryName5() {
		return hiddenCategoryName5;
	}

	public String getHiddenCategoryName6() {
		return hiddenCategoryName6;
	}

	public String getHiddenCategoryName7() {
		return hiddenCategoryName7;
	}

	public String getHiddenCategoryName8() {
		return hiddenCategoryName8;
	}

	public String getHiddenCategoryName9() {
		return hiddenCategoryName9;
	}

	public String getHiddenCategoryName10() {
		return hiddenCategoryName10;
	}

	public String getHiddenCategoryName11() {
		return hiddenCategoryName11;
	}

	public String getHiddenCategoryName12() {
		return hiddenCategoryName12;
	}

	public String getSubmitAdd5() {
		return submitAdd5;
	}

	public String getSubmitAdd6() {
		return submitAdd6;
	}

	public String getSubmitAdd7() {
		return submitAdd7;
	}

	public String getSubmitAdd8() {
		return submitAdd8;
	}

	public String getSubmitAdd9() {
		return submitAdd9;
	}

	public String getSubmitAdd10() {
		return submitAdd10;
	}

	public String getSubmitAdd11() {
		return submitAdd11;
	}

	public String getSubmitAdd12() {
		return submitAdd12;
	}

	public String getSubmitAdd13() {
		return submitAdd13;
	}

	public String getValue5() {
		return value5;
	}

	public String getValue6() {
		return value6;
	}

	public String getValue7() {
		return value7;
	}

	public String getValue8() {
		return value8;
	}

	public String getValue9() {
		return value9;
	}

	public String getValue10() {
		return value10;
	}

	public String getValue11() {
		return value11;
	}

	public String getValue12() {
		return value12;
	}

	public void setDisable5(String string) {
		weightText5 = "0";
		disable5 = string;
	}

	public void setDisable6(String string) {
		weightText6 = "0";
		disable6 = string;
	}

	public void setDisable7(String string) {
		weightText7 = "0";
		disable7 = string;
	}

	public void setDisable8(String string) {
		weightText8 = "0";
		disable8 = string;
	}

	public void setDisable9(String string) {
		weightText9 = "0";
		this.disable9 = string;
	}

	public void setDisable10(String string) {
		weightText10 = "0";
		disable10 = string;
	}

	public void setDisable11(String string) {
		weightText11 = "0";
		disable11 = string;
	}

	public void setDisable12(String string) {
		weightText12 = "0";
		disable12 = string;
	}

	public void setHiddenCategoryName5(String string) {
		hiddenCategoryName5 = string;
	}

	public void setHiddenCategoryName6(String string) {
		hiddenCategoryName6 = string;
	}

	public void setHiddenCategoryName7(String string) {
		hiddenCategoryName7 = string;
	}

	public void setHiddenCategoryName8(String string) {
		hiddenCategoryName8 = string;
	}

	public void setHiddenCategoryName9(String string) {
		this.hiddenCategoryName9 = string;
	}

	public void setHiddenCategoryName10(String string) {
		this.hiddenCategoryName10 = string;
	}

	public void setHiddenCategoryName11(String string) {
		this.hiddenCategoryName11 = string;
	}

	public void setHiddenCategoryName12(String string) {
		this.hiddenCategoryName12 = string;
	}

	public void setSubmitAdd5(String string) {
		submitAdd5 = string;
	}

	public void setSubmitAdd6(String string) {
		submitAdd6 = string;
	}

	public void setSubmitAdd7(String string) {
		submitAdd7 = string;
	}

	public void setSubmitAdd8(String string) {
		submitAdd8 = string;
	}

	public void setSubmitAdd9(String string) {
		this.submitAdd9 = string;
	}

	public void setSubmitAdd10(String string) {
		this.submitAdd10 = string;
	}

	public void setSubmitAdd11(String string) {
		this.submitAdd11 = string;
	}

	public void setSubmitAdd12(String string) {
		this.submitAdd12 = string;
	}

	public void setValue5(String string) {
		value5 = string;
	}

	public void setValue6(String string) {
		value6 = string;
	}

	public void setValue7(String string) {
		value7 = string;
	}

	public void setValue8(String string) {
		value8 = string;
	}

	public void setValue9(String string) {
		this.value9 = string;
	}

	public void setValue10(String string) {
		this.value10 = string;
	}

	public void setValue11(String string) {
		this.value11 = string;
	}

	public void setValue12(String string) {
		this.value12 = string;
	}

	public String getWeightText1() {
		return weightText1;
	}

	public void setWeightText1(String string) {
		weightText1 = string;
	}

	public String getWeightText2() {
		return weightText2;
	}

	public void setWeightText2(String string) {
		weightText2 = string;
	}

	public String getWeightText3() {
		return weightText3;
	}

	public void setWeightText3(String string) {
		weightText3 = string;
	}

	public String getWeightText4() {
		return weightText4;
	}

	public void setWeightText4(String string) {
		weightText4 = string;
	}

	public String getWeightText5() {
		return weightText5;
	}

	public void setWeightText5(String string) {
		weightText5 = string;
	}

	public String getWeightText6() {
		return weightText6;
	}

	public void setWeightText6(String string) {
		weightText6 = string;
	}

	public String getWeightText7() {
		return weightText7;
	}

	public void setWeightText7(String string) {
		weightText7 = string;
	}

	public String getWeightText8() {
		return weightText8;
	}

	public void setWeightText8(String string) {
		weightText8 = string;
	}

	public String getWeightText9() {
		return weightText9;
	}

	public void setWeightText9(String string) {
		this.weightText9 = string;
	}

	public String getWeightText10() {
		return weightText10;
	}

	public void setWeightText10(String string) {
		this.weightText10 = string;
	}

	public String getWeightText11() {
		return weightText11;
	}

	public void setWeightText11(String string) {
		this.weightText11 = string;
	}

	public String getWeightText12() {
		return weightText12;
	}

	public void setWeightText12(String string) {
		this.weightText12 = string;
	}

	public String getPointsText1() {
		return pointsText1;
	}

	public void setPointsText1(String string) {
		pointsText1 = string;
	}

	public String getPointsText2() {
		return pointsText2;
	}

	public void setPointsText2(String string) {
		pointsText2 = string;
	}

	public String getPointsText3() {
		return pointsText3;
	}

	public void setPointsText3(String string) {
		pointsText3 = string;
	}

	public String getPointsText4() {
		return pointsText4;
	}

	public void setPointsText4(String string) {
		pointsText4 = string;
	}

	public String getPointsText5() {
		return pointsText5;
	}

	public void setPointsText5(String string) {
		pointsText5 = string;
	}

	public String getPointsText6() {
		return pointsText6;
	}

	public void setPointsText6(String string) {
		pointsText6 = string;
	}

	public String getPointsText7() {
		return pointsText7;
	}

	public void setPointsText7(String string) {
		pointsText7 = string;
	}

	public String getPointsText8() {
		return pointsText8;
	}

	public void setPointsText8(String string) {
		pointsText8 = string;
	}

	public String getPointsText9() {
		return pointsText9;
	}

	public void setPointsText9(String string) {
		this.pointsText9 = string;
	}

	public String getPointsText10() {
		return pointsText10;
	}

	public void setPointsText10(String string) {
		this.pointsText10 = string;
	}

	public String getPointsText11() {
		return pointsText11;
	}

	public void setPointsText11(String string) {
		this.pointsText11 = string;
	}

	public String getPointsText12() {
		return pointsText12;
	}

	public void setPointsText12(String string) {
		this.pointsText12 = string;
	}

	public String getWeightText13() {
		return weightText13;
	}

	public void setWeightText13(String weightText13) {
		this.weightText13 = weightText13;
	}

	public String getValue13() {
		return value13;
	}

	public void setValue13(String value13) {
		this.value13 = value13;
	}

	public String getPointsText13() {
		return pointsText13;
	}

	public void setPointsText13(String pointsText13) {
		this.pointsText13 = pointsText13;
	}

	public String getDisable13() {
		return disable13 = disable13 == null ? "" : disable13;
	}

	public void setDisable13(String disable13) {
		weightText13 = "0";
		this.disable13 = disable13;
	}

	public String getHiddenCategoryName13() {
		return hiddenCategoryName13;
	}

	public void setHiddenCategoryName13(String hiddenCategoryName13) {
		this.hiddenCategoryName13 = hiddenCategoryName13;
	}

	public void setSubmitAdd13(String submitAdd13) {
		this.submitAdd13 = submitAdd13;
	}

	private ActionError checkWeight(String disable, String weightText, String field) {
		ActionError error = null;
		if (StringHelper.isNullOrEmpty(disable)) {
			if (StringHelper.isNullOrEmpty(weightText)) {
				error = new ActionError("errors.required", field);
			} else if (StringHelper.containsNonDigits(weightText)) {
				error = new ActionError("errors.integer", field);
			} else if (Integer.parseInt(weightText) < 1 || Integer.parseInt(weightText) > 10) {
				error = new ActionError("error.scoring.invalid.weight");
			}
		}
		return error;
	}

	private ActionError checkPoints(String pointsText, String field) {
		ActionError error = null;
		if (StringHelper.isNullOrEmpty(pointsText)) {
			error = new ActionError("errors.required", field);
		} else if (StringHelper.containsNonDigits(pointsText)) {
			error = new ActionError("errors.integer", field);
		} else if (Integer.parseInt(pointsText) < 0 || Integer.parseInt(pointsText) > 1000) {
			error = new ActionError("error.scoring.point.range");
		}
		return error;
	}

	private ActionError checkValue(String value, String field) {
		ActionError error = null;
		if (StringHelper.isNullOrEmpty(value)) {
			error = new ActionError("errors.required", field);
		} else if (StringHelper.containsNonDigits(value)) {
			error = new ActionError("errors.integer", field);
		}
		return error;
	}

	public void setWeightText15(String weightText15) {
		this.weightText15 = weightText15;
	}

	public String getWeightText15() {
		return weightText15;
	}

	public void setMinimumValueText15(String minimumValueText15) {
		this.minimumValueText15 = minimumValueText15;
	}

	public String getMinimumValueText15() {
		return minimumValueText15;
	}

	public void setMaximumValueText15(String maximumValueText15) {
		this.maximumValueText15 = maximumValueText15;
	}

	public String getMaximumValueText15() {
		return maximumValueText15;
	}

	public void setPointsText15(String pointsText15) {
		this.pointsText15 = pointsText15;
	}

	public String getPointsText15() {
		return pointsText15;
	}

	public void setSubmitAdd15(String submitAdd15) {
		this.submitAdd15 = submitAdd15;
	}

	public String getSubmitAdd15() {
		return submitAdd15;
	}

	public void setDisable15(String disable15) {
		this.disable15 = disable15;
	}

	public String getDisable15() {
		return disable15;
	}

	public void setHiddenCategoryName15(String hiddenCategoryName15) {
		this.hiddenCategoryName15 = hiddenCategoryName15;
	}

	public String getHiddenCategoryName15() {
		return hiddenCategoryName15;
	}
}
