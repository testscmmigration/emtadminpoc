package emgadm.scoring;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class SaveAutomationForm extends EMoneyGramAdmBaseValidatorForm
{

	private String currentTranType;
	private String automate;
	private String allowScoring; 
	private String submitCancel = null;
	private String submitSave = null;
	private String partnerSiteId = null;
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		automate = null;
		allowScoring = null; 
		submitCancel = null;
		submitSave = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		return errors;
	}


	/**
	 * @return
	 */
	public String getSubmitCancel()
	{
		return submitCancel;
	}

	/**
	 * @return
	 */
	public String getSubmitSave()
	{
		return submitSave;
	}


	/**
	 * @param string
	 */
	public void setSubmitCancel(String string)
	{
		submitCancel = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitSave(String string)
	{
		submitSave = string;
	}


	/**
	 * @return
	 */
	public String getCurrentTranType()
	{
		return currentTranType;
	}

	/**
	 * @param string
	 */
	public void setCurrentTranType(String string)
	{
		currentTranType = string;
	}

	/**
	 * @return
	 */
	public String getAllowScoring()
	{
		return allowScoring;
	}

	/**
	 * @return
	 */
	public String getAutomate()
	{
		return automate;
	}

	/**
	 * @param string
	 */
	public void setAllowScoring(String string)
	{
		allowScoring = string;
	}

	/**
	 * @param string
	 */
	public void setAutomate(String string)
	{
		automate = string;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

}
