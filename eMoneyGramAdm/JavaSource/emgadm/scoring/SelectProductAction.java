package emgadm.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ibm.wsspi.sib.exitpoint.ra.HashMap;

import shared.mgo.dto.MGOProduct;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.TranType;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.model.EMTTransactionType;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.model.ScoreConfiguration;
import emgshared.model.ScoringSingleton;
import emgshared.model.UserActivityType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ServiceFactory;
import emgshared.services.TransactionService;
import emgshared.util.*;

/**
 * @version 	1.0
 * @author
 */
public class SelectProductAction extends EMoneyGramAdmBaseAction
{

	@SuppressWarnings("unchecked")
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm strutsForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
	    ActionMessages messages = getActionMessages(request);
		ActionErrors errors = new ActionErrors();
		UserProfile up = getUserProfile(request);
		SelectProductForm form = (SelectProductForm) strutsForm;
		TransactionService ts = ServiceFactory.getInstance().getTransactionService();
		Collection<EMTTransactionType> tt = new ArrayList<EMTTransactionType>();
		
		String partnerSiteId = null;
		if (form.getPartnerSiteId() != null
				&& form.getPartnerSiteId().matches("[a-zA-Z_]+")) {
			partnerSiteId = form.getPartnerSiteId();
		} else {
			partnerSiteId = Constants.MGO_PARTNER_SITE_ID;
		}
//		System.out.println("SelectProductAction got partnerSiteId=" + partnerSiteId);

		//Only for the selected site.
		tt.addAll(ts.getTransactionTypes(Constants.partnerSiteIdToCode.get(partnerSiteId)));

		ArrayList<EMTTransactionType> ttFiltered = new ArrayList<EMTTransactionType>();
		for (Iterator<EMTTransactionType> iterator = tt.iterator(); iterator.hasNext();) {
			EMTTransactionType tranType = iterator.next();
			if (!((tranType.getEmgTranTypeCode().equalsIgnoreCase(MGOProduct.BILL_PAY_CASH)) || (tranType.getEmgTranTypeCode().equalsIgnoreCase(MGOProduct.SAME_DAY_CASH)))) {
				if (tranType.getEmgTranTypeCodeLabel().equals(form.getTranType())) {
					request.getSession().setAttribute("currentTranType", tranType);
				}
				ttFiltered.add(tranType);
			}
		}
		request.getSession().setAttribute("tranTypes", ttFiltered);

		if (!StringHelper.isNullOrEmpty(form.getSubmitAutomation()) || !StringHelper.isNullOrEmpty(form.getSubmitScoringConfig()))
		{
			for (Iterator<EMTTransactionType> it = tt.iterator(); it.hasNext();)
			{
				EMTTransactionType tranType = it.next();
				if (tranType.getEmgTranTypeCodeLabel().equals(form.getTranType()))
				{
					request.getSession().setAttribute("currentTranType",tranType);
					break;
				}
			}
		}

		// save as session attribute
		request.getSession().setAttribute("partnerSiteId", partnerSiteId);
		// TODO figure out what to do with MGOUK

	    if (!StringHelper.isNullOrEmpty(form.getSubmitSaveAutomationParms()))
	    {
	        try {
	            if (validateVelocityParms(form,errors,request))
	            {
	                updateVelocityAutomationParms(form,messages,request);
	                errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("automation.velocity.updated"));
	                saveMessages(request, errors);
	            }
            } catch (Exception e) {
                return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
            }
	    }
	    else
	    {
	        PropertyKey pk = new PropertyKey();
			pk.setPropName(EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD);
			Map propMap = PropertyDAO.getPropertiesByKey(up.getUID(),pk,true);
			PropertyBean pb = (PropertyBean) propMap.get(pk.getPropName());
			form.setMaximumNumberAutomatedPerPeriod(new Integer(pb.getPropKey().getPropVal()));
			pk.setPropName(EMTSharedDynProperties.AUTOMATIONPERIODINHRS);
			propMap = PropertyDAO.getPropertiesByKey(up.getUID(),pk,true);
			pb = (PropertyBean) propMap.get(pk.getPropName());
			form.setAutomationPeriodInHrs(new Integer(pb.getPropKey().getPropVal()));
	    }
	    request.getSession().setAttribute("SelectProductForm", form);

		if (!StringHelper.isNullOrEmpty(form.getSubmitAutomation()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_AUTOMATION);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitScoringConfig())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_SCORING_CONFIG);
		}

		if (form.getHiddenProductAction() != null)
		{
			
			Map<String, String> transferMap = new HashMap();
			transferMap.put("configId", form.getHiddenProductConfigId());
			request.getSession().setAttribute("configId", transferMap);

			if (form.getHiddenProductAction().equals("view"))
			{
				form.setHiddenProductAction(null);
				TranType trt = new TranType();
				trt.setEmgTranTypeCode(form.getHiddenProductTranTypeCode());
				trt.setEmgTranTypeDesc(form.getHiddenProductTranTypeDesc());
				request.getSession().setAttribute("currentTranType", trt);
				request.getSession().setAttribute("referer", "SelectProduct");
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_SCORING_CONFIG);
			}
		}

		Collection<ScoreConfiguration> scoreConfigurations = new ArrayList<ScoreConfiguration>();

		for (Iterator<EMTTransactionType> it = ttFiltered.iterator(); it.hasNext();) {
			EMTTransactionType tranType = it.next();
			ScoreConfiguration sc = ScoringSingleton.getInstance().getScoringConfiguration(tranType);
			if (sc !=  null) {
				scoreConfigurations.add(sc);
			}
		}

		request.getSession().setAttribute("scoreConfigurations", scoreConfigurations);
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}


	private void updateVelocityAutomationParms(SelectProductForm form,ActionMessages errors,HttpServletRequest request) throws Exception
	{
	    try {
            PropertyKey pkMaxNum = new PropertyKey(
                    EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD,
                    null, form.getMaximumNumberAutomatedPerPeriod().toString());
            PropertyBean pbMaxNum = new PropertyBean(pkMaxNum);
            pbMaxNum.setAdminUseFlag("N");
            pbMaxNum.setDataTypeCode("int");
            pbMaxNum.setDefinedInClass(false);
            pbMaxNum.setDefinedInDb(true);
            pbMaxNum.setEditable(false);
            pbMaxNum.setMultivalueFlag("N");
            pbMaxNum.setPropDesc("Max # of Transactions automated for a profile in a given period");

            PropertyKey pkAutomationPeriodInHours = new PropertyKey(
                    EMTSharedDynProperties.AUTOMATIONPERIODINHRS, null, form
                            .getAutomationPeriodInHrs().toString());
            PropertyBean pbAutomationPeriod = new PropertyBean(
                    pkAutomationPeriodInHours);
            pbAutomationPeriod.setAdminUseFlag("N");
            pbAutomationPeriod.setDataTypeCode("int");
            pbAutomationPeriod.setDefinedInClass(false);
            pbAutomationPeriod.setDefinedInDb(true);
            pbAutomationPeriod.setEditable(false);
            pbAutomationPeriod.setMultivalueFlag("N");
            pbAutomationPeriod.setPropDesc("Automation Time Period in hours");

            PropertyDAO.setPropertyValue(pbMaxNum);
            PropertyDAO.setPropertyValue(pbAutomationPeriod);
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.automation.parms.updated"));
            saveMessages(request, errors);
            try {
                StringBuffer logDetail = new StringBuffer(256);
                logDetail.append("<" + EMTSharedDynProperties.AUTOMATIONPERIODINHRS + ">"
                        + form.getAutomationPeriodInHrs().toString() + "</"
                        + EMTSharedDynProperties.AUTOMATIONPERIODINHRS + ">");
                logDetail.append("<" + EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD + ">"
                                + form.getMaximumNumberAutomatedPerPeriod().toString() + "</"
                                + EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD + ">");
                super.insertActivityLog(this.getClass(), request, logDetail.toString(),UserActivityType.EVENT_TYPE_TRANSACTIONSCORINGEDIT,null);
            } catch (Exception ignore) {
                // if loggin fails, just move on
            }

        } catch (Exception e) {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError(
                    "system.error.has.occurred"));
            throw e;
        }
	}


	private boolean validateVelocityParms(SelectProductForm form,ActionMessages errors,HttpServletRequest request) throws Exception
	{
		if (form.getMaximumNumberAutomatedPerPeriod() == null)
		    errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.invalid.max.automated.trans.per.period",form.getMaximumNumberAutomatedPerPeriod().toString()));
		else if (form.getMaximumNumberAutomatedPerPeriod().intValue() < 1)
		    errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.invalid.max.automated.trans.per.period",form.getMaximumNumberAutomatedPerPeriod().toString()));

		if (form.getAutomationPeriodInHrs() == null)
		    errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.invalid.automation.period",form.getAutomationPeriodInHrs().toString()));
		else if ((form.getAutomationPeriodInHrs().intValue() < 1) ||(form.getAutomationPeriodInHrs().intValue() > 5000) )
			errors.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("error.invalid.automation.period",form.getAutomationPeriodInHrs().toString()));

		if (errors.isEmpty())
		    return true;
		else
		{
		    saveMessages(request, errors);
		    return false;
		}
	}
}
