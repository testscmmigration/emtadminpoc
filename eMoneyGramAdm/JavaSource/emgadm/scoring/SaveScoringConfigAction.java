package emgadm.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.EMTTransactionType;
import emgshared.model.ScoreConfiguration;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;


/**
 * @version 	1.0
 * @author
 */
public class SaveScoringConfigAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm strutsform,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		SaveScoringConfigForm form = (SaveScoringConfigForm) strutsform;
		UserProfile up = getUserProfile(request);
		EMTTransactionType currentTranType = null;
		EMTSharedDynProperties eaap = new EMTSharedDynProperties();
		ServiceFactory sf = ServiceFactory.getInstance();
		ScoringService ss = sf.getScoringService();
		TransactionManager tm = getTransactionManager(request);

		if (request.getSession().getAttribute("currentTranType") != null){
			currentTranType = (EMTTransactionType)request.getSession().getAttribute("currentTranType");
			request.setAttribute("partnerSiteId", currentTranType.getPartnerSiteId());
			form.setCurrentTranType(currentTranType.getEmgTranTypeCode());
		}

		//Done button Clicked
		if (!StringHelper.isNullOrEmpty(form.getSubmitDone())) {
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SELECT_PRODUCT);
		}

		//Activating a Score Configuration
		if (form.getHiddenSaveAction() != null && form.getHiddenSaveAction().equals("activate")){
			String scoreConfigId = form.getHiddenConfigId();
			ss.UpdateScoreConfigStatus(up.getUID(), Integer.parseInt(scoreConfigId), currentTranType.getEmgTranTypeCode(), "A", currentTranType.getPartnerSiteId());
			form.setHiddenSaveAction(null);
		}

		//Test, View, Edit a Score Configuration
		if (form.getHiddenSaveAction() != null){
			String scoreConfigId = form.getHiddenConfigId();
			request.setAttribute("configId", scoreConfigId);

			if (form.getHiddenSaveAction().equals("test")){
				form.setHiddenSaveAction(null);
				return mapping.findForward(
					EMoneyGramAdmForwardConstants.LOCAL_FORWARD_TEST_SCORING_CONFIG);
			}

			if (form.getHiddenSaveAction().equals("view")){
				form.setHiddenSaveAction(null);
				return mapping.findForward(
					EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_SCORING_CONFIG);
			}

			if (form.getHiddenSaveAction().equals("edit")){
				form.setHiddenSaveAction(null);
				request.getSession().setAttribute("addEditFlag", "edit");
				return mapping.findForward(
					EMoneyGramAdmForwardConstants.LOCAL_FORWARD_EDIT_SCORING_CONFIG);
			}
		}

		//Deleting a Score Configuration
		if (form.getHiddenSaveAction() != null && form.getHiddenSaveAction().equals("delete")){
			String scoreConfigId = form.getHiddenConfigId();
			ss.UpdateScoreConfigStatus(up.getUID(), Integer.parseInt(scoreConfigId), currentTranType.getEmgTranTypeCode(), "D", currentTranType.getPartnerSiteId());
			form.setHiddenSaveAction(null);
		}

		try {
			Collection allVersionsUnfiltered = ss.getAllScoreConfigurations(up.getUID(), currentTranType.getEmgTranTypeCode(), currentTranType.getPartnerSiteId());
			Collection allVersions = new ArrayList();
			for(Iterator it=allVersionsUnfiltered.iterator(); it.hasNext();) {
				ScoreConfiguration scoreConfiguration = (ScoreConfiguration)it.next();
				if (currentTranType.getEmgTranTypeCodeLabel().equals(scoreConfiguration.getTransactionCodeLabel())) {
					scoreConfiguration.setEditable(tm.isScoringConfigurationEditable(up.getUID(), scoreConfiguration.getConfigurationId()));
					allVersions.add(scoreConfiguration);
				}
			}
			request.setAttribute("allVersions", allVersions);
		}catch (TooManyResultException te) {
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"error.too.many.results",
				eaap.getMaxDownloadTransactions() + ""));
			saveErrors(request, errors);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
