package emgadm.scoring;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.ibm.wsspi.sib.exitpoint.ra.HashMap;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.EMTTransactionType;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class ShowScoringConfigAction extends EMoneyGramAdmBaseAction {

	private static Logger logger = LogFactory.getInstance().getLogger(ShowScoringConfigAction.class);

	public ActionForward execute(ActionMapping mapping, ActionForm strutsform, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// ActionErrors errors = new ActionErrors();
		try {
			ShowScoringConfigForm form = (ShowScoringConfigForm) strutsform;
			UserProfile up = getUserProfile(request);
			String cat1 = ScoreCategory.cat1;
			String cat2 = ScoreCategory.cat2;
			String cat3 = ScoreCategory.cat3;
			String cat4 = ScoreCategory.cat4;
			String cat5 = ScoreCategory.cat5;
			String cat6 = ScoreCategory.cat6;
			String cat7 = ScoreCategory.cat7;
			String cat8 = ScoreCategory.cat8;
			String cat9 = ScoreCategory.cat9;
			String cat10 = ScoreCategory.cat10;
			String cat11 = ScoreCategory.cat11;
			String cat12 = ScoreCategory.cat12;
			String cat13 = ScoreCategory.cat13;
			String cat14 = ScoreCategory.cat14;
			String cat15 = ScoreCategory.cat15;
			ServiceFactory sf = ServiceFactory.getInstance();
			ScoringService ss = sf.getScoringService();
			// String action = null;
			
			logger.error("### Beginning show scoring configuration for configId: " + request.getAttribute("configId") + " ###");
			
			if (request.getAttribute("configId") != null) {
				logger.error("Calling getScoreConfigurations");
				Collection chosenConfig = null;
				try {
					logger.error("Calling getScoreConfigurations");
					chosenConfig = ss.getScoreConfigurations(up.getUID(), (String) request.getAttribute("configId"), null);
					logger.error("End getScoreConfigurations");
				} catch (Exception e) {
					logger.error("An exception was thrown from getScoreConfiguration " + e.getMessage(), e);
				}
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(2);
				for (Iterator it = chosenConfig.iterator(); it.hasNext();) {
					ScoreConfiguration scoreConfiguration = (ScoreConfiguration) it.next();
					logger.error("Configuration Id: " + scoreConfiguration.getConfigurationId());
					logger.error("Configuration Version Number: " + scoreConfiguration.getConfigurationVersionNumber());
					logger.error("Transaction Type: " + scoreConfiguration.getTransactionType());
					logger.error("");
					int totalWeights = scoreConfiguration.getGetTotalWeight();
					for (Iterator it1 = scoreConfiguration.getScoreCategories().values().iterator(); it1.hasNext();) {
						ScoreCategory scoreCategory = (ScoreCategory) it1.next();
						logger.error("Score Category: " + scoreCategory.getScoreCategoryName());
						logger.error("Weight: " + scoreCategory.getWeight());
						logger.error("Included: " + scoreCategory.isIncluded());
						logger.error("");
						if (scoreCategory.isIncluded()) {
							try {
								scoreCategory.setWeightPercentage(nf.format((scoreCategory.getWeight() * 100.00) / totalWeights));
							} catch (Exception e) {
								logger.error("An exception was thrown from setWeightPercentage " + e.getMessage(), e);
							}
						}
					}
					request.setAttribute("scoreConfiguration", scoreConfiguration);
					ArrayList arCategories = new ArrayList();
					final int NUMBER_OF_CATEGORIES = ScoreCategory.catList.length;
					for (int i = 0; i < NUMBER_OF_CATEGORIES; ++i) {
						arCategories.add(i, scoreConfiguration.getScoreCategories().get(ScoreCategory.catList[i]));
					}
					request.setAttribute("scoreCategories", arCategories);
				}
				Map<String, String> transferMap = new HashMap();
				transferMap.put("configId", (String) request.getAttribute("configId"));
				request.getSession().setAttribute("configId", transferMap);
				// form.setCurrentConfigId(configId);
			}
			logger.error("### Ending show scoring configuration for configId: " + request.getAttribute("configId") + " ###");
			// Clicked the DONE button
			if (!StringHelper.isNullOrEmpty(form.getSubmitShowDone())) {
				if (request.getSession().getAttribute("referer") != null) {
					request.getSession().setAttribute("referer", null);
					return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SELECT_PRODUCT);
				}
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_SCORING_CONFIG);
			}
			// Clicked the CLONE button
			if (!StringHelper.isNullOrEmpty(form.getSubmitClone())) {
				request.setAttribute("action", "clone");
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_EDIT_SCORING_CONFIG);
			}
			if (request.getSession().getAttribute("currentTranType") != null) {

			}
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		} catch (Exception e) {
			logger.error("Exception thrown during the execution of the Action " + e.getMessage(), e);
			throw e;
		}
	}
}
