package emgadm.scoring;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.TranType;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.EMTTransactionType;
import emgshared.model.UserActivityType;

/**
 * @version 	1.0
 * @author
 */
public class SaveAutomationAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm strutsform,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		SaveAutomationForm form = (SaveAutomationForm) strutsform;
		UserProfile up = getUserProfile(request);
		//		Collection blockedBins = new ArrayList();
		TransactionManager tm = getTransactionManager(request);
		EMTTransactionType currentTranType =
			(EMTTransactionType) request.getSession().getAttribute("currentTranType");
		String partnerSiteId = form.getPartnerSiteId();
		System.out.println("SaveAutomationAction got partnerSiteId=" + partnerSiteId);
		// save as session attribute
		request.setAttribute("partnerSiteId", currentTranType.getPartnerSiteId());
		// TODO find out what to do here with MGOUK
		if (StringHelper.isNullOrEmpty(form.getSubmitCancel()) && StringHelper.isNullOrEmpty(form.getSubmitSave()))
		{
			if (currentTranType != null)
			{
				form.setCurrentTranType(currentTranType.getEmgTranTypeCode());
				if (currentTranType.getScoreFlag().equalsIgnoreCase("Y"))
				{
					form.setAllowScoring("on");
				} else
				{
					form.setAllowScoring(null);
				}

				if (currentTranType.getAutoApproveFlag().equalsIgnoreCase("Y"))
				{
					form.setAutomate("on");
				} else
				{
					form.setAutomate(null);
				}
			}
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SELECT_PRODUCT);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSave()))
		{

			if (form.getAllowScoring() == null && form.getAutomate() != null)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("error.scoring.invalid.setting"));
				saveErrors(request, errors);
			} else
			{
				if (form.getAllowScoring() == null)
					currentTranType.setScoreFlag("N");
				else
					currentTranType.setScoreFlag("Y");

				if (form.getAutomate() == null)
					currentTranType.setAutoApproveFlag("N");
				else
					currentTranType.setAutoApproveFlag("Y");

				tm.updateTransactionType(up.getUID(), currentTranType);

				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("msg.scoring.automation.updated"));
				saveErrors(request, errors);

				try {
				    StringBuffer logDetail = new StringBuffer(256);
				    logDetail.append("<Scoring>" + form.getAllowScoring()  + "</Scoring>");
				    logDetail.append("<Automation>" + form.getAutomate() + "</Automation>");
			    	super.insertActivityLog(this.getClass(),request,logDetail.toString(),UserActivityType.EVENT_TYPE_AUTOMATIONTURNON,currentTranType.getEmgTranTypeCode());    
				} catch (Exception e) { }

				return mapping.findForward(
					EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SELECT_PRODUCT);
			}
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
