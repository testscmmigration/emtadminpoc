package emgadm.scoring;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class ShowScoringConfigForm extends EMoneyGramAdmBaseValidatorForm
{

	private String submitShowDone = null;
	private String submitClone = null;
	private String currentTranType = null;
	private String currentConfigId = null;
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		submitShowDone = null;
		submitClone = null;
		
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		return errors;
	}


	/**
	 * @return
	 */
	public String getSubmitClone()
	{
		return submitClone;
	}

	/**
	 * @param string
	 */
	public void setSubmitClone(String string)
	{
		submitClone = string;
	}

	/**
	 * @return
	 */
	public String getSubmitShowDone()
	{
		return submitShowDone;
	}

	/**
	 * @param string
	 */
	public void setSubmitShowDone(String string)
	{
		submitShowDone = string;
	}

	/**
	 * @return
	 */
	public String getCurrentTranType()
	{
		return currentTranType;
	}

	/**
	 * @param string
	 */
	public void setCurrentTranType(String string)
	{
		currentTranType = string;
	}

	/**
	 * @return
	 */
	public String getCurrentConfigId()
	{
		return currentConfigId;
	}

	/**
	 * @param string
	 */
	public void setCurrentConfigId(String string)
	{
		currentConfigId = string;
	}

}
