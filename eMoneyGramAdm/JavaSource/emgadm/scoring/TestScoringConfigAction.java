package emgadm.scoring;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.services.TransactionScoring;
import emgadm.services.TransactionService;
import emgadm.util.StringHelper;
import emgshared.model.CodeDescription;
import emgshared.model.ConsumerStatus;
import emgshared.model.ScoreCategory;
import emgshared.model.ScoreConfiguration;
import emgshared.services.ConsumerProfileService;
import emgshared.services.ScoringService;
import emgshared.services.ServiceFactory;

/**
 * @version 1.0
 * @author
 */
public class TestScoringConfigAction extends EMoneyGramAdmBaseAction {
	public ActionForward execute(ActionMapping mapping, ActionForm strutsform, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionErrors errors = new ActionErrors();
		TestScoringConfigForm form = (TestScoringConfigForm) strutsform;
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		ConsumerProfileService cps = sf.getConsumerProfileService();
		emgadm.services.ServiceFactory adminSf = emgadm.services.ServiceFactory.getInstance();
		TransactionScoring tscore = adminSf.getTransactionScoring();
		ScoringService ss = sf.getScoringService();

		if (request.getAttribute("configId") != null) {
			String configId = (String) request.getAttribute("configId");
			Collection chosenConfigs = ss.getScoreConfigurations(up.getUID(), configId, null);
			for (Iterator it = chosenConfigs.iterator(); it.hasNext();) {
				ScoreConfiguration scoreConfiguration = (ScoreConfiguration) it.next();
				int totalWeights = scoreConfiguration.getGetTotalWeight();
				NumberFormat nf = NumberFormat.getNumberInstance();
				nf.setMaximumFractionDigits(2);
				for (Iterator it1 = scoreConfiguration.getScoreCategories().values().iterator(); it1.hasNext();) {
					ScoreCategory scoreCategory = (ScoreCategory) it1.next();
					if (scoreCategory.isIncluded())
						scoreCategory.setWeightPercentage(nf.format((scoreCategory.getWeight() * 100.00) / totalWeights));
				}
				request.getSession().setAttribute("scoreConfiguration", scoreConfiguration);
				ArrayList<Object> arCategories = new ArrayList<Object>();
				final int NUMBER_OF_CATEGORIES = ScoreCategory.catList.length;
				for (int i = 0; i < NUMBER_OF_CATEGORIES; ++i) {
					arCategories.add(i, scoreConfiguration.getScoreCategories().get(ScoreCategory.catList[i]));
				}
				request.getSession().setAttribute("scoreCategories", arCategories);
				break;
			}
		}
		// Boolean List
		ArrayList<CodeDescription> bList = new ArrayList<CodeDescription>();
		bList.add(new CodeDescription("True", "True"));
		bList.add(new CodeDescription("False", "False"));
		Collection<CodeDescription> booleanList = bList;
		request.setAttribute("booleanList", booleanList);
		// Consumer profile status - This will be coming from database
		// ArrayList sList = new ArrayList();
		Collection statusList = cps.getConsumerSubStatuses(true);
		request.setAttribute("statusList", statusList);
		// Done button Clicked
		if (!StringHelper.isNullOrEmpty(form.getSubmitTestDone())) {
			request.getSession().removeAttribute("scoreConfiguration");
			request.getSession().removeAttribute("scoreCategories");
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SAVE_SCORING_CONFIG);
		}
		// Score button Clicked
		if (!StringHelper.isNullOrEmpty(form.getSubmitTestScore())) {
			// Validating the entries
			int testTranId = 0;

			if (StringHelper.isNullOrEmpty(form.getResult1())
					|| StringHelper.isNullOrEmpty(form.getResult2())
					||
					// Profile Status value must be selected, otherwise an
					// exception will occur
					"None".equalsIgnoreCase(form.getResult2()) || StringHelper.isNullOrEmpty(form.getResult5())
					|| StringHelper.isNullOrEmpty(form.getResult6()) || StringHelper.isNullOrEmpty(form.getResult7())
					|| StringHelper.isNullOrEmpty(form.getResult8()) || StringHelper.isNullOrEmpty(form.getResult9())
					|| StringHelper.isNullOrEmpty(form.getResult10()) || StringHelper.isNullOrEmpty(form.getResult11())
					|| StringHelper.isNullOrEmpty(form.getResult12())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.no.value"));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			}

			if (StringHelper.isNullOrEmpty(form.getResult3()) || StringHelper.isNullOrEmpty(form.getResult4()) ||
				StringHelper.isNullOrEmpty(form.getResult13())) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.no.value"));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
			} else {
				try {
					Integer.parseInt(form.getResult3());
					Integer.parseInt(form.getResult4());
					Integer.parseInt(form.getResult13());
				} catch (NumberFormatException nfe) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.scoring.invalid.number"));
					saveErrors(request, errors);
					return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
				}
			}

			Map<String,Object> catMap = new HashMap<String,Object>();
			catMap.put("Tran", null);

			if (form.getResult1().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[0], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[0], new Boolean(false));
			}

			ConsumerStatus cs = ConsumerStatus.getInstance("ACT", form.getResult2());
			catMap.put(ScoreCategory.catList[1], cs);
			catMap.put(ScoreCategory.catList[2], new Integer(form.getResult3()));
			catMap.put(ScoreCategory.catList[3], new Integer(form.getResult4()));

			if (form.getResult5().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[4], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[4], new Boolean(false));
			}
			if (form.getResult6().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[5], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[5], new Boolean(false));
			}
			if (form.getResult7().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[6], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[6], new Boolean(false));
			}
			if (form.getResult8().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[7], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[7], new Boolean(false));
			}
			if (form.getResult9().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[8], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[8], new Boolean(false));
			}
			if (form.getResult10().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[9], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[9], new Boolean(false));
			}
			if (form.getResult11().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[10], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[10], new Boolean(false));
			}
			if (form.getResult12().equalsIgnoreCase("True")) {
				catMap.put(ScoreCategory.catList[11], new Boolean(true));
			} else {
				catMap.put(ScoreCategory.catList[11], new Boolean(false));
			}

			catMap.put(ScoreCategory.catList[12], new Integer(form.getResult13()));
			catMap.put(ScoreCategory.catList[13], new Integer(form.getResult14()));
			catMap.put(ScoreCategory.catList[14], new Integer(form.getResult15()));
			Map results = tscore.getTransactionScore(up.getUID(), testTranId,
					(ScoreConfiguration) request.getSession().getAttribute("scoreConfiguration"), catMap);
			String[] labels = {"PasswordCategory", "ProfileCategory", "ChargeBackCategory", "DaysSinceCategory", "EmailCategory",
					"CountryThresholdCategory", "ReceiveStateCategory", "SeniorIdCategory", "ZipStateCategory", "AreaStateCategory",
					"NegativeAddressCategory", "EmailKeywordCategory", "TransMonitorCategory", "SendAmountCategory", "TransactionAmountCategory"};

			for (int i = 0; i < labels.length; ++i) {
				request.setAttribute(labels[i], results.get(ScoreCategory.catList[i]));
			}

			request.setAttribute("TransactionScoreResult", results.get("Transaction Score Result"));
		}
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
