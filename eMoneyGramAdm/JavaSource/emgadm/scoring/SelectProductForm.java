package emgadm.scoring;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class SelectProductForm extends EMoneyGramAdmBaseValidatorForm
{

	private String submitAutomation = null;
	private String submitScoringConfig = null;
	private String tranType = null;
	private String submitTest = null;
	private String hiddenProductAction = null;
	private String hiddenProductConfigId = null;
	private String hiddenProductTranTypeCode = null;
	private String hiddenProductTranTypeDesc = null;
	private Integer maximumNumberAutomatedPerPeriod = null;
	private Integer automationPeriodInHrs = null;
	private String submitSaveAutomationParms = null;
	private String partnerSiteId = null;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		
		tranType = null;
		submitAutomation = null;
		submitScoringConfig = null;
		maximumNumberAutomatedPerPeriod = null;
		automationPeriodInHrs = null;
		submitSaveAutomationParms = null;

	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		if (submitScoringConfig != null){
		} 			

		if (submitAutomation != null){
		} 			

		return errors;
	}


	/**
	 * @return
	 */
	public String getSubmitAutomation()
	{
		return submitAutomation;
	}

	/**
	 * @return
	 */
	public String getSubmitScoringConfig()
	{
		return submitScoringConfig;
	}

	/**
	 * @param string
	 */
	public void setSubmitAutomation(String string)
	{
		submitAutomation = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitScoringConfig(String string)
	{
		submitScoringConfig = string;
	}

	/**
	 * @return
	 */
	public String getTranType()
	{
		return tranType;
	}

	/**
	 * @param string
	 */
	public void setTranType(String string)
	{
		tranType = string;
	}

	/**
	 * @return
	 */
	public String getSubmitTest()
	{
		return submitTest;
	}

	/**
	 * @param string
	 */
	public void setSubmitTest(String string)
	{
		submitTest = string;
	}

	/**
	 * @return
	 */
	public String getHiddenProductAction()
	{
		return hiddenProductAction;
	}

	/**
	 * @return
	 */
	public String getHiddenProductConfigId()
	{
		return hiddenProductConfigId;
	}

	/**
	 * @param string
	 */
	public void setHiddenProductAction(String string)
	{
		hiddenProductAction = string;
	}

	/**
	 * @param string
	 */
	public void setHiddenProductConfigId(String string)
	{
		hiddenProductConfigId = string;
	}

	/**
	 * @return
	 */
	public String getHiddenProductTranTypeCode()
	{
		return hiddenProductTranTypeCode;
	}

	/**
	 * @return
	 */
	public String getHiddenProductTranTypeDesc()
	{
		return hiddenProductTranTypeDesc;
	}

	/**
	 * @param string
	 */
	public void setHiddenProductTranTypeCode(String string)
	{
		hiddenProductTranTypeCode = string;
	}

	/**
	 * @param string
	 */
	public void setHiddenProductTranTypeDesc(String string)
	{
		hiddenProductTranTypeDesc = string;
	}

    /**
     * @return Returns the automationPeriodInHrs.
     */
    public Integer getAutomationPeriodInHrs() {
        return automationPeriodInHrs;
    }
    
    /**
     * @param automationPeriodInHrs The automationPeriodInHrs to set.
     */
    public void setAutomationPeriodInHrs(Integer integerValue) {
        this.automationPeriodInHrs = integerValue;
    }
    
    /**
     * @return Returns the maximumNumberAutomatedPerPeriod.
     */
    public Integer getMaximumNumberAutomatedPerPeriod() {
        return maximumNumberAutomatedPerPeriod;
    }
    
    /**
     * @param maximumNumberAutomatedPerPeriod The maximumNumberAutomatedPerPeriod to set.
     */
    public void setMaximumNumberAutomatedPerPeriod(Integer integerValue) {
        this.maximumNumberAutomatedPerPeriod = integerValue;
    }

    /**
     * @return Returns the submitSaveAutomationParms.
     */
    public String getSubmitSaveAutomationParms() {
        return submitSaveAutomationParms;
    }
    
    /**
     * @param submitSaveAutomationParms The submitSaveAutomationParms to set.
     */
    public void setSubmitSaveAutomationParms(String submitSaveAutomationParms) {
        this.submitSaveAutomationParms = submitSaveAutomationParms;
    }

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}
}
