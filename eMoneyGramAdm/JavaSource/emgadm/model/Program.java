package emgadm.model;

public interface Program {

	String getId();

	void setId(String id);

	String getName();

	void setName(String name);

	String getDescription();

	void setDescription(String description);

}
