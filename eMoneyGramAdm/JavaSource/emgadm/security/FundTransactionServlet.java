/*
 * Created on Feb 18, 2005
 *
 */
package emgadm.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgshared.services.CreditCardServiceResponse;
import emgshared.services.CyberSourceResponse;

/**
 * @author A131
 *
 */
public class FundTransactionServlet extends HttpServlet
{
	protected void doGet(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException
	{
		String tranIdText = request.getParameter("tranId");
		String userLogonId = request.getParameter("userLogonId");

		response.setContentType("text/plain");
		PrintWriter pw = new PrintWriter(response.getOutputStream());

		TransactionService transactionService =
			ServiceFactory.getInstance().getTransactionService();
		/*try
		{
			int tranId = Integer.parseInt(tranIdText);
			transactionService.requestOwnership(tranId, userLogonId);
			CreditCardServiceResponse serviceResponse =
				transactionService.executeCreditCardAuth(tranId, userLogonId, request.getContextPath());
			transactionService.releaseOwnership(tranId, userLogonId);
			if (serviceResponse != null)
			{
				pw.println(serviceResponse.getIdentifier());
				if (serviceResponse.isAccepted())
				{
					pw.println(CyberSourceResponse.ACCEPTED);
				} else if (serviceResponse.isRejected())
				{
					pw.println(CyberSourceResponse.REJECTED);
				} else
				{
					pw.println(CyberSourceResponse.ERROR);
				}
				pw.println(serviceResponse.getReason());
			} else
			{
				pw.println("-1");
				pw.println(CyberSourceResponse.ERROR);
				pw.println("Unexpected error");
			}
		} catch (Throwable t)
		{
			pw.println("-1");
			pw.println(CyberSourceResponse.ERROR);
			pw.println(t.getMessage());
		} finally
		{
			pw.close();
		}*/
	}

	protected void doPost(
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException, IOException
	{
		doGet(request, response);
	}
}
