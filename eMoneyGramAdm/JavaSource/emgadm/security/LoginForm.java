package emgadm.security;

import javax.servlet.http.HttpServletRequest;

//  import emgadm.dataaccessors.EMoneyGramAdmLogger;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.validator.ValidatorForm;

import emgshared.dataaccessors.EMGSharedLogger;

public class LoginForm extends ValidatorForm {

    private String userID = null;

    private String userPassword = null;

    private String submit = null;

    private boolean sessionExpired;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String u) {
        this.userID = u;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String u) {
        this.userPassword = u;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String s) {
        this.submit = s;
    }

    public boolean isSessionExpired() {
        return sessionExpired;
    }

    public void setSessionExpired(boolean b) {
        sessionExpired = b;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {

        // Reset values are provided as samples only. Change as appropriate.

        userID = null;
        userPassword = null;
        submit = null;
        sessionExpired = false;

    }

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		super.validate(mapping, request);
		ActionErrors errors  = new ActionErrors();
		if (this.getSubmit() != null) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.debug("validating the login form...");
			errors = super.validate(mapping, request);
			if (errors.size() != 0) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.error("login form errors: " + errors.size());
			}
		}

		return errors;
	}
}
