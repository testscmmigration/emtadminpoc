package emgadm.security;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import emgadm.constants.EMoneyGramAdmButtonCommands;
import emgadm.model.ApplicationCommand;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmAppProperties;
import emgshared.model.Transaction;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.DateFormatter;

/**
 *
 * This class is used to establish MoneyGram transaction security permission for
 * controlling displaying of transaction buttons.
 *
 * A user must have the ownership of a transaction first. Once a person owns a
 * transaction, he/she must have permission to execute the action (controlled by
 * LDAP) and finally, this class will turn on the permission base on the current
 * status/substatus of the transaction. Once the permission is turned on, the
 * button will appear on tranaction detail screen. The status/substatus matrix
 * is shown at the setAuth() method.
 *
 */
public class TranSecurityMatrix {

	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	private boolean approve = false; // 1

	private String approveButton;

	private boolean deny = false; // 2

	private String denyButton;

	private boolean undoApprove = false; // 3

	private String undoApproveButton;

	private boolean undoDeny = false; // 4

	private String undoDenyButton;

	private boolean moveToPending = false; // 5

	private String moveToPendingButton;

	private boolean process = false; // 6

	private String processButton;

	private boolean resubmitAch = false; // 11, 12, 13

	private String resubmitAchButton;

	private boolean chargeBackupCreditCard = false; // 21, 22, 23

	private String chargeBackupCreditCardButton;

	private boolean recAchAutoRef = false; // 30

	private String recAchAutoRefButton;

	private boolean recAchCxlRef = false; // 31, 32

	private String recAchCxlRefButton;

	private boolean recAchRefundReq = false; // 38

	private String recAchRefundReqButton;

	private boolean recCcAutoRef = false; // 40

	private String recCcAutoRefButton;

	private boolean recCcCxlRef = false; // 41, 42

	private String recCcCxlRefButton;

	private boolean recordAchChargeback = false; // 43

	private String recordAchChargebackButton;

	private boolean recordCcChargeback = false; // 44

	private String recordCcChargebackButton;

	private boolean recCcRefundReq = false; // 48

	private String recCcRefundReqButton;

	private boolean manWriteOffLoss = false; // 51

	private String manWriteOffLossButton;

	private boolean manRefundFee = false; // 52

	private String manRefundFeeButton;

	private boolean manCollectFee = false; // 53

	private String manCollectFeeButton;

	private boolean manReturnFunding = false; // 54

	private String manReturnFundingButton;

	private boolean autoWriteOffLoss = false; // 55

	private String autoWriteOffLossButton;

	private boolean autoChargebackAndWriteOffLoss = false; // 56

	private String autoChargebackAndWriteOffLossButton;

	private boolean recordLossAdjustment = false; // 61

	private String recordLossAdjustmentButton;

	private boolean recordFeeAdjustment = false; // 62

	private String recordFeeAdjustmentButton;

	private boolean recordConsumerRefundOrRemittanceAdjustment = false; // 63

	private String recordConsumerRefundOrRemittanceAdjustmentButton;

	private boolean recordMgActivityAdjustment = false; // 64

	private String recordMgActivityAdjustmentButton;

	private boolean recordMgCancelRefund = false; // 71

	private String recordMgCancelRefundButton;

	private boolean cancelAchWaiting = false; // 81

	private String cancelAchWaitingButton;

	private boolean recoveryOfLoss = false; // 82

	private String recoveryOfLossButton;

	private boolean manualCorrectingEntry = false; // 83

	// no button for this, it's a hyperlink

	private boolean esMGSend = false; // 84

	private String esMGSendButton;

	private boolean esMGSendCancel = false; // 85

	private String esMGSendCancelButton;

	private boolean recordTranError = false; // 86

	private String recordTranErrorButton;

	private boolean releaseTransaction = false; // 87

	private String releaseTransactionButton;

	private boolean genTranScore = false; // 88

	private String genTranScoreButton;

	private boolean approveProcess = false; // 89

	private String approveProcessButton;

	private boolean retrievalRequest = false; // 160

	private String retrievalRequestButton;

	private boolean cancelBankRqstRefund = false; // 161
	private String cancelBankRqstRefundButton;
	private boolean bankRqstRefund = false; // 162
	private String bankRqstRefundButton;
	private boolean autoCancelBankRqstRefund = false; // 163
	private String autoCancelBankRqstRefundButton;
	private boolean bankRepresentRejDebit = false; // 164
	private String bankRepresentRejDebitButton;
	private boolean bankRejectedDebit = false; // 165
	private String bankRejectedDebitButton;
	private boolean bankRecordVoid = false; // 166
	private String bankRecordVoidButton;
	private boolean bankIssueRefund = false; // 167
	private String bankIssueRefundButton;
	private boolean bankWriteOffLoss = false; // 168
	private String bankWriteOffLossButton;
	private boolean bankRecoverLoss = false; // 169
	private String bankRecoverLossButton;

	private boolean ccPartialRefund = false; //	170
	private String ccPartialRefundButton;
	
	  private boolean undoDSError = false;  // 171
	  private String undoDSErrorButton;
	  private boolean ccManualRefund = false;  // 172
	  private String ccManualRefundButton;
	  private boolean ccManualVoid = false;  // 173
	  private String ccManualVoidButton;
	  
	  




	private UserProfile up; // UserProfile object from LDAP

//	private String statCode; // transaction status code

//	private String fundCode; // transaction sub-status (fund) code

	private String ownerId; // Transaction owner id

//	private String tranType; // Transaction Type

//	private Date createDate; // Transaction create date

//	private Date statusDate; // Transaction status date

//	private int remainingAmt = 0; // Amount remained to be recovered

	// max of 10 buttons shown on the transaction detail screen
	private String[] buttons = { " ", " ", " ", " ", " ", " ", " ", " ", " ", " " };

	private boolean canOwn = false;

	private boolean isOwner = false;

	private String esSendMG = "N";

	private String clearDate = "";

	private DateFormatter df1 = new DateFormatter("yyyyMMdd", true);

	private DateFormatter df2 = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

	boolean[] isFlags;

	/*
	 * Constructor that the caller needs to pass in request object, UserProfile
	 * object, transaction object and amount recovered so far.
	 *
	 * This contructor will set those transaction action to be used for the JSP
	 * pages to show or hide the buttons.
	 *
	 * to create this object and to be used by the JSP pages, codes as follows:
	 *
	 * request.setAttribute("auth", new TranSecurityMatrix(request, up, trans,
	 * null)); where up is User Profile bean and trans is a transaction bean.
	 *
	 */

	public TranSecurityMatrix(HttpServletRequest request, UserProfile up, Transaction transaction,
			BigDecimal recoveredAmount) {

		// get button names from LDAP along with html for submit buttons
		setButtonNames(request);

		this.up = up;
		this.ownerId = transaction.getCsrPrcsUserid();
		if (this.ownerId == null) {
			this.ownerId = "";
		}

		String prefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		if (prefix.length() < this.ownerId.length()
				&& prefix.equalsIgnoreCase(this.ownerId.substring(0, prefix.length() - 1))) {
			this.ownerId = "";
		}

		// check to see if ES Send MG is O.K.
		Map holidayMap = (Map) request.getSession().getServletContext().getAttribute("holidayMap");
		if (df1.format(transaction.getTranStatDate()).compareTo(
				df1.format(emgshared.util.DateFormatter.getAchStatusDay(new Date(System
						.currentTimeMillis()), holidayMap, dynProps.getEsSendWaitDays()))) <= 0) {
			esSendMG = "Y";
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTime(emgshared.util.DateFormatter.getAchSendDay(transaction.getTranStatDate(),
					holidayMap, dynProps.getEsSendWaitDays()));
			int clearTime = dynProps.getEsClearTime();
			cal.set(Calendar.HOUR_OF_DAY, clearTime / 100);
			cal.set(Calendar.MINUTE, clearTime % 100);
			clearDate = df2.format(cal.getTime());
		}

		isFlags = transaction.getCommandFlags(esSendMG, recoveredAmount);

		setAuth();
	}

	public String getOwnerId() {
		return ownerId == null ? "" : ownerId;
	}

	public void setOwnerId(String string) {
		ownerId = string;
	}

	public boolean getCanOwn() {
		return canOwn;
	}

	public void setCanOwn(boolean b) {
		canOwn = b;
	}

	public boolean getIsOwner() {
		return isOwner;
	}

	public void setIsOwner(boolean b) {
		isOwner = b;
	}

	private void setAuth() {

		/***********************************************************************
		 *
		 * Transaction security matrix
		 *
		 * UN: (unfunded transactions) NOF, ACR, ACE, ACC, AIE, CCF, CCR, CCC
		 *
		 * old old reason Role status substatus ====== =========
		 * =========================== ====== 1 FFS,PEN NOF Approve 1,2 2
		 * FFS,PEN NOF Deny 1,2 3 APP NOF Undo Approve 1 4 DEN NOF Undo Deny 1 5
		 * FFS NOF Move to Pending 1,2 6 APP NOF Process 1,2,4
		 *
		 * 11 SEN AIE Resubmit ACH ( with Fee to CC ) 1,2,4 SEN ACR Resubmit ACH (
		 * with Fee to CC ) 1,2,4 SEN ACE Resubmit ACH ( with Fee to CC ) 1,2,4
		 *
		 * 21 SEN AIE CC Sale ( with Fee) 1,2,3,4 SEN ACR CC Sale ( with Fee)
		 * 1,2,3,4 SEN ACE CC Sale ( with Fee) 1,2,3,4
		 *
		 * 30 SEN ACW,ACS Automatic ACH Cancel/Refund 1,2,3,4
		 *
		 * 31 SEN ACS Manual Record ACH Cancel/Refund 1,2,3,4 ERR,CXL ACS Manual
		 * Record ACH Cancel/Refund 1,2,3,4 38 SEN, ARR Record ACH Refund
		 * Requested 1,2,3,4 ERR,CXL
		 *
		 * 40 SEN CCS Automatic CC Cancel/Refund 1,2,3,4 41 SEN CCS Manual
		 * Record CC Cancel/Refund Amount 1,2,3,4 ERR,CXL CCS Manual Record CC
		 * Cancel/Refund Amount 1,2,3,4
		 *
		 * 43 SEN,ERR ACS Record ACH Chargeback/Return 1,2,3,4 APP 44 SEN CCS
		 * Record CC Chargeback 1,2,3,4 48 CXL CRR Record CC Refund Requested
		 * 1,2,3,4
		 *
		 * 51 SEN UN Manual Write-Off Loss 1,3 52 ERR,CXL UN Manual Refund Fee
		 * 1,3 53 SEN ACS,CCS Manual Collect Fee 1,3 54 ERR,CXL ACS,CCS Manual
		 * Return Funding 1,3 55 SEN ACR,CCR Auto Write-Off Loss 1,3 56 SEN
		 * ACS,CCS Auto Chargeback/Write-Off Loss 1
		 *
		 * 61 MAN MAN Record Loss Revenue Adj. 1,3 62 MAN MAN Record Fee Revenue
		 * Adj. 1,3 63 MAN MAN Record Cons Refund/Remit. 1,3 64 MAN MAN Record
		 * MG Transaction Activity 1,3
		 *
		 * 71 SEN ACS,ACC,CCS, Record MG Tran Cancel/Refund 1,2,3 CCF,CCC,CCR
		 *
		 * 81 SEN ACW Cancel ACH Waiting 1,2,3 82 SEN WOL Recovery of Loss 1,3
		 * 83 SEN,ERR * Manual Correcting Entry 1 CXL,MAN
		 *
		 * 84 APP ACS ES MoneyGram Send 1,2,3,4 85 APP ACS Cancel ES MG Send

		 * 1,2,3,4 86 APP CCA Record tran Error 1,2,3,4 87 FFS VBV Release
		 * Transaction 1,2,3,4 88 FFS,APP NOF Generate Transaction Score 1,2,3,4
		 * 89 FFS,PEN NOF Approve and Process 1,2 160 N/A Retrieval Request
		 *
		 * 161 - 169 For Telecheck (BankPaymentService) processing
		 **********************************************************************/

		// Does this user can own a transaction
		if (!up.hasPermission("transOwnership")) {
			return;
		}

		canOwn = true;

		// current user does not own this transaction
		if (!ownerId.equals(up.getUID())) {
			return;
		}

		isOwner = true;

		// LDAP command table
		String[] commands = { "approve", // 1
				"deny", // 2
				"undoApprove", // 3
				"undoDeny", // 4
				"moveToPending", // 5
				"process", // 6
				"resubmitAch", // 11, 12, 13
				"chargeBackupCreditCard", // 21, 22, 23
				"recAchAutoRef", // 30
				"recAchCxlRef", // 31, 32
				"recAchRefundReq", // 38
				"recCcAutoRef", // 40
				"recCcCxlRef", // 41, 42
				"recordAchChargeback", // 43
				"recordCcChargeback", // 44
				"recCcRefundReq", // 48
				"manWriteOffLoss", // 51
				"manRefundFee", // 52
				"manCollectFee", // 53
				"manReturnFunding", // 54
				"autoWriteOffLoss", // 55
				"autoChargebackAndWriteOffLoss", // 56
				"recordLossAdjustment", // 61
				"recordFeeAdjustment", // 62
				"recordConsumerRefundOrRemittanceAdjustment", // 63
				"recordMgActivityAdjustment", // 64
				"recordMgCancelRefund", // 71
				"cancelAchWaiting", // 81
				"recoveryOfLoss", // 82
				"manualCorrectingEntry", // 83
				"esMGSend", // 84
				"esMGSendCancel", // 85
				"recordTranError", // 86
				"releaseTransaction", // 87
				"genTranScore", // 88
				"approveProcess", // 89
				"retrievalRequest" // 160
				,"cancelBankRqstRefund" // 161
				,"bankRqstRefund" // 162
				,"autoCancelBankRqstRefund" // 163
				,"bankRepresentRejDebit" // 164
				,"bankRejectedDebit" // 165
				,"bankRecordVoid" // 166
				,"bankIssueRefund" // 167
				,"bankWriteOffLoss" // 168
				,"bankRecoverLoss" // 169
				,"ccPartialRefund" // 170
				,"undoDSError" // 171
				,"ccManualRefund" // 172
				,"ccManualVoid" // 173 

		};

		for (int i = 0; i < isFlags.length; i++) {

			// check command authorization
			if (!up.hasPermission(commands[i])) {
				isFlags[i] = false;
			}
		}

		int idx = 0;
		int cnt = 0;

		// 1 0
		approve = isFlags[idx++];
		// 2 1
		deny = isFlags[idx++];
		// 3 2
		undoApprove = isFlags[idx++];
		// 4 3
		undoDeny = isFlags[idx++];
		// 5 4
		moveToPending = isFlags[idx++];
		// 6 5
		process = isFlags[idx++];
		// 11, 12, 13 6
		resubmitAch = isFlags[idx++];
		// 21, 22, 23 7
		chargeBackupCreditCard = isFlags[idx++];
		// 30 8
		recAchAutoRef = isFlags[idx++];
		// 31, 32 9
		recAchCxlRef = isFlags[idx++];
		// 38 10
		recAchRefundReq = isFlags[idx++];
		// 40 11
		recCcAutoRef = isFlags[idx++];
		// 41, 42 12
		recCcCxlRef = isFlags[idx++];
		// 43 13
		recordAchChargeback = isFlags[idx++];
		// 44 14
		recordCcChargeback = isFlags[idx++];
		// 48 15
		recCcRefundReq = isFlags[idx++];
		// 51 16
		manWriteOffLoss = isFlags[idx++];
		// 52 17
		manRefundFee = isFlags[idx++];
		// 53 18
		manCollectFee = isFlags[idx++];
		// 54 19
		manReturnFunding = isFlags[idx++];
		// 55 20
		autoWriteOffLoss = isFlags[idx++];
		// 56 21
		autoChargebackAndWriteOffLoss = isFlags[idx++];
		// 61 22
		recordLossAdjustment = isFlags[idx++];
		// 62 23
		recordFeeAdjustment = isFlags[idx++];
		// 63 24
		recordConsumerRefundOrRemittanceAdjustment = isFlags[idx++];
		// 64 25
		recordMgActivityAdjustment = isFlags[idx++];
		// 71 26
		recordMgCancelRefund = isFlags[idx++];
		// 81 27
		cancelAchWaiting = isFlags[idx++];
		// 82 28
		recoveryOfLoss = isFlags[idx++];
		// 83 29
		manualCorrectingEntry = isFlags[idx++];
		// 84 30
		esMGSend = isFlags[idx++];
		// 85 31
		esMGSendCancel = isFlags[idx++];
		// 86 32
		recordTranError = isFlags[idx++];
		// 87 33
		releaseTransaction = isFlags[idx++];
		// 88 34
		genTranScore = isFlags[idx++];
		// 89 35
		approveProcess = isFlags[idx++];
		// 160 36
		retrievalRequest = isFlags[idx++];
		// 161 37
		cancelBankRqstRefund = isFlags[idx++];
		// 162 38
		bankRqstRefund = isFlags[idx++];
		// 163 39
		autoCancelBankRqstRefund = isFlags[idx++];
		// 164 40
		bankRepresentRejDebit = isFlags[idx++];
		// 165 41
		bankRejectedDebit = isFlags[idx++];
		// 166 42
		bankRecordVoid = isFlags[idx++];
		// 167 43
		bankIssueRefund = isFlags[idx++];
		// 168 44
		bankWriteOffLoss = isFlags[idx++];
		// 169 45
		bankRecoverLoss = isFlags[idx++];
		// 170 56
		ccPartialRefund = isFlags[idx++];
		// 171 47
		undoDSError = isFlags[idx++];
		// 172 48
		ccManualRefund = isFlags[idx++];
		// 173 49
		ccManualVoid = isFlags[idx++];    


		if (genTranScore) {
			buttons[cnt++] = genTranScoreButton;
		}

		if (approve) {
			buttons[cnt++] = approveButton;
		}

		if (approveProcess) {
			buttons[cnt++] = approveProcessButton;
		}

		if (deny) {
			buttons[cnt++] = denyButton;
		}

		if (undoApprove) {
			buttons[cnt++] = undoApproveButton;
		}

		if (undoDeny) {
			buttons[cnt++] = undoDenyButton;
		}

		if (moveToPending) {
			buttons[cnt++] = moveToPendingButton;
		}

		if (process) {
			buttons[cnt++] = processButton;
		}

		if (recAchAutoRef) {
			buttons[cnt++] = recAchAutoRefButton;
		}

		if (recCcAutoRef) {
			buttons[cnt++] = recCcAutoRefButton;
		}

		if (recordTranError) {
			buttons[cnt++] = recordTranErrorButton;
		}

		if (releaseTransaction) {
			buttons[cnt++] = releaseTransactionButton;
		}

		if (bankWriteOffLoss) {
			buttons[cnt++] = bankWriteOffLossButton;
		}

		if (autoCancelBankRqstRefund) {
			buttons[cnt++] = autoCancelBankRqstRefundButton;
		}

		if (bankRepresentRejDebit) {
			buttons[cnt++] = bankRepresentRejDebitButton;
		}
		if (bankRejectedDebit) {
			buttons[cnt++] = bankRejectedDebitButton;
		}
		
		if (recCcCxlRef) {
			buttons[cnt++] = recCcCxlRefButton;
		}


		// if automatic refund button is there, nothing else should be there
		// Analyzed the defect as per ALM Defect Id: 1126 and removed the !recCcAutoRef literal
		
		if (!recAchAutoRef && !autoCancelBankRqstRefund) {
			if (cancelBankRqstRefund) {
				buttons[cnt++] = cancelBankRqstRefundButton;
			}

			if (bankRqstRefund) {
				buttons[cnt++] = bankRqstRefundButton;
			}

			if (bankRecordVoid) {
				buttons[cnt++] = bankRecordVoidButton;
			}
			if (bankIssueRefund) {
				buttons[cnt++] = bankIssueRefundButton;
			}
			if (bankRecoverLoss) {
				buttons[cnt++] = bankRecoverLossButton;
			}

			if (resubmitAch) {
				buttons[cnt++] = resubmitAchButton;
			}

			if (chargeBackupCreditCard) {
				buttons[cnt++] = chargeBackupCreditCardButton;
			}

			if (recAchCxlRef) {
				buttons[cnt++] = recAchCxlRefButton;
			}

			if (recAchRefundReq) {
				buttons[cnt++] = recAchRefundReqButton;
			}

			if (recordAchChargeback) {
				buttons[cnt++] = recordAchChargebackButton;
			}

			if (recordCcChargeback) {
				buttons[cnt++] = recordCcChargebackButton;
			}

			if (recCcRefundReq) {
				buttons[cnt++] = recCcRefundReqButton;
			}

			if (manWriteOffLoss) {
				buttons[cnt++] = manWriteOffLossButton;
			}

			if (manRefundFee) {
				buttons[cnt++] = manRefundFeeButton;
			}

			if (manCollectFee) {
				buttons[cnt++] = manCollectFeeButton;
			}

			if (manReturnFunding) {
				buttons[cnt++] = manReturnFundingButton;
			}

			if (autoWriteOffLoss) {
				buttons[cnt++] = autoWriteOffLossButton;
			}

			if (autoChargebackAndWriteOffLoss) {
				buttons[cnt++] = autoChargebackAndWriteOffLossButton;
			}

			if (recordLossAdjustment) {
				buttons[cnt++] = recordLossAdjustmentButton;
			}

			if (recordFeeAdjustment) {
				buttons[cnt++] = recordFeeAdjustmentButton;
			}

			if (recordConsumerRefundOrRemittanceAdjustment) {
				buttons[cnt++] = recordConsumerRefundOrRemittanceAdjustmentButton;
			}

			if (recordMgActivityAdjustment) {
				buttons[cnt++] = recordMgActivityAdjustmentButton;
			}

			if (recordMgCancelRefund) {
				buttons[cnt++] = recordMgCancelRefundButton;
			}

			if (cancelAchWaiting) {
				buttons[cnt++] = cancelAchWaitingButton;
			}

			if (recoveryOfLoss) {
				buttons[cnt++] = recoveryOfLossButton;
			}

			if (retrievalRequest) {
				buttons[cnt++] = retrievalRequestButton;
			}

			if (esMGSend) {
				buttons[cnt++] = esMGSendButton;
			}

			if (esMGSendCancel) {
				buttons[cnt++] = esMGSendCancelButton;
			}
			
			if (ccManualRefund) {
				buttons[cnt++] = ccManualRefundButton;
			}
			if (ccManualVoid) {
				buttons[cnt++] = ccManualVoidButton;
			}  
		    if (undoDSError) {
		        buttons[cnt++] = undoDSErrorButton;
    	      }    


		}

	}

	private void setButtonNames(HttpServletRequest request) {
		String contextRoot = request.getContextPath();

		Map<String, ApplicationCommand> map = (Map<String, ApplicationCommand>) request.getSession().getServletContext().getAttribute("commandMap");
		approveButton = // 1
		"<input type='submit' class='transbtn1' name='submitApprove' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.APPROVE_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.APPROVE_COMMAND
				+ "' target='Button'>help</a>";

		denyButton = // 2
		"<input type='submit' class='transbtn1' name='submitDeny' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.DENY_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.DENY_COMMAND
				+ "' target='Button'>help</a>";

		undoApproveButton = // 3
		"<input type='submit' class='transbtn1' name='submitUndoApprove' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.UNDO_APPROVE_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.UNDO_APPROVE_COMMAND
				+ "' target='Button'>help</a>";

		undoDenyButton = // 4
		"<input type='submit' class='transbtn1' name='submitUndoDeny' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.UNDO_DENY_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.UNDO_DENY_COMMAND
				+ "' target='Button'>help</a>";

		moveToPendingButton = // 5
		"<input type='submit' class='transbtn1' name='submitMoveToPending' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.MOVE_TO_PENDING_COMMAND)).getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='" + contextRoot + "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.MOVE_TO_PENDING_COMMAND
				+ "' target='Button'>help</a>";

		processButton = // 6
		"<input type='submit' class='transbtn1' name='submitProcess' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.PROCESS_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.PROCESS_COMMAND
				+ "' target='Button'>help</a>";

		resubmitAchButton = // 11, 12, 13
		"<input type='submit' class='transbtn1' name='submitResubmitACH' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RESUBMIT_ACH_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.RESUBMIT_ACH_COMMAND
				+ "' target='Button'>help</a>";

		chargeBackupCreditCardButton = // 21, 22, 23
		"<input type='submit' class='transbtn1' name='submitCcSale' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.CHARGE_BACKUP_CREDIT_CARD_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.CHARGE_BACKUP_CREDIT_CARD_COMMAND
				+ "' target='Button'>help</a>";

		recAchAutoRefButton = // 30
		"<input type='submit' class='transbtn1' name='submitRecAchAutoRef' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.REC_ACH_AUTO_REF_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.REC_ACH_AUTO_REF_COMMAND
				+ "' target='Button'>help</a>";

		recAchCxlRefButton = // 31, 32
		"<input type='submit' class='transbtn1' name='submitRecAchCxlRef' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.REC_ACH_CXL_REF_COMMAND)).getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='" + contextRoot + "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.REC_ACH_CXL_REF_COMMAND
				+ "' target='Button'>help</a>";

		recAchRefundReqButton = // 38
		"<input type='submit' class='transbtn1' name='submitRecAchRefundReq' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.REC_ACH_REFUND_REQ_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.REC_ACH_REFUND_REQ_COMMAND
				+ "' target='Button'>help</a>";

		recCcAutoRefButton = // 40
		"<input type='submit' class='transbtn1' name='submitRecCcAutoRef' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.REC_CC_AUTO_REF_COMMAND)).getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='" + contextRoot + "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.REC_CC_AUTO_REF_COMMAND
				+ "' target='Button'>help</a>";

		recCcCxlRefButton = // 41, 42
		"<input type='submit' class='transbtn1' name='submitRecCcCxlRef' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.REC_CC_CXL_REF_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.REC_CC_CXL_REF_COMMAND
				+ "' target='Button'>help</a>";

		recordAchChargebackButton = // 43
		"<input type='submit' class='transbtn1' name='submitRecordAchChargeback' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.RECORD_ACH_CHARGE_BACK_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_ACH_CHARGE_BACK_COMMAND
				+ "' target='Button'>help</a>";

		recordCcChargebackButton = // 44
		"<input type='submit' class='transbtn1' name='submitRecordCcChargeback' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.RECORD_CC_CHARGE_BACK_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_CC_CHARGE_BACK_COMMAND
				+ "' target='Button'>help</a>";

		recCcRefundReqButton = // 48
		"<input type='submit' class='transbtn1' name='submitRecCcRefundReq' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.REC_CC_REFUND_REQ_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.REC_CC_REFUND_REQ_COMMAND
				+ "' target='Button'>help</a>";

		manWriteOffLossButton = // 51
		"<input type='submit' class='transbtn1' name='submitManWriteOffLoss' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.MAN_WRITE_OFF_LOSS_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.MAN_WRITE_OFF_LOSS_COMMAND
				+ "' target='Button'>help</a>";

		manRefundFeeButton = // 52
		"<input type='submit' class='transbtn1' name='submitManRefundFee' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.MAN_REFUND_FEE_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.MAN_REFUND_FEE_COMMAND
				+ "' target='Button'>help</a>";

		manCollectFeeButton = // 53
		"<input type='submit' class='transbtn1' name='submitManCollectFee' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.MAN_COLLECT_FEE_COMMAND)).getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='" + contextRoot + "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.MAN_COLLECT_FEE_COMMAND
				+ "' target='Button'>help</a>";

		manReturnFundingButton = // 54
		"<input type='submit' class='transbtn1' name='submitManReturnFunding' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.MAN_RETURN_FUNDING_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.MAN_RETURN_FUNDING_COMMAND
				+ "' target='Button'>help</a>";

		autoWriteOffLossButton = // 55
		"<input type='submit' class='transbtn1' name='submitAutoWriteOffLoss' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.AUTO_WRITE_OFF_LOSS_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.AUTO_WRITE_OFF_LOSS_COMMAND
				+ "' target='Button'>help</a>";

		autoChargebackAndWriteOffLossButton = // 56
		"<input type='submit' class='transbtn1' name='submitAutoChargebackAndWriteOffLoss' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.AUTO_CHARGE_BACK_AND_WRITE_OFF_LOSS_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.AUTO_CHARGE_BACK_AND_WRITE_OFF_LOSS_COMMAND
				+ "' target='Button'>help</a>";

		recordLossAdjustmentButton = // 61
		"<input type='submit' class='transbtn1' name='submitRecordLossAdjustment' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_LOSS_ADJUSTMENT_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_LOSS_ADJUSTMENT_COMMAND
				+ "' target='Button'>help</a>";

		recordFeeAdjustmentButton = // 62
		"<input type='submit' class='transbtn1' name='submitRecordFeeAdjustment' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_FEE_ADJUSTMENT_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_FEE_ADJUSTMENT_COMMAND
				+ "' target='Button'>help</a>";

		recordConsumerRefundOrRemittanceAdjustmentButton = // 63
		"<input type='submit' class='transbtn1' name='submitRecordConsumerRefundOrRemittanceAdjustment' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT_COMMAND))
						.getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='"
				+ contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT_COMMAND
				+ "' target='Button'>help</a>";

		recordMgActivityAdjustmentButton = // 64
		"<input type='submit' class='transbtn1' name='submitRecordMgActivityAdjustment' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_MG_ACTIVITY_ADJUSTMENT_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_MG_ACTIVITY_ADJUSTMENT_COMMAND
				+ "' target='Button'>help</a>";

		recordMgCancelRefundButton = // 71
		"<input type='submit' class='transbtn1' name='submitRecordMgCancelRefund' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_MG_CANCEL_REFUND_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.RECORD_MG_CANCEL_REFUND_COMMAND
				+ "' target='Button'>help</a>";

		cancelAchWaitingButton = // 81
		"<input type='submit' class='transbtn1' name='submitCancelAchWaiting' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.CANCEL_ACH_WAITING_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.CANCEL_ACH_WAITING_COMMAND
				+ "' target='Button'>help</a>";

		recoveryOfLossButton = // 82
		"<input type='submit' class='transbtn1' name='submitRecoveryOfLoss' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECOVERY_OF_LOSS_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.RECOVERY_OF_LOSS_COMMAND
				+ "' target='Button'>help</a>";

		esMGSendButton = // 84
		"<input type='submit' class='transbtn1' name='submitEsMGSend' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.ES_MG_SEND_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.ES_MG_SEND_COMMAND
				+ "' target='Button'>help</a>";

		esMGSendCancelButton = // 85
		"<input type='submit' class='transbtn1' name='submitEsMGSendCancel' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.ES_MG_SEND_CANCEL_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.ES_MG_SEND_CANCEL_COMMAND
				+ "' target='Button'>help</a>";

		recordTranErrorButton = // 86
		"<input type='submit' class='transbtn1' name='submitRecordTranError' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RECORD_TRAN_ERROR_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.RECORD_TRAN_ERROR_COMMAND
				+ "' target='Button'>help</a>";

		releaseTransactionButton = // 87
		"<input type='submit' class='transbtn1' name='submitReleaseTransaction' value='"
				+ (map
						.get(EMoneyGramAdmButtonCommands.RELEASE_TRANSACTION_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.RELEASE_TRANSACTION_COMMAND
				+ "' target='Button'>help</a>";

		genTranScoreButton = // 88
		"<input type='submit' class='transbtn1' name='submitGenTranScore' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.GEN_TRAN_SCORE_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.GEN_TRAN_SCORE_COMMAND
				+ "' target='Button'>help</a>";

		approveProcessButton = // 89
		"<input type='submit' class='transbtn1' name='submitApproveProcess' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.APPROVE_PROCESS_COMMAND)).getDisplayName()
				+ "' />&nbsp; &nbsp;<a href='" + contextRoot + "/ButtonActions.html#"
				+ EMoneyGramAdmButtonCommands.APPROVE_PROCESS_COMMAND
				+ "' target='Button'>help</a>";

		retrievalRequestButton = // 160
		"<input type='submit' class='transbtn1' name='submitRetrievalRequest' value='"
				+ (map.get(EMoneyGramAdmButtonCommands.RETRIEVAL_REQUEST_COMMAND))
						.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
				+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.RETRIEVAL_REQUEST_COMMAND
				+ "' target='Button'>help</a>";

		cancelBankRqstRefundButton = // 161
			"<input type='submit' class='transbtn1' name='submitCancelBankRqstRefund' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.CANCEL_BANK_RQST_REFUND_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.CANCEL_BANK_RQST_REFUND_COMMAND
					+ "' target='Button'>help</a>";

		bankRqstRefundButton = // 162
			"<input type='submit' class='transbtn1' name='submitBankRqstRefund' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_REQUEST_REFUND_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_REQUEST_REFUND_COMMAND
					+ "' target='Button'>help</a>";

		autoCancelBankRqstRefundButton = // 163
			"<input type='submit' class='transbtn1' name='submitAutoCancelBankRqstRefund' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.AUTO_CANCEL_BANK_RQST_REFUND_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.AUTO_CANCEL_BANK_RQST_REFUND_COMMAND
					+ "' target='Button'>help</a>";
		bankRepresentRejDebitButton = // 164
			"<input type='submit' class='transbtn1' name='submitBankRepresentRejDebit' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_REPRESENT_REJ_DEBIT_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_REPRESENT_REJ_DEBIT_COMMAND
					+ "' target='Button'>help</a>";
		bankRejectedDebitButton = // 165
			"<input type='submit' class='transbtn1' name='submitBankRejectedDebit' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_REJECTED_DEBIT_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_REJECTED_DEBIT_COMMAND
					+ "' target='Button'>help</a>";
		bankRecordVoidButton = // 166
			"<input type='submit' class='transbtn1' name='submitBankRecordVoid' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_RECORD_VOID_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_RECORD_VOID_COMMAND
					+ "' target='Button'>help</a>";
		bankIssueRefundButton = // 167
			"<input type='submit' class='transbtn1' name='submitBankIssueRefund' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_ISSUE_REFUND_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_ISSUE_REFUND_COMMAND
					+ "' target='Button'>help</a>";
		bankWriteOffLossButton = // 168
			"<input type='submit' class='transbtn1' name='submitBankWriteOffLoss' value='"
					+ (map.get(EMoneyGramAdmButtonCommands.BANK_WRITE_OFF_LOSS_COMMAND))
							.getDisplayName() + "' />&nbsp; &nbsp;<a href='" + contextRoot
					+ "/ButtonActions.html#" + EMoneyGramAdmButtonCommands.BANK_WRITE_OFF_LOSS_COMMAND
					+ "' target='Button'>help</a>";
		bankRecoverLossButton = // 169
	        "<logic:equal name=\"transactionDetailForm\" property=\"tranSubStatusCode\" value=\"BWL\">" +
	        "<button type='' class='transbtn1 writeOffLossTrigger' name='telecheckAction'>Bank Recovery of Loss</button><br />" +
	        "</logic:equal>";

		ccPartialRefundButton = // 170
			"<input type='submit' class='transbtn1 partialRefundTrigger' name='submitCcPartialRefund' value='"
			+ (map.get(EMoneyGramAdmButtonCommands.CREDIT_CARD_PARTIAL_REFUND))
					.getDisplayName() + "' />";

	    ccManualRefundButton = // 172
	        "<input type='submit' class='transbtn1' name='submitCcManualRefund' value='"
	        + (map.get(EMoneyGramAdmButtonCommands.CREDIT_CARD_MANUAL_REFUND))
	            .getDisplayName() + "' />";   
	    
	    ccManualVoidButton = // 173
	        "<input type='submit' class='transbtn1' name='submitCcManualVoid' value='"
	        + (map.get(EMoneyGramAdmButtonCommands.CREDIT_CARD_MANUAL_VOID))
	            .getDisplayName() + "' />";   

	    undoDSErrorButton = // 171
	        "<input type='submit' class='transbtn1' name='submitUndoDSError' value='"
	        + (map.get(EMoneyGramAdmButtonCommands.UNDO_DS_ERROR))
	            .getDisplayName() + "' />";   

	}

	public boolean isApprove() {
		return approve;
	}

	public void setApprove(boolean b) {
		approve = b;
	}

	public boolean isDeny() {
		return deny;
	}

	public void setDeny(boolean b) {
		deny = b;
	}

	public boolean isUndoApprove() {
		return undoApprove;
	}

	public void setUndoApprove(boolean b) {
		undoApprove = b;
	}

	public boolean isUndoDeny() {
		return undoDeny;
	}

	public void setUndoDeny(boolean b) {
		undoDeny = b;
	}

	public boolean isMoveToPending() {
		return moveToPending;
	}

	public void setMoveToPending(boolean b) {
		moveToPending = b;
	}

	public boolean isProcess() {
		return process;
	}

	public void setProcess(boolean b) {
		process = b;
	}

	public boolean isResubmitAch() {
		return resubmitAch;
	}

	public void setResubmitAch(boolean b) {
		resubmitAch = b;
	}

	public boolean isChargeBackupCreditCard() {
		return chargeBackupCreditCard;
	}

	public void setChargeBackupCreditCard(boolean b) {
		chargeBackupCreditCard = b;
	}

	public boolean isRecAchCxlRef() {
		return recAchCxlRef;
	}

	public void setRecAchCxlRef(boolean b) {
		recAchCxlRef = b;
	}

	public boolean isRecCcCxlRef() {
		return recCcCxlRef;
	}

	public void setRecCcCxlRef(boolean b) {
		recCcCxlRef = b;
	}

	public boolean isrecordLossAdjustment() {
		return recordLossAdjustment;
	}

	public void setrecordLossAdjustment(boolean b) {
		recordLossAdjustment = b;
	}

	public boolean isrecordFeeAdjustment() {
		return recordFeeAdjustment;
	}

	public void setrecordFeeAdjustment(boolean b) {
		recordFeeAdjustment = b;
	}

	public boolean isrecordConsumerRefundOrRemittanceAdjustment() {
		return recordConsumerRefundOrRemittanceAdjustment;
	}

	public void setrecordConsumerRefundOrRemittanceAdjustment(boolean b) {
		recordConsumerRefundOrRemittanceAdjustment = b;
	}

	public boolean isrecordMgActivityAdjustment() {
		return recordMgActivityAdjustment;
	}

	public void setrecordMgActivityAdjustment(boolean b) {
		recordMgActivityAdjustment = b;
	}

	public boolean isRecordAchChargeback() {
		return recordAchChargeback;
	}

	public void setRecordAchChargeback(boolean b) {
		recordAchChargeback = b;
	}

	public boolean isRecordCcChargeback() {
		return recordCcChargeback;
	}

	public void setRecordCcChargeback(boolean b) {
		recordCcChargeback = b;
	}

	public boolean isManWriteOffLoss() {
		return manWriteOffLoss;
	}

	public void setManWriteOffLoss(boolean b) {
		manWriteOffLoss = b;
	}

	public boolean isManCollectFee() {
		return manCollectFee;
	}

	public void setManCollectFee(boolean b) {
		manCollectFee = b;
	}

	public boolean isManRefundFee() {
		return manRefundFee;
	}

	public void setManRefundFee(boolean b) {
		manRefundFee = b;
	}

	public boolean isManReturnFunding() {
		return manReturnFunding;
	}

	public void setManReturnFunding(boolean b) {
		manReturnFunding = b;
	}

	public boolean isRecordLossAdjustment() {
		return recordLossAdjustment;
	}

	public void setRecordLossAdjustment(boolean b) {
		recordLossAdjustment = b;
	}

	public boolean isRecordFeeAdjustment() {
		return recordFeeAdjustment;
	}

	public void setRecordFeeAdjustment(boolean b) {
		recordFeeAdjustment = b;
	}

	public boolean isRecordConsumerRefundOrRemittanceAdjustment() {
		return recordConsumerRefundOrRemittanceAdjustment;
	}

	public void setRecordConsumerRefundOrRemittanceAdjustment(boolean b) {
		recordConsumerRefundOrRemittanceAdjustment = b;
	}

	public boolean isRecordMgActivityAdjustment() {
		return recordMgActivityAdjustment;
	}

	public void setRecordMgActivityAdjustment(boolean b) {
		recordMgActivityAdjustment = b;
	}

	public boolean isRecordMgCancelRefund() {
		return recordMgCancelRefund;
	}

	public void setRecordMgCancelRefund(boolean b) {
		recordMgCancelRefund = b;
	}

	public void setOwner(boolean b) {
		isOwner = b;
	}

	public boolean isCancelAchWaiting() {
		return cancelAchWaiting;
	}

	public void setCancelAchWaiting(boolean b) {
		cancelAchWaiting = b;
	}

	public boolean isRecCcAutoRef() {
		return recCcAutoRef;
	}

	public void setRecCcAutoRef(boolean b) {
		recCcAutoRef = b;
	}

	public boolean isRecAchAutoRef() {
		return recAchAutoRef;
	}

	public void setRecAchAutoRef(boolean b) {
		recAchAutoRef = b;
	}

	public boolean isRecAchRefundReq() {
		return recAchRefundReq;
	}

	public void setRecAchRefundReq(boolean b) {
		recAchRefundReq = b;
	}

	public boolean isRecCcRefundReq() {
		return recCcRefundReq;
	}

	public void setRecCcRefundReq(boolean b) {
		recCcRefundReq = b;
	}

	public boolean isAutoWriteOffLoss() {
		return autoWriteOffLoss;
	}

	public boolean isRecordTranError() {
		return recordTranError;
	}

	public void setRecordTranError(boolean b) {
		recordTranError = b;
	}

	public void setAutoWriteOffLoss(boolean b) {
		autoWriteOffLoss = b;
	}

	public boolean isAutoChargebackAndWriteOffLoss() {
		return autoChargebackAndWriteOffLoss;
	}

	public void setAutoChargebackAndWriteOffLoss(boolean b) {
		autoChargebackAndWriteOffLoss = b;
	}

	public boolean isRecoveryOfLoss() {
		return recoveryOfLoss;
	}

	public boolean isRetrievalRequest() {
		return retrievalRequest;
	}

	public void setRecoveryOfLoss(boolean b) {
		recoveryOfLoss = b;
	}

	public boolean isManualCorrectingEntry() {
		return manualCorrectingEntry;
	}

	public void setManualCorrectingEntry(boolean b) {
		manualCorrectingEntry = b;
	}

	public boolean isEsMGSend() {
		return esMGSend;
	}

	public void setEsMGSend(boolean b) {
		esMGSend = b;
	}

	public boolean isEsMGSendCancel() {
		return esMGSendCancel;
	}

	public void setEsMGSendCancel(boolean b) {
		esMGSendCancel = b;
	}

	public boolean isCancelBankRqstRefund() {
		return cancelBankRqstRefund;
	}

	public void setCancelBankRqstRefund(boolean cancelBankRqstRefund) {
		this.cancelBankRqstRefund = cancelBankRqstRefund;
	}

	public boolean isBankRqstRefund() {
		return bankRqstRefund;
	}

	public void setBankRqstRefund(boolean bankRqstRefund) {
		this.bankRqstRefund = bankRqstRefund;
	}

	public String getBankRqstRefundButton() {
		return bankRqstRefundButton;
	}

	public void setBankRqstRefundButton(String bankRqstRefundButton) {
		this.bankRqstRefundButton = bankRqstRefundButton;
	}

	public boolean isAutoCancelBankRqstRefund() {
		return autoCancelBankRqstRefund;
	}

	public void setAutoCancelBankRqstRefund(boolean autoCancelBankRqstRefund) {
		this.autoCancelBankRqstRefund = autoCancelBankRqstRefund;
	}

	public String getAutoCancelBankRqstRefundButton() {
		return autoCancelBankRqstRefundButton;
	}

	public void setAutoCancelBankRqstRefundButton(
			String autoCancelBankRqstRefundButton) {
		this.autoCancelBankRqstRefundButton = autoCancelBankRqstRefundButton;
	}

	public boolean isBankRepresentRejDebit() {
		return bankRepresentRejDebit;
	}

	public void setBankRepresentRejDebit(boolean bankRepresentRejDebit) {
		this.bankRepresentRejDebit = bankRepresentRejDebit;
	}

	public String getBankRepresentRejDebitButton() {
		return bankRepresentRejDebitButton;
	}

	public void setBankRepresentRejDebitButton(String bankRepresentRejDebitButton) {
		this.bankRepresentRejDebitButton = bankRepresentRejDebitButton;
	}

	public boolean isBankRejectedDebit() {
		return bankRejectedDebit;
	}

	public void setBankRejectedDebit(boolean bankRejectedDebit) {
		this.bankRejectedDebit = bankRejectedDebit;
	}

	public String getBankRejectedDebitButton() {
		return bankRejectedDebitButton;
	}

	public void setBankRejectedDebitButton(String bankRejectedDebitButton) {
		this.bankRejectedDebitButton = bankRejectedDebitButton;
	}

	public boolean isBankRecordVoid() {
		return bankRecordVoid;
	}

	public void setBankRecordVoid(boolean bankRecordVoid) {
		this.bankRecordVoid = bankRecordVoid;
	}

	public String getBankRecordVoidButton() {
		return bankRecordVoidButton;
	}

	public void setBankRecordVoidButton(String bankRecordVoidButton) {
		this.bankRecordVoidButton = bankRecordVoidButton;
	}

	public boolean isBankIssueRefund() {
		return bankIssueRefund;
	}

	public void setBankIssueRefund(boolean bankIssueRefund) {
		this.bankIssueRefund = bankIssueRefund;
	}

	public String getBankIssueRefundButton() {
		return bankIssueRefundButton;
	}

	public void setBankIssueRefundButton(String bankIssueRefundButton) {
		this.bankIssueRefundButton = bankIssueRefundButton;
	}

	public boolean isBankWriteOffLoss() {
		return bankWriteOffLoss;
	}

	public void setBankWriteOffLoss(boolean bankWriteOffLoss) {
		this.bankWriteOffLoss = bankWriteOffLoss;
	}

	public String getBankWriteOffLossButton() {
		return bankWriteOffLossButton;
	}

	public void setBankWriteOffLossButton(String bankWriteOffLossButton) {
		this.bankWriteOffLossButton = bankWriteOffLossButton;
	}

	public boolean isBankRecoverLoss() {
		return bankRecoverLoss;
	}

	public void setBankRecoverLoss(boolean bankRecoverLoss) {
		this.bankRecoverLoss = bankRecoverLoss;
	}

	public String getBankRecoverLossButton() {
		return bankRecoverLossButton;
	}

	public void setBankRecoverLossButton(String bankRecoverLossButton) {
		this.bankRecoverLossButton = bankRecoverLossButton;
	}

	public boolean isCcPartialRefund() {
		return ccPartialRefund;
	}

	public void setPartialRefund(boolean ccPartialRefund) {
		this.ccPartialRefund = ccPartialRefund;
	}

	public String getPartialRefundButton() {
		return ccPartialRefundButton;
	}

	public void setPartialRefundButton(String ccPartialRefundButton) {
		this.ccPartialRefundButton = ccPartialRefundButton;
	}

	public String getButtons(int i) {
		return buttons[i];
	}

	public String getEsSendMG() {
		return esSendMG;
	}

	public String getClearDate() {
		return clearDate;
	}

	public boolean isCcManualRefund() {
		return ccManualRefund;
	}

	public void setCcManualRefund(boolean ccManualRefund) {
		this.ccManualRefund = ccManualRefund;
	}

	public String getCcManualRefundButton() {
		return ccManualRefundButton;
	}

	public void setCcManualRefundButton(String ccManualRefundButton) {
		this.ccManualRefundButton = ccManualRefundButton;
	}

	public boolean isCcManualVoid() {
		return ccManualVoid;
	}

	public void setCcManualVoid(boolean ccManualVoid) {
		this.ccManualVoid = ccManualVoid;
	}

	public String getCcManualVoidButton() {
		return ccManualVoidButton;
	}

	public void setCcManualVoidButton(String ccManualVoidButton) {
		this.ccManualVoidButton = ccManualVoidButton;
	}

	public boolean isUndoDSError() {
		return undoDSError;
	}

	public void setUndoDSError(boolean undoDSError) {
		this.undoDSError = undoDSError;
	}

	public String getUndoDSErrorButton() {
		return undoDSErrorButton;
	}

	public void setUndoDSErrorButton(String undoDSErrorButton) {
		this.undoDSErrorButton = undoDSErrorButton;
	}

}
