package emgadm.security;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import emgadm.constants.EMoneyGramAdmButtonCommands;
import emgadm.model.ApplicationCommand;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmAppProperties;
import emgshared.model.ConsumerProfile;

/**
 *
 * This class is used to establish MoneyGram consumer profile security permission for
 * controlling displaying of document buttons.
 *
 * A user must have the ownership of a transaction first. Once a person owns a
 * transaction, he/she must have permission to execute the action (controlled by
 * LDAP). Once the permission is turned on, the
 * button will appear on consumer profile screen. The status/substatus matrix
 * is shown at the setAuth() method.
 *
 */
public class ConsumerSecurityMatrix {

	private boolean approveDocument = false; //	171
	private String approveDocumentButton;
	private boolean denyDocument = false;	 //	172
	private String denyDocumentButton;
	private boolean pendDocument = false;	 //	173
	private String pendDocumentButton;


	// 171
	public static final String APPROVE_DOCUMENT = "approveDocument";

	// 172
	public static final String DENY_DOCUMENT = "denyDocument";

	// 173
	public static final String PEND_DOCUMENT = "pendDocument";

	private UserProfile up; // UserProfile object from LDAP

	private String ownerId; // Transaction owner id

	private boolean canOwn = false;

	private boolean owner = false;

	boolean[] isFlags;

	/*
	 * Constructor that the caller needs to pass in request object, UserProfile
	 * object.
	 *
	 * This contructor will set those transaction/document/profile action to be used for the JSP
	 * pages to show or hide the buttons.
	 *
	 * to create this object and to be used by the JSP pages, codes as follows:
	 *
	 * request.setAttribute("auth", new ConsumerSecurityMatrix(request, up)); 
	 * where up is User Profile bean.
	 *
	 */

	public ConsumerSecurityMatrix(HttpServletRequest request, UserProfile up, ConsumerProfile consumer) {

		// get button names from LDAP along with html for submit buttons
		setButtonNames(request);

		this.up = up;
		this.ownerId = "";

		String prefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		if (prefix.length() < this.ownerId.length()
				&& prefix.equalsIgnoreCase(this.ownerId.substring(0, prefix.length() - 1))) {
			this.ownerId = "";
		}

		isFlags = consumer.getCommandFlags();

		// check to see if ES Send MG is O.K.
		setAuth();
	}

	public String getOwnerId() {
		return ownerId == null ? "" : ownerId;
	}

	public void setOwnerId(String string) {
		ownerId = string;
	}

	public boolean getCanOwn() {
		return canOwn;
	}

	public void setCanOwn(boolean b) {
		canOwn = b;
	}

	public boolean isOwner() {
		return owner;
	}

	public void setOwner(boolean b) {
		owner = b;
	}

	private void setAuth() {

		/***********************************************************************
		 *
		 * Transaction security matrix
		 *
		 * UN: (unfunded transactions) NOF, ACR, ACE, ACC, AIE, CCF, CCR, CCC
		 *
		 **********************************************************************/

		// Does this user can own a transaction
		if (!up.hasPermission("transOwnership")) {
			return;
		}

		canOwn = true;

		owner = true;

		// LDAP command table
		String[] commands = {"approveDocument" // 171
				,"denyDocument" // 172
				,"pendDocument" // 173

		};

		for (int i = 0; i < isFlags.length; i++) {

			// check command authorization
			if (!up.hasPermission(commands[i])) {
				isFlags[i] = false;
			}
		}
		int idx=0;
		approveDocument=isFlags[idx++];
		denyDocument=isFlags[idx++];
		pendDocument=isFlags[idx++];

	}

	private void setButtonNames(HttpServletRequest request) {
		Map<String, ApplicationCommand> map = (Map<String, ApplicationCommand>) request.getSession().getServletContext().getAttribute("commandMap");
		approveDocumentButton = 
			"<input type='submit' class='transbtn2' name='submitApproveDocument' value='"
			+ (map.get(APPROVE_DOCUMENT))
					.getDisplayName() + "' />";

		denyDocumentButton = 
			"<input type='submit' class='transbtn2' name='submitDenyDocument' value='"
			+ (map.get(DENY_DOCUMENT))
					.getDisplayName() + "' />";

		pendDocumentButton = 
			"<input type='submit' class='transbtn2' name='submitPendDocument' value='"
			+ (map.get(PEND_DOCUMENT))
					.getDisplayName() + "' />";
	}

	public String getApproveDocumentButton() {
		return approveDocumentButton;
	}

	public void setApproveDocumentButton(String approveDocumentButton) {
		this.approveDocumentButton = approveDocumentButton;
	}

	public String getDenyDocumentButton() {
		return denyDocumentButton;
	}

	public void setDenyDocumentButton(String denyDocumentButton) {
		this.denyDocumentButton = denyDocumentButton;
	}

	public String getPendDocumentButton() {
		return pendDocumentButton;
	}

	public void setPendDocumentButton(String pendDocumentButton) {
		this.pendDocumentButton = pendDocumentButton;
	}

	public boolean isApproveDocument() {
		return approveDocument;
	}

	public void setApproveDocument(boolean approveDocument) {
		this.approveDocument = approveDocument;
	}

	public boolean isDenyDocument() {
		return denyDocument;
	}

	public void setDenyDocument(boolean denyDocument) {
		this.denyDocument = denyDocument;
	}

	public boolean isPendDocument() {
		return pendDocument;
	}

	public void setPendDocument(boolean pendDocument) {
		this.pendDocument = pendDocument;
	}

}
