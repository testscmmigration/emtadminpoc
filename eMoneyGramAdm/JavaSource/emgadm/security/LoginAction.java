package emgadm.security;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.exceptions.ExpiredPasswordException;
import emgadm.exceptions.InactiveUserException;
import emgadm.exceptions.InvalidUserException;
import emgadm.exceptions.LockedUserException;
import emgadm.exceptions.UserAuthenticationException;
import emgadm.model.UserProfile;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.ServiceFactory;
import emgshared.services.UserActivityLogService;

public class LoginAction extends Action {
    public final static String ACTION_PATH = "login";
    public final static String ACTION_PATH_WITH_EXTENSION = ACTION_PATH + ".do";

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ActionErrors errors = new ActionErrors();
        ActionForward forward = new ActionForward();
        // return value
        LoginForm loginForm = (LoginForm) form;

        if (loginForm.isSessionExpired()) {
            errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.session.no.longer.valid"));
            forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_LOGIN_FORM);
        } else if (loginForm.getSubmit() == null) {
            forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_LOGIN_FORM);
        } else {
            HttpSession session = request.getSession();
            String userId = loginForm.getUserID();
            UserProfileManager userManager = null;
            try {
                userManager = ManagerFactory.createUserManager();
                boolean passwordExpired = userManager.isPasswordExpired(userId);
                if (passwordExpired) {
                	throw new ExpiredPasswordException();
                }
                UserProfile up = userManager.authenticate(userId, loginForm.getUserPassword());
                session.setAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE, up);
                userManager.isLoginAuthorized(up);
                insertActivityLog(this.getClass(),request,"<RESULT>Success</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null,userId);
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
            } catch (UserAuthenticationException e) {
                insertActivityLog(this.getClass(),request,"<RESULT>Failure</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null, userId);               
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.login.notauthorized"));
                EMGSharedLogger.getLogger(this.getClass()).error("errors.login.notauthorized (" + userId + ")");
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_FAILURE);
                session.invalidate();
            } catch (LockedUserException e) {
                try {
                    insertActivityLog(this.getClass(),request,"<RESULT>Failure - Locked</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null,userId);
                    //  For audit logging, put the user profile in the session if
                    // possible.
                    if (userManager != null) {
                        UserProfile lockedUserProfile = userManager.getUser(userId);
                        session.setAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE, lockedUserProfile);
                    }
                } catch (Exception ignoreSecondaryException) { }

                //	Decided to always inactive message to user, not locked
                // message.
                insertActivityLog(this.getClass(),request,"<RESULT>Failure - Inactive</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null,userId);
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.login.lockeduser"));
                EMGSharedLogger.getLogger(this.getClass()).error("errors.login.lockeduser(" + userId + ")");
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
                session.invalidate();
            } catch (ExpiredPasswordException e) {
            	insertActivityLog(this.getClass(),request,"<RESULT>Failure - Expired</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null, userId);               
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.login.expiredpassword"));
                EMGSharedLogger.getLogger(this.getClass()).error("errors.login.expiredpassword (" + userId + ")");
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_FAILURE);
                session.invalidate();
            } catch (InvalidUserException e) {
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.login.invaliduser"));
                EMGSharedLogger.getLogger(this.getClass()).error("errors.login.invaliduser(" + userId + ")");
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_FAILURE);
                session.invalidate();
            } catch (InactiveUserException e) {
                insertActivityLog(this.getClass(),request,"<RESULT>Failure - Inactive</RESULT>",UserActivityType.EVENT_TYPE_USERLOGIN,null,userId);
                errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.login.inactiveuser"));
                EMGSharedLogger.getLogger(this.getClass()).error("errors.login.inactiveuser(" + userId + ")");
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ERROR);
                session.invalidate();
            } catch (DataSourceException e) {
                EMGSharedLogger.getLogger(this.getClass()).severe("exception.data.source", e);
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.GLOBAL_FORWARD_SYSTEM_ERROR);
                session.invalidate();
            } catch (Exception e) {
                EMGSharedLogger.getLogger(this.getClass()).severe("Unexpected exception", e);
                forward = mapping.findForward(EMoneyGramAdmForwardConstants.GLOBAL_FORWARD_SYSTEM_ERROR);
                session.invalidate();
            }
        }

        if (errors.isEmpty() == false) {
            saveErrors(request, errors);
        }

        return forward;
    }

//    private boolean isSupportedBrowser(HttpServletRequest request) {
//        String browserKey = "MSIE"; //$NON-NLS-1$
//        String userAgent = request.getHeader("User-Agent").toUpperCase();
//        boolean usingMSIE = (userAgent.indexOf(browserKey) > -1);
//        boolean usingSupportedVersion = false;
//        if (usingMSIE) {
//            Double minMsieVersion = EMTAdmAppProperties.getMinMsieVersion();
//            Double versionNumber = new Double(0);
//            String str = userAgent.substring(userAgent.indexOf(browserKey) + 5);
//            String version = str.substring(0, str.indexOf(";"));
//            versionNumber = new Double(version);
//            usingSupportedVersion = (versionNumber.compareTo(minMsieVersion) >= 0);
//        }
//
//        return (usingMSIE && usingSupportedVersion);
//    }
    
	protected void insertActivityLog(Class loggingClass, HttpServletRequest request, String detailText, String activityTypeCode, String keyValues, String userId)
	{
	    try {
		    ServiceFactory sf = ServiceFactory.getInstance();
	        UserActivityLogService uals = sf.getUserActivityLogService();      
	        UserActivityLog ual = new UserActivityLog();
	        ual.setApplicationName(UserActivityLog.APPLICATION_ADMIN);
	        ual.setDataKeyValue(keyValues);
	        if (detailText == null)
	            ual.setDetailText("");
	        else
	            ual.setDetailText(detailText);
	        ual.setEventDate(Calendar.getInstance().getTime());
	        ual.setIPAddress(request.getRemoteAddr());
	        UserActivityType uat = uals.getUserActivityTypeByCode(activityTypeCode);
	        ual.setUserActivityType(uat);
	        ual.setUserId(userId);
	        uals.insertUserActivityLog("EMGMGI",ual);
            
        } catch (Exception e) {
            EMGSharedLogger.getLogger(loggingClass.getClass().getName().toString()).error(
            "User Activity Log insert failed:" + e.toString());
        }
	}

}
