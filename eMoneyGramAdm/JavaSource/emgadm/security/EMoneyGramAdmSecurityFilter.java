package emgadm.security;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import emgadm.actions.HeartBeatMonAction;
import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.dataaccessors.CommandManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.AuditLogUtils;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;

public class EMoneyGramAdmSecurityFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp,
            FilterChain chain) throws ServletException, IOException,
            java.security.AccessControlException {
        UserProfile user = null;
        RequestDispatcher rd = null;
        HttpServletRequest hreq = (HttpServletRequest) req;
        HttpServletResponse hresp = (HttpServletResponse) resp;

        EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("Received request: " + hreq.getRequestURL().toString());

        if (EMTAdmContainerProperties.isSecureProtocolRequired()) {
            if (isSecureProtocol(req) == false) {
                String securePort = EMTAdmContainerProperties.getSecurePort();
                StringBuffer urlBuffer = new StringBuffer(64);
                urlBuffer.append("https://");
                urlBuffer.append(req.getServerName());
                if (StringHelper.isNullOrEmpty(securePort) == false) {
                    urlBuffer.append(":");
                    urlBuffer.append(securePort);
                }
                urlBuffer.append(hreq.getRequestURI());
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("redirecting request to: " + urlBuffer.toString());
				hresp.sendRedirect(URLEncoder.encode(urlBuffer.toString(), "UTF-8"));
                return;
            }
        }

        String command = AuditLogUtils.getCommand(hreq.getRequestURI());
        command=StringEscapeUtils.escapeJava(command);
        if (hreq.getRequestedSessionId() != null && hreq.isRequestedSessionIdValid() == false) {
            if (LoginAction.ACTION_PATH.equalsIgnoreCase(command) == false && LogoffAction.ACTION_PATH.equalsIgnoreCase(command) == false) {
                if (EMGSharedLogger.getLogger(this.getClass().getName().toString()).isInfoEnabled()) {
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Session (" + hreq.getRequestedSessionId() + ") expired");
                }
                StringBuffer path = new StringBuffer(32);
                path.append(LoginAction.ACTION_PATH_WITH_EXTENSION);
                path.append("?sessionExpired=true");
                rd = req.getRequestDispatcher(path.toString());
                rd.forward(req, resp);
                return;
            }
        }

        if (HeartBeatMonAction.ACTION_PATH.equalsIgnoreCase(command) == false) {
            user = (UserProfile) hreq.getSession().getAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE);

            if (user == null) {
                StringBuffer path = new StringBuffer(32);
                path.append(LoginAction.ACTION_PATH_WITH_EXTENSION);
                if (LoginAction.ACTION_PATH.equalsIgnoreCase(command) == false) {
                    path.append("?sessionExpired=true");
                }
                rd = req.getRequestDispatcher(path.toString());
                rd.forward(req, resp);
                return;
            }
            try {
                if (CommandManager.contains(command)) {
                    ApplicationCommand com = CommandManager.getCommandByCommandName(command);
                    if (com.isAuthorizationRequired()) {
                        if (!user.hasPermission(command)) {
                            EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Attempting to access " + hreq.getRequestURI());
                            EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("No user role is authorized for this request");
                            rd = req.getRequestDispatcher("unAuthorizedAccess.do");
                            rd.forward(req, resp);
                            return;
                        }
                    }
                } else {
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Attempting to access " + hreq.getRequestURI());
                    EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Permissions are not defined for this request");
                    rd = req.getRequestDispatcher("unAuthorizedAccess.do");
                    rd.forward(req, resp);
                }
            } catch (Exception e) {
                EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(e.getMessage(), e);
                e.printStackTrace();
                rd = req.getRequestDispatcher("systemError.do");
                rd.forward(req, resp);
            }
        }

        chain.doFilter(req, resp);
    }

    private boolean isSecureProtocol(ServletRequest req) {
        return ("https".equalsIgnoreCase(req.getScheme()));
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
