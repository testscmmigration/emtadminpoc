package emgadm.util;

import javax.servlet.http.HttpServlet;

import com.moneygram.common.log.log4j.Log4JFactory;

/**
 * 
 * @author G.R.Svenddal
 * 
 * a non-mapped Servlet.
 * Should be placed first in loading order.
 * Will reference the com.moneygram.common.log.log4j system, to give it
 * the opportunity to initialize itself before Struts does the job through 
 * the commons-logging package, bypassing the Moneygram specific 
 * logging initialization. 
 * 
 * ( Previously, the first servlet to load was the Struts Action servlet, 
 * or our subclass of it anyway. While it might be possible to preempt the 
 * Struts logging at that point, it's easier to do it this way, and there's 
 * not much time before this build goes to Q/A. )
 *
 */
public class LogSystemInitialize extends HttpServlet
{

	/**
	 * Constructor of the object.
	 */
	public LogSystemInitialize()
	{
		super();
		Log4JFactory.getInstance();
		// this will trigger the Moneygram Logger initialization.

	}

}
