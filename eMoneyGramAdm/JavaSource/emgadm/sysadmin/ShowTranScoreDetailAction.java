package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.util.StringHelper;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;

public class ShowTranScoreDetailAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ShowTranScoreDetailForm form = (ShowTranScoreDetailForm) f;

		if (!StringHelper.isNullOrEmpty(form.getSubmitReturn()))
		{
			if ("q".equalsIgnoreCase(form.getType()))
			{
				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_SHOW_TRANS_QUEUE);
			} else
			{
				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_TRANSCTION_DETAIL);
			}
		}

		if (StringHelper.isNullOrEmpty(form.getTranId())
			|| StringHelper.containsNonDigits(form.getTranId()))
		{
			throw new EMGRuntimeException("Invalid Transaction Id");
		}

		TransactionManager tm = getTransactionManager(request);
		Transaction tran = null;
		tran = tm.getTransaction(Integer.parseInt(form.getTranId()));

		if (tran == null)
		{
			throw new EMGRuntimeException(
				"Transaction Id " + form.getTranId() + "' does not exist");
		}

		request.setAttribute("tran", tran);
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
