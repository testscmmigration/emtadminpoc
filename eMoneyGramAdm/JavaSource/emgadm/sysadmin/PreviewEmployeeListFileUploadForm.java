package emgadm.sysadmin;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class PreviewEmployeeListFileUploadForm extends EMoneyGramAdmBaseValidatorForm
{
	private String submitUpload = null;
	private String submitCancel = null;
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		return errors;
	}

	/**
	 * @return Returns the submitCancel.
	 */
	public String getSubmitCancel() {
		return submitCancel;
	}
	/**
	 * @param submitCancel The submitCancel to set.
	 */
	public void setSubmitCancel(String submitCancel) {
		this.submitCancel = submitCancel;
	}
	/**
	 * @return Returns the submitUpload.
	 */
	public String getSubmitUpload() {
		return submitUpload;
	}
	/**
	 * @param submitUpload The submitUpload to set.
	 */
	public void setSubmitUpload(String submitUpload) {
		this.submitUpload = submitUpload;
	}
}
