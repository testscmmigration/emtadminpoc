package emgadm.sysadmin;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.property.EMTAdmDynProperties;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.property.EMTSharedDynProperties;

public class EditDynPropertyAction extends EMoneyGramAdmBaseAction
{
	private final static String[] pFiles =
		{
			"emgadm.property.EMTAdmAppProperties",
			"emgadm.property.EMTAdmContainerProperties",
			"emgadm.property.EMTAdmDynProperties",
			"emgshared.property.EMTSharedContainerProperties",
			"emgshared.property.EMTSharedEnvProperties"	};
	
	private final static String[] pTypes =
		{
			"Admin Container",
			"Admin Environment",
			"Dynamic",
			"EMT Shared Container",
			"EMT Shared Environment" };

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		//		ActionErrors errors = new ActionErrors();
		UserProfile up = getUserProfile(request);
		boolean editable = up.hasPermission("updateDynProperty");

		// Get list of all dynamic properties (i.e., props stored in the DB)
		Map propMap = PropertyDAO.getProperties(up.getUID());
		
		// Get list of properties defined on the EMTSharedDynProperties object 
		List propList = getClassProperties(request);

//		List stuff = new ArrayList(propMap.values());
//		Collections.sort(stuff);
//		Iterator temp = stuff.iterator();
//		System.out.println("---------- DB VALUES ----------");
//		while (temp.hasNext()) {
//			PropertyBean propBean = (PropertyBean) temp.next();
//			System.out.println(propBean.getPropKey().getPropName() + ", " +
//					propBean.getPropKey().getPropType() + ", " + 
//					propBean.getPropKey().getPropVal() + ".");
//		}
		
		Iterator iter = propList.iterator();
//		System.out.println("\n\n---------- EMTSHaredDynProperties ----------");
//		while (iter.hasNext()) {
//			TmpBean tempBean = (TmpBean) iter.next();
//			System.out.println(tempBean.getName() + ", " +
//					tempBean.getType() + ", " + 
//					tempBean.getVal() + ".");
//		}

		// Iterate over the EMTSharedDynProperties and find where the method name
		// (minus the "get" or "is") matches the property name, type and value of
		// what we got back from the database.
		while (iter.hasNext())	{
			TmpBean tb = (TmpBean) iter.next();
			PropertyKey pk = new PropertyKey(tb.getName(), tb.getType(), tb.getVal());
			PropertyBean bean = (PropertyBean) propMap.get(pk);
			if (bean == null) {
				propMap.put(pk, new PropertyBean(pk));
			} else {
				bean.setEditable(false);
				bean.setDefinedInClass(true);
				
				if (editable && bean.isDefinedInClass()) {
					bean.setEditable(true);
				}
			}
		}

		List goodList = new ArrayList();
		List inDbList = new ArrayList();
		List inClassList = new ArrayList();
		String saveName = "";

		iter = propMap.values().iterator();
		List pList = new ArrayList();
		while (iter.hasNext()) {
			pList.add(iter.next());
		}
		Collections.sort(pList);
		iter = pList.iterator();

		while (iter.hasNext())	{
			PropertyBean pb = (PropertyBean) iter.next();
			//Only show Dynamic properties (not Container or Resource/Environment properties)
			if (pb.getPropKey().getPropType().equalsIgnoreCase(pTypes[2])) {
				if (pb.isDefinedInClass() && pb.isDefinedInDb()) {
					goodList.add(pb);
					if ("Y".equalsIgnoreCase(pb.getMultivalueFlag())
							&& !saveName.equalsIgnoreCase(pb.getPropKey().getPropName())) {
						goodList.add(addABlankBean(pb));
					}
					saveName = pb.getPropKey().getPropName();
				} else if (!pb.isDefinedInClass()) {
					inDbList.add(pb);
				} else if (!pb.isDefinedInDb()) {
					inClassList.add(pb);
				}
			}
		}
		Collections.sort(goodList);
		Collections.sort(inDbList);
		Collections.sort(inClassList);

		request.getSession().setAttribute("goodList", goodList);
		request.getSession().setAttribute("inDbList", inDbList);
		request.getSession().setAttribute("inClassList", inClassList);

		//		errors = (ActionErrors) request.getAttribute("propErrors");

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private PropertyBean addABlankBean(PropertyBean pb) {
		return new PropertyBean(
			new PropertyKey(
				pb.getPropKey().getPropName(),
				pb.getPropKey().getPropType(),
				"xAdd a New Value Here"),
			pb.getPropDesc(),
			pb.getAdminUseFlag(),
			pb.getMultivalueFlag(),
			pb.getDataTypeCode(),
			pb.getCreateDate(),
			pb.getCreateUserid(),
			pb.getLastUpdateDate(),
			pb.getLastUpdateUserid(),
			pb.isDefinedInDb(),
			pb.isDefinedInClass(),
			pb.isEditable());
	}

	private List getClassProperties(HttpServletRequest request) {
		EMTSharedDynProperties obj = null;

		List propList = new ArrayList();

		try	{
			for (int k = 0; k < pFiles.length; k++)	{
				obj = null;
				Class cls = Class.forName(pFiles[k]);

				//  access thru instance of the object.
				if (k == 2)	{
					obj = EMTAdmDynProperties.getDPO(request);
					cls = obj.getClass();
				}

				//  get a list of all methods for the class
				Method[] methodList = cls.getDeclaredMethods();

				//  get the name of method and get its value		  
				for (int i = 0; i < methodList.length; i++)
				{
					Method method = methodList[i];

					String name = method.getName();
					boolean isGetter = false;

					//  remove prefix from the method name
					if (name.substring(0, 3).equals("get"))	{
						name = name.substring(3);
						isGetter = true;
					} else if (name.substring(0, 2).equals("is")) {
						name = name.substring(2);
						isGetter = true;
					}

					if (isGetter && method.getParameterTypes().length == 0)	{
						String type = pTypes[k];
						String val = null;
						boolean isArray = false;
						String[] arr = null;

						if (method.invoke(obj, null) != null) {
							val = (method.invoke(obj, null)).toString();
							if (val.startsWith("[L")
								|| val.startsWith("[I")
								|| val.startsWith("[D")
								|| val.startsWith("[F"))
							{
								isArray = true;
								if (val.startsWith("[L")) {
									arr = (String[]) method.invoke(obj, null);
								}
								
								if (val.startsWith("[I")) {
									int[] n = (int[]) method.invoke(obj, null);
									arr = new String[n.length];
									for (int j = 0; j < n.length; j++) {
										arr[j] = String.valueOf(n[j]);
									}
								}
								
								if (val.startsWith("[D")) {
									double[] d = (double[]) method.invoke(obj, null);
									arr = new String[d.length];
									for (int j = 0; j < d.length; j++) {
										arr[j] = String.valueOf(d[j]);
									}
								}
								
								if (val.startsWith("[F")) {
									float[] f =	(float[]) method.invoke(obj, null);
									arr = new String[f.length];
									for (int j = 0; j < f.length; j++) {
										arr[j] = String.valueOf(f[j]);
									}
								}
							}

							if (!isArray
								&& (name.toUpperCase().endsWith("PASSWORD") ||
								    name.toUpperCase().indexOf("SECUREHASH") > 0)) {
								val = "********";
							}
						} else	{
							val = "****  Not Define  ****";
						}

						if (!isArray) {
							propList.add(new TmpBean(name, type, val));
						} else {
							for (int m = 0; m < arr.length; m++) {
								propList.add(new TmpBean(name, type, arr[m]));
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return propList;
	}

	public final class TmpBean
	{
		private String name;
		private String type;
		private String val;

		public TmpBean(String name, String type, String val)
		{
			this.name = name;
			this.type = type;
			this.val = val;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String string)
		{
			name = string;
		}

		public String getType()
		{
			return type;
		}

		public void setType(String string)
		{
			type = string;
		}

		public String getVal()
		{
			return val;
		}

		public void setVal(String string)
		{
			val = string;
		}
	}
}