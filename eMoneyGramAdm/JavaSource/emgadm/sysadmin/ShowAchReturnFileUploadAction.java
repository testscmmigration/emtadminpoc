package emgadm.sysadmin;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.model.UploadFile;
import emgadm.util.StringHelper;

/**
 * @version 	1.0
 * @author
 */
public class ShowAchReturnFileUploadAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(ActionMapping mapping,
		ActionForm form, 
		HttpServletRequest request, 
		HttpServletResponse response) throws Exception {

		ServletContext sc = request.getSession().getServletContext();		
//		ActionErrors errors = new ActionErrors();
		
		TransactionManager tm = getTransactionManager(request);
		ArrayList fileControlHistory = new ArrayList();
		
		ShowAchReturnFileUploadForm showAchReturnFileUploadForm = (ShowAchReturnFileUploadForm) form;
//		UserProfile up = getUserProfile(request);

		sc.removeAttribute("uploadFile");
		UploadFile uploadFile = new UploadFile(EMoneyGramAdmApplicationConstants.ACH_RETURN_FILE_UPLOAD,
				                               showAchReturnFileUploadForm.getTheFile());

		fileControlHistory = (ArrayList)tm.getLatestFileControl(EMoneyGramAdmApplicationConstants.ACH_RETURN_FILE_UPLOAD);
		sc.setAttribute("fileControlRecords", fileControlHistory);

		
		if (!StringHelper.isNullOrEmpty(showAchReturnFileUploadForm.getSubmitProcess())){
			sc.setAttribute("uploadFile", uploadFile);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_PREVIEW_ACH_RETURN_FILE_UPLOAD);
		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
