/*
 * Created on Jul 28, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EditNegativeTermListAction extends EMoneyGramAdmBaseAction
{
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm f,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception
		{
			ActionErrors errors = new ActionErrors();
			NegativeTermListForm form = (NegativeTermListForm) f;
			UserProfile up = getUserProfile(request);
			CommandAuth ca = new CommandAuth();
			Collection negativeTermList = new ArrayList();

			if (up.hasPermission(EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADD_NEGATIVE_TERM_LIST)) {
				ca.setAddNegativeTermList(true);
			} else {
				ca.setAddNegativeTermList(false);
			}
			request.getSession().setAttribute("auth", ca);

			ArrayList list = (ArrayList) request.getSession().getServletContext().getAttribute("custPurposeTypes");
					
			form.setPurposeTypes(list);
			
			request.getSession().setAttribute("types", list);
			
			// request to add a new negative string
			if (!StringHelper.isNullOrEmpty(form.getSubmitAdd()))
			{
				//MGO-11644 story changes starts
				//Adding saveToken will add token to he request,which in turn prevents the unauthorized access.
				saveToken(request);
				//MGO-11644 story changes ends
				request.setAttribute(
					"negativeType",
					form.getNegativeType().trim());
				request.setAttribute(
					"negativeTerm",
					form.getNegativeTerm().trim());
				
				return mapping.findForward(
					EMoneyGramAdmForwardConstants
						.LOCAL_FORWARD_ADD_NEGATIVE_TERM_LIST);
			}

			if (!StringHelper.isNullOrEmpty(form.getSubmitShowAll())
				|| !StringHelper.isNullOrEmpty(form.getSubmitSearch()))
			{
				try
				{
					ServiceFactory sf = ServiceFactory.getInstance();
					FraudService fs = sf.getFraudService();

					if (!StringHelper.isNullOrEmpty(form.getSubmitSearch()))
					{
						negativeTermList =
							fs.getNegativeTermList(
								up.getUID(),
								form.getNegativeType(),
								form.getNegativeTerm(), 0);
					} else
					{
						if (!StringHelper.isNullOrEmpty((String) request.getParameter("action")))
						{
							fs.deleteNegativeTerm(up.getUID(),
									(String) request.getParameter("negativeNum"));
							
					        ActionMessages messages = getActionMessages(request);
					        messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage(
					                "message.negativeterm.delete.success", form.getNegativeTerm()));
					        saveMessages(request, messages);
					        form.setNegativeTerm(null);
						}

						negativeTermList = 
							fs.getNegativeTermList(up.getUID(), 
									form.getNegativeType(), 
									null, 0);
					}

					if (negativeTermList == null || negativeTermList.size() == 0)
					{
						System.out.println("negativeTermList is empty");
						errors.add(
							ActionErrors.GLOBAL_ERROR,
							new ActionError("error.negativeterm.no.found"));
						saveErrors(request, errors);
					}

				} 
				catch (TooManyResultException e)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError(
							"error.too.many.results",
							dynProps.getMaxDownloadTransactions() + ""));
					saveErrors(request, errors);
				}
				catch(Exception e) {
					e.printStackTrace();
					throw new Exception(e.toString());
				}

				if (negativeTermList != null)
					Collections.sort((List) negativeTermList);
				request.setAttribute("negativeTermList", negativeTermList);
			}

			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}
}
