/*
 * Created on Jul 28, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.ArrayList;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class NegativeTermListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId = null;
	private String negativeType = null;
	private String negativeTerm = null;
	private String negativeAddress = null;
	private String submitAdd = null;
	private String submitSearch = null;
	private String submitShowAll = null;
	private String submitProcess = null;
	private String submitCancel = null;
	private List purposeTypes = new ArrayList(0);
	
	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		submitAdd = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitSearch)){
			if ((!StringHelper.isNullOrEmpty(negativeTerm) && negativeTerm.length() < 3) ||
					StringHelper.isNullOrEmpty(negativeTerm)) {
				errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.negativeterm.length.limit"));
			}
		}
		
		if (!StringHelper.isNullOrEmpty(submitProcess)) {
			if (StringHelper.isNullOrEmpty(negativeTerm)) {
				errors.add(
					"negativeTerm",
					new ActionError("errors.required", "Negative String"));
			}
		}
		return errors;
	}
	
	/**
	 * @return Returns the negativeTerm.
	 */
	public String getNegativeTerm() {
		return negativeTerm;
	}
	/**
	 * @param negativeTerm The negativeTerm to set.
	 */
	public void setNegativeTerm(String negativeTerm) {
		this.negativeTerm = negativeTerm;
	}
	/**
	 * @return Returns the negativeType.
	 */
	public String getNegativeType() {
		return negativeType;
	}
	/**
	 * @param negativeType The negativeType to set.
	 */
	public void setNegativeType(String negativeType) {
		this.negativeType = negativeType;
	}
	/**
	 * @return Returns the submitSearch.
	 */
	public String getSubmitSearch() {
		return submitSearch;
	}
	/**
	 * @param submitSearch The submitSearch to set.
	 */
	public void setSubmitSearch(String submitSearch) {
		this.submitSearch = submitSearch;
	}
	/**
	 * @return Returns the submitShowAll.
	 */
	public String getSubmitShowAll() {
		return submitShowAll;
	}
	/**
	 * @param submitShowAll The submitShowAll to set.
	 */
	public void setSubmitShowAll(String submitShowAll) {
		this.submitShowAll = submitShowAll;
	}
	/**
	 * @return Returns the submitProcess.
	 */
	public String getSubmitProcess() {
		return submitProcess;
	}
	/**
	 * @param submitProcess The submitProcess to set.
	 */
	public void setSubmitProcess(String submitProcess) {
		this.submitProcess = submitProcess;
	}
	/**
	 * @return Returns the submitCancel.
	 */
	public String getSubmitCancel() {
		return submitCancel;
	}
	/**
	 * @param submitCancel The submitCancel to set.
	 */
	public void setSubmitCancel(String submitCancel) {
		this.submitCancel = submitCancel;
	}
	/**
	 * @return Returns the purposeTypes.
	 */
	public List getPurposeTypes() {
		return purposeTypes;
	}
	/**
	 * @param purposeTypes The purposeTypes to set.
	 */
	public void setPurposeTypes(List purposeTypes) {
		this.purposeTypes = purposeTypes;
	}
	/**
	 * @return Returns the negativeAddress.
	 */
	public String getNegativeAddress() {
		return negativeAddress;
	}
	/**
	 * @param negativeAddress The negativeAddress to set.
	 */
	public void setNegativeAddress(String negativeAddress) {
		this.negativeAddress = negativeAddress;
	}
	/**
	 * @return Returns the custId.
	 */
	public String getCustId() {
		return custId;
	}
	/**
	 * @param custId The custId to set.
	 */
	public void setCustId(String custId) {
		this.custId = custId;
	}
}