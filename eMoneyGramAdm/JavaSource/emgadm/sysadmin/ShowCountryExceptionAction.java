package emgadm.sysadmin;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.EPartnerSite;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.CountryExceptionService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowCountryExceptionAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(ActionMapping mapping,
		ActionForm rawForm,
		HttpServletRequest request,
		HttpServletResponse response) throws Exception {

		ActionErrors errors = new ActionErrors();

		ShowCountryExceptionForm form = (ShowCountryExceptionForm) rawForm;
		UserProfile up = getUserProfile(request);
		setAuth(request, up);
		List excps =  new ArrayList();
		ServiceFactory sf = ServiceFactory.getInstance();
		CountryExceptionService ces = sf.getCountryExceptionService();
		HashMap countryMaster = ces.getCountryMasterList();

		//  go to add a new country excption screen
		if (! StringHelper.isNullOrEmpty(form.getSubmitAdd())) {
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_COUNTRY_EXCEPTION);
		}

		String id1 = request.getParameter("id1");
		if (id1 != null) {
			String mainCountry = request.getParameter("id2");
			ces.deleteCntryException(id1, mainCountry);
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.delete.country.exception", id1));
			saveErrors(request, errors);
			form.setSelectedCountry(mainCountry);
			try {
		    	super.insertActivityLog(this.getClass(),request,null,UserActivityType.EVENT_TYPE_COUNTRYEXCEPTIONSDELETE,id1 + UserActivityLog.dataKeyValueSeperator + mainCountry);
			} catch (Exception e) { }
		}
		Map<String, String> transferMap = (Map<String, String>) request
				.getSession().getAttribute(EditCountryExceptionAction.SELECTED_COUNTRY_OVR);
		String selectedCountryOverride = null;
		if (transferMap != null)
			if (transferMap.containsKey(EditCountryExceptionAction.SELECTED_COUNTRY_OVR))
				selectedCountryOverride = transferMap.get(EditCountryExceptionAction.SELECTED_COUNTRY_OVR);
		
		if (selectedCountryOverride != null) {
			form.setSelectedCountry(selectedCountryOverride);
			request.getSession().removeAttribute(EditCountryExceptionAction.SELECTED_COUNTRY_OVR);
		}
		excps = ces.getCntryExceptions(form.getSelectedCountry());
		if (excps == null || excps.size() == 0) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.county.exception.found"));
			saveErrors(request, errors);
		}

		if (excps.size() > 1) {
			Collections.sort(excps);
		}
		//System.out.println(request.getParameter("selectedPartner"));
		if(!StringHelper.isNullOrEmpty(request.getParameter("selectedCountry"))) {
			String selectedCountry = request.getParameter("selectedCountry");
			if (selectedCountry.equals("GBR")){
				form.setPartnerSiteId(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID);
			}else if (selectedCountry.equals("DEU")){
				form.setPartnerSiteId(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID);
			}
		}
		else {
			form.setPartnerSiteId(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID);
		}

		request.getSession().setAttribute("partnerList", EPartnerSite.values());
		request.setAttribute("excps", excps);
		request.setAttribute("countryMasterList", countryMaster);
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private void setAuth(HttpServletRequest request,UserProfile up) {
		CommandAuth ca = null;
		//  setup a flag to see if the user has update permission
		if (request.getSession().getAttribute("auth") == null) {
			ca = new CommandAuth();
		} else {
			ca = (CommandAuth) request.getSession().getAttribute("auth");
		}

		ca.setEditCntryExcp(false);
		if (up.hasPermission(EMoneyGramAdmForwardConstants.
			LOCAL_FORWARD_EDIT_COUNTRY_EXCEPTION)) {
			ca.setEditCntryExcp(true);
		}

		request.getSession().setAttribute("auth", ca);
	}
}
