package emgadm.sysadmin;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import org.apache.struts.upload.FormFile;

public class ShowEmployeeListFileUploadForm extends EMoneyGramAdmBaseValidatorForm
{
	private String fileType = null;
    private FormFile theFile;
    private String submitProcess;

	/**
	 * @return Returns the submitProcess.
	 */
	public String getSubmitProcess() {
		return submitProcess;
	}
	/**
	 * @param submitProcess The submitProcess to set.
	 */
	public void setSubmitProcess(String submitProcess) {
		this.submitProcess = submitProcess;
	}
    /**
     * @return Returns the theFile.
     */
    public FormFile getTheFile() {
      return theFile;
    }
    /**
     * @param theFile The FormFile to set.
     */
    public void setTheFile(FormFile theFile) {
      this.theFile = theFile;
    }
    
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		submitProcess = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		if (submitProcess != null){
			if (StringHelper.isNullOrEmpty(theFile.getFileName())){ 
				errors.add(ActionErrors.GLOBAL_ERROR, 
					new ActionError("errors.required", "File"));
			}
//			if (theFile.getFileSize() > 40000){
//				errors.add(ActionErrors.GLOBAL_ERROR, 
//						new ActionError("errors.required", "File is larger than 40000 bytes. Cannot be processed"));				
//			}
			
		}		
		return errors;
	}

	/**
	 * @return Returns the fileType.
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType The fileType to set.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
