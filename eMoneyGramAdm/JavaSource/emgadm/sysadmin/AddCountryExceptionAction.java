package emgadm.sysadmin;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import shared.mgo.dto.MGOCountry;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.AgentConnectServiceDoddFrank;
import shared.mgo.services.MGOServiceFactory;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.EPartnerSite;
import emgadm.model.PartnerSite;
import emgshared.model.CountryExceptionBean;
import emgshared.services.CountryExceptionService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddCountryExceptionAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(
				ActionMapping mapping,
				ActionForm rawForm,
				HttpServletRequest request,
				HttpServletResponse response)
			throws Exception {

		AddCountryExceptionForm form = (AddCountryExceptionForm) rawForm;
		ServiceFactory sf = ServiceFactory.getInstance();
		CountryExceptionService ces = sf.getCountryExceptionService();
		AgentConnectServiceDoddFrank agentConnectServiceDoddFrank = MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
		Collection countries = (Collection)agentConnectServiceDoddFrank.getActiveCountryList();
		Collection cntryExcps = ces.getCntryExceptions(form.getSelectedCountry());
		HashMap countryMaster = ces.getCountryMasterList();
		Collection excps = new ArrayList();
		Map tmpMap = new HashMap();
		Iterator iter = cntryExcps.iterator();

		while (iter.hasNext()) {
			CountryExceptionBean ce = (CountryExceptionBean) iter.next();
			tmpMap.put(ce.getRcvIsoCntryCode(), ce);
		}

		iter = countries.iterator();
		while (iter.hasNext()) {
			MGOCountry emtc = (MGOCountry) iter.next();
			if (!tmpMap.containsKey(emtc.getCountryCode())) {
				excps.add(emtc);
			}
		}
		//System.out.println(request.getParameter("selectedPartner"));
		request.getSession().setAttribute("selectedCountry", request.getParameter("selectedCountry"));
		EPartnerSite []partnerList = (EPartnerSite[])request.getSession().getAttribute("partnerList");
		for (EPartnerSite tmp : partnerList){
			if (tmp.getCountry().equals((String)request.getParameter("selectedCountry"))){
				request.getSession().setAttribute("partnerName", EPartnerSite.getPartnerByCountry(tmp.getCountry()).getPartnerSiteDesc());
				request.getSession().setAttribute("partnerCurrency", tmp.getPartnerSiteCurrency());
				break;
			}
		}
		request.setAttribute("excps", excps);
		request.setAttribute("countryMaster", countryMaster);

		tmpMap = null;

		if (excps.size() == 0) {
			ActionErrors errors = new ActionErrors();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.no.countries.to.add"));
			saveErrors(request, errors);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
