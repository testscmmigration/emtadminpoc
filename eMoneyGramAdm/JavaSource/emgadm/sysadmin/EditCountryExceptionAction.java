package emgadm.sysadmin;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.moneygram.common.util.StringUtility;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.EPartnerSite;
import emgadm.util.StringHelper;
import emgshared.model.CountryExceptionBean;
import emgshared.model.CountryMasterBean;
import emgshared.model.UserActivityLog;
import emgshared.model.UserActivityType;
import emgshared.services.CountryExceptionService;
import emgshared.services.ServiceFactory;


/**
 * @version 	1.0
 * @author
 */
public class EditCountryExceptionAction extends EMoneyGramAdmBaseAction
{

	public static final String SELECTED_COUNTRY_OVR = "selectedCountry_ovr";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm rawForm,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		EditCountryExceptionForm form =
			(EditCountryExceptionForm) rawForm;

		if (!StringHelper
			.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_COUNTRY_EXCEPTION);
		}
		ServiceFactory sf = ServiceFactory.getInstance();
		CountryExceptionService ces = sf.getCountryExceptionService();
		CountryExceptionBean excp = null;
		form.setPartnerSiteId(request.getParameter("partnerName"));
		if (form.getAddEditFlag() == null) {
		    form.setAddEditFlag("edit");
		}
		//  the user comes from the add country list screen with querystring
		if (request.getParameter("cntryId") != null) {
			CountryMasterBean countryMaster = (CountryMasterBean) ces.getCountryMasterList().get(request.getParameter("cntryId"));
			request.setAttribute("countryMaster", countryMaster);
			form.setRcvIsoCntryCode(request.getParameter("cntryId"));
			//System.out.println("----request.getAttribute('selectedPartner'):"+request.getSession().getAttribute("selectedPartner"));
			String selectedCountry = (String) request.getSession().getAttribute("selectedCountry");
			form.setIsoCntryCode(selectedCountry);
			form.setPartnerSiteCurrency(EPartnerSite.getPartnerByCountry(selectedCountry).getPartnerSiteCurrency());
			form.setSndAllowFlag("Y");
			form.setManualReviewAllowFlag("N");
			form.setSndMaxAmt("0.000");
			form.setSndThrldWarnAmt("0.000");
			form.setExcpCmntText("");
			form.setAddEditFlag("add");
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}


		//  the user comes from the country exception list screen with querystring
		String sndContry = request.getParameter("id1");
		if (sndContry != null) {
			String rcvCountry = request.getParameter("id2");
			excp = ces.getCntryException(sndContry, rcvCountry);
			CountryMasterBean countryMaster = (CountryMasterBean) ces.getCountryMasterList().get(sndContry);
			request.setAttribute("countryMaster", countryMaster);
			form.setRcvIsoCntryCode(excp.getRcvIsoCntryCode());
			form.setPartnerSiteCurrency(EPartnerSite.getPartnerByCountry(rcvCountry).getPartnerSiteCurrency());
			form.setIsoCntryCode(excp.getIsoCntryCode());
			form.setSndAllowFlag(excp.getSndAllowFlag());
			form.setSndMaxAmt(excp.getSndMaxAmt());
			form.setManualReviewAllowFlag(excp.getManualReviewAllowFlag());
			if ("Y".equalsIgnoreCase(excp.getSndAllowFlag())
					&& excp.getSndMaxAmtNbr() != 0F) {
				form.setDefaultMaxAmtFlag("N");
			}
			form.setSndThrldWarnAmt(excp.getSndThrldWarnAmt());
			form.setExcpCmntText(excp.getExcpCmntText());
			EditCountryExceptionForm editCountryExceptionFormBefore = form;
			request.getSession().setAttribute("beforeForm", editCountryExceptionFormBefore);
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSave())) {
			excp = new CountryExceptionBean();
			excp.setRcvIsoCntryCode(
				form.getRcvIsoCntryCode());
			String selectedCountry = form.getIsoCntryCode();
			excp.setIsoCntryCode(selectedCountry);
			form.setPartnerSiteCurrency(EPartnerSite.getPartnerByCountry(selectedCountry).getPartnerSiteCurrency());
			excp.setSndAllowFlag(form.getSndAllowFlag());
			excp.setManualReviewAllowFlag(form.getManualReviewAllowFlag());
			excp.setSndMaxAmt(form.getSndMaxAmt());
			excp.setSndThrldWarnAmt(
				form.getSndThrldWarnAmt());
			if (StringHelper.isNullOrEmpty(form.getExcpCmntText())) {
				excp.setExcpCmntText(" ");
			} else {
				excp.setExcpCmntText(
					form.getExcpCmntText());
			}
			ces.setCntryException(excp);
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"msg.county.exception.saved",
					form.getRcvIsoCntryCode()));
			saveErrors(request, errors);
			
			Map<String, String> transferMap = new HashMap();
			transferMap.put(SELECTED_COUNTRY_OVR, excp.getIsoCntryCode());
			request.getSession().setAttribute(SELECTED_COUNTRY_OVR, transferMap);
			try {
			    String logType = null;
			    StringBuffer detailText = new StringBuffer(256);
			    if (form.getAddEditFlag().equalsIgnoreCase("ADD"))
			        logType = UserActivityType.EVENT_TYPE_COUNTRYEXCEPTIONSADD;
			    else {
			        // TODO get an EDIT type
			        logType = UserActivityType.EVENT_TYPE_COUNTRYEXCEPTIONSADD;
			        EditCountryExceptionForm before = (EditCountryExceptionForm)request.getSession().getAttribute("beforeForm");
			        detailText.append("<FROM_ALLOW>" + before.getSndAllowFlag()+ "</FROM_ALLOW>");
			        detailText.append("<FROM_MANUAL_REVIEW>" + before.getManualReviewAllowFlag() + "</FROM_MANUAL_REVIEW>");
			        detailText.append("<FROM_AUTHENTICATE_TRAN>" + before.getManualReviewAllowFlag() + "</FROM_AUTHENTICATE_TRAN>");
			        detailText.append("<FROM_MAX>" + before.getSndMaxAmt() + "</FROM_MAX>");
			        detailText.append("<FROM_THRESHOLD>" + before.getSndThrldWarnAmt() + "</FROM_THRESHOLD>");
			        detailText.append("<TO_ALLOW>" + excp.getSndAllowFlag()+ "</TO_ALLOW>");
			        detailText.append("<TO_MANUAL_REVIEW>" + before.getManualReviewAllowFlag() + "</TO_MANUAL_REVIEW>");
			        detailText.append("<TO_AUTHENTICATE_TRAN>" + before.getManualReviewAllowFlag() + "</TO_AUTHENTICATE_TRAN>");
			        detailText.append("<TO_MAX>" + excp.getSndMaxAmt() + "</TO_MAX>");
			        detailText.append("<TO_THRESHOLD>" + excp.getSndThrldWarnAmt() + "</TO_THRESHOLD>");
			    }
		    	super.insertActivityLog(this.getClass(),request,detailText.toString(),logType,excp.getRcvIsoCntryCode() + UserActivityLog.dataKeyValueSeperator + excp.getIsoCntryCode());
			} catch (Exception e) { }

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_COUNTRY_EXCEPTION);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
