package emgadm.sysadmin;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.EPartnerSite;
import emgadm.property.EMTAdmDynProperties;
import emgadm.util.StringHelper;
import emgshared.exceptions.EMGRuntimeException;


public class EditCountryExceptionForm
		extends EMoneyGramAdmBaseValidatorForm {

	private String addEditFlag;
	private String rcvIsoCntryCode;
	private String isoCntryCode;
	private String partnerSiteCurrency;
	private String sndAllowFlag;
	private String defaultMaxAmtFlag;
	private String sndMaxAmt;
	private String sndThrldWarnAmt;
	private String excpCmntText;
	private String manualReviewAllowFlag;
	private String submitSave = null;
	private String submitCancel = null;
	private String partnerSiteId;
	private static NumberFormat nf = new DecimalFormat("#,##0.00");
    private static final String ZERO_AND_ZERO_CENTS = nf.format(0);

	public String getRcvIsoCntryCode()
	{
		return rcvIsoCntryCode == null ? "" : rcvIsoCntryCode;
	}

	public void setRcvIsoCntryCode(String string)
	{
		rcvIsoCntryCode = string;
	}

	public String getIsoCntryCode()
	{
		return isoCntryCode == null ? "" : isoCntryCode;
	}

	public void setIsoCntryCode(String string)
	{
		isoCntryCode = string;
	}

	public String getManualReviewAllowFlag()
	{
		return manualReviewAllowFlag == null ? "" : manualReviewAllowFlag;
	}

	public void setManualReviewAllowFlag(String string)
	{
		manualReviewAllowFlag = string;
	}

	public String getSndAllowFlag()
	{
		return sndAllowFlag == null ? "" : sndAllowFlag;
	}

	public void setSndAllowFlag(String string)
	{
		sndAllowFlag = string;
	}

	public String getDefaultMaxAmtFlag()
	{
		return defaultMaxAmtFlag == null ? "Y" : defaultMaxAmtFlag;
	}

	public void setDefaultMaxAmtFlag(String string)
	{
		defaultMaxAmtFlag = string;
	}

	public String getSndMaxAmt()
	{
		return sndMaxAmt == null ? ZERO_AND_ZERO_CENTS : sndMaxAmt;
	}

	public void setSndMaxAmt(String string)
	{
		sndMaxAmt = string;
	}

	public String getSndThrldWarnAmt()
	{
		return sndThrldWarnAmt == null ? ZERO_AND_ZERO_CENTS : sndThrldWarnAmt;
	}

	public void setSndThrldWarnAmt(String string)
	{
		sndThrldWarnAmt = string;
	}

	public String getExcpCmntText()
	{
		return excpCmntText == null ? "" : excpCmntText;
	}

	public void setExcpCmntText(String string)
	{
		excpCmntText = string;
	}

	public String getSubmitSave()
	{
		return this.submitSave;
	}

	public void setSubmitSave(String s)
	{
		this.submitSave = s;
	}

	public String getSubmitCancel()
	{
		return this.submitCancel;
	}

	public void setSubmitCancel(String s)
	{
		this.submitCancel = s;
	}

	public String getSysDfltMaxAmt()
	{
		return nf.format(getMaxAmt());
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		submitSave = null;
	}
	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		if (submitSave != null)
		{
			errors = super.validate(mapping, request);

			if ("Y".equalsIgnoreCase(getDefaultMaxAmtFlag())
				&& Float.parseFloat(sndMaxAmt) != 0F)
			{
			    this.setSndMaxAmt(ZERO_AND_ZERO_CENTS);
			}

			if ("N".equalsIgnoreCase(getSndAllowFlag())
				&& Float.parseFloat(sndMaxAmt) != 0F)
			{
			    this.setSndMaxAmt(ZERO_AND_ZERO_CENTS);
			}

			if ("N".equalsIgnoreCase(getSndAllowFlag())
				&& Float.parseFloat(sndThrldWarnAmt) != 0F)
			{
				setDefaultMaxAmtFlag("N");
				this.setSndThrldWarnAmt(ZERO_AND_ZERO_CENTS);
			}

			if (!StringHelper.isNullOrEmpty(excpCmntText)
				&& excpCmntText.length() > 255)
			{
				String[] parm = { "Country Exception Comments", "255" };
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.length.too.long", parm));
			}

			if (errors.size() == 0)
			{
				try
				{
					float propMaxAmt = getMaxAmt();

					if ("N".equalsIgnoreCase(defaultMaxAmtFlag))
					{
						float maxAmt = nf.parse(sndMaxAmt).floatValue();
						if (maxAmt > propMaxAmt)
						{
							String[] parms =
								{ sndMaxAmt, nf.format(propMaxAmt)};
							errors.add(
								ActionErrors.GLOBAL_ERROR,
								new ActionError(
									"error.max.amt.too.big",
									parms));
						}
					}

					if ("Y".equalsIgnoreCase(sndAllowFlag))
					{
						if ("N".equalsIgnoreCase(defaultMaxAmtFlag)
							&& (nf.parse(sndMaxAmt).doubleValue()
								< nf.parse(sndThrldWarnAmt).doubleValue()))
						{
							String[] strs =
								{ getSndThrldWarnAmt(), getSndMaxAmt(), getCurrencyPrefix(), getCurrencySuffix()};
							errors.add(
								ActionErrors.GLOBAL_ERROR,
								new ActionError(
									"error.max.threshold.amt.too.big",
									strs));
						}

						if ("Y".equalsIgnoreCase(defaultMaxAmtFlag))
						{
							if (nf.parse(sndThrldWarnAmt).doubleValue() == 0F)
							{
								errors.add(
									ActionErrors.GLOBAL_ERROR,
									new ActionError("error.threshold.amt.cannot.be.zero"));
							} else if (
								propMaxAmt
									< nf.parse(sndThrldWarnAmt).doubleValue())
							{
								String[] strs =
									{
										getSndThrldWarnAmt(),
										nf.format(propMaxAmt), getCurrencyPrefix(), getCurrencySuffix()};
								errors.add(
									ActionErrors.GLOBAL_ERROR,
									new ActionError(
										"error.max.threshold.amt.too.big1",
										strs));
							}
						}
					}
				} catch (ParseException e)
				{
					throw new EMGRuntimeException("Parsing exception");
				}
			}
		}
		return errors;
	}

	private String getCurrencySuffix() {
		return "";
	}

	private String getCurrencyPrefix() {
		return EPartnerSite.GBR.equals(this.isoCntryCode) ? "&pound;" : "$";
	}

	public float getMaxAmt() {
		if (EPartnerSite.GBR.getCountry().equals(this.isoCntryCode)) {
			return EMTAdmDynProperties.getDPO().getMGCCLimit_GB();
		}
		if (EPartnerSite.GER.getCountry().equals(this.isoCntryCode)) {
			return EMTAdmDynProperties.getDPO().getMGCCLimit_DE();
		}
		return EMTAdmDynProperties.getDPO().getMgACHLimit();
	}
    /**
     * @return Returns the addEditFlag.
     */
    public String getAddEditFlag() {
        return addEditFlag;
    }
    /**
     * @param addEditFlag The addEditFlag to set.
     */
    public void setAddEditFlag(String addEditFlag) {
        this.addEditFlag = addEditFlag;
    }

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void setPartnerSiteCurrency(String partnerSiteCurrency) {
		this.partnerSiteCurrency = partnerSiteCurrency;
	}

	public String getPartnerSiteCurrency() {
		return partnerSiteCurrency;
	}
}