package emgadm.actions;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.constants.EMoneyGramAdmForwardConstants;
/**
 * @version 	1.0
 * @author
 */
public class SystemErrorAction extends Action
{
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	{
		return (mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS));
	}
}
