/*
 * Created on Jan 7, 2005
 *
 */
package emgadm.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ExceptionHandler;
import org.apache.struts.config.ExceptionConfig;

import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.model.UserProfile;
import emgshared.dataaccessors.EMGSharedLogger;

/**
 * This class will handle any exceptions not handled
 * by individual actions.
 * @author A131
 *
 */
public class UnexpectedExceptionHandler extends ExceptionHandler
{

	public ActionForward execute(
		Exception e,
		ExceptionConfig exceptionConfig,
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws ServletException
	{

		StringBuffer message = new StringBuffer();
		message.append("[");
		UserProfile up =
			(UserProfile) request.getSession().getAttribute(
				EMoneyGramAdmSessionConstants.USER_PROFILE);
		message.append(up.getUID());
		message.append("]");
		EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
			message.toString(),
			e);

		//  create message beans to display on the screen
		List list = new ArrayList();
		ExceptionMessage em1 = new ExceptionMessage();
		em1.setMsgKey("User");
		em1.setMsgText(up.getUID());
		list.add(em1);

		ExceptionMessage em2 = new ExceptionMessage();
		em2.setMsgKey("Timestamp");
		em2.setMsgText((new Date(System.currentTimeMillis())).toString());
		list.add(em2);

		ExceptionMessage em3 = new ExceptionMessage();
		em3.setMsgKey("Class");
		em3.setMsgText(e.getClass().getName());
		list.add(em3);

		ExceptionMessage em4 = new ExceptionMessage();
		em4.setMsgKey("Message");
		em4.setMsgText(e.getMessage());
		list.add(em4);

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String stack = sw.toString();

		try
		{
			pw.close();
			sw.close();
		} catch (IOException e1)
		{
		}

		boolean firstTime = true;

		while (true)
		{
			if (stack.length() == 0)
			{
				break;
			}
			int i = stack.indexOf('\n');
			ExceptionMessage em = new ExceptionMessage();
			if (firstTime)
			{
				em.setMsgKey("Stack Trace");
				firstTime = false;
			} else
			{
				em.setMsgKey("");
			}
			if (i < 1)
			{
				em.setMsgText(stack);
				break;
			}
			em.setMsgText(stack.substring(0, i));
			list.add(em);
			stack = stack.substring(i + 1);
		}

		request.setAttribute("exceptionMessages", list);

		String path = exceptionConfig.getPath();
		if (path == null)
		{
			path = "/systemError.do";
		}

		return new ActionForward(path);
	}

	public class ExceptionMessage
	{
		private String msgKey;
		private String msgText;

		public String getMsgKey()
		{
			return msgKey;
		}

		public void setMsgKey(String string)
		{
			msgKey = string;
		}

		public String getMsgText()
		{
			return msgText;
		}

		public void setMsgText(String string)
		{
			msgText = string;
		}

	}
}
