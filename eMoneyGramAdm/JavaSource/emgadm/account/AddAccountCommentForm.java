/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class AddAccountCommentForm extends EMoneyGramAdmBaseValidatorForm
{
	private String accountId;
	private String accountTypeCode;
	private String commentReasonCode;
	private String commentText;

	public String getAccountId()
	{
		return accountId;
	}

	public String getCommentReasonCode()
	{
		return commentReasonCode;
	}

	public String getCommentText()
	{
		return commentText;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public void setCommentReasonCode(String string)
	{
		commentReasonCode = string;
	}

	public void setCommentText(String string)
	{
		commentText = string;
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}
}
