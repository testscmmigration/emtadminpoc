/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.ConsumerAccountType;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class AddAccountCommentAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	private static final String FORWARD_SUCCESS_BANK = "success_bank";
	private static final String FORWARD_SUCCESS_CC = "success_creditcard";
	private static final String FORWARD_FAILURE = "failure";
	private static final String FORWARD_FAILURE_BANK = "failure_bank";
	private static final String FORWARD_FAILURE_CC = "failure_creditcard";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		AddAccountCommentForm inputForm = (AddAccountCommentForm) form;
		String commentText = inputForm.getCommentText();
		String accountTypeCode = inputForm.getAccountTypeCode();
		if (StringUtils.isNotBlank(commentText))
		{
			AccountComment comment = new AccountComment();
			comment.setAccountId(Integer.parseInt(inputForm.getAccountId()));
			comment.setReasonCode(inputForm.getCommentReasonCode());
			comment.setText(inputForm.getCommentText());
			ConsumerAccountService accountService =
				ServiceFactory.getInstance().getConsumerAccountService();
			accountService.addAccountComment(
				comment,
				getUserProfile(request).getUID());
			inputForm.setCommentText(null);
			forward = mapping.findForward(getForwardKey(accountTypeCode, true));
		} else
		{
			ActionErrors errors = new ActionErrors();
			errors.add(
				"commentText",
				new ActionMessage("errors.required", "Comment text"));
			saveErrors(request, errors);
			forward =
				mapping.findForward(getForwardKey(accountTypeCode, false));
		}
		return forward;
	}

	private String getForwardKey(String accountTypeCode, boolean success)
	{
		String forwardKey;
		try
		{
			ConsumerAccountType accountType =
				ConsumerAccountType.getInstance(accountTypeCode);
			if (accountType.isBankAccount())
			{
				forwardKey =
					(success ? FORWARD_SUCCESS_BANK : FORWARD_FAILURE_BANK);
			} else if (accountType.isCardAccount())
			{
				forwardKey =
					(success ? FORWARD_SUCCESS_CC : FORWARD_FAILURE_CC);
			} else
			{
				forwardKey = (success ? FORWARD_SUCCESS : FORWARD_FAILURE);
			}
		} catch (IllegalArgumentException e)
		{
			forwardKey = (success ? FORWARD_SUCCESS : FORWARD_FAILURE);
		}
		return forwardKey;
	}
}
