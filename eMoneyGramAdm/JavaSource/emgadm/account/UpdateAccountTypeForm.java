package emgadm.account;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class UpdateAccountTypeForm extends EMoneyGramAdmBaseValidatorForm
{
	private String custId;
	private String accountId;
	private String accountTypeCode;
	private String submitCC;
	private String submitDC;

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}

	public String getSubmitCC()
	{
		return submitCC;
	}

	public void setSubmitCC(String string)
	{
		submitCC = string;
	}

	public String getSubmitDC()
	{
		return submitDC;
	}

	public void setSubmitDC(String string)
	{
		submitDC = string;
	}
}
