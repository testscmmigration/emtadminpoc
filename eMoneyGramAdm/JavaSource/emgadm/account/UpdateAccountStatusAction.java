/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgshared.exceptions.DataSourceException;
import emgshared.model.AccountComment;
import emgshared.model.AccountStatus;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.UserActivityType;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ServiceFactory;

/**
 * @author A131
 *
 */
public class UpdateAccountStatusAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws NumberFormatException, DataSourceException
	{
		ActionForward forward;
		UpdateAccountStatusForm inputForm = (UpdateAccountStatusForm) form;
		AccountStatus newStatus =
			AccountStatus.getInstanceByCombinedCode(
				inputForm.getCombinedStatusCode());
		String userId = getUserProfile(request).getUID();
		int accountId = Integer.parseInt(inputForm.getAccountId());
		ConsumerAccountType accountType =
			ConsumerAccountType.getInstance(inputForm.getAccountTypeCode());
		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();
		ConsumerAccount oldAcct = accountService.getAccountByAccountId(Integer.parseInt(inputForm.getCustId()),Integer.parseInt(inputForm.getAccountId()));
		accountService.updateStatus(accountId, accountType, newStatus, userId);
		//Look up status description
		Map Descriptions = accountService.getAccountStatusDescriptions();
		String StatusDesc = (String) Descriptions.get(newStatus);
			
		//Add status change comment
		AccountComment comment = new AccountComment();
		comment.setAccountId(accountId);
		comment.setReasonCode("OTH");
		comment.setText("Status Changed to "+StatusDesc);
		accountService.addAccountComment(comment, userId);
        try {
            StringBuffer detailText = new StringBuffer(512);
            detailText.append("<FROM_STATUS>" + oldAcct.getStatusCode() + ":" + oldAcct.getSubStatusCode() + "</FROM_STATUS>");
            detailText.append("<TO_STATUS>" + inputForm.getCombinedStatusCode() + "</TO_STATUS>");
            if (inputForm.getAccountTypeCode().startsWith("CC"))
                super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILECHGCCSTATUS,inputForm.getCustId());
            else if (inputForm.getAccountTypeCode().startsWith("BANK"))
            	super.insertActivityLog(this.getClass(),request,detailText.toString(),UserActivityType.EVENT_TYPE_CONSUMERPROFILECHGBANKACCTSTATUS,inputForm.getCustId());
		} catch (Exception ignore) { }
		
		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}
}
