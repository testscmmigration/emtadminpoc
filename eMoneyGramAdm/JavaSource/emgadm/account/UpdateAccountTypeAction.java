package emgadm.account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ServiceFactory;

public class UpdateAccountTypeAction extends EMoneyGramAdmBaseAction
{
	private static final String FORWARD_SUCCESS = "success";

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException
	{
		ActionForward forward;
		UpdateAccountTypeForm form = (UpdateAccountTypeForm) f;
		UserProfile up = getUserProfile(request);

		int accountId = Integer.parseInt(form.getAccountId());
		ConsumerAccountService accountService =
			ServiceFactory.getInstance().getConsumerAccountService();
		ConsumerCreditCardAccount account =
			accountService.getCreditCardAccount(accountId, up.getUID());

		String newType = null;
		String code = null;

		if (!StringHelper.isNullOrEmpty(form.getSubmitCC()))
		{
			newType = "CC";
			code = "C";
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitDC()))
		{
			newType = "DC";
			code = "D";
		}

		if (newType != null)
		{
			accountService.updateCreditCardAccountType(
				up.getUID(),
				account.getId(),
				newType + form.getAccountTypeCode().substring(2),
				code);
		}

		forward = mapping.findForward(FORWARD_SUCCESS);
		return forward;
	}

}
