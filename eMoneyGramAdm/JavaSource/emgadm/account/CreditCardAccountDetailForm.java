/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.view.AccountCommentView;
import emgshared.model.AccountStatus;

/**
 * @author A131
 *
 */
public class CreditCardAccountDetailForm extends EMoneyGramAdmBaseValidatorForm
{
	private String accountId;
	private String custId;
	private String accountNumberMask;
	private String ccHashed;
	private String binNumber;
	private String cardType;
	private String expirationMonth;
	private String expirationYear;
	private String statusCode;
	private String subStatusCode;
	private String accountTypeCode;
	private String accountTypeDesc;
	private String accountTaintText;
	private String binTaintText;
	private List accountStatusOptions = new ArrayList(0);
	private List accountCommentReasons = new ArrayList(0);
	private AccountCommentView[] comments = new AccountCommentView[0];

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setAccountNumberMask(String string)
	{
		accountNumberMask = string;
	}

	public void setCardType(String string)
	{
		cardType = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public String getExpirationMonth()
	{
		return expirationMonth;
	}

	public String getExpirationYear()
	{
		return expirationYear;
	}

	public void setExpirationMonth(String string)
	{
		expirationMonth = string;
	}

	public void setExpirationYear(String string)
	{
		expirationYear = string;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public void setStatusCode(String string)
	{
		statusCode = string;
	}

	public void setSubStatusCode(String string)
	{
		subStatusCode = string;
	}

	public String getCombinedStatusCode()
	{
		return this.statusCode + AccountStatus.DELIMITER + this.subStatusCode;
	}

	public void setCombinedStatusCode(String combinedStatusCode)
	{
		String[] codes =
			StringUtils.split(combinedStatusCode, AccountStatus.DELIMITER);
		this.statusCode = (codes.length > 0 ? codes[0] : null);
		this.subStatusCode = (codes.length > 1 ? codes[1] : null);
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public String getCustId()
	{
		return custId;
	}

	public void setCustId(String string)
	{
		custId = string;
	}

	/**
	 * @return
	 * 
	 * Created on Mar 7, 2005
	 */
	public List getAccountCommentReasons()
	{
		return accountCommentReasons;
	}

	/**
	 * @return
	 * 
	 * Created on Mar 7, 2005
	 */
	public List getAccountStatusOptions()
	{
		return accountStatusOptions;
	}

	/**
	 * @return
	 * 
	 * Created on Mar 7, 2005
	 */
	public String getAccountTypeDesc()
	{
		return accountTypeDesc;
	}

	/**
	 * @return
	 * 
	 * Created on Mar 7, 2005
	 */
	public AccountCommentView[] getComments()
	{
		return comments;
	}

	/**
	 * @param map
	 * 
	 * Created on Mar 7, 2005
	 */
	public void setAccountCommentReasons(List list)
	{
		accountCommentReasons = (list == null ? new ArrayList(0) : list);
	}

	/**
	 * @param map
	 * 
	 * Created on Mar 7, 2005
	 */
	public void setAccountStatusOptions(List list)
	{
		accountStatusOptions = (list == null ? new ArrayList(0) : list);
	}

	/**
	 * @param string
	 * 
	 * Created on Mar 7, 2005
	 */
	public void setAccountTypeDesc(String string)
	{
		accountTypeDesc = string;
	}

	/**
	 * @param views
	 * 
	 * Created on Mar 7, 2005
	 */
	public void setComments(AccountCommentView[] views)
	{
		comments = views;
	}

	/**
	 * @return
	 */
	public String getAccountTaintText() {
		return accountTaintText;
	}

	/**
	 * @param string
	 */
	public void setAccountTaintText(String string) {
		accountTaintText = string;
	}

	/**
	 * @return
	 */
	public String getBinTaintText() {
		return binTaintText;
	}

	/**
	 * @param string
	 */
	public void setBinTaintText(String string) {
		binTaintText = string;
	}

	public String getAccountBinMask() {
		return binNumber;
	}

	public String getCcHashed() {
		return ccHashed;
	}

	public void setCcHashed(String ccHashed) {
		this.ccHashed = ccHashed;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
}
