/*
 * Created on Feb 21, 2005
 *
 */
package emgadm.account;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * @author A131
 *
 */
public class UpdateAccountStatusForm extends EMoneyGramAdmBaseValidatorForm
{
	private String accountId;
	private String combinedStatusCode;
	private String accountTypeCode;
	private String custId;
	
	public String getAccountId()
	{
		return accountId;
	}

	public String getCombinedStatusCode()
	{
		return combinedStatusCode;
	}

	public void setAccountId(String string)
	{
		accountId = string;
	}

	public void setCombinedStatusCode(String string)
	{
		combinedStatusCode = string;
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}
    /**
     * @return Returns the custId.
     */
    public String getCustId() {
        return custId;
    }
    /**
     * @param custId The custId to set.
     */
    public void setCustId(String ci) {
        this.custId = ci;
    }
}
