package emgadm.roles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.RoleManager;
import emgadm.model.ApplicationCommand;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;

public class ViewRolesAction extends EMoneyGramAdmBaseAction {

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {
		ViewRolesForm roleForm = (ViewRolesForm) form;
		UserProfile userProfile = getUserProfile(request);
		if (userProfile.isInternalUser()) {
			try {
				roleForm.setInternalRoles(RoleManager.getFreshedRoles());
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		}
		if (roleForm.getRoleId() != null) {
			roleForm.setRole(RoleManager.getRole(roleForm.getRoleId()));
			Collection list = new ArrayList();
			Iterator iter =
				roleForm.getRole().getApplicationCommands().iterator();
			while (iter.hasNext()) {
				ApplicationCommand ac = (ApplicationCommand) iter.next();
				String desc = null;
				if (!StringHelper.isNullOrEmpty(ac.getDisplayName())) {
					desc = ac.getDisplayName();
				} else {
					desc = ac.getDescription();
				}
				list.add(desc);
			}
			request.setAttribute("descList", list);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
