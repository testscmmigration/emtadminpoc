package emgadm.test;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import emgadm.model.TranAction;


public class StatusTracker {

	   protected static Connection DBconn = null;
	   protected static String pDBConnString = "jdbc:oracle:thin:@172.16.32.201:1522:EMGD";
	   protected static String  pDBUserId = "emgmgi";
	   protected static String pDBPassword = "em6m6i";
	   protected static String pDBDriverName = "oracle.jdbc.driver.OracleDriver";
	   protected static ArrayList masterList = new ArrayList();
	   protected static HashMap exhaustedList = new HashMap();
	   private static int linecount = 0;
	   //TODO Removed unwanted entry
//	   public static void main(String[] args) {
//
//		PreparedStatement pt = null;
//		StringBuffer sqlBuffer = new StringBuffer(64);
//		sqlBuffer.append("Select * from emg_tran_reason where actv_reas_flag = 'Y' order by tran_old_stat_code,tran_old_sub_stat_code");
//		
//		
//
//		try {
//			Class.forName(StatusTracker.pDBDriverName).newInstance();
//			DBconn = DriverManager.getConnection(StatusTracker.pDBConnString,StatusTracker.pDBUserId,StatusTracker.pDBPassword);
//			//System.out.println("SQL:" + sqlBuffer.toString());
//			pt = DBconn.prepareStatement(sqlBuffer.toString());
//			ResultSet rs = pt.executeQuery();
//			
//			while (rs.next())
//			{
//				StatusHierarchy sh = new StatusHierarchy();
//				String status = rs.getString("ACTV_REAS_FLAG");
//				if (!status.equalsIgnoreCase("N")){
//					sh.setReasonID(rs.getString("TRAN_REAS_CODE"));
//					sh.setTranOldStatCode(rs.getString("TRAN_OLD_STAT_CODE"));
//					sh.setTranSubStatCode(rs.getString("TRAN_SUB_STAT_CODE"));
//					sh.setTranStatCode(rs.getString("TRAN_STAT_CODE"));
//					sh.setTranOldSubStatCode(rs.getString("TRAN_OLD_SUB_STAT_CODE"));
//					sh.setTranReasDesc(rs.getString("TRAN_REAS_DESC"));
//					masterList.add(sh);
//				}
//			}
//			buildHierarchy("FFS/NOF",1);
//		    	
//		} catch (Exception e) {
//			System.out.println("Exception:" + e.getLocalizedMessage());
//			e.printStackTrace();
//		} finally {
//			try {
//				pt.close();
//				DBconn.close();
//				
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//			
//		}
//		return;
//	}


	private static void buildHierarchy(String statusCombo, int level)
	{
		StatusTracker.exhaustedList.put(statusCombo, statusCombo);
		for (Iterator iter = StatusTracker.masterList.iterator(); iter.hasNext();) {
			StatusHierarchy sh = (StatusHierarchy) iter.next();
			String key = sh.getTranOldStatCode() + "/" + sh.getTranOldSubStatCode();
			if (key.equalsIgnoreCase(statusCombo))
			{
				String linecount2 = String.valueOf(linecount);
				if (linecount2.length()<2) linecount2="0" +linecount2;
				System.out.print(linecount2);
				for (int i = 1; i < level; i++){  
					if (i%2==0)System.out.print("|  "); 
					else System.out.print("-  ");
				}
				StatusTracker.linecount++;
				//System.out.println("-Level:" + level + "=" +  statusCombo +" -->"+ sh.getTranStatCode() + "/" + sh.getTranSubStatCode() + "  " + sh.getTranReasDesc());
				String key2 = sh.getTranStatCode() + "/" + sh.getTranSubStatCode();
				if (!StatusTracker.exhaustedList.containsKey(key2))
					buildHierarchy(sh.getTranStatCode() + "/" + sh.getTranSubStatCode(), level+1);
			}
			
		}
		return;
	}
	
	
//	private void dbConnect() throws Exception, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
//		  try
//		  {
////			System.out.println("Connection string:" + this.pDBConnString);
//			Class.forName(this.pDBDriverName).newInstance();
//			DBconn = DriverManager.getConnection(this.pDBConnString,this.pDBUserId,this.pDBPassword);
//		  }
//		  catch (Exception e)
//		  {
//			  System.out.println("error occurred in dbConnect of OFACDatabaseAccessor: " + e.toString());
//			  throw e;
//		  }
//	   } // end dbConnect



	
	
}


