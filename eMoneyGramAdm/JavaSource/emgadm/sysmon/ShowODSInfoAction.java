package emgadm.sysmon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;

public class ShowODSInfoAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		List infoList = new ArrayList();

		request.setAttribute("noCache", "Y");

		//  emgd database for shared site
//		infoList =
//			getInfo(OracleAccess.instance().getAllOds(), infoList, request);

		Collections.sort(infoList);
		request.setAttribute("infoList", infoList);

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	public class ODSInfoBean implements Comparable
	{
		private String dbName = "";
		private String cacheName = "";
		private int seq = 0;
		private String attr = "";
		private String value = "";

		public int compareTo(Object obj)
		{
			ODSInfoBean bean = (ODSInfoBean) obj;

			if (!this.dbName.equalsIgnoreCase(bean.dbName))
			{
				return this.dbName.compareToIgnoreCase(bean.dbName);
			}

			if (this.cacheName != null
				&& bean.cacheName != null
				&& !this.cacheName.equalsIgnoreCase(bean.cacheName))
			{
				return this.cacheName.compareToIgnoreCase(bean.cacheName);
			}

			if (this.seq != bean.seq)
			{
				return this.seq - bean.seq;
			}

			return this.attr.compareToIgnoreCase(bean.attr);
		}

		public String getDbName()
		{
			return dbName;
		}

		public void setDbName(String string)
		{
			dbName = string;
		}

		public String getCacheName()
		{
			return cacheName == null ? "" : cacheName;
		}

		public void setCacheName(String string)
		{
			cacheName = string;
		}

		public int getSeq()
		{
			return seq;
		}

		public void setSeq(int i)
		{
			seq = i;
		}

		public String getAttr()
		{
			return attr;
		}

		public void setAttr(String string)
		{
			attr = string;
		}

		public String getValue()
		{
			return value == null ? "" : value;
		}

		public void setValue(String string)
		{
			value = string;
		}

		public void setValue(boolean b)
		{
			value = String.valueOf(b);
		}
	}
}
