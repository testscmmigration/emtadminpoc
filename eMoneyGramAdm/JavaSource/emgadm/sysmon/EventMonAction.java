package emgadm.sysmon;

import java.io.FileInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.util.DateHelper;
import emgadm.util.StringHelper;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.util.DateFormatter;
import eventmon.eventmonitor.EventMonitor;

public class EventMonAction extends EMoneyGramAdmBaseAction
{
	static String MON_INTERVAL = "monInterval";
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		EventMonForm form = (EventMonForm) f;
		if (StringHelper.isNullOrEmpty(form.getRefTime())) {

			Map<String, String> transferMap = (Map<String, String>) request
					.getSession().getAttribute(MON_INTERVAL);
			String interval = null;
			if (transferMap != null)
				if (transferMap.containsKey(MON_INTERVAL))
					interval = transferMap.get(MON_INTERVAL);
			form.setRefTime(interval == null ? "0" : interval);
		}
		
		Map<String, String> transferMap = new HashMap();
		transferMap.put(MON_INTERVAL, form.getRefTime());
		request.getSession().setAttribute(MON_INTERVAL, transferMap);

		ActionErrors errors = new ActionErrors();
		EventMonitor em = null;

		if ("A".equalsIgnoreCase(form.getSelType()))
		{
			form.setDisplayDate(form.getArchiveDate());
			try
			{
				em = deserializeObj(form.getArchiveDate());
			} catch (Exception e)
			{
			}
		} else if ("R".equalsIgnoreCase(form.getSelType()))
		{
			em = EventMonitor.getInstance();
			form.setDisplayDate(
				 em.getDay()  + "/" + DateHelper.getMonthName(em.getMonth()) + "/" 
				 + em.getYear());
		}

		if (em == null || em.getEventMap().size() == 0)
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"errors.no.match.record.found",
					"Event Monitor"));
			saveErrors(request, errors);
		} else
		{
			request.setAttribute("eventMon", em);
			List list = new ArrayList();
			Iterator iter = em.getEventMap().values().iterator();
			while (iter.hasNext())
			{
				Object obj = iter.next();
				list.add(obj);
			}
			Collections.sort(list);
			request.setAttribute("eventStat", list);
		}
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private EventMonitor deserializeObj(String archiveDate) throws Exception
	{
		EventMonitor em = null;
		String dir = EMTSharedContainerProperties.getEventMonDir();

		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		Calendar cal = Calendar.getInstance();
		cal.setTime(df.parse(archiveDate));
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		NumberFormat nf1 = new DecimalFormat("0000");
		NumberFormat nf2 = new DecimalFormat("00");
		String archiveFile =
			dir
				+ "/"
				+ nf1.format(year)
				+ "-"
				+ nf2.format(month)
				+ "-"
				+ nf2.format(day)
				+ ".ser";

		ObjectInput in = null;
		try
		{
			in = new ObjectInputStream(new FileInputStream(archiveFile));
			em = (EventMonitor) in.readObject();
		} finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		return em;
	}
}
