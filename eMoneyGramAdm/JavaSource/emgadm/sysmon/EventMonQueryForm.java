package emgadm.sysmon;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import emgshared.exceptions.ParseDateException;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class EventMonQueryForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final DateFormatter df =
		new DateFormatter("dd/MMM/yyyy", true);

	private String event;
	private String selEvent;
	private String beginDate;
	private String endDate;
	private String selDateRange;
	private String timeType;
	private String startTime;
	private String endTime;
	private String selTimeRange;
	private String submitGo;

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		String[] tmpList = EventListConstant.getEVENT_LIST();
		List eventList = new ArrayList();
		for (byte i = 0; i < tmpList.length; i++)
		{
			eventList.add(new LabelValueBean(tmpList[i], tmpList[i]));
		}
		request.getSession().setAttribute("events", eventList);
		event = event == null ? tmpList[0] : event;

		long now = System.currentTimeMillis();
		long oneDay = 24L * 60L * 60L * 1000L;
		beginDate = df.format(new Date(now - 12 * oneDay));
		endDate = df.format(new Date(now - oneDay));

		timeType = "A";

		List hourList = new ArrayList();
		for (byte i = 0; i < 24; i++)
		{
			String time = (i < 12 ? i : i - 12) + (i < 12 ? " AM" : " PM");
			String time1 =
				(i + 1 < 12 ? i + 1 : i + 1 - 12)
					+ (i + 1 < 12 ? " AM" : " PM");
			hourList.add(
				new LabelValueBean(time + " - " + time1, String.valueOf(i)));
		}
		request.getSession().setAttribute("hours", hourList);
		startTime = "0";
		endTime = "23";

		submitGo = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitGo))
		{
			long oneDay = 24L * 60L * 60L * 1000L;
			Date yesterday = new Date(System.currentTimeMillis() - oneDay);

			try
			{
				Date tmpDate1 = df.parse(beginDate);
				if (tmpDate1.after(yesterday))
				{
					String[] parm = { "archive", "today" };
					errors.add(
						"beginDate",
						new ActionError("error.date.before", parm));
				}

				Date tmpDate2 = df.parse(endDate);
				if (tmpDate2.after(yesterday))
				{
					String[] parm = { "archive", "today" };
					errors.add(
						"endDate",
						new ActionError("error.date.before", parm));
				}

				if (errors.size() == 0 && tmpDate1.after(tmpDate2))
				{
					String[] parm = { beginDate, endDate };
					errors.add(
						"beginDate",
						new ActionError(
							"error.begin.date.later.than.end.date",
							parm));
				}

			} catch (ParseDateException e)
			{
				errors.add(
					"beginDate",
					new ActionError("error.malformed.date"));
			}

			if ("A".equals(timeType)
				&& Integer.parseInt(startTime) > Integer.parseInt(endTime))
			{
				String[] parms = { "Starting Time", "Ending Time" };
				errors.add(
					"timeRange",
					new ActionError("errors.seq.wrong", parms));
			}
		}
		return errors;
	}

	public String getEvent()
	{
		return event;
	}

	public void setEvent(String string)
	{
		event = string;
	}

	public String getSelEvent()
	{
		return selEvent;
	}

	public void setSelEvent(String string)
	{
		selEvent = string;
	}

	public String getBeginDate()
	{
		return beginDate;
	}

	public void setBeginDate(String string)
	{
		beginDate = string;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String string)
	{
		endDate = string;
	}

	public String getSelDateRange()
	{
		return selDateRange;
	}

	public void setSelDateRange(String string)
	{
		selDateRange = string;
	}

	public String getTimeType()
	{
		return timeType;
	}

	public void setTimeType(String string)
	{
		timeType = string;
	}

	public String getStartTime()
	{
		return startTime;
	}

	public void setStartTime(String string)
	{
		startTime = string;
	}

	public String getEndTime()
	{
		return endTime;
	}

	public void setEndTime(String string)
	{
		endTime = string;
	}

	public String getSelTimeRange()
	{
		return selTimeRange;
	}

	public void setSelTimeRange(String string)
	{
		selTimeRange = string;
	}

	public String getSubmitGo()
	{
		return submitGo;
	}

	public void setSubmitGo(String string)
	{
		submitGo = string;
	}
}