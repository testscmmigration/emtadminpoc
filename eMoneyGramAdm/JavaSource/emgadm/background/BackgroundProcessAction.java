package emgadm.background;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.moneygram.emoneygramadm.forms.BackgroundProcessingForm;

import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;

public class BackgroundProcessAction
	extends emgadm.actions.EMoneyGramAdmBaseAction
{
	// a little assertion to test configuration
	private boolean checkLoopbackEnvProps = true; // check once only.

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		BackgroundProcessingForm bpform = (BackgroundProcessingForm) form;
		String partnerSiteId = bpform.getPartnerSiteId();
		//System.out.println("BackgroundProcessingAction got partnerSiteId=" + partnerSiteId);
		// TODO figure out what should change here with MGOUK
		String act = request.getParameter("act");
		if (act != null)
		{
			if (act.equalsIgnoreCase("start"))
			{
				if (checkLoopbackEnvProps)
				{
					try
					{
						if (!EMTAdmContainerProperties
							.getLoopbackServer()
							.equals(request.getServerName()))
						{
							EMGSharedLogger.getLogger(
								this.getClass().getName().toString()).error(
								"Env Prop Loopback Server is "
									+ EMTAdmContainerProperties.getLoopbackServer()
									+ " - doesn't match request.getServerName() = "
									+ request.getServerName());
						}

					} catch (Exception e)
					{
						EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).error(
							"Exception checking loopback server name assertion.",
							e);
					} finally
					{
						checkLoopbackEnvProps = false;
					}
				}
				EMGSharedLogger.getLogger(
					this.getClass().getName().toString()).error(
					"start background processing ");
				String cr = request.getContextPath();
				BackgroundProcessMaster.startBackgroundProcessing(
					EMTAdmContainerProperties.getLoopbackServer(),
					EMTAdmContainerProperties.getLoopbackHost(),cr);

			} else if (act.equalsIgnoreCase("stop"))
			{
				BackgroundProcessMaster.stopAutoProcessing();
			}
		}
		return mapping.findForward("success");
	}

}