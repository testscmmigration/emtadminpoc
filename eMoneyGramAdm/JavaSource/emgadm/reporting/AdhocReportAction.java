package emgadm.reporting;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import shared.mgo.services.MGOServiceFactory;
import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TooManyResultException;
import emgadm.model.Trans;
import emgadm.sysmon.EventListConstant;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.Transaction;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.util.DateFormatter;
import eventmon.eventmonitor.EventMonitor;

/**
 * @version 	1.0
 * @author
 */
public class AdhocReportAction extends EMoneyGramAdmBaseAction
{
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
	{
		ActionErrors errors = new ActionErrors();
		ActionMessages messages = new ActionMessages();
		AdhocReportForm adhocReportForm = (AdhocReportForm) form;
		Collection transactions = new ArrayList();
		ArrayList transCollection = new ArrayList();
		TransactionManager tm = getTransactionManager(request);

		TransactionSearchRequest tsr = new TransactionSearchRequest();
		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		NumberFormat nf = new DecimalFormat("#,##0.00");

		ServletContext sc = request.getSession().getServletContext();

		List countries = new ArrayList();
		try {
			countries = (ArrayList) MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank().getActiveCountryList();
			Collections.sort(countries);
		} catch (Exception ignore) {
			//
		}

		request.getSession().setAttribute("country", countries);

		try
		{
			if (StringHelper.isNullOrEmpty(adhocReportForm.getTransStatus())) {
				sc.setAttribute("tranStatuses", tm.getTranStatusCodes());
			} else if (!adhocReportForm.getTransStatus().equals("All")) {
				tsr.setTranStatCode(adhocReportForm.getTransStatus());
			}

			if (StringHelper.isNullOrEmpty(adhocReportForm.getTransSubStatus())) {
				sc.setAttribute("tranSubStatuses", tm.getAllTranSubStatus());
			} else if (!adhocReportForm.getTransSubStatus().equals("All")) {
				tsr.setTranSubStatCode(adhocReportForm.getTransSubStatus());
			}

			if (StringHelper.isNullOrEmpty(adhocReportForm.getTransType())) {
				sc.setAttribute("tranTypes", tm.getTranTypes(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_CODE));
			} else if (!adhocReportForm.getTransType().equals("All")) {
				tsr.setTranType(adhocReportForm.getTransType());
			}

			if (StringHelper.isNullOrEmpty(adhocReportForm.getPartnerSiteId())) {
				sc.setAttribute("partnerSiteIds", tm.getPartnerSiteIds("A"));
			} else if (!adhocReportForm.getPartnerSiteId().equals("All")) {
				tsr.setPartnerSiteId(adhocReportForm.getPartnerSiteId());
			}

			if (StringHelper.isNullOrEmpty(adhocReportForm.getPaymentMethod())) {
				sc.setAttribute("paymentMethods", tm.getAccountTypes());
			} else if (!adhocReportForm.getPaymentMethod().equals("All")) {
				tsr.setAccountTypeCode(adhocReportForm.getPaymentMethod());
			}

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getDestinationCountry())) {
			if (!adhocReportForm.getDestinationCountry().equals("All")) {
				tsr.setDestinationCountry(adhocReportForm.getDestinationCountry());
			}
		}

		double miniAmt = Double.parseDouble(adhocReportForm.getMinimumAmount());
		if (miniAmt != 0) {
			tsr.setMinimumAmount(new Double(miniAmt));
		}

		double maxiAmt = Double.parseDouble(adhocReportForm.getMaximumAmount());
		if (maxiAmt != 0) {
			tsr.setMaximumAmount(new Double(maxiAmt));
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getProfileId())) {
			tsr.setCustLogonId(adhocReportForm.getProfileId());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getPrimaryAccount())) {
			tsr.setPrimaryAccountMaskNbr(adhocReportForm.getPrimaryAccount());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getSecondaryAccount())) {
			tsr.setSecondaryAccountMaskNbr(adhocReportForm.getSecondaryAccount());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getSenderRT())) {
			tsr.setBankAbaNbr(adhocReportForm.getSenderRT());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getSenderIP())) {
			tsr.setIpAddress(adhocReportForm.getSenderIP());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getReceiverLastName())) {
			tsr.setReceiverLastName(adhocReportForm.getReceiverLastName());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getSSN())) {
			tsr.setSsnMaskNbr(adhocReportForm.getSSN());
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getRcvAgcyCode())) {
			tsr.setRcvAgcyCode(adhocReportForm.getRcvAgcyCode());
		}

		if ("S".equalsIgnoreCase(adhocReportForm.getSelDates())) {
			tsr.setBeginDate(adhocReportForm.getBeginDateText());
			tsr.setEndDate(adhocReportForm.getEndDateText());
		}
		if ("C".equalsIgnoreCase(adhocReportForm.getSelDates())) {
			tsr.setCreateBeginDate(adhocReportForm.getCreateBeginDateText());
			tsr.setCreateEndDate(adhocReportForm.getCreateEndDateText());
		}
		tsr.setUseScore(adhocReportForm.getUseScore());
		if ("Y".equalsIgnoreCase(adhocReportForm.getUseScore())) {
			tsr.setLowScore(Integer.parseInt(adhocReportForm.getLowScore()));
			tsr.setHighScore(Integer.parseInt(adhocReportForm.getHighScore()));
		}
		if (!StringHelper.isNullOrEmpty(adhocReportForm.getSysAutoRsltCode())) {
			tsr.setSysAutoRsltCode(adhocReportForm.getSysAutoRsltCode());
		}

		if (StringHelper.isNullOrEmpty(adhocReportForm.getTranRvwPrcsCode())) {
            tsr.setTranRvwPrcsCode(null);
        } else if (adhocReportForm.getTranRvwPrcsCode().equalsIgnoreCase("ALL")) {
            tsr.setTranRvwPrcsCode(null);
        } else {
            tsr.setTranRvwPrcsCode(adhocReportForm.getTranRvwPrcsCode());
        }

		if (StringHelper.isNullOrEmpty(adhocReportForm.getRetrievalRequestCode())) {
            tsr.setRetrievalRequestCode(null);
        } else if (adhocReportForm.getRetrievalRequestCode().equalsIgnoreCase("ALL")) {
            tsr.setRetrievalRequestCode(null);
		} else {
            tsr.setRetrievalRequestCode(adhocReportForm.getRetrievalRequestCode());
        }

		if (StringHelper.isNullOrEmpty(adhocReportForm.getReferenceNumber())) {
            tsr.setReferenceNumber(null);
		} else {
            tsr.setReferenceNumber(adhocReportForm.getReferenceNumber());
        }

		String id = adhocReportForm.getIdNumber();
		if (!StringHelper.isNullOrEmpty(id)) {
			if (id.length() > 4) {
				emgshared.services.ServiceFactory serviceFactory = ServiceFactory.getInstance();
				ObfuscationService obfuscationService = serviceFactory.getObfuscationService();
				String encryptedId = obfuscationService.getAdditionalIdObfuscation(id);
				tsr.setFullIdNumber(encryptedId);
			} else {
				tsr.setLast4DigitsId(id.substring(id.length() - 4));
			}
		}

		if (!StringHelper.isNullOrEmpty(adhocReportForm.getIdType())) {
			tsr.setIdType(adhocReportForm.getIdType());
		}

		if (StringHelper.isNullOrEmpty(adhocReportForm.getSubmitSearch())) {
			request.setAttribute("Transactions", transCollection);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		//Getting all possible transaction for the given Dates. Once we get this list
		//we will filter records based on the other choices of the user
		long beginTime = System.currentTimeMillis();
		try {
			transactions = tm.getTransactions(tsr, "WEB");
		} catch (DataSourceException e1) {
			throw new EMGRuntimeException(e1);
		} catch (TooManyResultException e) {  // will never be thrown
			EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
			int max = dynProps.getMaxDownloadTransactions();
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.too.many.results", String.valueOf(max)));
			saveErrors(request, errors);
			request.getSession().setAttribute("Transactions", null);
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		} finally {
			EventMonitor.getInstance().recordEvent(EventListConstant.AD_HOC_REPORT, beginTime, System.currentTimeMillis());
		}

		Iterator iter = transactions.iterator();
		Trans.sortBy = adhocReportForm.getSortBy();

		while (iter.hasNext()) {
			Transaction transaction = (Transaction) iter.next();
			Trans trans = new Trans();
			trans.setTransId(Integer.toString(transaction.getEmgTranId()));
			trans.setTransCustId(Integer.toString(transaction.getCustId()));
			trans.setTransType(transaction.getEmgTranTypeDesc());
			trans.setTransSenderName(transaction.getSndCustFrstName() + " " + transaction.getSndCustLastName());
			if (transaction.getEmgTranTypeCode().equals(TransactionType.EXPRESS_PAYMENT_SEND_CODE)
					|| transaction.getEmgTranTypeCode().equals(EMoneyGramAdmApplicationConstants.EXPRESSPAYFORMFREE)) {
				trans.setTransReceiver(transaction.getRcvAgentName() +  " - " + transaction.getRcvAgcyCode());
			} else {
				trans.setTransReceiver(transaction.getRcvCustFrstName()	+ " " + transaction.getRcvCustLastName());
			}
			trans.setTransCntryCd(transaction.getRcvISOCntryCode());
			trans.setTransStatusDate(df.format(transaction.getTranStatDate()));
			trans.setTransCreateDate(df.format(transaction.getCreateDate()));
			trans.setTransStatus(transaction.getTranStatDesc());
			trans.setTransSubStatus(transaction.getTranSubStatDesc());
			trans.setTransAmount(nf.format(transaction.getSndTotAmt()));
			trans.setSysAutoRsltCode(transaction.getSysAutoRsltCode());
			trans.setTransScores(transaction.getTransScores());
			trans.setInternetPurchase(transaction.getInternetPurchase());
			trans.setTranRvwPrcsCode(transaction.getTranRvwPrcsCode());
			trans.setPartnerSiteId(transaction.getPartnerSiteId());
			if(!EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID.equals(trans.getPartnerSiteId())) {
				trans.setClientTraceId(transaction.getTCInternalTransactionNumber());
			}
			trans.setProcessorTraceId(transaction.getTCProviderTransactionNumber());
			trans.setSndISOCrncyId(transaction.getSndISOCrncyId());
			transCollection.add(trans);
		}
		Collections.sort(transCollection);

		if (transCollection.size() == 0) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.no.match.record.found", "Transaction"));
			saveErrors(request, errors);
		} else {
		    int maxCount = dynProps.getMaxDownloadTransactions();
		    ActionMessage myMsg;
		    if (transCollection.size() > maxCount) {
		    	myMsg = new ActionMessage("message.max.results.exceeded", String.valueOf(maxCount));
		    } else {
        	    myMsg = new ActionMessage("message.number.trans.found", String.valueOf(transCollection.size()));
		    }
		    messages.add("nbrFound", myMsg);

		    saveMessages(request, messages);
		}
		request.getSession().setAttribute("Transactions", transCollection);
		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
