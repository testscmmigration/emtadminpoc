package emgadm.reporting;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgshared.exceptions.ParseDateException;
import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.DateFormatter;

public class AdhocReportForm extends EMoneyGramAdmBaseValidatorForm
{
	private String selDates = "S";
	private String beginDateText = null;
	private String endDateText = null;
	private String createBeginDateText = null;
	private String createEndDateText = null;
	private String profileId = null;
	private String paymentMethod = null;
	private String receiverLastName = null;
	private String receiverFirstName = null;
	private String destinationCountry = null;
	private String transStatus = null;
	private String transSubStatus = null;
	private String transType = null;
	private String SSN = null;
	private String primaryAccount = null;
	private String secondaryAccount = null;
	private String senderIP = null;
	private String rcvAgcyCode;
	private String submitSearch = null;
	private String minimumAmount = "0";
	private String maximumAmount = "0";
	private String useScore = "N";
	private String lowScore = "0";
	private String highScore = "0";
	private String sysAutoRsltCode = "";
	private String senderRT = null;
	private String sortBy = null;
	private String tranRvwPrcsCode = null;
	private String retrievalRequestCode = null;
	private String RRN = null;
	private String referenceNumber = null;
	private String partnerSiteId = null;
	private String idType;
	private String idNumber;

	public String getSelDates()
	{
		return selDates;
	}

	public void setSelDates(String string)
	{
		selDates = string;
	}

	public String getBeginDateText()
	{

		return beginDateText;
	}

	public String getEndDateText()
	{
		return endDateText;
	}

	public void setBeginDateText(String string)
	{
		beginDateText = string;
	}

	public void setEndDateText(String string)
	{
		endDateText = string;
	}

	public String getCreateBeginDateText()
	{
		return createBeginDateText;
	}

	public void setCreateBeginDateText(String string)
	{
		createBeginDateText = string;
	}

	public String getCreateEndDateText()
	{
		return createEndDateText;
	}

	public void setCreateEndDateText(String string)
	{
		createEndDateText = string;
	}

	public String getDestinationCountry()
	{
		return destinationCountry;
	}

	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public String getPrimaryAccount()
	{
		return primaryAccount;
	}

	public String getProfileId()
	{
		return profileId;
	}

	public String getSecondaryAccount()
	{
		return secondaryAccount;
	}

	public String getSSN()
	{
		return SSN;
	}

	public String getTransStatus()
	{
		return transStatus;
	}

	public String getTransSubStatus()
	{
		return transSubStatus;
	}

	public String getTransType()
	{
		return transType;
	}

	public void setDestinationCountry(String string)
	{
		destinationCountry = string;
	}

	public void setPaymentMethod(String string)
	{
		paymentMethod = string;
	}

	public void setPrimaryAccount(String string)
	{
		primaryAccount = string;
	}

	public void setProfileId(String string)
	{
		profileId = string;
	}

	public void setSecondaryAccount(String string)
	{
		secondaryAccount = string;
	}

	public void setSSN(String string)
	{
		SSN = string;
	}

	public void setTransStatus(String string)
	{
		transStatus = string;
	}

	public void setTransSubStatus(String string)
	{
		transSubStatus = string;
	}

	public void setTransType(String string)
	{
		transType = string;
	}

	public String getReceiverLastName()
	{
		return receiverLastName;
	}

	public void setReceiverLastName(String string)
	{
		receiverLastName = string;
	}

	public String getSubmitSearch()
	{
		return submitSearch;
	}

	public void setSubmitSearch(String string)
	{
		submitSearch = string;
	}

	public String getMaximumAmount()
	{
		return maximumAmount;
	}

	public String getMinimumAmount()
	{
		return minimumAmount;
	}

	public String getSenderIP()
	{
		return senderIP;
	}

	public void setMaximumAmount(String s)
	{
		maximumAmount = s;
	}

	public void setMinimumAmount(String s)
	{
		minimumAmount = s;
	}

	public void setSenderIP(String string)
	{
		senderIP = string;
	}

	public String getRcvAgcyCode()
	{
		return rcvAgcyCode;
	}

	public void setRcvAgcyCode(String string)
	{
		rcvAgcyCode = string;
	}

	public String getSenderRT()
	{
		return senderRT;
	}

	public void setSenderRT(String string)
	{
		senderRT = string;
	}

	public String getSortBy()
	{
		return sortBy;
	}

	public void setSortBy(String string)
	{
		sortBy = string;
	}

	public String getUseScore()
	{
		return useScore;
	}

	public void setUseScore(String string)
	{
		useScore = string;
	}

	public String getLowScore()
	{
		return lowScore;
	}

	public void setLowScore(String string)
	{
		lowScore = string;
	}

	public String getHighScore()
	{
		return highScore;
	}

	public void setHighScore(String string)
	{
		highScore = string;
	}

	public String getSysAutoRsltCode()
	{
		return sysAutoRsltCode == null ? "" : sysAutoRsltCode;
	}

	public void setSysAutoRsltCode(String string)
	{
		sysAutoRsltCode = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);
		selDates = "S";
		minimumAmount = "0";
		maximumAmount = "0";
		useScore = "N";
		lowScore = "0";
		highScore = "0";

		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
		long now = System.currentTimeMillis();
		long nintyDays = 90L * 24L * 60L * 60L * 1000L;
		long oneDay = 24L * 60L * 60L * 1000L;
		beginDateText = df.format(new Date(now - nintyDays));
		endDateText = df.format(new Date(now + oneDay));
		createBeginDateText = beginDateText;
		createEndDateText = endDateText;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (submitSearch != null)
		{

			if (!StringHelper.isNullOrEmpty(SSN))
			{
				if (StringHelper.containsNonDigits(SSN))
				{
					errors.add("SSN", new ActionError("errors.integer", "SSN Mask"));
				} else if (SSN.length() != 4)
				{
					String[] parm = { "SSN Mask", "4" };
					errors.add(	"SSN",new ActionError("error.length.wrong", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(primaryAccount))
			{
				if (StringHelper.containsNonDigits(primaryAccount))
				{
					errors.add("primaryAccount", new ActionError("errors.integer","Primary Account Mask"));
				} else if (primaryAccount.length() != 4)
				{
					String[] parm = { "Primary Account Mask", "4" };
					errors.add("primaryAccount",new ActionError("error.length.wrong", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(secondaryAccount))
			{
				if (StringHelper.containsNonDigits(secondaryAccount))
				{
					errors.add("secondaryAccount",	new ActionError("errors.integer","Secondary Account Mask"));
				} else if (secondaryAccount.length() != 4)
				{
					String[] parm = { "Secondary Account Mask", "4" };
					errors.add("secondaryAccount",new ActionError("error.length.wrong", parm));
				}
			}

			if (!StringHelper.isNullOrEmpty(referenceNumber) )
			{
				if (referenceNumber.length() != 8)
				{
					String[] parm = { "Reference Number", "8" };
					errors.add(	"referenceNumber",new ActionError("error.length.wrong", parm));
				}
				else if (StringHelper.containsNonDigits(referenceNumber)) {
					String[] parm = { "Reference Number" };
					errors.add(	"referenceNumber",new ActionError("errors.integer", parm));					
				}
			}
		
			if ("Y".equalsIgnoreCase(useScore))
			{
				if (StringHelper.isNullOrEmpty(lowScore)
					|| StringHelper.isNullOrEmpty(highScore))
				{
					errors.add("score",	new ActionError("errors.required","Low and High Scores"));
				} else if (
					StringHelper.containsNonDigits(lowScore) || StringHelper.containsNonDigits(highScore))
				{
					errors.add("score",	new ActionError("errors.integer","Low and High Scores"));
				} else if (lowScore.length() > 4 || highScore.length() > 4)
				{
					String[] parms = { "Low and High Scores", "4" };
					errors.add("score",	new ActionError("errors.maxlength", parms));
				} else if (
					Integer.parseInt(lowScore) > Integer.parseInt(highScore))
				{
					String[] parms = { "Low Score", "High Score" };
					errors.add("score",	new ActionError("errors.seq.wrong", parms));
				}
			}

			if (errors.size() == 0)
			{
				DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy", true);
				try
				{
					if (dfmt.parse(beginDateText).after(dfmt.parse(endDateText)))
					{
						String[] parms = { beginDateText, endDateText };
						errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.begin.date.later.than.end.date",parms));
					}
					if (dfmt.parse(createBeginDateText).after(dfmt.parse(createEndDateText)))
					{
						String[] parms ={ createBeginDateText, createEndDateText };
						errors.add(	ActionErrors.GLOBAL_ERROR,
							new ActionError("error.begin.date.later.than.end.date",	parms));
					}
				} catch (ParseDateException e)
				{
					errors.add(ActionErrors.GLOBAL_ERROR,new ActionError("error.malformed.date"));
				}
			}
		}

		return errors;
	}
    /**
     * @return Returns the transPrcsCode.
     */
    public String getTranRvwPrcsCode() {
        return tranRvwPrcsCode;
    }
    /**
     * @param transPrcsCode The transPrcsCode to set.
     */
    public void setTranRvwPrcsCode(String tpc) {
        this.tranRvwPrcsCode = tpc;
    }
    /**
     * @return Returns the retrievalRequestCode.
     */
    public String getRetrievalRequestCode() {
        return retrievalRequestCode;
    }
    /**
     * @param retrievalRequestCode The retrievalRequestCode to set.
     */
    public void setRetrievalRequestCode(String rrc) {
        this.retrievalRequestCode = rrc;
    }
	/**
	 * @return Returns the RRN.
	 */
	public String getRRN() {
		return RRN;
	}
	/**
	 * @param rrn The RRN to set.
	 */
	public void setRRN(String rrn) {
		RRN = rrn;
	}
	
	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}


}