package emgadm.services4emg;

import emgshared.util.StringHelper;
import emgshared.simplesoap.RequestExceptionResponseBean;

/**
 * @author G.R.Svenddal
 * Created on Jul 14, 2005
 * SimpleSoapRequestException.java
 * 
 * */
public class SimpleSoapRequestException extends Exception  
{
   private String myMessage;
   private Throwable myCause;
   
   /**
    * @param message
    * @param cause
    * 
    * ignore constructing the super() with parameters. 
    * we don't care about it, we're only inheriting to 
    * provide Exception type behaviour. This exception will be 
    * caught by the SimpleSoapServlet and turned into a RequestExceptionResponseBean,
    * so the fields we provide are sufficient. The reason for ignoring our parent 
    * is that WSAD has the frustrating "feature" of not supporting JDK1.4 in a
    * J2EE1.3 web project. ( perhaps someone at IBM thought the J2EE version 
    * was tied to the JDK version. You would think IBM would know better. ;) 
    */
  
   
   public SimpleSoapRequestException(Throwable cause){
    // parameterless constructor that works with JDK1.3 
    // But I guess I don't really have to be this explicit.
     super(); 
     this.myCause = cause;
     this.myMessage = cause.getMessage();
   }
   
   public SimpleSoapRequestException(String message, Throwable cause)
   {
     this(cause);
     this.myMessage = message;
   }
   public SimpleSoapRequestException(String message) 
   {
      this.myMessage = message;
   }
   
   /**
    * Private Default Constructor: You must at least set a message.
    */
   private SimpleSoapRequestException()
   {
   }
  
   /**
    * Fixup a RequestExceptionResponseBean. 
    */
   public void fixupRERBean(RequestExceptionResponseBean rB){
      if ( this.myCause != null )
      {
         // tip o' the hat to Steve's handy method.
         rB.setExceptionStackDump(
         StringHelper.getExceptionDumpMsg(myCause)); 
        rB.setExceptionType(myCause.getClass());
      }
      rB.setUserMessage(this.myMessage);
   }
}
