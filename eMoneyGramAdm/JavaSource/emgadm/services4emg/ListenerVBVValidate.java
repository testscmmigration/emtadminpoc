package emgadm.services4emg;

import java.util.TooManyListenersException;

import emgadm.services.ServiceFactory;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.CreditCard;
import emgshared.services.ConsumerAccountService;
import emgshared.services.verifiedbyvisa.CyberSourceVBVServiceImpl;
import emgshared.simplesoap.AuthValidateRequest;

/**
 * @author G.R.Svenddal
 * Created on Jul 14, 2005
 * ListenerVBVValidate.java
 *  
 * 
 *  */
public class ListenerVBVValidate implements SSRequestListener
{
	static {
		try
		{
			SimpleSoapServlet.addListener(new ListenerVBVValidate());
		} catch (TooManyListenersException e)
		{
			EMGSharedLogger.getLogger(
				ListenerVBVValidate.class.toString()).error(
				"ListenerVBVValidate added twice!");
		}
	}

	public ListenerVBVValidate()
	{
	}

	public String getRequestBeanTypeName()
	{
		return emgshared.simplesoap.AuthValidateRequest.class.getName();
	}

	public Object processCall(Object requestBean, String contextRoot)
		throws SimpleSoapRequestException
	{
		try
		{
			AuthValidateRequest avr = (AuthValidateRequest) requestBean;
			int accountId = avr.getAccount();

			ConsumerAccountService accountService =
				emgshared
					.services
					.ServiceFactory
					.getInstance()
					.getConsumerAccountService();

			String me = this.getClass().getName();
			CreditCard card =
				ServiceFactory
					.getInstance()
					.getSecuredCreditCardService()
					.getCreditCard(
					accountService.getCreditCardAccount(accountId, me),
					me);

			return CyberSourceVBVServiceImpl
				.getInstance()
				.validateAuthentication(
				card,
				avr.getSignedPARes(),
				avr.getMerchantReferenceCode());
		} catch (Throwable e)
		{
			throw new SimpleSoapRequestException(
				"AuthValidateRequest exception",
				e);
		}
	}
}
