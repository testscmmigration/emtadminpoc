package emgadm.services4emg;

/**
 * @author G.R.Svenddal
 * Created on Jul 14, 2005
 * SSRequestListenerInterface.java
 * @param requestBean: must be a proper Java bean.
 * @return returns a proper Java bean with response content. 
 * 
 **/
public interface SSRequestListener
extends java.util.EventListener
{
   public String getRequestBeanTypeName();
   
   public Object processCall(Object requestBean, String contextRoot)
   	throws SimpleSoapRequestException ;
}
