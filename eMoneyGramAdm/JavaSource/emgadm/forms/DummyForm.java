package emgadm.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class DummyForm extends EMoneyGramAdmBaseValidatorForm
{

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{

		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		return errors;
	}
}
