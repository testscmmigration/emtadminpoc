package emgadm.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class HeartBeatMonForm extends EMoneyGramAdmBaseValidatorForm {
	private String buildDate;
	private String action;
	private String password;
	private String propPassword;

	public String getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(String string) {
		buildDate = string;
	}

	public String getAction() {
		return action == null ? "" : action;
	}

	public void setAction(String string) {
		action = string;
	}

	public String getPassword() {
		return password == null ? "" : password;
	}

	public void setPassword(String string) {
		password = string;
	}

	public String getPropPassword() {
		return propPassword == null ? "" : propPassword;
	}

	public void setPropPassword(String string) {
		propPassword = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		buildDate = "";
		action = "";
		password = "";
		propPassword = "";

	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		return errors;
	}
}
