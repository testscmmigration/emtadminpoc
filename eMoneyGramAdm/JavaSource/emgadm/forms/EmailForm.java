package emgadm.forms;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class EmailForm extends EMoneyGramAdmBaseValidatorForm {
	private String uid;
	private String email;
	private String firstName;
	private String lastName;

	/**
	 * @return
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param string
	 */
	public void setUid(String string) {
		uid = string;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}


	public void reset(ActionMapping mapping, HttpServletRequest request) {
		uid = "";
		email = "";
		firstName = "";
		lastName = "";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		return errors;
	}
}
