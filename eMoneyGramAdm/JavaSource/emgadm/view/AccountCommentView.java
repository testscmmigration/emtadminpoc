/*
 * Created on Mar 3, 2005
 *
 */
package emgadm.view;

import java.util.Date;

import emgshared.model.AccountComment;
import emgshared.services.CreditCardServiceResponseImpl;

/**
 * @author A131
 *
 */
public class AccountCommentView
{
	private String id;
	private String accountId;
	private String reasonCode;
	private String reasonDescription;
	private String text;
	private String formattedText;
	private Date createDate;
	private String createdBy;

	public AccountCommentView()
	{
		super();
	}

	public AccountCommentView(AccountComment comment)
	{
		super();
		this.id = String.valueOf(comment.getId());
		this.accountId = String.valueOf(comment.getAccountId());
		this.reasonCode = comment.getReasonCode();
		this.reasonDescription = comment.getReasonDescription();
		this.text = comment.getText();
		setFormattedText(getText());
		this.createDate = comment.getCreateDate();
		this.createdBy = comment.getCreatedBy();
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public String getId()
	{
		return id;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public String getReasonDescription()
	{
		return reasonDescription;
	}

	public String getText()
	{
		return text;
	}

	public String getFormattedText() {
		return formattedText;
	}

	public void setFormattedText(String formattedText) {
		if(formattedText.lastIndexOf(CreditCardServiceResponseImpl.ACCEPTED)!=0){
			this.formattedText=getText().replaceAll(CreditCardServiceResponseImpl.ACCEPTED, "<b>"+CreditCardServiceResponseImpl.ACCEPTED+"</b>");
		} else if(formattedText.lastIndexOf(CreditCardServiceResponseImpl.REJECTED)!=0){
			this.formattedText=getText().replaceAll(CreditCardServiceResponseImpl.REJECTED, "<b>"+CreditCardServiceResponseImpl.REJECTED+"</b>");
		}
	}
}
