/*
 * Created on Mar 3, 2005
 *
 */
package emgadm.view;

import java.util.Date;

import emgshared.model.ConsumerProfileComment;

/**
 * @author A131
 *
 */
public class ConsumerProfileCommentView
{
	private String id;
	private String custId;
	private String reasonCode;
	private String reasonDescription;
	private String text;
	private Date createDate;
	private String createdBy;

	public ConsumerProfileCommentView()
	{
		super();
	}

	public ConsumerProfileCommentView(ConsumerProfileComment comment)
	{
		super();
		this.id = String.valueOf(comment.getId());
		this.custId = String.valueOf(comment.getCustId());
		this.reasonCode = comment.getReasonCode();
		this.reasonDescription = comment.getReasonDescription();
		this.text = comment.getText();
		this.createDate = comment.getCreateDate();
		this.createdBy = comment.getCreatedBy();
	}

	public Date getCreateDate()
	{
		return createDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public String getCustId()
	{
		return custId;
	}

	public String getId()
	{
		return id;
	}

	public String getReasonCode()
	{
		return reasonCode;
	}

	public String getReasonDescription()
	{
		return reasonDescription;
	}

	public String getText()
	{
		return text;
	}
}
