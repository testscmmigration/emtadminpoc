package emgadm.view;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import emgshared.model.Transaction;
import emgshared.model.TransactionScore;

/**
 * @author A131
 */

public class TransactionView {
	private String tranId;

	private String ownerId;

	private Date createDate;

	private String senderName;

	private String custId;

	private String receiver;

	private String countryCode;

	private String statusDesc;

	private String subStatusDesc;

	private String type;

	private String totalAmount;

	private Date statusDate;

	private String primaryFundSource;

	private int transScores;

	private String sysAutoRsltCode;

	private List cmntList;

	private TransactionScore tranScore;

	private String partnerSiteId;

	public TransactionView() {
		super();
	}

	public TransactionView(Transaction transaction) {
		super();
		this.tranId = String.valueOf(transaction.getEmgTranId());
		this.custId = String.valueOf(transaction.getCustId());
		this.ownerId = transaction.getCsrPrcsUserid();
		this.createDate = transaction.getCreateDate();
		this.type = transaction.getEmgTranTypeCode();
		this.countryCode = transaction.getRcvISOCntryCode();
		this.statusDesc = transaction.getTranStatDesc();
		this.subStatusDesc = transaction.getTranSubStatDesc();
		this.primaryFundSource = transaction.getEmgTranTypeDesc();
		this.statusDate = transaction.getTranStatDate();
		this.sysAutoRsltCode = transaction.getSysAutoRsltCode();
		this.tranScore = transaction.getTranScore();
		this.cmntList = transaction.getCmntList();
		this.transScores = transaction.getTransScores();
		this.partnerSiteId = transaction.getPartnerSiteId();

		{
			StringBuffer fullName;
			fullName = new StringBuffer();
			String firstName = StringUtils.trimToEmpty(transaction.getSndCustFrstName());
			String lastName = StringUtils.trimToEmpty(transaction.getSndCustLastName());
			fullName.append(firstName);
			if (lastName != null && lastName.length() > 0) {
				fullName.append(' ');
				fullName.append(lastName);
			}
			this.senderName = fullName.toString();
		}
		if ("EPSEND".equalsIgnoreCase(transaction.getEmgTranTypeCode())) {
			this.receiver = transaction.getRcvAgcyCode();
		} else {
			StringBuffer fullName = new StringBuffer();
			String firstName = StringUtils.trimToEmpty(transaction.getRcvCustFrstName());
			String middleName = StringUtils.trimToEmpty(transaction.getRcvCustMidName());
			String lastName = StringUtils.trimToEmpty(transaction.getRcvCustLastName());
			fullName.append(firstName);
			if (middleName != null && middleName.length() > 0) {
				fullName.append(' ');
				fullName.append(middleName);
			}
			if (lastName != null && lastName.length() > 0) {
				fullName.append(' ');
				fullName.append(lastName);
			}
			this.receiver = fullName.toString();
		}

		NumberFormat df = new DecimalFormat("#,##0.00");
		this.totalAmount = df.format(transaction.getSndFaceAmt().doubleValue());
	}

	/**
	 * @return
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @param string
	 */
	public void setOwnerId(String string) {
		ownerId = string;
	}

	/**
	 * @return
	 */
	public String getTranId() {
		return tranId;
	}

	/**
	 * @param string
	 */
	public void setTranId(String string) {
		tranId = string;
	}

	/**
	 * @return
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param string
	 */
	public void setSenderName(String string) {
		senderName = string;
	}

	/**
	 * @return
	 */
	public String getCustId() {
		return custId == null ? "" : custId;
	}

	/**
	 * @param string
	 */
	public void setCustId(String string) {
		custId = string;
	}

	/**
	 * @return
	 */
	public String getReceiver() {
		return receiver;
	}

	/**
	 * @param string
	 */
	public void setReceiver(String string) {
		receiver = string;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String string) {
		countryCode = string;
	}

	/**
	 * @return
	 */
	public String getStatusDesc() {
		return statusDesc;
	}

	/**
	 * @param string
	 */
	public void setStatusDesc(String string) {
		statusDesc = string;
	}

	/**
	 * @return
	 */
	public String getSubStatusDesc() {
		return subStatusDesc;
	}

	/**
	 * @param string
	 */
	public void setSubStatusDesc(String string) {
		subStatusDesc = string;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * @return
	 */
	public String getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param string
	 */
	public void setTotalAmount(String string) {
		totalAmount = string;
	}

	public Date getStatusDate() {
		return statusDate;
	}

	public String getPrimaryFundSource() {
		return primaryFundSource;
	}

	public void setCreateDate(Date date) {
		createDate = date;
	}

	public void setPrimaryFundSource(String string) {
		primaryFundSource = string;
	}

	public void setStatusDate(Date date) {
		statusDate = date;
	}

	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @return
	 */
	public List getCmntList() {
		return cmntList;
	}

	/**
	 * @param list
	 */
	public void setCmntList(List list) {
		cmntList = list;
	}

	/**
	 * @return
	 */
	public int getTransScores() {
		return transScores;
	}

	/**
	 * @param i
	 */
	public void setTransScores(int i) {
		transScores = i;
	}

	/**
	 * @return
	 */
	public String getSysAutoRsltCode() {
		return sysAutoRsltCode;
	}

	/**
	 * @param string
	 */
	public void setSysAutoRsltCode(String string) {
		sysAutoRsltCode = string;
	}

	/**
	 * @return
	 */
	public TransactionScore getTranScore() {
		return tranScore;
	}

	/**
	 * @param score
	 */
	public void setTranScore(TransactionScore score) {
		tranScore = score;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

}
