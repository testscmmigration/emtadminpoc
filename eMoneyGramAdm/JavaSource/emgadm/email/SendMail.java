/*
 * Created on Dec 20, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.email;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import emgshared.property.EMTSharedContainerProperties;

public class SendMail
{

	//********************//
	//** class members ***//
	//********************//
	private String pMailHost;
	private String pMailFrom;
	private String pMailTo;
	private String execStackTrace = null;
	private String subjectMessage = "";
	private String textMessage = "";

	// construct with passed in values for from and to
	public SendMail(String from, String to)
	{
		pMailFrom = from;
		pMailTo = to;
		pMailHost = EMTSharedContainerProperties.getMailHost();
	}

	// construct using property file values for from, to , host
	public SendMail()
	{
		pMailFrom = EMTSharedContainerProperties.getMailFrom();
		pMailHost = EMTSharedContainerProperties.getMailHost();
	}

	public SendMail(Exception e)
	{

		pMailFrom = EMTSharedContainerProperties.getMailFrom();
		pMailHost = EMTSharedContainerProperties.getMailHost();
		System.out.println("in exception init");
		e.printStackTrace();
		ByteArrayOutputStream newOut = new ByteArrayOutputStream();
		e.printStackTrace(new PrintStream(newOut));
		execStackTrace = newOut.toString();
	}

	// Sends the email message
	public void sendMail(String sendMessage, String subject) throws Exception
	{

		try
		{
			// Set the host
			Properties props = new Properties();
			props.put("mail.host", pMailHost);
			Session session = Session.getDefaultInstance(props, null);
			// Construct the message
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(pMailFrom));
			msg.setRecipients(
				Message.RecipientType.TO,
				InternetAddress.parse(pMailTo, false));
			msg.setSubject(subject);
			if (execStackTrace != null)
				msg.setText(sendMessage + "\n\n" + execStackTrace);
			else
				msg.setText(sendMessage);
			msg.setHeader("X-Mailer", "eMoney Transfer");
			msg.setSentDate(new Date());

			// Send the email message
			Transport.send(msg);

		} catch (Exception e)
		{
			System.out.println(
				"An error has occured in sending mail: " + e.toString());
			throw e;
		}
	}

	/**
	 * Method setTextMessage.
	 * @param string
	 */
	public void setTextMessage(String string)
	{
		this.textMessage = string;
	}

	/**
	 * Method setSubjectMessage.
	 * @param string
	 */
	public void setSubjectMessage(String string)
	{
		this.subjectMessage = string;
	}

	/**
	 * Returns the subjectMessage.
	 * @return String
	 */
	public String getSubjectMessage()
	{
		return subjectMessage;
	}

	/**
	 * Returns the textMessage.
	 * @return String
	 */
	public String getTextMessage()
	{
		return textMessage;
	}

}
