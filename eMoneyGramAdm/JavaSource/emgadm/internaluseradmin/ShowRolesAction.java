package emgadm.internaluseradmin;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.RoleManager;
import emgadm.model.UserProfile;

/**
 * @version 	1.0
 * @author
 */
public class ShowRolesAction extends EMoneyGramAdmBaseAction
{
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		UserProfile up = getUserProfile(request);

		List roles = RoleManager.getFreshedRoles();
		Collections.sort(roles);

		request.setAttribute("roles", roles);
		request.setAttribute("auth", 
			up.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_EDIT_ROLE) ? "Y" : "N");

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
