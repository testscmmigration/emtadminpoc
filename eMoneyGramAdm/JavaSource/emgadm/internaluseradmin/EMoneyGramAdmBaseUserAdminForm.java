package emgadm.internaluseradmin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.model.UserProfileFactory;

/**
 * @author A131
 *
 */
public class EMoneyGramAdmBaseUserAdminForm
	extends EMoneyGramAdmBaseValidatorForm
{
	private UserProfile user;
//	private String newUserID = null;

	public UserProfile getUser()
	{
		return user;
	}

	public void setUser(UserProfile profile)
	{
		user = profile;
	}

	public String getUserId()
	{
		return user.getUID().trim();
	}

	public void setUserId(String UID)
	{
		user.setUID(UID);
	}

	public String getFirstName()
	{
		return user.getFirstName();
	}

	public String getLastName()
	{
		return user.getLastName();
	}

	public void setFirstName(String string)
	{
		user.setFirstName(string);
	}

	public void setLastName(String string)
	{
		user.setLastName(string);
	}

	public List<String> getRoleIds() {
		List<String> ids = new ArrayList<String>();

		if (user.getRoles() != null) {
			for (Role role : user.getRoles()) {
				ids.add(role.getId());
			}
		}
		return ids;
	}

	public String getCommaSeparatedRoleIds() {
		List<String> roleIds = getRoleIds();
		StringBuilder str = new StringBuilder(roleIds.size() * 10);
		boolean afterFirst = false;
		for (String string : roleIds) {
			if (afterFirst) {
				str.append(", ");
			} else {
				afterFirst = true;
			}
			str.append(string);
		}
		return str.toString();
	}

	public void setRoleIds(List<String> ids) {
		user.setRoles(getRoles(ids));
	}

	public String getGuid()
	{
		return user.getGuid();
	}

	public void setGuid(String string)
	{
		user.setGuid(string);
	}

	public String getEmailAddress()
	{
		return user.getEmailAddress();
	}

	public void setEmailAddress(String string)
	{
		user.setEmailAddress(string);
	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1)
	{
		super.reset(arg0, arg1);
		user = UserProfileFactory.createNewUserProfile();
	}
}
