package emgadm.internaluseradmin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.constants.EMoneyGramAdmSessionConstants;
import emgadm.model.ApplicationCommand;
import emgadm.model.CommandAndRoleMatrixBean;
import emgadm.model.CommandFactory;
import emgadm.model.Program;
import emgadm.model.ProgramFactory;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgadm.services.SecurityService;
import emgadm.services.ServiceFactory;
import emgadm.util.StringHelper;

public class ShowCommandAndRoleMatrixAction
		extends EMoneyGramAdmBaseAction {

	public static final String EMT_COMMAND = "showCommandAndRoleMatrix";

	public ActionForward execute(
				ActionMapping mapping,
				ActionForm f,
				HttpServletRequest request,
				HttpServletResponse response)
			throws Exception {

		SecurityService securityService = ServiceFactory.getInstance().getSecurityService();
		ShowCommandAndRoleMatrixForm form = (ShowCommandAndRoleMatrixForm) f;

		UserProfile user = (UserProfile) request.getSession().getAttribute(EMoneyGramAdmSessionConstants.USER_PROFILE);
		List<Program> authorizedProgramsByUserAndCommand = securityService.getAuthorizedProgramsByUserAndCommand(user, CommandFactory.getInstance(EMT_COMMAND));
		if (authorizedProgramsByUserAndCommand.isEmpty()) {
			authorizedProgramsByUserAndCommand.add(ProgramFactory.createProgram("MGO", "MGO", "MGO"));
		}
		Program program = form.getProgram();
		if (program == null) {
			if (authorizedProgramsByUserAndCommand.size() > 0) {
				program = authorizedProgramsByUserAndCommand.get(0);
				form.setProgram(program);
			}
		}
		List<Role> roleList = securityService.getRolesByProgram(program);
		String[] roleName = buildRoleNames(roleList);
		form.setRoleName(roleName);

		form.setPrograms(authorizedProgramsByUserAndCommand);

		List<CommandAndRoleMatrixBean> cmdList = new ArrayList<CommandAndRoleMatrixBean>();
		Iterator<ApplicationCommand> iter2 = securityService.getAllApplicationCommands().iterator();
		while (iter2.hasNext()) {
			CommandAndRoleMatrixBean matrix = null;
			ApplicationCommand cmd = iter2.next();
			if (cmd.isAuthorizationRequired()) {
				matrix = new CommandAndRoleMatrixBean();
				matrix.setCommand(cmd.getId());
				matrix.setMenuGroupName(cmd.getMenuGroupName());
				matrix.setMenuLevel(cmd.getMenuLevel());
				if (StringHelper.isNullOrEmpty(cmd.getDisplayName())) {
					matrix.setDisplayName(cmd.getDescription());
				} else {
					matrix.setDisplayName(cmd.getDisplayName());
				}

				String[] rolePermission = new String[roleList.size()];
				int idx = 0;
				Iterator<Role> iter1 = roleList.iterator();
				while (iter1.hasNext()) {
					Role cmdRole = iter1.next();
					rolePermission[idx] = cmdRole.hasPermission(cmd.getId()) ? "X" : "";
					idx++;
				}
				matrix.setRolePermission(rolePermission);
				cmdList.add(matrix);
			}
		}

//		Collections.sort(cmdList);
		Collections.sort(cmdList, new MenuGroupAndRoleDescriptionComparator());  
		request.setAttribute("cmdList", cmdList);

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private String[] buildRoleNames(List<Role> roleList) {
		String[] roleNames = new String[roleList.size()];
		int roleIndex = 0;
		Iterator<Role> iter = roleList.iterator();

		while (iter.hasNext()) {
			Role role = iter.next();
			roleNames[roleIndex++] = role.getHTMLWrappedShortName();
		}
		return roleNames;
	}
}
