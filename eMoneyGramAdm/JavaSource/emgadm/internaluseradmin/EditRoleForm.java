package emgadm.internaluseradmin;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.ImageButtonBean;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class EditRoleForm extends EMoneyGramAdmBaseValidatorForm
{
	private static final String[] RESET_ARRAY = new String[0];

	private String action;
	private String saveAction;
	private String dblClickAction;
	private String roleId;
	private String roleName;
	private String roleDesc;
	private String[] unauthCmdId;
	private String[] authCmdId;
	private ImageButtonBean submitAuth = new ImageButtonBean();
	private ImageButtonBean submitUnauth = new ImageButtonBean();
	private ImageButtonBean submitSave = new ImageButtonBean();
	private ImageButtonBean submitExit = new ImageButtonBean();

	public String getAction()
	{
		return action;
	}

	public void setAction(String string)
	{
		action = string;
	}

	public String getSaveAction()
	{
		return saveAction;
	}

	public void setSaveAction(String string)
	{
		saveAction = string;
	}

	public String getDblClickAction()
	{
		return dblClickAction;
	}

	public void setDblClickAction(String string)
	{
		dblClickAction = string;
	}

	public String getRoleId()
	{
		return roleId;
	}

	public void setRoleId(String string)
	{
		roleId = string;
	}

	public String getRoleName()
	{
		return roleName;
	}

	public void setRoleName(String string)
	{
		roleName = string;
	}

	public String getRoleDesc()
	{
		return roleDesc;
	}

	public void setRoleDesc(String string)
	{
		roleDesc = string;
	}

	public String[] getUnauthCmdId()
	{
		return unauthCmdId;
	}

	public String getUnauthCmdId(int i)
	{
		return unauthCmdId[i];
	}

	public void setUnauthCmdId(String[] strings)
	{
		unauthCmdId = strings;
	}

	public String[] getAuthCmdId()
	{
		return authCmdId;
	}

	public String getAuthCmdId(int i)
	{
		return authCmdId[i];
	}

	public void setAuthCmdId(String[] strings)
	{
		authCmdId = strings;
	}

	public ImageButtonBean getSubmitAuth()
	{
		return this.submitAuth;
	}

	public void setSubmitAuth(ImageButtonBean button)
	{
		this.submitAuth = button;
	}

	public ImageButtonBean getSubmitUnauth()
	{
		return this.submitUnauth;
	}

	public void setSubmitUnauth(ImageButtonBean button)
	{
		this.submitUnauth = button;
	}

	public ImageButtonBean getSubmitSave()
	{
		return this.submitSave;
	}

	public void setSubmitSave(ImageButtonBean button)
	{
		this.submitSave = button;
	}

	public ImageButtonBean getSubmitExit()
	{
		return this.submitExit;
	}

	public void setSubmitExit(ImageButtonBean button)
	{
		this.submitExit = button;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		action = "";
		saveAction = "";
		roleId = "";
		roleName = "";
		roleDesc = "";
		authCmdId = RESET_ARRAY;
		unauthCmdId = RESET_ARRAY;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		return errors;
	}
}
