package emgadm.internaluseradmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.dataaccessors.RoleManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgshared.model.UserActivityType;

/**
 * @version 	1.0
 * @author
 */
public class InternalUserAdminAction
		extends EMoneyGramAdmBaseAction {

	public ActionForward execute(ActionMapping mapping,
				ActionForm actionForm,
				HttpServletRequest request,
				HttpServletResponse response)
			throws Exception {

		ActionForward forward =	mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		InternalUserAdminForm internalUserAdminForm = (InternalUserAdminForm) actionForm;
		List<Role> freshedRoles = RoleManager.getFreshedRoles();
		request.getSession().setAttribute("internalRoles", freshedRoles);

		UserProfileManager userManager = getUserProfileManager(request);
		UserProfile user = internalUserAdminForm.getUser();

		if (internalUserAdminForm.getEdit() != null) {
			try {
				List<Role> newRoles = user.getRoles();
				user = userManager.getUser(user.getUID());
				internalUserAdminForm.setUser(user);
				userManager.updateUser(user, newRoles);
				ActionMessages messages = getActionMessages(request);
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("messages.user.updated", user.getUID()));
				// try inserting a user activity log
				try {
				    StringBuffer sb = new StringBuffer(256);
				    sb.append("<USER_ID>");
				    sb.append(user.getUID());
				    sb.append("<FIRST_NAME>");
				    sb.append(user.getFirstName());
				    sb.append("</FIRST_NAME><LAST_NAME>");
				    sb.append(user.getLastName());
				    sb.append("</LAST_NAME>");
				    sb.append("<EMAIL>");
				    sb.append(user.getEmailAddress());
				    sb.append("</EMAIL><ROLES>");
				    for (Role newRole : newRoles) {
					    sb.append("<ROLE>");
				    	sb.append(newRole.getDisplayName());
					    sb.append("</ROLE>");
				    }
				    sb.append("</ROLES>");
				    super.insertActivityLog(this.getClass(), request,sb.toString(), 
				    		                UserActivityType.EVENT_TYPE_USERADMINCHANGEROLE, user.getGuid());
                } catch (Exception e) {
                }

				saveMessages(request, messages);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (internalUserAdminForm.getCancel() != null) {
			if (internalUserAdminForm.getCancel().equals("edit")) {//$NON-NLS-1$
				forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_INTERNAL_USER_SEARCH);
			} else {
				forward = mapping.findForward(EMoneyGramAdmForwardConstants.GLOBAL_FORWARD_HOME_PAGE);
			}
		} else if (internalUserAdminForm.getView() != null) {
			user = userManager.getUser(user.getUID());
			internalUserAdminForm.setUser(user);
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_INTERNAL_USER_ADMIN);
		} else {
			internalUserAdminForm.reset(mapping, request);
			forward = mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_INTERNAL_USER_ADMIN);
		}
		return forward;
	}

//	private void log(
//		HttpServletRequest request,
//		UserProfile user,
//		String action)
//		throws SQLException, NamingException
//	{
//		Object obj[] = new Object[6];
//		obj[0] = user.getUID();
//		obj[1] = user.getFirstName();
//		obj[2] = user.getLastName();
//		obj[3] = user.getWebStatus();
//		obj[4] = user.getRole().getDisplayName();
//		obj[5] = String.valueOf(user.getWebPasswordRetryCount());
//		//gk		log(request, action, obj, null, null);
//	}
}
