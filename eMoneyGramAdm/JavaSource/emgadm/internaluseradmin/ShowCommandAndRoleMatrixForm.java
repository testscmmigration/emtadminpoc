package emgadm.internaluseradmin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.model.Program;

public class ShowCommandAndRoleMatrixForm
		extends EMoneyGramAdmBaseValidatorForm {

	private String supress;
	private String[] roleName;
	private Program program;
	private List<Program> programs;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		supress = "Y";
		roleName = null;
		program = null;
		programs = new ArrayList<Program>();
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = super.validate(mapping, request);
		if (errors == null)
			errors = new ActionErrors();
		return errors;

	}

	public String[] getRoleName() {
		return roleName;
	}

	public void setRoleName(String[] strings) {
		roleName = strings;
	}

	public String getRoleName(int i) {
		return roleName[i];
	}

	public String getSupress() {
		return supress == null ? "Y" : supress;
	}

	public void setSupress(String string) {
		supress = string;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public List<Program> getPrograms() {
		return programs;
	}

	public void setPrograms(List<Program> programs) {
		this.programs = programs;
	}

}
