package emgadm.internaluseradmin;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.dataaccessors.InvalidGuidException;
import emgadm.dataaccessors.RoleManager;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

/**
 * Form bean for a Struts application.
 * Users may access 11 fields on this form:
 * <ul>
 * <li>webStatus - [your comment here]
 * <li>emailAddress - [your comment here]
 * <li>roleId - [your comment here]
 * <li>loginFailureCount - [your comment here]
 * <li>userId - [your comment here]
 * <li>confirmPassword - [your comment here]
 * <li>edit - [your comment here]
 * <li>firstName - [your comment here]
 * <li>password - [your comment here]
 * <li>lastName - [your comment here]
 * <li>cancel - [your comment here]
 * </ul>
 * @version 	1.0
 * @author
 */
public class InternalUserAdminForm extends EMoneyGramAdmBaseUserAdminForm {
	public InternalUserAdminForm() {
        try {
            setRoles(RoleManager.getInternalRoles());
        } catch (InvalidGuidException e) {
            EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
        } catch (DataSourceException e) {
            EMGSharedLogger.getLogger(this.getClass()).error(e.getMessage(), e);
        }

	}

	public void reset(ActionMapping arg0, HttpServletRequest arg1) {
		super.reset(arg0, arg1);
		getUser().setInternalUser(true);
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if (getAdd() != null || getEdit() != null) {
			return super.validate(mapping, request);
		}
		
		return errors;
	}

}
