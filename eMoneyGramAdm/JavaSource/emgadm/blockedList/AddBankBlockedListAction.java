package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddBankBlockedListAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		AddBankBlockedListForm addBankBlockedListForm =
			(AddBankBlockedListForm) form;
		ServletContext sc = request.getSession().getServletContext();
		UserProfile up = getUserProfile(request);
		Collection accts = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		ObfuscationService os = sf.getObfuscationService();
		FraudService fs = sf.getFraudService();

		if (!StringHelper
			.isNullOrEmpty((String) request.getAttribute("abaNumber")))
			addBankBlockedListForm.setAbaNumber(
				(String) request.getAttribute("abaNumber"));

		if (!StringHelper
			.isNullOrEmpty((String) request.getAttribute("acctNumber")))
			addBankBlockedListForm.setFullAcct(
				(String) request.getAttribute("acctNumber"));

		Collection blockingReasons = fs.getBlockedReasons(up.getUID(), "BANK");
		Collection blockingActions = fs.getBlockedStatus(up.getUID(), "BANK");

		sc.setAttribute("blockingReasons", blockingReasons);
		sc.setAttribute("blockingActions", blockingActions);

		String paramAba = request.getParameter("aba");
		String paramAcct = request.getParameter("acct");

		if (paramAba != null)
		{

			if (paramAcct != null)
			{
				accts =
					fs.getBlockedBankAccounts(
						up.getUID(),
						paramAba,
						os.getBankAccountHash(paramAcct),
						null);
			} else
			{
				accts =
					fs.getBlockedBankAccounts(
						up.getUID(),
						paramAba,
						null,
						"****");
			}
			if (accts.size() > 0)
			{
				BlockedBean acct = (BlockedBean) accts.toArray()[0];
				addBankBlockedListForm.setAbaNumber(paramAba);
				addBankBlockedListForm.setFullAcct(paramAcct);
				addBankBlockedListForm.setBlockingAction(acct.getStatusCode());
				addBankBlockedListForm.setBlockingReason(acct.getReasonCode());
				addBankBlockedListForm.setBlockingComment(acct.getComment());
				addBankBlockedListForm.setRiskLevel(
					acct.getFraudRiskLevelCode());
			}
		}

		if (!StringHelper
			.isNullOrEmpty(addBankBlockedListForm.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_BANK_BLOCKED_LIST);
		}

		if (!StringHelper
			.isNullOrEmpty(addBankBlockedListForm.getSubmitProcess()))
		{

			BlockedBean acct = new BlockedBean();
			acct.setAbaNumber(addBankBlockedListForm.getAbaNumber());
			if (!StringHelper
				.isNullOrEmpty(addBankBlockedListForm.getFullAcct()))
			{
				acct.setMaskText(
					addBankBlockedListForm.getFullAcct().substring(
						addBankBlockedListForm.getFullAcct().length() - 4,
						addBankBlockedListForm.getFullAcct().length()));
				acct.setHashText(
					os.getBankAccountHash(
						addBankBlockedListForm.getFullAcct()));
				acct.setEncryptedNumber(
					os.getBankAccountObfuscation(
						addBankBlockedListForm.getFullAcct()));
				acct.setClearText(addBankBlockedListForm.getFullAcct());
			}
			acct.setReasonCode(addBankBlockedListForm.getBlockingReason());

			for (Iterator it = blockingReasons.iterator(); it.hasNext();)
			{
				BlockedReason br = (BlockedReason) it.next();
				if (br
					.getReasonCode()
					.equals(addBankBlockedListForm.getBlockingReason()))
				{
					acct.setReasonDesc(br.getReasonDesc());
					break;
				}
			}

			acct.setComment(addBankBlockedListForm.getBlockingComment());
			acct.setStatusCode(addBankBlockedListForm.getBlockingAction());

			for (Iterator it = blockingActions.iterator(); it.hasNext();)
			{
				BlockedStatus bs = (BlockedStatus) it.next();
				if (bs
					.getBlockedStatusCode()
					.equals(addBankBlockedListForm.getBlockingAction()))
				{
					acct.setStatusDesc(bs.getBlockedStatusDesc());
					break;
				}
			}

			acct.setFraudRiskLevelCode(addBankBlockedListForm.getRiskLevel());

			request.getSession().setAttribute("blockedAcct", acct);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SAVE_BANK_BLOCKED_LIST);

		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
