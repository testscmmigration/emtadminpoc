package emgadm.blockedList;

public class BlockedListCustomer {

	private int custId;
	private String loginId;
	private String firstName;
	private String lastName;
	private String profileSubstatus;
	private String blockData;
	private String blockReason ;
	private String creditCard;
	private String accountNumber;
	private String altPhoneNumber;
	private String displayData;
	
	/**
	 * @return the displayData
	 */
	public String getDisplayData() {
		return displayData;
	}

	/**
	 * @param displayData the displayData to set
	 */
	public void setDisplayData(String displayData) {
		this.displayData = displayData;
	}

	public String getBlockReason() {
		return blockReason;
	}

	public String getBlockComment() {
		return blockComment;
	}

	public void setBlockComment(String blockComment) {
		this.blockComment = blockComment;
	}

	private String blockComment ;
	private String primaryPhoneNumber;

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getProfileSubstatus() {
		return profileSubstatus;
	}

	public void setProfileSubstatus(String profileSubstatus) {
		this.profileSubstatus = profileSubstatus;
	}

	public String getBlockData() {
		return blockData;
	}

	public void setBlockData(String blockData) {
		this.blockData = blockData;
	}

	public boolean equals(int custid) {

		return (this.custId == custid);

	}

	public String getPrimaryPhoneNumber() {
		return primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	/**
	 * @return the creditCard
	 */
	public String getCreditCard() {
		return creditCard;
	}

	/**
	 * @param creditCard the creditCard to set
	 */
	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the altPhoneNumber
	 */
	public String getAltPhoneNumber() {
		return altPhoneNumber;
	}

	/**
	 * @param altPhoneNumber the altPhoneNumber to set
	 */
	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	public void setBlockReason(String blockReason) {
		this.blockReason = blockReason;
	}


}
