package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddBinBlockedListAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		AddBinBlockedListForm addBinBlockedListForm =
			(AddBinBlockedListForm) form;
		ServletContext sc = request.getSession().getServletContext();
		UserProfile up = getUserProfile(request);
		Collection bins = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		Collection blockingReasons = fs.getBlockedReasons(up.getUID(), "BIN");
		Collection blockingActions = fs.getBlockedStatus(up.getUID(), "BIN");
		sc.setAttribute("blockingReasons", blockingReasons);
		sc.setAttribute("blockingActions", blockingActions);

		String paramBin = request.getParameter("bin");
		if (paramBin != null)
		{

			ObfuscationService os = sf.getObfuscationService();
			bins = fs.getBlockedBins(up.getUID(), os.getBinHash(paramBin));

			if (bins.size() > 0)
			{
				BlockedBean bin = (BlockedBean) bins.toArray()[0];
				addBinBlockedListForm.setBinText(paramBin);
				addBinBlockedListForm.setBlockingAction(bin.getStatusCode());
				addBinBlockedListForm.setBlockingReason(bin.getReasonCode());
				addBinBlockedListForm.setBlockingComment(bin.getComment());
				addBinBlockedListForm.setRiskLevel(bin.getFraudRiskLevelCode());
			}
		}

		if (!StringHelper
			.isNullOrEmpty(addBinBlockedListForm.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_BIN_BLOCKED_LIST);
		}

		if (!StringHelper
			.isNullOrEmpty(addBinBlockedListForm.getSubmitProcess()))
		{
			ObfuscationService os = sf.getObfuscationService();

			BlockedBean bin = new BlockedBean();
			bin.setHashText(
				os.getCreditCardHash(addBinBlockedListForm.getBinText()));
			bin.setEncryptedNumber(
				os.getCreditCardObfuscation(
					addBinBlockedListForm.getBinText()));
			bin.setClearText(addBinBlockedListForm.getBinText());
			bin.setReasonCode(addBinBlockedListForm.getBlockingReason());
			for (Iterator it = blockingReasons.iterator(); it.hasNext();)
			{
				BlockedReason br = (BlockedReason) it.next();
				if (br
					.getReasonCode()
					.equals(addBinBlockedListForm.getBlockingReason()))
				{
					bin.setReasonDesc(br.getReasonDesc());
					break;
				}
			}

			bin.setComment(addBinBlockedListForm.getBlockingComment());
			bin.setStatusCode(addBinBlockedListForm.getBlockingAction());
			for (Iterator it = blockingActions.iterator(); it.hasNext();)
			{
				BlockedStatus bs = (BlockedStatus) it.next();
				if (bs
					.getBlockedStatusCode()
					.equals(addBinBlockedListForm.getBlockingAction()))
				{
					bin.setStatusDesc(bs.getBlockedStatusDesc());
					break;
				}
			}

			bin.setFraudRiskLevelCode(addBinBlockedListForm.getRiskLevel());

			request.getSession().setAttribute("blockedBin", bin);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SAVE_BIN_BLOCKED_LIST);

		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
