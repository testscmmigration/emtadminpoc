package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowBankBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String bankFull = null;
	private String bankMask = null;
	private String bankAba = null;
	private String submitAcctSearch = null;
	private String submitMaskSearch = null;
	private String submitShowAll = null;
	private String submitAdd = null;

	public String getSubmitAdd()
	{
		return submitAdd;
	}

	public void setSubmitAdd(String string)
	{
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		bankFull = null;
		bankMask = null;
		bankAba = null;
		submitAcctSearch = null;
		submitMaskSearch = null;
		submitShowAll = null;
		submitAdd = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (submitAcctSearch != null)
		{
			if (StringHelper.isNullOrEmpty(bankAba))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "ABA Number"));
			} else if (StringHelper.containsNonDigits(bankAba))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.integer", "ABA Number"));
			} else if (bankAba.trim().length() < 9)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"errors.required",
						"Invalid ABA Number. Length of 9 digits"));
			}
		}

		if (submitMaskSearch != null)
		{
			if (StringHelper.isNullOrEmpty(bankMask))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.required", "Account Mask Number"));
			} else if (StringHelper.containsNonDigits(bankMask))
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError("errors.integer", "Account Mask Number"));
			} else if (bankMask.trim().length() < 4)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"errors.required",
						"Invalid ABA Mask. Length of 4 digits"));
			}
		}

		return errors;
	}

	/**
	 * @return
	 */
	public String getBankFull()
	{
		return bankFull;
	}

	/**
	 * @return
	 */
	public String getBankMask()
	{
		return bankMask;
	}

	/**
	 * @return
	 */
	public String getSubmitAcctSearch()
	{
		return submitAcctSearch;
	}

	/**
	 * @param string
	 */
	public void setBankFull(String string)
	{
		bankFull = string;
	}

	/**
	 * @param string
	 */
	public void setBankMask(String string)
	{
		bankMask = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitAcctSearch(String string)
	{
		submitAcctSearch = string;
	}

	/**
	 * @return
	 */
	public String getBankAba()
	{
		return bankAba;
	}

	/**
	 * @param string
	 */
	public void setBankAba(String string)
	{
		bankAba = string;
	}

	/**
	 * @return
	 */
	public String getSubmitMaskSearch()
	{
		return submitMaskSearch;
	}

	/**
	 * @param string
	 */
	public void setSubmitMaskSearch(String string)
	{
		submitMaskSearch = string;
	}

	/**
	 * @return
	 */
	public String getSubmitShowAll()
	{
		return submitShowAll;
	}

	/**
	 * @param string
	 */
	public void setSubmitShowAll(String string)
	{
		submitShowAll = string;
	}

}
