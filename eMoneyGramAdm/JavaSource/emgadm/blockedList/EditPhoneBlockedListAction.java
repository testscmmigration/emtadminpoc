package emgadm.blockedList;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedStatus;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

public class EditPhoneBlockedListAction extends EMoneyGramAdmBaseAction
{
	public void saveDB(EditPhoneBlockedListForm form, HttpServletRequest request) throws Exception {
		ActionErrors errors = new ActionErrors();
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		ServletContext sc = request.getSession().getServletContext();
		BlockedBean bean = populate(form);
		fs.setBlockedPhone(up.getUID(), bean);
	}

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm f,
		HttpServletRequest request,
		HttpServletResponse response)
		throws DataSourceException, TooManyResultException
	{

		ActionErrors errors = new ActionErrors();
		EditPhoneBlockedListForm form = (EditPhoneBlockedListForm) f;
		UserProfile up = getUserProfile(request);

		if (!StringHelper.isNullOrEmpty(form.getSubmitCancel()))
		{
			return mapping.findForward("showPhoneBlockedList");
		}

		if (request.getAttribute("passedPhNbr") != null)
		{
			form.setPhoneNumber((String) request.getAttribute("passedPhNbr"));
		}

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		ServletContext sc = request.getSession().getServletContext();

		if (sc.getAttribute("blockingReasons") == null)
		{
			sc.setAttribute(
				"blockingReasons",
				fs.getBlockedReasons(up.getUID(), "PHON"));
		}

		if (sc.getAttribute("blockingStatuses") == null)
		{
			sc.setAttribute(
				"blockingStatuses",
				fs.getBlockedStatus(up.getUID(), "PHON"));
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitNew()))
		{
			form.reset(mapping, request);
			form.setAction("add");
			return mapping.findForward(
				EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitProcess()))
		{

			Collection phones =
				fs.getBlockedPhones(up.getUID(), form.getPhoneNumber());
			if (form.getAction().equalsIgnoreCase("add")
				&& phones != null
				&& phones.size() > 0)
			{

				Iterator iter = phones.iterator();
				while (iter.hasNext())
				{
					BlockedBean bb = (BlockedBean) iter.next();
					if (BlockedStatus
						.BLOCKED_CODE
						.equalsIgnoreCase(bb.getStatusCode()))
					{
						String[] parms =
							{ "Blocked Phone", form.getPhoneNumber()};
						errors.add(
							"phoneNumber",
							new ActionError("error.record.exist", parms));
						saveErrors(request, errors);
						return mapping.findForward(
							EMoneyGramAdmForwardConstants
								.LOCAL_FORWARD_SUCCESS);
					}
				}
			}
			form.setAction("edit");
			form.setSaveAction("Process");
		}

		if (!StringHelper.isNullOrEmpty(form.getSubmitSave()))
		{
			BlockedBean bean = populate(form);
			fs.setBlockedPhone(up.getUID(), bean);
			String[] parms = { "Blocked Phone Number", form.getPhoneNumber()};
			if (form.getSaveAction().equalsIgnoreCase("Process"))
			{
				errors.add("phoneNumber", new ActionError("msg.added", parms));
			} else
			{
				errors.add(
					"phoneNumber",
					new ActionError("msg.updated", parms));
			}
			saveErrors(request, errors);
			form.setAction("edit");
			form.setSaveAction("edit");
		}

		if (StringHelper.isNullOrEmpty(form.getSubmitProcess())
			&& form.getAction().equalsIgnoreCase("edit"))
		{
			Collection phones =
				fs.getBlockedPhones(up.getUID(), form.getPhoneNumber());
			BlockedBean bean = (BlockedBean) phones.iterator().next();
			form.setPhoneNumber(bean.getClearText());
			form.setBlkdReasCode(bean.getReasonCode());
			form.setBlkdReasDesc(bean.getReasonDesc());
			form.setBlkdStatCode(bean.getStatusCode());
			form.setBlkdStatDesc(bean.getStatusDesc());
			form.setBlkdCmntText(bean.getComment());
			form.setRiskLvlCode(bean.getFraudRiskLevelCode());
			form.setStatUpdateDate(bean.getStatusUpdateDate());
			form.setCreateDate(bean.getCreateDate());
			form.setCreateUser(bean.getCreateUserId());
			form.setLastUpdateUser(bean.getUpdateUserId());
			form.setLastUpdateDate(bean.getUpdateDate());
		}

		if (form.getAction().equalsIgnoreCase("edit"))
		{
			request.setAttribute(
				"consumerList",
				fs.getConsumersByPhone(up.getUID(), form.getPhoneNumber()));
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private BlockedBean populate(EditPhoneBlockedListForm form)
	{
		BlockedBean bean = new BlockedBean();
		bean.setClearText(form.getPhoneNumber());
		bean.setReasonCode(form.getBlkdReasCode());
		bean.setStatusCode(form.getBlkdStatCode());
		bean.setComment(form.getBlkdCmntText());
		bean.setFraudRiskLevelCode(form.getRiskLvlCode());
		return bean;
	}
}
