package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.util.StringHelper;

public class EditPhoneBlockedListForm extends EditBlockedBaseForm {
	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String string) {
		phoneNumber = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		phoneNumber = "";
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (!StringHelper.isNullOrEmpty(submitSave) ||
			!StringHelper.isNullOrEmpty(submitProcess)) {
			if (StringHelper.isNullOrEmpty(phoneNumber)) {
				errors.add(
					"phoneNumber",
					new ActionError("errors.required", "Phone Number"));
			} else if (StringHelper.containsNonDigits(phoneNumber)) {
				errors.add(
					"phoneNumber",
					new ActionError("errors.numeric"));
			} else if (phoneNumber.trim().length() < 7) {
				String[] parms = {"Phone Number", "7"};
				errors.add(
					"phoneNumber",
					new ActionError("errors.minlength", parms));
			} else if (phoneNumber.trim().length() > 14) {
				String[] parms = {"Phone Number", "14"};
				errors.add(
					"phoneNumber",
					new ActionError("errors.maxlength", parms));
			}
		}

		return errors;
	}
}
