package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.model.BlockedIP;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class SaveIPBlockedListAction extends EMoneyGramAdmBaseAction
{
	public void saveDB(SaveIPBlockedListForm form, HttpServletRequest request) throws Exception {
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		fs.setBlockedIPs(up.getUID(), (BlockedIP)request.getSession().getAttribute("blockedIP"));
	}
	
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		SaveIPBlockedListForm saveIPBlockedListForm =
			(SaveIPBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		Collection blockedIPs = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		if (!StringHelper
			.isNullOrEmpty(saveIPBlockedListForm.getSubmitDBCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_IP_BLOCKED_LIST);
		}

		BlockedIP blockedIP =
			(BlockedIP) request.getSession().getAttribute("blockedIP");

		try
		{

			blockedIPs =
				fs.getBlockedIPs(
					up.getUID(),
					blockedIP.getIpAddress1() == null
						? null
						: blockedIP.getIpAddress1().trim(),
					blockedIP.getIpAddress2() == null
						? null
						: blockedIP.getIpAddress2().trim());

		} catch (TooManyResultException e)
		{
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError(
					"error.too.many.results",
					dynProps.getMaxDownloadTransactions() + ""));
			saveErrors(request, errors);
		}

		request.setAttribute("blockedIPs", blockedIPs);

		if (!StringHelper
			.isNullOrEmpty(saveIPBlockedListForm.getSubmitDBSave()))
		{
			fs.setBlockedIPs(up.getUID(), blockedIP);

			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("msg.blockedip.added"));
			saveErrors(request, errors);

			request.setAttribute("blockedIPs", null);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_IP_BLOCKED_LIST);

		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
