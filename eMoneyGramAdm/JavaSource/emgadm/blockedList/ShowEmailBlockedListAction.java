package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.TooManyResultException;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowEmailBlockedListAction extends EMoneyGramAdmBaseAction
{
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		ShowEmailBlockedListForm showEmailBlockedListForm =
			(ShowEmailBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		CommandAuth ca = new CommandAuth();
		Collection emails = new ArrayList();

		if (up.hasPermission(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADD_EMAIL_BLOCKED_LIST))
		{
			ca.setAddEmailBlockedList(true);
		} else
		{
			ca.setAddEmailBlockedList(false);
		}
		request.getSession().setAttribute("auth", ca);

		if (!StringHelper
			.isNullOrEmpty(showEmailBlockedListForm.getSubmitAdd()))
		{
			request.setAttribute(
				"emailAddress",
				showEmailBlockedListForm.getEmailAddress().trim());
			request.setAttribute(
				"emailDomain",
				showEmailBlockedListForm.getEmailDomain().trim());

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_ADD_EMAIL_BLOCKED_LIST);
		}

		if (!StringHelper
			.isNullOrEmpty(showEmailBlockedListForm.getSubmitShowAll())
			|| !StringHelper.isNullOrEmpty(
				showEmailBlockedListForm.getSubmitEmailSearch())
			|| !StringHelper.isNullOrEmpty(
				showEmailBlockedListForm.getSubmitDomainSearch()))
		{

			try
			{
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();

				if (!StringHelper
					.isNullOrEmpty(
						showEmailBlockedListForm.getSubmitEmailSearch()))
				{
					emails =
						fs.getBlockedEmails(
							up.getUID(),
							showEmailBlockedListForm.getEmailAddress(),
							null);
				} else if (
					!StringHelper.isNullOrEmpty(
						showEmailBlockedListForm.getSubmitDomainSearch()))
				{
					emails =
						fs.getBlockedEmails(
							up.getUID(),
							null,
							showEmailBlockedListForm.getEmailDomain());
				} else
				{
					emails = fs.getBlockedEmails(up.getUID(), null, null);
				}

				if (emails == null || emails.size() == 0)
				{
					errors.add(
						ActionErrors.GLOBAL_ERROR,
						new ActionError("error.no.email.found"));
					saveErrors(request, errors);
				}

			} catch (TooManyResultException e)
			{
				errors.add(
					ActionErrors.GLOBAL_ERROR,
					new ActionError(
						"error.too.many.results",
						dynProps.getMaxDownloadTransactions() + ""));
				saveErrors(request, errors);
			}

			Collections.sort((List) emails);
			request.setAttribute("emails", emails);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
