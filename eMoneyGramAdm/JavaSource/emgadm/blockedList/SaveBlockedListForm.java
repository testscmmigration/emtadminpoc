package emgadm.blockedList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

public class SaveBlockedListForm extends EMoneyGramAdmBaseValidatorForm {

	private String custId = null;
	private String emailDomain = null;
	private String emailAddress = null;
	private String phoneNumber = null;
	private String binText = null;
	private String maskedCC = null;
	private String hashedCC = null;
	private String binNumber = null;
	private int cardNumberLength;
	private String singleIPSelect = null;
	private String ipAddress1 = null;
	private String ipAddress2 = null;
	
	private String blockEmail = null;
	private String blockPhone = null;
	private String blockBin = null;
	private String blockCC = null;
	private String blockIP = null;
	private String blockABA = null;	
	private String blockAcct = null;

	private String riskLevel = null;
	private String blockingComment = null;
	private String blockingReasonEML = null;
	private String blockingActionEML = null;
	private String blockingReasonPHON = null;
	private String blockingActionPHON = null;
	private String blockingReasonBIN = null;
	private String blockingActionBIN = null;
	private String blockingReasonCC = null;
	private String blockingActionCC = null;
	private String blockingReasonIP = null;
	private String blockingActionIP = null;
	private String blockingActionABA = null;
	private String blockingReasonABA = null;
	private String blockingActionACCT = null;
	private String blockingReasonACCT = null;

	private String submitProcess = null;

	private String submitCancel = null;
	private List<String>blockedStatusCode =null;
	private List<String>reasonCode = null;
	private String blockedStatusDesc = null;
	private String reasonDesc = null;
	//Added for Supertaint
	private String primaryPhoneBlocker = null;
	private String altPhoneBlocker = null;
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		emailDomain = null;
		emailAddress = null;

		riskLevel = null;
		blockingComment = null;
		submitProcess = null;
		submitCancel = null;
		blockEmail = null;

		blockingReasonEML = null;
		blockingActionEML = null;
		blockingReasonPHON = null;
		blockingActionPHON = null;
		blockingReasonBIN = null;
		blockingActionBIN = null;
		blockingReasonCC = null;
		blockingActionCC = null;
		blockingReasonIP = null;
		blockingActionIP = null;
		primaryPhoneBlocker = null;
		altPhoneBlocker = null;
		
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);
		
		if (submitProcess != null) {
        	String[] checkboxes = {primaryPhoneBlocker, altPhoneBlocker, blockCC, blockAcct};
        	int checkedCheckboxes = 0;
        	for (String checkbox : checkboxes) {
        		if(checkbox!=null) {
        			++checkedCheckboxes;
        		}
        	}
        	if (checkedCheckboxes < 1 ) {
        		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.blocked.error.noneselected"));
        	}
        	if (blockingReasonCC.equalsIgnoreCase("None")) {
        		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("option.select.one"));
        	}
        	if (blockingComment == null || blockingComment.equals("")){
        		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.add.comment"));
        	}
        }
		return errors;
	}

	public String getBlockingComment() {
		return blockingComment;
	}

	public String getRiskLevel() {
		return riskLevel;
	}

	public String getSubmitCancel() {
		return submitCancel;
	}

	public String getSubmitProcess() {
		return submitProcess;
	}

	public void setBlockingComment(String string) {
		blockingComment = string;
	}

	public void setRiskLevel(String string) {
		riskLevel = string;
	}

	public void setSubmitCancel(String string) {
		submitCancel = string;
	}

	public void setSubmitProcess(String string) {
		submitProcess = string;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getEmailDomain() {
		return emailDomain;
	}

	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	public void setEmailDomain(String string) {
		emailDomain = string;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBlockEmail() {
		return blockEmail;
	}

	public void setBlockEmail(String blockEmail) {
		this.blockEmail = blockEmail;
	}

	public String getBlockingReasonEML() {
		return blockingReasonEML;
	}

	public void setBlockingReasonEML(String blockingReasonEML) {
		this.blockingReasonEML = blockingReasonEML;
	}

	public String getBlockingActionEML() {
		return blockingActionEML;
	}

	public void setBlockingActionEML(String blockingActionEML) {
		this.blockingActionEML = blockingActionEML;
	}

	public String getBlockingReasonPHON() {
		return blockingReasonPHON;
	}

	public void setBlockingReasonPHON(String blockingReasonPHON) {
		this.blockingReasonPHON = blockingReasonPHON;
	}

	public String getBlockingActionPHON() {
		return blockingActionPHON;
	}

	public void setBlockingActionPHON(String blockingActionPHON) {
		this.blockingActionPHON = blockingActionPHON;
	}

	public String getBlockingReasonBIN() {
		return blockingReasonBIN;
	}

	public void setBlockingReasonBIN(String blockingReasonBIN) {
		this.blockingReasonBIN = blockingReasonBIN;
	}

	public String getBlockingActionBIN() {
		return blockingActionBIN;
	}

	public void setBlockingActionBIN(String blockingActionBIN) {
		this.blockingActionBIN = blockingActionBIN;
	}

	public String getBlockingReasonCC() {
		return blockingReasonCC;
	}

	public void setBlockingReasonCC(String blockingReasonCC) {
		this.blockingReasonCC = blockingReasonCC;
	}

	public String getBlockingActionCC() {
		return blockingActionCC;
	}

	public void setBlockingActionCC(String blockingActionCC) {
		this.blockingActionCC = blockingActionCC;
	}

	public String getBlockingReasonIP() {
		return blockingReasonIP;
	}

	public void setBlockingReasonIP(String blockingReasonIP) {
		this.blockingReasonIP = blockingReasonIP;
	}

	public String getBlockingActionIP() {
		return blockingActionIP;
	}

	public void setBlockingActionIP(String blockingActionIP) {
		this.blockingActionIP = blockingActionIP;
	}

	public String getBlockPhone() {
		return blockPhone;
	}

	public void setBlockPhone(String blockPhone) {
		this.blockPhone = blockPhone;
	}

	public String getBlockBin() {
		return blockBin;
	}

	public void setBlockBin(String blockBin) {
		this.blockBin = blockBin;
	}

	public String getBlockCC() {
		return blockCC;
	}

	public void setBlockCC(String blockCC) {
		this.blockCC = blockCC;
	}

	public String getBlockIP() {
		return blockIP;
	}

	public void setBlockIP(String blockIP) {
		this.blockIP = blockIP;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getBinText() {
		return binText;
	}

	public void setBinText(String binText) {
		this.binText = binText;
	}

	public String getMaskedCC() {
		return maskedCC;
	}

	public void setMaskedCC(String maskedCC) {
		this.maskedCC = maskedCC;
	}

	public String getHashedCC() {
		return hashedCC;
	}

	public void setHashedCC(String hashedCC) {
		this.hashedCC = hashedCC;
	}

	public String getBinNumber() {
		return binNumber;
	}

	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}

	public int getCardNumberLength() {
		return cardNumberLength;
	}

	public void setCardNumberLength(int cardNumberLength) {
		this.cardNumberLength = cardNumberLength;
	}

	public String getSingleIPSelect() {
		return singleIPSelect;
	}

	public void setSingleIPSelect(String singleIPSelect) {
		this.singleIPSelect = singleIPSelect;
	}

	public String getIpAddress1() {
		return ipAddress1;
	}

	public void setIpAddress1(String ipAddress1) {
		this.ipAddress1 = ipAddress1;
	}

	public String getIpAddress2() {
		return ipAddress2;
	}

	public void setIpAddress2(String ipAddress2) {
		this.ipAddress2 = ipAddress2;
	}

	public String getBlockABA() {
		return blockABA;
	}

	public void setBlockABA(String blockABA) {
		this.blockABA = blockABA;
	}

	public String getBlockAcct() {
		return blockAcct;
	}

	public void setBlockAcct(String blocAcct) {
		this.blockAcct = blocAcct;
	}

	public String getBlockingActionABA() {
		return blockingActionABA;
	}

	public void setBlockingActionABA(String blockingActionABA) {
		this.blockingActionABA = blockingActionABA;
	}

	public String getBlockingReasonABA() {
		return blockingReasonABA;
	}

	public void setBlockingReasonABA(String blockingReasonABA) {
		this.blockingReasonABA = blockingReasonABA;
	}

	public String getBlockingActionACCT() {
		return blockingActionACCT;
	}

	public void setBlockingActionACCT(String blockingActionACCT) {
		this.blockingActionACCT = blockingActionACCT;
	}

	public String getBlockingReasonACCT() {
		return blockingReasonACCT;
	}

	public void setBlockingReasonACCT(String blockingReasonACCT) {
		this.blockingReasonACCT = blockingReasonACCT;
	}

	public List<String> getBlockedStatusCode() {
		return blockedStatusCode;
	}

	public void setBlockedStatusCode(List<String> blockedStatusCode) {
		this.blockedStatusCode = blockedStatusCode;
	}

	public List<String> getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(List<String> reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getBlockedStatusDesc() {
		return blockedStatusDesc;
	}

	public void setBlockedStatusDesc(String blockedStatusDesc) {
		this.blockedStatusDesc = blockedStatusDesc;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}

	public String getPrimaryPhoneBlocker() {
		return primaryPhoneBlocker;
	}

	public void setPrimaryPhoneBlocker(String primaryPhoneBlocker) {
		this.primaryPhoneBlocker = primaryPhoneBlocker;
	}

	public String getAltPhoneBlocker() {
		return altPhoneBlocker;
	}

	public void setAltPhoneBlocker(String altPhoneBlocker) {
		this.altPhoneBlocker = altPhoneBlocker;
	}
	
}
