package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;

public class ShowCCBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String ccFull = null;
	private String ccMask = null;
	private String ccHashed = null;
	private String submitFullSearch = null;
	private String submitMaskSearch = null;
	private String submitShowAll = null;
	private String submitAdd = null;

	public String getSubmitAdd()
	{
		return submitAdd;
	}

	public void setSubmitAdd(String string)
	{
		submitAdd = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request)
	{
		super.reset(mapping, request);

		ccFull = null;
		ccMask = null;
		ccHashed = null;
		submitFullSearch = null;
		submitMaskSearch = null;
		submitShowAll = null;
		submitAdd = null;
	}

	public ActionErrors validate(
		ActionMapping mapping,
		HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (submitFullSearch != null)
		{
			if (StringHelper.isNullOrEmpty(ccFull))
			{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Credit Card Number"));
			} else if (StringHelper.containsNonDigits(ccFull)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.integer", "Credit Card Number"));
			} else if (ccFull.length() != 16) {
				String[] parms = { "Credit Card Number", "16" };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.length.wrong", parms));
			}
		} else 	if (submitMaskSearch != null) {
			if (StringHelper.isNullOrEmpty(ccMask))	{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "Credit Card Mask"));
			} else if (StringHelper.containsNonDigits(ccMask)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.integer", "Credit Card Mask"));
			} else if (ccMask.length() != 4) {
				String[] parms = { "Credit Card Mask", "4" };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.length.wrong", parms));
			}
		} else if (submitAdd != null) {
			if (StringHelper.containsNonDigits(ccFull))	{
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.integer", "Credit Card Number"));
			} else if (ccFull != null && ccFull.length() != 16) {
				String[] parms = { "Credit Card Number", "16" };
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.length.wrong", parms));
			}			
		}

		return errors;
	}

	/**
	 * @return
	 */
	public String getCcFull()
	{
		return ccFull;
	}

	/**
	 * @return
	 */
	public String getCcMask()
	{
		return ccMask;
	}

	/**
	 * @return
	 */
	public String getSubmitFullSearch()
	{
		return submitFullSearch;
	}

	/**
	 * @return
	 */
	public String getSubmitMaskSearch()
	{
		return submitMaskSearch;
	}

	/**
	 * @param string
	 */
	public void setCcFull(String string)
	{
		ccFull = string;
	}

	/**
	 * @param string
	 */
	public void setCcMask(String string)
	{
		ccMask = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitFullSearch(String string)
	{
		submitFullSearch = string;
	}

	/**
	 * @param string
	 */
	public void setSubmitMaskSearch(String string)
	{
		submitMaskSearch = string;
	}

	/**
	 * @return
	 */
	public String getSubmitShowAll()
	{
		return submitShowAll;
	}

	/**
	 * @param string
	 */
	public void setSubmitShowAll(String string)
	{
		submitShowAll = string;
	}

	public String getCcHashed() {
		return ccHashed;
	}

	public void setCcHashed(String hashedCC) {
		this.ccHashed = hashedCC;
	}

}
