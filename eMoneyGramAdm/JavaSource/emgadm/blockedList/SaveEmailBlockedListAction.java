package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.ConsumerProfileSearchView;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class SaveEmailBlockedListAction extends EMoneyGramAdmBaseAction
{
	public void saveDB(ActionForm form,	HttpServletRequest request) throws Exception {
		ActionErrors errors = new ActionErrors();
		SaveEmailBlockedListForm SaveEmailBlockedListForm = (SaveEmailBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		Collection emails = new ArrayList();
		Collection consumerProfiles = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		BlockedBean blockedEmail = (BlockedBean) request.getSession().getAttribute("blockedEmail");
		
		// Getting all the Blocked Email records that get affected
		if (blockedEmail.getAddrFmtCode().equals("A")) {
			emails = fs.getBlockedEmails(up.getUID(), blockedEmail.getClearText(),	null);
		} else {
			emails = fs.getBlockedEmails(up.getUID(), null,	blockedEmail.getClearText());
		}

		request.setAttribute("blockedEmails", emails);

		// Getting all the customer profiles that get affected
		if (blockedEmail.getAddrFmtCode().equals("A")) {
			consumerProfiles = fs.getConsumersByEmail(up.getUID(), blockedEmail.getClearText(),	null);
		} else {
			consumerProfiles = fs.getConsumersByEmail(up.getUID(), null, blockedEmail.getClearText());
		}

		request.setAttribute("consumerProfiles", consumerProfiles);
		
		ArrayList<ConsumerProfileSearchView> ts_consumerProfiles = (ArrayList<ConsumerProfileSearchView>)consumerProfiles;
			
		for (ConsumerProfileSearchView consumer : ts_consumerProfiles) {
			String custId = consumer.getCustId();
			fs.setBlockedEmail(up.getUID(), custId, blockedEmail);
		}

		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.email.added"));			
		saveErrors(request, errors);

		request.setAttribute("emails", null);
	}
	
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		SaveEmailBlockedListForm SaveEmailBlockedListForm =
			(SaveEmailBlockedListForm) form;
		UserProfile up = getUserProfile(request);
		Collection emails = new ArrayList();
		Collection consumerProfiles = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		if (!StringHelper
			.isNullOrEmpty(SaveEmailBlockedListForm.getSubmitDBCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_EMAIL_BLOCKED_LIST);
		}

		BlockedBean blockedEmail =
			(BlockedBean) request.getSession().getAttribute("blockedEmail");

		// Getting all the Blocked Email records that get affected
		if (blockedEmail.getAddrFmtCode().equals("A"))
			emails =
				fs.getBlockedEmails(
					up.getUID(),
					blockedEmail.getClearText(),
					null);

		else
			emails =
				fs.getBlockedEmails(
					up.getUID(),
					null,
					blockedEmail.getClearText());

		request.setAttribute("blockedEmails", emails);

		// Getting all the customer profiles that get affected
		if (blockedEmail.getAddrFmtCode().equals("A"))
			consumerProfiles =
				fs.getConsumersByEmail(
					up.getUID(),
					blockedEmail.getClearText(),
					null);
		else
			consumerProfiles =
				fs.getConsumersByEmail(
					up.getUID(),
					null,
					blockedEmail.getClearText());

		request.setAttribute("consumerProfiles", consumerProfiles);

		if (!StringHelper
			.isNullOrEmpty(SaveEmailBlockedListForm.getSubmitDBSave()))
		{
			
			ArrayList<ConsumerProfileSearchView> ts_consumerProfiles 
				= (ArrayList<ConsumerProfileSearchView>)consumerProfiles;
			
			for (ConsumerProfileSearchView consumer : ts_consumerProfiles) {
				String custId = consumer.getCustId();
				fs.setBlockedEmail(up.getUID(), custId, blockedEmail);
			}
			
			errors.add(
				ActionErrors.GLOBAL_ERROR,
				new ActionError("msg.email.added"));
			saveErrors(request, errors);

			request.setAttribute("emails", null);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_EMAIL_BLOCKED_LIST);
		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
