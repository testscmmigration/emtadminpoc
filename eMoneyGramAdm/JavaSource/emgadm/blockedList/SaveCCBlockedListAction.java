package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.CustomerProfileAccount;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.services.PCIService;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.www.PCIDecryptService_v2.AccountNotFoundException;

/**
 * @version 	1.0
 * @author
 */
public class SaveCCBlockedListAction extends EMoneyGramAdmBaseAction
{
	
	private static final PCIService pciService = 
		emgshared.services.ServiceFactory.getInstance().getPCIService();
	private static Logger logger = LogFactory.getInstance().getLogger(
			SaveCCBlockedListAction.class);

	public void saveDB(SaveCCBlockedListForm form, HttpServletRequest request) throws Exception {
		UserProfile up = getUserProfile(request);
		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();
		BlockedBean blockedCC =	(BlockedBean) request.getSession().getAttribute("blockedCC");
		ObfuscationService os = ServiceFactory.getInstance().getObfuscationService();

		Collection customerProfileAccounts = fs.getConsumersByAccounts(up.getUID(), os.getBinHash(blockedCC.getBinText()), blockedCC.getMaskText(), true, 0);
		Collection filteredCustomerProfileAccounts = new ArrayList();
		
		/*
		 * Spin through the list of filtered cc accounts and find the exact 
		 * matches.
		 */
		for (Iterator it = customerProfileAccounts.iterator(); it.hasNext();) {
			
			CustomerProfileAccount cpa = (CustomerProfileAccount) it.next();
			String decryptedAccountNumber =	
				pciService.retrieveCardNumber(cpa.getCustAcctId(), true);
			
			if (!StringHelper.isNullOrEmpty(blockedCC.getBlockedId()) && 
					(blockedCC.getBlockedId().equals(
					pciService.retrieveBlockedId(decryptedAccountNumber)))) {
				filteredCustomerProfileAccounts.add(cpa);
			}

		}
		fs.setBlockedCC(up.getUID(), blockedCC,	filteredCustomerProfileAccounts);
		request.setAttribute("blockedCCs", null);
	}

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		SaveCCBlockedListForm saveCCBlockedListForm = (SaveCCBlockedListForm) form;
		UserProfile up = getUserProfile(request);

		
		Collection filteredCustomerProfileAccounts = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		if (!StringHelper.isNullOrEmpty(saveCCBlockedListForm.getSubmitDBCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_CC_BLOCKED_LIST);
		}

		BlockedBean blockedCC =	(BlockedBean) request.getSession().getAttribute("blockedCC");

		// Getting all the Blocked CC records that get affected	

		Collection blockedCCs;
		if (!StringHelper.isNullOrEmpty(blockedCC.getBlockedId())) {
			blockedCCs = fs.getBlockedCCAccounts(up.getUID(),
				blockedCC.getBlockedId(), null);
		} else {
			blockedCCs = new ArrayList();
		}		
		request.setAttribute("blockedCCs", blockedCCs);

		// Let's figure out the consumer profiles that are affected.
		Collection customerProfileAccounts;
		ObfuscationService os = ServiceFactory.getInstance().getObfuscationService();
		customerProfileAccounts = fs.getConsumersByAccounts(
			up.getUID(),
			os.getBinHash(blockedCC.getBinText()),
			blockedCC.getMaskText(),
			true, 0);
			
		// Get all the customer profiles that WILL be affected.
		for (Iterator it = customerProfileAccounts.iterator(); it.hasNext();) {
			CustomerProfileAccount cpa = (CustomerProfileAccount) it.next();
		
			try {
				if (!StringHelper.isNullOrEmpty(blockedCC.getMaskText()) 
						&& !StringHelper.isNullOrEmpty(cpa.getAcctMaskNbr())
						&& blockedCC.getMaskText().equals(cpa.getAcctMaskNbr())){ 
					
					String decryptedAccountNumber =	
						pciService.retrieveCardNumber(cpa.getCustAcctId(), true);
					if (decryptedAccountNumber.equals(blockedCC.getClearText())) {
						filteredCustomerProfileAccounts.add(cpa);
					}		
				}
			} catch (AccountNotFoundException ex) {					
			}
		}

		request.setAttribute("customerProfileAccounts", filteredCustomerProfileAccounts);

		// Save/update blocked CC number.
		if (!StringHelper.isNullOrEmpty(saveCCBlockedListForm.getSubmitDBSave()))
		{
			try{
			fs.setBlockedCC(up.getUID(), blockedCC,	filteredCustomerProfileAccounts);

			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("msg.blockedcc.added"));
			saveErrors(request, errors);

			request.setAttribute("blockedCCs", null);
			}catch(Exception ex){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("label.blockedlist.cc.nopci"));
				saveErrors(request, errors);
				return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_CC_BLOCKED_LIST);
			}

			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_CC_BLOCKED_LIST);
		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}
}
