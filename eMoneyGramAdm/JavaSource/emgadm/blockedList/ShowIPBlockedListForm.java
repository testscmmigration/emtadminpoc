package emgadm.blockedList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;
import emgadm.util.StringHelper;
import emgshared.util.IPAddressValidator;

public class ShowIPBlockedListForm extends EMoneyGramAdmBaseValidatorForm
{
	private String ipAddress = null;
	private String submitSearch = null;
	private String submitAdd = null;
	private String submitShowAll = null;


	public String getSubmitSearch() {
		return submitSearch;
	}

	public void setSubmitSearch(String string) {
		submitSearch = string;
	}

	public String getSubmitAdd() {
		return submitAdd;
	}

	public void setSubmitAdd(String string) {
		submitAdd = string;
	}

	public String getSubmitShowAll() {
		return submitShowAll;
	}

	public void setSubmitShowAll(String string) {
		submitShowAll = string;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);

		submitSearch = null;
		submitAdd = null;
		submitShowAll = null;
	}
	
	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
	{
		ActionErrors errors = new ActionErrors();
		errors = super.validate(mapping, request);

		if (submitSearch != null){
			if (StringHelper.isNullOrEmpty(ipAddress)){ 
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.required", "IP Search Key"));
			}else{
				IPAddressValidator ipValidator = new IPAddressValidator(ipAddress);
				if (ipValidator.isIpInvalid()){
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("errors.invalid.ip", ipValidator.getInvalidReason()));				
				}
			}
		}
		return errors;
	}

	/**
	 * @return
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param string
	 */
	public void setIpAddress(String string) {
		ipAddress = string;
	}

}
