package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.model.ElectronicTypeCode;
import emgshared.services.FraudService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class AddEmailBlockedListAction extends EMoneyGramAdmBaseAction
{

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		AddEmailBlockedListForm addEmailBlockedListForm =
			(AddEmailBlockedListForm) form;
		ServletContext sc = request.getSession().getServletContext();
		UserProfile up = getUserProfile(request);
		Collection emails = new ArrayList();

		ServiceFactory sf = ServiceFactory.getInstance();
		FraudService fs = sf.getFraudService();

		if (!StringHelper
			.isNullOrEmpty((String) request.getAttribute("emailAddress")))
			addEmailBlockedListForm.setEmailAddress(
				(String) request.getAttribute("emailAddress"));

		if (!StringHelper
			.isNullOrEmpty((String) request.getAttribute("emailDomain")))
			addEmailBlockedListForm.setEmailDomain(
				(String) request.getAttribute("emailDomain"));

		Collection blockingReasons = fs.getBlockedReasons(up.getUID(), "EML");
		Collection blockingActions = fs.getBlockedStatus(up.getUID(), "EML");

		sc.setAttribute("blockingReasons", blockingReasons);
		sc.setAttribute("blockingActions", blockingActions);

		String paramEmailAddress = request.getParameter("paramemail");
		String paramEmailDomain = request.getParameter("paramdomain");

		if (!StringHelper.isNullOrEmpty(paramEmailAddress)
			|| !StringHelper.isNullOrEmpty(paramEmailDomain))
		{

			if (paramEmailAddress != null)
			{
				emails =
					fs.getBlockedEmails(up.getUID(), paramEmailAddress, null);
			}
			if (paramEmailDomain != null)
			{
				emails =
					fs.getBlockedEmails(up.getUID(), null, paramEmailDomain);
			}

			if (emails.size() > 0)
			{
				BlockedBean email = (BlockedBean) emails.toArray()[0];
				addEmailBlockedListForm.setEmailAddress(paramEmailAddress);
				addEmailBlockedListForm.setEmailDomain(paramEmailDomain);
				addEmailBlockedListForm.setBlockingAction(
					email.getStatusCode());
				addEmailBlockedListForm.setBlockingReason(
					email.getReasonCode());
				addEmailBlockedListForm.setBlockingComment(email.getComment());
				addEmailBlockedListForm.setRiskLevel(
					email.getFraudRiskLevelCode());
			}
		}

		if (!StringHelper
			.isNullOrEmpty(addEmailBlockedListForm.getSubmitCancel()))
		{
			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SHOW_EMAIL_BLOCKED_LIST);
		}

		if (!StringHelper
			.isNullOrEmpty(addEmailBlockedListForm.getSubmitProcess()))
		{
			BlockedBean email = new BlockedBean();

			if (!StringHelper
				.isNullOrEmpty(addEmailBlockedListForm.getEmailAddress()))
			{
				email.setClearText(addEmailBlockedListForm.getEmailAddress());
				email.setAddrFmtCode("A");
			}

			if (!StringHelper
				.isNullOrEmpty(addEmailBlockedListForm.getEmailDomain()))
			{
				email.setClearText(addEmailBlockedListForm.getEmailDomain());
				email.setAddrFmtCode("D");
			}

			email.setReasonCode(addEmailBlockedListForm.getBlockingReason());

			for (Iterator it = blockingReasons.iterator(); it.hasNext();)
			{
				BlockedReason br = (BlockedReason) it.next();
				if (br
					.getReasonCode()
					.equals(addEmailBlockedListForm.getBlockingReason()))
				{
					email.setReasonDesc(br.getReasonDesc());
					break;
				}
			}

			email.setComment(addEmailBlockedListForm.getBlockingComment());
			email.setStatusCode(addEmailBlockedListForm.getBlockingAction());

			for (Iterator it = blockingActions.iterator(); it.hasNext();)
			{
				BlockedStatus bs = (BlockedStatus) it.next();
				if (bs
					.getBlockedStatusCode()
					.equals(addEmailBlockedListForm.getBlockingAction()))
				{
					email.setStatusDesc(bs.getBlockedStatusDesc());
					break;
				}
			}

			email.setFraudRiskLevelCode(addEmailBlockedListForm.getRiskLevel());
			email.setElecAddrTypeCode(ElectronicTypeCode.EMAIL.getTypeCode());

			request.getSession().setAttribute("blockedEmail", email);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SAVE_EMAIL_BLOCKED_LIST);

		}

		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
