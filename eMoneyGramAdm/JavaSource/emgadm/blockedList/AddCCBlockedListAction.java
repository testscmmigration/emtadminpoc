package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.model.BlockedBean;
import emgshared.model.BlockedReason;
import emgshared.model.BlockedStatus;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.ServiceFactory;
import emgshared.services.PCIService;

/**
 * @version 	1.0
 * @author
 */
public class AddCCBlockedListAction extends EMoneyGramAdmBaseAction
{
	
	private static final PCIService pciService = 
		emgshared.services.ServiceFactory.getInstance().getPCIService();

	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{
		ServiceFactory sf = ServiceFactory.getInstance();
		ObfuscationService os = sf.getObfuscationService();
		AddCCBlockedListForm addCCBlockedListForm = (AddCCBlockedListForm) form;
		ServletContext sc = request.getSession().getServletContext();
		UserProfile up = getUserProfile(request);
		Collection ccs = new ArrayList();

		FraudService fs = sf.getFraudService();
		String ccnumber = (String) request.getSession().getAttribute("ccnumber");
		// Prepare form to display before submitting process request
		if (!StringHelper.isNullOrEmpty(ccnumber)) {			
			/* 
			 * Determine if card number entered by EOC is already blocked.
			 * blockedId will be a nonempty string if this is the case.
			 */
			String blockedId = pciService.retrieveBlockedId(ccnumber);
			addCCBlockedListForm.setBlockedId(blockedId);
			addCCBlockedListForm.setBinNumber(ccnumber.toString().substring(0, 6));
			addCCBlockedListForm.setMaskedCC(ccnumber.toString().substring(12));
			addCCBlockedListForm.setCardNumberLength(ccnumber.toString().length());

		} else if (!StringHelper.isNullOrEmpty((String) request.getAttribute("ccCustId"))) {
			
		}

		Collection blockingReasons = fs.getBlockedReasons(up.getUID(), "CC");
		Collection blockingActions = fs.getBlockedStatus(up.getUID(), "CC");

		sc.setAttribute("blockingReasons", blockingReasons);
		sc.setAttribute("blockingActions", blockingActions);

		/* 
		 * paramCc will only be set on an edit (accessed via search results)
		 * of the block credit card's status.
		 */
		String paramCc = request.getParameter("cc");
		if (paramCc != null)
		{
			ccs = fs.getBlockedCCAccounts(up.getUID(), paramCc, null);
			String decryptedCCNumber = pciService.retrieveCardNumber(paramCc, PCIService.SourceUsageCategoryCode.BLOCKED);
			request.getSession().setAttribute("ccnumber", decryptedCCNumber);
			if (ccs.size() > 0)
			{
				BlockedBean cc = (BlockedBean) ccs.toArray()[0];
				addCCBlockedListForm.setBinNumber(cc.getBinText());
				addCCBlockedListForm.setCardNumberLength(cc.getAcctNumberLength());
				addCCBlockedListForm.setMaskedCC(cc.getMaskText());
				addCCBlockedListForm.setHashedCC(cc.getHashText());
				addCCBlockedListForm.setBlockedId(cc.getBlockedId());
				addCCBlockedListForm.setBlockingAction(cc.getStatusCode());
				addCCBlockedListForm.setBlockingReason(cc.getReasonCode());
				addCCBlockedListForm.setBlockingComment(cc.getComment());
				addCCBlockedListForm.setRiskLevel(cc.getFraudRiskLevelCode());				
			}
		}
		
		// Parameter is set by viewCreditCardAccountDetail view.
		String ccAccountId = request.getParameter("ccAccountId");
		if (ccAccountId != null) {
			addCCBlockedListForm.setBinNumber(request.getParameter("bin"));
			addCCBlockedListForm.setMaskedCC(request.getParameter("mask"));
			String decryptedCCNumber = 
				pciService.retrieveCardNumber(ccAccountId, true);
			String blockedId = pciService.retrieveBlockedId(decryptedCCNumber);
			addCCBlockedListForm.setBlockedId(blockedId);
			request.getSession().setAttribute("ccnumber", decryptedCCNumber);
		  if (!blockedId.equals("")) {
				ccs = fs.getBlockedCCAccounts(up.getUID(), blockedId, null);
				
				if (ccs.size() > 0) {
					BlockedBean cc = (BlockedBean) ccs.toArray()[0];
					addCCBlockedListForm.setBinNumber(cc.getBinText());
					addCCBlockedListForm.setCardNumberLength(cc.getAcctNumberLength());
					addCCBlockedListForm.setMaskedCC(cc.getMaskText());
					addCCBlockedListForm.setHashedCC(cc.getHashText());
					addCCBlockedListForm.setBlockedId(cc.getBlockedId());
					addCCBlockedListForm.setBlockingAction(cc.getStatusCode());
					addCCBlockedListForm.setBlockingReason(cc.getReasonCode());
					addCCBlockedListForm.setBlockingComment(cc.getComment());
					addCCBlockedListForm.setRiskLevel(cc.getFraudRiskLevelCode());
				}
			}
		}

		if (!StringHelper.isNullOrEmpty(addCCBlockedListForm.getSubmitCancel()))
		{
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SHOW_CC_BLOCKED_LIST);
		}

		/*
		 * Process credit card with selected action and reason.  Credit card 
		 * may be new, or an existing card.
		 */
		if (!StringHelper.isNullOrEmpty(addCCBlockedListForm.getSubmitProcess()))
		{	
			
			/* 
			 * Determine if card number entered by EOC is already blocked.
			 * If true, then blockedId will be a nonempty string.
			 */
			BlockedBean cc = new BlockedBean();
			cc.setMaskText(getLast4Mask(addCCBlockedListForm));
			
			cc.setHashText(addCCBlockedListForm.getHashedCC());
			cc.setClearText(ccnumber);
			cc.setBlockedId(addCCBlockedListForm.getBlockedId());
			cc.setBinText(addCCBlockedListForm.getBinNumber());
			cc.setAcctNumberLength(addCCBlockedListForm.getCardNumberLength());
			cc.setReasonCode(addCCBlockedListForm.getBlockingReason());
			
			for (Iterator it = blockingReasons.iterator(); it.hasNext();)
			{
				BlockedReason br = (BlockedReason) it.next();
				if (br.getReasonCode().equals(addCCBlockedListForm.getBlockingReason()))
				{
					cc.setReasonDesc(br.getReasonDesc());
					break;
				}
			}
			cc.setComment(addCCBlockedListForm.getBlockingComment());
			cc.setStatusCode(addCCBlockedListForm.getBlockingAction());
			for (Iterator it = blockingActions.iterator(); it.hasNext();)
			{
				BlockedStatus bs = (BlockedStatus) it.next();
				if (bs.getBlockedStatusCode()
						.equals(addCCBlockedListForm.getBlockingAction()))
				{
					cc.setStatusDesc(bs.getBlockedStatusDesc());
					break;
				}
			}
			cc.setFraudRiskLevelCode(addCCBlockedListForm.getRiskLevel());
			request.getSession().setAttribute("blockedCC", cc);

			return mapping.findForward(
				EMoneyGramAdmForwardConstants
					.LOCAL_FORWARD_SAVE_CC_BLOCKED_LIST);
		}
		
		return mapping.findForward(
			EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

	private String getLast4Mask(AddCCBlockedListForm addCCBlockedListForm) {
		if(null!=addCCBlockedListForm.getMaskedCC() && 0!=addCCBlockedListForm.getMaskedCC().length()){
			return addCCBlockedListForm.getMaskedCC().substring(addCCBlockedListForm.getMaskedCC().length() - 4);
			}else{
				return "";
			}
	}

}
