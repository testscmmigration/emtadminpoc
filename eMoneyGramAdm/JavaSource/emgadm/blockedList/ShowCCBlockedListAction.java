package emgadm.blockedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import emgadm.actions.EMoneyGramAdmBaseAction;
import emgadm.constants.EMoneyGramAdmForwardConstants;
import emgadm.model.CommandAuth;
import emgadm.model.UserProfile;
import emgadm.util.StringHelper;
import emgshared.exceptions.MaxRowsHashCryptoException;
import emgshared.model.BlockedBean;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.FraudService;
import emgshared.services.ObfuscationService;
import emgshared.services.PCIService;
import emgshared.services.ServiceFactory;

/**
 * @version 	1.0
 * @author
 */
public class ShowCCBlockedListAction extends EMoneyGramAdmBaseAction
{
	private static EMTSharedDynProperties dynProps =
		new EMTSharedDynProperties();

	private static final PCIService pciService = 
		emgshared.services.ServiceFactory.getInstance().getPCIService();
	
	public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception
	{

		ActionErrors errors = new ActionErrors();
		ShowCCBlockedListForm showCCBlockedListForm = new ShowCCBlockedListForm();
		 showCCBlockedListForm = (ShowCCBlockedListForm) form;
		AddCCBlockedListForm addCCBlockedListForm = new AddCCBlockedListForm();
		UserProfile up = getUserProfile(request);
		CommandAuth ca = new CommandAuth();
		Collection ccs = new ArrayList();
		if (up.hasPermission(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_CC_BLOCKED_LIST)) {
			ca.setAddCCBlockedList(true);
		} else {
			ca.setAddCCBlockedList(false);
		}
		request.getSession().setAttribute("auth", ca);
		
//		if(showCCBlockedListForm.getCcFull().matches("[0-9_]+")){
		
		request.getSession().setAttribute("ccnumber", showCCBlockedListForm.getCcFull());
		
		if (!StringHelper.isNullOrEmpty(showCCBlockedListForm.getSubmitAdd()))
		{
			return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_ADD_CC_BLOCKED_LIST);
		}

		if (!StringHelper.isNullOrEmpty(showCCBlockedListForm.getSubmitShowAll())
			|| !StringHelper.isNullOrEmpty(showCCBlockedListForm.getSubmitFullSearch())
			|| !StringHelper.isNullOrEmpty(showCCBlockedListForm.getSubmitMaskSearch()))
		{

			try
			{
				ServiceFactory sf = ServiceFactory.getInstance();
				FraudService fs = sf.getFraudService();
				ObfuscationService os = sf.getObfuscationService();
				
				String maskText = showCCBlockedListForm.getCcMask();
				String blockedId = "";
				
				if (!StringHelper.isNullOrEmpty(
						showCCBlockedListForm.getSubmitFullSearch())) {
					
					/* 
					 * Determine if card number entered by EOC is already
					 * blocked
					 */ 
					blockedId = pciService.retrieveBlockedId(
							showCCBlockedListForm.getCcFull());
					
					if (!StringHelper.isNullOrEmpty(blockedId)) {

						/* 
						 * Card is already blocked, let's use blockedId to get 
						 * blocked account record tied to blockedId
						 */
						ccs = fs.getBlockedCCAccounts(up.getUID(), blockedId, null);
						showCCBlockedListForm.setCcFull(
								StringHelper.maskCc(showCCBlockedListForm.getCcFull()));
					} else {
						ccs = new ArrayList();
					}
				} else if (!StringHelper.isNullOrEmpty(
						showCCBlockedListForm.getSubmitMaskSearch())) {
					ccs = fs.getBlockedCCAccounts(up.getUID(), blockedId, maskText);
				} else {
					/*
					 * Show all (as long as result set does not exceeds 
					 * pre-configured result set max count
					 */
					ccs = fs.getBlockedCCAccounts(up.getUID(), null, null);
				}
				

				if (ccs == null || ccs.size() == 0)
				{
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.blockedcc.found"));
					saveErrors(request, errors);
				} else	{

					BlockedBean.sortBy = BlockedBean.SORT_BY_CLEAR_TEXT;
					Collections.sort((List) ccs);
					request.setAttribute("ccs", ccs);
				}

			} catch (MaxRowsHashCryptoException te)	{
				errors.add(ActionErrors.GLOBAL_ERROR,
					new ActionError("error.too.many.results", dynProps.getMaxRowsHashCrypto() + ""));
				saveErrors(request, errors);
			}
		} 
		
//		}else {
//			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.no.blockedcc.found"));
//			saveErrors(request, errors);
//		}

		return mapping.findForward(EMoneyGramAdmForwardConstants.LOCAL_FORWARD_SUCCESS);
	}

}
