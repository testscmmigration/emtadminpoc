package com.moneygram.emoneygramadm.forms;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import emgadm.forms.EMoneyGramAdmBaseValidatorForm;

/**
 * Form bean for a Struts application. Users may access 1 field on this form:
 * <ul>
 * <li>partnerSiteId - The partner Site ID
 * </ul>
 * 
 * @version 1.0
 * @author uo87
 */
public class BackgroundProcessingForm extends EMoneyGramAdmBaseValidatorForm {

	private static final long serialVersionUID = -5499616412287997680L;
	
	private String partnerSiteId = null;

	/**
	 * Get partnerSiteId
	 * 
	 * @return String
	 */
	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	/**
	 * Set partnerSiteId
	 * 
	 * @param <code>String</code>
	 */
	public void setPartnerSiteId(String p) {
		this.partnerSiteId = p;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// Reset values are provided as samples only. Change as appropriate.

		partnerSiteId = null;

	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		// Validate the fields in your form, adding
		// adding each error to this.errors as found, e.g.

		// if ((field == null) || (field.length() == 0)) {
		// errors.add("field", new
		// org.apache.struts.action.ActionError("error.field.required"));
		// }
		return errors;

	}
}
