<%@ page import="org.apache.struts.action.ActionErrors" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ page import="java.util.*" %>
<%@ page import="emgshared.services.*" %>
<%@ page import="emgshared.model.*" %>
<%@ page import="com.moneygram.acclient.*" %>
<%@ page import="java.math.*" %>

<%@page import="shared.mgo.services.MGOServiceFactory"%>
<%@page import="shared.mgo.services.MGOAgentProfileService"%>
<%@page import="shared.mgo.dto.MGOAgentProfile"%><html>
<head>
<title>eMoneyGram Admin Intranet Web</title>
</head>
<body>
<%
	shared.mgo.services.AgentConnectService acs = MGOServiceFactory.getInstance().getAgentConnectService();
String country = "MEX";
//ArrayList al = (ArrayList)acs.getReceiveCountryRequirementsList(country);
//Iterator iter = al.iterator();
//out.println("Receive Country Requirements for Country: " + country);
//while (iter.hasNext())
//{
	//ReceiveCountryRequirementsInfo ri = (ReceiveCountryRequirementsInfo)iter.next();
	//out.println("<br/>Del Option:" +ri.getDeliveryOption());
	//out.println("<br/>Del Option:" +ri.getReceiveCountry());
	//out.println("<br/>Del Option:" +ri.getTypeDesc());
		

//}
//CountryCurrencyInfo cci = acs.getCountryCurrencyInfo(country, "MXN", "WILL_CALL");
//if (cci!=null)
//out.println("<br/>CCI :" +cci.getDeliveryOption() + " "  +  cci.getReceiveCurrency());

//EMTProductFieldInfo pfi = acs.getFieldsByProduct("MEX","MXN","WILL_CALL","0");		
//out.println("<br/>PFI :" + pfi.getDeliveryOption() +  " " );
//Map m =  pfi.getProductFieldInfo();
//Iterator it = m.entrySet().iterator();
 //while (it.hasNext()) {
       //Map.Entry pairs = (Map.Entry)it.next();
        //out.println("<br/>" + pairs.getKey() + " = " + pairs.getValue());
//}
//MGOAgentProfileCache.initAgentProfileCache();
out.println("<BR/> Same Day Agent");
MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
//MGOAgentProfile mgoAgentProfile = maps.getMGOProfile("MGO","Same Day");
MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId("43537899","Same Day");
out.println("Agent ID:" + mgoAgentProfile.getAgentId());
FeeInfo[] fi = acs.feeLookupAllOptions(mgoAgentProfile,new BigDecimal(222.22),"MEX",ProductType.SEND,null,null);
for (int i = 0; i < fi.length;i++)
{
	out.println("<BR> Delivery Option:" + fi[i].getDeliveryOptDisplayName());
	out.println("<BR> Fee:" + fi[i].getFeeAmount());
	out.println("<BR> Send Amt:" + fi[i].getSendAmount());
	out.println("<BR> Total Amt:" + fi[i].getTotalAmount());
	
}

out.println("<BR><BR/><BR/><BR/> Economy Agent");
	
mgoAgentProfile = maps.getMGOProfile("MGO","Economy");
out.println("Agent ID:" + mgoAgentProfile.getAgentId());
fi = acs.feeLookupAllOptions(mgoAgentProfile, new BigDecimal(222.22),"MEX",ProductType.SEND,null,null);
for (int i = 0; i < fi.length;i++)
{
	out.println("<BR> Delivery Option:" + fi[i].getDeliveryOptDisplayName());
	out.println("<BR> Fee:" + fi[i].getFeeAmount());
	out.println("<BR> Send Amt:" + fi[i].getSendAmount());
	out.println("<BR> Total Amt:" + fi[i].getTotalAmount());
	
}
%>
</body>
</html>
