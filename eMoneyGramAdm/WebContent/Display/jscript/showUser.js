function openWindow(url) {
      windowHandle = window.open(url, 'window', 'width=600,height=600');
}
var detailWindow = null;
function showUserDetail(url) {
      var msie = window.navigator.appName.indexOf("Microsoft");
      if (msie >= 0) {
            detailWindow = window.open(url, "Detail",
                        "status,scrollbars,resizable,width=800,height=400");
      } else {
            url = "../../" + url;
            detailWindow = window.open(url, "Detail",
                        "status,scrollbars,resizable,width=800,height=400");
      }
      detailWindow.focus();
}

function validatefileType(form,name){
	if(!testFileType(document.forms[name].elements["theFile"].value,
	['xls', 'xlsx', 'csv', 'txt', 'doc', 'docx', 'rtf' ])){
	$('.error' ).html('Please process a valid file.');
	$('#processing').hide();return false;
	}$('#processing').show();
	return true;
}

function testFileType(fileName,fileTypes) {
	if (!fileName) return false;
	var dots = fileName.split(".")
	fileType = "." + dots[dots.length-1];
	return (fileTypes.join(".").indexOf(fileType) != -1) ? true :false;
}