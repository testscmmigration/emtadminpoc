/*nav menu*/
/* cgomez@moneygram.com / UI developer */

if (!window.console) console = {};
console.log = console.log || function(){};
console.warn = console.warn || function(){};
console.error = console.error || function(){};
console.info = console.info || function(){};
console.debug = console.debug || function(){};

var eMGAdm ={
	nav_menu: function(nav_ul){
		nav_ul.find("li").hover(
			function(){
				if($(this).find("li").length>0){
					$(this).addClass("expanded_level1");
				}
			},
			function(){
				$(this).removeClass("expanded_level1");
				$(this).find("li").removeClass("expanded_level2");
			}
		);
		nav_ul.find("ul li").mouseenter(
			function(){
				if($(this).find("li").length>0){
					$(this).siblings().removeClass("expanded_level2");
					$(this).addClass("expanded_level2");
				}
			}
		);

		return nav_ul;
	},

	eMGAdm_contentToggler: function(){
		// content toggler

		$('.content_toggler').each(function(index){
			var content_toggler=$(this);
			if (content_toggler.find('.content_toggler_content').length>1){
				var default_content;
				if (content_toggler.find('.content_toggler_default').length>0){
					default_content=content_toggler.find('.content_toggler_default').first();
				}else{
					default_content=content_toggler.find('.content_toggler_content').first();
				}
				default_content.addClass('content_toggler_visible').show().siblings('.content_toggler_content').addClass('content_toggler_hidden').hide();
			}
			content_toggler.find('.content_toggler_button').click(function(e){
				e.preventDefault();
				var next;
				if (content_toggler.find('.content_toggler_visible').next('.content_toggler_hidden').lenght>0){
					next=content_toggler.find('.content_toggler_visible').next('.content_toggler_hidden');
				}else{
					next=content_toggler.find('.content_toggler_visible').siblings('.content_toggler_hidden').first();
				}
				next.removeClass('content_toggler_hidden').addClass('content_toggler_visible').show().siblings('.content_toggler_content').removeClass('content_toggler_visible').addClass('content_toggler_hidden').hide();
			});
		})
	},

	eMGAdm_events: function (){
		// Identification Box Toggle

		$('#adhoreport_form select#partnerSiteId, #advancedConsumerSearchForm select.partnerSiteId ').change(function(e){
			e.preventDefault();

			if($(this).val() == "All") {
				$('#adhoreport_form #ukIDrow').hide();
				$('#adhoreport_form #europeanIDrow').hide();
				$('#adhoreport_form .addContainer').hide();
				$('.europeanIDrow').show();
				$('.deIDrow').show();
				$('.ukIDrow').show();
				$('#adhoreport_form select.idType').val("");
				$('#adhoreport_form .idNumber').val("");
				$('#advancedConsumerSearchForm select.additionalIdType').val("");
				$('#advancedConsumerSearchForm select.additionalDocStatus').val("");
				$('#advancedConsumerSearchForm .searchId').val("");
			}
//		else if ($('#adhoreport_form select#partnerSiteId, #advancedConsumerSearchForm select.partnerSiteId').val() == "MGOUK") {
			else if($('#advancedConsumerSearchForm select.partnerSiteId').val() == "MGOUK" ) {
				$('.deIDrow').hide();
				$('#adhoreport_form #europeanIDrow').show();
				$('.europeanIDrow').show();
			}
			else if($('#adhoreport_form select#partnerSiteId').val() == "MGOUK" ) {
				$('#adhoreport_form #europeanIDrow').hide();
				$('#adhoreport_form #ukIDrow').show();
				$('.europeanIDrow').hide();
				$('.ukIDrow').show();
			}
			else if($(this).val() == "MGODE" ) {
				$('#adhoreport_form #ukIDrow').hide();
				$('#adhoreport_form #europeanIDrow').hide();
				$('.deIDrow').show();
				$('.ukIDrow').hide();
				$('.europeanIDrow').hide();
			}
			else {
				$('#adhoreport_form #europeanIDrow').hide();
				$('#adhoreport_form #ukIDrow').hide();
				$('#adhoreport_form .addContainer').hide();
				$('#adhoreport_form select.idType').val("");
				$('#adhoreport_form .idNumber').val("");
				$('.europeanIDrow').hide();
				$('.deIDrow').hide();
				$('.ukIDrow').hide();
				$('#advancedConsumerSearchForm select.additionalIdType').val("");
				$('#advancedConsumerSearchForm select.additionalDocStatus').val("");
				$('#advancedConsumerSearchForm .searchId').val("");
			}
		});

		$('#advancedConsumerSearchForm .addContainer, #adhoreport_form .addContainer').hide();

		$('#adhoreport_form select#additionalIdType').change(function(e) {
			e.preventDefault();

			if($(this).val() != '') {
				var addCont = $('#adhoreport_form .' + $(this).val());
				$('#adhoreport_form .addContainer').slideUp();
				addCont.slideDown();
			}
			else {
				$('#adhoreport_form .addContainer').slideUp();
			}

		});

		//populate costumer profile form data

		if ($('#advancedConsumerSearchForm select.partnerSiteId').val() != "All"
				&& $('#adhoreport_form select#partnerSiteId, #advancedConsumerSearchForm select.partnerSiteId').val() != "MGOUK"
					&& $('#adhoreport_form select#partnerSiteId, #advancedConsumerSearchForm select.partnerSiteId').val() != "MGODE") {

			$('#adhoreport_form #ukIDrow').hide();
			$('#adhoreport_form #europeanIDrow').hide();
			$('#adhoreport_form .addContainer').hide();
			$('#adhoreport_form select#additionalIdType').val("");
			$('.europeanIDrow').hide();
			$('.deIDrow').hide();
			$('.ukIDrow').hide();
			$('#advancedConsumerSearchForm select#additionalIdType').val("");
			$('#advancedConsumerSearchForm select#additionalDocStatus').val("");
			$('#advancedConsumerSearchForm .searchId').val("");
		}
		else if ($('#adhoreport_form select#partnerSiteId, #advancedConsumerSearchForm select.partnerSiteId').val() == "MGOUK") {
			if ($('#advancedConsumerSearchForm select#additionalIdType').length>0){
				var orig_value=$('#advancedConsumerSearchForm select#additionalIdType');
				$('#advancedConsumerSearchForm select#additionalIdType').val(orig_value[index]);
			}
			$('.deIDrow').hide();
			$('.europeanIDrow').hide();
			if ($('#adhoreport_form select#partnerSiteId').val() == "MGOUK") {
				$('#adhoreport_form #ukIDrow').show();
				$('.ukIDrow').show();
			}
		}
		else {
			if ($('#advancedConsumerSearchForm select#additionalDocStatus').length>0){
				var orig_value=$('#advancedConsumerSearchForm select#additionalDocStatus');
				$('#advancedConsumerSearchForm select#additionalDocStatus').val(orig_value[index]);
			}
			$('#adhoreport_form #ukIDrow').hide();
			$('.europeanIDrow').hide();
			$('.ukIDrow').hide();
			$('#adhoreport_form #europeanIDrow').hide();
		}

		if ($('#addId_passport_number_value').length>0){
			var orig_value=$('#addId_passport_number_value').text().split(' ');
			$('#addId_passport_number_inputs input').each(function(index){
				$(this).val(orig_value[index]);
			});
		}

		if ($('#addId_passport_expiry_value').length>0){
			var orig_value=$('#addId_passport_expiry_value').text().split('/');
			$('#addId_passport_expiry_inputs select').each(function(index){
				$(this).val(orig_value[index]);
			});
		}

		if ($('#addId_passport_country_value').length>0){
			var orig_value=$('#addId_passport_country_value').text();
			$('#addId_passport_country_inputs select').val($("#addId_passport_country_inputs select option[name='"+orig_value+"']").val());
		}

		if ($('#addId_license_number_value').length>0){
			var orig_value=$('#addId_license_number_value').text().split(' ');
			$('#addId_license_number_inputs input').each(function(index){
				$(this).val(orig_value[index]);
			});
		}

		if ($('#addId_card_number_1_value').length>0){
			var orig_value=$('#addId_card_number_1_value').text().split(' ');
			$('#addId_card_number_1_inputs input').each(function(index){
				$(this).val(orig_value[index]);
			});
		}
		if ($('#addId_card_number_2_value').length>0){
			var orig_value=$('#addId_card_number_2_value').text().split(' ');
			$('#addId_card_number_2_inputs input').each(function(index){
				$(this).val(orig_value[index]);
			});
		}
		if ($('#addId_card_number_3_value').length>0){
			var orig_value=$('#addId_card_number_3_value').text().split(' ');
			$('#addId_card_number_3_inputs input').each(function(index){
				$(this).val(orig_value[index]);
			});
		}

		if ($('#addId_card_expiry_value').length>0){
			var orig_value=$('#addId_card_expiry_value').text().split('/');
			$('#addId_card_expiry_inputs select').each(function(index){
				$(this).val(orig_value[index]);
			});
		}

		if ($('#addId_card_country_value').length>0){
			var orig_value=$('#addId_card_country_value').text();
			$('#addId_card_country_inputs select').val($("#addId_card_country_inputs select option[name='"+orig_value+"']").val());
		}

		// ToolTip
		var toolTipCTA = $('.toolTipCTA');

		toolTipCTA.each(function(i, e) {
			var toolTip, toolTipOffX;
			toolTip = $(this).next();
			toolTip.parent().css('position', 'relative');

			toolTip.append('<div class="arrow lf">&lt;</div>');
			if (toolTip.hasClass("addClose")){
				toolTip.append('<div class="close closeCTA">close</div>');
			}
			toolTip.find('.closeCTA').click(function(){
				toolTipCTA.each(function(){
					$(this).removeClass("tooltipCTA_active").next().hide();
				});
			});
			$(this).click(function(e) {
				e.preventDefault();
			});
			$(this).mouseenter(function(e) {
				e.preventDefault();
				if (!$(this).hasClass("tooltipCTA_active")){
					toolTipCTA.each(function(){
						$(this).removeClass("tooltipCTA_active").next().hide();
					});
					$(this).addClass("tooltipCTA_active");
					toolTipOffX = $(this).position().left;
					toolTipOffY = $(this).position().top;
					toolTipOffW = $(this).width();
					toolTip.css('left', toolTipOffX + toolTipOffW + 15);
					toolTip.css('top', toolTipOffY - 25);
					toolTip.show();
				}
			});

		});

	}
}
//construct
$(document).ready(function(e){
	eMGAdm.eMGAdm_events();
	eMGAdm.eMGAdm_contentToggler();
	eMGAdm.nav_menu($("ul.tree_menu"));
});