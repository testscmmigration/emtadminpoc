/************************************************************
open's the audit detail window
*************************************************************/
var detailWindow = null;
function showAuditLogDetail(refID)
{
	 if (!detailWindow || detailWindow.closed || detailWindow==null)  
	 {
       detailWindow = window.open("showAuditLogDetail.do?logNumber=" + refID ,"Detail","status,scrollbars,resizable,width=800");
     }
     else
     {
       detailWindow.location.href="showAuditLogDetail.do?logNumber=" + refID
     }   
     detailWindow.focus();   
}
