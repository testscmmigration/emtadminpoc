function disableInvalidStatus() {
	try {
		var combo = document.getElementsByName('custStatus');
		if (!combo || combo.length == 0) {
			return;
		}
		combo = combo[0];
		var invalidOptions = [ 'ACT:L00', // Active: Validation Level 0
							   'ACT:VAB', // Active: Verid Abandon
							   'NAT:BEM', // Not Active: Bounced Email
							   'NAT:MDF', // Not Active: Micro Deposit Fail
							   'NAT:PWF', // Not Active: Password Failures
							   'NAT:RRN', // Not Active: Receive Registration Failure
							   'PEN:INI',  // Pending: Initial
							   'ACT:L13', // Active: Validation Level 13
							   'ACT:L14' // Active: Validation Level 14

		];
		for (var i=0; i<invalidOptions.length; i++) {
			var disabledOpt = invalidOptions[i];
			for (var j=0; j<combo.options.length; j++) {
				var option = combo.options[j];
				if (option.value == disabledOpt) {
					option.disabled = 'disabled';
					option.setAttribute('disabled', 'disabled');
					$(option).optionDisable();
					break;
				}
			}
		}
	}
	catch(e) {}
}

$(document).ready(function()
{
	$.fn.extend(
	{
		optionDisable:function()
		{
			var ths = $(this);
			if(ths.attr('tagName').toLowerCase() == 'option')
			{
				if (!ths[0].selected) {	
					ths.before($('<optgroup>&nbsp;</optgroup>').css({color:'#ccc',height:ths.height()}).attr({id:ths.attr('value'),label:ths.text()})).remove();
				}
			}
			return ths;
		}
	});
	
	disableInvalidStatus();
});