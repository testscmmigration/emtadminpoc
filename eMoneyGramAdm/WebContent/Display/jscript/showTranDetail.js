/************************************************************
open's the transaction detail window
*************************************************************/
var detailWindow = null;
function showTranDetail(refID)
{
	 if (!detailWindow || detailWindow.closed || detailWindow==null)  
	 {
       detailWindow = window.open("showTransactionDetail.do?referenceId=" + refID ,"Detail","status,scrollbars,resizable,width=620");
     }
     else
     {
       detailWindow.location.href="showTransactionDetail.do?referenceId=" + refID
     }   
     detailWindow.focus();   
}
