<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

</CENTER>

<h3>${adhocReportForm.partnerSiteId} <fmt:message key="label.report.adhoc"/></h3>
<P class="error">
<html:messages id="message" property='nbrFound' message="true">
	<c:out value="${message}"/>
</html:messages>
</P>

<center>
<html:form action="/adhocReport.do" focus="beginDateText" styleId="adhoreport_form">
	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.report.adhoc.search"/></legend>

		<span class="td-label-small">Search dates by: &nbsp; &nbsp; &nbsp;
			<html:radio property="selDates" value="S">Status Dates</html:radio>
			&nbsp; &nbsp; &nbsp; &nbsp;
			<html:radio property="selDates" value="C">Create Dates</html:radio>
			&nbsp; &nbsp; &nbsp; &nbsp;
			<html:radio property="selDates" value="N">No Dates</html:radio></span>
				
		<table border='0' width='80%' cellpadding='1' cellspacing='1'>

			<tr>
				<td nowrap="nowrap" class="td-label-small">
					<fmt:message key="label.status.begin.date"/>
					<fmt:message key="label.suffix"/>&nbsp;
				</td>
				<td nowrap="nowrap" class="td-small">
					<html:text 
						size="10" 
						property="beginDateText" 
						onchange="setOtherRangeValue(this, adhocReportForm.beginDateText);" 
					/>
					<img border="0" src="../../images/calendar.gif" onclick="show_calendar('adhocReportForm.beginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='beginDateText'/></span>
				</td>
				
				<td>&nbsp;&nbsp;</td>
				
				<td nowrap="nowrap" class="td-label-small">
					<fmt:message key="label.create.begin.date"/>
					<fmt:message key="label.suffix"/>&nbsp;
				</td>
				<td nowrap="nowrap" class="td-small">
					<html:text 
						size="10" 
						property="createBeginDateText" 
						onchange="setOtherRangeValue(this, adhocReportForm.createBeginDateText);" 
					/>
					<img border="0" src="../../images/calendar.gif" onclick="show_calendar('adhocReportForm.createBeginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='createBeginDateText'/></span>
				</td>
				
			</tr>
			<tr>
				<td nowrap="nowrap" class="td-label-small">
					<fmt:message key="label.status.end.date"/>
					<fmt:message key="label.suffix"/>&nbsp;
				</td>
				<td nowrap="nowrap" class="td-small">
					<html:text 
						size="10" 
						property="endDateText" 
						onchange="setOtherRangeValue(this, adhocReportForm.endDateText);" 
					/>
					<img border="0" src="../../images/calendar.gif" onclick="show_calendar('adhocReportForm.endDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='endDateText'/></span>
				</td>
				
				<td>&nbsp;&nbsp;</td>
				
				<td nowrap="nowrap" class="td-label-small">
					<fmt:message key="label.create.end.date"/>
					<fmt:message key="label.suffix"/>&nbsp;
				</td>
				<td nowrap="nowrap" class="td-small">
					<html:text 
						size="10" 
						property="createEndDateText" 
						onchange="setOtherRangeValue(this, adhocReportForm.createEndDateText);" 
					/>
					<img border="0" src="../../images/calendar.gif" onclick="show_calendar('adhocReportForm.createEndDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='createEndDateText'/></span>
				</td>				
			</tr>
			
			<tr>			
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.min.amount"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="18" property="minimumAmount" />
					<span class="error"><html:errors property='minimumAmount'/></span>
				</td>

				<td>&nbsp;&nbsp;</td>
				
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.max.amount"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="18" property="maximumAmount" />
					<span class="error"><html:errors property='maximumAmount'/></span>
				</td>
			</tr>
					 	
			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.paymentmethod"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="paymentMethod" size="1">
						<html:option value="All"><fmt:message key="option.all"/></html:option>												
						<html:options collection="paymentMethods" property="acctTypeCode" labelProperty="acctTypeDesc" />
					</html:select>				
					
					<span class="error"><html:errors property='paymentMethod'/></span>
				</td>
				<td>&nbsp;&nbsp;</td>
				
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.profileid"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="30" property="profileId" />
					<span class="error"><html:errors property='profileId'/></span>
				</td>
			</tr>
			
			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.mgstatus"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="transStatus" size="1">
						<html:option value="All"><fmt:message key="option.all"/></html:option>												
						<html:options collection="tranStatuses" property="tranStatCode" labelProperty="tranStatDesc" />
					</html:select>				
					
				</td>
				<td>&nbsp;&nbsp;</td>
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.ssn"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="4" maxlength="4" property="SSN" />
					<span class="error"><html:errors property='SSN'/></span>
				</td>
			</tr>

			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.billingstatus"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="transSubStatus" size="1">
						<html:option value="All"><fmt:message key="option.all"/></html:option>												
						<html:options collection="tranSubStatuses" property="tranSubStatCode" labelProperty="tranSubStatDesc" />
					</html:select>				
				</td>

				<td>&nbsp;</td>
				
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.primaryaccount"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="4" maxlength="4" property="primaryAccount" />
					<span class="error"><html:errors property='primaryAccount'/></span>
				</td>
			</tr>

			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.transaction.type"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="transType" size="1">
						<html:option value="All"><fmt:message key="option.all"/></html:option>												
						<html:options collection="tranTypes" property="emgTranTypeCode" labelProperty="emgTranTypeDesc" />
					</html:select>				
				</td>
				<td>&nbsp;</td>
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.secondaryaccount"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="4" maxlength="4" property="secondaryAccount" />
					<span class="error"><html:errors property='secondaryAccount'/></span>
				</td>
			</tr>

			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.destinationcountry"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					
					<html:select name="adhocReportForm" property="destinationCountry" size="1">
						<html:option value="All"><fmt:message key="option.all"/></html:option>												
						<html:options collection="country" property="countryCode" labelProperty="countryName" />
					</html:select>				
				</td>
				<td>&nbsp;</td>
				<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.senderrt"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="9" property="senderRT" />
					<span class="error"><html:errors property='senderRT'/></span>
				</td>
			</tr>
				<tr>
					<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.receiverfirstname"/><fmt:message key="label.suffix"/>&nbsp;</td>
					<td nowrap="nowrap" class="td-small">
						<html:text size="30" property="receiverFirstName" />
						<span class="error"><html:errors property='receiverFirstName'/></span>
					</td>
					<td></td>
					<td align="right" nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.senderip"/><fmt:message key="label.suffix"/>&nbsp;</td>
					<td nowrap="nowrap" class="td-small"><html:text size="15" property="senderIP" />
					<span class="error"><html:errors property='senderIP'/></span></td>
				</tr>
				<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.report.adhoc.receiverlastname"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="30" property="receiverLastName" />
					<span class="error"><html:errors property='receiverLastName'/></span>
				</td>
				<td>&nbsp;</td>
				<td align="right" nowrap="nowrap" class="td-label-small">If search by scores<fmt:message key="label.suffix"/> &nbsp;</td>
				<td nowrap="nowrap" class="td-small">
				<b><fmt:message key="label.low.score"/><fmt:message key="label.suffix"/> &nbsp;</b><html:text property="lowScore" size="4" /> &nbsp; &nbsp;
					<b><fmt:message key="label.high.score"/><fmt:message key="label.suffix"/> &nbsp;</b><html:text property="highScore" size="4" /> &nbsp; &nbsp;
					<span class="error"><html:errors property='score'/></span>	
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='td-label-small'>
					<fmt:message key="label.use.score"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:radio property="useScore" value="Y">Yes</html:radio> &nbsp;
					<html:radio property="useScore" value="N">No</html:radio></td>
				<td>&nbsp;</td>
				<td nowrap='nowrap' class='td-label-small'>
				<fmt:message key="label.processing.type"/><fmt:message key="label.suffix"/>&nbsp;
					</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="tranRvwPrcsCode" size="1">
						<html:option value="ALL"><fmt:message key="option.all"/></html:option>
						<html:option value="CEX"><fmt:message key="label.country.exception.processing"/></html:option>
						<html:option value="MAN"><fmt:message key="label.manual.processing"/></html:option>
						<html:option value="PRE"><fmt:message key="label.premier.processing"/></html:option>
						<html:option value="ASP"><fmt:message key="label.automated.scoring.processing"/></html:option>
						<html:option value="VEL"><fmt:message key="label.velocity.processing"/></html:option>
					</html:select>
	   			</td>
			</tr>
			<tr>
				<td nowrap="nowrap" class="td-label-small"> <fmt:message key="label.receiver.agency.code"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="20" property="rcvAgcyCode" />
					<span class="error"><html:errors property='rcvAgcyCode'/></span>
				</td>
				<td>&nbsp;</td>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.retrieval.request"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="retrievalRequestCode" size="1">
						<html:option value="ALL"><fmt:message key="option.all"/></html:option>
						<html:option value="N"><fmt:message key="option.no.retrieval.request"/></html:option>
						<html:option value="Y"><fmt:message key="option.retrieval.request"/></html:option>
					</html:select>
				</td>
			</tr>

			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.lgcy.ref.nbr"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="12" maxlength="8" property="referenceNumber" />
					<span class="error"><html:errors property='referenceNumber'/></span>
				</td>
				<td>
				</td><td nowrap="nowrap" class="td-label-small"><fmt:message key="label.sort.report.by"/><fmt:message key="label.suffix"/>&nbsp;</td>
			<td nowrap="nowrap" class="td-label-small">
					<html:select name="adhocReportForm" property="sortBy" size="1">
						<html:option value="transId"><fmt:message key="label.trans.id"/></html:option>
						<html:option value="transSenderName"><fmt:message key="label.sender"/></html:option>
						<html:option value="transReceiver"><fmt:message key="label.receiver"/></html:option>
						<html:option value="transCntryCd"><fmt:message key="label.dest"/></html:option>												
						<html:option value="transStatus"><fmt:message key="label.trans.status"/></html:option>
						<html:option value="transSubStatus"><fmt:message key="label.trans.funded.status"/></html:option>
						<html:option value="transType"><fmt:message key="label.trans.type"/></html:option>
						<html:option value="transAmount"><fmt:message key="label.amount"/></html:option>
						<html:option value="transStatusDate"><fmt:message key="label.status.time"/></html:option>
						<html:option value="transCreateDate"><fmt:message key="label.create.time"/></html:option>
						<html:option value="transScores"><fmt:message key="label.trans.scores"/></html:option>
					</html:select>
			</td>
			</tr>
			<tr>
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.partner.site.id"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select name="adhocReportForm" property="partnerSiteId" size="1" styleId="partnerSiteId" onchange="toggleIdOptions()"; >
						<html:option value="All"><fmt:message key="option.all"/></html:option>
						<html:option value="MGO">MGO</html:option>
						<html:option value="WAP">WAP</html:option>
						<html:option value="MGOUK">MGOUK</html:option>						
						<html:option value="MGODE">MGODE</html:option>						
					</html:select>
				</td>
				<td>&nbsp;</td>
				<td nowrap="nowrap" class="td-label-small"></td>
				<td nowrap="nowrap" class="td-small"></td>
			</tr>
			<tr id="ukIDrow">
				<td nowrap="nowrap" class="td-label-small"><fmt:message key="label.id.type"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:select styleClass="idType" property="idType">
						<html:option value="">All</html:option>
						<html:option value="SP">Passport</html:option>
						<html:option value="ECDL">UK Driver License</html:option>
					</html:select>
					<span class="error"></span>
				</td>
				<td></td>
				<td nowrap="nowrap" class="td-label-small">ID Number<fmt:message key="label.suffix"/></td>
				<td>
					<html:text styleClass="idNumber" property="idNumber" />
				</td>
			</tr>					
		</table>
		
	<div id="formButtons">
		<input type="submit" class='transbtn2' name='submitSearch' value='Search'
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
	</div >
			
	<div id="processing"  style="visibility: hidden">
		<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
	</div >				

	</fieldset>
<logic:notEmpty name="Transactions">
	<%String dispClass = "TD-SMALL";%>	

	&nbsp;
	
	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.report.adhoc.report"/> - 
		
	<html:messages id="message" property='nbrFound' message="true">
		<c:out value="${message}"/>
	</html:messages>
     
		</legend>
		<table>
			<tbody>
				<tr><td colspan="11" align="right"><a href="../../ExcelReport.emt?id=Transactions" target="excel">
					<fmt:message key="msg.view.or.download.excel" /></a>
					</td></tr>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.id"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sender"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.receiver"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.dest"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.status"/></td>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.funded.status"/></td>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.type"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sendcurrency"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sendamount"/></td>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status.time"/></td>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.time"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.inet.purchase"/></td>	
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/> / <br /><fmt:message key="label.trans.prcs.automation"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.partner.site.id"/> </td>		
                <td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.client.trace.id"/> </td>
                <td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.processor.trace.id"/> </td>				
				</tr>
								
				<logic:iterate id="transaction" name="Transactions">
				
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<a href='../../transactionDetail.do?tranId=<bean:write filter="false" name="transaction" property="transId"/>'>
						<bean:write filter="false" name="transaction" property="transId"/>
						</a>

					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<a href='../../showCustomerProfile.do?custId=<bean:write filter="false" name="transaction" property="transCustId"/>'>
							<bean:write name="transaction" property="transSenderName"/>
						</a>						
					</td>
					
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write name="transaction" property="transReceiver"/>						
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transCntryCd"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transStatus"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transSubStatus"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transType"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align="right"><bean:write filter="false" name="transaction" property="sndISOCrncyId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align="right"><bean:write  name="transaction" property="transAmount"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transStatusDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="transaction" property="transCreateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'><bean:write filter="false" name="transaction" property="internetPurchase"/></td>
					<logic:equal name="transaction" property="sysAutoRsltCode" value="">
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
					</logic:equal>
					<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="transId"/>'>
							<bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a> / <bean:write filter="false" name="transaction" property="tranRvwPrcsCode"/></td>
					</logic:notEqual>
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'><bean:write filter="false" name="transaction" property="partnerSiteId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'><bean:write filter="false" name="transaction" property="clientTraceId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'><bean:write filter="false" name="transaction" property="processorTraceId"/></td>					
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>
				</logic:iterate>
			</tbody>			      
		</table>
	</fieldset>
</logic:notEmpty>	
</html:form>
