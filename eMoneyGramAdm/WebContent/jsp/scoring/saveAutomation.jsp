<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<H2>

</H2>
</CENTER>

<% int count = 0; %>

<html:form action="/saveAutomation.do">
<h3><fmt:message key="label.transaction.scoring.configuration" /> (${fn:escapeXml(partnerSiteId)}) </h3>
<br>

<table>
	<tbody>
		<tr>
			<td class="TD-SMALL" align="right"><b>Selected Product :&nbsp; </b></td>
			<td class="TD-SMALL">
				<bean:write filter="false" name="currentTranType" property="emgTranTypeDescLabel"/>
			</td>			
		</tr>
		
		<tr>
			<td class="TD-SMALL" colspan="2">&nbsp;</td>
		</tr>
		
		<tr>
			<td class="TD-SMALL" align="right"><b>Automate :</b></td>
			<td class="TD-SMALL">
				<b><html:checkbox property="automate"></html:checkbox></b>		  	
			</td>			
		</tr>
		<tr>		
			<td class="TD-SMALL" align="right"><b>Allow Scoring :</b></td>
			<td class="TD-SMALL">
				<b><html:checkbox property="allowScoring"></html:checkbox></b>		  	
			</td>					
		</tr>
	</tbody>
</table>
	
<br>

<TABLE >
	<TBODY>
    <TR >
	<TD align="left">
		<div id="formButtons">
					<html:submit property="submitSave" value="  Save  "  
					onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		</div>
	</TD>
	<TD>
		<div id="formButtons1">
				<html:submit property="submitCancel" value=" Cancel "  
				onclick="formButtons1.style.display='none';processing.style.visibility='visible';" />
		</div>
	</TD>
    </TR>		        
	</TBODY>
</TABLE>      

<br>	
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

</html:form>


