<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>
<h3><fmt:message key="label.transaction.score.detail"/> (<bean:write name="showTranScoreDetailForm" property="partnerSiteId" />) </h3>

<html:form action="/showTranScoreDetail.do">
<html:hidden property="type"/>

<logic:notEmpty name="tran">
<table border='0' cellpadding='1' cellspacing='1'>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.id"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
		    <bean:write name="tran" property="emgTranId" />
			<html:hidden property="tranId"/></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.status"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="status.statusCode" /> (
			<bean:write name="tran" property="tranStatDesc" />)</td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.sender"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<html:link action="/showCustomerProfile.do" paramId="custId" paramName="tran" paramProperty="sndCustId">
				<bean:write name="tran" property="sndCustId" />
			</html:link></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.funding.status"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="status.subStatusCode" /> (
			<bean:write name="tran" property="tranSubStatDesc" />)</td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.type"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="emgTranTypeCode" 
			/> (<bean:write name="tran" property="emgTranTypeDesc" />)</td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.send.amount"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="sndTotAmt" /></td>
	</tr>
	<logic:present name="tran" property="tranScore">
	<bean:define id="score" name="tran" property="tranScore" />
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.id"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgId" /> (
			<a href='../../showScoreConfig.do?submitCnfg=go&from=y&scoreCnfgId=<bean:write name="score" property="scoreCnfgId" />'>
			<fmt:message key="label.see.config.detail"/></a>)</td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.ver.nbr"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgVerNbr" /></td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.stat.code" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgStatCode" /></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.total.weight.included"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="totalWeight" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.tran.score.nbr"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="tranScoreNbr" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.rslt.code" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="sysAutoRsltCode" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.review.code"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreRvwCode" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.review.cmnt.text" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="tranScoreCmntText" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.date"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="createDate" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.userid" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="createUserid" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="lastUpdateDate" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.userid" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="lastUpdateUserid" /></td>
	</tr>
	</logic:present>
</table>

<logic:present name="tran" property="tranScore">
<bean:define id="catScoreList" name="score" property="tranScoreCatList" />
<logic:notEmpty name="catScoreList">
<br />
<table border='1' cellpadding='1' cellspacing='1'>
	<%String dispClass = "TD-SMALL";%>
	<tr>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.code" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.name" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.desc" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.list.type" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.data.type" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.incl.flag" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.wgt.nbr" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.category.weight.percentage" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.rslt.data" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.raw.score" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.wgt.score" /></TD>
	</tr>
	<logic:iterate id="catScore" name="catScoreList">
	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>' align='center' valign='top'>
			<bean:write name="catScore" property="scoreCatCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="scoreCatName"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="scoreCatDesc"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="listTypeCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="dataTypeCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='center' valign='top'>
			<bean:write name="catScore" property="inclFlag"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="catWgtNbr"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="wgtPct"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="scoreQueryRsltData"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="rawScoreNbr"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="wgtScoreNbr"/></td>
	<tr>
	<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
		else dispClass = "TD-SHADED-SMALL"; %>
	</logic:iterate>
</table>
</logic:notEmpty>
</logic:present>
</logic:notEmpty>
<br />
<input type='submit' name='submitReturn' value='Return' />

</html:form>
