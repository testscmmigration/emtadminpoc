<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script>
	$("#partnerSiteId").change(function() {
		$("#scoreConfigurationForm").submit();
	});
</script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>

<h3>
	<fmt:message key="label.score.config.detail"/>:
	<logic:notEmpty name="showScoreConfigForm" property="partnerSiteId">
		<bean:write name="showScoreConfigForm" property="partnerSiteId" />
	</logic:notEmpty>
	<logic:empty name="showScoreConfigForm" property="partnerSiteId">
		MGO US
	</logic:empty>
</h3>
<center>

<html:form action="/showScoreConfig.do" styleId="scoreConfigurationForm">
<div id="partnerSiteSelect_div">Select Partner Site: 
    <html:select property="partnerSiteId" size="1" styleId="partnerSiteId" onchange="javascript:document.getElementById('scoreConfigurationForm').submit()">
        <html:option value="MGO">MGO</html:option>
        <html:option value="MGOUK">MGO UK</html:option>
        <html:option value="MGODE">MGO DE</html:option>
    </html:select>
</div>

<html:select property="scoreCnfgId" size="1">
	<html:options collection="configs" property="value" labelProperty="label" />
</html:select> &nbsp; &nbsp; &nbsp;
<html:submit property='submitCnfg' value='Go' />

<logic:equal name="showScoreConfigForm" property="from" value="y">
&nbsp; &nbsp; &nbsp; <a href="javascript: window.history.go(-1);">Return to Transaction Score</a>
</logic:equal>

<br />

<logic:notEmpty name="scoreConfig">
<table border='0' cellpadding='1' cellspacing='1'>
	<tr>
		<td valign="top">
			<table>
				<tr>
					<td colspan="2" align="center"><h4><fmt:message key="label.transaction.type"/></h4></td>
				</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.type.code"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="transactionType" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.type.desc"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="emgTranTypeDesc" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.tran.type.score.flag"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="scoreFlag" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.tran.type.auto.approval.flag"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="autoAprvFlag" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="typeLastUpdateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="typeLastUpdateUserid" /></td>
		    	</tr>
			</table>
		</td>
		<td> &nbsp; &nbsp; </td>
		<td valign="top">
			<table>
				<tr>
					<td colspan="2" align="center"><h4><fmt:message key="label.score.configuration"/></h4></td>
				</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.version.number"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="configurationVersionNumber" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.status.code"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="status" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.approve.threshold"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="acceptThreshold" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.reject.threshold"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="rejectThreshold" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.total.weight.included"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="totalWeight" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="createDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="createUserid" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="lastUpdateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="scoreConfig" property="lastUpdateUserid" /></td>
		    	</tr>
			</table>
		</td>
	</tr>
</table>

<bean:define id="catCnfgList" name="scoreConfig" property="categoryList" />
<logic:notEmpty name="catCnfgList">
<logic:iterate id="catCnfg" name="catCnfgList">

<br />
<fieldset>
	<legend class="td-label"> <fmt:message key="label.scoring.config.id"/>: 
		<bean:write name="showScoreConfigForm" property="scoreCnfgId" />
		& <fmt:message key="label.scoring.category.code"/>:
		<bean:write name="catCnfg" property="scoreCategoryCode" />
	</legend>

<table>
	<tr>
		<td valign="top">
			<table>
				<tr>
					<td colspan="2" align="center"><h4><fmt:message key="label.score.category"/></h4></td>
				</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.score.category.code"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="scoreCategoryCode" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.score.category.name"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="scoreCategoryName" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.score.category.desc"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="scoreCategoryDesc" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.category.list.type.code"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="listTypeCode" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.category.data.type.code"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="dataTypeCode" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.data.max.length"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="maximumLengthQuantity" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catCreateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catCreateUserid" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catLastUpdateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catLastUpdateUserid" /></td>
		    	</tr>
			</table>
		</td>
		<td> &nbsp; &nbsp; </td>
		<td valign="top">
			<table>
				<tr>
					<td colspan="2" align="center"><h4>Score Category Configuration</h4></td>
				</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.category.include.flag"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="inclFlag" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.category.weight.number"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="weight" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.category.weight.percentage"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="weightPercentage" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catCnfgCreateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="createUserid" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="catCnfgLastUpdateDate" /></td>
		    	</tr>
				<tr>
					<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.user"/>: </td>
					<td  nowrap='nowrap' class='TD-SMALL'>
		    			<bean:write name="catCnfg" property="lastUpdateUserid" /></td>
		    	</tr>
			</table>
		</td>
	</tr>
</table>
<br />

<bean:define id="valList" name="catCnfg" property="scoreValues" />
<logic:notEmpty name="valList">

<table border='1' cellpadding='1' cellspacing='1'>
	<%String dispClass = "TD-SMALL";%>
	<tr>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.value" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.points" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.create.date" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.create.userid" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.last.update.date" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.last.update.userid" /></TD>
	</tr>
	<logic:iterate id="value" name="valList">
	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="value" property="value"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="value" property="points"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="value" property="valCreateDate"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="value" property="createUserid"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="value" property="valLastUpdateDate"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="value" property="lastUpdateUserid"/></td>
	<tr>
	<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
		else dispClass = "TD-SHADED-SMALL"; %>
	</logic:iterate>
</table>

</logic:notEmpty>

</fieldset>

</logic:iterate>
</logic:notEmpty>
</logic:notEmpty>
</html:form>
</center>