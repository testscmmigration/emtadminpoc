<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>
<html:form action="/showAchReturnFileUpload.do" method="post" enctype="multipart/form-data" focus="theFile">
<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3><fmt:message key="label.file.achreturn.upload.title" /></h3>

	<br><br>

	<table class="TD-SMALL" border="0" width="100%">
	<tr>
		<td width="80" valign="top"><b><fmt:message key="label.file.name" />: </b></td>
		<td>
			<div id="formSubmitProcess">
				<html:file name="showAchReturnFileUploadForm" property="theFile" size="20" />
				<html:submit property="submitProcess" value="Process" 
				onclick="return validatefileType(this,'showAchReturnFileUploadForm');"/>
			</div>
			
		</td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	</table>
	
	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>

	<table class="TD-SMALL" border="0" width="100%">

	<tr>
		<td colspan="2">
			&nbsp;
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<b><fmt:message key="label.file.upload.History" /></b>
		</td>
	</tr>

	<tr>
		<td nowrap='nowrap' colspan="2">
			<table border="0" width="100%">
				<tr class='TD-HEADER-SMALL'>
					<td>&nbsp;<fmt:message key="label.file.control.number" /></td>
					<td>&nbsp;<fmt:message key="label.file.receive.date" /></td>
					<td>&nbsp;<fmt:message key="label.file.total.upload.rows" /></td>
					<td>&nbsp;<fmt:message key="label.file.total.upload.good.rows" /></td>
					<td>&nbsp;<fmt:message key="label.file.total.upload.failed.rows" /></td>
					<td>&nbsp;<fmt:message key="label.file.process.name" /></td>
				</tr>

				<%String dispClass = "TD-SHADED-SMALL";%>
				
				<logic:iterate id="fileControl" name="fileControlRecords">
					<tr>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="file_cntl_seq_nbr"/>
						</td>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="receive_date"/>
						</td>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="rec_cnt"/>
						</td>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="proc_cnt_1"/>
						</td>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="proc_cnt_2"/>
						</td>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<bean:write filter="false" name="fileControl" property="process_name"/>
						</td>
					</tr>
					<%  
						if (dispClass.equals("TD-SHADED-SMALL")) {
							dispClass = "TD-SMALL"; 
						} else {
							dispClass = "TD-SHADED-SMALL";
						}  
					%>					
				</logic:iterate>
			</table>
		</td>
	</tr>
	
	</table>


</html:form>