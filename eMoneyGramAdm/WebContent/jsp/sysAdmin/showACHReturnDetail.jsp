<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>


<h3><fmt:message key="label.achreturn.trx.detail"/></h3>

<html:form action="/showACHReturnDetail.do">

<logic:notEmpty name="achReturn">
<table border='0' cellpadding='1' cellspacing='1'>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.trx.id"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
		    <bean:write name="achReturn" property="achReturnTranId" />
		    <html:hidden name="achReturn" property="achReturnTranId" />
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.status"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<html:select name="showACHReturnDetailForm" property="achStatusCode" size="1">
				<html:options collection="achTranStatuses" property="achReturnStatCode" labelProperty="achReturnStatDesc" />
			</html:select>
		<input type="submit" name="submitSave" value=" Save " />	
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.accnt.type"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="accountType" /></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.asofdate"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="asOfDate" /></td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.coid"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="companyId"/></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.creditamt"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="creditAmount" /></td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.debitamt"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="debitAmount" /></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.descdate"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="descriptiveDate" /></td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.effdate" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="effectiveDate" /></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.filecntrl.num"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="fileControlSequenceNumber" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.fileid"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="fileId" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.holder.name" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="accountHolderName" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.individ"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="individualName" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.origtrc.nbr" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="originalTraceNo" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.retreason"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="returnReasonDesc" /> (
			<bean:write name="achReturn" property="returnReasonCode" />)</td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.rettrc.nbr" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="returnTraceNo" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.rettype"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="returnTypeDesc" /> (
			<bean:write name="achReturn" property="returnTypeCode" />)</td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.setlaba.nbr" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="settlementBank" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.setacct.nbr"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="settlementAccount" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.acct.nbr" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="accountNo" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.srchres"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="searchResult" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.trans.code" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="returnStatCode" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.internal.transid"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="tranId" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.micro.transid" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="microTranId" /></td>
	</tr>

	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.achreturn.last.update"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="achReturn" property="lastUpdateDate" /> by:
			<bean:write name="achReturn" property="lastUpdateUserId" /></td>
		<td> &nbsp; </td>
		<td> &nbsp; </td>
		<td> &nbsp; </td>
	</tr>

</table>

</logic:notEmpty>
<br />
<input type="submit" name="submitReturn" value="Return" />
</center>

</html:form>
