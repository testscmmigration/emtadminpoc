<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
	
</span>


</CENTER>

<h3><fmt:message key="label.edit.country.exception"/></h3>
<p><b>Partner Site:</b> <bean:write name="editCountryExceptionForm" property="partnerSiteId" /></p>
<html:form action="/editCountryException.do" focus="sndMaxAmt">
	<html:hidden name="editCountryExceptionForm" property="rcvIsoCntryCode" />
	<html:hidden name="editCountryExceptionForm" property="isoCntryCode" /><br />
	<html:hidden name="editCountryExceptionForm" property="addEditFlag" />
	<table border="0">
	    <tbody>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.rcv.iso.cntry.code"/>: </td>
	            <td><bean:write name="editCountryExceptionForm" property="rcvIsoCntryCode" />
	            <jsp:useBean id="countryMaster" class="emgshared.model.CountryMasterBean"  scope="request"/> - 
	            <bean:write name="countryMaster" property="countryName" />
	            </td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.iso.cntry.code"/>: </td>
	            <td><bean:write name="editCountryExceptionForm" property="isoCntryCode" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.send.allow.flag"/>: </td>
	            <td><html:radio name="editCountryExceptionForm" property="sndAllowFlag" value="Y" />Yes &nbsp; &nbsp; 
					<html:radio name="editCountryExceptionForm" property="sndAllowFlag" value="N" />No</td>
			</tr>
			<tr>
	            <td class="TD-LABEL"><fmt:message key="label.manual.review.allow.flag"/>: </td>
	            <td><html:radio name="editCountryExceptionForm" property="manualReviewAllowFlag" value="Y" />Yes &nbsp; &nbsp; 
					<html:radio name="editCountryExceptionForm" property="manualReviewAllowFlag" value="N" />No</td>
			</tr>
			<html:hidden name="editCountryExceptionForm" property="tranAuthFlag" value="N"/>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.send.maximum.amount"/>: </td>
	            <td><html:radio name="editCountryExceptionForm" property="defaultMaxAmtFlag" value="Y" />Use System Default Maximum Amount and 
		    		currently, it is set to <b><bean:write name="editCountryExceptionForm" property="sysDfltMaxAmt" /></b>
		    		<bean:write name="editCountryExceptionForm" property="partnerSiteCurrency" />
					<html:hidden property="sysDfltMaxAmt"/></td>
			</tr>
			<tr>
				<td> &nbsp; </td>
				<td><html:radio name="editCountryExceptionForm" property="defaultMaxAmtFlag" value="N" />Amount is
					<html:text name="editCountryExceptionForm" property="sndMaxAmt" size="12" maxlength="12" /></td>
			</tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.maximum.threshold.amount"/>: </td>
	            <td><html:text name="editCountryExceptionForm" property="sndThrldWarnAmt" size="12" maxlength="12" /></td>
			</tr>
	        <tr>
	            <td class="TD-LABEL" valign='top'><fmt:message key="label.exception.comment"/>: </td>
	            <td><html:textarea name="editCountryExceptionForm" property="excpCmntText"  rows="6" cols="50" 
	            	onkeypress="textCounter(this, 255);"  /></td>
			</tr>
	        <tr>
	            <td colspan="2"> &nbsp; </td>
	        </tr>
	        <tr>
			    <td colspan="2" align="center">
					<html:submit property="submitSave" value="Save" /> &nbsp; &nbsp;
					<html:submit property="submitCancel" value="Cancel" />
			    </td>
			</tr>
	    </tbody>
	</table>

</html:form>
