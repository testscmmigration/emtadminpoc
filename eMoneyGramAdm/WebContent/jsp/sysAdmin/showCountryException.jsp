<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>

<html:form action="/showCountryException.do" focus="submitAdd">

	<div id="headCEL" class="styledHead ddHead">
		<h3><fmt:message key="label.country.exception.list" /></h3>
		<html:select property="selectedCountry" onchange="this.form.submit()">
			<html:options collection="partnerList" property="country" labelProperty="partnerSiteDesc" />
		</html:select>
		<logic:equal name="auth" property="editCntryExcp" value="true">
			<html:submit property="submitAdd" value="Add New Country Exception" styleClass="submitAdd" />
		</logic:equal>
	</div>
	
	<table border='0' cellpadding='1' cellspacing='1' id="tblCEL">
		<tbody>
		<logic:notEmpty name="excps">
		<%String dispClass = "TD-SMALL";%>
			<tr>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.rcv.code" /></TD>		
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.allow.flag" /></TD>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.automation.override" /></TD>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.max.amount" /></TD>		
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.warn.amount" /></TD>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.comment.text" /></TD>
			<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.action.text" /></td>
			</tr>
			<logic:iterate id="excp" name="excps">
			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
					<logic:equal name="auth" property="editCntryExcp" value="true">
						<a href='../../editCountryException.do
						?id1=<bean:write filter="false" name="excp" property="rcvIsoCntryCode"/>
						&id2=<bean:write filter="false" name="excp" property="isoCntryCode"/>
						&partnerName=<bean:write filter="false" name="showCountryExceptionForm" property="partnerSiteId"/>'>
						<bean:write filter="false" name="excp" property="rcvIsoCntryCode" /> - 
						<bean:write filter="false" name="excp" property="rcvCountryName" /> 
						</a>
					</logic:equal>
					<logic:equal name="auth" property="editCntryExcp" value="false">
				    	<bean:write filter="false" name="excp" property="rcvIsoCntryCode" />
					</logic:equal>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center' valign='top'>
					<bean:write filter="false" name="excp" property="sndAllowFlag" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center' valign='top'>
					<bean:write filter="false" name="excp" property="manualReviewAllowFlag" />
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
					<logic:equal name="excp" property="sndAllowFlag" value="Y">
					<logic:equal name="excp" property="sndMaxAmt" value="0.00">
						System Default
					</logic:equal>
					<logic:notEqual name="excp" property="sndMaxAmt" value="0.00">
						<bean:write filter="false" name="excp" property="sndMaxAmt"/>
					</logic:notEqual>
					</logic:equal>
					<logic:notEqual name="excp" property="sndAllowFlag" value="Y">
						N/A
					</logic:notEqual>					
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
					<logic:equal name="excp" property="sndAllowFlag" value="Y">
						<bean:write filter="false" name="excp" property="sndThrldWarnAmt"/>
					</logic:equal>
					<logic:notEqual name="excp" property="sndAllowFlag" value="Y">
						N/A
					</logic:notEqual>					
				</td>
				<td class='<%=dispClass%>'>
					<bean:write filter="false" name="excp" property="excpCmntText"/></td>
				<logic:equal name="auth" property="editCntryExcp" value="true">
					<td nowrap='nowrap' class='<%=dispClass%>' align='center' valign='top'><a href='../../showCountryException.do?id1=<bean:write 
						filter="false" name="excp" property="rcvIsoCntryCode"/>&id2=<bean:write 
						filter="false" name="excp" property="isoCntryCode"/>'>delete</a></td>
				</logic:equal>
				<logic:equal name="auth" property="editCntryExcp" value="false">
					<td> &nbsp; </td>
				</logic:equal>
			<tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>
		</logic:notEmpty>
		</tbody>			      
	</table>

</html:form>
