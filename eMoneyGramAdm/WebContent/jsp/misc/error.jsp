<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html>
<head>
	<link href="../../theme/eMGAdm.css" rel="stylesheet" type="text/css" />
	<title>eMoneyTransfer Admin Intranet Web</title>
	<html:base/>
</head>
<body MARGINWIDTH="0" MARGINHEIGHT="0" rightMargin="0" topMargin="0" leftMargin="0" bottomMargin="0" style="MARGIN-TOP: 0px; MARGIN-LEFT: 0px">
<table border="0" width='100%"' height='20%' cellspacing="0" cellpadding="0" >

	<tr height='36' valign='top' >
		<td width='6%' bgcolor='#000000' valign='top' align='left' colspan='2'>
			<img border='0' src='../../images/emoneygramheader.jpg'  />
		</td>
		<td width='*' bgcolor='#000000' colspan='3'>
			
		</td>
	</tr>
	<tr height='40'>
		<td colspan='3' align='top'>
			<table border="0" bgcolor="#FFFFFF"  width="100%" cellPadding="0" cellSpacing="0" >
				<tr>
					<td width="3%" bgcolor="#FFFFFF" valign="top" align="left"></td>
					<td bgcolor="#ffffff" align='left' valign='top' nowrap='true' class="GREY-BOLD" colspan='1'>
				</tr>
				<tr>
					<td></td>
					<td colspan='2' align='left' class='TITLE' bgcolor='#FFFFFF'><br/>
						&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="6%" bgcolor="#ffffff" align="left" valign="top" ></td>
		<td width="84%" valign="top" align="left"></td>
		<td width='10%' colspan='2'></td>
	</tr>
	<tr>
		<td colspan="4" valign="bottom">
		</td>
	</tr>
</table>

<br/>

<span class="error" style="color: #cc0033;font-weight: bold;padding-left: 5%">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/login" focus="userID" style="margin-left: 5%;">

	<table border='0'>
	<tr>
		<td  class="TD-LABEL">User ID:</td>
		<td colspan="2"><html:text property="userID" maxlength="50" size="25\" autocomplete=\"off" /><span class="error"> <html:errors property="userID"/></span></td>
	</tr>
	<tr>
		<td  class="TD-LABEL">Password:</td>
		<td colspan="2"><html:password property="userPassword" maxlength="30" size="25\" autocomplete=\"off"></html:password>
			<span class="error"><html:errors property="userPassword"/></span>
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2">	<input type="submit" name="submit" value="Login"/>
		</td>
	</tr>
	</table>

	<p/>

</html:form>
<div style="margin-left: 5%;">
<h3>
	<fmt:message key="exception.unexpected"/>
<br />
</h3>

<br />
<b>
Please print this screen and submit it to the system support team.
</b>
<br /><br />
<html:link action="index.do" >Return to the application.</html:link>


<logic:notEmpty name="exceptionMessages">
	<table>
		<logic:iterate id="exceptionMessage" name="exceptionMessages">
		<tr>
			<td nowrap='nowrap' class='ERROR' valign='top' align='right'>
				<bean:write name="exceptionMessage" property="msgKey" /></td>
			<td> &nbsp; </td>
			<td class='ERROR' valign='top'>
				<bean:write name="exceptionMessage" property="msgText" /></td>
		</tr>
		</logic:iterate>
	</table>
</logic:notEmpty>
<br />
</div>
</body>
</html>
