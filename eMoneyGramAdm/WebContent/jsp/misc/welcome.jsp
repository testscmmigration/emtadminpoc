<%@page import="com.moneygram.common.util.StringUtility"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<%@ page import="org.apache.struts.action.ActionMessages"%>
<%@ page import="emgshared.property.EMTVersionProperties"%>

<h3 align='left'>Welcome to eMoneyTransfer Admin Intranet
	Application.</h3>

<h3>
	<br /> <br /> You are in <font color='blue'> <%
 	String mgi_env = System.getProperty("mgi.env");
 	if (StringUtility.isNullOrEmpty(mgi_env))
 		mgi_env = "";
 %> 
 <%emgshared.property.EMTVersionProperties EMTVersionProperties = null; %>
 <%=mgi_env%></font> environment. <br /> <br /> Current build version is <font
		color='blue'><%=EMTVersionProperties.getBuildString()%></font>.
</h3>