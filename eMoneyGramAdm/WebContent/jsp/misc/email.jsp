
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html:html>
<HEAD>

<%@ page 
language="java"

contentType="text/html; charset=ISO-8859-1"

pageEncoding="ISO-8859-1"

%>

<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="GENERATOR" content="IBM WebSphere Studio">

<META http-equiv="Content-Style-Type" content="text/css">
<LINK href="../../theme/eMGAdm.css" rel="stylesheet"
	type="text/css">
<TITLE></TITLE>
</HEAD>

<BODY>

<P>
The password has been reset for user id <bean:write name="emailForm" property="uid" /> (
<bean:write name="emailForm" property="firstName" />
<bean:write name="emailForm" property="lastName" />).
<br />
An e-mail has been sent to <bean:write name="emailForm" property="email" /> with a new password.
</P>
<html:button property="close" value="Close Window" onclick="window.close()"/>
</BODY>
</html:html>
