<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<h3>You are not authorized to view this section. Please contact your Administrator.<br />
</h3>
<a href="../../welcome.do">Return to the application.</a>