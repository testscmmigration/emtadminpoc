<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<span class="error">
	<html:errors property="org.apache.struts.action.GLOBAL_ERROR"/>
</span>

<html:form action="/changePassword" focus="newPassword">
	<br/>
	<table border='0'>
	<tr>
		<td  class="TD-LABEL">New Password:</td>
		<td colspan="2">
			<html:password property="newPassword" maxlength="30" size="25" />
			<span class="error">
				<html:errors property="newPassword"/>
			</span>
		</td>
	</tr>
	<tr>
		<td  class="TD-LABEL">Confirm New Password:</td>
		<td colspan="2">
			<html:password property="confirmNewPassword" maxlength="30" size="25" />
			<span class="error">
				<html:errors property="confirmNewPassword"/>
			</span>
		</td>
	</tr> 
	</table>
	<br/>
	<input type="submit" name="changePassword" value="Submit Password Change"/>
	<c:if test="${userProfile.passwordChangeRequired==false}">
		<input type="submit" name="cancel" value="Cancel" />
	</c:if>

</html:form>