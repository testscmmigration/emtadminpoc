<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<fmt:setTimeZone value="${timeZone}" />

<html:form action="/viewAddressHistory" >

</center>
<h3>Customer Addess History - <c:out value="${addressCount}" ></c:out> address record(s)</h3>
<br />
<a href='../../showCustomerProfile.do?custId=<c:out value="${custId}"/>&basicFlag=N'><fmt:message key="link.back.to.customer" /></a>
<logic:notEmpty name="addresses">

<c:choose>
	<c:when test="${viewAddressHistoryForm.country == 'GBR'}">
		<table border='0' cellpadding='0' cellspacing='0' width='100%'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.create.status" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.createdby" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr1" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr2" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.town.city" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.county" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.country" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.postcode" /></td>
			</tr>
			<% String dispClass = "td-small"; %>
			<logic:iterate id="address" name="addresses">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<fmt:formatDate value="${address.created}" pattern="MMM dd yy h:mm a z" /> - 
						<bean:write name="address" property="statusCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="createdBy" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="addressLine1" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine2" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="city" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="countyName" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="isoCountryCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="postalCode" /><bean:write name="address" property="zip4" />
					</td>
				</tr>
				<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>
			</logic:iterate>
		</table>
	</c:when>
	<c:when test="${viewAddressHistoryForm.country == 'DEU'}">
		<table border='0' cellpadding='0' cellspacing='0' width='100%'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.create.status" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.createdby" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.building.no" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.street" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.town.city" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.country" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.postcode" /></td>
			</tr>
			<% String dispClass = "td-small"; %>
			<logic:iterate id="address" name="addresses">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<fmt:formatDate value="${address.created}" pattern="MMM dd yy h:mm a z" /> - 
						<bean:write name="address" property="statusCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="createdBy" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="addressBuildingName" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine1" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="city" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="isoCountryCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="postalCode" />
					</td>
				</tr>
				<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>
			</logic:iterate>
		</table>
	</c:when>
	<c:otherwise>  
		<table border='0' cellpadding='0' cellspacing='0' width='100%'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.create.status" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.createdby" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr1" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.addr2" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.city.state" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.country" /></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="header.addr.history.zip" /></td>
			</tr>
			<% String dispClass = "td-small"; %>
			<logic:iterate id="address" name="addresses">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<fmt:formatDate value="${address.created}" pattern="MMM dd yy h:mm a z" /> - 
						<bean:write name="address" property="statusCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="createdBy" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write name="address" property="addressLine1" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="addressLine2" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="city" />,<bean:write name="address" property="state" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="isoCountryCode" />
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'  align='left'>
						<bean:write name="address" property="postalCode" />
					</td>
				</tr>
				<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>
			</logic:iterate>
		</table>	
	</c:otherwise>
</c:choose>

<br />

</logic:notEmpty>
</html:form>

</center>
