<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style>

.consumerProfile{font-family:Arial; font-size:11px; color:#000;}
.consumerProfile td{padding:8px 0;}
.consumerProfile label{font-weight:bold;}
.consumerProfile b, .consumerProfile label{width:100px; float:left; display:block; text-align:right; margin-right:10px;}
.consumerProfile input, .consumerProfile select{width:100px; border:1px solid #000;}
.consumerProfile .stateSelect {width:auto;}
.consumerProfile .zip input{width:40px;}
.consumerProfile .check {width:20px; float:left; border:none;}
.consumerProfile .miniLabel{width:auto; margin-right:15px;}
.consumerProfile .buttons input{width:auto; border-top:1px solid #ffffff; border-left:1px solid #ffffff; border-right:1px solid #333333; border-bottom:1px solid #333333;}

</style>

<h3>
<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>
</h3>

<h3>Update Consumer Profile</h3>

<html:form action="updateCustomerProfile">
<html:hidden property="partnerSiteId" />
	<logic:equal name="duplicateProfileOverride" value="Y">
		<logic:equal name="allowDuplicate" value="Y">
			<td nowrap="nowrap" class="td-small" align="right"><b>Allow Duplicate:</b><html:checkbox  property="allowOverride" /></td>
			<td><span class="error">
				<html:messages id="message" property="duplicateProfileOverrideErr">
					<c:out value="${message}"/>
				</html:messages>
				</span>
			</td>
			</tr>
		</logic:equal>
	</logic:equal>
	
	<logic:empty name="duplicateProfileOverride" >
		<logic:equal name="allowDuplicate" value="Y">
			<td nowrap="nowrap" class="td-small" align="right">&nbsp;<html:hidden  value="false" property="allowOverride" /></td>
			<td><span class="error">
				<html:messages id="message" property="duplicateProfileOverrideErr">
					<c:out value="${message}"/>
				</html:messages>
				</span>
			</td>
			</tr>
		</logic:equal>
	</logic:empty>

<c:choose>
	<c:when test="${updateCustomerProfileForm.partnerSiteId == 'MGO' || updateCustomerProfileForm.partnerSiteId == 'WAP'}">
		<TABLE border="0" width="1100px" class="consumerProfile">
		<TBODY>
			<tr>
				<td>
					<b><fmt:message key="label.first.name"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="custFirstName" size="30" maxlength="20" />
					<span class="error">
						<html:messages id="message" property="firstName">
							<c:out value="${message}"/>
						</html:messages>
					</span>					
				</td>
				<td width="350px">
					<table width="100%" border="0" class="consumerProfile" cellspacing="0" cellpadding="0">
					  <tr>
					    <td width="25%"><b><fmt:message key="label.middle.name"/><fmt:message key="label.suffix"/></b><br>
					    <span style="margin-left: 55px"><fmt:message key="label.optional"></fmt:message></span></td>
					    <td>
					    	<html:text name="updateCustomerProfileForm" property="middleName" size="30" maxlength="20" />
					    </td>
					  </tr>
					</table>
		    		<span class="error">
						<html:messages id="message" property="middleName">
							<c:out value="${message}"/>
						</html:messages>
					</span>					
				</td>
				<td>
				</td>
			</tr>
					<tr>
						<td>
							<b><fmt:message key="label.last.name" /> 
								<fmt:message key="label.suffix" /> </b>
							<html:text name="updateCustomerProfileForm" property="custLastName" size="40" maxlength="30" /> 
							<span class="error"> 
								<html:messages id="message" property="lastName">
									<c:out value="${message}" />
								</html:messages> 
							</span>
						</td>
						<td>
							<table width="100%" border="0" class="consumerProfile" cellspacing="0" cellpadding="0">
							  <tr>
							    <td width="25%" nowrap="nowrap"><b style=""><fmt:message key="label.second.last.name"/><fmt:message key="label.suffix"/></b><br>
							    <span style="margin-left: 55px"><fmt:message key="label.optional"></fmt:message></span>
									</td>
							    <td><html:text name="updateCustomerProfileForm" property="custSecondLastName" size="40" maxlength="30" /></td>
							  </tr>
							</table>
				    		<span class="error">
								<html:messages id="message" property="secondLastName">
									<c:out value="${message}"/>
								</html:messages>
							</span>					
						</td>
						<td></td>
					</tr>
					<tr>
				<td>
					<b><fmt:message key="label.street.address"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="addressLine1"  size="30" maxlength="60"/>
					<span class="error">
						<html:messages id="message" property="addressLine1">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
					<b><fmt:message key="label.city"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="city" size="20" maxlength="40"/>
					<span class="error">
						<html:messages id="message" property="city">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
					<b><fmt:message key="label.state"/><fmt:message key="label.suffix"/></b>
					<html:select size="1" name="updateCustomerProfileForm" property="state" styleClass="stateSelect">
						<html:option value="">
							Select a State
						</html:option>
						<html:option value="AL">
							ALABAMA - AL
						</html:option>
					    <html:option value="AK">
					      	ALASKA - AK
						</html:option>		
						<html:option value="AZ">
						    ARIZONA - AZ
						</html:option>		
						<html:option value="AR">
						    ARKANSAS - AR
						</html:option>
					    <html:option value="CA">
						    CALIFORNIA - CA
						</html:option>
						<html:option value="CO">
						    COLORADO - CO
						</html:option>		
						<html:option value="CT">
						    CONNECTICUT - CT
						</html:option>		
						<html:option value="DE">
						    DELAWARE - DE
						</html:option>	
						<html:option value="DC">
						    DISTRICT OF COLUMBIA - DC
						</html:option>		
						<html:option value="FL">
						    FLORIDA - FL
						</html:option>		
						<html:option value="GA">
						    GEORGIA - GA
						</html:option>		
						<html:option value="HI">
						    HAWAII - HI
						</html:option>
						<html:option value="ID">
						    IDAHO - ID
						</html:option>		
						<html:option value="IL">
						    ILLINOIS - IL
						</html:option>
						<html:option value="IN">
						    INDIANA - IN
						</html:option>		
						<html:option value="IA">
						    IOWA - IA
						</html:option>	
						<html:option value="KS">
						    KANSAS - KS
						</html:option>		
						<html:option value="KY">
						    KENTUCKY - KY
						</html:option>		
						<html:option value="LA">
						    LOUISIANA - LA
						</html:option>		
						<html:option value="ME">
						    MAINE - ME
						</html:option>		
						<html:option value="MD">
						    MARYLAND - MD
						</html:option>		
						<html:option value="MA">
						    MASSACHUSETTS - MA
						</html:option>		
						<html:option value="MI">
						    MICHIGAN - MI
						</html:option>		
						<html:option value="MN">
						    MINNESOTA - MN
						</html:option>		
						<html:option value="MS">
						    MISSISSIPPI - MS
						</html:option>		
						<html:option value="MO">
						    MISSOURI - MO
						</html:option>		
						<html:option value="MT">
						    MONTANA - MT
						</html:option>		
						<html:option value="NE">
						    NEBRASKA - NE
						</html:option>
						<html:option value="NV">
						    NEVADA - NV
						</html:option>		
						<html:option value="NH">
						    NEW HAMPSHIRE - NH
						</html:option>		
						<html:option value="NJ">
						    NEW JERSEY - NJ
						</html:option>		
						<html:option value="NM">
						    NEW MEXICO - NM
						</html:option>		
						<html:option value="NY">
						    NEW YORK - NY
						</html:option>		
						<html:option value="NC">
						    NORTH CAROLINA - NC
						</html:option>
						<html:option value="ND">
						    NORTH DAKOTA - ND
						</html:option>		
						<html:option value="OH">
						    OHIO - OH
						</html:option>		
						<html:option value="OK">
						    OKLAHOMA - OK
						</html:option>		
						<html:option value="OR">
						    OREGON - OR
						</html:option>		
						<html:option value="PA">
						    PENNSYLVANIA - PA
						</html:option>
						<html:option value="RI">
						    RHODE ISLAND - RI
						</html:option>		
						<html:option value="SC">
						    SOUTH CAROLINA - SC
						</html:option>		
						<html:option value="SD">
						    SOUTH DAKOTA - SD
						</html:option>		
						<html:option value="TN">
						    TENNESSEE - TN
						</html:option>		
						<html:option value="TX">
						    TEXAS - TX
						</html:option>		
						<html:option value="UT">
						    UTAH - UT
						</html:option>		
						<html:option value="VT">
						    VERMONT - VT
						</html:option>		
						<html:option value="VA">
						    VIRGINIA - VA
						</html:option>		
						<html:option value="WA">
						    WASHINGTON - WA
						</html:option>		
						<html:option value="WV">
						    WEST VIRGINIA - WV
						</html:option>		
						<html:option value="WI">
						    WISCONSIN - WI
						</html:option>
						<html:option value="WY">
						    WYOMING - WY
						</html:option>
					</html:select> 
					<span class="error">
						<html:messages id="message" property="state">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
			</tr>
			<tr>
				<td>
					<b><fmt:message key="label.street.address2"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="addressLine2" size="30" maxlength="60"/>
				</td>
				<td class="zip">
					<b><fmt:message key="label.zip.code"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="postalCode" size="10" maxlength="5"/>
					&nbsp;-&nbsp;<html:text name="updateCustomerProfileForm" property="zip4" size="4" maxlength="4"/>
					<span class="error">
						<html:messages id="message" property="postalCode">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					<b><fmt:message key="label.phone.number"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="phoneNumber" /> 
					<html:hidden name="updateCustomerProfileForm" property="savePhoneNumber" />
					<span class="error">
						<html:messages id="message" property="phoneNumber">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
					<b><fmt:message key="label.phone.number.alternate"/><fmt:message key="label.suffix"/></b>
					<html:text name="updateCustomerProfileForm" property="phoneNumberAlternate"/>
					<html:hidden name="updateCustomerProfileForm" property="savePhoneNumberAlternate" />
					<span class="error">
						<html:messages id="message" property="phoneNumberAlternate">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
					<b><fmt:message key="label.birth.date"/><fmt:message key="label.suffix"/> (dd/MMM/yyyy)</b>
					<html:text	size="10" property="birthDateText" />
					<span class="error">
						<html:messages id="message" property="birthDateText">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
  				<td>
  					<b><fmt:message key="label.loyaltyId"/> <fmt:message key="label.suffix"/></b>
  					<html:text name="updateCustomerProfileForm" property="loyaltyPgmMembershipId"/>
					<span class="error">
						<html:messages id="message" property="loyaltyId">
							<c:out value="${message}"/>
						</html:messages>
					</span>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="3" class="buttons">
					<html:submit property="updateSubmit" value="Update Profile" />
					&nbsp;&nbsp;
					<html:submit property="dvsValidate" value="DVS Validation" />
					&nbsp;&nbsp;
					<html:submit property="updateCancel" value="Cancel" />
					<html:hidden name="updateCustomerProfileForm" property="custId"/>
				</td>
			</tr>
		</TBODY>
	</TABLE>

	</c:when>
	<c:when test="${updateCustomerProfileForm.partnerSiteId == 'MGOUK'}">
	<table width="900" id="updateConsumerForm" class="consumerProfile">
<tr>
  <td>
    <label>First Name</label>
    <html:text name="updateCustomerProfileForm" property="custFirstName" maxlength="20" />
  </td>
  <td>
	<table width="100%" border="0" class="consumerProfile" cellspacing="0" cellpadding="0">
	  <tr>
	    <td width="25%"><label><fmt:message key="label.middle.name"/></label><br>
	    <span style="margin-left: 55px"><fmt:message key="label.optional" /></span></td>
	    <td><html:text name="updateCustomerProfileForm" property="middleName" maxlength="20" />
	  	</td>
	  </tr>
	</table>
  </td>
  <td>
  </td>
</tr>
				<tr>
					<td><label>Last Name</label> <html:text
							name="updateCustomerProfileForm" property="custLastName"
							maxlength="30" /></td>
					<td>						
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="consumerProfile">
						  <tr>
						    <td nowrap="nowrap" width="25%"><label><fmt:message key="label.second.last.name"/></label><br>
						    	<span style="margin-left: 55px"><fmt:message
										key="label.optional" /></span></td>
						    <td><html:text name="updateCustomerProfileForm" property="custSecondLastName" maxlength="30" />
						  	</td>
						  </tr>
						</table>
					</td>
					<td></td>
				</tr>
				<tr>
  <td>
    <label>Address Line 1</label>
    <html:text name="updateCustomerProfileForm" property="addressLine1" maxlength="80" />
  </td>
  <td>
    <label>Town/City</label>
    <html:text name="updateCustomerProfileForm" property="city" maxlength="40" />
  </td>
  <td>
    <label>County</label>
    <html:text name="updateCustomerProfileForm" property="county" maxlength="40" />
  </td>
</tr>
<tr>
  <td>
    <label>Addres Line 2</label>
    <html:text name="updateCustomerProfileForm" property="addressLine2" maxlength="60" />
  </td>
  <td class="zip">
    <label>Postcode</label>
    <html:text name="updateCustomerProfileForm" property="postalMini1" maxlength="4" />
    <html:text name="updateCustomerProfileForm" property="postalMini2" maxlength="8" />
  </td>
  <td>
    <label>Country</label>
    <html:select name="updateCustomerProfileForm" property="state" value="${updateCustomerProfileForm.state}">
    	<html:option value="ENG">England</html:option>
    	<html:option value="WLS">Wales</html:option>
    	<html:option value="SCT">Scotland</html:option>
    	<html:option value="NIR">Northern Ireland</html:option>
    </html:select>
  </td>
</tr>
<tr>
  <td>
    <label>Phone Number</label>
    <html:text name="updateCustomerProfileForm" property="phoneNumber" maxlength="14" />
  </td>
  <td>
    <html:radio property="phoneNumberType" value="{H}" styleClass="check" /><label class="miniLabel">Home</label>
    <html:radio property="phoneNumberType" value="{M}" styleClass="check" /><label class="miniLabel">Mobile</label>
  </td>
  <td>
  </td>
</tr>
<tr>
  <td>
    <label>Alternate Phone Number</label>
    <html:text name="updateCustomerProfileForm" property="phoneNumberAlternate" maxlength="14" />
  </td>
  <td>
    <html:radio property="phoneNumberAlternateType" value="{H}" styleClass="check" /><label class="miniLabel">Home</label>
    <html:radio property="phoneNumberAlternateType" value="{M}" styleClass="check" /><label class="miniLabel">Mobile</label>
  </td>
  <td>
  </td>
</tr>
<tr>
  <td>
    <label>Birth Date: (dd/MMM/yyyy)</label>
    <html:text name="updateCustomerProfileForm" property="birthDateText" maxlength="14" />
  </td>
  <td>
  	  	<b><fmt:message key="label.loyaltyId"/> <fmt:message key="label.suffix"/></b>
  		<html:text name="updateCustomerProfileForm" property="loyaltyPgmMembershipId"/>
		<span class="error">
			<html:messages id="message" property="loyaltyId">
				<c:out value="${message}"/>
			</html:messages>
		</span>				
  </td>
  <td>
  </td>
</tr>
<tr>
	<td colspan="3" align="center" class="buttons"> 
		<html:submit property="updateSubmit" value="Update Profile" />
&nbsp;&nbsp;
<html:submit property="updateCancel" value="Cancel" />
<html:hidden name="updateCustomerProfileForm" property="custId" />
	</td>
</tr>
</table>	

	</c:when>
    <c:when test="${updateCustomerProfileForm.partnerSiteId == 'MGODE'}">
      <table width="900" id="updateConsumerForm" class="consumerProfile">
        <tr>
  		  <td>
    	    <label>First Name</label>
    		<html:text name="updateCustomerProfileForm" property="custFirstName" maxlength="30" />
  		  </td>
  		  <td>
			<table width="100%" border="0" class="consumerProfile" cellspacing="0" cellpadding="0">
			  <tr>
			    <td width="25%"><label><fmt:message key="label.middle.name"/></label><br>
			    <span style="margin-left: 55px"><fmt:message key="label.optional" /></span></td>
			    <td><html:text name="updateCustomerProfileForm" property="middleName" maxlength="20" />
			  	</td>
			  </tr>
			</table>
		  </td>
		</tr>
				<tr>
					<td><label>Last Name</label> <html:text
							name="updateCustomerProfileForm" property="custLastName"
							maxlength="30" /></td>
					<td>
						<table width="100%" border="0" class="consumerProfile" cellspacing="0" cellpadding="0">
						  <tr>
						    <td width="25%" nowrap="nowrap"><label><fmt:message key="label.second.last.name"/></label><br>
						    <span style="margin-left: 55px"><fmt:message key="label.optional"/></span></td>
						    <td><html:text name="updateCustomerProfileForm" property="custSecondLastName" maxlength="30" />
						  	</td>
						  </tr>
						</table>
					</td>
					<td></td>
				</tr>
				<tr>
		  <td>
    	    <label><fmt:message key="address.de"/></label>
    		<html:text name="updateCustomerProfileForm" property="addressLine1" maxlength="80" />
  		  </td>
  		  <td>
    	    <label><fmt:message key="address.de.building.name"/></label>
    		<html:text name="updateCustomerProfileForm" property="addressBuildingName" maxlength="30" />
  		  </td>
		</tr>
		<tr>
  		  <td class="zip">
    	    <label>Postcode</label>
    		<html:text name="updateCustomerProfileForm" property="postalCode" maxlength="5" />
  		  </td>
		  <td>
    	  	<label>Town/City</label>
    		<html:text name="updateCustomerProfileForm" property="city" maxlength="40" />
  		  </td>
  		</tr>
  		<tr>
  		  <td>
    	    <label><fmt:message key="label.country"/></label>
		    <html:text name="updateCustomerProfileForm" property="isoCountryCode" maxlength="3" readonly="true"/>
          </td>
  		  <td></td>
  		  <td></td>  		  
  		</tr>
		<tr>
		  <td>
		    <label>Phone Number</label>
		    <html:text name="updateCustomerProfileForm" property="phoneNumber" maxlength="14" />
		  </td>
		  <td>
		    <html:radio property="phoneNumberType" value="{H}" styleClass="check" /><label class="miniLabel">Home</label>
		    <html:radio property="phoneNumberType" value="{M}" styleClass="check" /><label class="miniLabel">Mobile</label>
		  </td>
		  <td>
		  </td>
		</tr>
		<tr>
		  <td>
		    <label>Alternate Phone Number</label>
		    <html:text name="updateCustomerProfileForm" property="phoneNumberAlternate" maxlength="14" />
		  </td>
		  <td>
		    <html:radio property="phoneNumberAlternateType" value="{H}" styleClass="check" /><label class="miniLabel">Home</label>
		    <html:radio property="phoneNumberAlternateType" value="{M}" styleClass="check" /><label class="miniLabel">Mobile</label>
		  </td>
		  <td>
		  </td>
		</tr>
		<tr>
		  <td>
		    <label>Birth Date: (dd/MMM/yyyy)</label>
		    <html:text name="updateCustomerProfileForm" property="birthDateText" maxlength="14" />
		  </td>
		  <td>
		  </td>
		  <td>
		  </td>
		</tr>
		<tr>
			<td colspan="3" align="center" class="buttons"> 
				<html:submit property="updateSubmit" value="Update Profile" />
		&nbsp;&nbsp;
		<html:submit property="updateCancel" value="Cancel" />
		<html:hidden name="updateCustomerProfileForm" property="custId" />
			</td>
		</tr>
	  </table>	
    </c:when>
</c:choose>
</html:form>
