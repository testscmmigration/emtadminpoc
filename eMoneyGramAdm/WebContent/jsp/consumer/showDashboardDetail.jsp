<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<html:messages id="message" property="headerMessage">
	<c:out value="${message}"/>
</html:messages>

<center>	
<% String dispClass = ""; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<tr>
		<td class="td-label">Consumer Id:</td>
		<td><c:out value="${showDashboardDetailForm.custId}" /></td>
		<logic:equal name="showDashboardDetailForm" property="from" value="dash">
			<td align="right"><a 
				href="javascript: window.history.go(-1);">Go back</a></td>
		</logic:equal>
		<logic:notEqual name="showDashboardDetailForm" property="from" value="dash">
			<td> &nbsp; </td>
		</logic:notEqual>
	</tr>
	<tr>
		<td class="td-label">Logon Id:</td>
		<td><c:out value="${showDashboardDetailForm.custLogonId}" /></td>
		<td align="right"><a 
			href='../../showCustomerProfile.do?custId=<bean:write 
				name="showDashboardDetailForm" property="custId"/>'>Go to Consumer Profile</a></td>
	</tr>
</table>

<br />

<logic:present name="showDashboardDetailForm" property="tranViews">	
<h3><c:out value="${showDashboardDetailForm.actionDesc}" /> 
	<fmt:message key="label.transaction.list" /></h3>
<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>		
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showDashboardDetailForm.tranViews}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="MMM dd yy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>				
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>
								
				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>				
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<logic:iterate id="cmnt" name="cmnts">
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b><bean:write name="cmnt" property="createDate" /> - 
							<bean:write name="cmnt" property="createUserId" /> - 
							<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>				
				
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
		</c:forEach>
	</tbody>
</table>
</logic:present>


<logic:present name="showDashboardDetailForm" property="bankAccounts">	
<h3><c:out value="${showDashboardDetailForm.actionDesc}" /> 
	<fmt:message key="label.bank.accounts" /></h3>
	<% dispClass = "td-small"; %>

	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<thead>
			<tr>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.account.number" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.routing.number" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.bank" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.account.type" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="bankAccount" items="${showDashboardDetailForm.bankAccounts}">
				<tr>
					<html:form action="updateAccountStatus">
					<td nowrap="nowrap" class="<%=dispClass%>">
						<a href='../../viewBankAccountDetail.do?accountId=<c:out value="${bankAccount.accountId}"/>&custId=<c:out value="${showDashboardDetailForm.custId}"/>'>
							<c:out value="${bankAccount.accountNumberMask}" />
						</a> &nbsp;
						<c:if test="${bankAccount.accountTaintText != 'Not Blocked'}">
							<font color="red"><b> &nbsp; (ACCT: <c:out value="${bankAccount.accountTaintText}" />)</b></font>
						</c:if>
					</td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.routingNumber}" />
						<c:if test="${bankAccount.bankAbaTaintText != 'Not Blocked'}">
							<font color="red"><b> &nbsp; (ABA: <c:out value="${bankAccount.bankAbaTaintText}" />)</b></font>
						</c:if>	
					</td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.financialInstitutionName}" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.accountTypeDesc}" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>">
							<html:hidden name="showDashboardDetailForm" property="custId"/>
							<html:hidden name="bankAccount" property="accountId" />
							<html:hidden name="bankAccount" property="accountTypeCode" />
							<html:select name="bankAccount" property="combinedStatusCode">
								<html:optionsCollection name="showDashboardDetailForm" property="accountStatusOptions"/>
							</html:select>
							<logic:equal name="updateAcct" value="Y">
							<input type="submit" class="transbtn2" value="Update Status" />
							</logic:equal>
					</td>
					<logic:present name="bankAccount" property="microDeposit">
					<td nowrap="nowrap" class="<%=dispClass%>">
						<b><fmt:message key="label.md.status" />: </b>
						<c:out value="${bankAccount.microDeposit.status.mdStatus}" />
						(<c:out value="${bankAccount.microDeposit.status.mdStatusDesc}" />)
						<logic:equal name="bankAccount" property="statusCode" value="ACT">
						<logic:equal name="validateMD" value="Y">
						<c:if test="${bankAccount.microDeposit.status.mdStatus == 'ACS'}">
							&nbsp; <a href='../../validateMicroDeposit.do?accountId=<c:out value="${bankAccount.accountId}"/>'>Validate Micro Deposits</a>
						</c:if>
						</logic:equal>
						</logic:equal>
					</td>
					</logic:present>
					<logic:notPresent name="bankAccount" property="microDeposit">
					<td nowrap="nowrap" class="<%=dispClass%>"> &nbsp; </td>
					</logic:notPresent>
					</html:form>
				</tr>
					<logic:notEmpty name="bankAccount" property="cmntList">
					<bean:define id="cmnts" name="bankAccount" property="cmntList" />
				<tr>
					<td colspan="5">
					<table border='0' cellpadding='1' cellspacing='1'>
					<logic:iterate id="cmnt" name="cmnts">
						<tr>
							<td nowrap="nowrap" class="td-small" valign="top">
								<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp; 
								<bean:write name="cmnt" property="createDate" /> - 
								<bean:write name="cmnt" property="createdBy" /> - 
								<bean:write name="cmnt" property="reasonDescription" />
								</b></font></td>
							<td class="td-small">
								<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
						</tr>
					</logic:iterate>
					</table>
					</td>
				</tr>
					</logic:notEmpty>				
								
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
			</c:forEach>
		</tbody>
	</table>
</logic:present>

<logic:present name="showDashboardDetailForm" property="creditCardAccounts">	
<h3><c:out value="${showDashboardDetailForm.actionDesc}" /> 
	<fmt:message key="label.credit.accounts" /></h3>
<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.number" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.expiration.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.issuer" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.type" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="creditCardAccount" items="${showDashboardDetailForm.creditCardAccounts}" varStatus="status">
			<tr>
				<html:form action="updateAccountStatus">
				<td nowrap="nowrap" class="<%=dispClass%>">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${creditCardAccount.accountId}"/>&custId=<c:out value="${showDashboardDetailForm.custId}"/>'>
						<c:out value="${creditCardAccount.accountNumberMask}" />
					</a>
					<c:if test="${creditCardAccount.accountTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (ACCT: <c:out value="${creditCardAccount.accountTaintText}" />)</b></font>
					</c:if>
					<c:if test="${creditCardAccount.creditCardBinTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (BIN: <c:out value="${creditCardAccount.creditCardBinTaintText}" />)</b></font>
					</c:if>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.expirationMonth}/${creditCardAccount.expirationYear}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.issuer}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.creditOrDebitCode}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
						<html:hidden name="showDashboardDetailForm" property="custId"/>
						<html:hidden name="creditCardAccount" property="accountId" />
						<html:hidden name="creditCardAccount" property="accountTypeCode" />
						<html:select name="creditCardAccount" property="combinedStatusCode">
							<html:optionsCollection name="showDashboardDetailForm" property="accountStatusOptions"/>
						</html:select>
						<logic:equal name="updateAcct" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						</logic:equal>
				</td>
				</html:form>
			</tr>

				<logic:notEmpty name="creditCardAccount" property="cmntList">
				<bean:define id="cmnts" name="creditCardAccount" property="cmntList" />
			<tr>
				<td colspan="5">
				<table border='0' cellpadding='1' cellspacing='1'>
				<logic:iterate id="cmnt" name="cmnts">
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp; 
							<bean:write name="cmnt" property="createDate" /> - 
							<bean:write name="cmnt" property="createdBy" /> - 
							<bean:write name="cmnt" property="reasonDescription" />
							</b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>				
				
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
		</c:forEach>
	</tbody>
</table>
</logic:present>

<logic:present name="showDashboardDetailForm" property="debitCardAccounts">	
<h3><c:out value="${showDashboardDetailForm.actionDesc}" /> 
	<fmt:message key="label.credit.accounts" /></h3>
<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.debit.card.number" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.expiration.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.issuer" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.type" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="debitCardAccount" items="${showDashboardDetailForm.debitCardAccounts}" varStatus="status">
			<tr>
				<html:form action="updateAccountStatus">
				<td nowrap="nowrap" class="<%=dispClass%>">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${debitCardAccount.accountId}"/>&custId=<c:out value="${showDashboardDetailForm.custId}"/>'>
						<c:out value="*****${debitCardAccount.accountNumberMask}" />
					</a>
					<c:if test="${debitCardAccount.accountTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (ACCT: <c:out value="${debitCardAccount.accountTaintText}" />)</b></font>
					</c:if>
					<c:if test="${debitCardAccount.creditCardBinTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (BIN: <c:out value="${debitCardAccount.creditCardBinTaintText}" />)</b></font>
					</c:if>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.expirationMonth}/${debitCardAccount.expirationYear}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.issuer}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.creditOrDebitCode}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
						<html:hidden name="showDashboardDetailForm" property="custId"/>
						<html:hidden name="debitCardAccount" property="accountId" />
						<html:hidden name="debitCardAccount" property="accountTypeCode" />
						<html:select name="debitCardAccount" property="combinedStatusCode">
							<html:optionsCollection name="showDashboardDetailForm" property="accountStatusOptions"/>
						</html:select>
						<logic:equal name="updateAcct" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						</logic:equal>
				</td>
				</html:form>
			</tr>

				<logic:notEmpty name="debitCardAccount" property="cmntList">
				<bean:define id="cmnts" name="debitCardAccount" property="cmntList" />
			<tr>
				<td colspan="5">
				<table border='0' cellpadding='1' cellspacing='1'>
				<logic:iterate id="cmnt" name="cmnts">
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp; 
							<bean:write name="cmnt" property="createDate" /> - 
							<bean:write name="cmnt" property="createdBy" /> - 
							<bean:write name="cmnt" property="reasonDescription" />
							</b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>				
				
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small"; 
				} else {
					dispClass = "td-shaded-small";
				}  %>
				
		</c:forEach>
	</tbody>
</table>
</logic:present>

</center>