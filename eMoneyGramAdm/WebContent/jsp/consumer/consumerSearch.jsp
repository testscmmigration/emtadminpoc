<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<CENTER>


<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<h3><fmt:message key="label.consumer.profile.search" /></h3>

<html:form action="/consumerSearch.do" focus="searchFirstName">
<html:hidden property="firstTime" />
<P class="error">
<html:messages id="message" property='nbrFound' message="true">
	<c:out value="${message}"/>
</html:messages>
</P>

<table>
	<tbody>
		<tr>
			<td align="right" class="TD-SMALL"><b><fmt:message key="label.first.name"/>: </b></td>
			<td class="TD-SMALL"><html:text property="searchFirstName" size="25" maxlength="25" /></td>							
			<td> &nbsp; </td>
			<td align="right" class="TD-SMALL"><b><fmt:message key="label.login.id"/>: </b></td>
			<td class="TD-SMALL"><html:text property="searchLoginId" size="30" maxlength="30" /></td>							
		</tr>
		<tr>
			<td align="right" class="TD-SMALL"><b><fmt:message key="label.middle.name"/>: </b></td>
			<td class="TD-SMALL"><html:text property="searchMiddleName" size="25" maxlength="20" /></td>							
			<td> &nbsp; </td>
			<td align="right" class="TD-SMALL"><b><fmt:message key="label.ssn.mask"/>: </b></td>
			<td class="TD-SMALL"><html:text property="searchSSN" size="4" maxlength="4" /> (last 4) 
				<span class="error"><html:errors property="searchSSN"/></span></td>
		</tr>
			<tr>
				<td align="right" class="TD-SMALL"><b><fmt:message key="label.last.name"/>: </b></td>
				<td class="TD-SMALL"><html:text property="searchLastName" size="25" maxlength="25" /></td>
				<td></td>
				<td nowrap="nowrap" align="right" class="TD-SMALL"><fmt:message key="label.sort.report.by"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap">
				<html:select name="consumerSearchForm" property="sortBy" size="1">
					<html:option value="custId"><fmt:message key="label.consumer.id"/></html:option>												
					<html:option value="custLogonId"><fmt:message key="label.login.id"/></html:option>												
					<html:option value="custStatCode"><fmt:message key="label.status"/></html:option>												
					<html:option value="custSubStatCode"><fmt:message key="label.sub.status"/></html:option>												
					<html:option value="custFrstName"><fmt:message key="label.first.name"/></html:option>												
					<html:option value="custLastName"><fmt:message key="label.last.name"/></html:option>												
					<html:option value="custPhoneNbr"><fmt:message key="label.phone"/></html:option>												
					<html:option value="custSSNMask"><fmt:message key="label.ssn.mask"/></html:option>												
					<html:option value="custBrthDate"><fmt:message key="label.cust.birth.date"/></html:option>												
					<html:option value="custCreateDate"><fmt:message key="label.create.date"/></html:option>												
					<html:option value="custCreateIpAddrId"><fmt:message key="label.create.ipaddress"/></html:option>												
				</html:select>
				</td>
			</tr>

			<tr>
				<td align="right" class="TD-SMALL"><b><fmt:message key="label.profile.create.date"/>: </b></td>
				<td class="TD-SMALL"><html:text property="searchCreateDate" size="11" maxlength="11" />  (DD/MMM/YYYY)
				<span class="error"><html:errors property="searchCreateDate"/></span></td>
				<td></td>
				<td nowrap="nowrap" align="right" class="TD-SMALL"></td>
				<td nowrap="nowrap"></td>
			</tr>
			<tr>
				<td align="right" class="TD-SMALL"></td>
				<td class="TD-SMALL"></td>
				<td></td>
				<td nowrap="nowrap" align="right" class="TD-SMALL"></td>
				<td nowrap="nowrap"></td>
			</tr>
		</tbody>
</table>


<div id="formButtons">

	<html:submit property="submitSearch" value="Search"  
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />

</div>
	
<div id="processing" style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >

<logic:present name="profileList">

	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.consumer.profile.search"/> - 
			
			<html:messages id="message" property='nbrFound' message="true">
				<c:out value="${message}"/>
			</html:messages>
	     
		</legend>
	
	
	<%String dispClass = "TD-SMALL";%>
	<table>
		<tbody>
			<tr><th colspan="13" align="right"><a href="../../ExcelReport.emt?id=profileList" target="excel">
				<fmt:message key="msg.view.or.download.excel" /></a>
				</th></tr>
			<tr>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'> &nbsp; </th>			
				<th nowrap='nowrap' class='TD-HEADER-SMALL' colspan='3'><fmt:message key="label.consumer.id"/></th>		
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.phone"/></th>		
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date"/></th>		
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.date"/></th>		
				<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.ipaddress"/></th>					
			</tr>
			<logic:iterate id="profile" name="profileList">
			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>'><font color="red"><b>
					<bean:write name="profile" property="taintedText" /></b></font></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custId" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
				    <a href='../../showCustomerProfile.do?custId=<bean:write 
				    name="profile" property="custId" />&basicFlag=N'>Detail</a></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
				    <a href='../../showCustomerProfile.do?custId=<bean:write 
				    name="profile" property="custId" />&basicFlag=Y'>Basic</a></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custLogonId" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custStatDesc" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custSubStatDesc" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custFrstName" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custLastName" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custPhoneNbr" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custSSNMask" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custBrthDate" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custCreateDate" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="profile" property="custCreateIpAddrId" /></td>
					
			</tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>	
			</logic:iterate>
		</tbody>		      
	</table>
	</fieldset>
</logic:present>

</html:form>

