<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/showUser"  focus="close">

<logic:present name="invalidUser">
<br /> <br /><span class="error">The user <bean:write name="showUserForm" property="userId"/> 
is not a true online user or an user who is no longer exist in this system.</span>
<br /><br />
<html:button property="close" value="Close Window" onclick="window.close()"/>
</logic:present>

<logic:notPresent name="invalidUser">
<table border="0">
	<tbody>
		<tr>
			<td nowrap="nowrap" class="TD-LABEL">User ID: </td>
			<td><bean:write name="showUserForm" property="userId"/></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="TD-LABEL">First Name: </td>
			<td nowrap="nowrap" >
				<bean:write name="showUserForm" property="firstName"/></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="TD-LABEL">Last Name: </td>
			<td nowrap="nowrap" >
				<bean:write name="showUserForm"  property="lastName"/></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="TD-LABEL">Application Role: </td>
			<td>
				<bean:write name="showUserForm"  property="user.roleDescriptions" /></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="TD-LABEL">Email Address: </td>
			<td class="TD-LABEL">
			   <bean:write name="showUserForm"  property="emailAddress"/></td>
		</tr>
		<tr><td colspan="2"> &nbsp; </td></tr>
		<tr><td colspan="2" align="center">
			<html:button property="close" value="Close Window" onclick="window.close()"/></td>
		</tr>
	</tbody>
</table>
</logic:notPresent>

</html:form>
