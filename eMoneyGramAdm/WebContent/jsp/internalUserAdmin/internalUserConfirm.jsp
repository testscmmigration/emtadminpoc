<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>


<bean:define id="internalUserAdminForm" name="internalUserAdminForm" scope="request"/> 

<TABLE border="0">
	<TBODY>
		<TR valign="top">
			<TD nowrap="nowrap" class="TD-LABEL" width="25%">User ID:</TD>
			<TD nowrap="nowrap" width="25%"><bean:write name="internalUserAdminForm" property="userId"/></TD>
		</TR>
		<TR valign="top">
			<TD nowrap="nowrap" class="TD-LABEL">First Name:</TD>
			<TD><bean:write name="internalUserAdminForm" property="firstName"/></TD>
		</TR>
		<TR>
			<TD nowrap="nowrap" class="TD-LABEL">Last Name:</TD>
			<TD><bean:write name="internalUserAdminForm" property="lastName"/></TD>
		</TR>
		<TR valign="top">
			<TD nowrap="nowrap" class="TD-LABEL">Application Role:</TD>
			<TD><bean:write name="internalUserAdminForm" property="user.roleDescriptions"/><br/>
			<I><a href=../viewRoles.do' onClick="this.href='javascript:showRoleDetail(\'viewRoles.do\')'">View Application Roles</a><I>
		</TR>

		</TBODY>
</TABLE>
<br/>
<TABLE border="0">
	<TBODY>
		<TR>	
			<TD><html:form action="/internalUserAdmin" method="post">
				<html:hidden name="internalUserAdminForm" property="userId"/>
				<html:hidden name="internalUserAdminForm" property="guid"/>
				<INPUT type="submit" name="addNew" value="Add New User">
				<INPUT type="submit" name="view" value="Edit This User">
				<html:link action="/internalUserSearch">User Search</html:link>
			</html:form>
			</TD>
		</TR>
	</TBODY>
</TABLE>
