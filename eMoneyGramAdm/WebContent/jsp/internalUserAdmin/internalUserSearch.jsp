<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>
<html:form action="/internalUserSearch" focus="userID">
	<html:hidden property="internal" value="true"/>
<TABLE border="0">
				<TBODY>
					<TR>
						<TD nowrap="nowrap" class="TD-LABEL">User ID:</TD>
						<TD nowrap="nowrap"><html:text property="userID" size="25" maxlength="25"/><span class="error"> <html:errors property="userID"/></span></TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" class="TD-LABEL">First Name:</TD>
						<TD nowrap="nowrap"><html:text property="firstName" size="25" maxlength="25"/><span class="error"> <html:errors property="firstName"/></span></TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" class="TD-LABEL">Last Name:</TD>
						<TD nowrap="nowrap"><html:text property="lastName" size="25" maxlength="25"/><span class="error"> <html:errors property="lastName"/></span></TD>
					</TR>
					<TR>
						<TD nowrap="nowrap" class="TD-LABEL">Role:</TD>
						<TD>
							<html:select property="roleID" size="1">
								<bean:define id="internalRoles" name="internalUserSearchForm" property="internalRoles" scope="request" />
							    <html:option value="*" >All</html:option>
							    <html:options collection="internalRoles" labelProperty="shortNamePlusProgramPrefix" property="id"/>
		                	</html:select><br>
		                </TD>
					</TR>
					</TBODY>
			</TABLE>
<BR/>
	
	<TABLE border="0">
		<TBODY>
			<TR>
				<TD>
					<div id="formButtons">
						<INPUT type="submit" name="search" value="Search" onclick="formButtons.style.display='none';processing.style.visibility='visible';">
					</div >
					<div id="processing"  style="visibility: hidden">
						<table>
							<tr>
								<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
							</tr>
						</table>
					</div >					
				</TD>
				<TD>
				</TD>
			</TR>
			
		</TBODY>
	</TABLE>
	<BR/>
	<CENTER>

	<c:if test="${internalUserSearchForm.userCount > 0}">
	<%String dispClass = "TD-SMALL";%>
	<P>
		<table border='0' cellpadding='2' cellspacing='1' width='100%'>
			<TR>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>User ID</TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>First Name</TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>Last Name</TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>Role</TD>		
			</TR>
			
			<c:forEach var="user" items="${internalUserSearchForm.users}">
				
			<TR>
				<td nowrap='nowrap' class='<%=dispClass%>'/>
					<c:if test="${auth.editAdminUserProfile == true}">
						<A HREF='../../internalUserAdmin.do?view=all&userId=<c:out value="${user.UID}"/>'>
					    	<c:out value="${user.UID}"/>
				    	</A>
					</c:if>			
							
					<c:if test="${auth.editAdminUserProfile != true}">
				    	<c:out value="${user.UID}"/>
					</c:if>					
		    	</td>
	
				<td nowrap='nowrap' class='<%=dispClass%>'/><c:out value="${user.firstName}"/></TD>
				<td nowrap='nowrap' class='<%=dispClass%>'/><c:out value="${user.lastName}"/></TD>
				<td nowrap='nowrap' class='<%=dispClass%>'/><c:out value="${user.roleDescriptions}"/></TD>
			</TR>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>	
		    </c:forEach>
		</table>
	</c:if>	
</CENTER>

</html:form>
