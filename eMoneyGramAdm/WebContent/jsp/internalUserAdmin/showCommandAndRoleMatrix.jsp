<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h3>Role Permission Matrix</h3>

<html:form action="/showCommandAndRoleMatrix">

	<%
	String[] dispClasses = {"TD-SMALL", "TD-SHADED-SMALL"};
	String dispClass = null;%>
	<label class="partnerFilterLabel" for="program">Partner Filter</label><html:select property="program" onchange="this.form.submit()">
		<html:options name="showCommandAndRoleMatrixForm" property="programs" />
	</html:select>
	<table border='0' cellpadding='1' cellspacing='1' width='100%'>
		<TR class="permMatrixHead">
			<logic:notEqual name="showCommandAndRoleMatrixForm" property="supress" value="Y">
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Command Name</TD>
			</logic:notEqual>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Description</TD>
			<logic:iterate id="roleId" name="showCommandAndRoleMatrixForm" property="roleName" indexId="idx1">
    			<td nowrap='nowrap' class='TD-HEADER-SMALL'>
    				<bean:write name='showCommandAndRoleMatrixForm' property='<%= "roleName[" + idx1 + "]" %>' filter="false"/></td>
   			</logic:iterate>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Menu<br />Name</TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Level</TD>
		</tr>
		<tbody class="permMatrixBody">	
		<logic:iterate id="cmd" name="cmdList" indexId="rowNum">
			<%  dispClass = dispClasses[rowNum.intValue() % 2]; %>	
		<tr>
			<logic:notEqual name="showCommandAndRoleMatrixForm" property="supress" value="Y">
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="cmd" property="command" /></td>
			</logic:notEqual>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="cmd" property="displayName" /></td>
			<logic:iterate id="permission" name="cmd" property="rolePermission" indexId="idx2">
				<td nowrap='nowrap' align='center' class='<%=dispClass%>'><bean:write name='cmd' property='<%= "rolePermission[" + idx2 + "]" %>' /></td>
			</logic:iterate>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="cmd" property="menuGroupName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="cmd" property="menuLevel" /></td>
		</tr>
		</logic:iterate>
		</tbody>

	</table>

</html:form>
