<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>
<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>
</center>

<html:form action="/postAdminMessage">

<h3><fmt:message key="label.post.new.admin.messages"/></h3>
<br />
<center>
<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td	nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap"><font size="2" color="red"><b>
			<fmt:message key="label.multi.select.instruction"/></b></font></td>
		<td nowrap="nowrap" class="td-small" colspan="2"> &nbsp; </td>
		<td	nowrap="nowrap"><font size="2" color="red"><b>
			<fmt:message key="label.multi.select.instruction"/></b></font></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" valign="top">
			<fmt:message key="label.send.to.roles"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="postAdminMessageForm" property="roleList" multiple="true" size="8">
				<html:options collection="sysRoles" property="value" labelProperty="label"/>
			</html:select>
			<span class="error"><html:errors property='roleList'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small" valign="top">
			<fmt:message key="label.send.to.users"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="postAdminMessageForm" property="userList" multiple="true" size="8">
				<html:options collection="sysUsers" property="value" labelProperty="label"/>
			</html:select>
			<span class="error"><html:errors property='userList'/></span></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" colspan='5'> &nbsp; </td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.message.type"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="postAdminMessageForm" property="typeCode" size="1">
				<html:option value=""><fmt:message key="label.select.a.message.type"/></html:option>
				<html:options collection="msgTypes" property="value" labelProperty="label"/>
			</html:select>
			<span class="error"><html:errors property='typeCode'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.message.priority"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="postAdminMessageForm" property="prtyCode" size="1">
				<html:option value="">
					<fmt:message key="label.select.a.message.priority"/></html:option>
				<html:options collection="priorities" property="value" labelProperty="label"/>
			</html:select>
			<span class="error"><html:errors property='prtyCode'/></span></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" colspan='5'> &nbsp; </td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.post.begin.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="postAdminMessageForm" property="beginDate" 
				onchange="setOtherRangeValue(this, postAdminMessageForm.beginDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('postAdminMessageForm.beginDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.hour"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="postAdminMessageForm" property="beginHour" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.minute"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="postAdminMessageForm" property="beginMinute" size="1">
					<html:options collection="minutes" property="value" labelProperty="label"/>
				</html:select>
			<span class="error"><html:errors property='beginDate'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.post.end.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="postAdminMessageForm" property="endDate" 
				onchange="setOtherRangeValue(this, postAdminMessageForm.endDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('postAdminMessageForm.endDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.hour"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="postAdminMessageForm" property="endHour" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.minute"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="postAdminMessageForm" property="endMinute" size="1">
					<html:options collection="minutes" property="value" labelProperty="label"/>
				</html:select>
			<span class="error"><html:errors property='endDate'/></span></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" colspan='5'> &nbsp; </td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" valign="top">
			<fmt:message key="label.message.text"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td nowrap='nowrap' class='td-small' colspan='4'>
			<html:textarea name="postAdminMessageForm" property="msgText" rows="4" cols="80" 
				onkeypress="textCounter(this, 255);" />
			<span class="error"><html:errors property='msgText'/></span></td>			
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small" colspan='5'> &nbsp; </td>
	</tr>
	<tr>
	<html:hidden property="entryState"  />
	<logic:equal name="postAdminMessageForm" property="entryState" value="post">
		<td nowrap='nowrap' class='td-small' colspan='5' align='center'>
			<html:submit property="submitPost" value="Post this Message" /></td>
	</logic:equal>
	<logic:equal name="postAdminMessageForm" property="entryState" value="reset">
		<td nowrap='nowrap' class='td-small' colspan='5' align='center'>
			<html:submit property="submitReset" value="Create Another Message" /></td>
	</logic:equal>
	</tr>
</table>

</center>
</html:form>
