<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<html:form action="/viewMessages">
<html:hidden property="saveAction" />
<html:hidden property="saveAction1" />
<html:hidden property="currentUser" />

</center>
<h3><fmt:message key="label.view.admin.messages"/></h3>
<br />

<center>
<table>
	<tr>
		<td	nowrap="nowrap" class="td-small">
			<b><a href="../../viewMessages.do?submitAllMsgs=Y">
				<fmt:message key="label.get.all.my.messages"/></a></b>
				&nbsp; &nbsp; &nbsp;
			<b><a href="../../viewMessages.do?submitUnread=Y">
				<fmt:message key="label.get.only.my.unread.messages"/></a></b>
				&nbsp; &nbsp; &nbsp;
			<b><a href="../../viewMessages.do?submitPoster=Y"">
				<fmt:message key="label.get.my.posted.messages"/></a></b>
			<logic:equal name="allowPostMsg" value="Y">
					&nbsp; &nbsp; &nbsp;
				<b><a href="../../postAdminMessage.do">
					<fmt:message key="label.post.a.new.message"/></a></b>
			</logic:equal>
		</td>
	</tr>
</table>

<br />

<fieldset>
	<legend class="td-label"><font color='red'>
		<fmt:message key="label.admin.message.search"/></font></legend>

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.message.type"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="viewMessagesForm" property="searchTypeCode" size="1">
				<html:option value="All"><fmt:message key="option.all"/></html:option>
				<html:options collection="msgTypes" property="value" labelProperty="label"/>
			</html:select></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.message.priority"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="viewMessagesForm" property="searchPrtyCode" size="1">
				<html:option value="All"><fmt:message key="option.all"/></html:option>
				<html:options collection="priorities" property="value" labelProperty="label"/>
			</html:select></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-small" colspan="2">
			<html:radio name="viewMessagesForm" property="useDateSearch" 
				value="Y"><b><fmt:message key="label.use.dates.for.search"/></b></html:radio>
			&nbsp; &nbsp; &nbsp;
			<html:radio name="viewMessagesForm" property="useDateSearch" 
				value="N"><b><fmt:message key="label.not.use.dates.for.search"/></b></html:radio>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.message.text"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:text name="viewMessagesForm" property="searchMsgText" size="40" maxlength="40" /></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.post.begin.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="viewMessagesForm" property="beginDate" 
				onchange="setOtherRangeValue(this, viewMessagesForm.beginDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('viewMessagesForm.beginDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.hour"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="viewMessagesForm" property="beginHour" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.minute"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="viewMessagesForm" property="beginMinute" size="1">
					<html:options collection="minutes" property="value" labelProperty="label"/>
				</html:select>
			<span class="error"><html:errors property='beginDate'/></span></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.search.post.end.time"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:text size="10" name="viewMessagesForm" property="endDate" 
				onchange="setOtherRangeValue(this, viewMessagesForm.endDate);" />
				<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('viewMessagesForm.endDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.hour"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="viewMessagesForm" property="endHour" size="1">
					<html:options collection="hours" property="value" labelProperty="label"/>
				</html:select>
				&nbsp; <span class="td-label-small">
				<fmt:message key="label.minute"/><fmt:message key="label.suffix"/></span> &nbsp; 
				<html:select name="viewMessagesForm" property="endMinute" size="1">
					<html:options collection="minutes" property="value" labelProperty="label"/>
				</html:select>
			<span class="error"><html:errors property='endDate'/></span></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.order.by"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="viewMessagesForm" property="orderBy" size="1">
				<html:option value="adminMsgId">
					<fmt:message key="label.msg.id"/></html:option>
				<html:option value="msgBegDate">
					<fmt:message key="label.post.begin.time"/></html:option>
				<html:option value="msgEndDate">
					<fmt:message key="label.post.end.time"/></html:option>
				<html:option value="adminMsgTypeDesc">
					<fmt:message key="label.message.type"/></html:option>
				<html:option value="msgPrtyCode">
					<fmt:message key="label.priority"/></html:option>
				<html:option value="createUserid">
					<fmt:message key="label.poster"/></html:option>
				<html:option value="createDate">
					<fmt:message key="label.message.post.time"/></html:option>
			</html:select></td>
		<td nowrap="nowrap" class="td-small"> &nbsp; </td>
		<td	nowrap="nowrap" class="td-label-small">
			<fmt:message key="label.order.seq"/><fmt:message key="label.suffix"/> &nbsp; </td>
		<td	nowrap="nowrap" class="td-small">
			<html:select name="viewMessagesForm" property="orderSeq" size="1">
				<html:option value="A"><fmt:message key="option.ascending"/></html:option>
				<html:option value="D"><fmt:message key="option.descending"/></html:option>
			</html:select></td>
	</tr>
	<tr>
		<td	nowrap="nowrap" class="td-small" colspan="5" align="center">
			<html:submit property="submitSearch" value="Search Messages" /> &nbsp; &nbsp; 
		</td>
	</tr>
	<tr>
</table>
</fieldset>

<logic:notEmpty name="msgList">
<br />
<table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
		<td nowrap='nowrap' class='TD-SMALL' colspan='6'>
		<logic:equal name="viewMessagesForm" property="saveAction" value="list">
			<input type='submit' name='submitMarkRead' value='Mark Selected as Read' /> 
			&nbsp; &nbsp;
			<input type='submit' name='submitMarkUnread' value='Mark Selected as Unread' /> 
			&nbsp; &nbsp;
		</logic:equal>
		<logic:equal name="viewMessagesForm" property="saveAction" value="search">
		<logic:equal name="purgeMsgs" value="Y">
			<input type='submit' name='submitPurge' value='Purge Selected Messages' /> 
		</logic:equal>
		</logic:equal>
		<td  nowrap='nowrap' class='TD-SMALL' colspan='3' align='right'>
			<a href="../../ExcelReport.emt?id=msgList" target="excel">
			<fmt:message key="msg.view.or.download.excel" /></a></td>
	</tr>
	<tr>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'> &nbsp; </td>
		<logic:equal name="viewMessagesForm" property="saveAction" value="list">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.read.flag"/></td>
		</logic:equal>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.msg.id"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.post.begin.time"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.post.end.time"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.message.type"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.priority"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.poster"/></td>
		<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.message.post.time"/></td>
	</tr>

	<% String dispClass = "td-small"; %>

	<logic:iterate id="msg" name="msgList">

	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
		<logic:equal name="viewMessagesForm" property="saveAction" value="list">
			<html:multibox property="selMsgs">
				<bean:write name="msg" property="adminMsgId"/>
			</html:multibox>
		</logic:equal>
		<logic:equal name="viewMessagesForm" property="saveAction" value="search">
		<logic:equal name="purgeMsgs" value="Y">
			<html:multibox property="selMsgs">
				<bean:write name="msg" property="adminMsgId"/>
			</html:multibox>
		</logic:equal>
		</logic:equal>
		</td>
		<logic:equal name="viewMessagesForm" property="saveAction" value="list">
		<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
			<bean:write name="msg" property="readFlag" /></td>
		</logic:equal>
		<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
			<bean:write name="msg" property="adminMsgId" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="msg" property="msgBegDateText" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="msg" property="msgThruDateText" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="msg" property="adminMsgTypeDesc" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
			<bean:write name="msg" property="msgPrtyCode" /></td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="msg" property="createUserid" />
			<logic:equal name="allowViewRecipnt" value="Y">
			<c:if test="${msg.createUserid == viewMessagesForm.currentUser}">
				<a href="../../viewMessageRecipients.do?msgId=<c:out 
					value="${msg.adminMsgId}" />&saveAction=<c:out
					value="${viewMessagesForm.saveAction}" />&saveAction1=<c:out
					value="${viewMessagesForm.saveAction1}" /> ">See Recipients</a>			
			</c:if>
			</logic:equal>
		</td>
		<td nowrap='nowrap' class='<%=dispClass%>'>
			<bean:write name="msg" property="createDateText" /></td>
	</tr>
	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>'> &nbsp; </td>
		<td class='<%=dispClass%>' colspan='8'> 
			<bean:write name="msg" property="msgText" filter="false" /></td>
	</tr>

	<% dispClass = dispClass.equals("td-shaded-small") ? "td-small" : "td-shaded-small"; %>

	</logic:iterate>
</table>
</logic:notEmpty>

</html:form>

</center>
