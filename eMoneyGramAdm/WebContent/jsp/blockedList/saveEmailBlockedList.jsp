<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</center>

<html:form action="/saveEmailBlockedList.do">

<h3><fmt:message key="label.blockedlist.email.title" /></h3>
	<table border="0">
		<tbody>
	        <tr>
	            <td width="200"><fmt:message key="label.email.address" /> 
	            <td><bean:write name="blockedEmail" property="clearText" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.email.type" /> 
	            <td><bean:write name="blockedEmail" property="elecAddrTypeCode" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.block.reason" /></td>
	            <td><bean:write name="blockedEmail" property="reasonDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.block.status" /></td>
	            <td><bean:write name="blockedEmail" property="statusDesc" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.block.comment" /></td>
	            <td><bean:write name="blockedEmail" property="comment" /></td>
	        </tr>
	        <tr>
	            <td><fmt:message key="label.fraud.risk.level" /></td>
	            <td><bean:write name="blockedEmail" property="fraudRiskLevelCode" /></td>
	        </tr>
	        <tr>
	            <td colspan="2">&nbsp;</td>
			</tr>
	        
	        <tr>
	            <td>&nbsp;</td>
			    <td>
					<div id="formButtonsAdd">
					<html:submit property="submitDBSave" value=" Save "
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					<html:submit property="submitDBCancel" value="Cancel"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
					</div>
			    </td>
			</tr>
		</tbody>
	</table>

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>
	
	<h3><fmt:message key="label.blockedlist.email.existing.list" /></h3>	
	<logic:empty name="blockedEmails">
		<fmt:message key="label.blockedlist.email.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedEmails">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.email.address" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.email.type" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.comment" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.block.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.fraud.risk.level" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.create.user" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'>
					<fmt:message key="label.last.update.user" /></TD>
				</tr>
								

				<logic:iterate id="blockedBean" name="blockedEmails">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="elecAddrTypeCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.email.consumer.list" /></h3>	

	<logic:empty name="consumerProfiles">
		<fmt:message key="label.blockedlist.email.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="consumerProfiles">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				</tr>

				<logic:iterate id="consumerProfile" name="consumerProfiles">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSSNMask"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
