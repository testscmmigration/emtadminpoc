<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<html:form action="/showBankBlockedList.do" focus="bankAba">
<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<h3><fmt:message key="label.blockedlist.bank.title" /></h3>
<br>
	<table border="0" width="100%">
	<tr>
		<td colspan="3">
			You may enter just an ABA or an ABA and Bank Account together or just an Account Number Mask.
			<br><br>
		</td>
	</tr>
	</table>
	<table border="0" width="100%">
	<tr>

		<td width="200" valign="top"><fmt:message key="label.blockedlist.bank.ababank" /></td>
		<td>
			<html:text name="showBankBlockedListForm" property="bankAba" size="9" maxlength="9" />
			<html:text name="showBankBlockedListForm" property="bankFull" size="16" />

			<html:submit property="submitAcctSearch" value="Search" 
				onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
		</td>
	</tr>
	<tr>
		<td width="180" valign="top"><fmt:message key="label.blockedlist.bank.masknumber" /></td>
		<td>
			<html:text name="showBankBlockedListForm" property="bankMask" size="4" maxlength="4" />
			<html:submit property="submitMaskSearch" value="Search" 
				onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitShowAll" value="Show All"
					 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				<logic:equal name="auth" property="addBankBlockedList" value="true">
					<html:submit property="submitAdd" value="Add to Blocked List"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</div>
		</td>
	</tr>
	</table>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING">Your request is in process ....</span></div>


	
	<logic:notEmpty name="accts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.abanumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.acctnumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.modifiedby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.action" /> </TD>
				</tr>


				<logic:iterate id="blockedBean" name="accts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="abaNumber"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="createUserId" value="">
            				<bean:write name="blockedBean" property="createUserId" />
            			</logic:equal>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="updateUserId" value="">
            				<bean:write name="blockedBean" property="updateUserId" />
            			</logic:equal>
					</td>
					<% String edit = "Edit"; %>
					<td nowrap='nowrap' class='<%=dispClass%>'>

             		<logic:equal name="blockedBean" property="clearText" value="">
						<a href='../../addBankBlockedList.do?aba=<bean:write filter="false" name="blockedBean" property="abaNumber"/>
							&action=<%=edit%>'>
							<%=edit%>
						</a>
            		</logic:equal>

             		<logic:notEqual name="blockedBean" property="clearText" value="">
						<a href='../../addBankBlockedList.do?acct=
							<bean:write filter="false" name="blockedBean" property="clearText"/>
							&aba=<bean:write filter="false" name="blockedBean" property="abaNumber"/>
							&action=<%=edit%>'>
							<%=edit%>
						</a>
            		</logic:notEqual>
					
					</td>
						
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
		</table>
	</logic:notEmpty>
</html:form>