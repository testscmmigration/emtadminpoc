<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

<script language="javascript">
	function setVisibility(id, visible) { 
    	var o = getElemById(o); 
	    if (o) { 
    	    if (typeof o.style != 'undefined') { 
        	    o.style.visibility = (visible ? 'visible' : 'hidden'); 
            	return (o.style.visibility == (visible ? 'visible' : 'hidden')); 
	        } else { 
    	        o.visibility = (visible ? 'show' : 'hide'); 
        	    return (o.visibility == (visible ? 'show' : 'hide')); 
	        } 
    	} 
	    return false; 
} 
</script>

</center>

<html:form action="/addEmailBlockedList.do" focus="emailAddress">

<h3><fmt:message key="label.blockedlist.email.title" /></h3>

	<table border="0" width="100%">

	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>

	<tr>
		<td width="140" valign="top"><fmt:message key="label.blockedlist.email.email" /></td>
		<td>
			<html:text name="addEmailBlockedListForm" property="emailAddress" size="20" />
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	
	<tr>
		<td width="140" valign="top"><fmt:message key="label.blockedlist.email.domain" /></td>
		<td>
			<html:text name="addEmailBlockedListForm" property="emailDomain" size="20" />
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td width="140"><fmt:message key="label.fraud.risk.level" /></td>
		<td colspan="2">
			<html:text name="addEmailBlockedListForm" property="riskLevel" size="3" maxlength="3" />
		</td>
	</tr>
	<tr>
		<td width="140"><fmt:message key="label.block.status" /></td>
		<td colspan="2">
			<html:select name="addEmailBlockedListForm" property="blockingAction" size="1">
				<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
				<html:options collection="blockingActions" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
			</html:select>		
		</td>
	</tr>
	<tr>
		<td width="140"><fmt:message key="label.block.reason" /></td>
		<td colspan="2">
			<html:select name="addEmailBlockedListForm" property="blockingReason" size="1">
				<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
				<html:options collection="blockingReasons" property="reasonCode" labelProperty="reasonDesc" />
			</html:select>		
		</td>
	</tr>
	<tr>
		<td width="140" valign="top"><fmt:message key="label.block.comment" /></td>
		<td colspan="2">
			<html:textarea name="addEmailBlockedListForm" property="blockingComment"  rows="4" cols="40" 
					onkeypress="textCounter(this, 128);" />			
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td width="140" valign="top">&nbsp;</td>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitProcess" value=" Process " 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
				<html:submit property="submitCancel" value="Cancel" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>			
			</div>
		</td>
	</tr>
	
	</table>
	

	<div id="processing" style="visibility:hidden" >
		<span class="PROCESSING"><fmt:message key="message.in.process" />
		</span>
	</div>

</html:form>
