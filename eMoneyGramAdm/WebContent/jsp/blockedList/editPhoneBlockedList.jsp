<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<%	String focus="phoneNumber"; %>
<c:if test="${editPhoneBlockedListForm.action == 'edit'}">
	<%focus = "riskLvlCode";%>
</c:if>


<html:form action="/editPhoneBlockedList" focus="<%=focus%>">
<html:hidden name="editPhoneBlockedListForm" property="action" />
<html:hidden name="editPhoneBlockedListForm" property="saveAction" />

<h3><fmt:message key="label.maintain.blocked.phone.title" /></h3>
<br /><br />

	<table border="0">
		<tbody>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.phone.number" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
	            <logic:equal name="editPhoneBlockedListForm" property="action" value="edit">
	            	<td nowrap="nowrap"><bean:write name="editPhoneBlockedListForm" property="phoneNumber" /></td>
	            	<html:hidden name="editPhoneBlockedListForm" property="phoneNumber" />
	            </logic:equal>
	            <logic:notEqual name="editPhoneBlockedListForm" property="action" value="edit">
	            	<td nowrap="nowrap"><html:text property='phoneNumber' size='14'  maxlength="14"  /></td>
	            </logic:notEqual>
	            <td nowrap="nowrap"><span class="error">
					<html:errors property="phoneNumber"/></span></td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.block.status" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
				<td nowrap="nowrap"><html:select name="editPhoneBlockedListForm" property="blkdStatCode" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
					<html:options collection="blockingStatuses" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
					</html:select></td>
	            <td nowrap="nowrap"><span class="error">
					<html:errors property="blkdStatCode"/></span></td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.block.reason" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
				<td nowrap="nowrap"><html:select name="editPhoneBlockedListForm" property="blkdReasCode" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>												
					<html:options collection="blockingReasons" property="reasonCode" labelProperty="reasonDesc" />
					</html:select></td>
	            <td nowrap="nowrap"><span class="error">
					<html:errors property="blkdReasCode"/></span></td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.fraud.risk.level" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap"><html:text name="editPhoneBlockedListForm" property="riskLvlCode" size="3"  maxlength="3"  /> &nbsp;
            		<fmt:message key="msg.fraud.risk.level" /></td>
	            <td nowrap="nowrap"><span class="error">
					<html:errors property="riskLvlCode"/></span></td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label" valign="top"><fmt:message key="label.block.comment" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
	            <td><html:textarea name="editPhoneBlockedListForm" property="blkdCmntText"  rows="4" cols="40" 
					onkeypress="textCounter(this, 128);" /></td>
	            <td nowrap="nowrap"><span class="error">
					<html:errors property="blkdCmntText"/></span></td>
	        </tr>
	        <logic:notEqual name="editPhoneBlockedListForm" property="statUpdateDate" value="">
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.status.update.date" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap"><bean:write name="editPhoneBlockedListForm" property="statUpdateDate" />
	            	<html:hidden name="editPhoneBlockedListForm" property="statUpdateDate" /></td>
	            <td nowrap="nowrap"> &nbsp; </td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.create.date" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap"><bean:write name="editPhoneBlockedListForm" property="createDate" />
	            	<html:hidden name="editPhoneBlockedListForm" property="createDate" /></td>
	            <td nowrap="nowrap"> &nbsp; </td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.create.user" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap">
            		<logic:notEqual name="editPhoneBlockedListForm" property="createUser" value="">
						<a href='../../showUser.do?userId=<c:out 
						value="${editPhoneBlockedListForm.createUser}"/>' 
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
						value="${editPhoneBlockedListForm.createUser}"/>\')'">
						<b><bean:write name="editPhoneBlockedListForm" property="createUser" /></b></a>
					</logic:notEqual>
             		<logic:equal name="editPhoneBlockedListForm" property="createUser" value="">
            			<bean:write name="editPhoneBlockedListForm" property="createUser" />
            		</logic:equal>
	            	<html:hidden name="editPhoneBlockedListForm" property="createUser" /></td>
	            <td nowrap="nowrap"> &nbsp; </td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.last.update.date" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap"><bean:write name="editPhoneBlockedListForm" property="lastUpdateDate" />
	            	<html:hidden name="editPhoneBlockedListForm" property="lastUpdateDate" /></td>
	            <td nowrap="nowrap"> &nbsp; </td>
	        </tr>
	        <tr>
	            <td nowrap="nowrap" class="td-label"><fmt:message key="label.last.update.user" />
	            	<fmt:message key="label.suffix" /> &nbsp; </td>
            	<td nowrap="nowrap">
	               	<logic:notEqual name="editPhoneBlockedListForm" property="lastUpdateUser" value="">
						<a href='../../showUser.do?userId=<c:out 
						value="${editPhoneBlockedListForm.lastUpdateUser}"/>' 
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
						value="${editPhoneBlockedListForm.lastUpdateUser}"/>\')'">
						<b><bean:write name="editPhoneBlockedListForm" property="lastUpdateUser" /></b></a>
					</logic:notEqual>
             		<logic:equal name="editPhoneBlockedListForm" property="lastUpdateUser" value="">
            			<bean:write name="editPhoneBlockedListForm" property="lastUpdateUser" />
            		</logic:equal>
	            	<html:hidden name="editPhoneBlockedListForm" property="lastUpdateUser" /></td>
	            <td nowrap="nowrap"> &nbsp; </td>
	        </tr>
	        </logic:notEqual>
	        <tr>
	            <td colspan="3">&nbsp;</td>
			</tr>
	        
	        <tr>
			    <td colspan='3' align='center'>
			    	<div id="formButtons">
             			<logic:equal name="editPhoneBlockedListForm" property="action" value="add">
							<html:submit property="submitProcess" value="Process"
								onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
								&nbsp; &nbsp;
						</logic:equal>
             			<logic:notEqual name="editPhoneBlockedListForm" property="action" value="add">
							<html:submit property="submitSave" value="Save"
								onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
								&nbsp; &nbsp;
						</logic:notEqual>
						<html:submit property="submitNew" value="Add a New Blocked Phone"
							onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
							&nbsp; &nbsp;
						<html:submit property="submitCancel" value="Blocked Phone Search"
							onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
					</div>
					<div id="processing" style="visibility:hidden" >
						<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
					</div>
			    </td>
			</tr>
		</tbody>
	</table>

<logic:equal name="editPhoneBlockedListForm" property="action" value="edit">
<logic:present name="consumerList">
<logic:notEmpty name="consumerList">
<%String dispClass = "TD-SMALL";%>

<h3><fmt:message key="label.affected.consumer.profile.list"/> &nbsp; 
	<bean:write name="editPhoneBlockedListForm" property="phoneNumber" /></h3>
<br />
<table>
	<tbody>
		<tr>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>		
		</tr>
		<logic:iterate id="profile" name="consumerList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="profile" property="custId" />'
			    ><bean:write name="profile" property="custId" />
				</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLogonId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSubStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custFrstName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLastName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custBrthDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSSNMask" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</tbody>		      
</table>
</logic:notEmpty>
<logic:empty name="consumerList">
<h3><fmt:message key="label.no.affected.consumer.profile.found"/></h3>
</logic:empty>
</logic:present>

<logic:notPresent name="consumerList">
<h3><fmt:message key="label.no.affected.consumer.profile.found"/></h3>
</logic:notPresent>

</logic:equal>

</html:form>
