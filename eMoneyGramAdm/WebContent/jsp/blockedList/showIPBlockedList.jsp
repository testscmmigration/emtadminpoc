<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<html:form action="/showIPBlockedList.do" focus="ipAddress">

<h3><fmt:message key="label.blockedlist.ipaddress.title" /></h3>
<br>
	<table border="0" width="100%">
	<tr>
		<td width="100" valign="top"><fmt:message key="label.blockedlist.ipaddress.ipaddress" /></td>
		<td>
			<html:text name="showIPBlockedListForm" property="ipAddress" size="15" maxlength="15" />
			&nbsp;(Format : <i>nnn.nnn.nnn.nnn</i>. Use * as wild card)
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitSearch" value="Search" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
				<html:submit property="submitShowAll" value="Show All" 
					onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>			
			
				<logic:equal name="auth" property="addIPBlockedList" value="true">
					<html:submit property="submitAdd" value="Add to Blocked List"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</div>
		</td>
	</tr>
	
	</table>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING">Your request is in process ....</span></div>

	
	<logic:notEmpty name="blockedIPs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.ipaddress" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.comments" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.risklevel" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createddate" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createdby" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifieddate" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifiedby" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.action" /></TD>
				
				</tr>

				<logic:iterate id="blockedIP" name="blockedIPs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="ipAddress1"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedIP" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedIP.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedIP.createUserId}"/>\')'">
							<b><bean:write name="blockedIP" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedIP" property="createUserId" value="">
            				<bean:write name="blockedIP" property="createUserId" />
            			</logic:equal>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedIP" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedIP.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedIP.updateUserId}"/>\')'">
							<b><bean:write name="blockedIP" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedIP" property="updateUserId" value="">
            				<bean:write name="blockedIP" property="updateUserId" />
            			</logic:equal>
					</td>
					<% String edit = "Edit"; %>
						<td nowrap='nowrap' class='<%=dispClass%>'>
							<a href='../../addIPBlockedList.do?ip=
								<bean:write filter="false" name="blockedIP" property="ipAddress1"/>&action=<%=edit%>'>
								<%=edit%>
							</a>
						</td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
