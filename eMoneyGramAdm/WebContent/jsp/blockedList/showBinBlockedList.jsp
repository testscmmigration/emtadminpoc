<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>
</center>

<html:form action="/showBinBlockedList.do" focus="binText">

<h3><fmt:message key="label.blockedlist.bin.title" /></h3>
<br>
	<table border="0" width="100%">
	<tr>
		<td width="180" valign="top"><fmt:message key="label.blockedlist.bin.bin" /> </td>
		<td>
			<html:text name="showBinBlockedListForm" property="binText" size="6" maxlength="6" />
			&nbsp;
			<html:submit property="submitSearch" value="Search" 
				onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';"/>
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<div id="formButtonsAdd">
				<html:submit property="submitShowAll" value="Show All"
					 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				<logic:equal name="auth" property="addBinBlockedList" value="true">
					&nbsp; &nbsp;
					<html:submit property="submitAdd" value="Add to Blocked List"
							 onclick="formButtonsAdd.style.display='none';processing.style.visibility='visible';" />
				</logic:equal>
			</div>
		</td>
	</tr>
	
	</table>

	<div id="processing" style="visibility: hidden"><span
		class="PROCESSING">Your request is in process ....</span></div>

	
	<logic:notEmpty name="bins">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.bin" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifiedby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.action" /> </TD>
				
				</tr>


				<logic:iterate id="blockedBean" name="bins">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="createUserId" value="">
            				<bean:write name="blockedBean" property="createUserId" />
            			</logic:equal>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="updateUserId" value="">
            				<bean:write name="blockedBean" property="updateUserId" />
            			</logic:equal>
					</td>
					<% String edit = "Edit"; %>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<a href='../../addBinBlockedList.do?bin=
							<bean:write filter="false" name="blockedBean" property="clearText"/>&action=<%=edit%>'>
							<%=edit%>
						</a>
					</td>
						
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</html:form>
