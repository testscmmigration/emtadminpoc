<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script>
function goBack() {
  history.go(-1);
}
</script>

<center>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

<script language="javascript">
	function setVisibility(id, visible) { 
    	var o = getElemById(o); 
	    if (o) { 
    	    if (typeof o.style != 'undefined') { 
        	    o.style.visibility = (visible ? 'visible' : 'hidden'); 
            	return (o.style.visibility == (visible ? 'visible' : 'hidden')); 
	        } else { 
    	        o.visibility = (visible ? 'show' : 'hide'); 
        	    return (o.visibility == (visible ? 'show' : 'hide')); 
	        } 
    	} 
	    return false; 
} 
</script>

</center>

<html:form action="/saveBlockedList.do">

<h3><fmt:message key="label.blocked.list.title" /></h3>

<table border="0">
<tr>
	<th></th>
	<th></th>
	<th></th>
	<th><fmt:message key="label.block.reason" /></th>
	<th><fmt:message key="label.block.status" /></th>
</tr>

<logic:notEmpty name="emailBlocker">

<tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockEmail"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.email.email" /></td>
    <td>
      <c:out value="${blockEmailAddress}" />
      <html:hidden name="saveBlockedListForm" property="emailAddress" value="${blockEmailAddress}" />
    </td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionEML" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionEML" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonEML" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonEML" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>

</logic:notEmpty>
 
<logic:notEmpty name="phoneBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockPhone"></html:checkbox></td>
    <td><fmt:message key="label.phone.number" /></td>
    <td>
      <c:out value="${blockPhoneNumber}" />
      <html:hidden name="saveBlockedListForm" property="phoneNumber" value="${blockPhoneNumber}" />
    </td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionPHON" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionPHON" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonPHON" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonPHON" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
 </logic:notEmpty>
 <logic:notEmpty name="ccBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockCC"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.cc.cc" /></td>
    <td>
      <c:out value="${blockBin}******${blockMask}" />
    </td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionCC" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionCC" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonCC" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonCC" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
  </logic:notEmpty>
<logic:notEmpty name="binBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockBin"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.bin.bin" /></td>
    <td><c:out value="${blockBin}" /></td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionBIN" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionBIN" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonBIN" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonBIN" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
</logic:notEmpty>
<logic:notEmpty name="ipBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockIP"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.ipaddress.ipaddress" /></td>
    <td><c:out value="${blockIP}" /></td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionIP" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionIP" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonIP" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonIP" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
</logic:notEmpty>
<logic:notEmpty name="abaBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockABA"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.bank.abanumber" /></td>
    <td><c:out value="${blockABANumber}" /></td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionABA" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionBANK" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonABA" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonBANK" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
  </logic:notEmpty>
  <logic:notEmpty name="accountBlocker">
  <tr>
	<td><html:checkbox name="saveBlockedListForm" property="blockAcct"></html:checkbox></td>
    <td><fmt:message key="label.blockedlist.bank.acctnumber" /></td>
    <td><c:out value="${blockAccountNumber}" /></td>
    <td width="140">
	    <html:select name="saveBlockedListForm" property="blockingActionACCT" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingActionBANK" property="blockedStatusCode" labelProperty="blockedStatusDesc" />
		</html:select>
    </td>
    <td width="140">
    	<html:select name="saveBlockedListForm" property="blockingReasonACCT" size="1">
			<html:option value="None"><fmt:message key="option.select.an.option"/></html:option>												
			<html:options collection="blockingReasonBANK" property="reasonCode" labelProperty="reasonDesc" />
		</html:select>
    </td>
  </tr>
  </logic:notEmpty>
</table>

<table border="0">

	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>

	<tr>
		<td width="140"><fmt:message key="label.fraud.risk.level" /></td>
		<td colspan="2">
			<html:text name="saveBlockedListForm" property="riskLevel" size="3" maxlength="3" />
		</td>
	</tr>
	<tr>
		<td width="140" valign="top"><fmt:message key="label.block.comment" /></td>
		<td colspan="2">
			<html:textarea name="saveBlockedListForm" property="blockingComment"  rows="4" cols="40" 
					onkeypress="textCounter(this, 128);" />			
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	</table>
<html:hidden name="showBlockedListForm" property="custId" value="${custId}" />

<html:submit property="submitProcess" value=" Process " />
<button type="button" onclick="goBack();">Back</button>
</html:form>

<br/>

<div>

<logic:notEmpty name="emailBlocker">
<fieldset>
    <legend>Email</legend>

<h3><fmt:message key="label.blockedlist.email.existing.list" /></h3>	
	<logic:empty name="blockedEmails">
		<fmt:message key="label.blockedlist.email.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedEmails">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.email.address" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.email.type" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.reason" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.comment" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.block.status" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.fraud.risk.level" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.create.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.create.user" /></TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.last.update.date" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'>
					<fmt:message key="label.last.update.user" /></TD>
				</tr>
								

				<logic:iterate id="blockedBean" name="blockedEmails">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="elecAddrTypeCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.email.consumer.list" /></h3>	

	<logic:empty name="consumerProfiles">
		<fmt:message key="label.blockedlist.email.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="consumerProfiles">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				</tr>

				<logic:iterate id="consumerProfile" name="consumerProfiles">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfile" property="custSSNMask"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
	</fieldset>
	
	<hr width="50%" align="left">
	</logic:notEmpty>

<logic:notEmpty name="phoneBlocker">
<fieldset><legend>Phone</legend>
<logic:present name="consumerList">
<logic:notEmpty name="consumerList">
<%String dispClass = "TD-SMALL";%>

<h3><fmt:message key="label.affected.consumer.profile.list"/> </h3>
<br />
<table>
	<tbody>
		<tr>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.cust.birth.date"/></th>		
			<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>		
		</tr>
		<logic:iterate id="profile" name="consumerList">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>'>
			    <a href='../../showCustomerProfile.do?custId=<bean:write 
			    name="profile" property="custId" />'
			    ><bean:write name="profile" property="custId" />
				</a></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLogonId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSubStatDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custFrstName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custLastName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custBrthDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="profile" property="custSSNMask" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>	
		</logic:iterate>
	</tbody>		      
</table>
</logic:notEmpty>
<logic:empty name="consumerList">
<h3><fmt:message key="label.no.affected.consumer.profile.found"/></h3>
</logic:empty>
</logic:present>

<logic:notPresent name="consumerList">
<h3><fmt:message key="label.no.affected.consumer.profile.found"/></h3>
</logic:notPresent>
</fieldset>


<hr width="50%" align="left">
</logic:notEmpty>

<logic:notEmpty name="ccBlocker">
<fieldset><legend>Credit Card Account</legend>
	<h3><fmt:message key="label.blockedlist.cc.existing.list" /></h3>	
	<logic:empty name="blockedCCs">
		<fmt:message key="label.blockedlist.cc.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedCCs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.mask" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.cc.modifiedby" /> </TD>
				</tr>

				<logic:iterate id="blockedBean" name="blockedCCs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="binText"/>******<bean:write filter="false" name="blockedBean" property="maskText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.cc.consumer.list" /></h3>	

	<logic:empty name="customerProfileAccounts">
		<fmt:message key="label.blockedlist.cc.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="customerProfileAccounts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				
				</tr>

				<logic:iterate id="customerProfileAccount" name="customerProfileAccounts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="customerProfileAccount" property="custSsnMaskNbr"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
</fieldset>

	<hr width="50%" align="left">
</logic:notEmpty>

<logic:notEmpty name="binBlocker">
<h3><fmt:message key="label.blockedlist.bin.bin" /></h3>
	<logic:empty name="bins">
		<fmt:message key="label.blockedlist.bin.consumer.norecord" />
	</logic:empty>
	<logic:notEmpty name="bins">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.bin" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.modifiedby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bin.action" /> </TD>
				
				</tr>


				<logic:iterate id="blockedBean" name="bins">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="createUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.createUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="createUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="createUserId" value="">
            				<bean:write name="blockedBean" property="createUserId" />
            			</logic:equal>
					</td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
	               		<logic:notEqual name="blockedBean" property="updateUserId" value="">
							<a href='../../showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>' 
							onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out 
							value="${blockedBean.updateUserId}"/>\')'">
							<b><bean:write name="blockedBean" property="updateUserId" /></b></a>
						</logic:notEqual>
             			<logic:equal name="blockedBean" property="updateUserId" value="">
            				<bean:write name="blockedBean" property="updateUserId" />
            			</logic:equal>
					</td>
					<% String edit = "Edit"; %>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<a href='../../addBinBlockedList.do?bin=
							<bean:write filter="false" name="blockedBean" property="clearText"/>&action=<%=edit%>'>
							<%=edit%>
						</a>
					</td>
						
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
</fieldset>
<hr width="50%" align="left">	
	</logic:notEmpty>

<logic:notEmpty name="ipBlocker">
<fieldset><legend>IP</legend>
	<h3>Existing records that will be affected by this change</h3>	
	<logic:empty name="blockedIPs">
		<fmt:message key="label.blockedlist.ipaddress.noexisting" />
	</logic:empty>
	
	<logic:notEmpty name="blockedIPs">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.ipaddress" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ipaddress.modifiedby" /> </TD>
				
				</tr>

				<logic:iterate id="blockedIP" name="blockedIPs">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="ipAddress1"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedIP" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedIP" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>
</fieldset>
</logic:notEmpty>
	
	<logic:notEmpty name="abaBlocker">
<fieldset><legend>ABA Number</legend>
<h3><fmt:message key="label.blockedlist.bank.existing.list" /></h3>	
	<logic:empty name="blockedAccts">
		<fmt:message key="label.blockedlist.bank.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedAccts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.abanumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.masknumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.modifiedby" /> </TD>
				</tr>

				<logic:iterate id="blockedBean" name="blockedAccts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="abaNumber"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

	<BR>
	
	<h3><fmt:message key="label.blockedlist.bank.consumer.list" /></h3>	

	<logic:empty name="consumerProfileAccounts">
		<fmt:message key="label.blockedlist.bank.consumer.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="consumerProfileAccounts">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.consumer.id"/></th>		
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.login.id"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.first.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.last.name"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sub.status"/></th>
					<th nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.ssn.mask"/></th>						
				</tr>

				<logic:iterate id="consumerProfileAccount" name="consumerProfileAccounts">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custLogonId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custFrstName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custLastName" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custSubStatDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
						<bean:write filter="false" name="consumerProfileAccount" property="custSsnMaskNbr"/></td>

				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>

</fieldset>
</logic:notEmpty>

<logic:notEmpty name="accountBlocker">
<fieldset><legend>Account Number</legend>
<h3><fmt:message key="label.blockedlist.bank.existing.list" /></h3>	
	<logic:empty name="blockedAcct">
		<fmt:message key="label.blockedlist.bank.norecord" />
	</logic:empty>
	
	<logic:notEmpty name="blockedAcct">
		<%String dispClass = "TD-SMALL";%>	
		<table>
			<tbody>
				<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.abanumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.bank.masknumber" /></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.reason" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.comments" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.status" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.risklevel" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.createddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.createdby" /> </TD>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.modifieddate" /> </TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.blockedlist.ssn.modifiedby" /> </TD>
				</tr>

				<logic:iterate id="blockedBean" name="blockedAcct">
				<tr>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="abaNumber"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="clearText"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="reasonDesc" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="comment" /></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="statusDesc"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
						<bean:write filter="false" name="blockedBean" property="fraudRiskLevelCode"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="createUserId"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="statusUpdateDate"/></td>
					<td nowrap='nowrap' class='<%=dispClass%>'>
						<bean:write filter="false" name="blockedBean" property="updateUserId"/></td>
				<tr>
				<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
					else dispClass = "TD-SHADED-SMALL"; %>	
				</logic:iterate>
			</tbody>			      
		</table>
	</logic:notEmpty>



</fieldset>
</logic:notEmpty>

</div>