<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>


<center>
	<span class="error"> <html:errors
			property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
	</span>
	 <div id="jsErrCheckbox" class="error" style="display:none">You must select at least one option from the checkbox list</div>
	<div id="jsErrReason" class="error" style="display:none">You must select a Block Reason </div>
	<div id="jsErrComment" class="error" style="display:none">You must provide a Block Comment</div>

	<script language="javascript">
		function setVisibility(id, visible) {
			var o = getElemById(o);
			if (o) {
				if (typeof o.style != 'undefined') {
					o.style.visibility = (visible ? 'visible' : 'hidden');
					return (o.style.visibility == (visible ? 'visible'
							: 'hidden'));
				} else {
					o.visibility = (visible ? 'show' : 'hide');
					return (o.visibility == (visible ? 'show' : 'hide'));
				}
			}
			return false;
		}
		
		window.onload = function() {
		/*document.getElementById("blockedstatuscode").checked = true;*/
			selectDefaultInputs();
		}
		
				
		
	</script>

</center>

<h3>
	<fmt:message key="label.blocked.list.title" />
</h3>

<html:form action="/saveBlockedList.do">

	<div>
		<table id="superTaintOptions">

			<c:forEach var="cc" items="${blockCCList}">
				<tr>
					<td width="200px"><input type="checkbox" name="blockCC" id="${cc.maskedText}"  /> <!-- ***check auth here to see if disabled or not***  -->
						<fmt:message key="label.blockedlist.cc.card" /></td>
					<c:if test="${blockMask != null && blockMask != ''}">
						<c:if test="${auth.addCCBlockedList}">
							<input type="checkbox" checked="checked" name="blockCC" id="${cc.maskedText}"/>
						</c:if>
						<c:if test="${!auth.addCCBlockedList}">
							<input type="checkbox" checked="checked" name="blockCC" disabled />
						</c:if>
					</c:if>
					<td>${cc.maskedText}</td>
				</tr>
			</c:forEach>

			<tr>
				<td width="200px"><input type="checkbox"
					name="primaryPhoneBlocker" checked="checked"/> <fmt:message
						key="label.blockedlist.phone.primary.number" /></td>
				<td><c:out value="${fn:escapeXml(blockPrimaryPhoneNumber)}" />
					<input type="hidden" name="blockPrimaryPhoneNumber"
					value="${fn:escapeXml(blockPrimaryPhoneNumber)}" />
				</td>
			</tr>


			<c:if
				test="${blockAltPhoneNumber != null && blockAltPhoneNumber != ''}">
				<tr>
					<td width="200px"><input type="checkbox"
						name="altPhoneBlocker" checked="checked"/> <fmt:message
							key="label.blockedlist.phone.alt.number" /></td>
					<td><c:out value="${fn:escapeXml(blockAltPhoneNumber)}" /> <input
						type="hidden" name="blockAltPhoneNumber"
						value="${fn:escapeXml(blockAltPhoneNumber)}" />
					</td>
				</tr>
			</c:if>

			<tr>
			</tr>

			<c:forEach var="ba" items="${blockBankList}">
				<tr>

					<c:if test="${auth.addBankBlockedList}">
						<td width="200px"><input type="checkbox" name="blockAcct" id="${ba.encryptedText}" />
							<!-- ***check auth here to see if disabled or not***  --> <fmt:message
								key="label.blockedlist.bank.acctnumber" /></td>
					</c:if>
					<c:if test="${!auth.addBankBlockedList}">
						<td width="200px"><input type="checkbox" name="blockAcct"
							disabled /> <fmt:message key="label.blockedlist.bank.acctnumber" />
						</td>
					</c:if>
					<td>*${ba.maskedText}</td>
				</tr>
			</c:forEach>

			<tr>
			<tr>
				<td Width="100px"><fmt:message key="label.block.status" />
				</td>
				<c:forEach var="bb" items="${blockingActionCC}">

					<td width="105px"><html:radio property="blockingActionCC"
							value="${bb.blockedStatusCode}" styleId="blockedstatuscode" /> <span
						style="width: 80px"><c:out value="${bb.blockedStatusDesc}"></c:out>
					</span></td>
				</c:forEach>


				<td><fmt:message key="label.block.reason" /> <html:select
						name="saveBlockedListForm" styleId="selectReason"
						property="blockingReasonCC" size="1">
						<html:option value="None">
							<fmt:message key="option.select.an.option" />
						</html:option>
						<html:options collection="blockingReasonCC" property="reasonCode"
							labelProperty="reasonDesc" />
					</html:select>
				</td>
			</tr>

			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>


			<tr>
				<td width="140" valign="top"><fmt:message
						key="label.block.comment" /></td>
				<td colspan="2"><html:textarea name="saveBlockedListForm"
						styleId="blkComment" property="blockingComment" rows="4" cols="40"
						onkeypress="textCounter(this, 128);" />
				</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
		</table>
		<input type="hidden" id="blockCCVal" name="blockCCVal" value="" />
		<input type="hidden" id="blockBnkAccVal" name="blockBnkAccVal" value="" />
		<p>

			<html:submit property="submitProcess"
				onclick="validateBlockingForm(this.form); return false;"
				value=" Process " styleId="submittbutton" />
			<input type="button" value="Cancel"
				onclick="javascript:window.close();" />

		</p>
		<% Boolean isavailPcidb=(Boolean) request.getSession().getAttribute("pciCardNotFound");
		if(isavailPcidb==true){
		%>

		<p class="error">
			<fmt:message key="label.blockedlist.cc.nopci" />
		</p>
		<%} %>
	</div>
</html:form>