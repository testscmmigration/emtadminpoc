<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ page import="emgadm.constants.EMoneyGramAdmSessionConstants" %>
<%@ page import="emgadm.model.UserProfile" %>
<html>

<head>
	<%@ page language="java"
			 contentType="text/html; charset=ISO-8859-1"
			 pageEncoding="ISO-8859-1"%>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<meta http-equiv="Content-Style-Type" content="text/css"/>

	<html:base/>
	<link href="../../theme/eMGAdm.css" rel="stylesheet" type="text/css" />
</head>
<body MARGINWIDTH="0" MARGINHEIGHT="0" rightMargin="0" topMargin="0" leftMargin="0" bottomMargin="0" style="MARGIN-TOP: 0px; MARGIN-LEFT: 0px">
<table border="0" width='100%"' height='100%' cellspacing="0" cellpadding="0" >

	<tr height='16' valign='top' >
		<td colspan='4' bgcolor="#CF002F" valign="top" align="left"></td>
	</tr>

<!--
	<tr height='8' valign='top' >
		<td colspan='4' bgcolor="#BCBCBC" valign="top" align="left"></td>
	</tr>

	<tr height='80' valign='top' >
		<td colspan='4' bgcolor="#000000" valign="top" align="left" ><img src="../../images/emoneygramheader.jpg"  /></td>
	</tr>
-->

	<tr height='8' valign='top' >
		<td colspan='4' bgcolor="#BCBCBC" valign="top" align="left"></td>
	</tr>


	<tr height='36' valign='top' >
		<td width='100%' bgcolor="#000000" colspan="4">
			<tiles:insert attribute='header' />
		</td>
	</tr>
	
	<tr height='40'>
		<td width="3%" bgcolor="#FFFFFF" valign="top" align="left"></td>
		<td width='97%' align='left' nowrap='nowrap' colspan='3'>
			<table border="0" width='100%"' height='100%' cellspacing="0" cellpadding="0" >
				<tr>
					<td nowrap='nowrap'><tiles:insert attribute='quickSearch' /></td>
	 				<td align="right"><img src="../../images/adminLogo.gif" /></td>
	 			</tr>
	 		</table>
	 	</td>
	</tr>

	<tr>
		<td></td>
		<td colspan='3' align="left" class='TITLE' bgcolor="#FFFFFF">
		&nbsp;&nbsp;&nbsp;<img src="../../images/doubleGrayLine.gif"  />
		</td>
	</tr>

	<tr>
		<td width="3%" bgcolor="#ffffff" align="left" valign="top" ></td>
		<td width="85%" valign="top" align="left"><tiles:insert attribute='body' /></td>
		<td width='10%' colspan='2'></td>
	</tr>
	<tr>
		<td colspan="4" valign="bottom">
			<tiles:insert attribute="footer" />
		</td>
	</tr>
</table>

</body>
</html>
