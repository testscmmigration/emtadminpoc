<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%
String versionLabel = (String) request.getSession().getServletContext().getAttribute("versionLabel");
 %>
<table cellpadding='0' cellspacing='0' width="100%" id="footer">
	<tr>
		<td>
			<font color="#ffffff" >�  2006 MoneyGram International, Inc. All Rights Reserved.</font>
		</td>
		<td align='right'>
			<font color="#ffffff" >${fn:escapeXml(versionLabel)}</font>
		</td>
	</tr>
</table>