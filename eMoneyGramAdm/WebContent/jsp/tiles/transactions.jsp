<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-tiles" prefix="tiles" %>
<%@ page import="emgadm.constants.EMoneyGramAdmSessionConstants" %>
<%@ page import="emgadm.model.UserProfile" %>
<%@ page import="emgadm.constants.EMoneyGramAdmRequestConstants" %>

<html> 
<head>
	<title><tiles:getAsString name="title"/></title>
	<html:base/>
	<link href="../../theme/eMGAdm.css" rel="stylesheet" type="text/css" />
	<link href="../../theme/EMTBox.css" rel="stylesheet" type="text/css" />
	<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
	<META HTTP-EQUIV="Expires" CONTENT="-1">
</head>

<body MARGINWIDTH="0" MARGINHEIGHT="0" rightMargin="0" topMargin="0" leftMargin="0" bottomMargin="0" style="MARGIN-TOP: 0px; MARGIN-LEFT: 0px">

<table id="headerTable" border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr height='16' valign='top' >
		<td colspan='4' bgcolor="#CF002F" valign="top" align="left"></td>
	</tr>

	<tr height='8' valign='top' >
		<td colspan='4' bgcolor='#BCBCBC' valign='top' align='left'></td>
	</tr>

	<tr height='36' valign='top' >
		<td width='6%' bgcolor='#000000' valign='top' align='left' colspan='2'></td>
		<td width='*' bgcolor='#000000' colspan='2'>
			<tiles:insert attribute='header' /></td>
	</tr>

	<tr height='40'>
		<td width='3%' bgcolor='#FFFFFF' valign='top' align='left'></td>
		<td width='97%' align='left' nowrap='nowrap' colspan='3'>
			<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" >
				<tr>
					<td nowrap='nowrap'><tiles:insert attribute='quickSearch' /></td>
	 				<td align="right"><img src="../../images/adminLogo.gif" /></td>
	 			</tr>
	 		</table>
		</td>
	</tr>
</table>
<div id="contentWrapper">
	<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
				<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="0" >
					<tr>
						<td></td>
						<td colspan='3' align='left' class='TITLE' bgcolor='#FFFFFF'>
							&nbsp;&nbsp;&nbsp;<img src="../../images/doubleGrayLine.gif"  /></td>
					</tr>
				
					<tr>
						<td width='6%' bgcolor='#ffffff' align='left' valign='top'></td>
						<td width='84%' valign='top' align='left'>
						<html:form action="/showTransQueue.do" focus="submitRefresh" styleId="showTransQueue">
							<tiles:insert attribute='body' />
							<tiles:insert attribute='transQueue' />
						</html:form>
						</td>
						<td width='10%' colspan='2'></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
		<td colspan="4" valign="bottom">
			<tiles:insert attribute="footer" />
		</td>
	</tr>
	</table>
</div>

<script src="../../Display/jscript/fixedHeader.js" language="JavaScript"></script>
<script language="JavaScript">
<!--
var url = unescape(window.location.href);
<%  java.lang.String jumpTo = (java.lang.String) request.getAttribute("jumpTo"); %>
var tmp = "<%=jumpTo%>";
<%    if (jumpTo != null && !"".equals(jumpTo)) { %>
        var newUrl = url + "#" + tmp;
        window.location.replace(newUrl);
<%  } %>
//-->
</script>

</body>

</html>
