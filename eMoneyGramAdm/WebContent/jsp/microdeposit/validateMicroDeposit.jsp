<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<center>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>

<h3><fmt:message key="label.validate.micro.deposit.amount"/></h3>

<html:form action="/validateMicroDeposit.do">

<center>
	<table border='0' width='100%' cellpadding='1' cellspacing='10'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.cust.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${validateMicroDepositForm.custId}" />
					<html:hidden name="validateMicroDepositForm" property="custId" />
				</td>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${validateMicroDepositForm.accountId}" />
					<html:hidden name="validateMicroDepositForm" property="accountId" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.number"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${validateMicroDepositForm.accountNumberMask}" />
					<html:hidden name="validateMicroDepositForm" property="accountNumberMask" />
				</td>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.routing.number"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${validateMicroDepositForm.routingNumber}" />
					<html:hidden name="validateMicroDepositForm" property="routingNumber" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.bank"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${validateMicroDepositForm.financialInstitutionName}" />
					<html:hidden name="validateMicroDepositForm" property="financialInstitutionName" />
				</td>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.type"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT" colspan="3">
					<c:out value="${validateMicroDepositForm.accountTypeDesc}" />
					<html:hidden name="validateMicroDepositForm" property="accountTypeDesc" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.taint.text" /><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT"><font color='red'><b>
					<c:out value="${validateMicroDepositForm.accountTaintText}" /></b></font>
					<html:hidden name="validateMicroDepositForm" property="accountTaintText" />
				</td>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.aba.taint.text"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT"><font color='red'><b>
					<c:out value="${validateMicroDepositForm.abaTaintText}" /></b></font>
					<html:hidden name="validateMicroDepositForm" property="abaTaintText" />
				</td>
			</tr>
			<logic:notEmpty name="acctCmnt">
			<tr>
				<td colspan="4">
				<table width="100%">
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.user" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment.reason" /></td>
						</tr>
					</thead>
					<tbody>
						<logic:iterate id="comment" name="acctCmnt">
							<tr>
								<td>
									<fmt:formatDate value="${comment.createDate}" pattern="MMM dd yy h:mm a z" />
								</td>
								<td>
									<c:out value="${comment.text}" />
								</td>
								<td>
									<c:out value="${comment.createdBy}" />
								</td>
								<td>
									<c:out value="${comment.reasonDescription}" />
								</td>
							</tr>
						</logic:iterate>
					</tbody>
				</table>
				</td>
			</tr>
			</logic:notEmpty>
		</tbody>
	</table>
<br />

<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.bank.account.confirmation" />: 
	</legend>

<fmt:message key="label.enter.customer.reported.deposit.amount" />:
<table>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL'>
			<fmt:message key="label.deposit.amount"/><fmt:message key="label.suffix"/>
		</td>
		<td nowrap="nowrap" class="TD-INPUT">
			$0.<html:text size="4" name="validateMicroDepositForm" property="amt1" /> (in cents)
			<span class="error"><html:errors property="amt1"/></span>
		</td>
	</tr>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL'>
			<fmt:message key="label.deposit.amount"/><fmt:message key="label.suffix"/>
		</td>
		<td nowrap="nowrap" class="TD-INPUT">
			$0.<html:text size="4" name="validateMicroDepositForm" property="amt2" /> (in cents)
			<span class="error"><html:errors property="amt2"/></span>
		</td>
	</tr>
</table>
<br />

<input type="submit" name='submitValidate' value='Submit Amounts' /> &nbsp; &nbsp; &nbsp;

<input type="submit" name='submitOverride' value='Force Micro-Deposit Validation' />

<input type="submit" name='submitCancel' value='Exit' /> &nbsp; &nbsp; &nbsp;

</fieldset>
</center>
</html:form>

