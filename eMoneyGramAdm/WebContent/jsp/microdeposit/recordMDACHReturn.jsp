<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</center>

<h3><fmt:message key="label.record.micro.deposit.ach.return"/></h3>

<html:form action="/recordMDACHReturn.do">
<input type="submit" name='submitReturn' value='Return' />

<logic:notEmpty name="vldnList">
<br />
<h4><fmt:message key="msg.record.micro.deposit.ach.return"/></h4>
<br />
<center>
	<%String dispClass = "TD-SMALL";%>

		<table border='0' width='100%' cellpadding='0' cellspacing='1'>
			<tr>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.ach.return" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.deact.acct" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.vldn.id" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.account.id" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.md.status"/></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.vldn.count"/></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.md.stat.date"/></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.batch.id" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.md.tran.id" /></td>
				<td nowrap='nowrap' class='td-header-small' align='center'>
					<fmt:message key="label.md.deposit.amt"/></td>
			</tr>
			<tr><td colspan='12'> &nbsp; </td></tr>
							
			<logic:iterate id="vldn" name="vldnList">
			
			<tr>
				<logic:equal name="vldn" property="status.mdStatus" value="ACS">
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
						<html:multibox property="selectedVldn">
							<bean:write name="vldn" property="validationId" />
						</html:multibox>
					</td>
				</logic:equal>
				
				<logic:notEqual name="vldn" property="status.mdStatus" value="ACS">
					<td nowrap='nowrap' class='<%=dispClass%>'> &nbsp; </td>
				</logic:notEqual>
				
				<logic:equal name="vldn" property="status.mdStatus" value="ACS">

					<logic:equal name="vldn" property="processDeactiveAcct" value="false">
					<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
						<html:multibox property="selectedDeAct">
							<bean:write name="vldn" property="validationId" />
						</html:multibox></td>
					</logic:equal>

					<logic:equal name="vldn" property="processDeactiveAcct" value="true">
						<td nowrap='nowrap' class='<%=dispClass%>'> &nbsp; </td>
					</logic:equal>
					
				</logic:equal>
				
				<logic:notEqual name="vldn" property="status.mdStatus" value="ACS">
					<td nowrap='nowrap' class='<%=dispClass%>'> &nbsp; </td>
				</logic:notEqual>
						
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="validationId" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<a href='../../viewBankAccountDetail.do?accountId=
					<bean:write name="vldn" property="custAccountId" />'>
					<bean:write name="vldn" property="custAccountId" /></a></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="vldn" property="status.mdStatus" /> (
					<bean:write name="vldn" property="status.mdStatusDesc" />)</td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="validationTryCount" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="statUpdateDate" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="batchId1" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="depositTranId1" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="fmtAmount1" /></td>
			</tr>
			<tr>
				<td  nowrap='nowrap' class='<%=dispClass%>' colspan='7'>
					<font color='red'><b><bean:write name="vldn" property="msg" /></b></font></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="batchId2" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="depositTranId2" /></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<bean:write name="vldn" property="fmtAmount2" /></td>
			</tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
					
			</logic:iterate>

		</table>
		<br />
		<logic:equal name="vldn" property="status.mdStatus" value="ACS">
			<input type="submit" name='submitUpdate' value='Update' />
		</logic:equal>

</logic:notEmpty>	
</html:form>
