<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<h3><fmt:message key="label.micro.deposit.ach.search"/></h3>

<center>

<html:form action="/listMDBatches.do" focus="tranId">

<logic:equal name="updateACR" value="Y">
	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.quick.search.md.tran"/></legend>
	
		<table border='0' width='80%' cellpadding='1' cellspacing='1'>
			<tr>
				<td align="right" nowrap="nowrap" class="td-label-small">
					<fmt:message key="label.md.tran.id"/><fmt:message key="label.suffix"/>&nbsp;</td>
				<td nowrap="nowrap" class="td-small">
					<html:text size="7" maxlength="7" property="tranId" />
					&nbsp; &nbsp;
					<input type="submit" name='submitGo' value='  Go  ' />
					<span class="error"><html:errors property='tranId'/></span>
				</td>
			</tr>
		</table>
	</fieldset>
</logic:equal>

<br />

	<fieldset>
		<legend class="td-label-small"><fmt:message key="label.md.batch.search"/></legend>

		<table border='0' width='80%' cellpadding='1' cellspacing='1'>
			<tr>
				<td nowrap="nowrap" class="td-label-small" valign="middle">
					<fmt:message key="label.batch.begin.date"/><fmt:message key="label.suffix"/> &nbsp;
					<html:text size="10" property="batchBeginDate" 
						onchange="setOtherRangeValue(this, listMDBatchesForm.batchBeginDate);" />
						<img border="0" src="../../images/calendar.gif" 
						onclick="show_calendar('listMDBatchesForm.batchBeginDate');" 
						onmouseover="window.status='Pick a Date';return true;" 
						onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='batcgBeginDate'/></span>
				</td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap="nowrap" class="td-label-small" valign="middle">
					<fmt:message key="label.batch.end.date"/><fmt:message key="label.suffix"/> &nbsp;
					<html:text size="10" property="batchEndDate" 
						onchange="setOtherRangeValue(this, listMDBatchesForm.batchEndDate);" />
						<img border="0" src="../../images/calendar.gif" 
						onclick="show_calendar('listMDBatchesForm.batchEndDate');" 
						onmouseover="window.status='Pick a Date';return true;" 
						onmouseout="window.status='';return true;"/>
					<span class="error"><html:errors property='batchEndDate'/></span>
				</td>
				<td> &nbsp; &nbsp; </td>
				<td nowrap="nowrap" class="td-small" valign="middle">
					<input type="submit" name='submitSearch' value='Search' />
				</td>
			</tr>
		</table>

	</fieldset>

<logic:notEmpty name="batchList">
<br />
	<%String dispClass = "TD-SMALL";%>

		<table border='0' width='50%' cellpadding='1' cellspacing='1'>
			<tr>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.batch.id"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.batch.receive.date"/></td>
				<td nowrap='nowrap' class='TD-HEADER-SMALL' align='center'><fmt:message key="label.batch.rec.cnt"/></td>
			</tr>
							
			<logic:iterate id="batch" name="batchList">
			<tr>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
					<logic:equal name="updateACR" value="Y">
						<a href='../../recordMDACHReturn.do?batchId=<bean:write name="batch" property="batchId"/>'>
						<bean:write filter="false" name="batch" property="batchId"/></a>
					</logic:equal>
					<logic:notEqual name="updateACR" value="Y">
						<bean:write filter="false" name="batch" property="batchId"/>
					</logic:notEqual>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write name="batch" property="receiveDate"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'><bean:write name="batch" property="recCnt"/></td>
			<tr>
			<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
				else dispClass = "TD-SHADED-SMALL"; %>
			</logic:iterate>

		</table>

</logic:notEmpty>	
</html:form>
