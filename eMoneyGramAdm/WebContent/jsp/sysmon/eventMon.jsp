<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

</CENTER>

<html:form action="/eventMon">

<logic:equal name="eventMonForm" property="selType" value="R">
<logic:notEqual name="eventMonForm" property="refTime" value="0">
<meta http-equiv="refresh" content="<bean:write name='eventMonForm' property='refTime' />" />
</logic:notEqual>
</logic:equal>

<h3>System Event Monitor</h3>

<table>
	<tr>
		<td>
			<html:radio property="selType" value="R">Realtime for today</html:radio>
			and automatically refreshes data every 
			<html:text size="4" property="refTime" /> seconds
			<span class="error"><html:errors property='refTime'/></span>
		</td>
		<td rowspan="2"> &nbsp; &nbsp; </td>
		<td rowspan="2" valign="middle"><html:submit property="submitGo" value="Refresh Data" /></td>
	</tr>
	<tr>
		<td>
			<html:radio property="selType" value="A">Archive for</html:radio>
			<html:text size="10" property="archiveDate" 
				onchange="setOtherRangeValue(this, eventMonForm.archiveDate);" />
			<img border="0" src="../../images/calendar.gif" 
				onclick="show_calendar('eventMonForm.archiveDate');" 
				onmouseover="window.status='Pick a Date';return true;" 
				onmouseout="window.status='';return true;" />
			<span class="error"><html:errors property='archiveDate'/></span>
		</td>
	</tr>
</table>

<br />

<center>

<h3><bean:write name="eventMonForm" property="displayDate" /></h3>

<logic:notEmpty name="eventStat">
<logic:iterate id="eventEntry" name="eventStat">

<table border='1' cellpadding='0' cellspacing='0' width='100%'>
	<tr><td nowrap="nowrap" class="td-header-small" colspan="26">
		<b>Event: &nbsp; &nbsp; &nbsp;
			</b><font color="red"><bean:write name="eventEntry" property="event" /></font></td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small" align="center">&nbsp;</td>		
		<td nowrap="nowrap" class="td-header-small" align="center">0-1</td>
		<td nowrap="nowrap" class="td-header-small" align="center">1-2</td>
		<td nowrap="nowrap" class="td-header-small" align="center">2-3</td>
		<td nowrap="nowrap" class="td-header-small" align="center">3-4</td>
		<td nowrap="nowrap" class="td-header-small" align="center">4-5</td>
		<td nowrap="nowrap" class="td-header-small" align="center">5-6</td>
		<td nowrap="nowrap" class="td-header-small" align="center">6-7</td>
		<td nowrap="nowrap" class="td-header-small" align="center">7-8</td>
		<td nowrap="nowrap" class="td-header-small" align="center">8-9</td>
		<td nowrap="nowrap" class="td-header-small" align="center">9-10</td>
		<td nowrap="nowrap" class="td-header-small" align="center">10-11</td>
		<td nowrap="nowrap" class="td-header-small" align="center">11-12</td>
		<td nowrap="nowrap" class="td-header-small" align="center">12-1</td>
		<td nowrap="nowrap" class="td-header-small" align="center">1-2</td>
		<td nowrap="nowrap" class="td-header-small" align="center">2-3</td>
		<td nowrap="nowrap" class="td-header-small" align="center">3-4</td>
		<td nowrap="nowrap" class="td-header-small" align="center">4-5</td>
		<td nowrap="nowrap" class="td-header-small" align="center">5-6</td>
		<td nowrap="nowrap" class="td-header-small" align="center">6-7</td>
		<td nowrap="nowrap" class="td-header-small" align="center">7-8</td>
		<td nowrap="nowrap" class="td-header-small" align="center">8-9</td>
		<td nowrap="nowrap" class="td-header-small" align="center">9-10</td>
		<td nowrap="nowrap" class="td-header-small" align="center">10-11</td>
		<td nowrap="nowrap" class="td-header-small" align="center">11-12</td>
		<td nowrap="nowrap" class="td-header-small" align="center">Summary</td>
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small"><b>Count</b></td>
		<logic:iterate id="cnt" name="eventEntry" property="countText">
			<td nowrap="nowrap" class="td-small" align="center"><bean:write name="cnt" /></td>
		</logic:iterate>
		<td nowrap="nowrap" class="td-small" align="center"><bean:write name="eventEntry" property="dailyCount" /></td>		
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small"><b>High</b></td>
		<logic:iterate id="high" name="eventEntry" property="highText">
			<td nowrap="nowrap" class="td-small" align="center"><bean:write name="high" /></td>
		</logic:iterate>
		<td nowrap="nowrap" class="td-small" align="center"><bean:write name="eventEntry" property="dailyHigh" /></td>		
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small"><b>Avg</b></td>
		<logic:iterate id="avg" name="eventEntry" property="averageText">
			<td nowrap="nowrap" class="td-small" align="center"><bean:write name="avg" /></td>
		</logic:iterate>
		<td nowrap="nowrap" class="td-small" align="center"><bean:write name="eventEntry" property="dailyAverage" /></td>		
	</tr>
	<tr>
		<td nowrap="nowrap" class="td-header-small"><b>Low</b></td>
		<logic:iterate id="low" name="eventEntry" property="lowText">
			<td nowrap="nowrap" class="td-small" align="center"><bean:write name="low" /></td>
		</logic:iterate>
		<td nowrap="nowrap" class="td-small" align="center"><bean:write name="eventEntry" property="dailyLow" /></td>		
	</tr>
</table>
<br />

</logic:iterate>
</logic:notEmpty>
</center>	
</html:form>
