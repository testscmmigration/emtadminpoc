<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<fieldset class="fieldset1">
	<legend class="legend">
		Account Detail: 
	</legend>

	<table border='0' width='100%' cellpadding='1' cellspacing='10'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.accountId}" />
				</td>
				<logic:notEmpty name="bankAccountDetailForm" property="microDeposit">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.md.vldn.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.microDeposit.validationId}" />
				</td>
				</logic:notEmpty>
				<logic:empty name="bankAccountDetailForm" property="microDeposit">
				<td colspan="2"> &nbsp; </td>
				</logic:empty>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.number"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.accountNumberMask}" />
				</td>
				<logic:notEmpty name="bankAccountDetailForm" property="microDeposit">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.md.status"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.microDeposit.status.mdStatus}" />
					(<c:out value="${bankAccountDetailForm.microDeposit.status.mdStatusDesc}" />)
				</td>
				</logic:notEmpty>
				<logic:empty name="bankAccountDetailForm" property="microDeposit">
				<td colspan="2"> &nbsp; </td>
				</logic:empty>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.routing.number"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.routingNumber}" />
				</td>
				<logic:notEmpty name="bankAccountDetailForm" property="microDeposit">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.md.stat.date"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.microDeposit.statUpdateDate}" />
				</td>
				</logic:notEmpty>
				<logic:empty name="bankAccountDetailForm" property="microDeposit">
				<td colspan="2"> &nbsp; </td>
				</logic:empty>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.bank"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.financialInstitutionName}" />
				</td>
				<logic:notEmpty name="bankAccountDetailForm" property="microDeposit">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.vldn.count"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${bankAccountDetailForm.microDeposit.validationTryCount}" />
				</td>
				</logic:notEmpty>
				<logic:empty name="bankAccountDetailForm" property="microDeposit">
				<td colspan="2"> &nbsp; </td>
				</logic:empty>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.type"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT" colspan="2">
					<c:out value="${bankAccountDetailForm.accountTypeDesc}" />
				</td>
				<logic:present name="bankAccountDetailForm" property="microDeposit">
				<td nowrap="nowrap" class="TD-INPUT">
					<logic:equal name="validateMD" value="Y">
					<c:if test="${bankAccountDetailForm.microDeposit.status.mdStatus == 'ACW' || 
								  bankAccountDetailForm.microDeposit.status.mdStatus == 'EXP' || 
                				  bankAccountDetailForm.microDeposit.status.mdStatus == 'ACS' || 
  								  bankAccountDetailForm.microDeposit.status.mdStatus == 'MDF'}">
						<a href='../../validateMicroDeposit.do?accountId=<c:out value="${bankAccountDetailForm.accountId}"/>'>Validate Micro Deposit</a>
					</c:if>
					</logic:equal>
				</td>
				</logic:present>
				<logic:notPresent name="bankAccountDetailForm" property="microDeposit">
				<td> &nbsp; </td>
				</logic:notPresent>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.taint.text" /><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT" colspan="3"><font color='red'><b>
					<c:out value="${bankAccountDetailForm.accountTaintText}" /></b></font>
					<logic:equal name="addBlockedBank" value="Y">
					<c:if test="${bankAccountDetailForm.accountTaintText == 'Not Blocked'}">
					<c:if test="${bankAccountDetailForm.abaTaintText == 'Not Blocked'}">
						&nbsp; &nbsp; <a href='../../showBankBlockedList.do?bankFull=<c:out value="${bankAccountDetailForm.accountNumberMask}" />&bankAba=<c:out value="${bankAccountDetailForm.routingNumber}" />&submitAdd=add'>Block Account</a>			
					</c:if>					
					</c:if>					
					</logic:equal>			
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.aba.taint.text"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT" colspan="3"><font color='red'><b>
					<c:out value="${bankAccountDetailForm.abaTaintText}" /></b></font>
					<logic:equal name="addBlockedBank" value="Y">
					<c:if test="${bankAccountDetailForm.abaTaintText == 'Not Blocked'}">
						&nbsp; &nbsp; <a href='../../showBankBlockedList.do?bankFull=%20&bankAba=<c:out value="${bankAccountDetailForm.routingNumber}" />&submitAdd=add'>Block this ABA</a>
					</c:if>				
					</logic:equal>
					
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.status"/><fmt:message key="label.suffix"/>
				</td>
				<td align="left" colspan="3">
					<html:form action="/updateAccountStatus.do">
						<html:hidden name="bankAccountDetailForm" property="custId"/>
						<html:hidden name="bankAccountDetailForm" property="accountId" />
						<html:hidden name="bankAccountDetailForm" property="accountTypeCode" />
						<html:select name="bankAccountDetailForm" property="combinedStatusCode">
							<html:optionsCollection name="bankAccountDetailForm" property="accountStatusOptions"/>
						</html:select>
						<html:submit>Update Status</html:submit>
					</html:form>
				</td>
			</tr>
			<html:form action="addAccountComment">
				<html:hidden name="bankAccountDetailForm" property="custId"/>
				<html:hidden name="bankAccountDetailForm" property="accountId"/>
				<html:hidden name="bankAccountDetailForm" property="accountTypeCode"/>
			<tr>
				<td colspan="4" align="left">
					<html:select property="commentReasonCode">
						<html:optionsCollection name="bankAccountDetailForm" property="accountCommentReasons"/>
					</html:select>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="left">
					<html:textarea property="commentText" rows="4" cols="80" onkeydown="if (document.forms['addAccountCommentForm'].commentText.value.length > 255) alert('Comment cannot be over 255 characters!')"/>
					<html:submit>Add Comment</html:submit>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="left">
					<input type="button" name="exit" value="Return to Consumer Profile" onclick="parent.location='showCustomerProfile.do?custId=<c:out value="${bankAccountDetailForm.custId}"/>'"></input>
					&nbsp; &nbsp; &nbsp; <a href="javascript: window.history.go(-1);">Go back</a>
				</td>
			</tr>
			</html:form>
			<tr>
				<td colspan="4">
				<table width="100%">
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.user" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment.reason" /></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="comment" items="${bankAccountDetailForm.comments}">
							<tr>
								<td>
									<fmt:formatDate value="${comment.createDate}" pattern="MMM dd yy h:mm a z" />
								</td>
								<td>
									<c:out value="${comment.text}" />
								</td>
								<td>
									<c:out value="${comment.createdBy}" />
								</td>
								<td>
									<c:out value="${comment.reasonDescription}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>
