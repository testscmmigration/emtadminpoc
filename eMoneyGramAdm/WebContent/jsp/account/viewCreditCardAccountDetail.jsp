<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<fieldset class="fieldset1">
	<legend class="legend">
		Account Detail: 
	</legend>
	<table border='0' width='100%' cellpadding='1' cellspacing='10'>
		<tbody>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${creditCardAccountDetailForm.accountId}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<logic:notEqual name="debitCard" value="Y">
						<fmt:message key="label.credit.card.number"/><fmt:message key="label.suffix"/>
					</logic:notEqual>
					<logic:equal name="debitCard" value="Y">
						<fmt:message key="label.debit.card.number"/><fmt:message key="label.suffix"/>
					</logic:equal>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${creditCardAccountDetailForm.accountBinMask}" />******<c:out value="${creditCardAccountDetailForm.accountNumberMask}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.credit.card.expiration.date"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${creditCardAccountDetailForm.expirationMonth}/${creditCardAccountDetailForm.expirationYear}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.credit.card.issuer"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${creditCardAccountDetailForm.accountTypeDesc}" />
				</td>
			</tr>
			<tr>
				<html:form action="/updateAccountType.do">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.credit.card.type"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<c:out value="${creditCardAccountDetailForm.cardType}" />
					<html:hidden name="creditCardAccountDetailForm" property="custId"/>
					<html:hidden name="creditCardAccountDetailForm" property="accountId" />
					<html:hidden name="creditCardAccountDetailForm" property="accountTypeCode" />

					<logic:equal name="creditCardAccountDetailForm" property="statusCode" value="ACT">
					<logic:equal name="creditCardAccountDetailForm" property="cardType" value="Unknown">
					<logic:equal name="updateAcctType" value="Y">
						 &nbsp; &nbsp; &nbsp;
						<html:submit property="submitCC" value="Change to Credit Card" /> &nbsp;
						<html:submit property="submitDC" value="Change to Debit Card" />
					</logic:equal>
					</logic:equal>
					</logic:equal>
				</td>
				</html:form>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.account.taint.text"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT"><font color='red'><b>
					<c:out value="${creditCardAccountDetailForm.accountTaintText}" /></b></font>
					
					<logic:equal name="addBlockedCc" value="Y">
					<c:if test="${creditCardAccountDetailForm.accountTaintText == 'Not Blocked'}">
					    &nbsp; &nbsp; <a href='../../showCCBlockedList.do?ccAccountId=<c:out value="${creditCardAccountDetailForm.accountId}"/>&bin=<c:out value="${creditCardAccountDetailForm.binNumber}"/>&mask=<c:out value="${creditCardAccountDetailForm.accountNumberMask}"/>&submitAdd=add'>Block this Account</a>
					</c:if>
					</logic:equal>
					
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.bin.taint.text"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT"><font color='red'><b>
					<c:out value="${creditCardAccountDetailForm.binTaintText}" /></b></font>

					<logic:equal name="addBlockedBin" value="Y">
					<c:if test="${creditCardAccountDetailForm.binTaintText == 'Not Blocked'}">
						&nbsp; &nbsp; <a href='../../showBinBlockedList.do?binText=<c:out value="${creditCardAccountDetailForm.accountBinMask}" />&submitAdd=add'>Block this BIN</a>
					</c:if>
					</logic:equal>
					
				</td>
			</tr>
			<tr>
				<html:form action="/updateAccountStatus.do">
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.status"/><fmt:message key="label.suffix"/>
				</td>
				<td align="left">
						<html:hidden name="creditCardAccountDetailForm" property="custId"/>
						<html:hidden name="creditCardAccountDetailForm" property="accountId" />
						<html:hidden name="creditCardAccountDetailForm" property="accountTypeCode" />
						<html:select name="creditCardAccountDetailForm" property="combinedStatusCode">
							<html:optionsCollection name="creditCardAccountDetailForm" property="accountStatusOptions"/>
						</html:select>
						<html:submit>Update Status</html:submit>
				</td>
				</html:form>
			</tr>
			<html:form action="addAccountComment">
				<html:hidden name="creditCardAccountDetailForm" property="custId"/>
				<html:hidden name="creditCardAccountDetailForm" property="accountId"/>
				<html:hidden name="creditCardAccountDetailForm" property="accountTypeCode"/>
				<tr>
					<td colspan="2" align="left">
						<html:select property="commentReasonCode">
							<html:optionsCollection name="creditCardAccountDetailForm" property="accountCommentReasons"/>
						</html:select>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<html:textarea property="commentText" rows="4" cols="80" onkeydown="if (document.forms['addAccountCommentForm'].commentText.value.length > 255) alert('Comment cannot be over 255 characters!')"/>
						<html:submit>Add Comment</html:submit>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<input type="button" name="exit" value="Return to Consumer Profile" onclick="parent.location='showCustomerProfile.do?custId=<c:out value="${creditCardAccountDetailForm.custId}"/>'"></input>
						&nbsp; &nbsp; &nbsp; <a href="javascript: window.history.go(-1);">Go back</a>
					</td>
				</tr>
			</html:form>
			<tr>
				<td colspan="2">
				<table width="100%">
					<thead>
						<tr>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.create.user" /></td>
							<td class='TD-HEADER-SMALL'><fmt:message key="label.comment.reason" /></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="comment" items="${creditCardAccountDetailForm.comments}">
							<tr>
								<td>
									<fmt:formatDate value="${comment.createDate}" pattern="MMM dd yy h:mm a z" />
								</td>
								<td>
									<c:out value="${comment.formattedText}" escapeXml="false"/>
								</td>
								<td>
									<c:out value="${comment.createdBy}" />
								</td>
								<td>
									<c:out value="${comment.reasonDescription}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>
