<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

<html:form action="/tranConfirm" focus="submitOk">
<html:hidden property="tranId" />
<html:hidden property="tranType" />
<html:hidden property="custId" />

<html:hidden property="submitRetrievalRequest" />
<html:hidden property="submitApprove" />
<html:hidden property="submitApproveProcess" />
<html:hidden property="submitUndoApprove" />
<html:hidden property="submitUndoDeny" />
<html:hidden property="submitProcess" />
<html:hidden property="submitRecAchAutoRef" />
<html:hidden property="submitRecAchCxlRef" />
<html:hidden property="submitRecCcAutoRef" />
<html:hidden property="submitRecCcCxlRef" />
<html:hidden property="submitManWriteOffLoss" />
<html:hidden property="submitRecordMgCancelRefund" />
<html:hidden property="submitRecordAchChargeback" />
<html:hidden property="submitRecordCcChargeback" />
<html:hidden property="submitCancelAchWaiting" />
<html:hidden property="submitRecAchRefundReq" />
<html:hidden property="submitRecCcRefundReq" />
<html:hidden property="submitAutoWriteOffLoss" />
<html:hidden property="submitAutoChargebackAndWriteOffLoss" />
<html:hidden property="submitRecoveryOfLoss" />
<html:hidden property="submitEsMGSend" />
<html:hidden property="submitEsMGSendCancel" />
<html:hidden property="submitRecordTranError" />
<html:hidden property="submitReleaseTransaction" />
<html:hidden property="submitCancelBankRqstRefund" />
<html:hidden property="submitBankRqstRefund" />
<html:hidden property="submitAutoCancelBankRqstRefund" />
<html:hidden property="submitBankRepresentRejDebit" />
<html:hidden property="submitBankRejectedDebit" />
<html:hidden property="submitBankRecordVoid" />
<html:hidden property="submitBankIssueRefund" />
<html:hidden property="submitBankWriteOffLoss" />

<html:hidden property="tranStatusCode" />
<html:hidden property="tranStatusDesc" />
<html:hidden property="tranSubStatusCode" />
<html:hidden property="tranSubStatusDesc" />
<html:hidden property="tranOwnerId" />
<html:hidden property="tranSenderName" />
<html:hidden property="tranReceiverName" />
<html:hidden property="tranFaceAmt" />
<html:hidden property="tranSendFee" />
<html:hidden property="tranReturnFee" />
<html:hidden property="tranSendAmt" />
<html:hidden property="tranType" />
<html:hidden property="tranTypeDesc" />
<html:hidden property="tranSendCountry" />
<html:hidden property="tranRecvCountry" />
<html:hidden property="tranPrimAcctType" />
<html:hidden property="tranPrimAcctMask" />
<html:hidden property="tranSecAcctType" />
<html:hidden property="tranSecAcctMask" />
<html:hidden property="tranCustIP" />
<html:hidden property="sendCustAcctId" />
<html:hidden property="sendCustBkupAcctId" />
<html:hidden property="tranCcAuthCode" />
<html:hidden property="tranSndMsg1Text" />
<html:hidden property="tranSndMsg2Text" />
<html:hidden property="esMGSendFlag" />
<html:hidden property="transScores" />
<html:hidden property="sysAutoRsltCode" />
<html:hidden property="showScore" />

<center>

<h3><fmt:message key="label.transaction.action.confirmation"/></h3>


	<logic:equal name="tranConfirmForm" property="showWarning" value="Y">
	<logic:equal name="tranConfirmForm" property="esMGSendFlag" value="N">
		<h3>Warning: Economy Service Transaction.  ACH Funding in progress, please wait to Process until the ACH has cleared.</h3>
	</logic:equal>
	</logic:equal>

<table>
	<tr><td valign='top'><font color='#0000FF'><b>Action Description: </b></font></td>
	<td class='td-small'>
<% out.println((String)request.getSession().getAttribute("msgText")); %>
</td></tr>
</table><br />
<bean:write name="tranConfirmForm" property="actionDesc" />
<br />
<logic:equal name="tranConfirmForm" property="showScore" value="Y">
<logic:notEqual name="tranConfirmForm" property="sysAutoRsltCode" value="">
<table>
<tr><td colspan="2">
<h3>Alert: The system has automatically generated a score for this transaction.</h3>
<br />
The automate transaction score is
<b><font color="red"><bean:write name="tranConfirmForm" property="transScores" /></font></b>
and the automatic result code is
<b><font color="red"><bean:write name="tranConfirmForm" property="sysAutoRsltDesc" /></font></b>. &nbsp; &nbsp;
Please review the transaction score detail (see below) and select your recommended result
code and enter your review comment why you make this decision.</td></tr>
<tr><td colspan="2"> &nbsp; </td></tr>
<tr><td nowrap='nowrap' class="td-label-small">Your Recommended Result Code: </td>
<td class="td-small">
<html:select property="scoreRvwCode" size="1">
	<html:option value="">Select an option</html:option>												
	<html:option value="APRV">Approve</html:option>
	<html:option value="REVW">Review</html:option>
	<html:option value="REJ">Reject</html:option>
</html:select> &nbsp; &nbsp; (Automatic result code: 
<b><font color="red"><bean:write name="tranConfirmForm" property="sysAutoRsltDesc" /></font></b>)
</td></tr>
<tr valign="top"><td nowrap='nowrap' class="td-label-small">Your Review Comment: </td>
<td class="td-small">
	<html:textarea property="tranScoreCmntText" rows="4" cols="80" 
				onkeypress="textCounter(this, 255);" />
</td></tr>
</table>
</logic:notEqual>
</logic:equal>
Are you sure?	
<br />
<div id="formButtons">
	<html:submit property="submitOk"  value="Yes" 
		onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
 		&nbsp; &nbsp; &nbsp;
	<html:submit property="submitCancel"  value="No" />
</div >
<div id="processing"  style="visibility: hidden">
	<table>
		<tr>
			<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
		</tr>
	</table>
</div >					

<br />
<html:hidden property="releaseOwnership" />

<logic:equal name="tranConfirmForm" property="showScore" value="Y">
<h3><fmt:message key="label.transaction.score.detail"/></h3>

<logic:notEmpty name="tran">
<table border='0' cellpadding='1' cellspacing='1'>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.id"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
		    <bean:write name="tran" property="emgTranId" />
			<html:hidden property="tranId"/></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.status"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="status.statusCode" /> (
			<bean:write name="tran" property="tranStatDesc" />)</td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.sender"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'><bean:write name="tran" property="sndCustId" /></td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.funding.status"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="status.subStatusCode" /> (
			<bean:write name="tran" property="tranSubStatDesc" />)</td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.transaction.type"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="emgTranTypeCode" 
			/> (<bean:write name="tran" property="emgTranTypeDesc" />)</td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.send.amount"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="tran" property="sndTotAmt" /></td>
	</tr>
	<logic:present name="tran" property="tranScore">
	<bean:define id="score" name="tran" property="tranScore" />
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.id"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgId" /> (
			<a href='../../showScoreConfig.do?submitCnfg=go&scoreCnfgId=<bean:write name="score" property="scoreCnfgId" />'>
			<fmt:message key="label.see.config.detail"/></a>)</td>
		<td> &nbsp; </td>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.ver.nbr"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgVerNbr" /></td>
	</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.config.stat.code" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreCnfgStatCode" /></td>
		<td colspan='3'> &nbsp; </td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.tran.score.nbr"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="tranScoreNbr" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.rslt.code" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="sysAutoRsltCode" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.review.code"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="scoreRvwCode" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.review.cmnt.text" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="tranScoreCmntText" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.date"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="createDate" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.create.userid" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="createUserid" /></td>
	</tr>
	<tr>
 		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.date"/>: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="lastUpdateDate" /></td>
		<td> &nbsp; </td>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.last.update.userid" />: </td>
		<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="score" property="lastUpdateUserid" /></td>
	</tr>
	</logic:present>
</table>

<logic:present name="tran" property="tranScore">
<bean:define id="catScoreList" name="score" property="tranScoreCatList" />
<logic:notEmpty name="catScoreList">
<br />
<table border='1' cellpadding='1' cellspacing='1'>
	<%String dispClass = "TD-SMALL";%>
	<tr>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.code" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.name" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.desc" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.list.type" /></TD>		
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.data.type" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.incl.flag" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.cat.wgt.nbr" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.raw.score" /></TD>
		<td class='TD-HEADER-SMALL' align='center'><fmt:message key="label.wgt.score" /></TD>
	</tr>
	<logic:iterate id="catScore" name="catScoreList">
	<tr>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="scoreCatCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="scoreCatName"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="scoreCatDesc"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="listTypeCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="dataTypeCode"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='left' valign='top'>
			<bean:write name="catScore" property="inclFlag"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="catWgtNbr"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="rawScoreNbr"/></td>
		<td nowrap='nowrap' class='<%=dispClass%>' align='right' valign='top'>
			<bean:write name="catScore" property="wgtScoreNbr"/></td>
	<tr>
	<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
		else dispClass = "TD-SHADED-SMALL"; %>
	</logic:iterate>
</table>
</logic:notEmpty>
</logic:present>
</logic:notEmpty>
</logic:equal>

</center>
</html:form>
