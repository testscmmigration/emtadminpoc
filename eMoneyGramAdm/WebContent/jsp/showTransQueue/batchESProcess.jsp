<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<center>
<span class="error">
	<html:errors property='<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>' />
</span>

</center>

<html:form action="/batchESProcess.do" focus="submitConfirm">
<html:hidden property="actionType" />
<html:hidden property="done" />

<h3>Batch Economy Service Transaction Processing</h3>

<center>
<font size="3" color="black"><b>
Economy Service Transaction Batch Action: </b></font>
<font size="3" color="red"><b>
<bean:write name="batchESProcessForm" property="actionType" /></b></font>

<br /><br />

<logic:notEqual name="batchESProcessForm" property="done" value="Y">
	<font size="3" color="red"><b>
	Please confirm the list of transaction ids and the action requested.  Press the Confirm 
	button to go ahead the process or press the Cancel button to return to transaction queue screen
	</b></font>
	<br /><br />
</logic:notEqual>

<div id="formButtons">
	<logic:notEqual name="batchESProcessForm" property="done" value="Y">
		<input type='submit' name='submitConfirm' value='Confirm' 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		&nbsp; &nbsp;
		<input type='submit' name='submitCancel' value='Cancel' 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
	</logic:notEqual>
	<logic:equal name="batchESProcessForm" property="done" value="Y">
		<input type='submit' name='submitReturn' value='Return to Tran Queue' 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
	</logic:equal>
</div>
	
<div id="processing" style="visibility: hidden">
	<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
</div >
	
<table>
	<tbody>
		<tr>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.id"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.owned.id"/></TD>		
			<logic:equal name="batchESProcessForm" property="done" value="Y">
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.status"/></TD>		
				<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.funded.status"/></TD>		
			</logic:equal>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sender"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.receiver"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.dest"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.face.amount"/></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.total.amount"/></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.create.date"/></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.result.code"/></TD>		
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.msg"/></TD>		
		</tr>

		<% String dispClass = "td-small"; %>
		
		<logic:iterate id="view" name="batchTrans">
		<tr>
			<logic:equal name="batchESProcessForm" property="done" value="Y">
				<td><a href='../../transactionDetail.do?tranId=<bean:write 
					name="view" property="id"/>'>
					<bean:write name="view" property="id"/></a></td>
			</logic:equal>
			<logic:notEqual name="batchESProcessForm" property="done" value="Y">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="view" property="id"/></td>
			</logic:notEqual>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="owner" /></td>
			<logic:equal name="batchESProcessForm" property="done" value="Y">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="view" property="status"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<bean:write name="view" property="subStatus"/></td>
			</logic:equal>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="sndName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="rcvName" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="rcvCntryCd" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
				<bean:write name="view" property="faceAmt"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='right'>
				<bean:write name="view" property="totalAmt"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="createDate"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="transScore"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<bean:write name="view" property="rsltCode"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>'>
				<b><font color='red'><bean:write name="view" property="msg"/></font></b></td>
		</tr>
		<%
			if (dispClass.equals("TD-SHADED-SMALL")) {
				dispClass = "TD-SMALL"; 
			} else {
			dispClass = "TD-SHADED-SMALL";
			}
		%>
		</logic:iterate>
	</tbody>			      
</table>

</center>
</html:form>
