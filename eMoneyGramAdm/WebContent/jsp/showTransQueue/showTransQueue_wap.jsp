<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/blitzer/jquery-ui.css" />

<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>
<script src="../../Display/jscript/setOtherRangeValue.js" language="JavaScript"></script>
<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>

<CENTER>
<span class="error">
	<html:errors />
</span>

</CENTER>

<html:hidden property="partnerSiteId"/>

<h3><fmt:message key="label.transaction.queue.list"/></h3>

<table><tr>
	<td><b>Approve:</b></td>
	<logic:equal name="showTransQueueForm" property="appP2PCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>P2P
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="appP2PCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranApprovalP2P.do?partnerSiteId=WAP' class='active'>P2P</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="appP2PCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; </td>

	<logic:equal name="showTransQueueForm" property="appDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>DS
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="appDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href="../../tranApprovalDS.do?partnerSiteId=WAP" id="tranApprovalDS.do">DS</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="appDSCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; &nbsp; &nbsp; <b>Process:</b></td>

	<logic:equal name="showTransQueueForm" property="procP2PCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>P2P
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="procP2PCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranProcessP2P.do?partnerSiteId=WAP'>P2P</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="procP2PCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; </td>

	<logic:equal name="showTransQueueForm" property="procDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>DS
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="procDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranProcessDS.do?partnerSiteId=WAP' id="tranProcessDS">DS</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="procDSCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; &nbsp; &nbsp; <b>Send:</b></td>

	<logic:equal name="showTransQueueForm" property="sendDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>DS
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="sendDSCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranSendDS.do?partnerSiteId=WAP' id="tranSendDS">DS</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="sendDSCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>

	<logic:equal name="showTransQueueForm" property="inProgressCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>In-Progress
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="inProgressCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranInProgress.do?partnerSiteId=WAP'>In-Progress</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="inProgressCount" /> )</font></td>
	</logic:notEqual>

	<td nowrap='nowrap'> &nbsp; </td>

	<logic:equal name="showTransQueueForm" property="exceptionCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>Exceptions
		<font color='red'> ( 0 )</font></td>
	</logic:equal>
	<logic:notEqual name="showTransQueueForm" property="exceptionCount" value="0">
		<td nowrap='nowrap' class='TD-HEADER-SMALL'>
		<a href='../../tranException.do?partnerSiteId=WAP'>Exceptions</a><font color='red'> (
		<bean:write name="showTransQueueForm" property="exceptionCount" /> )</font></td>
	</logic:notEqual>

</tr>
</table>


<table>
	<tr><td nowrap='nowrap' class='TD-SMALL'>

	<b><fmt:message key="label.date.begin"/><fmt:message key="label.suffix"/></b>&nbsp;
	<html:text size="10" property="beginDateText" onchange="setOtherRangeValue(this, showTransQueueForm.endDateText);" />
	<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showTransQueueForm.beginDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>

	<b><fmt:message key="label.date.end"/><fmt:message key="label.suffix"/></b>&nbsp;
	<html:text size="10" property="endDateText" onchange="setOtherRangeValue(this, showTransQueueForm.beginDateText);" />
 	<img border="0" src="../../images/calendar.gif" onclick="show_calendar('showTransQueueForm.endDateText');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>

	<b><fmt:message key="label.trans.status"
		/>: </b><html:select name="showTransQueueForm" property="status" styleId="status" size="1">
		<html:option value="All"><fmt:message key="option.all"/></html:option>
		<html:options collection="tranStatuses" property="tranStatCode" labelProperty="tranStatDesc" />
	</html:select>

	<b><fmt:message key="label.trans.funded.status"
		/>: </b><html:select name="showTransQueueForm" property="fundStatus" styleId="fundStatus" size="1">
		<html:option value="All"><fmt:message key="option.all"/></html:option>
		<html:options collection="tranSubStatuses" property="tranSubStatCode" labelProperty="tranSubStatDesc" />
		</html:select>
	</td></tr>
	<tr><td nowrap='nowrap' class='TD-SMALL'>
	<b><fmt:message key="label.trans.type"
		/>: </b><html:select name="showTransQueueForm" property="type" styleId="type" size="1">
		<html:option value="All"><fmt:message key="option.all"/></html:option>
		<html:options collection="tranTypes" property="emgTranTypeCode" labelProperty="emgTranTypeDesc" />
	</html:select> &nbsp;

	<b><fmt:message key="label.owned.by"
	/>: </b><html:select name="showTransQueueForm" property="ownedBy" size="1">
		<html:option value="All"><fmt:message key="option.all"/></html:option>
		<html:option value="None"><fmt:message key="option.none"/></html:option>
		<html:options collection="users" property="userId"/>
	</html:select> &nbsp;

	<b><fmt:message key="label.sort.by"
	/>: </b><html:select name="showTransQueueForm" property="sortBy" size="1">
		<html:option value="transUserId"><fmt:message key="label.owner"/></html:option>
		<html:option value="transId"><fmt:message key="label.trans.id"/></html:option>
		<html:option value="transSenderName"><fmt:message key="label.sender"/></html:option>
		<html:option value="transReceiver"><fmt:message key="label.receiver"/></html:option>
		<html:option value="transCntryCd"><fmt:message key="label.country.cd"/></html:option>
		<html:option value="transStatus"><fmt:message key="label.trans.status"/></html:option>
		<html:option value="transSubStatus"><fmt:message key="label.trans.sub.status"/></html:option>
		<html:option value="transType"><fmt:message key="label.trans.type"/></html:option>
		<html:option value="transAmount"><fmt:message key="label.amount"/></html:option>
		<html:option value="transStatusDate"><fmt:message key="label.status.time"/></html:option>
		<html:option value="transScores"><fmt:message key="label.trans.scores"/></html:option>
	</html:select>

	<html:select name="showTransQueueForm" property="sortSeq" size="1">
		<html:option value="A"><fmt:message key="option.ascending"/></html:option>
		<html:option value="D"><fmt:message key="option.descending"/></html:option>
	</html:select>

	</tr>
	<tr><td nowrap='nowrap' class='TD-SMALL'>
		<b><fmt:message key="label.use.score"/>: </b>
			<html:radio property="useScore" value="Y">Yes</html:radio> &nbsp;
			<html:radio property="useScore" value="N">No</html:radio> &nbsp; &nbsp; <b>If Yes, &nbsp;
		   <fmt:message key="label.low.score"/>: </b><html:text property="lowScore" size="4" /> &nbsp; &nbsp;
		<b><fmt:message key="label.high.score"/>: </b><html:text property="highScore" size="4" /> &nbsp; &nbsp;
	</tr>

	<%String dispClass = "TD-SHADED-SMALL";
	  boolean haveES = false;
	  java.util.Iterator iter = ((java.util.List) request.getSession().getAttribute("queueList")).iterator();
	  while (iter.hasNext()) {
	  	if (!((emgadm.model.TranQueueView)iter.next()).getEsSendable().equals("")) {
	  		haveES = true;
	  		break;
	  	}
	  }
	  boolean warning = false; %>

	<tr><td nowrap='nowrap' class='TD-SMALL'>
		<div id="formButtons">
			<b><font color="#CF002F">Total records displayed :
			<bean:write name="showTransQueueForm" property="recordCount" /></font><b>
			&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
			<input type='submit' class='transbtn2' name='submitRefresh' id="submitRefresh" value='Refresh Queue'
				onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
		</div>

		<div id="processing" style="visibility: hidden">
			<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
		</div >

	</td></tr>
</table>

	<logic:notEmpty name="queueList">

	<table>
		<tbody>
			<tr>
			<% if (haveES) { %>
				<td nowrap='nowrap' class='TD-SMALL' colspan='6'>
					<logic:equal name="canProcess" value="Y">
					<logic:equal name="batchProcess" value="Y">
					<b>For checked transactions ==> &nbsp; </b>
					<input type='submit' class='transbtn1' name='submitESSend' value='Send MoneyGram' />
					&nbsp; &nbsp;
					<input type='submit' class='transbtn1' name='submitESDeny' value='ACH Return and Cancel' />
					</logic:equal>
					</logic:equal>
				<td  nowrap='nowrap' class='TD-SMALL' colspan='6' align='right'><a href="../../ExcelReport.emt?id=queueList" target="excel">
					<fmt:message key="msg.view.or.download.excel" /></a></td>
			<% } %>
			<% if (!haveES) { %>
				<td  nowrap='nowrap' class='TD-SMALL' colspan='11' align='right'><a href="../../ExcelReport.emt?id=queueList" target="excel">
					<fmt:message key="msg.view.or.download.excel" /></a></td>
			<% } %>
			</tr>
			<tr>
			<% if (haveES) { %>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'>Sendable</td>
			<% } %>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.owned.by.time.action"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.program"/></td>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.id"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.sender"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.receiver"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.dest"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.status"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.funded.status"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.type"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.amount"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.status.time"/></TD>
			<td nowrap='nowrap' class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></TD>
			</tr>

			<logic:iterate id="trans" name="queueList">

			<logic:equal name="trans" property="esSendable" value="No">
				<% dispClass = "TD-BG-SMALL";
				   warning = true; %>
			</logic:equal>

			<tr>
				<% if (haveES) { %>
				<td nowrap='nowrap' class='<%=dispClass%>' align='center'>
				<logic:equal name="canProcess" value="Y">
				<logic:equal name="batchProcess" value="Y">
				<logic:equal name="trans" property="esSendable" value="Yes">
					<html:multibox property="selTrans">
					<bean:write name="trans" property="transId"/>
					</html:multibox>
				</logic:equal>
				</logic:equal>
				</logic:equal>
				<logic:equal name="trans" property="esSendable" value="No">
					<bean:write name="trans" property="esSendable"/>
				</logic:equal>
				</td>
				<% } %>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<logic:notEqual name="trans" property="transUserId" value="">
						<a href='../../showUser.do?userId=<c:out
						value="${trans.transUserId}"/>'
						onClick="this.href='javascript:showUserDetail(\'showUser.do?userId=<c:out
						value="${trans.transUserId}"/>\')'">
						<b><bean:write name="trans" property="transUserId" /></b></a> -
					</logic:notEqual>
					<logic:notEqual name="trans" property="transOwnershipDate" value="">
				    	<bean:write name="trans" property="transOwnershipDate" /> -
					</logic:notEqual>
				    <logic:equal name="trans" property="transCommand" value="Take Ownership">
				    	<a href='../../transOwnership.do?partnerSiteId=${fn:escapeXml(partnerSiteId)}&action=TakeOwnership&id=<bean:write filter="false" name="trans" property="transId"/>'
				    	><bean:write name="trans" property="transCommand" /></a>
				    </logic:equal>
				    <logic:equal name="trans" property="transCommand" value="Take Over">
				    	<a href='../../transOwnership.do?partnerSiteId=${fn:escapeXml(partnerSiteId)}&action=TakeOver&id=<bean:write filter="false" name="trans" property="transId"/>&oldUserId=<bean:write name="trans" property="transUserId" />'
				    	><bean:write name="trans" property="transCommand" /></a>
				    </logic:equal>
				    <logic:equal name="trans" property="transCommand" value="Release">
				    	<a href='../../transOwnership.do?partnerSiteId=${fn:escapeXml(partnerSiteId)}&action=Release&id=<bean:write filter="false" name="trans" property="transId"/>'
				    	><bean:write name="trans" property="transCommand" /></a>
				    </logic:equal>
				</td>

				<td><bean:write name="trans" property="partnerSiteId"/></td>

				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../transactionDetail.do
						?tranId=<bean:write filter="false" name="trans" property="transId"/>
						&partnerSiteId=<bean:write filter="false" name="trans" property="partnerSiteId"/>
					'>
					<logic:equal name="trans" property="overThrldWarnAmt" value="false">
					<bean:write filter="false" name="trans" property="transId"/>
					</logic:equal>
					<logic:equal name="trans" property="overThrldWarnAmt" value="true">
					<span style="COLOR: red;"><bean:write filter="false" name="trans" property="transId"/></span>
					</logic:equal>
					</a></td>
				
				<td nowrap='nowrap' class='<%=dispClass%>'><a
						href='../../showCustomerProfile.do
						?custId=<bean:write filter="false" name="trans" property="transCustId"/>
						&partnerSiteId=<bean:write filter="false" name="trans" property="partnerSiteId"/>'>
						<bean:write filter="false" name="trans" property="transSenderName" /></a>
				</td>
					
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<logic:equal name="trans" property="transType" value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
				    	<a href='../../showBillerInfo.do?agentId=<bean:write filter="false" name="trans" property="transReceiver"/>'
				    	><bean:write name="trans" property="transReceiver" /></a>
				    </logic:equal>
					<logic:notEqual name="trans" property="transType" value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
						<bean:write filter="false" name="trans" property="transReceiver"/>
				    </logic:notEqual>
				</td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="trans" property="transCntryCd"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="trans" property="transStatus"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="trans" property="transSubStatus"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="trans" property="transType"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>' align="right"><bean:write filter="false" name="trans" property="transAmount"/></td>
				<td nowrap='nowrap' class='<%=dispClass%>'><bean:write filter="false" name="trans" property="transStatusDate"/></td>
				<logic:equal name="trans" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="trans" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q
					&tranId=<bean:write filter="false" name="trans" property="transId"/>
					&partnerSiteId=<bean:write filter="false" name="trans" property="partnerSiteId"/>'>
					<bean:write filter="false" name="trans" property="transScores"/>-<bean:write filter="false" name="trans" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
				</tr>
			<%  if (haveES) {
					dispClass = "TD-SHADED-SMALL";
				} else if (dispClass.equals("TD-SHADED-SMALL")) {
					dispClass = "TD-SMALL";
				} else {
					dispClass = "TD-SHADED-SMALL";
				}  %>
			</logic:iterate>
		</tbody>
	</table>
			<% if (warning) { %>
					<br />
					<h4>Warning:  Items marked Sendable=No should not be processed.  These item are waiting for completion of the ACH clearing process .</h4>
			<%	} %>
	</logic:notEmpty>
