<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<h3>
	<html:messages id="message" property='<%=org.apache.struts.action.ActionMessages.GLOBAL_MESSAGE%>' message="true">
		<c:out value="${message}"/>
	</html:messages>
</h3>

<h3>Verified By Visa Detail Information</h3>

<html:form action="/showVBV">
<html:hidden property="tranId" />

<table border='0' cellpadding='1' cellspacing='1'>
	<tr>
		<td class='td-header' nowrap='nowrap'><fmt:message key='label.emg.tran.id' />: </td>
		<td><c:out value='${showVBVForm.tranId}' /></td>
	</tr>
</table>

<br />
<center>
<logic:empty name="vbvList">
<h4>No Verified By Visa information found for the transaction.</h4>
</logic:empty>

<logic:notEmpty name="vbvList">

<table border='0' cellpadding='1' cellspacing='1'>

<logic:iterate id="vbv" name="vbvList">

	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.cyber.source.tran.id' />: </td>
		<td><bean:write name="vbv" property="csTranId" /></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.auth.cmrc.code' />: </td>
		<td><bean:write name="vbv" property="authCmrcCode" /></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.eci.raw.code' />: </td>
		<td><bean:write name="vbv" property="eciRawCode" /></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.vbv.tran.id' />: </td>
		<td><bean:write name="vbv" property="vbvTranId" /></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.proof.xml.text' />: </td>
		<td><textarea readonly rows='10' cols='80'>
			<bean:write name="vbv" property="proofXmlText" /></textarea></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.cyber.source.request.id' />: </td>
		<td><bean:write name="vbv" property="csRqstId" /></td>
	</tr>
	<tr>
		<td class='td-header' nowrap='nowrap' align='right'><fmt:message key='label.create.date' />: </td>
		<td><bean:write name="vbv" property="createDate" /></td>
	</tr>
	<tr><td colspan='2'><hr /></td></tr>
</logic:iterate>
</table>

</logic:notEmpty>
<br /><br />
<a href="../../transactionDetail.do?tranId=<c:out value='${showVBVForm.tranId}' />">Return 
	to Transaction Detail</a>
</center>

</html:form>
