<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script src="../../Display/jscript/showUser.js" language="JavaScript"></script>
<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script src="../../Display/jscript/datePicker.js" language="JavaScript"></script>

<center>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
</span>

</center>
<h3><fmt:message key="label.chargeback.tracking.detail"/></h3>

<center>

<html:form type="emgadm.transqueue.CcChargebackTrackingForm" name="ccChargebackTrackingForm" action="/ccChargebackTracking.do">

<%String dispClass = "TD-SMALL";%>	
<fieldset>
	<legend class="TD-SMALL"> 
		<b>Transaction and Customer Details</b>
		
	</legend>
	<table align="center" border="0" cellpadding="0" cellspacing="4" width="98%">

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Tran ID:</b>&nbsp;
			</td>
	
			<td class="TD-SMALL-LEFT">
				<a href='../../transactionDetail.do?tranId=<bean:write name="tran" property="emgTranId" />'><bean:write name="tran" property="emgTranId" /></a>
				&nbsp;(<bean:write name="tran" property="emgTranTypeCode" /> - <bean:write name="tran" property="emgTranTypeDesc" />)		
	
	
			</td>
			
			<td class="TD-SMALL-RIGHT" >
				<b>Ref Number:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="tran" property="lgcyRefNbr" />
			</td>
			
			<td class="TD-SMALL-RIGHT">
				<b>Tran Create Date/Time:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="sndTranDate" />
			</td>
		</tr>
				<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Transaction IP:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="tran" property="sndCustIPAddrId" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b><fmt:message key="label.report.adhoc.senderip"/>:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="profile" property="createIpAddrId" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>User who Approved:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="approvedUser" />
			</td>

		</tr>
		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Tran Total:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				$<bean:write format="##,##0.00" filter="false" name="tran" property="sndTotAmt"/>
			</td>
		
			<td class="TD-SMALL-RIGHT">
				<b>Primary Funding:</b>&nbsp;
			</td>
			<logic:match name="tran" property="sndAcctTypeCode" value="CC" location="start"> 
				<td class="TD-SMALL-LEFT">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<bean:write name="tran" property="sndCustAcctId" />&custId=<bean:write name="tran" property="sndCustId" />'><bean:write name="tran" property="sndCustAcctId" /></a> (<bean:write name="tran" property="sndAcctTypeCode" />)
				</td>
			</logic:match>
			<logic:match name="tran" property="sndAcctTypeCode" value="BANK" location="start"> 
				<td class="TD-SMALL-LEFT">
					<a href='../../viewBankAccountDetail.do?accountId=<bean:write name="tran" property="sndCustAcctId" />&custId=<bean:write name="tran" property="sndCustId" />'><bean:write name="tran" property="sndCustAcctId" /></a> (<bean:write name="tran" property="sndAcctTypeCode" />)
				</td>
			</logic:match>

			<td class="TD-SMALL-RIGHT">
				<b>Secondary Funding:</b>&nbsp;
			</td>
			
			<td class="TD-SMALL-LEFT">
			<logic:notEqual name="tran" property="sndCustBkupAcctId" value="0"> 	
				<logic:match name="tran" property="sndBkupAcctTypeCode" value="CC" location="start"> 
						<a href='../../viewCreditCardAccountDetail.do?accountId=<bean:write name="tran" property="sndCustBkupAcctId" />&custId=<bean:write name="tran" property="sndCustId" />'><bean:write name="tran" property="sndCustBkupAcctId" /></a> (<bean:write name="tran" property="sndBkupAcctTypeCode" />)
				</logic:match>
				<logic:match name="tran" property="sndBkupAcctTypeCode" value="BANK" location="start"> 
						<a href='../../viewBankAccountDetail.do?accountId=<bean:write name="tran" property="sndCustBkupAcctId" />&custId=<bean:write name="tran" property="sndCustId" />'><bean:write name="tran" property="sndCustBkupAcctId" /></a> (<bean:write name="tran" property="sndBkupAcctTypeCode" />)
				</logic:match>
			</logic:notEqual>
			<logic:equal name="tran" property="sndCustBkupAcctId" value="0"> 
				( None )
			</logic:equal>

			</td>
			
			<td class="TD-SMALL-RIGHT">
				
			</td>
			<td class="TD-SMALL-LEFT">
				
			</td>

		</tr>
		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Intended Dest. :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<logic:notEqual name="tran" property="rcvCustAddrStateName" value=""> 
					<bean:write name="tran" property="rcvCustAddrStateName" />,&nbsp;
				</logic:notEqual> 
					
				<bean:write name="tran" property="rcvISOCntryCode" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Actual Dest. :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<logic:notEqual name="tran" property="rcvAgtState" value=""> 
					<logic:equal name="tran" property="rcvStateMatchesSendState" value="true"> 
							<bean:write name="tran" property="rcvAgtState" />,&nbsp;
					</logic:equal> 
					<logic:equal name="tran" property="rcvStateMatchesSendState" value="false"> 
						<font color='red'><b>
							<bean:write name="tran" property="rcvAgtState" />,&nbsp;
						</b></font>
					</logic:equal> 
				</logic:notEqual> 					
				<bean:write name="tran" property="rcvAgtCntry" />
			</td>
			<td class="TD-SMALL-RIGHT">
				<b>Receive Date:</b>&nbsp;				
			</td>
			<td class="TD-SMALL-LEFT">
				 <fmt:formatDate value="${tran.rcvDate}" pattern="dd/MMM/yyyy" />
			</td>
		</tr>		
		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Sender Name:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="profile" property="firstName" />&nbsp;
				<logic:notEmpty name="profile" property="middleName">
					<bean:write name="profile" property="middleName" />&nbsp;
				</logic:notEmpty>
				<bean:write name="profile" property="lastName" />&nbsp;
			</td>
			
			<td class="TD-SMALL-RIGHT" >
				<b>Receiver Name:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="tran" property="rcvCustFrstName" />&nbsp;
				<bean:write name="tran" property="rcvCustLastName" />			
			</td>
			<c:if test="${profile.partnerSiteId != 'MGODE'}">
			    <td class="TD-SMALL-RIGHT">
				    <b>Sender State:</b>
			    </td>
			    <td class="TD-SMALL-LEFT">
				    <bean:write name="senderState" scope="session" />
			    </td>
			</c:if>
			<c:if test="${profile.partnerSiteId == 'MGODE'}">
			    <td class="TD-SMALL-RIGHT"/>
			    <td class="TD-SMALL-LEFT"/>
			</c:if>
		</tr>
				<tr>
			<td class="TD-SMALL-RIGHT">
				<b><fmt:message key="label.cust.id"/>:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<a href='../../showCustomerProfile.do?custId=<bean:write name="tran" property="sndCustId" />&basicFlag=N'><bean:write name="tran" property="sndCustId" /></a>
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Email Address:</b>
			</td>
			<td class="TD-SMALL-LEFT"  colspan="3">
				<bean:write name="emailAddresses" />
			</td>			
					
		</tr>
		
		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Number of Phone #'s:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="numberOfPhoneNumbers" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Primary Phone to State:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="phoneState" />
			</td>
			<td class="TD-SMALL-RIGHT">
				<b>Alt Phone to State:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="phoneStateAlt" />
			</td>
		</tr>
		<tr>
			
			<td class="TD-SMALL-RIGHT">
				<b>Unique CCs :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="distinctCCs" />
			</td>			

			<td class="TD-SMALL-RIGHT">
				<b>CC failure(s) :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="failedCCs" />
			</td>			
		</tr>

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Verid Result:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="profile" property="authStatusFlag" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Verid Attempts:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="profile" property="authStatusFailCount" />
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Previous Transactions :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="previousTransactions" />
			</td>
		</tr>

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Any PEN Status :</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="everPended" />
			</td>					
			<td class="TD-SMALL-RIGHT">
				<b><fmt:message key="label.cust.birth.date"/>:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="birthdate" />
			</td>
			<td class="TD-SMALL-RIGHT">
				<b><fmt:message key="label.profile.create.date"/>:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<bean:write name="profileCreateDate" />
			</td>
		</tr>		
	</table>	
</fieldset>
<br>
<fieldset>
	<legend class="TD-SMALL"> 
		<b>Chargeback Header Information</b>
	</legend>
	<table align="center" border="0" cellpadding="0" cellspacing="4" width="98%">
		<logic:notEmpty name="ccChargebackTrackingForm" property="chgbkCreateDate">
		<tr>
			<td class="TD-SMALL-RIGHT" >
			<b>Tracking Create Date:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<fmt:formatDate value="${ccChargebackTrackingForm.chgbkCreateDate}" pattern="dd/MMM/yyyy" />
			</td>
		</tr>	
		</logic:notEmpty>
		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Chargeback Status :</b>&nbsp;
			</td>
			
			<td class="TD-SMALL-LEFT" colspan="3" >
				<html:select name="ccChargebackTrackingForm" property="chgbkStatCode" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:options collection="chargebackStatuses" 
						property="chargebackStatusCode" labelProperty="chargebackStatusDesc" />
				</html:select> 
			</td>
			<td class="TD-SMALL-RIGHT" >
				<b>MoneyGram Received Date:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<html:text size="10" name="ccChargebackTrackingForm" property="chgbkTrkDate" onchange="setOtherRangeValue(this, ccChargebackTrackingForm.chgbkTrkDate);" />
				<img border="0" src="../../images/calendar.gif" onclick="show_calendar('ccChargebackTrackingForm.chgbkTrkDate');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
			</td>
			
		</tr>

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Contact Method:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="custCntctMhtdCode" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:options collection="customerContactMethodTypes" 
						property="customerContactMethodTypeCode" labelProperty="customerContactMethodTypeDesc" />
				</html:select> 			
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Email Consistent:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="emailCnstntFlag" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:option value="N">No</html:option>												
					<html:option value="Y">Yes</html:option>												
				</html:select> 			
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Phone Verified:</b>
			</td>			
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="phNbrVerfdFlag" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:option value="N">No</html:option>												
					<html:option value="Y">Yes</html:option>												
				</html:select> 			
			</td>
		</tr>

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Verbal Contact (past OR present)?:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="custVrblCntctFlag" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:option value="N">No</html:option>												
					<html:option value="Y">Yes</html:option>												
				</html:select> 			
			</td>

			<td class="TD-SMALL-RIGHT">
				<b>Unsolicited Phone Call (this transaction)?:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="unsolicitPhCallFlag" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:option value="N">No</html:option>												
					<html:option value="Y">Yes</html:option>												
				</html:select> 			
			</td>
			
			<td class="TD-SMALL-RIGHT">
				<b>Does Victim Name Match Profile Name?:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="custPrflVldnFlag" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:option value="N">No</html:option>												
					<html:option value="Y">Yes</html:option>												
				</html:select> 			
			</td>
			
		</tr>		

		<tr>
			<td class="TD-SMALL-RIGHT">
				<b>Linked Profile Count:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:text name="ccChargebackTrackingForm" property="linkedCustPrflCnt" size="5" maxlength="5" />
			</td>
			<td class="TD-SMALL-RIGHT">
				<b>Distance between sndr and rcvr:</b>
			</td>
			<td class="TD-SMALL-LEFT">
				<html:text name="ccChargebackTrackingForm" property="sndRcvMileQty" size="5" maxlength="5" />
			</td>
			
			<td class="TD-SMALL-RIGHT">
				<b>Gender Code:</b>&nbsp;
			</td>
			<td class="TD-SMALL-LEFT">
				<html:select name="ccChargebackTrackingForm" property="gndrCode" size="1">
					<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
					<html:options collection="genderTypes" 
						property="genderCode" labelProperty="genderDesc" />
				</html:select> 			
			</td>
			
		</tr>

		<tr>
			<td class="TD-SMALL-LEFT" colspan="6" >
				<div id="formButtons">
					<html:submit property="submitSaveHeader" value="  Save Header  "
						onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
				</div>
			
				<div id="processing" style="visibility: hidden">
					<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
				</div >
			</td>
		</tr>
		
	</table>	
	
</fieldset>

<logic:notEmpty name="chargebackHeader">
<br>
<fieldset>
	<legend class="TD-SMALL"> 
		<b>Chargeback Action History</b>
	</legend>
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="98%">
		<tr class="TD-HEADER-SMALL" >
			<td>Status</td>
			<td>Create Date</td>
			<td>Create UserId</td>
		</tr>

		<%dispClass = "TD-SMALL";%>	

		<logic:iterate id="chargebackAction" name="chargebackActions">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackAction" property="chargebackStatusDesc"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackAction" property="createDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackAction" property="createUserId" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>
		</logic:iterate>

		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		
	</table>	
</fieldset>
</logic:notEmpty>


<logic:notEmpty name="chargebackHeader">
<br>
<fieldset>
	<legend class="TD-SMALL"> 
		<b>Chargeback Case History</b>
	</legend>
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="98%">
		<tr class="TD-HEADER-SMALL" >
			<td>Originator Case ID</td>
			<td>Denial Type</td>
			<td>Statement Date</td>
			<td>Create Date</td>
			<td>Create UserId</td>
		</tr>

		<%dispClass = "TD-SMALL";%>	

		<logic:iterate id="chargebackProviderCase" name="chargebackProviderCases">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackProviderCase" property="chargebackProviderCaseId" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackProviderCase" property="chargebackDenialDesc" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackProviderCase" property="chargebackOriginalStatementDate"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackProviderCase" property="createDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackProviderCase" property="createUserId" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>
		</logic:iterate>
	</table>	
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="98%">
	  <tr class="TD-SMALL-LEFT">
		<td nowrap='nowrap' align='left'>
		    <b>Originator Case ID:</b><br>
			<html:text name="ccChargebackTrackingForm" property="chgbkPrvdrCaseId" size="20" maxlength="20" />
		</td>
		<td nowrap='nowrap' align='left'>
		    <b>Fraud Loss Type:</b><br>
			<html:select name="ccChargebackTrackingForm" property="chgbkDenialCode" size="1">
				<html:option value=""><fmt:message key="option.select.an.option"/></html:option>
				<html:options collection="chargebackDenialTypes" 
					property="chargebackDenialTypeCode" labelProperty="chargebackDenialTypeDesc" />
			</html:select> 
			
		</td>
		<td nowrap='nowrap' align='left'>
		    <b>Originator Statement Month:</b><br>
			<html:text size="10" name="ccChargebackTrackingForm" property="chgbkOrgnStmtDate" onchange="setOtherRangeValue(this, ccChargebackTrackingForm.chgbkOrgnStmtDate);" />
			<img border="0" src="../../images/calendar.gif" onclick="show_calendar('ccChargebackTrackingForm.chgbkOrgnStmtDate');" onmouseover="window.status='Pick a Date';return true;" onmouseout="window.status='';return true;"/>
		</td>
	  </tr>	
	  <tr>	
	  	<td colspan="3">
			<div id="formButtons1">
				<html:submit property="submitSaveCase" value="     Add Case      "
					onclick="formButtons1.style.display='none';processing1.style.visibility='visible';" />
				<br>	
			</div>
		
			<div id="processing1" style="visibility: hidden">
				<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
			</div>
		</td>
	  </tr>
	</table>
</fieldset>
</logic:notEmpty>

<logic:notEmpty name="chargebackHeader">
<br>
<fieldset>
	<legend class="TD-SMALL"> 
		<b>Chargeback Comments</b>
	</legend>
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="98%">
		<tr class="TD-HEADER-SMALL" >
			<td>Comment Text</td>
			<td>Create Date</td>
			<td>Create UserId</td>
		</tr>

		<%dispClass = "TD-SMALL";%>	

		<logic:iterate id="chargebackComment" name="chargebackComments">
		<tr>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackComment" property="commentText"/></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackComment" property="createDate" /></td>
			<td nowrap='nowrap' class='<%=dispClass%>' align='left'>
				<bean:write filter="false" name="chargebackComment" property="createUserId" /></td>
		</tr>
		<%  if (dispClass.equals("TD-SHADED-SMALL")) dispClass = "TD-SMALL"; 
			else dispClass = "TD-SHADED-SMALL"; %>
		</logic:iterate>
	</table>	
	<table align="center" border="0" cellpadding="1" cellspacing="1" width="98%">
	<tr class="TD-SMALL-LEFT">
		<td nowrap='nowrap' align='left'>
		    <b>New Comment Text :</b><br>
			<html:text name="ccChargebackTrackingForm" property="chgbkCmntText" size="100" maxlength="250" />

			<div id="formButtons2">
				<html:submit property="submitSaveComment" value="  Add Comment   "
					onclick="formButtons2.style.display='none';processing2.style.visibility='visible';" />
				<br>	
			</div>
		
			<div id="processing2" style="visibility: hidden">
				<span class="PROCESSING"><fmt:message key="message.in.process" /></span>
			</div >
		</td>
	</tr>
	</table>
</fieldset>
</logic:notEmpty>


	
</center>		
</html:form>
