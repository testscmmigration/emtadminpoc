<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

		<% 
		
		emgadm.security.TranSecurityMatrix matrix =null;
		matrix=(emgadm.security.TranSecurityMatrix) request.getSession().getAttribute("tranAuth");
		String chargeBackTrackingHyperLink = null;
		String chgBckTrackingViewInd = null;
		if (request.getSession().getAttribute("chargeBackTrackingHyperLink") != null)
		{
		   chargeBackTrackingHyperLink = (String) request.getSession().getAttribute("chargeBackTrackingHyperLink");
		   chgBckTrackingViewInd = (String) request.getSession().getAttribute("chgBckTrackingViewInd");
		}
		int buttonIndex = 0;
		%>
<script language="javascript">
	
	function set(target) {
		document.forms[0].dispatch.value = target;
	}
	window.onload = function() {
		var transend = document.getElementById("tranSendCountry").value;
		var consumerCountry = document.getElementById("consumerCountryCode").value;
		var consumerISP = document.getElementById("consumerISP").value;
		if (consumerCountry != "" && consumerCountry != null
				&& typeof (consumerCountry) != 'undefined') {
			if (transend != consumerCountry || consumerCountry == '') {
				document.getElementById("count").style.color = 'red';
			}
		}
		
		if (consumerCountry == "") {
			document.getElementById("count").style.color = 'red';
		}
	}
</script>
<table border='0' cellpadding='0' cellspacing='0' width='100%'>
		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.transaction.id"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="transactionDetailForm" property="tranId" /></td>
			<td nowrap='nowrap' class='TD-SMALL'><html:hidden property="tranId"/>
		</tr>
		<tr>
			<td nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.partner.site.id"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			    <bean:write name="transactionDetailForm" property="transPartnerSiteId" /></td>
			<td nowrap='nowrap' class='TD-SMALL'><%=matrix.getButtons(buttonIndex++)%></td>
			</td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.transaction.status"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'><font color="red"><b>
				<bean:write name="transactionDetailForm" property="tranStatusCode" /></b></font> (
				<bean:write name="transactionDetailForm" property="tranStatusDesc" />)</td>
			<td> &nbsp; </td>
				<html:hidden property="tranStatusCode"/>
				<html:hidden property="tranStatusDesc"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' valign='top' class='TD-LABEL-SMALL'><b><fmt:message key="label.transaction.funding.status"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranSubStatusCode" /> (
				<bean:write name="transactionDetailForm" property="tranSubStatusDesc" />)
				</td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%> </td>
				<html:hidden property="tranSubStatusCode"/>
				<html:hidden property="tranSubStatusDesc"/>
		</tr>
	<logic:notEmpty name="chargeBackTrackingHyperLink">
		<tr><td align='right'><div id="trackingLinkProcessing" style="visibility:hidden"><span class="ERROR">Processing ...</span></div></td><td>
			<logic:equal name="chgBckTrackingViewInd" value="VIEW">
				<div id="trackingLink"><a href='../../<%=chargeBackTrackingHyperLink%>' onclick="trackingLink.style.display='none';trackingLinkProcessing.style.visibility='visible';" >View Chargeback Tracking</a></div>
			</logic:equal>
			<logic:equal name="chgBckTrackingViewInd" value="ADD">
				<div id="trackingLink"><a href='../../<%=chargeBackTrackingHyperLink%>' onclick="trackingLink.style.display='none';trackingLinkProcessing.style.visibility='visible';" >Add Chargeback Tracking</a>&nbsp;</div>
			</logic:equal>
	    </td><td></td></tr>
	</logic:notEmpty>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.lgcy.ref.nbr"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranLgcyRefNbr" />
				<c:if test="${transactionDetailForm.displayMoneygramDetails}">
					<a href='../../showMainframeDetails.do
						?tranType=<bean:write name="transactionDetailForm" property="tranType" />
						&tranLgcyRefNbr=<bean:write name="transactionDetailForm" property="tranLgcyRefNbr" />
						&id=<bean:write name="transactionDetailForm" property="tranId" />
						&partnerSiteId=<bean:write name="transactionDetailForm" property="transPartnerSiteId" />'>
						MoneyGram Details
					</a>
				</c:if>
			</td>
			<td> &nbsp; </td>
				<html:hidden property="tranLgcyRefNbr"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.owned.by"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranOwnerId" /></td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
				<html:hidden property="tranOwnerId"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.sender"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranSenderName" />
			    (<a href='../../showCustomerProfile.do?custId=<bean:write name="transactionDetailForm" property="custId" />&basicFlag=N'>Detail</a>)&nbsp;
			    (<a href='../../showCustomerProfile.do?custId=<bean:write name="transactionDetailForm" property="custId" />&basicFlag=Y'>Basic</a>)&nbsp;
			</td>
			<td> &nbsp; </td>
		</tr>
	<tr>
		<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.sender.second.last.name"/>:</b></td>
		<td nowrap="nowrap" class="TD-SMALL"><bean:write name="transactionDetailForm" property="tranSenderSecondLastName" /></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'>
			<logic:notEqual name="transactionDetailForm" property="tranType"
				value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
 				<b><fmt:message key="label.receiver"/>:</b>
			</logic:notEqual>
			<logic:equal name="transactionDetailForm" property="tranType"
				value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
 				<b><fmt:message key="label.receiver.agency.code"/>:</b>
			</logic:equal>
				</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			<logic:notEqual name="transactionDetailForm" property="tranType"
				value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
				<bean:write name="transactionDetailForm" property="rcvCustFrstName" />
				<a href='../../adhocReport.do?submitSearch=Search&receiverLastName=<bean:write name="transactionDetailForm" property="rcvCustLastName" />'><bean:write name="transactionDetailForm" property="rcvCustLastName" /></a>
				</td>
				<html:hidden property="rcvCustFrstName"/>
				<html:hidden property="rcvCustLastName"/>
			</logic:notEqual>
			<logic:equal name="transactionDetailForm" property="tranType"
				value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
				<a href='../../showBillerInfo.do?agentId=<bean:write filter="false"
					name="transactionDetailForm" property="tranReceiverName"/>&detail=Y&id=<bean:write
					name="transactionDetailForm" property="tranId"/>'
				    ><bean:write name="transactionDetailForm" property="tranReceiverName" /></a>
			</logic:equal>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.mid.name"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="rcvCustMidName" value="/">
				<bean:write name="transactionDetailForm" property="rcvCustMidName" />
				</logic:notEqual>
					&nbsp; </td>
			<td> &nbsp; </td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.matrnl.name"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="rcvCustMatrnlName" value="">
				<bean:write name="transactionDetailForm" property="rcvCustMatrnlName" />
				</logic:notEqual>
					&nbsp; </td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
		</tr>
<!-- MAS - PCI
			<tr>
	 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.account.id"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:equal name="transactionDetailForm" property="tranType"
					value="<%=emgshared.model.TransactionType.EXPRESS_PAYMENT_SEND_CODE%>">
				<bean:write name="transactionDetailForm" property="tranRcvAgcyAcctMask" />
				</logic:equal>
					&nbsp; </td>
				<td> &nbsp; </td>
			</tr>
 -->
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.amount"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranFaceAmt" />
				<html:hidden property="tranFaceAmt"/>
			</td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.fee"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranSendFee" /></td>
			<td> &nbsp; </td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.return.fee"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranReturnFee" /></td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.total.amount"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
			<bean:write name="transactionDetailForm" property="tranSendAmt" /></td>
			<td> &nbsp; </td>
			<html:hidden property="tranSendAmt"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.transaction.type"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranType"
				/> (<bean:write name="transactionDetailForm" property="tranTypeDesc" />)</td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
			<html:hidden property="tranType"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.country"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="tranSendCity" value="">
					<bean:write name="transactionDetailForm" property="tranSendCity" />, &nbsp;
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="tranSendCountry" value="USA">
					<logic:notEqual name="transactionDetailForm" property="tranSendState" value="">
						<bean:write name="transactionDetailForm" property="tranSendState" />, &nbsp;
					</logic:notEqual>
				</logic:equal>
				<bean:write name="transactionDetailForm" property="tranSendCountry" /></td>
			<td> &nbsp; </td>
			<html:hidden styleId="tranSendCountry" property="tranSendCountry" />
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.currency"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="tranSendCurrency" value="">
					<bean:write name="transactionDetailForm" property="tranSendCurrency" />
				</logic:notEqual>
			<td> &nbsp; </td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.exchange.rate"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="exchangeRate" value="">
					<bean:write name="transactionDetailForm" property="exchangeRate" />
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="exchangeRate" value="">
					<c:out value="0"></c:out>
				</logic:equal>
			<td> &nbsp; </td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.country"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="rcvCustAddrCityName" value="">
					<bean:write name="transactionDetailForm" property="rcvCustAddrCityName" />, &nbsp;
				</logic:notEqual>
				<logic:notEqual name="transactionDetailForm" property="rcvCustAddrStateName" value="">
					<bean:write name="transactionDetailForm" property="rcvCustAddrStateName" />, &nbsp;
				</logic:notEqual>
				<bean:write name="transactionDetailForm" property="tranRecvCountry" />
				<logic:equal name="transactionDetailForm" property="overThrldAmt" value="true">
					<b><font color='red'>Over country threshold warning amount (
					<bean:write name="transactionDetailForm" property="sndThrldWarnAmt" />)</font></b>
				</logic:equal>
				</td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.other.fees"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="otherFees" value="">
					<bean:write name="transactionDetailForm" property="otherFees" />
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="otherFees" value="">
					<c:out value="0"></c:out>
				</logic:equal>
			</td>
		</tr>		
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.other.taxes"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="otherTaxes" value="">
					<bean:write name="transactionDetailForm" property="otherTaxes" />
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="otherTaxes" value="">
					<c:out value="0"></c:out>
				</logic:equal>
			</td>
		</tr>		
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.total.amount"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="totalReceiveAmount" value="">
					<bean:write name="transactionDetailForm" property="totalReceiveAmount" />
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="totalReceiveAmount" value="">
					<c:out value="0"></c:out>
				</logic:equal>
			</td>
		</tr>		
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.currency"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranRecvCurrency" />
				<logic:notEqual name="transactionDetailForm" property="tranRecvCurrencyName" value="">
					&nbsp;(<bean:write name="transactionDetailForm" property="tranRecvCurrencyName" />)
				</logic:notEqual>
			</td>
			<td> &nbsp; </td>
				<html:hidden property="rcvAgtAddr"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.rcv.agent.addr"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvAgtAddr" /></td>
			<td nowrap='nowrap' class='TD-SMALL'>
				<%=matrix.getButtons(buttonIndex++)%></td>
				<html:hidden property="tranPrimAcctType"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.primary.funding.source"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranPrimAcctType"
				/>-************<bean:write name="transactionDetailForm" property="tranPrimAcctMask" /></td>
			<td> &nbsp; </td>
			<logic:notEqual name="transactionDetailForm" property="mccPartnerProfileId" value="N/A">
				<tr>
 					<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.payment.profile.id"/>:</b></td>
					<td  nowrap='nowrap' class='TD-SMALL'>
						<bean:write name="transactionDetailForm" property="mccPartnerProfileId" />
					</td>
			</tr>
			</logic:notEqual>

		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.secondary.funding.source"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:notEqual name="transactionDetailForm" property="tranSecAcctMask" value="">
					<bean:write name="transactionDetailForm" property="tranSecAcctType"
					/>-************<bean:write name="transactionDetailForm" property="tranSecAcctMask" /></td>
					<html:hidden property="tranSecAcctType"/>
				</logic:notEqual>
				<logic:equal name="transactionDetailForm" property="tranSecAcctMask" value="">
					&nbsp;
				</logic:equal>
			<td> &nbsp; </td>
		</tr>
		<logic:notEqual name="transactionDetailForm" property="retrievalRequestDate" value="">
			<tr>
 				<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.retrieval.request.date"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL-RED'>
				<fmt:formatDate value="${transactionDetailForm.retrievalRequestDate}" pattern="dd/MMM/yyyy h:mm a" />
				<td> &nbsp; </td>
			</tr>
		</logic:notEqual>
		<logic:present name="transactionDetailForm" property="rcvRRN">
		<logic:notEqual name="transactionDetailForm" property="rcvRRN" value=" ">
			<tr>
	 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.rrn"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'>
						<bean:write name="transactionDetailForm" property="rcvRRN" />
						&nbsp; </td>
				<td> &nbsp; </td>
			</tr>
		</logic:notEqual>
		</logic:present>
		<logic:present name="transactionDetailForm" property="rcvAcctMaskNbr">
		<logic:notEqual name="transactionDetailForm" property="rcvAcctMaskNbr" value="">
			<tr>
	 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.acct.mask.nbr"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'>
						<bean:write name="transactionDetailForm" property="rcvAcctMaskNbr" />
						&nbsp; </td>
				<td> &nbsp; </td>
			</tr>
		</logic:notEqual>
		</logic:present>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.dlvr.option"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="deliveryOptionName" />
						&nbsp; </td>
			<td> &nbsp; </td>
		</tr>

		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.tran.score.nbr"/>:</b></td>
			<logic:notEqual name="transactionDetailForm" property="sysAutoRsltCode" value="">
				<logic:equal name="showTranScoreDetail" value="Y">
					<td  nowrap='nowrap' class='TD-SMALL'>
						<a href='../../showTranScoreDetail.do?type=d
							&tranId=<bean:write name="transactionDetailForm" property="tranId"/>
							&partnerSiteId=<bean:write name="transactionDetailForm" property="transPartnerSiteId" />'>
					<bean:write name="transactionDetailForm" property="transScores" />-<bean:write name="transactionDetailForm" property="sysAutoRsltCode" /></a></td>
				</logic:equal>
				<logic:notEqual name="showTranScoreDetail" value="Y">
					<td  nowrap='nowrap' class='TD-SMALL'><bean:write name="transactionDetailForm" property="transScores" />-<bean:write name="transactionDetailForm" property="sysAutoRsltCode" /></a></td>
				</logic:notEqual>
			</logic:notEqual>
			<logic:equal name="transactionDetailForm" property="sysAutoRsltCode" value="">
				<td  nowrap='nowrap' class='TD-SMALL'>None</td>
			</logic:equal>
			<td> &nbsp; </td>
				<html:hidden property="transScores"/>
				<html:hidden property="sysAutoRsltCode"/>
		</tr>
		<logic:notEqual name="transactionDetailForm" property="scoreRvwCode" value="">
		<tr>
			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.score.review.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="scoreRvwCode" /></td>
			<td> &nbsp; </td>
				<html:hidden property="scoreRvwCode"/>
		</tr>
		<tr>
			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.score.review.comment"/>:</b></td>
			<td  class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranScoreCmntText" /></td>
			<td> &nbsp; </td>
				<html:hidden property="tranScoreCmntText"/>
		</tr>
		</logic:notEqual>











		<tr>
			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.trans.monitoring.score"/>:</b></td>
 			<logic:notEqual name="transactionDetailForm" property="tranRiskScore" value="">
				<td  nowrap='nowrap' class='TD-SMALL'><bean:write name="transactionDetailForm" property="tranRiskScore" />
					<c:if test="${transactionDetailForm.tranRiskScoreDefaulted == 'true'}"> - System Default</c:if>
				</td>
			</logic:notEqual>
			<logic:equal name="transactionDetailForm" property="tranRiskScore" value="">
				<td  nowrap='nowrap' class='TD-SMALL'>None</td>
			</logic:equal>
		</tr>









		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.processing.type"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="">
					<fmt:message key="label.manual.processing"/>
				</logic:equal>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="MAN">
					<fmt:message key="label.manual.processing"/>
				</logic:equal>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="PRE">
					<fmt:message key="label.premier.processing"/>
				</logic:equal>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="ASP">
					<fmt:message key="label.automated.scoring.processing"/>
				</logic:equal>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="VEL">
					<fmt:message key="label.velocity.processing"/>
				</logic:equal>
				<logic:equal name="transactionDetailForm" property="tranRvwPrcsCode" value="CEX">
					<fmt:message key="label.country.exception.processing"/>
				</logic:equal>

			</td>
			<td> &nbsp; </td>
				<html:hidden property="tranRvwPrcsCode"/>
		</tr>

	<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.ip.address"/>:</td>
 			 
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranCustIP" />
				<html:hidden property="tranCustIP"/>
				<html:hidden property="dispatch" value=" "/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 
				<html:submit onclick="set('lookUp');">LookUp IP Address</html:submit>
		</tr>
	<!-- changes for Req 4052 -->
		
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.isp"/>:</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="consumerISP" />
				
			<td> &nbsp; </td>
				<html:hidden styleId="consumerISP" property="consumerISP"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.domain"/>:</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="consumerDomain" /></td>
			<td> &nbsp; </td>
				<html:hidden property="consumerDomain"/>
		</tr>
	<tr>
		<td nowrap='nowrap' class='TD-LABEL-SMALL' id="count"><fmt:message
				key="label.consumer.country" />:</td>
		<td nowrap='nowrap' class='TD-SMALL'>
			<%-- <font color="red"><bean:write name="transactionDetailForm"  property="consumerCountry" /></font></td> --%>
			<logic:equal name="transactionDetailForm" property="countrySameFlag"
				value="true">
				<bean:write name="transactionDetailForm" property="consumerCountry" />
			</logic:equal> <logic:equal name="transactionDetailForm" property="countrySameFlag"
				value="false">
				<font color="red"><bean:write name="transactionDetailForm"
						property="consumerCountry" /> </font>
			</logic:equal>
		<td>&nbsp;</td>
		<html:hidden styleId="consumerCountry" property="consumerCountry" />
		<html:hidden styleId="consumerCountryCode" property="consumerCountryCode" />
	</tr>
	<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.state"/>:</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="consumerRegion" /></td>
			<td> &nbsp; </td>
				<html:hidden property="consumerRegion"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.city"/>:</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="consumerCity" /></td>
			<td> &nbsp; </td>
				<html:hidden  styleId="consumerCity"  property="consumerCity"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><fmt:message key="label.consumer.postalcode"/>:</td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="consumerZipCode" /></td>
			<td> &nbsp; </td>
				<html:hidden property="consumerZipCode"/>
		</tr>
		<!-- changes 4052 -->
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.msg.1"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranSndMsg1Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="tranSndMsg1Text"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.msg.2"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="tranSndMsg2Text" /></td>
			<td > &nbsp; </td>
				<html:hidden property="tranSndMsg2Text"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.create.date"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="createDate" /></td>
			<td> &nbsp; </td>
				<html:hidden property="createDate"/>
		</tr>
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.internet.purchase"/>:</b></td>
			<logic:equal name="transactionDetailForm" property="inetPurchFlag" value="Yes">
				<td nowrap='nowrap' class='TD-SMALL-RED'>
			</logic:equal>
			<logic:notEqual name="transactionDetailForm" property="inetPurchFlag" value="Yes">
				<td nowrap='nowrap' class='TD-SMALL'>
			</logic:notEqual>
				<bean:write name="transactionDetailForm" property="inetPurchFlag" />
			<td> &nbsp; </td>
				<html:hidden property="inetPurchFlag"/>
		</tr>

		<logic:notEqual name="transactionDetailForm" property="rcvDate" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.receive.date"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvDate" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvDate"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvAgentRefNbr" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.agent.ref.nbr"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvAgentRefNbr" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvAgentRefNbr"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustAddrLine1Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.addr.1.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustAddrLine1Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustAddrLine1Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustAddrLine2Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.addr.2.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustAddrLine2Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustAddrLine2Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustAddrLine3Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.addr.3.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustAddrLine3Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustAddrLine3Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustAddrPostalCode" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.recv.cust.postal.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustAddrPostalCode" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustAddrPostalCode"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustDlvrInstr1Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.dlvr.instr.1.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustDlvrInstr1Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustDlvrInstr1Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustDlvrInstr2Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.dlvr.instr.2.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustDlvrInstr2Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustDlvrInstr2Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustDlvrInstr3Text" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.dlvr.instr.3.text"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustDlvrInstr3Text" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustDlvrInstr3Text"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="rcvCustPhNbr" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.rcv.cust.ph.nbr"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="rcvCustPhNbr" /></td>
			<td> &nbsp; </td>
				<html:hidden property="rcvCustPhNbr"/>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="customerAutoEnrollFlag" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.customer.auto.enroll"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="customerAutoEnrollFlag" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="testQuestion" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.testQuestion"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="testQuestion" /></td>
		</tr>
		</logic:notEqual>
		<logic:notEqual name="transactionDetailForm" property="testQuestionAnswer" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.testQuestion.answer"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="testQuestionAnswer" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="loyaltyPgmMembershipId" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.loyaltyId"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="loyaltyPgmMembershipId" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="sndCustPhotoIdStateCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.state.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustPhotoIdStateCode" /></td>
		</tr>
		</logic:notEqual>
		
		<logic:equal name="transactionDetailForm" property="sndCustPhotoIdStateCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.state.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'><b><fmt:message key="label.na"/></td>
		</tr>
		</logic:equal>
		
		<logic:notEqual name="transactionDetailForm" property="sndCustPhotoIdTypeCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.type.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustPhotoIdTypeCode" /></td>
		</tr>
		</logic:notEqual>

		<logic:equal name="transactionDetailForm" property="sndCustPhotoIdTypeCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.type.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'><b><fmt:message key="label.na"/></b></td>
		</tr>
		</logic:equal>

		<logic:notEqual name="transactionDetailForm" property="sndCustPhotoId" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustPhotoId" /></td>
		</tr>
		</logic:notEqual>

		<logic:equal name="transactionDetailForm" property="sndCustPhotoId" value="">
			<tr>
 				<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'><b><fmt:message key="label.na"/></b></td>
			</tr>
		</logic:equal>
		
		<logic:notEqual name="transactionDetailForm" property="sndCustPhotoIdExpDate" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.exp.date"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<fmt:formatDate value="${transactionDetailForm.sndCustPhotoIdExpDate}" pattern="dd/MMM/yyyy h:mm a" />
			</td>
		</tr>
		</logic:notEqual>

		<logic:equal name="transactionDetailForm" property="sndCustPhotoIdExpDate" value="">
			<tr>
	 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.exp.date"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'><b><fmt:message key="label.na"/></b></td>
			</tr>
		</logic:equal>		


		<logic:notEqual name="transactionDetailForm" property="sndCustPhotoIdCntryCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.cntry.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustPhotoIdCntryCode" /></td>
		</tr>
		</logic:notEqual>

		<logic:equal name="transactionDetailForm" property="sndCustPhotoIdCntryCode" value="">
			<tr>
	 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.photoid.cntry.code"/>:</b></td>
				<td  nowrap='nowrap' class='TD-SMALL'><b><fmt:message key="label.na"/></b></td>
			</tr>
		</logic:equal>

		<logic:notEqual name="transactionDetailForm" property="sndCustLegalId" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.legalid"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustLegalId" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="sndCustLegalIdTypeCode" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.legalid.type.code"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustLegalIdTypeCode" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="sndCustDOB" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.cust.DOB"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<fmt:formatDate value="${transactionDetailForm.sndCustDOB}" pattern="dd/MMM/yyyy" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="sndFeeAmtNoDiscountAmt" value="">
			<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.send.fee.amt.no.disc"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndFeeAmtNoDiscountAmt" /></td>
		</tr>
		</logic:notEqual>

		<logic:notEqual name="transactionDetailForm" property="sndCustOccupationText" value="">
		<tr>
 			<td  nowrap='nowrap' class='TD-LABEL-SMALL'><b><fmt:message key="label.snd.cust.occupation"/>:</b></td>
			<td  nowrap='nowrap' class='TD-SMALL'>
				<bean:write name="transactionDetailForm" property="sndCustOccupationText" /></td>
		</tr>
		</logic:notEqual>

		<logic:present name='vbvExist'>
		<tr><td colspan='2' align='center'>
			<a href="../../showVBV.do?tranId=<c:out  value='${transactionDetailForm.tranId}' />">See
			Verified By Visa Detail Information for the Transaction</a>
		</td></tr>
		</logic:present>
	</table>
