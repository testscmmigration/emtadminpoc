<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

<script src="../../Display/jscript/textCounter.js" language="JavaScript"></script>
<script src="../../Display/jscript/showPopup.js" language="JavaScript"></script>
<script src="../../Display/jscript/consumerStatus.js" language="JavaScript"></script>

<SCRIPT LANGUAGE="JavaScript">

function ClipBoardEdir(guid) 
{
	guidOuter.style.display='none';
	guidCopy.style.display='inline';	
	if( window.clipboardData && clipboardData.setData )
	{
		clipboardData.setData("Text", guid);
	}
	setTimeout("hideit()",1500); 
	
}

function hideit()
{
	guidOuter.style.display='inline';
	guidCopy.style.display='none';	
}

</SCRIPT>

<fmt:setTimeZone value="${userTimeZone}" />

<c:choose>  
	<c:when test="${showCustomerProfileForm.partnerSiteId == 'MGOUK'}">
		<c:set var="custIdMarket" value="UK${showCustomerProfileForm.custId}" />
	</c:when>  
	<c:when test="${showCustomerProfileForm.partnerSiteId == 'MGODE'}">
		<c:set var="custIdMarket" value="DE ${showCustomerProfileForm.custId}" />
	</c:when>  
	<c:otherwise>  
		<c:set var="custIdMarket" value="${showCustomerProfileForm.custId}" />
	</c:otherwise>  
</c:choose> 


<h4>						
<html:messages id="message" property="headerMessage">
	<c:out value="${message}"/>
</html:messages>
</h4>
<h3>
	<fmt:message key="label.customer.profile" />
</h3>

<div class="cProfile cProfile_${market}">



<fieldset class="fieldset1">
<html:form type="emgadm.consumer.UpdateCustomerProfileForm" name="superTaintForm" 
target="showBlockedList"
action="showBlockedList" styleId="superTaintForm"  method="GET">
	<input type="hidden" value="${showCustomerProfileForm.custId}" name="custId" />
	<button type="submit" value="Super Taint">Super Taint</button> 
</html:form>
<legend class="legend">
<fmt:message key="label.dashboard" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

<center>
<logic:notEmpty name="dashboard" >

<table border='1' cellpadding='1' cellspacing='1' bgcolor='#e6e6fa'>
	<tr>
		<td class="td-small">
			<b>First Login Date:</b> <fmt:formatDate value="${dashboard.lastLogin}" pattern="yyyy-MM-dd HH:mm:ss z" />
 			&nbsp; &nbsp; &nbsp;
			<b>Last Login Date:</b> <fmt:formatDate value="${dashboard.lastLogin}" pattern="yyyy-MM-dd HH:mm:ss z" />
		</td>
	</tr>

	<tr>
		<td class="td-small">
		<table>
			<tr>
				<td class="td-label-small">Good Trans In last 30 days: </td>

				<logic:equal name="dashboard" property="recentTranCnt" value="0">
				<td  class="td-small"><b>Tran Count:</b> 0</td>
				</logic:equal>

				<logic:notEqual name="dashboard" property="recentTranCnt" value="0">
				<td class="td-small">
					<b>Tran Count:</b>
					<logic:equal name="dashDetail" value="Y">
						<a href='../../showDashboardDetail.do?from=dash&id=recentTran'>
						<bean:write name="dashboard" property="recentTranCnt" /></a>
					</logic:equal>
					<logic:notEqual name="dashDetail" value="Y">
						<bean:write name="dashboard" property="recentTranCnt" />
					</logic:notEqual>
					&nbsp; &nbsp; &nbsp;

					<b>Total Amount:</b> <bean:write name="dashboard" property="recentAmt" /><br />
					<bean:define id="rcvs" name="dashboard" property="receiverList" />
				</td>
			</tr><tr>
				<td class="td-small"> &nbsp; </td>
				<td class="td-small">
					<b>Receiver:</b>
					<logic:iterate id="rcv" name="rcvs" >
						<bean:write name="rcv" property="frstName" />
						<bean:write name="rcv" property="lastName" />
						<logic:equal name="dashDetail" value="Y">
							<a href='../../showDashboardDetail.do?from=dash&id=recentRcv&frst=<bean:write
							name="rcv" property="frstName" />&last=<bean:write
							name="rcv" property="lastName" />'>
							<bean:write name="rcv" property="tranCnt" /></a>
						</logic:equal>
						<logic:notEqual name="dashDetail" value="Y">
							<bean:write name="rcv" property="tranCnt" />
						</logic:notEqual>
						&nbsp; &nbsp;
					</logic:iterate>
				</td>
				</logic:notEqual>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="td-small">
			<b>Total Tran:</b>

			<logic:equal name="dashboard" property="totalTranCnt" value="0">
			0
			</logic:equal>

			<logic:notEqual name="dashboard" property="totalTranCnt" value="0">
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=totalTran'>
					<bean:write name="dashboard" property="totalTranCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="totalTranCnt" />
				</logic:notEqual>
			</logic:notEqual>

			&nbsp; &nbsp;

			<logic:notEqual name="dashboard" property="denyTranCnt" value="0">
				<b>Deny:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=denyTran'>
					<bean:write name="dashboard" property="denyTranCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="denyTranCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>

			<logic:notEqual name="dashboard" property="sendTranCnt" value="0">
				<b>Send:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=sendTran'>
					<bean:write name="dashboard" property="sendTranCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="sendTranCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>

			<logic:notEqual name="dashboard" property="cancelTranCnt" value="0">
				<b>Cancel:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=cancelTran'>
					<bean:write name="dashboard" property="cancelTranCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="cancelTranCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>

			<logic:notEqual name="dashboard" property="fundedTranCnt" value="0">
				<b>Funded:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=fundedTran'>
					<bean:write name="dashboard" property="fundedTranCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="fundedTranCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>

			<b>Average Amount:</b> <bean:write name="dashboard" property="tranAvgAmt" />
		</td>
	</tr>
	<tr>
		<td class="td-small">
			<logic:notEqual name="dashboard" property="bankAccountCnt" value="0">
				<b>Bank Account:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=bankAccts'>
					<bean:write name="dashboard" property="bankAccountCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="bankAccountCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>

			<logic:notEqual name="dashboard" property="creditCardAccountCnt" value="0">
				<b>Credit Card Account:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=creditCardAccts'>
					<bean:write name="dashboard" property="creditCardAccountCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="creditCardAccountCnt" />
				</logic:notEqual>
				&nbsp; &nbsp;
			</logic:notEqual>
			<logic:notEqual name="dashboard" property="debitCardAccountCnt" value="0">
				<b>Debit Card Account:</b>
				<logic:equal name="dashDetail" value="Y">
					<a href='../../showDashboardDetail.do?from=dash&id=debitCardAccts'>
					<bean:write name="dashboard" property="debitCardAccountCnt" /></a>
				</logic:equal>
				<logic:notEqual name="dashDetail" value="Y">
					<bean:write name="dashboard" property="debitCardAccountCnt" />
				</logic:notEqual>
			</logic:notEqual>
		</td>
	</tr>
</table>
</logic:notEmpty>
</center>

</fieldset>

<br />

<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.profile.info" />:
	</legend>
	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<tbody>
			<logic:equal name="isTainted" value="Y">
			<tr>
				<td colspan='2'>
						<h3><fmt:message key="label.profile.tainted" /></h3>
				</td>
				<logic:equal name="isSSNTainted" value="Y">				
					<html:form type="emgadm.consumer.UpdateCustomerProfileForm" name="clearSSNTaintForm" action="clearSSNTaint">					
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.ssnDetaint" />: </b></td>
					<td nowrap="nowrap" class="td-small">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<logic:equal name="clearSSNTaint" value="Y">
							<input type="submit" class="transbtn2" value="Clear SSN Taint" />
						</logic:equal>
						<logic:notEqual name="clearSSNTaint" value="Y">
							See Manager for removal
						</logic:notEqual>
					</td>
					</html:form>
				</logic:equal>
			</tr>
			</logic:equal>
			<logic:notEqual name="isTainted" value="Y">
				<logic:equal name="profileMasterOnlyTaint" value="Y">
						<tr>
							<td colspan='3'>
								<h3><fmt:message key="label.profile.tainted.master.only" /></h3>
							</td>
					<html:form type="emgadm.consumer.UpdateCustomerProfileForm" name="clearMasterTaintForm" action="clearMasterTaint">					
					<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.masterDetaint" />: </b></td>
					<td nowrap="nowrap" class="td-small">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<logic:equal name="clearMasterTaint" value="Y">
							<input type="submit" class="transbtn2" value="Clear Master Taint" />
						</logic:equal>
						<logic:notEqual name="clearMasterTaint" value="Y">
							See Manager for removal
						</logic:notEqual>
					</td>
					</html:form>
					</tr>
				</logic:equal>
			</logic:notEqual>
			<tr>
				<td nowrap="nowrap" class="td-small" colspan="3">
					<logic:equal name="searchAuth" value="Y">
						<a href="../../consumerSearchSelection.do"><fmt:message key="label.search.similar.profile" /></a>
					</logic:equal>
					</td>
					<td></td><td></td>	
			</tr>


			<tr>
				<td nowrap="nowrap" class="td-small" colspan="1">
					<logic:equal name="searchAuth" value="Y">
						<logic:notEqual name="showCustomerProfileForm" property="createdIPAddress" value="">					
							<b><fmt:message key="label.create.ipaddress" />:</b></td>
							<td nowrap="nowrap" class="td-small" align="left"><c:out value="${showCustomerProfileForm.createdIPAddress}" /></td><td></td>
						</logic:notEqual>
						<logic:equal name="showCustomerProfileForm" property="createdIPAddress" value="">					
							</td><td nowrap="nowrap" class="td-small" align="left"></td><td></td>
						</logic:equal>
					</logic:equal>
					</td>
						<td nowrap="nowrap" class="td-small" align="right">					
						</td>
						<td nowrap="nowrap" class="td-small" align="left"></td>
				</td>	
			</tr>

			<!-- Code modified to disable the button -->
			<logic:equal name="profileDeletedFromLdap" value="Y">
			<tr>
				<td nowrap="nowrap" class="td-small" align="left" colspan='1'><b>User is Deleted from eDirectory LDAP</b></td>
				<html:form type="emgadm.consumer.UpdateCustomerProfileForm" name="receateLDAPEntryForm" action="recreateLDAPEntry">					
					<td nowrap="nowrap" class="td-small">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<logic:equal name="recreateLDAPEntry" value="Y">
						<input type="submit" class="transbtn2" value="Recreate LDAP Entry" id ="submittbutton" 
						onclick="this.disabled=disabled;
						document.getElementById('submittbutton').disabled='disable';
						this.form.submit(); "	/> 
						 
						</logic:equal>
						<logic:notEqual name="recreateLDAPEntry" value="Y">
							See Manager for LDAP recreate functionality.
						</logic:notEqual>
					</td>
				</html:form>
			</tr>
			</logic:equal>
			<logic:notEqual name="profileDeletedFromLdap" value="Y">
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.invalid.logon.try" />:</b></td>
				<td nowrap="nowrap" class="td-small" align="left"><c:out value="${showCustomerProfileForm.invlLoginTryCnt}" /></td>
				<td>&nbsp;</td>
				<td nowrap="nowrap" class="td-small" align="right"><b>Profile Locked: </b></td>
					
				<logic:equal name="profileLocked" value="Y">
					<html:form type="emgadm.consumer.UnlockCustomerProfileForm" name="showCustomerProfileForm" action="unlockCustomerProfile">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
					<td nowrap="nowrap" class="td-small" align="left">Yes&nbsp;<input type="submit" class="transbtn2" value="Unlock Profile" /></td>	
				</html:form>
				</logic:equal>
				<logic:notEqual name="profileLocked" value="Y">
				<td nowrap="nowrap" class="td-small" align="left">No&nbsp;</td>	
				</logic:notEqual>
			</tr>
			</logic:notEqual>
			<tr>
				<html:form type="emgadm.consumer.UpdateCustomerPasswordForm" name="showCustomerProfileForm" action="updateCustomerPassword">
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.logon.id" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
					<b><c:out value="${custIdMarket}" /></b>)</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<logic:equal name="updatePw" value="Y">
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.new.password" />: </b></td>
				<td nowrap="nowrap" class="td-small">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<html:password property="newValue" value="" size="20" maxlength="20"/>
						<input type="submit" class="transbtn2" value="Set Password" />
						<font class="error">
							<html:messages id="message" property="newValue">
								<c:out value="${message}"/>
							</html:messages>
						</font>
				</td>
				</logic:equal>
				<logic:notEqual name="updatePw" value="Y">
				<td nowrap="nowrap" colspan="2"> &nbsp; </td>
				</logic:notEqual>
				</html:form>
			</tr>
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.edir.guid" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<div id="guidOuter" style="visibility:visible">
					<c:if test="${showCustomerProfileForm.EDirectoryGuid == null}">Unknown</c:if>
					<c:if test="${showCustomerProfileForm.EDirectoryGuid == ''}">Unknown</c:if>
					<c:if test="${showCustomerProfileForm.EDirectoryGuid != ''}">
						<c:out value="${showCustomerProfileForm.EDirectoryGuid}" />
						<a href="javascript:ClipBoardEdir('${showCustomerProfileForm.EDirectoryGuid}');"> (Copy)</a>
					</div>	
					<div id="guidCopy" style="display:none">
			 				<span class="error">E-Directory Guid is Copied to your clipboard</span>
					</div>
					</c:if>
				</td>
				<td>&nbsp;</td>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.rsa.enrollment.date" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<c:if test="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate == null}">
						<fmt:message key="msg.not.enrolled" />
					</c:if>
					<c:if test="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate != null}">
						<fmt:formatDate value="${showCustomerProfileForm.adaptiveAuthProfileCompleteDate}" pattern="dd/MMM/yyyy h:mm a z" />
					</c:if>
				</td>
				
			</tr>
			
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.name" />: </b></td>
				<td nowrap="nowrap" class="td-small cpName">
					<span class="contain">
					<c:out value="${showCustomerProfileForm.custFirstName}" />
					<c:if test="${showCustomerProfileForm.custMiddleName != null}">
						<c:out value="${showCustomerProfileForm.custMiddleName}" />
					</c:if>
					<c:out value="${showCustomerProfileForm.custLastName}" />
					<c:if test="${showCustomerProfileForm.custSecondLastName != null}">
					    <c:out value="${showCustomerProfileForm.custSecondLastName}" />
					</c:if>
					&nbsp;&nbsp;
					<c:set var="custId" value="${showCustomerProfileForm.custId}" />
					<logic:equal name="phoneTainted" value="N">
						<logic:equal name="updateProfile" value="Y">
							<html:link action="updateCustomerProfile.do" paramId="custId" paramName="custId"><fmt:message key="label.edit.consumer.profile" /></html:link>
						</logic:equal>
					</logic:equal>
					</span>
						<a href="http://www.facebook.com/search.php?q=<c:out value="${showCustomerProfileForm.custFirstName} ${showCustomerProfileForm.custMiddleName} ${showCustomerProfileForm.custLastName}" />" class="cpFacebook name contain" target="_blank" title="<fmt:message key="label.search.facebook" />">
							<fmt:message key="label.search.facebook" />
						</a>
					</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap" class="td-small" align="right">					
				<b><fmt:message key="label.created.partner.site.id" />:</b></td>
				<td nowrap="nowrap" class="td-small" align="left"><c:out value="${showCustomerProfileForm.partnerSiteId}" /></td>

			</tr>
			
<c:choose>  
	<c:when test="${findProfiles.partnerSiteId == 'MGOUK'}">
		<tr>
			<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.address" />: </b></td>
			<td class="td-small address_cell">
				<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_address' class='miscdatatable'>
					<thead></thead>
					<tbody>
						<tr class="oddrow">
							<td class="highlight">Address Line 1</td>
							<td><c:out value="${showCustomerProfileForm.addressLine1}" /></td>
							<td class="highlight">Town/City</td>
							<td><c:out value="${showCustomerProfileForm.city}" /></td>
						</tr>
						<tr>
							<td class="highlight">Address Line 2</td>
							<td>
								<c:if test="${showCustomerProfileForm.addressLine2 != null}">
									<c:out value="${showCustomerProfileForm.addressLine2}" />
								</c:if>
							</td>
							<td class="highlight">Postal Code</td>
							<td><c:out value="${showCustomerProfileForm.postalCode}" /><c:if test="${showCustomerProfileForm.zip4 != null}"><c:out value="${showCustomerProfileForm.zip4}" /></c:if></td>
						</tr>
						<tr class="oddrow">
							<td class="highlight">County</td>
							<td><c:out value="${showCustomerProfileForm.countyName}" /></td>
							<td class="highlight">Country</td>
							<td><c:out value="${showCustomerProfileForm.isoCountryCode}" /></td>
						</tr>
					</tbody>
				</table>
				<a href='../../viewAddressHistory.do?custId=<c:out value="${showCustomerProfileForm.custId}" />&country=<c:out value="${showCustomerProfileForm.isoCountryCode}" />'><fmt:message key="link.view.address.history" /></a>
			</td>
			<td nowrap="nowrap" class="td-small"> &nbsp; </td>
			<td nowrap="nowrap"  class="td-small"align="right"><b>Identification: </b></td>
			<td class="td-small id_cell">
				<div class="content_toggler">
				<div class="content_toggler_content">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
								<td class="highlight">
									Consumer<br />
									<c:out value="${showCustomerProfileForm.idTypeFriendlyName}" />
								</td>
								<td><c:out value="${showCustomerProfileForm.idNumber}" /></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2 content_toggler_button" value="Change" disabled="disabled" />
				</div>
				<html:messages id="sssnerror" property='custSsn'>
					<div class="content_toggler_content content_toggler_default">
				</html:messages>
				<c:if test="${sssnerror==null || ssnerror ==''}">
					<div class="content_toggler_content">
				</c:if>
				<html:form action="updateCustomerSsn">
					<logic:equal name="updateSSN" value="Y">
					<c:if test="${showCustomerProfileForm.custSsnTaintText != 'Blocked'}">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
									<td class="highlight"><fmt:message key="label.new.ssn" /></td>
									<td>
										<html:hidden name="showCustomerProfileForm" property="custId"/>
										<html:hidden name="showCustomerProfileForm" property="custSsnTaintText"/>
										<html:hidden name="showCustomerProfileForm" property="saveCustSsn"/>
										<html:text property="custSsn" value="${showCustomerProfileForm.custSsn}" />
										<span class="error">
											<html:messages id="message" property='custSsn'>
												<c:out value="${message}"/>
											</html:messages>
										</span>
									</td>
								</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2" value="Change SSN" />
					</c:if>
					</logic:equal>
				<logic:equal name="duplicateProfileOverride" value="Y">
					<logic:equal name="duplicateOverrideSSN" value="Y">
						Allow Duplicate:</b><html:checkbox  property="allowOverride" /></td>
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:equal>
				<logic:empty name="duplicateProfileOverride" >
					<logic:equal name="duplicateOverrideSSN" value="Y">
						<html:hidden  value="false" property="allowOverride" />
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:empty>
				<logic:notEqual name="updateSSN" value="Y">
				<td nowrap="nowrap" colspan="2"> &nbsp; </td>
				</logic:notEqual>
				</html:form>				
					</div>
					</div>
				</td>
			</tr>
	</c:when>  
	<c:when test="${findProfiles.partnerSiteId == 'MGODE'}">
		<tr>
			<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.address" />: </b></td>
			<td class="td-small address_cell">
				<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_address' class='miscdatatable'>
					<thead></thead>
					<tbody>
						<tr class="oddrow">
							<td class="highlight">Building No.</td>
							<td><c:out value="${showCustomerProfileForm.buildingName}" /></td>
							<td class="highlight">Postal Code</td>
							<td><c:out value="${showCustomerProfileForm.postalCode}" /><c:if test="${showCustomerProfileForm.zip4 != null}"><c:out value="${showCustomerProfileForm.zip4}" /></c:if></td>
						</tr>
						<tr>
							<td class="highlight">Street</td>
							<td>
								<c:if test="${showCustomerProfileForm.addressLine1 != null}">
									<c:out value="${showCustomerProfileForm.addressLine1}" />
								</c:if>
							</td>
							<td class="highlight">Country</td>
							<td><c:out value="${showCustomerProfileForm.isoCountryCode}" /></td>
						</tr>
						<tr class="oddrow">
							<td class="highlight">Town/City</td>
							<td><c:out value="${showCustomerProfileForm.city}" /></td>
							<td/><td/>
						</tr>
					</tbody>
				</table>
				<a href='../../viewAddressHistory.do?custId=<c:out value="${showCustomerProfileForm.custId}" />&country=<c:out value="${showCustomerProfileForm.isoCountryCode}" />'><fmt:message key="link.view.address.history" /></a>
			</td>
			<td nowrap="nowrap" class="td-small"> &nbsp; </td>
			<td nowrap="nowrap"  class="td-small"align="right"><b>Identification: </b></td>
			<td class="td-small id_cell">
				<div class="content_toggler">
				<div class="content_toggler_content">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
								<td class="highlight">
									<c:out value="${showCustomerProfileForm.idTypeFriendlyName}" />
								</td>
								<td style="font-size: smaller; "><c:out value="${showCustomerProfileForm.idNumber}" /></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2 content_toggler_button" value="Change" disabled="disabled" />
				</div>
				<html:messages id="sssnerror" property='custSsn'>
					<div class="content_toggler_content content_toggler_default">
				</html:messages>
				<c:if test="${sssnerror==null || ssnerror ==''}">
					<div class="content_toggler_content">
				</c:if>
				<html:form action="updateCustomerSsn">
					<logic:equal name="updateSSN" value="Y">
					<c:if test="${showCustomerProfileForm.custSsnTaintText != 'Blocked'}">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
									<td class="highlight"><fmt:message key="label.new.ssn" /></td>
									<td>
										<html:hidden name="showCustomerProfileForm" property="custId"/>
										<html:hidden name="showCustomerProfileForm" property="custSsnTaintText"/>
										<html:hidden name="showCustomerProfileForm" property="saveCustSsn"/>
										<html:text property="custSsn" value="${showCustomerProfileForm.custSsn}" />
										<span class="error">
											<html:messages id="message" property='custSsn'>
												<c:out value="${message}"/>
											</html:messages>
										</span>
									</td>
								</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2" value="Change SSN" />
					</c:if>
					</logic:equal>
				<logic:equal name="duplicateProfileOverride" value="Y">
					<logic:equal name="duplicateOverrideSSN" value="Y">
						Allow Duplicate:</b><html:checkbox  property="allowOverride" /></td>
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:equal>
				<logic:empty name="duplicateProfileOverride" >
					<logic:equal name="duplicateOverrideSSN" value="Y">
						<html:hidden  value="false" property="allowOverride" />
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:empty>
				<logic:notEqual name="updateSSN" value="Y">
				<td nowrap="nowrap" colspan="2"> &nbsp; </td>
				</logic:notEqual>
				</html:form>				
					</div>
					</div>
				</td>
			</tr>
			<html:form action="approveDocument" >
			<html:hidden name="showCustomerProfileForm" property="idExtnlId" />
			<html:hidden name="showCustomerProfileForm" property="custId"/>
			<input type="hidden" name="docStatus" value="<bean:write name="showCustomerProfileForm" property="docStatusString" />" />
			<input type="hidden" name="consumerStatus" value="<bean:write name="showCustomerProfileForm" property="custStatus" />" />
			<tr>
				<td/>
				<td/>
				<td/>
				<td nowrap="nowrap"  class="td-small"align="right"><b>Document Status: </b></td>
				<td nowrap="nowrap" class="td-small"><c:out value="${showCustomerProfileForm.docStatusString}" /></td>
				<td colspan="2"/>
				<td/>
			</tr>
		 	<%
		 		
				emgadm.security.ConsumerSecurityMatrix matrix=(emgadm.security.ConsumerSecurityMatrix) request.getSession().getAttribute("consumerAuth");
			%>
			<tr>
				<td colspan="3"/>
				<td/>
				<td align="left">
					<table cellpadding="0" cellspacing="0" width="80%" border="0" align="left">
					  <tr>
						<logic:equal name="consumerAuth" property="owner" value="true">
						 	<c:if test="${consumerAuth.approveDocument == true}">
						 		<td>
							  <div class='TD-SMALL' align='center' >
							  	<c:out value="${consumerAuth.approveDocumentButton}" escapeXml="false"></c:out>
							  </div>
							  </td>
						  	</c:if>
						</logic:equal>
						<logic:equal name="consumerAuth" property="owner" value="true">
						 	<c:if test="${consumerAuth.denyDocument == true}">
						 		<td>
							  <div class='TD-SMALL' align='center' >
							  	<c:out value="${consumerAuth.denyDocumentButton}" escapeXml="false"></c:out>
							  </div>
							  </td>
						  	</c:if>
						</logic:equal>
						<logic:equal name="consumerAuth" property="owner" value="true">
						 	<c:if test="${consumerAuth.pendDocument == true}">
						 		<td>
							  <div class='TD-SMALL' align='center' >
							  	<c:out value="${consumerAuth.pendDocumentButton}" escapeXml="false"></c:out>
							  </div>
							  </td>
						  	</c:if>
						</logic:equal>
					  </tr>
					</table>
				</td>
				
				<td colspan="3"/>
			</tr>
			</html:form>
	</c:when>
	<c:otherwise>
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.address" />: </b></td>
				<td class="td-small address_cell">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_address' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
								<td class="highlight">Address 1</td>
								<td><c:out value="${showCustomerProfileForm.addressLine1}" /></td>
								<td class="highlight">Town/City</td>
								<td><c:out value="${showCustomerProfileForm.city}" /></td>
							</tr>
							<tr>
								<td class="highlight">Address 2</td>
								<td>
								<c:if test="${showCustomerProfileForm.addressLine2 != null}">
									<c:out value="${showCustomerProfileForm.addressLine2}" />
								</c:if>
								</td>
								<td class="highlight">Zip Code</td>
								<td><c:out value="${showCustomerProfileForm.postalCode}" /><c:if test="${showCustomerProfileForm.zip4 != null}">-<c:out value="${showCustomerProfileForm.zip4}" /></c:if></td>
							</tr>
							<tr class="oddrow">
								<td class="highlight">State</td>
								<td><c:out value="${showCustomerProfileForm.state}" /></td>
								<td class="highlight">Country</td>
								<td><c:out value="${showCustomerProfileForm.isoCountryCode}" /></td>
							</tr>
						</tbody>
					</table>
					<a href='../../viewAddressHistory.do?custId=<c:out value="${showCustomerProfileForm.custId}" />'><fmt:message key="link.view.address.history" /></a>
				</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap"  class="td-small"align="right"><b>Identification: </b></td>
				<td class="td-small id_cell">
					<div class="content_toggler">
					<div class="content_toggler_content">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
							<tr class="oddrow">
								<td class="highlight"><fmt:message key="label.consumer.ssn" /></td>
								<td><c:out value="${showCustomerProfileForm.custSsn}" /></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2 content_toggler_button" value="Change" />
					</div>
					<html:messages id="sssnerror" property='custSsn'>
						<div class="content_toggler_content content_toggler_default">
					</html:messages>
					<c:if test="${sssnerror==null || ssnerror ==''}">
						<div class="content_toggler_content">
					</c:if>
					<html:form action="updateCustomerSsn">
					<logic:equal name="updateSSN" value="Y">
					<c:if test="${showCustomerProfileForm.custSsnTaintText != 'Blocked'}">
					<table border='0' width='100%' cellpadding='0' cellspacing='0' id='consumer_ssn' class='miscdatatable'>
						<thead></thead>
						<tbody>
								<tr class="oddrow">
									<td class="highlight"><fmt:message key="label.new.ssn" /></td>
									<td>
										<html:hidden name="showCustomerProfileForm" property="custId"/>
										<html:hidden name="showCustomerProfileForm" property="custSsnTaintText"/>
										<html:hidden name="showCustomerProfileForm" property="saveCustSsn"/>
										<html:text property="custSsn" value="${showCustomerProfileForm.custSsn}" />
										<span class="error">
											<html:messages id="message" property='custSsn'>
												<c:out value="${message}"/>
											</html:messages>
										</span>
									</td>
								</tr>
						</tbody>
					</table>
					<input type="submit" class="transbtn2" value="Change SSN" />
					</c:if>
					</logic:equal>
				<logic:equal name="duplicateProfileOverride" value="Y">
					<logic:equal name="duplicateOverrideSSN" value="Y">
						Allow Duplicate:</b><html:checkbox  property="allowOverride" /></td>
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:equal>
				<logic:empty name="duplicateProfileOverride" >
					<logic:equal name="duplicateOverrideSSN" value="Y">
						<html:hidden  value="false" property="allowOverride" />
						<span class="error">
							<html:messages id="message" property="custSSNUpdateStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
				</logic:empty>

				<logic:notEqual name="updateSSN" value="Y">
				<td nowrap="nowrap" colspan="2"> &nbsp; </td>
				</logic:notEqual>
				</html:form>				
					</div>
					</div>
				</td>
			</tr>
	</c:otherwise>  
</c:choose> 	
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.date.of.birth" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<c:out value="${showCustomerProfileForm.brthDate}" /> - (<c:out value="${showCustomerProfileForm.custAge}" />)
				</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap" class="td-small" align="right"><b>Gender: </b></td>
				<td nowrap="nowrap" class="td-small">
					<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGOUK' || showCustomerProfileForm.partnerSiteId == 'MGODE'}">
						<c:out value="${showCustomerProfileForm.gender}" />
					</c:if>
				</td>				
			</tr>

			<tr class="rewards_row">
				<td nowrap="nowrap" class="td-small" align="right"><b>Plus #:&nbsp;</b> </td>
				<td nowrap="nowrap" class="td-small">
					<c:if test="${showCustomerProfileForm.loyaltyPgmMembershipId == null}">						
						<fmt:message key="msg.not.enrolled" />
					</c:if>
					<c:if test="${showCustomerProfileForm.loyaltyPgmMembershipId != null}">
						<c:out value="${showCustomerProfileForm.loyaltyPgmMembershipId}" /> &nbsp;
					</c:if>
					</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap" class="td-small" align="right"><b>Plus Auto Enroll Flag: </b></td>
				<td nowrap="nowrap" class="td-small">
				
					<%--Defect 711 Changes Starts
					 <c:out value="${showCustomerProfileForm.custAutoEnrollFlag}" /> --%>
					 <c:if test="${showCustomerProfileForm.loyaltyPgmMembershipId == null}">						
						<c:out value="N"/>
					</c:if>
					<c:if test="${showCustomerProfileForm.loyaltyPgmMembershipId != null}">
					<c:out value="Y"/>
					</c:if>
					<%--Defect 711 Changes Ends --%>
					 </td>
			</tr>
			<tr>
				<html:form type="emgadm.consumer.UpdateCustomerStatusForm" name="UpdateCustomerStatusForm" action="updateCustomerStatus">
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.primary.phone" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<c:out value="${showCustomerProfileForm.phoneNumberFormatted}" /><c:if test="${showCustomerProfileForm.phoneNumberTaintText != 'Not Blocked'}"><font color="red"><b> &nbsp; (<c:out value="${showCustomerProfileForm.phoneNumberTaintText}" />)</b></font></c:if>
					<c:if test="${findProfiles.partnerSiteId == 'MGOUK' || findProfiles.partnerSiteId == 'MGODE'}">
						<c:out value="${findProfiles.phoneNumberType}" />
					</c:if>
					<logic:equal name="showCustomerProfileForm" property="phoneNumberTaintText" value="Not Blocked">
					<logic:equal name="addBlockedPhone" value="Y">
					<logic:notEmpty name="showCustomerProfileForm" property="phoneNumber">
						&nbsp; &nbsp; <a href='../../showPhoneBlockedList.do?phNbr=<c:out value="${showCustomerProfileForm.phoneNumber}" />&submitNew=add'>Block this Phone</a>
					</logic:notEmpty>
					</logic:equal>
					</logic:equal>
					</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.status" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<html:hidden name="showCustomerProfileForm" property="custId"/>
					<html:select name="showCustomerProfileForm" property="custStatus">
						<html:optionsCollection name="showCustomerProfileForm" property="custStatusOptions"/>
					</html:select>
					<input type="hidden" name="currentCustStatus" value="<bean:write name="showCustomerProfileForm" property="custStatus" />" />
					<logic:equal name="updateStatus" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						<span class="error">
							<html:messages id="message" property="custStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</logic:equal>
					<logic:equal name="updateStatus" value="N">
						<logic:equal name="duplicateOverride" value="N">
							<html:hidden name="UpdateCustomerStatusForm" property="allowOverride"/>
							<span class="error">
								<html:messages id="message" property="custStatus">
									<c:out value="${message}"/>
								</html:messages>
							</span>
						</logic:equal>
					</logic:equal>
					</td>
					</tr>
					<logic:equal name="duplicateProfileOverride" value="Y">
						<logic:equal name="duplicateOverride" value="Y">
							<tr><td colspan='3'></td>
							<td nowrap="nowrap" class="td-small" align="right"><b>
							Allow Duplicate:</b><html:checkbox name="UpdateCustomerStatusForm"  property="allowOverride" /></td>
							<td><span class="error">
								<html:messages id="message" property="custStatus">
									<c:out value="${message}"/>
								</html:messages>
							</span></td></tr>
						</logic:equal>
					</logic:equal>
					<logic:empty name="duplicateProfileOverride" >
						<logic:equal name="duplicateOverride" value="Y">
							<tr><td colspan='3'></td>
							<td nowrap="nowrap" class="td-small" align="right">&nbsp;
							<html:hidden name="UpdateCustomerStatusForm" value="false" property="allowOverride" /></td>
							<td><span class="error">
								<html:messages id="message" property="custStatus">
									<c:out value="${message}"/>
								</html:messages>
							</span></td></tr>
						</logic:equal>
					</logic:empty>
			<tr>
				<td nowrap="nowrap" class="td-small" align="right">
					<b><fmt:message key="label.alternate.phone" />: <c:if test="${showCustomerProfileForm.phoneNumberAlternateTaintText != 'Not Blocked'}"><font color="red"><b> &nbsp; (<c:out value="${showCustomerProfileForm.phoneNumberAlternateTaintText}" />)</b></font></c:if></b>
				</td>
				</html:form>
				<html:form type="emgadm.consumer.UpdateCustomerEmailForm" name="UpdateCustomerEmailForm" action="updateCustomerEmail">
				<td nowrap="nowrap" class="td-small">
					<c:out value="${showCustomerProfileForm.phoneNumberAlternateFormatted}" />
					<c:if test="${findProfiles.partnerSiteId == 'MGOUK' || findProfiles.partnerSiteId == 'MGODE'}">
						<c:out value="${findProfiles.phoneNumberAlternateType}" />
					</c:if>
					<logic:equal name="showCustomerProfileForm" property="phoneNumberAlternateTaintText" value="Not Blocked">
					<logic:equal name="addBlockedPhone" value="Y">
					<logic:notEmpty name="showCustomerProfileForm" property="phoneNumberAlternate">
						&nbsp; &nbsp; <a href='../../showPhoneBlockedList.do?phNbr=<c:out value="${showCustomerProfileForm.phoneNumberAlternate}" />&submitNew=add'>Block this Phone</a>
					</logic:notEmpty>
					</logic:equal>
					</logic:equal>
					</td>
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.consumer.email.address" />: </b></td>
				<td nowrap="nowrap" class="td-small cpEmail">
				<span class="contain">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<c:out value="${showCustomerProfileForm.emailAddress}" />
						<c:if test="${showCustomerProfileForm.emailAddressTaintText != 'Not Blocked'}">
							<font color="red">
								<b> &nbsp;
									(<c:out value="${showCustomerProfileForm.emailAddressTaintText}" />)
								</b>
							</font>
						</c:if>
						<c:if test="${showCustomerProfileForm.emailDomainTaintText != 'Not Blocked'}">
							<font color="red">
								<b> &nbsp;
									(Domain <c:out value="${showCustomerProfileForm.emailDomainTaintText}" />)
								</b>
							</font>
						</c:if>

					<c:if test="${showCustomerProfileForm.emailAddressTaintText != 'Blocked'}">
						<c:choose>
							<c:when test="${showCustomerProfileForm.isEmailActive == false}">
							<logic:equal name="updateEmail" value="Y">
								<input type="submit" class="transbtn2" value="Activate Email" />
							</logic:equal>
							</c:when>
						</c:choose>
						<span class="error">
							<html:messages id="message" property="emailStatus">
								<c:out value="${message}"/>
							</html:messages>
						</span>
					</c:if>
					<logic:equal name="showCustomerProfileForm" property="emailAddressTaintText" value="Not Blocked">
					<logic:equal name="addBlockedEmail" value="Y">
					<logic:notEmpty name="showCustomerProfileForm" property="emailAddress">
						&nbsp; &nbsp; <a href='../../showEmailBlockedList.do?emailAddress=<c:out value="${showCustomerProfileForm.emailAddress}" />&emailDomain=%20&submitAdd=add'>Block this Email</a>
					</logic:notEmpty>
					</logic:equal>
					</logic:equal>
					</span>
					<a href="http://www.facebook.com/search.php?q=<c:out value="${showCustomerProfileForm.emailAddress}" />" class="cpFacebook email contain" target="_blank" title="<fmt:message key="label.search.facebook" />">
						<fmt:message key="label.search.facebook" />
					</a>
				</td>
				</html:form>
			</tr>

			<tr>
			<html:form type="emgadm.consumer.UpdateCustPremierCodeForm" name="UpdateCustPremierCodeForm" action="updateCustPremierCode">
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.cust.premier.code" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<html:hidden name="showCustomerProfileForm" property="custId"/>
					<html:select name="showCustomerProfileForm" property="prmrCode">
						<html:optionsCollection name="showCustomerProfileForm" property="custPrmrCodes"/>
					</html:select>
					<logic:equal name="updatePrmrCode" value="Y">
						<input width="29" type="submit" class="transbtn2" value="Update" />
					</logic:equal>
					<fmt:formatDate value="${showCustomerProfileForm.prmrCodeDate}" pattern="dd/MMM/yyyy h:mm a z" />
					<span class="error">
						<html:messages id="message" property="prmrCode">
							<c:out value="${message}"/>
						</html:messages>
					</span>
					</td>
				</html:form>
				
				<td nowrap="nowrap" class="td-small"> &nbsp; </td>

				<html:form type="emgadm.consumer.UpdateCustomerProfileForm" name="UpdateCustomerPromoForm" action="updateCustomerPromo">
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.accept.promo.emails" />: </b></td>
				<td nowrap="nowrap" class="td-small">
					<html:hidden name="showCustomerProfileForm" property="custId"/>
         		<html:hidden name="showCustomerProfileForm" property="acceptPromotionalEmail"/>
					<c:if test="${showCustomerProfileForm.acceptPromotionalEmail == true}">
						<fmt:message key="label.yes" />
						<logic:equal name="updatePromo" value="Y">
							<input type="submit" class="transbtn2" value="Stop Emails" />
						</logic:equal>
					</c:if>
					<c:if test="${showCustomerProfileForm.acceptPromotionalEmail == false}">
						<fmt:message key="label.no" />
						<logic:equal name="updatePromo" value="Y">
							<input type="submit" class="transbtn2" value="Send Emails" />
						</logic:equal>
					</c:if>
				</td>
				</html:form>
			</tr>


			<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGODE' && showCustomerProfileForm.isIdImageAvailable == true}">
				<tr>
				  <td nowrap="nowrap" class="td-small"> &nbsp; </td>
				  <td nowrap="nowrap" class="td-small"> &nbsp; </td>
				  <td nowrap="nowrap" class="td-small"> &nbsp; </td>
				  <td nowrap="nowrap" class="td-small" align="left">
				  <a href='../../showCustomerProfile.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>&basicFlag=N&action=ViewIdImage'
				  	 onclick="openWindow('<c:out value="${pageContext.request.contextPath}" />/showCustomerProfile.do?custId=<c:out value="${showCustomerProfileForm.custId}"/>&basicFlag=N&action=ViewIdImage');return false;">
				  	 View Scanned Document
				  </a>
				</tr>
			</c:if>
			<logic:equal name="addCustCmt" value="Y">
			<html:form type="emgadm.consumer.AddCustomerCommentForm" name="AddCustomerCommentForm" action="addCustomerComment">
			<html:hidden name="showCustomerProfileForm" property="custId"/>
			<tr>
				<td nowrap="nowrap" colspan='5' class='TD-HEADER-SMALL'>Customer Profile Comments <a href="../../addCustomerComment.do?custId=<c:out value="${showCustomerProfileForm.custId}" />&commentReasonCode=IP&commentText=<c:out value="${showCustomerProfileForm.createdIPAddress}" />">Add IP Lookup comment</a></td>
			</tr>
			<tr>
				<td nowrap="nowrap" class="td-small" align="right"><b><fmt:message key="label.comment.reason" />: </b></td>
				<td colspan="4" nowrap="nowrap" class="td-small" valign="top">
					<html:select property="commentReasonCode" value="">
						<html:option value="">Select One </html:option>
						<html:option value=" ">------------------- </html:option>
						<html:optionsCollection name="showCustomerProfileForm"	property="customerCommentReasons"/>
					</html:select>
					<span class="error">
						<html:messages id="message" property="commentReason">
							<c:out value="${message}"/>
						</html:messages>
					</span></td>
			</tr>

			<tr>
				<td nowrap="nowrap" class="td-small" align="right" valign="top"><b><fmt:message key="label.consumer.comment" />: </b></td>
				<td colspan="4" nowrap="nowrap" class="td-small" valign="top">
					<html:textarea property="commentText" rows="4" cols="80" onkeypress="textCounter(this, 255);" />
					<input type="submit" class="transbtn2" value="Add Comment" />
					</td>
			</tr>
			</html:form>
			</logic:equal>
		</tbody>
	</table>

<% String dispClass = "td-small"; 
   String dispClass2 = "TD-SHADED3-SMALL";%>

<logic:notEqual name="noComments" value="Y">
	<table width="100%">
		<thead>
			<tr>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.create.date" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.comment" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.create.user" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.comment.reason" /></td>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="comment" items="${showCustomerProfileForm.comments}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${comment.createDate}" pattern="dd/MMM/yyyy h:mm a z" /></td>
				<td class="<%=dispClass%>">
					<c:out value="${comment.text}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${comment.createdBy}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${comment.reasonDescription}" /></td>
			</tr>
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
		</tbody>
	</table>
</logic:notEqual>
<hr />

<logic:notEqual name="noActivities" value="Y">
	<table width="100%">
		<thead>
			<tr>
				<td nowrap="nowrap" colspan='4' class='TD-HEADER-SMALL'>Customer Profile Activity Log</td>
			</tr>
			<tr>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.activity.date" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.activity.description" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.activity.category" /></td>
				<td nowrap="nowrap" class='TD-HEADER-SMALL'><fmt:message key="label.create.user" /></td>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="acty" items="${showCustomerProfileForm.consumerProfileActivity}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${acty.activityDate}" pattern="dd/MMM/yyyy h:mm a z" /></td>
				<td class="<%=dispClass%>">
					<c:out value="${acty.activityBusinessDescription}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${acty.activityBusinessCategory}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${acty.createdBy}" /></td>
			</tr>
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
		</tbody>
	</table>
</logic:notEqual>

</fieldset>

<img height="20" src="../../images/trans.gif" width="10">

<c:if test="${showCustomerProfileForm.partnerSiteId == 'MGODE'}">
	<fieldset class="fieldset1">
		<legend class="legend">
			<fmt:message key="label.gbgroup" />:
		</legend>
		<table width="60%" align="center">
			<tr>
				<c:if test="${not empty gbGroupLog}">
				<td nowrap="nowrap" class="td-small viewGBGroupLogDetailsTrigger">
					<a name="gbGroupDetailLink"
					href='../../showCustomerProfile.do'>
						<c:out value="${showCustomerProfileForm.viewGBGroupLogLinkString}" />
				</a></td>
				</c:if>
				<c:if test="${empty gbGroupLog}">
					<td class="td-small">
						<fmt:message key="label.gbgroup.noinfo"></fmt:message>
					</td>
				</c:if>
			</tr>
		</table>
	</fieldset>
	<br/>
</c:if>

<logic:equal name="showCustomerProfileForm" property="basicFlag" value="N">


<fieldset class="fieldset1">
	<legend class="legend">
		<fmt:message key="label.bank.accounts" />:
	</legend>
	<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
		<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noBankAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

	<logic:notEqual name="noBankAccts" value="Y">

	<% dispClass = "td-small"; %>

	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<thead>
			<tr>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.account.number" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.routing.number" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.bank" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.account.type" /></td>
				<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="bankAccount" items="${showCustomerProfileForm.bankAccounts}">
				<tr>
					<html:form type="emgadm.account.UpdateAccountStatusForm" name="UpdateAccountStatusForm" action="updateAccountStatus">
					<td nowrap="nowrap" class="<%=dispClass%>">
						<a href='../../viewBankAccountDetail.do?accountId=<c:out value="${bankAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
							<c:out value="*****${bankAccount.accountNumberMask}" />
						</a> &nbsp;
						<c:if test="${bankAccount.accountTaintText != 'Not Blocked'}">
							<font color="red"><b> &nbsp; (ACCT: <c:out value="${bankAccount.accountTaintText}" />)</b></font>
						</c:if>
					</td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.routingNumber}" />
						<c:if test="${bankAccount.bankAbaTaintText != 'Not Blocked'}">
							<font color="red"><b> &nbsp; (ABA: <c:out value="${bankAccount.bankAbaTaintText}" />)</b></font>
						</c:if>
					</td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.financialInstitutionName}" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${bankAccount.accountTypeDesc}" /></td>
					<td nowrap="nowrap" class="<%=dispClass%>">
							<html:hidden name="showCustomerProfileForm" property="custId"/>
							<html:hidden name="bankAccount" property="accountId" />
							<html:hidden name="bankAccount" property="accountTypeCode" />
							<html:select name="bankAccount" property="combinedStatusCode">
								<html:optionsCollection name="showCustomerProfileForm" property="accountStatusOptions"/>
							</html:select>
							<logic:equal name="updateAcct" value="Y">
							<input type="submit" class="transbtn2" value="Update Status" />
							</logic:equal>
					</td>
					<logic:present name="bankAccount" property="microDeposit">
					<td nowrap="nowrap" class="<%=dispClass%>">
						<b><fmt:message key="label.md.status" />: </b>
						<c:out value="${bankAccount.microDeposit.status.mdStatus}" />
						(<c:out value="${bankAccount.microDeposit.status.mdStatusDesc}" />)
						<logic:equal name="bankAccount" property="statusCode" value="ACT">
						<logic:equal name="validateMD" value="Y">
						<c:if test="${bankAccount.microDeposit.status.mdStatus == 'ACS'}">
							&nbsp; <a href='../../validateMicroDeposit.do?accountId=<c:out value="${bankAccount.accountId}"/>'>Validate Micro Deposits</a>
						</c:if>
						</logic:equal>
						</logic:equal>
					</td>
					</logic:present>
					<logic:notPresent name="bankAccount" property="microDeposit">
					<td nowrap="nowrap" class="<%=dispClass%>"> &nbsp; </td>
					</logic:notPresent>
					</html:form>
				</tr>

					<logic:notEmpty name="bankAccount" property="cmntList">
					<bean:define id="cmnts" name="bankAccount" property="cmntList" />
				<tr>
					<td colspan="5">
					<table border='0' cellpadding='1' cellspacing='1'>
					<% dispClass2 ="TD-SHADED3-SMALL";%>
					<logic:iterate id="cmnt" name="cmnts">
					<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
							dispClass2 = "TD-SHADED3-SMALL";
						} else {
						dispClass2 = "TD-SHADED2-SMALL";
						}  %>
					
					
						<tr>
							<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
								<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp;
								<fmt:formatDate value="${cmnt.createDate}" pattern="dd/MMM/yyyy hh:mm:ss a z"/> -
								<bean:write name="cmnt" property="createdBy" /> -
								<bean:write name="cmnt" property="reasonDescription" />
								</b></font></td>
							<td class="<%=dispClass2%>">
								<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
						</tr>
					</logic:iterate>
					</table>
					</td>
				</tr>
					</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

			</c:forEach>
		</tbody>
	</table>
	</logic:notEqual>
</fieldset>

<img height="20" src="../../images/trans.gif" width="10">

<fieldset class="fieldset1"><legend class="legend"><fmt:message key="label.credit.accounts" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noCreditAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noCreditAccts" value="Y">

<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.number" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.expiration.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.issuer" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.type" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="creditCardAccount" items="${showCustomerProfileForm.creditCardAccounts}" varStatus="status">
			<tr>
				<html:form type="emgadm.account.UpdateAccountStatusForm" name="UpdateAccountStatusForm" action="updateAccountStatus">
				<td nowrap="nowrap" class="<%=dispClass%>">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${creditCardAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
						<c:out value="${creditCardAccount.accountNumberMask}" />
					</a>
					<c:if test="${creditCardAccount.accountTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (ACCT: <c:out value="${creditCardAccount.accountTaintText}" />)</b></font>
					</c:if>
					<c:if test="${creditCardAccount.creditCardBinTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (BIN: <c:out value="${creditCardAccount.creditCardBinTaintText}" />)</b></font>
					</c:if>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.expirationMonth}/${creditCardAccount.expirationYear}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.issuer}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${creditCardAccount.creditOrDebitCode}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<html:hidden name="creditCardAccount" property="accountId" />
						<html:hidden name="creditCardAccount" property="accountTypeCode" />
						<html:select name="creditCardAccount" property="combinedStatusCode">
							<html:optionsCollection name="showCustomerProfileForm" property="accountStatusOptions"/>
						</html:select>
						<logic:equal name="updateAcct" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						</logic:equal>
				</td>
				</html:form>
			</tr>

				<logic:notEmpty name="creditCardAccount" property="cmntList">
				<bean:define id="cmnts" name="creditCardAccount" property="cmntList" />
			<tr>
				<td colspan="5">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
					<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
							dispClass2 = "TD-SHADED3-SMALL";
						} else {
						dispClass2 = "TD-SHADED2-SMALL";
				}  %>
					
					<tr>
						<td width='10'>&nbsp;</td>
						<td nowrap="nowrap" class="<%=dispClass2 %>" valign="top">
							<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp;
							<fmt:formatDate value="${cmnt.createDate}" pattern="dd/MMM/yyyy hh:mm:ss a z"/> -
							<bean:write name="cmnt" property="createdBy" /> -
							<bean:write name="cmnt" property="reasonDescription" />
							</b></font></td>
						<td class="<%=dispClass2 %>">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>

<img height="20" src="../../images/trans.gif" width="10">

<fieldset class="fieldset1"><legend class="legend"><fmt:message key="label.debit.accounts" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noDebitAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noDebitAccts" value="Y">

<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.debit.card.number" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.expiration.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.issuer" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.type" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="debitCardAccount" items="${showCustomerProfileForm.debitCardAccounts}" varStatus="status">
			<tr>
				<html:form  type="emgadm.account.UpdateAccountStatusForm" name="UpdateAccountStatusForm" action="updateAccountStatus">
				<td nowrap="nowrap" class="<%=dispClass%>">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${debitCardAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
						<c:out value="*****${debitCardAccount.accountNumberMask}" />
					</a>
					<c:if test="${debitCardAccount.accountTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (ACCT: <c:out value="${debitCardAccount.accountTaintText}" />)</b></font>
					</c:if>
					<c:if test="${debitCardAccount.creditCardBinTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (BIN: <c:out value="${debitCardAccount.creditCardBinTaintText}" />)</b></font>
					</c:if>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.expirationMonth}/${debitCardAccount.expirationYear}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.issuer}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${debitCardAccount.creditOrDebitCode}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<html:hidden name="debitCardAccount" property="accountId" />
						<html:hidden name="debitCardAccount" property="accountTypeCode" />
						<html:select name="debitCardAccount" property="combinedStatusCode">
							<html:optionsCollection name="showCustomerProfileForm" property="accountStatusOptions"/>
						</html:select>
						<logic:equal name="updateAcct" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						</logic:equal>
				</td>
				</html:form>
			</tr>

				<logic:notEmpty name="debitCardAccount" property="cmntList">
				<bean:define id="cmnts" name="debitCardAccount" property="cmntList" />
			<tr>
				<td colspan="5">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
				<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
						dispClass2 = "TD-SHADED3-SMALL";
					} else {
						dispClass2 = "TD-SHADED2-SMALL";
				}  %>
				
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
							<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp;
							<fmt:formatDate value="${cmnt.createDate}" pattern="dd/MMM/yyyy hh:mm:ss a z"/> -
							<bean:write name="cmnt" property="createdBy" /> -
							<bean:write name="cmnt" property="reasonDescription" />
							</b></font></td>
						<td  class="<%=dispClass2%>" >
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>
<c:choose>
<c:when test="${findProfiles.partnerSiteId == 'MGODE'}">
<br/>
<fieldset class="fieldset1"><legend class="legend"><fmt:message key="label.maestro.accounts" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noMaestroAccts" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noMaestroAccts" value="Y">

<% dispClass = "td-small"; %>

<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.number" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.expiration.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.credit.card.type" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="maestroAccount" items="${showCustomerProfileForm.maestroAccounts}" varStatus="status">
			<tr>
				<html:form type="emgadm.account.UpdateAccountStatusForm" name="UpdateAccountStatusForm" action="updateAccountStatus">
				<td nowrap="nowrap" class="<%=dispClass%>">
					<a href='../../viewCreditCardAccountDetail.do?accountId=<c:out value="${maestroAccount.accountId}"/>&custId=<c:out value="${showCustomerProfileForm.custId}"/>'>
						<c:out value="${maestroAccount.accountNumberMask}" />
					</a>
					<c:if test="${maestroAccount.accountTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (ACCT: <c:out value="${maestroAccount.accountTaintText}" />)</b></font>
					</c:if>
					<c:if test="${maestroAccount.creditCardBinTaintText != 'Not Blocked'}">
						<font color="red"><b> &nbsp; (BIN: <c:out value="${maestroAccount.creditCardBinTaintText}" />)</b></font>
					</c:if>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<c:out value="${maestroAccount.expirationMonth}/${maestroAccount.expirationYear}" />
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>">
						<html:hidden name="showCustomerProfileForm" property="custId"/>
						<html:hidden name="maestroAccount" property="accountId" />
						<html:hidden name="maestroAccount" property="accountTypeCode" />
						<html:select name="maestroAccount" property="combinedStatusCode">
							<html:optionsCollection name="showCustomerProfileForm" property="accountStatusOptions"/>
						</html:select>
						<logic:equal name="updateAcct" value="Y">
						<input type="submit" class="transbtn2" value="Update Status" />
						</logic:equal>
				</td>
				</html:form>
			</tr>

				<logic:notEmpty name="maestroAccount" property="cmntList">
				<bean:define id="cmnts" name="maestroAccount" property="cmntList" />
			<tr>
				<td colspan="5">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
					<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
							dispClass2 = "TD-SHADED3-SMALL";
						} else {
						dispClass2 = "TD-SHADED2-SMALL";
				}  %>
					
					<tr>
						<td width='10'>&nbsp;</td>
						<td nowrap="nowrap" class="<%=dispClass2 %>" valign="top">
							<font color="#008b8b"><b>&nbsp; &nbsp; &nbsp; &nbsp;
							<fmt:formatDate value="${cmnt.createDate}" pattern="dd/MMM/yyyy hh:mm:ss a z"/> -
							<bean:write name="cmnt" property="createdBy" /> -
							<bean:write name="cmnt" property="reasonDescription" />
							</b></font></td>
						<td class="<%=dispClass2 %>">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="text" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>
</c:when>
</c:choose>

<img height="20" src="../../images/trans.gif" width="10">

<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.person.to.person.transactions" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noMGTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>



<logic:notEqual name="noMGTrans" value="Y">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.program" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.country.cd" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showCustomerProfileForm.mgTransactions}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.partnerSiteId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="/transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.countryCode}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="dd/MMM/yyyy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q
						&tranId=<bean:write filter="false" name="transaction" property="tranId"/>
						&partnerSiteId=${showCustomerProfileForm.partnerSiteId}'>
					<bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>

				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
				<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
						dispClass2 = "TD-SHADED3-SMALL";
					} else {
					dispClass2 = "TD-SHADED2-SMALL";
				}  %>
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
							<font color="#008b8b"><b>
								<fmt:formatDate value="${cmnt.date}" pattern="dd/MMM/yyyy hh:mm a z" /> -
								<bean:write name="cmnt" property="createUserId" /> -
								<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td class="<%=dispClass2%>" >
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>

<br />
<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.economy.service.transactions" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noESTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noESTrans" value="Y">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showCustomerProfileForm.esTransactions}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="/transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="dd/MMM/yyyy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>

				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
				
				<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
					dispClass2 = "TD-SHADED3-SMALL";
					} else {
					dispClass2 = "TD-SHADED2-SMALL";
					}  %>
				
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2 %>" valign="top">
							<font color="#008b8b"><b><bean:write name="cmnt" property="createDate" /> -
							<bean:write name="cmnt" property="createUserId" /> -
							<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td  class="<%=dispClass2 %>">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>
		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>

<br />

<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.express.payment.transactions" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noEPTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noEPTrans" value="Y">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showCustomerProfileForm.epTransactions}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="/transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="dd/MMM/yyyy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>

				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
					<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
						dispClass2 = "TD-SHADED3-SMALL";
					} else {
					dispClass2 = "TD-SHADED2-SMALL";
					}  %>
				
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
							<font color="#008b8b"><b><bean:write name="cmnt" property="createDate" /> -
							<bean:write name="cmnt" property="createUserId" /> -
							<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td class="<%=dispClass2%>">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>
		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>
</logic:equal>
<br />


<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.cash.transactions" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noCashTrans"  value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>



<logic:notEqual name="noCashTrans" value="Y">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.country.cd" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showCustomerProfileForm.cashTransactions}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="/transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.countryCode}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="dd/MMM/yyyy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>

				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
				<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
						dispClass2 = "TD-SHADED3-SMALL";
					} else {
					dispClass2 = "TD-SHADED2-SMALL";
				}  %>
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2%>" valign="top">
							<font color="#008b8b"><b><bean:write name="cmnt" property="createDate" /> -
							<bean:write name="cmnt" property="createUserId" /> -
							<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td class="<%=dispClass2%>" >
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>

		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>

<br />

<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.wap.delay.send.transactions" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

	<logic:equal name="noDSTrans" value="Y">
	&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
	</logic:equal>

<logic:notEqual name="noDSTrans" value="Y">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.owned.by" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.id" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.sender" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.receiver" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.amount" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.primary.funding.source" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.transaction.funding.status" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.date.time" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.trans.scores"/></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="transaction" items="${showCustomerProfileForm.dsTransactions}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.ownerId}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<html:link action="/transactionDetail.do" paramId="tranId" paramName="transaction" paramProperty="tranId">
						<c:out value="${transaction.tranId}" />
					</html:link>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.senderName}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.receiver}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.totalAmount}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.primaryFundSource}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.statusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${transaction.subStatusDesc}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<fmt:formatDate value="${transaction.statusDate}" pattern="dd/MMM/yyyy h:mm a z" />
				</td>
				<logic:equal name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>' align='right'>None</td>
				</logic:equal>
				<logic:notEqual name="transaction" property="sysAutoRsltCode" value="">
				<td nowrap='nowrap' class='<%=dispClass%>'>
					<a href='../../showTranScoreDetail.do?type=q&tranId=<bean:write filter="false" name="transaction" property="tranId"/>'><bean:write filter="false" name="transaction" property="transScores"/>-<bean:write filter="false" name="transaction" property="sysAutoRsltCode"/></a></td>
				</logic:notEqual>
			</tr>

				<logic:notEmpty name="transaction" property="tranScore">
				<bean:define id="score" name="transaction" property="tranScore" />
				<logic:notEmpty name="score" property="scoreRvwCode">
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
					<tr>
						<td nowrap="nowrap" class="td-small" valign="top">
							<font color="#008b8b"><b>Review Code: <bean:write name="score" property="scoreRvwCode" /></b></font></td>
						<td class="td-small">
							<font color="#2f4f4f"><b>(<bean:write name="score" property="tranScoreCmntText" />)</b></font></td>
					</tr>
				</table>
				</td>
			</tr>
				</logic:notEmpty>
				</logic:notEmpty>

				<logic:notEmpty name="transaction" property="cmntList">
				<bean:define id="cmnts" name="transaction" property="cmntList" />
			<tr>
				<td colspan="2"> &nbsp; </td>
				<td colspan="8">
				<table border='0' cellpadding='1' cellspacing='1'>
				<% dispClass2 = "TD-SHADED3-SMALL"; %>
				<logic:iterate id="cmnt" name="cmnts">
				
				<%  if (dispClass2.equals("TD-SHADED2-SMALL")) {
					dispClass2 = "TD-SHADED3-SMALL";
					} else {
					dispClass2 = "TD-SHADED2-SMALL";
					}  %>
				
					<tr>
						<td nowrap="nowrap" class="<%=dispClass2 %>" valign="top">
							<font color="#008b8b"><b><bean:write name="cmnt" property="createDate" /> -
							<bean:write name="cmnt" property="createUserId" /> -
							<bean:write name="cmnt" property="cmntReasDesc" />
							</b></font></td>
						<td  class="<%=dispClass2 %>">
							<font color="#2f4f4f"><b><bean:write name="cmnt" property="cmntText" /></b></font></td>
					</tr>
				</logic:iterate>
				</table>
				</td>
			</tr>
				</logic:notEmpty>

			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>
		</c:forEach>
	</tbody>
</table>
</logic:notEqual>
</fieldset>

<br />

<fieldset class="fieldset1"><legend class="legend">
<fmt:message key="label.ip.history.summary" />: </legend>
<c:out value="${showCustomerProfileForm.custLogonId}" /> &nbsp; &nbsp; (Id:
	<b><c:out value="${custIdMarket}" /></b>)

<logic:empty name="ipHistorySummaryList">
&nbsp; &nbsp; <font color="red"><b>(None)</b></font>
</logic:empty>

<logic:notEmpty name="ipHistorySummaryList">
<% dispClass = "td-small"; %>
<table border='0' width='100%' cellpadding='1' cellspacing='1'>
	<thead>
		<tr>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.consumer.ip.address" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.all.users" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.all.users" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.this.user" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.logon.count" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.first.logon.date" /></td>
			<td class='TD-HEADER-SMALL'><fmt:message key="label.last.logon.date" /></td>
		</tr>
	</thead>
	<tbody>
		<c:forEach var="ipSummary" items="${ipHistorySummaryList}">
			<tr>
				<td nowrap="nowrap" class="<%=dispClass%>">
					<div>
					<a href="#" class="toolTipCTA">
					<c:out value="${ipSummary.ipAddress}"/></a>
					<div class="toolTip addClose">
					  <p>Country :<c:out value="${ipSummary.country}" /></p>
					  <p>State :<c:out value="${ipSummary.state}" /></p>
					  <p>City :<c:out value="${ipSummary.city}" /></p>
					  <p>Postal Code :<c:out value="${ipSummary.zipCode}" /></p>
					  <p>ISP :<c:out value="${ipSummary.ISP}" /></p>
					</div>
					<a href="#" class="toolTipCTA"><span>MD</span></a>
					<div class="toolTip addClose">
					  <p>Country :<c:out value="${ipSummary.country}" /></p>
					  <p>State :<c:out value="${ipSummary.state}" /></p>
					  <p>City :<c:out value="${ipSummary.city}" /></p>
					  <p>Postal Code :<c:out value="${ipSummary.zipCode}" /></p>
					  <p>ISP :<c:out value="${ipSummary.ISP}" /></p>
					</div>
						<c:if test="${ipSummary.ipAddressTaintText == 'Not Blocked'}">
						<logic:equal name="addBlockedIp" value="Y">
							&nbsp; &nbsp; <a href='../../showIPBlockedList.do?ipAddress=<c:out value="${ipSummary.ipAddress}" />&submitAdd=add'>Block this IP</a>
						</logic:equal>
						</c:if>
						<c:if test="${ipSummary.ipAddressTaintText != 'Not Blocked'}">
							<font color="red"><b> &nbsp; (<c:out value="${ipSummary.ipAddressTaintText}" />)</b></font>
						</c:if>
					<div>
				</td>
				<td nowrap="nowrap" class="<%=dispClass%>"><a href='../../adhocReport.do?senderIP=<c:out
					value="${ipSummary.ipAddress}"/>&beginDateText=01/Jan/2003&submitSearch=Search'>
					<b>Trans for IP</b></a></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><a href='../../logonIpConsumers.do?ipAddress=<c:out
					value="${ipSummary.ipAddress}"/>' onClick="this.href='javascript:showIPHistory(\'<%=request.getContextPath()%>/logonIpConsumers.do?ipAddress=<c:out
					value="${ipSummary.ipAddress}"/>\')'">
					<b>Logins for IP</b></a></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><a href='../../adhocReport.do?profileId=<c:out
					value="${showCustomerProfileForm.custLogonId}" />&senderIP=<c:out
					value="${ipSummary.ipAddress}"/>&beginDateText=01/Jan/2003&submitSearch=Search'>
					<b>Trans for IP/Login</b></a></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><a href='<c:out value="${url}" />'
						onClick="this.href='javascript:showIPHistory(\'<c:out value="${url}" />&ipAddress=<c:out
						value="${ipSummary.ipAddress}"/>\')'">
						<b><c:out value="${ipSummary.loginCount}" /></b></a></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${ipSummary.firstLoginDate}" /></td>
				<td nowrap="nowrap" class="<%=dispClass%>"><c:out value="${ipSummary.lastLoginDate}" /></td>
			</tr>
			<%  if (dispClass.equals("td-shaded-small")) {
					dispClass = "td-small";
				} else {
					dispClass = "td-shaded-small";
				}  %>
		</c:forEach>
			<tr>
				<td nowrap="nowrap" class="td-small" colspan='4' align='center'>
					Click <a href='<c:out value="${url}" />'
						onClick="this.href='javascript:showIPHistory(\'<c:out value="${url}" />\')'">
						<b>here</b></a> to see all IPs history details.
				</td>
			</tr>
	</tbody>
</table>

</logic:notEmpty>
</fieldset>
<br/>
<logic:notEmpty name="gbGroupLog">
	<div class="gbGroupdiv">
		<html:form action="showCustomerProfile" styleClass="view">
		<fieldset>
			<table>
			  <tr>
			    <td class="td-small">
			    <b>
			    	<fmt:message key="label.decision">
			    	</fmt:message>:</b>
			    	<c:out value="${gbGroupLog.deciBandText}"></c:out> 
			    </td>
			  </tr>
			  <tr>
			  	<td class="td-small">
			  		<b>
			  			<fmt:message key="label.score">
			  			</fmt:message>:
			  		</b>
			    	<c:out value="${gbGroupLog.scoreNbr}"></c:out> 
			  	</td>
			  </tr>
			  <tr>
			  	<td>
			  	<c:if test="${not empty gbGroupLog && not empty gbGroupLogDetails}">
			  		<fieldset>
			  			<legend class="legend"><c:out value="${gbGroupLog.chkTypeDesc}"></c:out></legend>
			  				<table align="center" width="100%">
								<logic:iterate name="gbGroupLogDetails" id="detail">
								  <tr>
									<c:choose>
										<c:when test="${detail.inCommentRange}">
											<td>
												<img src="../../images/info.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inMatchRange}">
											<td>
												<img src="../../images/ok.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inWarningRange}">
											<td>
												<img src="../../images/warning.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:when test="${detail.inMismatchRange}">
											<td>
												<img src="../../images/wrong.png" height="28px" width="28px"/>
											</td>
										</c:when>
										<c:otherwise>
											<td>
												<c:out value="${detail.rsltSvrtyText}"></c:out>	
											</td>
										</c:otherwise>
									</c:choose>
								    <td class="td-small" style="font-size:x-small;">&nbsp;&nbsp;<bean:write name="detail" property="rsltCode"/></td>
								    <td class="td-small" style="font-size:x-small;">&nbsp;&nbsp;<bean:write name="detail" property="rsltDesc"/></td>
								  </tr>
								</logic:iterate>							  
							</table>
			  		</fieldset>
			  		</c:if>
			  	</td>
			  </tr>
			</table>
		</fieldset>
		<a href="#" class="viewGBGroupLogDetailsExit">Cancel/Exit</a>
		</html:form>
	</div>

	<script type="text/javascript" language="JavaScript">
		$(document).ready(function(){
		
		$('.viewGBGroupLogDetailsTrigger').click(function(e){
			e.preventDefault();
			$.EMTbox(".gbGroupdiv");
			$('html, body').css('overflow','auto');
			$('.viewGBGroupLogDetailsExit').click(function(e){
				e.preventDefault();
				$.EMTbox.close();
				$('html, body').css('overflow','hidden');
				});
			});
		});

	</script>

</logic:notEmpty></div>
<img height="20" src="../../images/trans.gif" width="10">
