<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>"/>
	<html:errors />
</span>

<center>

<h3>
		Record Consumer Refund Or Remittance Adjustment
</h3>
<br/>
<br/>

<table>
	<tr><td valign='top'><font color='#0000FF'><b>Action Description: </b></font></td>
	<td class='td-small'>
<% out.println((String)request.getAttribute("msgText")); %>
</td></tr></table><br />

<html:form action="/recordConsumerRefundOrRemittanceAdjustment">
	<table border='0' width='100%' cellpadding='1' cellspacing='1'>
		<tbody>
			<tr>
			<html:hidden property="subReasonRequired" value="N" />
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.transaction.id"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<html:hidden property="tranId" />
					<c:out value="${manualAdjustmentForm.tranId}" />
				</td>
			</tr>
			<tr>
				<td nowrap='nowrap' class='TD-LABEL'>
					<fmt:message key="label.amount"/><fmt:message key="label.suffix"/>
				</td>
				<td nowrap="nowrap" class="TD-INPUT">
					<html:text property="amount" />
				</td>
			</tr>
		</tbody>
	</table>

	<br />
	
	<div id="formButtons">
		<html:submit property="submitRecord" value="Record" 
			onclick="formButtons.style.display='none';processing.style.visibility='visible';" />
			&nbsp; &nbsp; &nbsp;
		<html:submit property="submitCancel" value="Cancel" />
	</div>
	
	<div id="processing" style="visibility: hidden">
		<table>
			<tr>
				<td class="PROCESSING"><fmt:message key="message.in.process" /></td>
			</tr>
		</table>
	</div >

</html:form>
</center>
