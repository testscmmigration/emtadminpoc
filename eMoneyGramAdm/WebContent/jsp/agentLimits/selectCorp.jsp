<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<CENTER>
<span class="error">
	<html:errors property="<%=org.apache.struts.action.ActionErrors.GLOBAL_ERROR%>" />
</span>

</CENTER><br>

<h3><fmt:message key="label.corp.selection.list"/></h3><br>

<html:form action="/billerAgentLimits.do" focus="corporationName" >
	<table border="0">
		<tbody>
			<tr>
				<td>
					<html:select property="corpSelection" size="12">
						<bean:define id="corps" name="corpSelectForm" property="corps" />
					    <html:options collection="corps" labelProperty="displayName" property="agentId"/>
	                </html:select>
				</td>

				<td>
					<input type="submit" name="submitCorp" value="Process Selection" />
				</td>
			</tr>
		</tbody>
	</table>
	<br/>
	<table border="0">
		<tbody>
			<tr>
				<td class="TD-LABEL">
					<fmt:message key="label.narrow.list"/>:
				</td>
				
				<td class="TD-INPUT">
					<html:text property="corporationName" size="20"	maxlength="35" />
				</td>
				
				<td>
					<html:submit property="submitCorpFilter" value="Narrow the List" /> 
					<html:submit property="submitClearFilter" value="Show All" />
					<html:submit property="submitCancel" value="Cancel Adding Biller Limit" /> 
				</td>
			</tr>
		</tbody>
	</table>

</html:form>
