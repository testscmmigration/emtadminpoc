<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<span  class="error">
	<div align="center"><html:errors /></div>
</span>

<h3><fmt:message key="label.change.agent.limit"/></h3>

<html:form action="/changeLimit.do" focus="limitAmt">
	<br />
	<table border="0">
	    <tbody>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.id"/>: </td>
	            <td><bean:write name="agent" property="agentId" scope="session" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.name"/>: </td>
	            <td><bean:write name="agent" property="agentName" scope="session" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.city"/>: </td>
	            <td><bean:write name="agent" property="agentCity" scope="session" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.state"/>: </td>
	            <td><bean:write name="agent" property="agentState" scope="session" /></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL"><fmt:message key="label.biller.location.receive.code"/>: </td>
	            <td><bean:write name="agent" property="receiveCode" scope="session" /></td>
	        </tr>
	        <tr>
	            <td colspan="2"><font color="red"><hr /></font></td>
	        </tr>
	        <tr>
	            <td class="TD-LABEL">Dollar Amount Limit: </td>
	            <td><html:text name="changeLimitForm" property="limitAmt" size="12" maxlength="12" /></td>
			</tr>			
			<tr>
	            <td class="TD-LABEL"><fmt:message key="label.payment.profile.id"/>: </td>
	            <td>
   					<html:select name="changeLimitForm" property="paymentProfileId" size="1">
						<html:option value="(Select)"><fmt:message key="option.select"/></html:option>
						<html:options collection="merchantIds" property="formValue" labelProperty="displayValue" />
					</html:select>
	            </td>
			</tr>
	      
	        <tr>
	            <td colspan="2"> &nbsp; </td>
	        </tr>
	        <tr>
			    <td colspan="2" align="center">
					<html:submit property="submitSave" value="Save" /> &nbsp; &nbsp;
					<html:submit property="submitCancel" value="Cancel" />
			    </td>
			</tr>
	    </tbody>
	</table>

</html:form>
