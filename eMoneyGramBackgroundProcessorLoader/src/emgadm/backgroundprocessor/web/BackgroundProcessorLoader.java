package emgadm.backgroundprocessor.web;

import javax.ejb.CreateException;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.moneygram.common.log.Logger;
import com.moneygram.common.log.log4j.Log4JFactory;

import emgadm.backgroundprocessor.BackgroundProcessorBeanUtil;
import emgadm.backgroundprocessor.BackgroundProcessorConfig;
import emgadm.backgroundprocessor.BackgroundProcessorLocal;
import emgadm.backgroundprocessor.BackgroundProcessorLocalHome;
import emgadm.backgroundprocessor.ProcessApprovedBankTxProcessorConfig;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.EMTEnvironmentInitializer;

/**
 * Application Lifecycle Listener implementation class BackgroundProcessorLoader
 *
 */
public class BackgroundProcessorLoader
		implements ServletContextListener {

	    private static Logger mLogr = Log4JFactory.getInstance().getLogger(BackgroundProcessorLoader.class);
	    private BackgroundProcessorLocal processor;
	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		mLogr.info("S26-contextDestroyed in background processor loader");
		if (processor != null) {
			processor.cancelAllTimers();
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event) {
		try {
			EMTEnvironmentInitializer.initEnvironment(null);
			BackgroundProcessorLocalHome localHome = BackgroundProcessorBeanUtil.getLocalHome();
			processor = localHome.create();

			ServletContext servletContext = event.getServletContext();
			if(EMTAdmContainerProperties.isStartBackgroundTimer()) {
				mLogr.error("****************initializeBackgroundProcessor***************** :: Starts");
				initializeBackgroundProcessor(processor, servletContext);
				mLogr.error("****************initializeBackgroundProcessor***************** :: Ends");
			}
			if(EMTAdmContainerProperties.isStartApproveBankBackgroundTimer()){
				mLogr.error("****************initializeProcessApprovedBankTxTask***************** :: Starts");
				initializeProcessApprovedBankTxTask(processor, servletContext);
				mLogr.error("****************initializeProcessApprovedBankTxTask***************** :: Ends");
			}
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (CreateException e) {
			e.printStackTrace();
		}

	}

	private void initializeBackgroundProcessor(BackgroundProcessorLocal processor,
			ServletContext servletContext) {
		BackgroundProcessorConfig config = new BackgroundProcessorConfig();
		config.setLoopbackServer(EMTAdmContainerProperties.getLoopbackServer());
		config.setLoopbackHost(EMTAdmContainerProperties.getLoopbackHost());
		config.setContextName(servletContext.getServletContextName());

		config.setTimeToStart(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.timeToStart",
				BackgroundProcessorConfig.DEFAULT_TIME_TO_START));
		config.setTimeToPollBackAfterIdle(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.timeToPollBackAfterIdle",
				BackgroundProcessorConfig.DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE));
		config.setTimeToProcessNext(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.timeToProcessNext",
				BackgroundProcessorConfig.DEFAULT_TIME_TO_PROCESS_NEXT));
		config.setTimeToRetryAfterError(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.timeToRetryAfterError",
				BackgroundProcessorConfig.DEFAULT_TIME_TO_RETRY_AFTER_ERROR));
		config.setTimeToRetryWhenBusy(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.timeToRetryWhenBusy",
				BackgroundProcessorConfig.DEFAULT_TIME_TO_RETRY_WHEN_BUSY));
		config.setMaxErrors(getSafeInitParameterAsInt(servletContext,
				"backgroundProcessor.maxErrors",
				BackgroundProcessorConfig.DEFAULT_MAX_ERRORS));

		processor.start(config);
	}

	private void initializeProcessApprovedBankTxTask(BackgroundProcessorLocal processor,
			ServletContext servletContext) {
		ProcessApprovedBankTxProcessorConfig config = new ProcessApprovedBankTxProcessorConfig();
		config.setLoopbackServer(EMTAdmContainerProperties.getLoopbackServer());
		config.setLoopbackHost(EMTAdmContainerProperties.getLoopbackHost());
		config.setContextName(servletContext.getServletContextName());

		config.setTimeToStart(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.processApprovedBankTx.timeToStart",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_TIME_TO_START));
		config.setTimeToProcessAgain(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.processApprovedBankTx.timeToProcessAgain",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_TIME_TO_PROCESS_AGAIN));
		config.setTimeToPollBackAfterIdle(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.processApprovedBankTx.timeToPollBackAfterIdle",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE));
		config.setTimeToRetryAfterError(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.processApprovedBankTx.timeToRetryAfterError",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_TIME_TO_RETRY_AFTER_ERROR));

		// This is being taken as a dynamic property
		config.setMinimumTimeToProcess(ProcessApprovedBankTxProcessorConfig.DEFAULT_MINIMUM_TIME_TO_PROCESS);
mLogr.error("******************************************************");
mLogr.error("******************************************************");
mLogr.error("******************************************************");
mLogr.error(String.valueOf(ProcessApprovedBankTxProcessorConfig.DEFAULT_MINIMUM_TIME_TO_PROCESS));
mLogr.error("******************************************************");
mLogr.error("******************************************************");
mLogr.error("******************************************************");
mLogr.error("******************************************************");
		config.setTimeToRetryWhenBusy(getSafeInitParameterAsLong(servletContext,
				"backgroundProcessor.processApprovedBankTx.timeToRetryWhenBusy",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_TIME_TO_RETRY_WHEN_BUSY));
		config.setMaxErrors(getSafeInitParameterAsInt(servletContext,
				"backgroundProcessor.processApprovedBankTx.maxErrors",
				ProcessApprovedBankTxProcessorConfig.DEFAULT_MAX_ERRORS));

		processor.startProcessApprovedBankTx(config);
	}

	private int getSafeInitParameterAsInt(ServletContext servletContext, String name,
			int defaultValue) {
		try {
			return Integer.parseInt(servletContext.getInitParameter(name));
		} catch (Exception e) {
			return defaultValue;
		}
	}

	private long getSafeInitParameterAsLong(ServletContext servletContext, String name,
			long defaultValue) {
		try {
			return Long.parseLong(servletContext.getInitParameter(name));
		} catch (Exception e) {
			return defaultValue;
		}
	}

}
