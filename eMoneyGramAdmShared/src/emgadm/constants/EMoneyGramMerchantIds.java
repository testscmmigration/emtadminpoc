package emgadm.constants;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import emgadm.property.EMTAdmContainerProperties;
import emgshared.model.SelectOption;
import emgshared.property.EMTSharedContainerProperties;


public class EMoneyGramMerchantIds {

	private static Collection<SelectOption> merchantList = null;
	private static Map<String,String> merchantMap= null;
	private static final String EMG = "EMG";
	private static final String GC = "GC";
	private static final String TELECOM = "Telecom";
	private static final String MORTGAGE = "Mortgage";
	private static final String AUTO = "Auto";
	private static final String OTHER = "Other";
	private static final String WAP = "MGOWAP"; //Walmart


	public static Map<String,String> getMerchantMap() {
		if (EMoneyGramMerchantIds.merchantMap==null) EMoneyGramMerchantIds.initMerchants();
		return EMoneyGramMerchantIds.merchantMap;
	}


	public static Collection<SelectOption> getMerchantList() {
		if (EMoneyGramMerchantIds.merchantList==null) EMoneyGramMerchantIds.initMerchants();
		return EMoneyGramMerchantIds.merchantList;
	}

	private static void initMerchants() {

		EMoneyGramMerchantIds.merchantList = new ArrayList<SelectOption>();
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.EMG, EMTSharedContainerProperties.getMerchantIdEMG()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.GC, EMTSharedContainerProperties.getMerchantIdGC(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID)));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.TELECOM,EMTSharedContainerProperties.getMerchantIdTelecom()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.MORTGAGE,EMTSharedContainerProperties.getMerchantIdMortgage()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.AUTO, EMTSharedContainerProperties.getMerchantIdAuto()));

   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.OTHER,EMTSharedContainerProperties.getMerchantIdOther()));
   		EMoneyGramMerchantIds.merchantList.add(new SelectOption(EMoneyGramMerchantIds.WAP,EMTAdmContainerProperties.getMCCPartnerIdOverride()));

		EMoneyGramMerchantIds.merchantMap = new HashMap<String,String>();
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdEMG(),EMoneyGramMerchantIds.EMG);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdGC(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID),EMoneyGramMerchantIds.GC);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdGC(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID),EMoneyGramMerchantIds.GC);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdGC(EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID),EMoneyGramMerchantIds.GC);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdTelecom(),EMoneyGramMerchantIds.TELECOM);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdMortgage(),EMoneyGramMerchantIds.MORTGAGE);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdAuto(),EMoneyGramMerchantIds.AUTO);
   		EMoneyGramMerchantIds.merchantMap.put(EMTSharedContainerProperties.getMerchantIdOther(),EMoneyGramMerchantIds.OTHER);
   		EMoneyGramMerchantIds.merchantMap.put(EMTAdmContainerProperties.getMCCPartnerIdOverride(),EMoneyGramMerchantIds.WAP);

	}
}
