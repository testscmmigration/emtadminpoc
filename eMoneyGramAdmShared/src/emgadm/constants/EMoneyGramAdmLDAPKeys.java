package emgadm.constants;
/**
 * @version 	1.0
 * @author	David Hadley
 */
public class EMoneyGramAdmLDAPKeys
{
	public static final String CN 				= "cn"; //$NON-NLS-1$
	public static final String OBJECTCLASS 		= "objectClass"; //$NON-NLS-1$
	public static final String TOP 				= "Top"; //$NON-NLS-1$
	public static final String GROUP_OF_NAMES   = "groupOfNames";
	public static final String FALSE_LDAP_VALUE = "false"; //$NON-NLS-1$
	public static final String TRUE_LDAP_VALUE 	= "true"; //$NON-NLS-1$

	public static final String USER_GUID					= "GUID"; //$NON-NLS-1$
	public static final String USER_ID 						= "cn"; //$NON-NLS-1$
	public static final String USER_FIRST_NAME 				= "givenName"; //$NON-NLS-1$
	public static final String USER_LAST_NAME 				= "sn"; //$NON-NLS-1$
	public static final String USER_EMAIL 					= "mail"; //$NON-NLS-1$
	public static final String USER_TEST_USER				= "ADMIN001"; //$NON-NLS-1$
	public static final String USER_ROLES 					= "groupMembership"; //$NON-NLS-1$
	
	public static final String EMT_ADMIN_USERS_GROUP_DN     = "cn=APP - EMT - Admin,ou=Admin,ou=EMT,ou=applications,o=moneygram";
	public static final String USER_ROLE_DN_SUFFIX          = "ou=Roles,ou=Admin,ou=EMT,ou=applications,o=moneygram";
	public static final String COMMAND_DN_SUFFIX            = "ou=Commands,ou=Admin,ou=EMT,ou=applications,o=moneygram";
//	public static final String USER_OBJECT            		= "emgadmin-user"; //$NON-NLS-1$
//	public static final String USER_OBJECT_PERSON     		= "person"; //$NON-NLS-1$
//	public static final String USER_OBJECT_ORG_PERSON       = "organizationalPerson"; //$NON-NLS-1$
//	public static final String USER_OBJECT_INET_PERSON 		= "inetorgperson"; //$NON-NLS-1$
//	public static final String USER_OBJECT_INET_PERSON_TEMG = "inetorgperson-temg"; //$NON-NLS-1$
//	public static final String USER_PASSWORD 				= "userPassword"; //$NON-NLS-1$
//	public static final String USER_PASSWORD_CHANGE_DATE 	= "passwordchangedate"; //$NON-NLS-1$
//	public static final String USER_PASSWORD_CHANGE_REQUIRED= "password-change-required"; //$NON-NLS-1$
//	public static final String USER_STATUS 					= "webstatus"; //$NON-NLS-1$
//	public static final String USER_IS_INTERNAL				= "internal-user"; //$NON-NLS-1$
//	public static final String USER_IS_ACTIVE 				= "Active"; //$NON-NLS-1$
//	public static final String USER_IS_INACTIVE 			= "Inactive"; //$NON-NLS-1$
//	public static final String USER_LAST_ACCESS_TIMESTAMP 	= "weblastaccesstimestamp"; //$NON-NLS-1$
//	public static final String USER_PASSWORD_RETRY_COUNT 	= "webpasswordretrycount"; //$NON-NLS-1$
//	public static final String USER_WEB_STATUS			 	= "webstatus"; //$NON-NLS-1$
//	public static final String USER_ADDRESS1 				= "address1"; //$NON-NLS-1$
//	public static final String USER_ADDRESS2 				= "address2"; //$NON-NLS-1$
//	public static final String USER_CITY 					= "city"; //$NON-NLS-1$
//	public static final String USER_STATE 					= "state"; //$NON-NLS-1$
//	public static final String USER_ZIP_CODE 				= "zipcode"; //$NON-NLS-1$
	
	public static final String ROLE_OBJECT      = "moneygramApplicationRole"; //$NON-NLS-1$
	public static final String ROLE_ROLE        = "cn"; //$NON-NLS-1$
	public static final String ROLE_DESCRIPTION = "description"; //$NON-NLS-1$
	public static final String ROLE_COMMANDS    = "moneygramAppOptionMapping"; //$NON-NLS-1$

	public static final String APPLICATION_OPTIONS_OBJECT         = "moneygramApplicationOption"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_MENULEVEL 	  = "MenuLevel"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_COMMAND 		  = "applicationCommand"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_COMMANDS		  = "commands"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_AUTH_REQUIRED  = "authenticationRequired"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_DESCRIPTION 	  = "description"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_MENUGROUP 	  = "menuGroup"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_ALLOW_INTERNAL = "allowInternal"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_MENU_NAME      = "menuName"; //$NON-NLS-1$
	public static final String APPLICATION_OPTIONS_SHOW_IN_MENU   = "showInMenu"; //$NON-NLS-1$


	public static final String BILLER_OBJECT         			 = "eptd-biller-main-office"; //$NON-NLS-1$
	public static final String BILLER_BUSINESS_WEEK 	  		 = "BusinessWeek"; //$NON-NLS-1$
	public static final String BILLER_EODTIME 		  			 = "EODTime"; //$NON-NLS-1$
	public static final String BILLER__MAX_LOGIN_INAVTIVITY_DAYS = "MaxLoginInactivityDays"; //$NON-NLS-1$
	public static final String BILLER_MIN_ALPHA_PASSWORD_CHARS	 = "MinAlphaPasswordChars"; //$NON-NLS-1$
	public static final String BILLER_MIN_NUMERIC_PASSWORD_CHARS = "MinNumericPasswordChars"; //$NON-NLS-1$
	public static final String BILLER_MIN_PASSWORD_LENGTH		 = "MinPasswordLength"; //$NON-NLS-1$
	public static final String BILLER_TIME_ZONE					 = "TimeZone"; //$NON-NLS-1$
	public static final String BILLER_PASSWORD_RESET_DURATION 	 = "passwordResetDuration"; //$NON-NLS-1$
	
}
