/*
 * Created on Nov 20, 2003
 */
package emgadm.constants;

/**
 * @author A119
 */
public class EMoneyGramAdmSessionConstants
{
	public static final String USER_PROFILE = "userProfile"; 
	public static final String USER_TEMP_PASSWORD = "tempPassword";
}
