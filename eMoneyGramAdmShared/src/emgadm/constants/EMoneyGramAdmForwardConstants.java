/*
 * Created on Dec 9, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.constants;

/**
 * @author A113
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EMoneyGramAdmForwardConstants
{

	public static final String LOCAL_FORWARD_SHOW_LOGIN_FORM = "showLoginPage";
	public static final String LOCAL_FORWARD_SUCCESS = "success";
	public static final String LOCAL_FORWARD_ERROR = "error";
	public static final String LOCAL_FORWARD_FAILURE = "failure";
	public static final String LOCAL_FORWARD_WELCOME = "welcome";
	public static final String LOCAL_FORWARD_BROWSER_NOT_SUPPORTED =
		"unsupportedBrowser";
	public static final String GLOBAL_FORWARD_SYSTEM_ERROR = "systemError";
	public static final String LOCAL_FORWARD_CHANGE_PASSWORD_REQUIRED =
		"changePasswordRequired";
	public static final String LOCAL_FORWARD_SHOW_CHANGE_PASSWORD =
		"showChgPasswordForm";

	public static final String LOCAL_FORWARD_SHOW_INTERNAL_USER_ADMIN =
		"showInternalUserAdmin";
	public static final String LOCAL_FORWARD_SHOW_INTERNAL_USER_SEARCH =
		"showInternalUserSearch";

	public static final String LOCAL_FORWARD_SHOW_BILLER_AGENT_LIMITS =
		"billerAgentLimits";
	public static final String LOCAL_FORWARD_SHOW_SELECT_AGENT = "agentSelect";
	public static final String LOCAL_FORWARD_SHOW_CHANGE_LIMIT = "changeLimit";
	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE =
		"showTransQueue";
	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGO =
		"showTransQueueMGO";
	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGOUK =
		"showTransQueueMGOUK";
	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE_MGODE = 
		"showTransQueueMGODE";

	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE_WAP =
		"showTransQueueWAP";
	public static final String LOCAL_FORWARD_SHOW_TRANS_QUEUE_ALL =
		"showTransQueueALL";
	public static final String LOCAL_FORWARD_TRAN_CONFIRM = "tranConfirm";
	public static final String LOCAL_FORWARD_TRAN_APPROVAL_P2P =
		"tranApprovalP2P";
	public static final String LOCAL_FORWARD_TRAN_APPROVAL_EP =
		"tranApprovalEP";
	public static final String LOCAL_FORWARD_TRAN_APPROVAL_ES =
		"tranApprovalES";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_P2P =
		"tranProcessP2P";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_EP = "tranProcessEP";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_ES = "tranProcessES";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_DOC = "tranProcessDOC";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_DOC_PEN = "tranProcessDOCPEN";
	public static final String LOCAL_FORWARD_TRAN_SEND_ES = "tranSendES";
	public static final String LOCAL_FORWARD_TRAN_APPROVAL_DS = "tranApprovalDS";
	public static final String LOCAL_FORWARD_TRAN_PROCESS_DS = "tranProcessDS";
	public static final String LOCAL_FORWARD_TRAN_SEND_DS = "tranSendDS";
	public static final String LOCAL_FORWARD_TRAN_IN_PROGRESS =
		"tranInProgress";
	public static final String LOCAL_FORWARD_TRAN_EXCEPTION = "tranException";
	public static final String LOCAL_FORWARD_TRANS_OWNERSHIP = "transOwnership";
	public static final String LOCAL_FORWARD_TRANSCTION_DETAIL =
		"transactionDetail";
	public static final String LOCAL_FORWARD_REFRESH = "refresh";
	public static final String LOCAL_FORWARD_SHOW_BILLER_LIMITS =
		"showBillerLimits";
	public static final String LOCAL_FORWARD_ADD_COUNTRY_EXCEPTION =
		"addCountryException";
	public static final String LOCAL_FORWARD_EDIT_COUNTRY_EXCEPTION =
		"editCountryException";
	public static final String LOCAL_FORWARD_SHOW_COUNTRY_EXCEPTION =
		"showCountryException";
	public static final String LOCAL_FORWARD_RECOVERY_OF_LOSS =
		"recoveryOfLoss";
	public static final String LOCAL_FORWARD_RECORD_CC_CHARGEBACK =
	    "recordCcChargeback";
	public static final String LOCAL_FORWARD_DENY = "deny";
	public static final String LOCAL_FORWARD_PEND = "moveToPending";
	public static final String LOCAL_FORWARD_RECORD_LOASS_ADJUSTMENT =
		"recordLossAdjustment";
	public static final String LOCAL_FORWARD_RECORD_FEE_ADJUSTMENT =
		"recordFeeAdjustment";
	public static final String LOCAL_FORWARD_RECORD_CONSUMER_REFUND_OR_REMITTANCE_ADJUSTMENT =
		"recordConsumerRefundOrRemittanceAdjustment";
	public static final String LOCAL_FORWARD_RECORD_MG_ACTIVITY_ADJUSTMENT =
		"recordMgActivityAdjustment";
	public static final String LOCAL_FORWARD_MAN_REFUND_FEE = "manRefundFee";
	public static final String LOCAL_FORWARD_MAN_COLLECT_FEE = "manCollectFee";
	public static final String LOCAL_FORWARD_MAN_RETURN_FUNDING =
		"manReturnFunding";
	public static final String LOCAL_FORWARD_RESUBMIT_ACH = "resubmitAch";
	public static final String LOCAL_FORWARD_CHARGE_BACKUP_CREDIT_CARD =
		"chargeBackupCreditCard";

	public static final String LOCAL_FORWARD_SHOW_IP_BLOCKED_LIST =
		"showIPBlockedList";
	public static final String LOCAL_FORWARD_ADD_IP_BLOCKED_LIST =
		"addIPBlockedList";
	public static final String LOCAL_FORWARD_SAVE_IP_BLOCKED_LIST =
		"saveIPBlockedList";
	public static final String LOCAL_FORWARD_SHOW_CC_BLOCKED_LIST =
		"showCCBlockedList";
	public static final String LOCAL_FORWARD_ADD_CC_BLOCKED_LIST =
		"addCCBlockedList";
	public static final String LOCAL_FORWARD_SAVE_CC_BLOCKED_LIST =
		"saveCCBlockedList";
	public static final String LOCAL_FORWARD_SHOW_BIN_BLOCKED_LIST =
		"showBinBlockedList";
	public static final String LOCAL_FORWARD_ADD_BIN_BLOCKED_LIST =
		"addBinBlockedList";
	public static final String LOCAL_FORWARD_SAVE_BIN_BLOCKED_LIST =
		"saveBinBlockedList";
	public static final String LOCAL_FORWARD_SHOW_BANK_BLOCKED_LIST =
		"showBankBlockedList";
	public static final String LOCAL_FORWARD_ADD_BANK_BLOCKED_LIST =
		"addBankBlockedList";
	public static final String LOCAL_FORWARD_SAVE_BANK_BLOCKED_LIST =
		"saveBankBlockedList";
	public static final String LOCAL_FORWARD_SHOW_EMAIL_BLOCKED_LIST =
		"showEmailBlockedList";
	public static final String LOCAL_FORWARD_ADD_EMAIL_BLOCKED_LIST =
		"addEmailBlockedList";
	public static final String LOCAL_FORWARD_SAVE_EMAIL_BLOCKED_LIST =
		"saveEmailBlockedList";

	public static final String LOCAL_FORWARD_EDIT_ROLE = "editRole";
	public static final String LOCAL_FORWARD_SHOW_ROLES = "showRoles";
	public static final String LOCAL_FORWARD_CONSUMER_SEARCH = "consumerSearch";
	public static final String LOCAL_FORWARD_ADVANCED_CONSUMER_SEARCH =
		"advancedConsumerSearch";

	public static final String LOCAL_FORWARD_SELECT_PRODUCT = "selectProduct";
	public static final String LOCAL_FORWARD_SAVE_AUTOMATION = "saveAutomation";
	public static final String LOCAL_FORWARD_SAVE_SCORING_CONFIG =
		"saveScoringConfig";
	public static final String LOCAL_FORWARD_EDIT_SCORING_CONFIG =
		"editScoringConfig";
	public static final String LOCAL_FORWARD_TEST_SCORING_CONFIG =
		"testScoringConfig";
	public static final String LOCAL_FORWARD_SHOW_SCORING_CONFIG =
		"showScoringConfig";

	public static final String LOCAL_FORWARD_EDIT_DYN_PROPERTY =
		"editDynProperty";
	public static final String LOCAL_FORWARD_LIST_MD_BATCHES = "listMDBatches";
	public static final String LOCAL_FORWARD_RECORD_MD_ACH_RETURN =
		"recordMDACHReturn";
	public static final String LOCAL_FORWARD_VIEW_BANK_ACCOUNT_DETAIL =
		"viewBankAccountDetail";
	public static final String LOCAL_FORWARD_SHOW_CUSTOMER_PROFILE =
		"showCustomerProfile";
	public static final String LOCAL_FORWARD_BATCH_ES_PROCESS =
		"batchESProcess";

	public static final String LOCAL_FORWARD_EDIT_ADMIN_USER_PROFILE =
		"editAdminUserProfile";
	public static final String LOCAL_FORWARD_RESET_ADMIN_USER_PASSWORD =
		"resetAdminUserPassword";

	public static final String LOCAL_FORWARD_SHOW_EMPLOYEE_LIST_FILE_UPLOAD =
		"showEmployeeListFileUpload";
	public static final String LOCAL_FORWARD_PREVIEW_EMPLOYEE_LIST_FILE_UPLOAD =
		"previewEmployeeListFileUpload";
	public static final String LOCAL_FORWARD_SHOW_AREACODE_STATE_FILE_UPLOAD =
		"showAreaCodeStateFileUpload";
	public static final String LOCAL_FORWARD_PREVIEW_AREACODE_STATE_FILE_UPLOAD =
		"previewAreaCodeStateFileUpload";
	public static final String LOCAL_FORWARD_SHOW_ACH_RETURN_FILE_UPLOAD =
		"showAchReturnFileUpload";
	public static final String LOCAL_FORWARD_PREVIEW_ACH_RETURN_FILE_UPLOAD =
		"previewAchReturnFileUpload";
	public static final String LOCAL_FORWARD_SHOW_ZIP_STATE_FILE_UPLOAD =
		"showZipStateFileUpload";
	public static final String LOCAL_FORWARD_PREVIEW_ZIP_STATE_FILE_UPLOAD =
		"previewZipStateFileUpload";

	public static final String LOCAL_FORWARD_SHOW_ACHRETURN_QUEUE =
		"showACHReturnQueue";
	public static final String LOCAL_FORWARD_SHOW_ACHRETURN_DETAIL =
		"showACHReturnDetail";

	public static final String LOCAL_FORWARD_SHOW_NEGATIVE_TERM_LIST =
		"showNegativeTermList";
	public static final String LOCAL_FORWARD_ADD_NEGATIVE_TERM_LIST =
		"addNegativeTermList";
	public static final String LOCAL_FORWARD_SAVE_NEGATIVE_TERM_LIST =
		"saveNegativeTermList";
	
	public static final String LOCAL_FORWARD_APPROVE_DOCUMENT =
		"approveDocument";
	public static final String LOCAL_FORWARD_PEND_DOCUMENT =
		"pendDocument";
	public static final String LOCAL_FORWARD_DENY_DOCUMENT =
		"denyDocument";
	
	public static final String LOCAL_FORWARD_UPDATE_DOCUMENT_STATUS =
		"updateCustDocumentStatus";
	
	public static final String LOCAL_FORWARD_RELEASE_TRANSACTIONS_DOCUMENT_STATUS =
		"releaseTransactionsDocumentStatus";
	
	

	//	for generic cancel opertions or just logged in ... send to Home screen for lack of a better place to go
	public static final String GLOBAL_FORWARD_HOME_PAGE = "home";

}
