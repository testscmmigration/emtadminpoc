package emgadm.dataaccessors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import emgadm.constants.EMoneyGramAdmLDAPKeys;
import emgadm.model.ApplicationCommand;
import emgadm.model.Role;
import emgadm.model.RoleFactory;
import emgshared.exceptions.DataSourceException;

public class RoleManager
{
	private static List<Role> roles = new ArrayList<Role>();
	// The pattern below is used to extract the cn part of a dn which can then be located in group 2 of the match
	private static final Pattern CN_PATTERN = Pattern.compile("^(cn=)(.*?)(,.*)");
	
	public static Role getRole(String guid)	throws DataSourceException, InvalidGuidException
	{
		for (Iterator<Role> iter = getRoles().iterator(); iter.hasNext();)
		{
			Role role = iter.next();
			if (role.getId().equals(guid))
			{
				return role;
			}
		}
		throw new InvalidGuidException(
			"Role with id: " + guid + " does not exist.");
	}
	
	public static Role getEmtRole(Object roleAttribute) throws DataSourceException, InvalidGuidException
	{
		if (roleAttribute instanceof ArrayList) {
			return RoleManager.getEmtRole((List<String>) roleAttribute);
		} else { 
			//  It must be a single role with type of String
			return RoleManager.getEmtRole((String) roleAttribute);
		}
	}
	
	public static List<Role> getEmtRoles(Object roleAttribute)
			throws DataSourceException, InvalidGuidException {
		List<String> roleNames = new ArrayList<String>();
		if (roleAttribute instanceof ArrayList) {
			roleNames = (List<String>) roleAttribute;
		} else { 
			//  It must be a single role with type of String
			roleNames.add((String) roleAttribute);
		}		
		return RoleManager.getEmtRoles(roleNames);
	}

	public static List<Role> getEmtRoles(List<String> roleAttributes)
			throws DataSourceException, InvalidGuidException {
		return getRoles(roleAttributes);
	}

	public static Role getEmtRole(List<String> roleAttributes) throws DataSourceException, InvalidGuidException
	{
		List<Role> roles = getRoles(roleAttributes);
		return (roles.size() > 0) ? roles.get(0) : null;
	}
	
	public static Role getEmtRole(String roleAttribute) throws DataSourceException, InvalidGuidException
	{
		Matcher m = CN_PATTERN.matcher((String) roleAttribute);
		return findMatchingRole(getRoles(), m);
	}

	private static Role findMatchingRole(List<Role> roleList, Matcher m) {
		Role assignedRole = null;
		String roleName;
		
		if (m.matches()) {
			roleName = m.group(2);
			for (Iterator<Role> iter = roleList.iterator(); iter.hasNext();)
			{
				Role role = iter.next();
				if (role.getId().equals(roleName))
				{
					assignedRole =  role;
					break;
				}
			}
		}
		return assignedRole;
	}
	
	public static List<Role> getRoles(List<String> roleGuid)
		throws DataSourceException, InvalidGuidException {
		List<Role> assignedRoles = new ArrayList<Role>();
		List<Role> roleList = getRoles();
		for (int i = 0; i < roleGuid.size(); i++) {
			Matcher m = CN_PATTERN.matcher(roleGuid.get(i));
			Role role = findMatchingRole(roleList, m);
			if (role != null) {
				assignedRoles.add(role);
			}
		}
		return assignedRoles;
	}

	public static List<Role> getRoles()
		throws DataSourceException, InvalidGuidException {
		return retrieveRoles();
	}

	public static List<Role> getInternalRoles()
		throws DataSourceException, InvalidGuidException
	{
		return retrieveRoles();
	}

	private static synchronized List<Role> retrieveRoles()
			throws InvalidGuidException, DataSourceException {
		DirContext dirContext = null;
		roles = new ArrayList<Role>();
		//System.out.println("Inside Role Manager " + roles.size());
		try {
			dirContext = JNDIManager.getInternalRoleContext();
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
			NamingEnumeration<SearchResult> namingEnum = dirContext.search("", "(&(cn=*))", searchControls); //$NON-NLS-1$ //$NON-NLS-2$

			while (namingEnum.hasMore()) {
				SearchResult sr = namingEnum.next();
				Attributes attributes = sr.getAttributes();

				if (attributes != null) {
					Role role =
						RoleFactory.createRole(
							getId(attributes),
							getName(attributes),
							getDescription(attributes),
							getApplicationCommands(attributes));
					roles.add(role);
				}
			}
		} catch (NamingException e)	{
			throw new DataSourceException("JNDI error retreiving roles", e);
		} catch (Throwable e) {
			throw new DataSourceException("Error retreiving roles", e);
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}
		System.out.println("Inside Role Manager End " + roles.size());
		return roles;
	}

	private static Collection<ApplicationCommand> getApplicationCommands(Attributes attributes)
		throws DataSourceException, InvalidGuidException, NamingException
	{
		List<ApplicationCommand> commands = new ArrayList<ApplicationCommand>();
		Attribute attribute = attributes.get(EMoneyGramAdmLDAPKeys.ROLE_COMMANDS);
		if (attribute == null) {
			return commands;
		}
		
		NamingEnumeration attrEnum = attribute.getAll();

		while (attrEnum.hasMoreElements())
		{
			Matcher m = CN_PATTERN.matcher((String) attrEnum.nextElement());
			if (!m.matches()) {
				throw new InvalidGuidException(
						"Application Command with DN: " + (String) attrEnum.nextElement() + " does not exist.");
			}
			String command = m.group(2);
			commands.add(CommandManager.getCommandById(command));
		}

		if (commands.isEmpty() == false)
		{
			ApplicationCommand command =
				commands.iterator().next();
			Collections.sort(
				commands,
				command.getCommandSorterByMenuAndDisplayName());
		}

		return commands;
	}

	private static String getDescription(Attributes attributes)
			throws NamingException {
		Attribute attribute = attributes.get(EMoneyGramAdmLDAPKeys.ROLE_DESCRIPTION);
		return (attribute != null) ? (String) attribute.get() : "";
	}

	private static String getName(Attributes attributes) throws NamingException
	{
		return (String) attributes.get(EMoneyGramAdmLDAPKeys.ROLE_ROLE).get();
	}

	private static String getId(Attributes attributes) throws NamingException {
		return (String) attributes.get(EMoneyGramAdmLDAPKeys.CN).get();
	}

	public static List<Role> getFreshedRoles() throws DataSourceException, InvalidGuidException {
		return retrieveRoles();
	}

	public static void updateRolePermissions(String roleId, List<ApplicationCommand> commands) throws Exception
	{
		DirContext context = JNDIManager.getInternalRoleContext(); ///cn=emt,ou=services,o=moneygram

		try {
			ModificationItem[] modificationItems = new ModificationItem[commands.size()];
			int x = 0;
			BasicAttribute attribute = null;

			for (Iterator<ApplicationCommand> iter = commands.iterator(); iter.hasNext();) {
				ApplicationCommand command = iter.next();
				
				if (attribute == null) {
					attribute = new BasicAttribute(EMoneyGramAdmLDAPKeys.ROLE_COMMANDS, getRoleDn(command.getId()));
				} else {
					attribute.add(getRoleDn(command.getId()));
				}
				modificationItems[x] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attribute);
				x++;
			}
			context.modifyAttributes(EMoneyGramAdmLDAPKeys.CN + "=" + roleId, modificationItems);

		} finally {
			context.close();
		}
	}
	
	protected static String getRoleDn(String roleId) {
		return EMoneyGramAdmLDAPKeys.CN +"=" + roleId + "," + EMoneyGramAdmLDAPKeys.COMMAND_DN_SUFFIX;
	}
}
