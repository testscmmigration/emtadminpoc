package emgadm.dataaccessors;

import java.util.Collection;
import java.util.List;

import emgshared.exceptions.DataSourceException;

public interface BillerMainOfficeManager
{
	//	public static final String DEFAULT_GUID = "0"; //$NON-NLS-1$
	//
	//	public BillerMainOffice getBillerMainOffice(String guid)
	//		throws ParseException, DataSourceException;
	//	public BillerMainOffice getDefaultBillerMainOffice()
	//		throws ParseException, DataSourceException;
	//	public void updateSecurityStandards(SecurityStandards securityStandards)
	//		throws ParseException, DataSourceException;
	//	public void updateBusinessAdminSettings(BusinessAdminSettings businessAdminSettings)
	//		throws ParseException, DataSourceException;

	//	public abstract Agent getMainOffice(String agentId)
	//		throws DataSourceException;
	//	public abstract List getDivisions(String agentId)
	//		throws DataSourceException;
	abstract Collection getMainOffices() throws DataSourceException;
	abstract List getLocations(String agentId) throws DataSourceException;
}
