/*
 * Created on Feb 13, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.dataaccessors;

/**
 * @author A132
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class InvalidGuidException extends Exception
{
	private String description;
	
	public InvalidGuidException(String s)
	{
		description = s;
	}
	
	public String toString()
	{
		return description;
	}

}
