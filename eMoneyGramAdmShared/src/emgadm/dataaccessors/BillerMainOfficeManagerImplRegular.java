package emgadm.dataaccessors;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import oracle.jdbc.OracleTypes;
import emgadm.model.Agent;
import emgadm.model.AgentBuilder;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.OracleAccess;
import emgshared.exceptions.DataSourceException;

public class BillerMainOfficeManagerImplRegular
	implements BillerMainOfficeManager
{

	private static BillerMainOfficeManager _instance =
		new BillerMainOfficeManagerImplRegular();

	private BillerMainOfficeManagerImplRegular()
	{
	}

	static BillerMainOfficeManager instance()
	{
		return _instance;
	}

	public Collection getMainOffices() throws DataSourceException
	{
		try
		{
			return getAgents(null, Agent.MAIN_OFFICE_LEVEL);
		} catch (SQLException e)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				e.getMessage(),
				e);
			throw new DataSourceException(e.getMessage());
		}
	}

	public List getLocations(String mainOfficeAgentId)
		throws DataSourceException
	{
		try
		{
			return getAgents(mainOfficeAgentId, Agent.LOCATION_LEVEL);
		} catch (SQLException e)
		{
			EMGSharedLogger.getLogger(
				this.getClass().getName().toString()).error(
				e.getMessage(),
				e);
			throw new DataSourceException(e.getMessage());
		}
	}

	private List getAgents(String agentId, int hierarchyLevel)
		throws SQLException, DataSourceException
	{
		List agents = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;

		boolean searchForMainOffices = hierarchyLevel == 1;
		String storedProcName =
			searchForMainOffices
				? "pkg_em_agent_list.prc_get_mainoffice_id_cv"
				: "pkg_em_agent_list.prc_get_location_agent_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try
		{
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			int cursorParamIndex = 0;

			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			if (StringHelper.isNullOrEmpty(agentId))
			{
				cs.setNull(++paramIndex, Types.INTEGER);
			} else
			{
				cs.setInt(++paramIndex, Integer.parseInt(agentId));
			}

			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			cursorParamIndex = paramIndex;

			cs.execute();

			rs = (ResultSet) cs.getObject(cursorParamIndex);

			while (rs.next())
			{
				if (searchForMainOffices
					|| (!searchForMainOffices
						&& !StringHelper.isNullOrEmpty(
							rs.getString("agt_rcv_agency"))))
				{
					AgentBuilder agentBuilder = new AgentBuilder();
					agentBuilder.setAgentId(rs.getString("agent_id"));
					agentBuilder.setHierarchyLevel(hierarchyLevel);
					agentBuilder.setAgentParentID(
						rs.getString("parent_agent_id"));
					agentBuilder.setAgentCity(rs.getString("city"));
					agentBuilder.setAgentName(rs.getString("agent_name"));
					agentBuilder.setStatus(rs.getString("agent_status"));
					agentBuilder.setReceiveCode(rs.getString("agt_rcv_agency"));
					if (searchForMainOffices == false)
					{
						agentBuilder.setAgentState(
							rs.getString("state_province"));
					} else
					{
						agentBuilder.setAgentState("");
					}
					agents.add(agentBuilder.buildAgent());
					agentBuilder = null;
				}
			}
		} finally
		{
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return agents;
	}

	//	private static DirContext getDirectoryContext(
	//		DirContext dirContext,
	//		String string) {
	//		try {
	//			return (DirContext) dirContext.lookup(EMoneyGramAdmLDAPKeys.CN + "=" + string); //$NON-NLS-1$
	//		} catch (NamingException ex) {
	//			return null;
	//		}
	//	}

	//	public BillerMainOffice getBillerMainOffice(String guid)
	//		throws DataSourceException, ParseException {
	//		BillerMainOffice corp = retrieveCorporation(guid);
	//		return corp == null ? getDefaultBillerMainOffice().mimic(guid) : corp;
	//	}

	//	private SecurityStandards getSecurityStandards(String mainOfficeId)
	//		throws DataSourceException, ParseException, NamingException
	//	{
	//		SecurityStandards standards;
	//		try
	//		{
	//			standards = retrieveSecurityStandards(mainOfficeId);
	//		} catch (NameNotFoundException e)
	//		{
	//			standards = getSecurityStandardsDefault().mimic(mainOfficeId);
	//		}
	//		return standards;
	//	}
	//
	//	private SecurityStandards getSecurityStandardsDefault()
	//		throws ParseException, DataSourceException {
	//		return (getDefaultBillerMainOffice().getSecurityStandards());
	//	}

	//	private SecurityStandards retrieveSecurityStandards(String mainOfficeId)
	//		throws ParseException, NamingException, DataSourceException
	//	{
	//		DirContext context = JNDIManager.getCorporationContext();
	//		StringBuffer name = new StringBuffer(32);
	//		name.append(EMoneyGramAdmLDAPKeys.CN);
	//		name.append("="); //$NON-NLS-1$
	//		name.append(mainOfficeId);
	//		Attributes attributes = context.getAttributes(name.toString());
	//		return (retrieveSecurityStandards(attributes));
	//	}
	//
	//	SecurityStandards retrieveSecurityStandards(Attributes attributes)
	//		throws ParseException, DataSourceException {
	//		String mainOfficeId;
	//		int minPasswordLength;
	//		int minPasswordAlphaChars;
	//		int minPasswordNumericChars;
	//		int maxPasswordDuration;
	//		int maxInactivityDuration;
	//		try {
	//			mainOfficeId =
	//				(String) attributes.get(EMoneyGramAdmLDAPKeys.CN).get();
	//			NumberFormat parser = NumberFormat.getNumberInstance();
	//			minPasswordLength =
	//				parser
	//					.parse(
	//						(String) attributes.get(
	//							EMoneyGramAdmLDAPKeys
	//								.BILLER_MIN_PASSWORD_LENGTH)
	//								.get(
	//							0))
	//					.intValue();
	//			minPasswordAlphaChars =
	//				parser
	//					.parse(
	//						(String) attributes
	//							.get(
	//								EMoneyGramAdmLDAPKeys
	//									.BILLER_MIN_ALPHA_PASSWORD_CHARS)
	//							.get(0))
	//					.intValue();
	//			minPasswordNumericChars =
	//				parser
	//					.parse(
	//						(String) attributes
	//							.get(
	//								EMoneyGramAdmLDAPKeys
	//									.BILLER_MIN_NUMERIC_PASSWORD_CHARS)
	//							.get(0))
	//					.intValue();
	//			maxPasswordDuration =
	//				parser
	//					.parse(
	//						(String) attributes
	//							.get(
	//								EMoneyGramAdmLDAPKeys
	//									.BILLER_PASSWORD_RESET_DURATION)
	//							.get(0))
	//					.intValue();
	//			maxInactivityDuration =
	//				parser
	//					.parse(
	//						(String) attributes
	//							.get(
	//								EMoneyGramAdmLDAPKeys
	//									.BILLER__MAX_LOGIN_INAVTIVITY_DAYS)
	//							.get(0))
	//					.intValue();
	//		} catch (NamingException e) {
	//			throw new DataSourceException(e.getMessage());
	//		}
	//
	//		return (
	//			new SecurityStandards(
	//				mainOfficeId,
	//				minPasswordLength,
	//				minPasswordAlphaChars,
	//				minPasswordNumericChars,
	//				maxPasswordDuration,
	//				maxInactivityDuration));
	//	}

	//	private BusinessAdminSettings getBusinessAdminSettings(String mainOfficeId)
	//		throws DataSourceException, NamingException, ParseException
	//	{
	//		BusinessAdminSettings businessAdminSettings;
	//		try
	//		{
	//			businessAdminSettings = retrieveBusinessAdminSettings(mainOfficeId);
	//		} catch (NameNotFoundException e)
	//		{
	//			businessAdminSettings =
	//				getBusinessAdminSettingsDefault().mimic(mainOfficeId);
	//		}
	//		return businessAdminSettings;
	//	}
	//
	//	private BusinessAdminSettings getBusinessAdminSettingsDefault()
	//		throws NamingException, DataSourceException, ParseException {
	//		return (getDefaultBillerMainOffice().getBusinessAdminSettings());
	//	}

	//	private BusinessAdminSettings retrieveBusinessAdminSettings(String mainOfficeId)
	//		throws NamingException, ParseException, DataSourceException
	//	{
	//		DirContext context = JNDIManager.getCorporationContext();
	//		StringBuffer name = new StringBuffer(32);
	//		name.append(EMoneyGramAdmLDAPKeys.CN);
	//		name.append("="); //$NON-NLS-1$
	//		name.append(mainOfficeId);
	//		Attributes attributes = context.getAttributes(name.toString());
	//		return (retrieveBusinessAdminSettings(attributes));
	//	}
	//
	//	BusinessAdminSettings retrieveBusinessAdminSettings(Attributes attributes)
	//		throws ParseException, DataSourceException {
	//		String mainOfficeId;
	//		BusinessWeekType businessWeek;
	//		Date endOfDayTime;
	//		TimeZone timeZone;
	//		try {
	//			mainOfficeId =
	//				(String) attributes.get(EMoneyGramAdmLDAPKeys.CN).get();
	//
	//			String businessWeekTag =
	//				((String) attributes
	//					.get(EMoneyGramAdmLDAPKeys.BILLER_BUSINESS_WEEK)
	//					.get(0));
	//			businessWeek = BusinessWeekType.getInstance(businessWeekTag);
	//
	//			String timeZoneId =
	//				((String) attributes
	//					.get(EMoneyGramAdmLDAPKeys.BILLER_TIME_ZONE)
	//					.get(0));
	//			timeZone = TimeZone.getTimeZone(timeZoneId);
	//
	//			String endOfDayTimeText =
	//				((String) attributes
	//					.get(EMoneyGramAdmLDAPKeys.BILLER_EODTIME)
	//					.get(0));
	//			endOfDayTime =
	//				DateHelper.getTime24HourFormatter(timeZone).parse(
	//					endOfDayTimeText);
	//		} catch (NamingException e) {
	//			throw new DataSourceException(e.getMessage());
	//		}
	//
	//		return (
	//			new BusinessAdminSettings(
	//				mainOfficeId,
	//				businessWeek,
	//				endOfDayTime,
	//				timeZone));
	//	}

	//	private void addSecurityStandards(SecurityStandards securityStandards)
	//		throws DataSourceException, NamingException, ParseException
	//	{
	//		BusinessAdminSettings businessAdminSettings =
	//			getBusinessAdminSettings(securityStandards.getBillerMainOfficeId());
	//		addAccountAdminSettings(securityStandards, businessAdminSettings);
	//	}
	//
	//	private void addBusinessAdminSettings(BusinessAdminSettings businessAdminSettings)
	//		throws DataSourceException, ParseException, NamingException
	//	{
	//		SecurityStandards securityStandards =
	//			getSecurityStandards(businessAdminSettings.getBillerMainOfficeId());
	//		addAccountAdminSettings(securityStandards, businessAdminSettings);
	//	}
	//
	//	private void addAccountAdminSettings(
	//		SecurityStandards securityStandards,
	//		BusinessAdminSettings businessAdminSettings)
	//		throws NamingException, DataSourceException
	//	{
	//		DirContext context = JNDIManager.getCorporationContext();
	//		try
	//		{
	//			Attributes attrs = new BasicAttributes(true);
	//			populateAttributes(attrs, securityStandards);
	//			populateAttributes(attrs, businessAdminSettings);
	//			context.createSubcontext(EMoneyGramAdmLDAPKeys.CN + "=" + securityStandards.getBillerMainOfficeId(), attrs); //$NON-NLS-1$
	//		} finally
	//		{
	//			JNDIManager.releaseDirContext(context);
	//		}
	//	}
	//
	//	public void updateSecurityStandards(SecurityStandards securityStandards)
	//		throws ParseException, DataSourceException
	//	{
	//		DirContext initialContext = JNDIManager.getCorporationContext();
	//		try
	//		{
	//			DirContext dirContext =
	//				getDirectoryContext(
	//					initialContext,
	//					securityStandards.getBillerMainOfficeId());
	//			if (dirContext == null)
	//			{
	//				addSecurityStandards(securityStandards);
	//			} else
	//			{
	//				try
	//				{
	//					Attributes attrs = new BasicAttributes(true);
	//					populateAttributes(attrs, securityStandards);
	//					dirContext.modifyAttributes("", DirContext.REPLACE_ATTRIBUTE, attrs); //$NON-NLS-1$
	//				} finally
	//				{
	//					JNDIManager.releaseDirContext(dirContext);
	//				}
	//			}
	//		} catch (NamingException e)
	//		{
	//			//  EMoneyGramAdmLogger.getLogger().error(e.getMessage(), e);
	//			System.out.println(
	//				"Error: JNDI naming exception, - '" + e.getMessage() + "'");
	//			throw new DataSourceException(e.getMessage());
	//		} finally
	//		{
	//			JNDIManager.releaseDirContext(initialContext);
	//		}
	//	}
	//
	//	public void updateBusinessAdminSettings(BusinessAdminSettings businessAdminSettings)
	//		throws ParseException, DataSourceException
	//	{
	//		DirContext initialContext = JNDIManager.getCorporationContext();
	//		try
	//		{
	//			DirContext dirContext =
	//				getDirectoryContext(
	//					initialContext,
	//					businessAdminSettings.getBillerMainOfficeId());
	//			if (dirContext == null)
	//			{
	//				addBusinessAdminSettings(businessAdminSettings);
	//			} else
	//			{
	//				try
	//				{
	//					Attributes attrs = new BasicAttributes(true);
	//					populateAttributes(attrs, businessAdminSettings);
	//					dirContext.modifyAttributes("", DirContext.REPLACE_ATTRIBUTE, attrs); //$NON-NLS-1$
	//				} finally
	//				{
	//					JNDIManager.releaseDirContext(dirContext);
	//				}
	//			}
	//		} catch (NamingException e)
	//		{
	//			//  EMoneyGramAdmLogger.getLogger().error(e.getMessage(), e);
	//			System.out.println(
	//				"Error: JNDI naming exception - '" + e.getMessage() + '"');
	//			throw new DataSourceException(e.getMessage());
	//		} finally
	//		{
	//			JNDIManager.releaseDirContext(initialContext);
	//		}
	//	}
	//
	//	private void populateAttributes(
	//		Attributes attrs,
	//		BusinessAdminSettings businessAdminSettings) {
	//		Attribute businessWeek =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_BUSINESS_WEEK,
	//				businessAdminSettings.getBusinessWeek().getTag());
	//		attrs.put(businessWeek);
	//
	//		Attribute timeZone =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_TIME_ZONE,
	//				businessAdminSettings.getTimeZone().getID());
	//		attrs.put(timeZone);
	//
	//		String endOfDayText =
	//			DateHelper.getTime24HourFormatter(
	//				businessAdminSettings.getTimeZone()).format(
	//				businessAdminSettings.getEndOfDayTime());
	//		Attribute endOfDayTime =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_EODTIME,
	//				endOfDayText);
	//		attrs.put(endOfDayTime);
	//	}
	//
	//	private void populateAttributes(
	//		Attributes attrs,
	//		SecurityStandards securityStandards) {
	//		Attribute objclass =
	//			new BasicAttribute(EMoneyGramAdmLDAPKeys.OBJECTCLASS);
	//		objclass.add(EMoneyGramAdmLDAPKeys.TOP);
	//		objclass.add(EMoneyGramAdmLDAPKeys.BILLER_OBJECT);
	//		attrs.put(objclass);
	//		Attribute minPasswordLength =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_MIN_PASSWORD_LENGTH,
	//				String.valueOf(securityStandards.getMinimumPasswordLength()));
	//		attrs.put(minPasswordLength);
	//		Attribute minPasswordAlphaChars =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_MIN_ALPHA_PASSWORD_CHARS,
	//				String.valueOf(
	//					securityStandards.getMinimumAlphaCharsInPassword()));
	//		attrs.put(minPasswordAlphaChars);
	//		Attribute minPasswordNumericChars =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_MIN_NUMERIC_PASSWORD_CHARS,
	//				String.valueOf(
	//					securityStandards.getMinimumNumericCharsInPassword()));
	//		attrs.put(minPasswordNumericChars);
	//		Attribute maxPasswordDuration =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER_PASSWORD_RESET_DURATION,
	//				String.valueOf(securityStandards.getPasswordResetDuration()));
	//		attrs.put(maxPasswordDuration);
	//		Attribute maxInactivityDuration =
	//			new BasicAttribute(
	//				EMoneyGramAdmLDAPKeys.BILLER__MAX_LOGIN_INAVTIVITY_DAYS,
	//				String.valueOf(securityStandards.getMaximumLoginInactivity()));
	//		attrs.put(maxInactivityDuration);
	//	}

	//	public Agent getMainOffice(String agentId) throws DataSourceException {
	//		Agent mainOffice = null;
	//		try {
	//			Collection c = getAgents(agentId, Agent.MAIN_OFFICE_LEVEL);
	//			if (c.size() > 0) {
	//				mainOffice = (Agent) c.iterator().next();
	//			}
	//		} catch (SQLException e) {
	//			//  EMoneyGramAdmLogger.getLogger().error(e.getMessage(), e);
	//			System.out.println(
	//				"Error: SQL exception - '" + e.getMessage() + '"');
	//			throw new DataSourceException(e.getMessage());
	//		}
	//		return (mainOffice);
	//	}

	//	public List getDivisions(String mainOfficeAgentId)
	//		throws DataSourceException {
	//		try {
	//			return getAgents(mainOfficeAgentId, Agent.DIVISION_LEVEL);
	//		} catch (SQLException e) {
	//			//  EMoneyGramAdmLogger.getLogger().error(e.getMessage(), e);
	//			System.out.println(
	//				"Error: SQL exception - '" + e.getMessage() + '"');
	//			throw new DataSourceException(e.getMessage());
	//		}
	//	}

	//	BillerMainOffice retrieveCorporation(String guid)
	//		throws ParseException, DataSourceException
	//	{
	//		DirContext dirContext = null;
	//		BillerMainOffice corp = null;
	//		try
	//		{
	//			dirContext = JNDIManager.getCorporationContext();
	//			SearchControls searchControls = new SearchControls();
	//			searchControls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
	//			NamingEnumeration enum = dirContext.search("", "(&(cn=" + guid + "))", searchControls); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
	//
	//			if (enum.hasMoreElements())
	//			{
	//				DirContext context =
	//					(DirContext) dirContext.lookup(
	//						((NameClassPair) enum.next()).getName());
	//				Attributes attributes = context.getAttributes(""); //$NON-NLS-1$
	//				corp = retrieveCorporation(attributes);
	//				JNDIManager.releaseDirContext(context);
	//			}
	//		} catch (NamingException e)
	//		{
	//			throw new DataSourceException(e.getMessage());
	//		} finally
	//		{
	//			JNDIManager.releaseDirContext(dirContext);
	//		}
	//		return corp;
	//	}
	//
	//	BillerMainOffice retrieveCorporation(Attributes attributes)
	//		throws ParseException, NamingException, DataSourceException {
	//		String id = (String) attributes.get(EMoneyGramAdmLDAPKeys.CN).get();
	//		BusinessAdminSettings businessAdminSettings =
	//			retrieveBusinessAdminSettings(attributes);
	//		SecurityStandards securityStandards =
	//			retrieveSecurityStandards(attributes);
	//		BillerHierarchy bh = new BillerHierarchy(id);
	//		return (
	//			BillerMainOfficeFactory.createBillerMainOffice(
	//				id,
	//				businessAdminSettings,
	//				securityStandards,
	//				bh));
	//	}

	/* (non-Javadoc)
	 * @see emgadm.dataaccessors.AbstractBillerMainOfficeManagerImpl#retrieveCorporation(java.lang.String)
	 */
	//	BillerMainOffice retrieveCorporation(String guid)
	//		throws ParseException, DataSourceException {
	//		return null;
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see emgadm.dataaccessors.BillerMainOfficeManager#updateSecurityStandards(emgadm.model.SecurityStandards)
	//	 */
	//	public void updateSecurityStandards(SecurityStandards securityStandards)
	//		throws ParseException, DataSourceException {
	//
	//	}
	//
	//	/* (non-Javadoc)
	//	 * @see emgadm.dataaccessors.BillerMainOfficeManager#updateBusinessAdminSettings(emgadm.model.BusinessAdminSettings)
	//	 */
	//	public void updateBusinessAdminSettings(BusinessAdminSettings businessAdminSettings)
	//		throws ParseException, DataSourceException {
	//
	//	}
}
