package emgadm.dataaccessors;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;

import emgadm.constants.EMoneyGramAdmLDAPKeys;
import emgadm.exceptions.InvalidUserException;
import emgadm.exceptions.LockedUserException;
import emgadm.exceptions.UserAuthenticationException;
import emgadm.model.UserProfile;
import emgadm.model.UserProfileFactory;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmContainerProperties;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.DataSourceException;

abstract class AbstractUserProfileManagerImpl implements UserProfileManager {
	public UserProfile authenticate(String userId, String password) throws DataSourceException, UserAuthenticationException,
			InvalidUserException, LockedUserException {
		DirContext dirContext = null;

		try {
			dirContext = JNDIManager.getUserDirContext(userId, password);
		} catch (UserAuthenticationException uae) {
			try {
				dirContext = JNDIManager.getInitialContext("c=" + userId + ","
						+ EMTAdmContainerProperties.getLDAP_USER_CONTEXT());
			} catch (DataSourceException ignoreSecondaryException) {
				ignoreSecondaryException.printStackTrace();
			} finally {
				JNDIManager.releaseDirContext(dirContext);
			}

			throw uae;
		} catch (DataSourceException e) {
			if (e.getMessage().lastIndexOf("error code 53") > 0) {
				throw new LockedUserException();
			}
		}

		UserProfile up;

		try {
			up = buildUserFromMap(getMap(dirContext.getAttributes("")));
		} catch (InvalidGuidException e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(this.getClass().getName(), e);
			throw new InvalidUserException(e);
		} catch (ParseException e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(this.getClass().getName(), e);
			e.printStackTrace();
			throw new InvalidUserException(e);
		} catch (NamingException e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(this.getClass().getName(), e);
			throw new DataSourceException(e);
		} finally {
			JNDIManager.releaseDirContext(dirContext);
		}

		return (up);
	}

	protected static Map getMap(Attributes attributes) throws NamingException {
		HashMap map = new HashMap();
		NamingEnumeration keys = attributes.getAll();
		Attribute key;
		NamingEnumeration namingEnum;
		ArrayList values;
		while (keys.hasMore()) {
			key = (Attribute) keys.next();
			namingEnum = key.getAll();
			values = new ArrayList();
			while (namingEnum.hasMore()) {
				values.add(namingEnum.next());
			}
			if (values.size() > 1) {
				map.put(key.getID(), values);
			} else {
				map.put(key.getID(), values.get(0));
			}
		}
		return map;
	}

	protected UserProfile buildUserFromMap(Map m) throws DataSourceException, InvalidGuidException, NamingException,
			InvalidUserException, ParseException {
		UserProfile up = UserProfileFactory.createUserProfile((String) m.get(EMoneyGramAdmLDAPKeys.USER_ID));

		up.setGuid((String) (m.get(EMoneyGramAdmLDAPKeys.USER_GUID)));
		Object givenName = m.get(EMoneyGramAdmLDAPKeys.USER_FIRST_NAME);
		if (givenName == null ){
			up.setFirstName(""); // do nothing...unusual to not have a first name but it's happened once otherwise this wouldn't be here :)
		} else if (givenName instanceof String) {
			up.setFirstName((String) (givenName));
		} else {  // a few rare users have more than one given name...
			up.setFirstName((String)((List)givenName).get(0));
		}
		up.setLastName((String) m.get(EMoneyGramAdmLDAPKeys.USER_LAST_NAME));
		up.setEmailAddress((String) m.get(EMoneyGramAdmLDAPKeys.USER_EMAIL));
		up.setRoles(RoleManager.getEmtRoles(m.get(EMoneyGramAdmLDAPKeys.USER_ROLES)));
		up.setInternalUser(true);

		return up;
	}

	String getNewUserID(String firstName, String lastName) {
		int nbrFound = 1;
		int i = 1;
		String userName = null;
		UserQueryCriterion uqc = null;
		while (nbrFound > 0) {
			userName = generateUserID(firstName, lastName, i);
			uqc = new UserQueryCriterion();
			uqc.setUserID(userName);
			try {
				nbrFound = getCommonUserIdCount(uqc);
			} catch (Exception e) {
				// if not found then just break out of the loop;
				break;
			}
			if (nbrFound == 0) {
				break;
			}
			i++;
		}
		return userName;
	}

	String generateUserID(String firstName, String lastName, int nbrSequence) {
		String userID = null;
		String zeroes = "0000";
		Integer IDCount = new Integer(nbrSequence);
		String sIDCount = IDCount.toString();
		sIDCount = zeroes.substring(0, 3 - sIDCount.length()) + sIDCount;
		lastName = lastName.trim();
		firstName = firstName.trim();
		while ((lastName.indexOf(' ') > -1)) {
			lastName = lastName.substring(0, lastName.indexOf(' '))
					+ lastName.substring(lastName.indexOf(' ') + 1, lastName.length());
		}
		if (firstName.length() > 0) {
			userID = firstName.substring(0, 1);
		}
		if (lastName.length() > 3) {
			userID = userID + lastName.substring(0, 4);
		} else if (lastName.length() > 0) {
			userID = userID + lastName.substring(0, lastName.length());
		}
		userID = userID + zeroes.substring(0, (5 - userID.length())) + sIDCount;
		return userID;
	}

	int getCommonUserIdCount(UserQueryCriterion uqc) throws DataSourceException, NamingException {
		DirContext dirContext = null;
		try {
			dirContext = JNDIManager.getInitialContext(EMTAdmContainerProperties.getLDAP_MAIN_USER_TREE_CONTEXT());
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration namingEnum = dirContext.search("", uqc.getLDAPFilter(), searchControls);

			int count = 0;
			while (namingEnum.hasMore()) {
				NameClassPair pair = (NameClassPair) namingEnum.next();

				if (pair.getName() == null) {
					continue;
				}
				try {
					DirContext userContext = (DirContext) dirContext.lookup(pair.getName());
					userContext.close();
					count++;
				} catch (Exception e) {
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(uqc.getUserID() + " does not exist");
				}
			}
			return count;
		} catch (NamingException namingException) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(namingException.getMessage(), namingException);
			throw namingException;
		} finally {
			dirContext.close();
		}
	}

	protected void setSingleValueAttribute(DirContext dirContext, Attribute attribute) throws NamingException {
		BasicAttributes attributes = new BasicAttributes();
		attributes.put(attribute);
		try {
			if (dirContext.getAttributes("").get(attribute.getID()) == null) {
				dirContext.modifyAttributes("", DirContext.ADD_ATTRIBUTE, attributes);
			} else {
				dirContext.modifyAttributes("", DirContext.REPLACE_ATTRIBUTE, attributes);
			}
		} catch (NamingException namingException) {
			throw namingException;
		}
	}

	public boolean isPasswordSameAsExisting(String userId, String newPassword) {
		try {
			DirContext context = JNDIManager.getUserDirContext(userId, newPassword);
			context.close();
			return true;
		} catch (UserAuthenticationException e) {
			return false;
		} catch (DataSourceException e) {
			return false;
		} catch (NamingException e) {
			return false;
		}
	}

//	protected abstract int incrementWebPasswordRetryCount(DirContext dirContext, String userId) throws NamingException,
//			DataSourceException;
}
