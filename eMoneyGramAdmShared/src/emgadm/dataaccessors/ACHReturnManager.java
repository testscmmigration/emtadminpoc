package emgadm.dataaccessors;

import java.sql.SQLException;
import java.util.Collection;

import emgadm.exceptions.TooManyResultException;
import emgadm.model.AchReturn;
import emgadm.model.AchReturnSearchRequest;
import emgshared.exceptions.DataSourceException;

public interface ACHReturnManager
{

	Collection getAchReturnQueue(String userId, AchReturnSearchRequest acsr)
		throws DataSourceException, SQLException, TooManyResultException;

	Collection getAchReturnStatus(String userId) throws DataSourceException;


	AchReturn getAchReturn(String userId, int achReturnTranId);
	
	int setAchReturnStatus(String userId, int achTranId, String status);
	
	public Collection getAchTranTypes() throws DataSourceException, SQLException;
	
}
