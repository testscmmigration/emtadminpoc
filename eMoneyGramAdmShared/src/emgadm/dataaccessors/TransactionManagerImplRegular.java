package emgadm.dataaccessors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.struts.util.LabelValueBean;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import com.moneygram.common.util.StringUtility;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.exceptions.TooManyResultException;
import emgadm.model.AccountType;
import emgadm.model.AchReturn;
import emgadm.model.ChargebackAction;
import emgadm.model.ChargebackComment;
import emgadm.model.ChargebackDenialType;
import emgadm.model.ChargebackProviderCase;
import emgadm.model.ChargebackStatus;
import emgadm.model.ChargebackTrackingSearchRequest;
import emgadm.model.ChargebackTransactionHeader;
import emgadm.model.ConsumerProfileDashboard;
import emgadm.model.ConsumerProfileDashboardFactory;
import emgadm.model.CustAddress;
import emgadm.model.CustomerContactMethodType;
import emgadm.model.DeadTransaction;
import emgadm.model.GenderType;
import emgadm.model.GlAccount;
import emgadm.model.IPHistoryDetail;
import emgadm.model.IPHistorySummary;
import emgadm.model.IpConsumer;
import emgadm.model.JournalEntry;
import emgadm.model.PartnerSite;
import emgadm.model.RecentTranReceiver;
import emgadm.model.TranAction;
import emgadm.model.TranComment;
import emgadm.model.TranCommentReason;
import emgadm.model.TranStatus;
import emgadm.model.TranSubStatus;
import emgadm.model.TranType;
import emgadm.model.VBVViewBean;
import emgadm.property.EMTAdmAppProperties;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.AbstractOracleDAO;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.dataaccessors.OracleAccess;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.BillerLimit;
import emgshared.model.ConsumerProfile;
import emgshared.model.EMTTransactionType;
import emgshared.model.FileControl;
import emgshared.model.IPDetails;
import emgshared.model.MicroDeposit;
import emgshared.model.Transaction;
import emgshared.model.TransactionScore;
import emgshared.model.TransactionScoreCategory;
import emgshared.model.TransactionSearchRequest;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;

class TransactionManagerImplRegular implements TransactionManager {

	private static final EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	static Logger logger = LogFactory.getInstance().getLogger(
			TransactionManagerImplRegular.class);

	public static final String dbCallTypeCode = "WEB";

	private static final TransactionManager _instance = new TransactionManagerImplRegular();

	private DateFormatter df = new DateFormatter("yyyyMMdd", true);

	private DateFormatter df1 = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

	private static final NumberFormat nf = new DecimalFormat("#0.00");

	private TransactionManagerImplRegular() {
	}

	static TransactionManager instance() {
		return _instance;
	}

	public Collection getTransactionQueue(TransactionSearchRequest tsr)
			throws DataSourceException, SQLException, TooManyResultException {
		Collection transactions = new HashSet();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_transactions.prc_get_emg_transaction_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR); // 1 -- iv_user_id IN
												// customer.cust_logon_id%TYPE,
			cs.setNull(++parm, Types.VARCHAR); // 2 -- iv_call_type_code IN
												// process_log_a.call_type_code%TYPE,
			if (StringHelper.isNullOrEmpty(tsr.getBeginDate())) {
				cs.setNull(++parm, Types.VARCHAR); // 3.1 -- iv_begin_date IN
													// VARCHAR2,
			} else {
				cs.setString(++parm, tsr.getBeginDate()); // 3.2 --
															// iv_begin_date IN
															// VARCHAR2,
			}
			if (StringHelper.isNullOrEmpty(tsr.getEndDate())) {
				cs.setNull(++parm, Types.VARCHAR); // 4.1 -- iv_end_date IN
													// VARCHAR2,
			} else {
				cs.setString(++parm, tsr.getEndDate()); // 4.2 -- iv_end_date IN
														// VARCHAR2,
			}
			cs.setNull(++parm, Types.INTEGER); // 5.1 -- iv_emg_tran_id IN
												// emg_transaction.emg_tran_id%TYPE,
			if (StringHelper.isNullOrEmpty(tsr.getTranType())) {
				cs.setNull(++parm, Types.VARCHAR); // 6.1 --
													// iv_emg_tran_type_code IN
													// emg_transaction.emg_tran_type_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranType()); // 6.2 --
															// iv_emg_tran_type_code
															// IN
															// emg_transaction.emg_tran_type_code%TYPE,
			}
			cs.setNull(++parm, Types.INTEGER); // 7 -- iv_snd_cust_acct_id IN
												// emg_transaction.emg_tran_type_code%TYPE,
			cs.setNull(++parm, Types.INTEGER); // 8 -- iv_snd_cust_acct_ver_nbr
												// IN
												// emg_transaction.snd_cust_acct_id%TYPE,
			if (StringHelper.isNullOrEmpty(tsr.getTranStatCode())) {
				cs.setNull(++parm, Types.CHAR); // 9.1 -- iv_tran_stat_code IN
												// emg_transaction.tran_stat_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranStatCode()); // 9.2 --
																// iv_tran_stat_code
																// IN
																// emg_transaction.tran_stat_code%TYPE,
			}
			if (StringHelper.isNullOrEmpty(tsr.getTranSubStatCode())) {
				cs.setNull(++parm, Types.CHAR); // 10.1 -- iv_tran_sub_stat_code
												// IN
												// emg_transaction.tran_sub_stat_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranSubStatCode()); // 10.2 --
																// iv_tran_sub_stat_code
																// IN
																// emg_transaction.tran_sub_stat_code%TYPE,
			}
			cs.setNull(++parm, Types.DATE); // 11 -- iv_tran_stat_date IN
											// emg_transaction.tran_stat_date%TYPE,

			if (StringHelper.isNullOrEmpty(tsr.getRcvAgcyCode())) {
				cs.setNull(++parm, Types.CHAR); // 12.1 -- iv_rcv_agcy_code IN
												// emg_transaction.rcv_agcy_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getRcvAgcyCode()); // 12.2 --
															// iv_rcv_agcy_code
															// IN
															// emg_transaction.rcv_agcy_code%TYPE,
			}

			if ("Y".equalsIgnoreCase(tsr.getUseScore())) {
				cs.setInt(++parm, tsr.getLowScore()); // 13.1 --
														// iv_low_tran_score_nbr
														// IN
														// emg_tran_score.tran_score_nbr%TYPE,
				cs.setInt(++parm, tsr.getHighScore()); // 14.1 --
														// iv_high_tran_score_nbr
														// IN
														// emg_tran_score.tran_score_nbr%TYPE,
			} else {
				cs.setNull(++parm, Types.INTEGER); // 13.1 --
													// iv_low_tran_score_nbr IN
													// emg_tran_score.tran_score_nbr%TYPE,
				cs.setNull(++parm, Types.INTEGER); // 14.1 --
													// iv_high_tran_score_nbr IN
													// emg_tran_score.tran_score_nbr%TYPE,
			}
			if (StringHelper.isNullOrEmpty(tsr.getSysAutoRsltCode())) {
				cs.setNull(++parm, Types.VARCHAR); // 15.1 --
													// iv_sys_auto_rslt_code IN
													// emg_tran_score.sys_auto_rslt_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getSysAutoRsltCode()); // 15.2 --
																// iv_sys_auto_rslt_code
																// IN
																// emg_tran_score.sys_auto_rslt_code%TYPE,
			}
			cs.setNull(++parm, Types.VARCHAR); // 16 -- iv_inet_purch_flag IN
												// emg_transaction.inet_purch_flag%TYPE,
			cs.setNull(++parm, Types.VARCHAR); // 17 -- iv_tran_rvw_prcs_code IN
												// emg_transaction.tran_rvw_prcs_code%TYPE,
			cs.setNull(++parm, Types.VARCHAR); // 18 --
												// iv_include_fund_rqst_dateIN
												// CHAR,

			/*
			 * if (!StringHelper.isNullOrEmpty(tsr.getPartnerSiteId())) {
			 * cs.setNull(++parm, Types.VARCHAR); } else { cs.setInt(++parm,
			 * Integer.valueOf(tsr.getPartnerSiteId())); }
			 */

			cs.setNull(++parm, Types.INTEGER); // 19 --
												// iv_tran_crt_src_web_site_code
												// IN
												// emg_transaction.tran_create_src_web_site_code%TYPE,

			if (!StringHelper.isNullOrEmpty(tsr.getAdditionalIdType())) {
				cs.setNull(++parm, Types.VARCHAR); // 20 --
													// iv_ident_doc_bsns_code IN
													// identification_doc_cntry.ident_doc_bsns_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getAdditionalIdType()); // 20 --
																	// iv_ident_doc_bsns_code
																	// IN
																	// identification_doc_cntry.ident_doc_bsns_code%TYPE,
			}

			if (!StringHelper.isNullOrEmpty(tsr.getAdditionalIdEncrypted())) {
				cs.setNull(++parm, Types.VARCHAR); // 21 -- iv_encrypt_extnl_id
													// IN
													// cust_extnl_ident.encrypt_extnl_id%TYPE,
			} else {
				cs.setString(++parm, tsr.getAdditionalIdEncrypted());// 21 --
																		// iv_encrypt_extnl_id
																		// IN
																		// cust_extnl_ident.encrypt_extnl_id%TYPE,
			}

			if (!StringHelper.isNullOrEmpty(tsr.getAdditionalIdMasked())) {
				cs.setNull(++parm, Types.VARCHAR); // 22 -- iv_mask_extnl_id IN
													// cust_extnl_ident.mask_extnl_id%TYPE,
			} else {
				cs.setString(++parm, tsr.getAdditionalIdMasked()); // 22 --
																	// iv_mask_extnl_id
																	// IN
																	// cust_extnl_ident.mask_extnl_id%TYPE,
			}
			cs.setNull(++parm, Types.VARCHAR); // 23 --
												// iv_ident_doc_stat_bsns_code
												// IN
												// ident_doc_stat.bsns_code%TYPE,
			int dbLogIdIndex = -1;
			try {
				// output parameters
				cs.registerOutParameter(++parm, Types.INTEGER); // 24 --
																// ov_prcs_log_id
																// OUT
																// process_log_a.prcs_log_id%TYPE,
				dbLogIdIndex = parm;
				cs.registerOutParameter(++parm, OracleTypes.CURSOR); // 25 --
																		// ov_transaction_cv
																		// OUT
																		// transaction_cv_type

				cs.execute();
			} catch (Exception e) {
				String message = e.getMessage();
			}

			long dbLogId = cs.getLong(dbLogIdIndex);
			rs = (ResultSet) cs.getObject(parm);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			long cnt = 0;

			while (rs.next()) {
				if (tsr.getStatusDate() == null
						|| df.format(rs.getDate("TRAN_STAT_DATE")).compareTo(
								df.format(tsr.getStatusDate())) <= 0) {
					transactions.add(createTransactionBeanForSearch(rs, false));
				} else {
					transactions.add(createTransactionBeanForSearch(rs, true));
				}

				if (cnt++ > maxDownloadTransactions) {
					logEndTime(dbLogId, conn, cnt);
					throw new TooManyResultException(
							"Too many results and maximum download transaction is "
									+ maxDownloadTransactions);
				}
			}
			logEndTime(dbLogId, conn, cnt);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return transactions;
	}

	public Collection getTransactionQueueDocStatus(TransactionSearchRequest tsr)
			throws DataSourceException, SQLException, TooManyResultException {
		Collection transactions = new HashSet();
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_transactions.prc_get_emg_tran_by_doc_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?,?,?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR); // 1 -- iv_user_id IN
												// customer.cust_logon_id%TYPE,
			cs.setNull(++parm, Types.VARCHAR); // 2 -- iv_call_type_code IN
												// process_log_a.call_type_code%TYPE,

			if(StringHelper.isNullOrEmpty(tsr.getDocStatus())){
				cs.setNull(++parm, Types.VARCHAR); // 3.1 -- iv_ident_doc_stat_bsns_code
			} else {
				cs.setString(++parm, tsr.getDocStatus()); // 3.2 --
			}

			if (StringHelper.isNullOrEmpty(tsr.getBeginDate())) {
				cs.setNull(++parm, Types.VARCHAR); // 4.1 -- iv_ident_doc_stat_begin_date
													// VARCHAR2,
			} else {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				try {
					long l=dateFormat.parse(tsr.getBeginDate()).getTime();
					java.sql.Date convertedDate = new java.sql.Date(l);
					cs.setDate(++parm, convertedDate); // 4.2
				} catch (Exception e) {

				}
			}

			if (StringHelper.isNullOrEmpty(tsr.getEndDate())) {
				cs.setNull(++parm, Types.VARCHAR); // 5.1 -- iv_ident_doc_stat_end_date
			} else {
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				try {
					java.sql.Date convertedDate = new java.sql.Date(dateFormat.parse(tsr.getEndDate()).getTime());
					cs.setDate(++parm, convertedDate); // 4.2
				} catch (Exception e) {

				}
			}
			if (StringHelper.isNullOrEmpty(tsr.getTranType())) {
				cs.setNull(++parm, Types.VARCHAR); // 6.1 --
													// iv_emg_tran_type_code IN
													// emg_transaction.emg_tran_type_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranType()); // 6.2 --
															// iv_emg_tran_type_code
															// IN
															// emg_transaction.emg_tran_type_code%TYPE,
			}
			if (StringHelper.isNullOrEmpty(tsr.getTranStatCode())) {
				cs.setNull(++parm, Types.CHAR); // 7.1 -- iv_tran_stat_code IN
												// emg_transaction.tran_stat_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranStatCode()); // 7.2 -- iv_tran_stat_code
																// IN
																// emg_transaction.tran_stat_code%TYPE,
			}
			if (StringHelper.isNullOrEmpty(tsr.getTranSubStatCode())) {
				cs.setNull(++parm, Types.CHAR); // 8.1 -- iv_tran_sub_stat_code
												// IN
												// emg_transaction.tran_sub_stat_code%TYPE,
			} else {
				cs.setString(++parm, tsr.getTranSubStatCode()); // 8.2 --
																// iv_tran_sub_stat_code
																// IN
																// emg_transaction.tran_sub_stat_code%TYPE,
			}


			if (StringHelper.isNullOrEmpty(tsr.getPartnerSiteId())) {
				cs.setNull(++parm, Types.VARCHAR);				// 9 -- iv_prfl_crt_src_web_site_cd
			} else {
				cs.setInt(++parm, Constants.partnerSiteIdToCode.get(tsr.getPartnerSiteId()));
			}

			int dbLogIdIndex = -1;
			try {
				// output parameters
				cs.registerOutParameter(++parm, Types.INTEGER); // 24 --
																// ov_prcs_log_id
																// OUT
																// process_log_a.prcs_log_id%TYPE,
				dbLogIdIndex = parm;
				cs.registerOutParameter(++parm, OracleTypes.CURSOR); // 25 --
																		// ov_transaction_cv
																		// OUT
																		// transaction_cv_type

				cs.execute();
			} catch (Exception e) {
				String message = e.getMessage();
			}

			long dbLogId = cs.getLong(dbLogIdIndex);
			rs = (ResultSet) cs.getObject(parm);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			long cnt = 0;

			while (rs.next()) {
				if (tsr.getStatusDate() == null
						|| df.format(rs.getDate("TRAN_STAT_DATE")).compareTo(
								df.format(tsr.getStatusDate())) <= 0) {
					transactions.add(createTransactionBeanForSearch(rs, false));
				} else {
					transactions.add(createTransactionBeanForSearch(rs, true));
				}

				if (cnt++ > maxDownloadTransactions) {
					logEndTime(dbLogId, conn, cnt);
					throw new TooManyResultException(
							"Too many results and maximum download transaction is "
									+ maxDownloadTransactions);
				}
			}
			logEndTime(dbLogId, conn, cnt);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return transactions;
	}

	public Transaction getTransaction(int tranId) {
		ResultSet rs = null;
		CallableStatement cs = null;
		Connection conn = null;
		Transaction transaction = null;

		String storedProcName = "pkg_em_transactions.prc_get_emg_transaction_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setInt(++parm, tranId);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.INTEGER);
			cs.setNull(++parm, Types.INTEGER);
			cs.setNull(++parm, Types.CHAR);
			cs.setNull(++parm, Types.CHAR);
			cs.setNull(++parm, Types.DATE);
			cs.setNull(++parm, Types.CHAR);
			cs.setNull(++parm, Types.INTEGER);
			cs.setNull(++parm, Types.INTEGER);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			// output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(parm);

			if (rs.next()) {
				transaction = createTransactionBean(conn, rs);
			}
			logEndTime(dbLogId, conn, 1);
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return transaction;
	}

	private Transaction createTransactionBean(Connection conn, ResultSet rs)
			throws SQLException {
		return createTransactionBean(conn, rs, false);
	}

	private Transaction createTransactionBean(Connection conn, ResultSet rs,
			boolean esUnsendable) throws SQLException {
		int tranId = rs.getInt("EMG_TRAN_ID");
		String statusCode = rs.getString("TRAN_STAT_CODE");
		String subStatusCode = rs.getString("TRAN_SUB_STAT_CODE");
		TransactionStatus status = TransactionStatus.getInstance(statusCode,
				subStatusCode);
		Transaction transaction = new Transaction(status);
		transaction.setEmgTranId(tranId);
		transaction.setEmgTranTypeCode(rs.getString("EMG_TRAN_TYPE_CODE"));
		transaction.setEmgTranTypeDesc(rs.getString("EMG_TRAN_TYPE_DESC"));
		transaction.setSndCustAcctId(rs.getInt("SND_CUST_ACCT_ID"));
		transaction.setSndCustLogonId(rs.getString("SND_CUST_LOGON_ID"));
		transaction.setCustId(rs.getInt("SND_CUST_ID"));
		transaction.setSndCustFrstName(rs.getString("SND_CUST_FRST_NAME"));
		transaction.setSndCustLastName(rs.getString("SND_CUST_LAST_NAME"));
		transaction.setSndCustScndLastName(rs.getString("SND_CUST_MATRNL_NAME"));
		transaction.setSndCustSsnMaskNbr(rs.getString("SND_CUST_SSN_MASK_NBR"));
		transaction.setSndCustAcctVerNbr(rs.getInt("SND_CUST_ACCT_VER_NBR"));
		transaction.setSndAcctTypeCode(rs.getString("ACCT_TYPE_CODE"));
		transaction.setSndAcctMaskNbr(rs.getString("ACCT_MASK_NBR"));
		transaction.setSndCustBkupAcctId(rs.getInt("SND_CUST_BKUP_ACCT_ID"));
		transaction.setSndCustBkupAcctVerNbr(rs
				.getInt("SND_CUST_BKUP_ACCT_VER_NBR"));
		transaction.setSndBkupAcctTypeCode(rs.getString("BKUP_ACCT_TYPE_CODE"));
		transaction.setSndBkupAcctMaskNbr(rs.getString("BKUP_ACCT_MASK_NBR"));
		transaction.setBankAbaNbr(rs.getString("BANK_ABA_NBR"));
		transaction.setLgcyRefNbr(rs.getString("LGCY_REF_NBR"));
		transaction.setSndTranDate(rs.getTimestamp("SND_TRAN_DATE"));
		transaction.setTranStatDesc(rs.getString("TRAN_STAT_DESC"));
		transaction.setTranSubStatDesc(rs.getString("TRAN_SUB_STAT_DESC"));
		transaction.setTranStatDate(rs.getTimestamp("TRAN_STAT_DATE"));
		transaction.setSndAgentId(rs.getInt("SND_AGENT_ID"));
		transaction.setDlvrOptnId(rs.getInt("DLVR_OPTN_ID"));
		transaction.setSndISOCntryCode(rs.getString("SND_ISO_CNTRY_CODE"));
		transaction.setSndISOCrncyId(rs.getString("SND_ISO_CRNCY_ID"));
		transaction.setSndFaceAmt(rs.getBigDecimal("SND_FACE_AMT"));
		transaction.setSndFeeAmt(rs.getBigDecimal("SND_FEE_AMT"));
		transaction.setRtnFeeAmt(rs.getBigDecimal("RTN_FEE_AMT"));
		transaction.setSndTotAmt(rs.getBigDecimal("SND_TOT_AMT"));
		transaction.setSndFxCnsmrRate(rs.getBigDecimal("SND_FX_CNSMR_RATE"));
		transaction.setSndThrldWarnAmt(rs.getBigDecimal("SND_THRLD_WARN_AMT"));
		transaction.setFormFreeConfNbr(rs.getInt("FORM_FREE_CONF_NBR"));
		transaction.setIntndRcvAgentId(rs.getInt("INTND_RCV_AGENT_ID"));
		transaction.setRcvAgentId(rs.getInt("RCV_AGENT_ID"));
		transaction.setRcvAgentConfId(rs.getString("RCV_AGENT_CONF_ID"));
		transaction.setRcvAgcyCode(rs.getString("RCV_AGCY_CODE"));
		transaction.setRcvISOCntryCode(rs.getString("RCV_ISO_CNTRY_CODE"));
		transaction.setRcvISOCrncyId(rs.getString("RCV_ISO_CRNCY_ID"));
		transaction.setSndCustIPAddrId(rs.getString("SND_CUST_IP_ADDR_ID"));
		transaction.setSndMsg1Text(rs.getString("SND_MSG_1_TEXT"));
		transaction.setSndMsg2Text(rs.getString("SND_MSG_2_TEXT"));
		transaction.setSndCustAddrId(rs.getInt("SND_CUST_ADDR_ID"));
		transaction.setRcvCustFrstName(rs.getString("RCV_CUST_FRST_NAME"));
		transaction.setRcvCustMidName(rs.getString("RCV_CUST_MID_NAME"));
		transaction.setRcvCustLastName(rs.getString("RCV_CUST_LAST_NAME"));
		transaction.setRcvAgentName(rs.getString("RCV_AGENT_NAME"));
		transaction.setRiskLvlCode(rs.getString("RISK_LVL_CODE"));
		transaction.setTransScores(rs.getInt("TRAN_SCORE_NBR"));
		transaction.setSysAutoRsltCode(rs.getString("SYS_AUTO_RSLT_CODE"));
		transaction.setCsrPrcsUserid(rs.getString("CSR_PRCS_USERID"));
		transaction.setCsrPrcsDate(rs.getTimestamp("CSR_PRCS_DATE"));
		transaction.setAchFileCntlSeqNbr(rs.getInt("ACH_FILE_CNTL_SEQ_NBR"));
		transaction.setCreateDate(rs.getTimestamp("CREATE_DATE"));
		transaction.setCreateUserid(rs.getString("CREATE_USERID"));
		transaction.setLastUpdateUserid(rs.getString("LAST_UPDATE_USERID"));
		transaction.setLastUpdateDate(rs.getTimestamp("LAST_UPDATE_DATE"));
		transaction.setRcvAgcyAcctEncryp(rs
				.getString("RCV_AGCY_ACCT_ENCRYP_NBR"));
		transaction.setRcvAgcyAcctMask(rs.getString("RCV_AGCY_ACCT_MASK_NBR"));
		transaction.setEsSendable("");
		if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE)
				&& transaction.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.APPROVED_CODE)
				&& transaction.getTranSubStatCode().equalsIgnoreCase(
						TransactionStatus.ACH_SENT_CODE)) {
			transaction.setEsSendable(esUnsendable ? "No" : "Yes");
		}

		transaction.setRcvCustMatrnlName(rs.getString("RCV_CUST_MATRNL_NAME"));
		transaction.setRcvCustMidName(rs.getString("RCV_CUST_MID_NAME"));
		transaction.setRcvCustAddrStateName(rs
				.getString("RCV_CUST_ADDR_STATE_NAME"));
		transaction.setRcvAgentRefNbr(rs.getString("RCV_AGENT_REF_NBR"));
		transaction.setRcvCustAddrLine1Text(rs
				.getString("RCV_CUST_ADDR_LINE1_TEXT"));
		transaction.setRcvCustAddrLine2Text(rs
				.getString("RCV_CUST_ADDR_LINE2_TEXT"));
		transaction.setRcvCustAddrLine3Text(rs
				.getString("RCV_CUST_ADDR_LINE3_TEXT"));
		transaction.setRcvCustAddrCityName(rs
				.getString("RCV_CUST_ADDR_CITY_NAME"));
		transaction.setRcvCustAddrPostalCode(rs
				.getString("RCV_CUST_ADDR_POSTAL_CODE"));
		transaction.setRcvCustDlvrInstr1Text(rs
				.getString("RCV_CUST_DLVR_INSTR1_TEXT"));
		transaction.setRcvCustDlvrInstr2Text(rs
				.getString("RCV_CUST_DLVR_INSTR2_TEXT"));
		transaction.setRcvCustDlvrInstr3Text(rs
				.getString("RCV_CUST_DLVR_INSTR3_TEXT"));
		transaction.setRcvCustPhNbr(rs.getString("RCV_CUST_PH_NBR"));
		transaction.setRcvAgtCity(rs.getString("AGT_CITY"));
		transaction.setRcvAgtState(rs.getString("AGT_STATEPROVINCE"));
		transaction.setRcvAgtCntry(rs.getString("AGT_ISO_CNTRY"));
		transaction.setRcvDate(rs.getTimestamp("RCV_DATETIME"));
		transaction.setTranScore(getTranScoreDetail(conn, tranId));
		transaction.setInternetPurchase(rs.getString("INET_PURCH_FLAG"));
		transaction.setTranRvwPrcsCode(rs.getString("TRAN_RVW_PRCS_CODE"));
		transaction.setFundRetrievalRequestDate(rs
				.getTimestamp("FUND_RTRV_RQST_DATE"));
		transaction.setRRN(rs.getString("RCVR_RGSTN_ID"));
		transaction.setLoyaltyPgmMembershipId(rs
				.getString("LYLTY_PGM_MBSHP_ID"));
		transaction.setSndCustPhotoIdCntryCode(rs
				.getString("SND_CUST_PHOTO_ID_CNTRY_CODE"));
		transaction.setSndCustPhotoIdStateCode(rs
				.getString("SND_CUST_PHOTO_ID_STATE_CODE"));
		transaction.setSndCustLegalIdTypeCode(rs
				.getString("SND_CUST_LEGL_ID_TYPE_CODE"));
		transaction.setSndCustLegalId(rs.getString("SND_CUST_LEGL_ID"));
		transaction.setSndCustOccupationText(rs
				.getString("SND_CUST_OCUPN_TEXT"));
		transaction.setTestQuestion(rs.getString("TEST_QUEST_TEXT"));
		transaction.setTestQuestionAnswer(rs.getString("TEST_ANS_TEXT"));
		transaction.setCustomerAutoEnrollFlag(rs
				.getString("CUST_AUTO_ENRL_FLAG"));
		transaction.setSndFeeAmtNoDiscountAmt(rs
				.getBigDecimal("SND_FEE_NO_DCNT_APLY_AMT"));
		transaction.setRcvCustAddrISOCntryCode(rs
				.getString("RCV_CUST_ADDR_ISO_CNTRY_CODE"));
		transaction.setIntndDestStateProvinceCode(rs
				.getString("INTND_DEST_STATEPROVINCE_CODE"));
		transaction.setMccPartnerProfileId(rs.getString("MCC_PTNR_PRFL_ID"));
		transaction.setPartnerSiteId(rs.getString("BSNS_CODE"));
		if(transaction.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGO_PARTNER_SITE_ID)
				||transaction.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID)
				||transaction.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID)){
			transaction.setSndCustPhotoIdTypeCode(EMoneyGramAdmApplicationConstants.photoIdTypeToString.get(rs
					.getString("SND_CUST_PHOTO_ID_TYPE_CODE")));
			String photoId=rs.getString("SND_CUST_PHOTO_ID");
			transaction.setSndCustPhotoId(!StringUtility.isNullOrEmpty(photoId)?StringHelper.decryptIt(photoId):photoId);
		} else {
			transaction.setSndCustPhotoIdTypeCode(rs.getString("SND_CUST_PHOTO_ID_TYPE_CODE"));			
			transaction.setSndCustPhotoId(rs.getString("SND_CUST_PHOTO_ID"));
		}
		transaction.setThreeMinuteFreePhoneNumber(rs
				.getString("PHN_CALL_TOLL_FREE_NBR"));
		transaction.setThreeMinuteFreePinNumber(rs
				.getString("PHN_CALL_PIN_NBR"));

		transaction.setSndCustPhotoIdExpDate(rs.getTimestamp("SND_CUST_PHOTO_ID_EXP_DATE"));
		transaction.setRcvAcctMaskNbr(rs.getString("INRCV_CNSMR_ACCT_MASK_NBR"));
		try {

			// test global collect
			// transaction.setTCProviderCode(2);
			// Test cybersource
			// transaction.setTCProviderCode(1);
			// original source
			transaction.setTCProviderCode(rs.getInt("PYMT_SVC_PRVDR_CODE"));
			transaction.setTCProviderTransactionNumber(rs
					.getString("PYMT_SVC_PRVDR_TRC_NBR"));
			transaction.setTCInternalTransactionNumber(rs
					.getString("INTRNL_TRC_NBR"));
		} catch (Exception e) {
			// TODO To be removed when SP is updated
		}

		if ((rs.getBigDecimal("TRAN_RISK_SCORE_NBR")) == null) {
			transaction.setTranRiskScore(new Integer(EMTAdmAppProperties
					.getTransMonitorDefaultScore()));
			transaction.setTranRiskScoreDefaulted(true);
		} else {
			transaction.setTranRiskScore(new Integer(rs
					.getInt("TRAN_RISK_SCORE_NBR")));
		}

		transaction.setMgiTransactionSessionID(rs.getString("MG_TRAN_SESS_ID"));
		transaction.setRegulationVersion(rs.getString("SUBDIV_REG_CNTNT_VER_ID"));

		transaction.setRcvNonMgiFeeAmt(rs.getBigDecimal("INRCV_NON_MGI_FEE_AMT"));
		transaction.setRcvNonMgiTaxAmt(rs.getBigDecimal("INRCV_NON_MGI_TAX_AMT"));
		transaction.setRcvPayoutAmt(rs.getBigDecimal("INRCV_PAYOUT_AMT"));
		transaction.setRcvPayoutISOCrncyId(rs.getString("INRCV_PAYOUT_CRNCY_CODE"));
		
		// single messaging bill pay - MGO 5000
		transaction.setReceiptTextInfoEng(rs.getString("RCPT_TXT_INFO_ENG"));
		transaction.setReceiptTextInfoSpa(rs.getString("RCPT_TXT_INFO_SPA"));
		//end -MGO 5000
		
		// for req 4052
		IPDetails ipDetails = new IPDetails();
		ipDetails.setConsumerCity(rs.getString("SND_CUST_IP_ADDR_CITY_NAME"));
		ipDetails.setConsumerCountry(rs.getString("SND_CUST_IP_ADDR_CNTRY_NAME"));
		ipDetails.setConsumerDomain(rs.getString("SND_CUST_IP_ADDR_DOMN_NAME"));
		ipDetails.setConsumerISP(rs.getString("SND_CUST_IP_ADDR_ISP_NAME"));
		ipDetails.setConsumerLatitude(rs.getBigDecimal("SND_CUST_IP_ADDR_LAT_NBR"));
		ipDetails.setConsumerLongitude(rs.getBigDecimal("SND_CUST_IP_ADDR_LONG_NBR"));
		ipDetails.setConsumerRegion(rs.getString("SND_CUST_IP_ADDR_SUBDIV_NAME"));
		ipDetails.setConsumerZipCode(rs.getString("SND_CUST_IP_ADDR_ZIP_CODE"));
		transaction.setIpDetails(ipDetails);
		
		return transaction;
	}

	// a scaled down version of the createTransactionBean, used for search
	// results and fewer fields
	// need to be populated.
	private Transaction createTransactionBeanForSearch(ResultSet rs,
			boolean esUnsendable) throws SQLException {

		String statusCode = rs.getString("TRAN_STAT_CODE");
		String subStatusCode = rs.getString("TRAN_SUB_STAT_CODE");
		TransactionStatus status = TransactionStatus.getInstance(statusCode,
				subStatusCode);
		Transaction transaction = new Transaction(status);
		transaction.setCsrPrcsUserid(rs.getString("CSR_PRCS_USERID"));
		transaction.setCreateDate(rs.getTimestamp("CREATE_DATE"));
		int tranId = rs.getInt("EMG_TRAN_ID");
		transaction.setCustId(rs.getInt("SND_CUST_ID"));
		transaction.setRcvCustFrstName(rs.getString("RCV_CUST_FRST_NAME"));
		transaction.setRcvCustLastName(rs.getString("RCV_CUST_LAST_NAME"));
		transaction.setSndISOCntryCode(rs.getString("SND_ISO_CNTRY_CODE"));
		transaction.setSndISOCrncyId(rs.getString("SND_ISO_CRNCY_ID"));
		transaction.setEmgTranId(tranId);
		transaction.setEmgTranTypeCode(rs.getString("EMG_TRAN_TYPE_CODE"));
		transaction.setEmgTranTypeDesc(rs.getString("EMG_TRAN_TYPE_DESC"));
		transaction.setSndCustFrstName(rs.getString("SND_CUST_FRST_NAME"));
		transaction.setSndCustLastName(rs.getString("SND_CUST_LAST_NAME"));
		transaction.setSndCustScndLastName(rs.getString("SND_CUST_MATRNL_NAME"));
		transaction.setTranStatDesc(rs.getString("TRAN_STAT_DESC"));
		transaction.setTranSubStatDesc(rs.getString("TRAN_SUB_STAT_DESC"));
		transaction.setTranStatDate(rs.getTimestamp("TRAN_STAT_DATE"));
		transaction.setSndFaceAmt(rs.getBigDecimal("SND_FACE_AMT"));
		transaction.setSndFeeAmt(rs.getBigDecimal("SND_FEE_AMT"));
		transaction.setRtnFeeAmt(rs.getBigDecimal("RTN_FEE_AMT"));
		transaction.setSndTotAmt(rs.getBigDecimal("SND_TOT_AMT"));
		transaction.setRcvISOCntryCode(rs.getString("RCV_ISO_CNTRY_CODE"));
		transaction.setRcvISOCrncyId(rs.getString("RCV_ISO_CRNCY_ID"));
		transaction.setRiskLvlCode(rs.getString("RISK_LVL_CODE"));
		transaction.setTransScores(rs.getInt("TRAN_SCORE_NBR"));
		transaction.setSysAutoRsltCode(rs.getString("SYS_AUTO_RSLT_CODE"));
		transaction.setCsrPrcsDate(rs.getTimestamp("CSR_PRCS_DATE"));
		transaction.setMccPartnerProfileId(rs.getString("MCC_PTNR_PRFL_ID"));
		transaction.setPartnerSiteId(rs.getString("BSNS_CODE"));
		transaction.setEsSendable("");
		if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE)
				&& transaction.getTranStatCode().equalsIgnoreCase(
						TransactionStatus.APPROVED_CODE)
				&& transaction.getTranSubStatCode().equalsIgnoreCase(
						TransactionStatus.ACH_SENT_CODE)) {
			transaction.setEsSendable(esUnsendable ? "No" : "Yes");
		}

		transaction.setRcvCustMatrnlName(rs.getString("RCV_CUST_MATRNL_NAME"));
		transaction.setRcvCustMidName(rs.getString("RCV_CUST_MID_NAME"));
		transaction.setRcvCustAddrStateName(rs
				.getString("RCV_CUST_ADDR_STATE_NAME"));
		if (transaction.getEmgTranTypeCode().equalsIgnoreCase(
				TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
			transaction.setRcvAgentName(rs.getString("RCV_AGENT_NAME"));
			transaction.setRcvAgcyCode(rs.getString("RCV_AGCY_CODE"));
		}
		transaction.setInternetPurchase(rs.getString("INET_PURCH_FLAG"));
		transaction.setTranRvwPrcsCode(rs.getString("TRAN_RVW_PRCS_CODE"));
		transaction.setLoyaltyPgmMembershipId(rs
				.getString("LYLTY_PGM_MBSHP_ID"));
		transaction.setCustomerAutoEnrollFlag(rs
				.getString("CUST_AUTO_ENRL_FLAG"));
		transaction.setSndFeeAmtNoDiscountAmt(rs
				.getBigDecimal("SND_FEE_NO_DCNT_APLY_AMT"));
		transaction.setRcvCustAddrISOCntryCode(rs
				.getString("RCV_CUST_ADDR_ISO_CNTRY_CODE"));
		transaction.setIntndDestStateProvinceCode(rs
				.getString("INTND_DEST_STATEPROVINCE_CODE"));
		transaction.setMccPartnerProfileId(rs.getString("MCC_PTNR_PRFL_ID"));
		transaction.setTCInternalTransactionNumber(rs
				.getString("INTRNL_TRC_NBR"));
		transaction.setTCProviderTransactionNumber(rs
				.getString("PYMT_SVC_PRVDR_TRC_NBR"));
		transaction.setThreeMinuteFreePhoneNumber(rs
				.getString("PHN_CALL_TOLL_FREE_NBR"));
		transaction.setThreeMinuteFreePinNumber(rs
				.getString("PHN_CALL_PIN_NBR"));
		return transaction;
	}

	public int setTransactionOwnership(int tranId, String userId) {
		Double rc = new Double(-1);
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_update_transaction";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer
				.append("{ call "
						+ storedProcName
						+ "(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?, ?,?,?,?, ?,?,?, ?,?,?,?,?, ?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			Timestamp now = new Timestamp(System.currentTimeMillis());
			if (userId == null) {
				now = null;
			}

			// input parameters
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setInt(++paramIndex, tranId); // transaction id
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.DATE);
			cs.setNull(++paramIndex, Types.CHAR);
			cs.setNull(++paramIndex, Types.CHAR);
			cs.setNull(++paramIndex, Types.DATE);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setString(++paramIndex, userId); // new user id to replace
			cs.setTimestamp(++paramIndex, now); // system date
			cs.setNull(++paramIndex, Types.FLOAT);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.FLOAT);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.DECIMAL);
			cs.setNull(++paramIndex, Types.DECIMAL);
			cs.setNull(++paramIndex, Types.DECIMAL);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// additional GC parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.DATE);
            // Code Added for Story 4052
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			// output parameters
			cs.registerOutParameter(++paramIndex, Types.FLOAT);
			int outParamIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(outParamIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rc = (Double) cs.getObject(outParamIndex);
			safeCommit(conn);
			logEndTime(dbLogId, conn);
		} catch (Exception e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return rc.intValue();
	}

	public int setTransactionStatus(int tranId, TransactionStatus status,
			String userId) {
		return setTransactionStatus(tranId, status.getStatusCode(),
				status.getSubStatusCode(), userId);
	}

	public int setTransactionStatus(int tranId, String status,
			String subStatus, String userId) {
		Double rc = new Double(-1);
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_set_tran_stat_code";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setString(++parm, userId); // update user id
			cs.setString(++parm, "Web");
			cs.setInt(++parm, tranId); // transaction id
			cs.setString(++parm, status); // transaction status code
			cs.setString(++parm, subStatus); // transaction fund status code
			cs.setNull(++parm, Types.VARCHAR); // sub reason code
			// output parameters
			cs.registerOutParameter(++parm, Types.FLOAT);
			int dbLogIdIndex = parm;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rc = (Double) cs.getObject(parm);
			safeCommit(conn);
			logEndTime(dbLogId, conn);

		} catch (Exception e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return rc.intValue();
	}

	public void updateTransaction(Transaction tran, String userId) {
		updateTransaction(tran, userId, (String) null, (Float) null);
	}

	public void updateTransaction(Transaction tran, String userId,
			String actionConfId, Float postAmount) {
		updateTransaction(tran, userId, actionConfId, postAmount, null);
	}

	public void updateTransaction(Transaction tran, String userId,
			String actionConfId, Float postAmount, String subReasonCode) {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_update_transaction";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer
				.append("{ call "
						+ storedProcName
						+ "(?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?, ?,?,?,?, ?,? ,?,?,?,? ,?,?,?,? ,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			Timestamp now = new Timestamp(System.currentTimeMillis());
			java.util.Date sendDate = tran.getSndTranDate();
			Timestamp sqlSendDate = (sendDate == null ? null : new Timestamp(
					sendDate.getTime()));
			java.util.Date statDate = tran.getTranStatDate();
			Timestamp sqlStatDate = (statDate == null ? null : new Timestamp(
					statDate.getTime()));

			// input parameters
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setInt(++paramIndex, tran.getEmgTranId());
			cs.setString(++paramIndex, tran.getLgcyRefNbr());
			cs.setTimestamp(++paramIndex, sqlSendDate);
			cs.setString(++paramIndex, tran.getTranStatCode());
			cs.setString(++paramIndex, tran.getTranSubStatCode());
			cs.setTimestamp(++paramIndex, sqlStatDate);
			cs.setInt(++paramIndex, tran.getFormFreeConfNbr());
			cs.setString(++paramIndex, tran.getCsrPrcsUserid());
			cs.setTimestamp(++paramIndex, now); // system date
			cs.setFloat(++paramIndex, tran.getAchFileCntlSeqNbr());
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			if (actionConfId != null) {
				cs.setString(++paramIndex, actionConfId);
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			if (postAmount != null) {
				cs.setFloat(++paramIndex, postAmount.floatValue());
			} else {
				cs.setNull(++paramIndex, Types.FLOAT);
			}

			if (StringHelper.isNullOrEmpty(tran.getRcvAgentRefNbr())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, tran.getRcvAgentRefNbr());
			}

			if (tran.getSndFeeAmt() != null) {
				cs.setFloat(++paramIndex, tran.getSndFeeAmt().floatValue());
			} else {
				cs.setNull(++paramIndex, Types.FLOAT);
			}

			if (tran.getSndTotAmt() != null) {
				cs.setFloat(++paramIndex, tran.getSndTotAmt().floatValue());
			} else {
				cs.setNull(++paramIndex, Types.FLOAT);
			}

			if (tran.getSndFxCnsmrRate() != null) {
				cs.setFloat(++paramIndex, tran.getSndFxCnsmrRate().floatValue());
			} else {
				cs.setNull(++paramIndex, Types.FLOAT);
			}
			cs.setString(++paramIndex, tran.getInternetPurchase());
			cs.setString(++paramIndex, tran.getTranRvwPrcsCode());
			if (subReasonCode == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, subReasonCode);
			if (tran.getFundRetrievalRequestDate() != null) {
				Timestamp ts = new Timestamp(tran.getFundRetrievalRequestDate()
						.getTime());
				ts.setTime(tran.getFundRetrievalRequestDate().getTime());
				cs.setTimestamp(++paramIndex, ts);
			} else
				cs.setNull(++paramIndex, Types.DATE);

			if (tran.getRRN() != null)
				cs.setString(++paramIndex, tran.getRRN());
			else
				cs.setNull(++paramIndex, Types.VARCHAR);
			// output parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			if (tran.getMccPartnerProfileId() != null)
				cs.setString(++paramIndex, tran.getMccPartnerProfileId());
			else
				cs.setNull(++paramIndex, Types.VARCHAR);

			// new GB collect parameters
			if (tran.getTCProviderCode() != null
					&& tran.getTCProviderCode() == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT) {
				System.out.println("Transaction Provider :"
						+ tran.getTCProviderCode());
				cs.setInt(++paramIndex, tran.getTCProviderCode());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, tran.getTCInternalTransactionNumber());
			cs.setLong(++paramIndex, tran.getTranSeqNumber());
			if (tran.getThreeMinuteFreePhoneNumber() != null
					&& !"".equals(tran.getThreeMinuteFreePhoneNumber())) {
				cs.setString(++paramIndex, tran.getThreeMinuteFreePhoneNumber());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}
			if (tran.getThreeMinuteFreePinNumber() != null
					&& !"".equals(tran.getThreeMinuteFreePinNumber())) {
				cs.setString(++paramIndex, tran.getThreeMinuteFreePinNumber());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			if(tran.getTranAvailabilityDate()!=null){
				cs.setDate(++paramIndex, new java.sql.Date(tran.getTranAvailabilityDate().getTime()));
			} else {
				cs.setNull(++paramIndex, Types.DATE);
			}

			IPDetails ipDetails = new IPDetails();
			ipDetails = tran.getIpDetails();
			if (ipDetails != null) {
				if (ipDetails.getConsumerLatitude() != null) {
					cs.setInt(++paramIndex, Integer.parseInt(ipDetails
							.getConsumerLatitude().toString()));
				} else {
					cs.setInt(++paramIndex, 0);
				}
				if (ipDetails.getConsumerLongitude() != null) {
					cs.setInt(++paramIndex, Integer.parseInt(ipDetails
							.getConsumerLongitude().toString()));
				} else {
					cs.setInt(++paramIndex, 0);
				}

				cs.setString(++paramIndex, ipDetails.getConsumerZipCode());
				cs.setString(++paramIndex, ipDetails.getConsumerCity());
				cs.setString(++paramIndex, ipDetails.getConsumerRegion());
				cs.setString(++paramIndex, ipDetails.getConsumerCountry());
				cs.setString(++paramIndex, ipDetails.getConsumerISP());
				cs.setString(++paramIndex, ipDetails.getConsumerDomain());
			} else {

				cs.setInt(++paramIndex, 0);
				cs.setInt(++paramIndex, 0);

				cs.setString(++paramIndex, "");
				cs.setString(++paramIndex, "");
				cs.setString(++paramIndex, "");
				cs.setString(++paramIndex, "");
				cs.setString(++paramIndex, "");
				cs.setString(++paramIndex, "");
			}
			
			// MGO-5000
			cs.setString(++paramIndex, tran.getReceiptTextInfoEng());
			cs.setString(++paramIndex, tran.getReceiptTextInfoSpa());
			//end MGO-5000

			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			int dbLogIdIndex = paramIndex;

			int rowsAffected = cs.executeUpdate();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			if (rowsAffected == 0) {
				logEndTime(dbLogId, conn, 0);
				throw new EMGRuntimeException("update transaction failed.");
			}
			safeCommit(conn);
			long count = rowsAffected;
			logEndTime(dbLogId, conn, count);
		} catch (Exception e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return;
	}

	private void safeCommit(java.sql.Connection c) throws SQLException {
		try {
			c.commit();
		} catch (SQLException e) {
			if (notPartOfGlobalTransaction(e)) {
				throw e;
			}
		}
	}

	private void safeRollback(java.sql.Connection c) throws SQLException {
		try {
			c.rollback();
		} catch (SQLException e) {
			System.err.println("Rollback was not possible: " + e.getMessage());
			if (notPartOfGlobalTransaction(e)) {
				throw e;
			}
		}
	}

	private boolean notPartOfGlobalTransaction(Exception e) {
		try {
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			return out.toString().toString()
					.indexOf("during a global transaction") == -1;
		} catch (Exception e2) {
			return false;
		}
	}

	public Collection getTransactionCommentReasons()
			throws DataSourceException, SQLException {
		Collection reasons = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_emg_tran_cmnt_reas_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.CHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranCommentReason reason = new TranCommentReason();
				reason.setEmgTranCmntReasCode(rs
						.getString("EMG_TRAN_CMNT_REAS_CODE"));
				reason.setCmntReasDesc(rs.getString("CMNT_REAS_DESC"));
				reasons.add(reason);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return reasons;
	}

	public Collection getTransactionComments(int tranId)
			throws DataSourceException, SQLException {
		Collection comments = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_comments.prc_get_emg_tran_comment_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, tranId);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.CHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranComment comment = new TranComment();
				comment.setTranCmntReasCode(rs
						.getString("EMG_TRAN_CMNT_REAS_CODE"));
				comment.setCmntReasDesc(rs.getString("CMNT_REAS_DESC"));
				DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a",
						true);
				comment.setCreateUserId(rs.getString("CREATE_USERID"));
				comment.setCreateDate(df.format(rs.getTimestamp("CREATE_DATE")));
				comment.setCmntText(rs.getString("CMNT_TEXT"));
				comments.add(comment);
			}
			logEndTime(dbLogId, conn, count);

		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		Collections.sort((List) comments);
		return comments;
	}

	// private List getTransactionCmt(java.sql.Connection conn, int tranId)
	// throws Exception
	// {
	// List comments = new ArrayList();
	// ResultSet rs = null;
	// CallableStatement cs = null;
	//
	//
	// String storedProcName = "pkg_em_comments.prc_get_emg_tran_comment_cv";
	// StringBuffer sqlBuffer = new StringBuffer(64);
	// sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");
	//
	// try
	// {
	// cs = conn.prepareCall(sqlBuffer.toString());
	//
	// int paramIndex = 0;
	// // input parameters
	// cs.setNull(++paramIndex, Types.VARCHAR);
	// cs.setNull(++paramIndex, Types.VARCHAR);
	// cs.setInt(++paramIndex, tranId);
	// cs.setNull(++paramIndex, Types.INTEGER);
	// cs.setNull(++paramIndex, Types.CHAR);
	//
	// // output parameters
	// cs.registerOutParameter(++paramIndex, Types.INTEGER);
	// int dbLogIdIndex = paramIndex;
	// cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
	// int rsIndex = paramIndex;
	//
	// cs.execute();
	//
	// long dbLogId = cs.getLong(dbLogIdIndex);
	// EMGSharedLogger.getLogger(
	// this.getClass().getName().toString()).diagnostic(
	// "dbLogId = " + dbLogId);
	//
	// rs = (ResultSet) cs.getObject(rsIndex);
	//
	// while (rs.next())
	// {
	// TranComment comment = new TranComment();
	// comment.setTranCmntReasCode(
	// rs.getString("EMG_TRAN_CMNT_REAS_CODE"));
	// comment.setCmntReasDesc(rs.getString("CMNT_REAS_DESC"));
	// DateFormatter df =
	// new DateFormatter("MM/dd/yyyy hh:mm a", true);
	// comment.setCreateUserId(rs.getString("CREATE_USERID"));
	// comment.setCreateDate(
	// df.format(rs.getTimestamp("CREATE_DATE")));
	// comment.setCmntText(rs.getString("CMNT_TEXT"));
	// comments.add(comment);
	// }
	// } finally
	// {
	// OracleAccess.close(rs);
	// OracleAccess.close(cs);
	// }
	//
	// Collections.sort((List) comments);
	// return comments;
	// }

	public int setTransactionComment(int tranId, String reasonCode,
			String comment, String userId) {
		Integer cmntId = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_comments.prc_insrt_emg_tran_comment";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, tranId); // transaction id
			cs.setString(++paramIndex, reasonCode);
			// transaction comment reason code
			cs.setString(++paramIndex, comment);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			cmntId = (Integer) cs.getObject(rsIndex);
			safeCommit(conn);
			logEndTime(dbLogId, conn);
		} catch (Exception e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new EMGRuntimeException(e);

		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return cmntId.intValue();
	}

	public Collection getTranStatusCodes() throws DataSourceException,
			SQLException {
		Collection tranStatuses = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_emg_tran_status_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranStatus status = new TranStatus();
				status.setTranStatCode(rs.getString("TRAN_STAT_CODE"));
				status.setTranStatDesc(rs.getString("TRAN_STAT_DESC"));
				tranStatuses.add(status);
			}
			logEndTime(dbLogId, conn, count);

		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return tranStatuses;
	}

	public Collection getTranSubStatusCodes() throws DataSourceException,
			SQLException {
		Collection subStatuses = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_tran_sub_status_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranSubStatus subStatus = new TranSubStatus();
				subStatus
						.setTranSubStatCode(rs.getString("TRAN_SUB_STAT_CODE"));
				subStatus
						.setTranSubStatDesc(rs.getString("TRAN_SUB_STAT_DESC"));
				subStatuses.add(subStatus);
			}
			logEndTime(dbLogId, conn, count);

		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return subStatuses;
	}

	public Collection<TranType> getTranTypes(int partnerSiteCode)
			throws DataSourceException, SQLException {
		Collection<TranType> tranTypes = new ArrayList<TranType>();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_emg_tran_type_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			if (partnerSiteCode != -1) {
				cs.setInt(++paramIndex, partnerSiteCode);
			} else {
				cs.setNull(++paramIndex, Types.INTEGER);
			}

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranType type = new TranType();
				type.setEmgTranTypeCode(rs.getString("EMG_TRAN_TYPE_CODE"));
				type.setEmgTranTypeDesc(rs.getString("EMG_TRAN_TYPE_DESC"));
				type.setScoreFlag(rs.getString("SCORE_FLAG"));
				type.setAutoApproveFlag(rs.getString("AUTO_APRV_FLAG"));
				type.setPartnerSiteId(rs.getString("BSNS_CODE"));
				tranTypes.add(type);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return tranTypes;
	}

	/*
	 * Status value must be "A" (active), "I" (inactive), "B" (both)
	 */
	public Collection<PartnerSite> getPartnerSiteIds(String status)
			throws DataSourceException, SQLException {
		Collection<PartnerSite> partnerSites = new ArrayList<PartnerSite>();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_src_web_site_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			if (StringHelper.isNullOrEmpty(status))
				throw new DataSourceException(this.getClass().getName()
						+ ".getPartnerSiteIds status parm must be A, B, or I");
			else if (!((status.equals("A")) || (status.equals("B")) || (status
					.equals("I"))))
				throw new DataSourceException(this.getClass().getName()
						+ ".getPartnerSiteIds status parm must be A, B, or I");

			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());
			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setString(++paramIndex, status);
			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				PartnerSite ps = new PartnerSite();
				ps.setPartnetSideIdCode(rs.getString("SRC_WEB_SITE_CODE"));
				ps.setPartnerSiteId(rs.getString("BSNS_CODE"));
				ps.setPartnerSiteDesc(rs.getString("BSNS_DESC"));
				ps.setPartnerSiteNarrDesc(rs.getString("NARR_DESC"));
				ps.setPartnerSiteDipslayOrder(rs.getString("DISPL_ORD_NBR"));
				partnerSites.add(ps);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return partnerSites;
	}

	public Collection getProcessUsers() throws DataSourceException,
			SQLException {
		Collection prcsUsers = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_emg_tran_prcs_users_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				prcsUsers.add(rs.getString("csr_prcs_userid"));
			}
			logEndTime(dbLogId, conn, count);

		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return prcsUsers;
	}

	public Collection getAccountTypes() throws DataSourceException,
			SQLException {
		Collection accountTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_cust_account_type_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				AccountType type = new AccountType();
				type.setAcctTypeCode(rs.getString("ACCT_TYPE_CODE"));
				type.setAcctTypeDesc(rs.getString("ACCT_TYPE_DESC"));
				accountTypes.add(type);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return accountTypes;
	}

	// queries for all transactions for a consumerid, will return a "shallow" or
	// partially
	// populated transaction. Done for background processing effciency, only
	// return relevant data
	// for it's needs.
	public List getTransactionsForVelocityCheck(Integer consumerId,
			String callerLoginId) throws Exception {
		ResultSet rsTransaction = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List transactions = new ArrayList();
		if (consumerId == null) {
			throw new Exception("Invalid Consumer ID");
		}
		try {
			conn = OracleAccess.getConnection();
			int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}
			String storedProcName = "pkg_em_transactions.prc_get_emg_trans_velocity_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setInt(++paramIndex, consumerId.intValue());
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}
			rsTransaction = (ResultSet) cs.getObject(paramTransactionIndex);
			long cnt = 0;
			while (rsTransaction.next()) {
				if (cnt++ > maxDownloadTransactions) {
					break;
				}
				String statusCode = rsTransaction.getString("TRAN_STAT_CODE");
				String subStatusCode = rsTransaction
						.getString("TRAN_SUB_STAT_CODE");
				TransactionStatus status = TransactionStatus.getInstance(
						statusCode, subStatusCode);
				Transaction transaction = new Transaction(status);
				transaction.setCreateDate(rsTransaction
						.getTimestamp("CREATE_DATE"));
				int tranId = rsTransaction.getInt("EMG_TRAN_ID");
				transaction.setEmgTranId(tranId);
				transaction.setCustId(rsTransaction.getInt("SND_CUST_ID"));
				transaction.setEmgTranTypeCode(rsTransaction
						.getString("EMG_TRAN_TYPE_CODE"));
				transaction.setTranStatDate(rsTransaction
						.getTimestamp("TRAN_STAT_DATE"));
				transactions.add(transaction);
			}
			logEndTime(dbLogId, conn, cnt);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rsTransaction);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return transactions;
	}

	public List getTransactions(TransactionSearchRequest searchCriteria,
			String callerLoginId) throws DataSourceException,
			TooManyResultException {
		ResultSet rsTransaction = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List transactions = new ArrayList();

		if (searchCriteria == null) {
			searchCriteria = new TransactionSearchRequest();
		}

		try {
			conn = OracleAccess.getConnection();
			int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();
			// MAS - need replacement techique for this. This no longer works
			// since we don't have an OracleConnection anymore
			// conn.setDefaultRowPrefetch(100);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_transactions.prc_get_emg_transactions_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?,?,?," + " ?,?,?,?,?,?,?,?,?,?,"
					+ " ?,?,?,?,?,?,?,?,?,?," + " ?,?,?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);

			Integer consumerId = searchCriteria.getConsumerId();
			if (consumerId == null) {
				cs.setNull(++paramIndex, Types.INTEGER);
			} else {
				cs.setInt(++paramIndex, consumerId.intValue());
			}

			String custLogonId = searchCriteria.getCustLogonId();
			if (custLogonId == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, custLogonId);
			}

			String ssnMaskNbr = searchCriteria.getSsnMaskNbr();
			if (ssnMaskNbr == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, ssnMaskNbr);
			}

			String accountTypeCode = searchCriteria.getAccountTypeCode();
			if (accountTypeCode == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, accountTypeCode);
			}

			String primaryAccountMaskNbr = searchCriteria
					.getPrimaryAccountMaskNbr();
			if (primaryAccountMaskNbr == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, primaryAccountMaskNbr);
			}

			String secondaryAccountMaskNbr = searchCriteria
					.getSecondaryAccountMaskNbr();
			if (secondaryAccountMaskNbr == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, secondaryAccountMaskNbr);
			}

			String bankAbaNbr = searchCriteria.getBankAbaNbr();
			if (bankAbaNbr == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, bankAbaNbr);
			}

			Integer tranId = searchCriteria.getTranId();
			if (tranId == null) {
				cs.setNull(++paramIndex, Types.INTEGER);
			} else {
				cs.setInt(++paramIndex, tranId.intValue());
			}

			String sndCustAcctId = searchCriteria.getSndCustAcctId();
			if (sndCustAcctId == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, sndCustAcctId);
			}

			cs.setNull(++paramIndex, Types.VARCHAR);

			String beginDate = searchCriteria.getBeginDate();
			if (beginDate == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, beginDate);
			}

			String endDate = searchCriteria.getEndDate();
			if (endDate == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, endDate);
			}

			Double minimumAmount = searchCriteria.getMinimumAmount();
			if (minimumAmount == null) {
				cs.setNull(++paramIndex, Types.DOUBLE);
			} else {
				cs.setDouble(++paramIndex, minimumAmount.doubleValue());
			}

			Double maximumAmount = searchCriteria.getMaximumAmount();
			if (maximumAmount == null) {
				cs.setNull(++paramIndex, Types.DOUBLE);
			} else {
				cs.setDouble(++paramIndex, maximumAmount.doubleValue());
			}

			String tranStatCode = searchCriteria.getTranStatCode();
			if (tranStatCode == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, tranStatCode);
			}

			String tranSubStatCode = searchCriteria.getTranSubStatCode();
			if (tranSubStatCode == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, tranSubStatCode);
			}

			String tranType = searchCriteria.getTranType();
			if (tranType == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, tranType);
			}

			String tranDate = searchCriteria.getTranDate();
			if (tranDate == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, tranDate);
			}

			String destinationCountry = searchCriteria.getDestinationCountry();
			if (destinationCountry == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, destinationCountry);
			}

			String receiverLastName = searchCriteria.getReceiverLastName();
			if (receiverLastName == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, receiverLastName);
			}

			String ipAddress = searchCriteria.getIpAddress();
			if (ipAddress == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, ipAddress);
			}

			String rcvAgcyCode = searchCriteria.getRcvAgcyCode();
			if (rcvAgcyCode == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, rcvAgcyCode);
			}

			String createBeginDate = searchCriteria.getCreateBeginDate();
			if (createBeginDate == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, createBeginDate);
			}

			String createEndDate = searchCriteria.getCreateEndDate();
			if (createEndDate == null) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, createEndDate);
			}

			if ("Y".equalsIgnoreCase(searchCriteria.getUseScore())) {
				cs.setInt(++paramIndex, searchCriteria.getLowScore());
				cs.setInt(++paramIndex, searchCriteria.getHighScore());
			} else {
				cs.setNull(++paramIndex, Types.INTEGER);
				cs.setNull(++paramIndex, Types.INTEGER);
			}

			if (StringHelper.isNullOrEmpty(searchCriteria.getSysAutoRsltCode())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, searchCriteria.getSysAutoRsltCode());
			}
			cs.setNull(++paramIndex, Types.VARCHAR); // Inet_Purch_Flag

			if (StringHelper.isNullOrEmpty(searchCriteria.getTranRvwPrcsCode())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, searchCriteria.getTranRvwPrcsCode()); // prcs
																					// ind
			}

			if (StringHelper.isNullOrEmpty(searchCriteria
					.getRetrievalRequestCode())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex,
						searchCriteria.getRetrievalRequestCode()); // retrieval
																	// request
																	// code
			}

			if (StringHelper.isNullOrEmpty(searchCriteria.getReferenceNumber())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, searchCriteria.getReferenceNumber()); // reference
																					// #
			}

			if (StringHelper.isNullOrEmpty(searchCriteria.getPartnerSiteId())) {
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setString(++paramIndex, searchCriteria.getPartnerSiteId()); // partner
																				// site
																				// identifier
			}

			if (!StringHelper.isNullOrEmpty(searchCriteria.getIdType())
					&& !"All".equals(searchCriteria.getIdType())) {
				cs.setString(++paramIndex, searchCriteria.getIdType());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			if (!StringHelper.isNullOrEmpty(searchCriteria.getFullIdNumber())) {
				cs.setString(++paramIndex, searchCriteria.getFullIdNumber());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			if (!StringHelper.isNullOrEmpty(searchCriteria.getLast4DigitsId())) {
				cs.setString(++paramIndex, searchCriteria.getLast4DigitsId());
			} else {
				cs.setNull(++paramIndex, Types.VARCHAR);
			}

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rsTransaction = (ResultSet) cs.getObject(paramTransactionIndex);
			long cnt = 0;

			while (rsTransaction.next()) {
				if (cnt++ > maxDownloadTransactions) {
					// throw new TooManyResultException("Too many result");
					break;
				}
				Transaction tran = createTransactionBeanForSearch(
						rsTransaction, true);
				transactions.add(tran);
			}
			logEndTime(dbLogId, conn, cnt);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rsTransaction);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return transactions;
	}

	public Collection getJournalEntries(int tranId, String beginDate,
			String endDate) throws DataSourceException, SQLException,
			TooManyResultException {
		Collection journalEntries = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_transactions.prc_get_emg_tran_actions_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			if (tranId != -1) {
				cs.setInt(++paramIndex, tranId);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
			} else {
				cs.setNull(++paramIndex, Types.INTEGER);
				cs.setString(++paramIndex, beginDate);
				cs.setString(++paramIndex, endDate);
			}

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);

			long cnt = 0;
			while (rs.next()) {
				journalEntries.add(createJournalEntryBean(rs));
				if (cnt++ > maxDownloadTransactions) {
					logEndTime(dbLogId, conn, cnt);
					throw new TooManyResultException(
							"Too many results and maximum download Entires is "
									+ maxDownloadTransactions);
				}
			}
			logEndTime(dbLogId, conn, cnt);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return journalEntries;
	}

	private JournalEntry createJournalEntryBean(ResultSet rs)
			throws SQLException {
		JournalEntry journalEntry = new JournalEntry();
		journalEntry.setTranId(rs.getInt("EMG_TRAN_ID"));
		journalEntry.setEntryDate(rs.getTimestamp("tran_actn_date"));
		journalEntry.setUserId(rs.getString("snd_cust_logon_id"));
		journalEntry.setTranReasonDescription(rs.getString("tran_reas_desc"));
		journalEntry.setTranActionId(rs.getInt("emg_tran_actn_id"));
		journalEntry.setTranStatCode(rs.getString("tran_stat_code"));
		journalEntry.setTranSubStatCode(rs.getString("tran_sub_stat_code"));
		journalEntry.setOldTranStatCode(rs.getString("tran_old_stat_code"));
		journalEntry.setOldTranSubStatCode(rs
				.getString("tran_old_sub_stat_code"));
		journalEntry.setSenderFirstName(rs.getString("snd_cust_frst_name"));
		journalEntry.setSenderLastName(rs.getString("snd_cust_last_name"));
		journalEntry.setPostAmount(rs.getDouble("post_amt"));
		journalEntry.setDebitAccountId(rs.getInt("dr_emg_gl_acct_id"));
		journalEntry.setCreditAccountId(rs.getInt("cr_emg_gl_acct_id"));
		journalEntry.setDebitAccountDesc(rs.getString("dr_emg_gl_acct_desc"));
		journalEntry.setCreditAccountDesc(rs.getString("cr_emg_gl_acct_desc"));
		journalEntry.setSubReasonCode(rs.getString("tran_sub_reas_code"));
		journalEntry.setTranSubReasDesc(rs.getString("tran_sub_reas_desc"));
		journalEntry.setTranReasTypeCode(rs.getString("tran_reas_type_code"));
		return journalEntry;
	}

	public String getConsumerEmail(int custId) throws DataSourceException,
			SQLException {
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String emailAddress = null;

		String storedProcName = "pkg_em_customer_profile.prc_get_cust_elec_addr_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, custId);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setNull(++paramIndex, Types.CHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(paramIndex);

			if (rs.next()) {
				emailAddress = rs.getString("cust_elec_addr_id");
			}
			logEndTime(dbLogId, conn, 1);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return emailAddress;
	}

	public String getCCAuthConfId(int tranId, String reasonCode)
			throws DataSourceException, SQLException {
		if (StringHelper.isNullOrEmpty(reasonCode)) {
			throw new EMGRuntimeException(
					"Passing parameter reasonCode is invalid");
		}

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String confId = null;

		String storedProcName = "pkg_em_transactions.prc_get_emg_tran_actions_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {

			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setInt(++paramIndex, tranId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(paramIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				String reasonId = rs.getString("tran_reas_code");
				if (!StringHelper.isNullOrEmpty(reasonId)) {
					if (reasonId.trim().equals(reasonCode)) {
						confId = rs.getString("tran_conf_id");
						break;
					}
				}
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return confId == null ? "" : confId;
	}

	public List getTranActions(int tranId, String logonId)
			throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List tranActions = new ArrayList();

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_transactions.prc_get_emg_tran_actions_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, logonId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranAction ta = new TranAction();
				ta.setTranActionId(rs.getInt("emg_tran_actn_id"));
				ta.setTranId(rs.getInt("emg_tran_id"));
				ta.setTranReasCode(rs.getString("tran_reas_code"));
				ta.setTranActionDate(rs.getTimestamp("tran_actn_date"));
				ta.setTranConfId(rs.getString("tran_conf_id"));
				ta.setTranPostAmt(rs.getBigDecimal("post_amt"));
				ta.setTranStatCode(rs.getString("tran_stat_code"));
				ta.setTranSubStatCode(rs.getString("tran_sub_stat_code"));
				ta.setTranOldStatCode(rs.getString("tran_old_stat_code"));
				ta.setTranOldSubStatCode(rs.getString("tran_old_sub_stat_code"));
				ta.setTranReasDesc(rs.getString("tran_reas_desc"));
				ta.setDrGLAcctId(rs.getInt("dr_emg_gl_acct_id"));
				ta.setCrGLAcctId(rs.getInt("cr_emg_gl_acct_id"));
				ta.setDrGLAcctDesc(rs.getString("dr_emg_gl_acct_desc"));
				ta.setCrGLAcctDesc(rs.getString("cr_emg_gl_acct_desc"));
				ta.setTranCustLastName(rs.getString("snd_cust_last_name"));
				ta.setTranCustFrstName(rs.getString("snd_cust_frst_name"));
				ta.setTranCustLogonId(rs.getString("snd_cust_logon_id"));
				ta.setTranCreateUserid(rs.getString("create_userid"));
				ta.setSubReasonCode(rs.getString("tran_sub_reas_code"));
				ta.setTranSubReasDesc(rs.getString("tran_sub_reas_desc"));
				ta.setTranReasTypeCode(rs.getString("tran_reas_type_code"));
				tranActions.add(ta);
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return tranActions;
	}

	public int[] getQueueCounts() throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		int[] cnt = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_transactions.prc_get_tran_queue_count_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			if (rs.next()) {
				count++;
				cnt[0] = rs.getInt("approval_p2p_count");
				cnt[1] = rs.getInt("approval_ep_count");
				cnt[2] = rs.getInt("process_p2p_count");
				cnt[3] = rs.getInt("process_ep_count");
				cnt[4] = rs.getInt("in_progress_count");
				cnt[5] = rs.getInt("exception_queue_count");
				cnt[6] = rs.getInt("approval_es_count");
				cnt[7] = rs.getInt("process_es_count");
				cnt[8] = rs.getInt("send_es_count");
				cnt[9] = rs.getInt("approval_ds_count");
				cnt[10] = rs.getInt("process_ds_count");
				cnt[11] = rs.getInt("send_ds_count");
				cnt[12] = rs.getInt("process_doc_count");
				cnt[13] = rs.getInt("process_doc_pen_count");
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return cnt;
	}

	public List getIpHistorySummaryList(String searchLogonId, String logonId)
			throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List ipSummaryList = new ArrayList();

		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_login_access.prc_get_logon_try_counts_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, logonId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setString(++paramIndex, searchLogonId);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				IPHistorySummary ihs = new IPHistorySummary();
				ihs.setIpAddress(rs.getString("logon_ip_addr_id").trim());
				ihs.setLoginCount(rs.getInt("logon_count"));
				ihs.setFirstLoginDate(df.format(rs
						.getTimestamp("min_create_date")));
				ihs.setLastLoginDate(df.format(rs
						.getTimestamp("max_create_date")));
				ipSummaryList.add(ihs);
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return ipSummaryList;
	}

	public List getIpHistoryDetailList(String searchLogonId, String logonId)
			throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List ipDetailList = new ArrayList();
		DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		try {
			conn = OracleAccess.getConnection();
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_login_access.prc_get_logon_try_attempts_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, logonId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setString(++paramIndex, searchLogonId);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				IPHistoryDetail ihd = new IPHistoryDetail();
				ihd.setIpAddress(rs.getString("logon_ip_addr_id").trim());
				ihd.setWebServerName(rs.getString("web_srvr_name"));
				ihd.setLoginDate(df.format(rs.getTimestamp("create_date")));
				ihd.setResult(rs.getString("logon_attempts"));
				ihd.setPartnerSiteId(rs.getString("SRC_WEB_SITE_BSNS_CD"));
				ihd.setTainted(rs.getString("logon_failure"));
				ipDetailList.add(ihd);
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return ipDetailList;
	}

	public List getLogonIpConsumerList(String ipAddress, String logonId)
			throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List ipConsumerList = new ArrayList();

		DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);

		try {
			conn = OracleAccess.getConnection();
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_login_access.prc_get_logon_try_cust_info_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, logonId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);
			cs.setString(++paramIndex, ipAddress);
			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				IpConsumer ic = new IpConsumer();
				ic.setLogonId(rs.getString("logon_id"));
				ic.setStatCode(rs.getString("cust_stat_code"));
				ic.setSubStatCode(rs.getString("cust_sub_stat_code"));
				ic.setFrstName(rs.getString("cust_frst_name"));
				ic.setLastName(rs.getString("cust_last_name"));
				ic.setPhoneNbr(rs.getString("cust_ph_nbr"));
				ic.setBrthDate(df.format(rs.getDate("cust_brth_date")));
				ic.setBeginDate(df.format(rs.getDate("prfl_eff_date")));
				ic.setEndDate(df.format(rs.getDate("prfl_thru_date")));
				ic.setCustBlockedCode(rs.getString("CUST_BLKD_CODE"));
				ic.setCustId(String.valueOf(rs.getInt("CUST_ID")));
				ipConsumerList.add(ic);
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return ipConsumerList;
	}

	public CustAddress getCustAddress(int addressId) throws DataSourceException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		CustAddress addr = null;

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_customer_profile.prc_get_cust_address_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.INTEGER);
			cs.setInt(++paramIndex, addressId);
			cs.setNull(++paramIndex, Types.CHAR);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramAddressIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());

				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramAddressIndex);
			long count = 0;
			if (rs.next()) {
				count++;
				addr = new CustAddress();
				addr.setAddrLine1Text(rs.getString("addr_line1_text"));
				addr.setAddrLine2Text(rs.getString("addr_line2_text"));
				addr.setAddrLine3Text(rs.getString("addr_line3_text"));
				addr.setAddrCityName(rs.getString("addr_city_name"));
				addr.setAddrStateName(rs.getString("addr_state_name"));
				addr.setAddrPostalCode(rs.getString("addr_postal_code"));
				addr.setAddrCntryId(rs.getString("addr_cntry_id"));
				addr.setAddrStatCode(rs.getString("addr_stat_code"));
				addr.setAddrStatDesc(rs.getString("addr_stat_desc"));
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return addr;
	}

	public List<GlAccount> getGLAccounts(String userId)
			throws DataSourceException, SQLException {

		List<GlAccount> glAccounts = new ArrayList<GlAccount>();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_gl_account_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?, ?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				GlAccount glAccount = new GlAccount();
				glAccount.setGlAccountId(rs.getInt("emg_gl_acct_id"));
				glAccount.setGlAccountDesc(rs.getString("acct_type_desc"));
				glAccounts.add(glAccount);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return glAccounts;
	}

	public void insertGLAdjustment(TranAction glAdjustment, int agentId,
			String callerLoginId) throws DataSourceException {
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_insert_gl_adjustment";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, glAdjustment.getTranId());
			cs.setInt(++paramIndex, glAdjustment.getDrGLAcctId());
			cs.setInt(++paramIndex, glAdjustment.getCrGLAcctId());
			cs.setString(++paramIndex, "N");
			cs.setBigDecimal(++paramIndex, glAdjustment.getTranPostAmt());
			cs.setInt(++paramIndex, agentId);
			cs.setString(++paramIndex, glAdjustment.getTranReasDesc());
			cs.registerOutParameter(++paramIndex, Types.BIGINT);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	public List getVBVBeans(int tranId) {
		List vvbList = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_tran_authentication_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs.setNull(++parm, Types.VARCHAR);
			cs.setNull(++parm, Types.VARCHAR);
			cs.setInt(++parm, tranId);

			// output parameters
			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(parm);

			while (rs.next()) {
				VBVViewBean vvb = new VBVViewBean();
				vvb.setTranId(rs.getInt("EMG_TRAN_ID"));
				vvb.setCsTranId(rs.getString("PAYER_AUTHN_RQST_TRAN_ID"));
				vvb.setAuthCmrcCode(rs.getString("AUTHN_CMRC_CODE"));
				vvb.setEciRawCode(rs.getString("ECI_RAW_CODE"));
				vvb.setVbvTranId(rs.getString("VBV_CAVV_TRAN_ID"));
				vvb.setProofXmlText(rs.getString("PRF_XML_TEXT"));
				vvb.setCsRqstId(rs.getString("CYBERSOURCE_RQST_ID"));
				DateFormatter df = new DateFormatter("dd/MMM/yyyy", true);
				vvb.setCreateDate(df.format(rs.getDate("CREATE_DATE")));
				vvbList.add(vvb);
			}

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return vvbList;
	}

	public void updateTransactionType(String userId, EMTTransactionType tranType)
			throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			// update Transaction Type
			String storedProcName = "pkg_emg_type_stat_maint.prc_iu_emg_transaction_type";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, tranType.getEmgTranTypeCode());
			cs.setString(++paramIndex, tranType.getEmgTranTypeDesc());
			cs.setString(++paramIndex, tranType.getScoreFlag());
			cs.setString(++paramIndex, tranType.getAutoApproveFlag());
			int parentSiteCode = EMoneyGramAdmApplicationConstants.partnerSiteIdToCode
					.get(tranType.getPartnerSiteId());
			cs.setInt(++paramIndex, parentSiteCode);
			cs.execute();

			// update Cache table
			storedProcName = "pkg_emg_type_stat_maint.prc_iu_cache_mgmt";
			cs = conn.prepareCall("{ call " + storedProcName + "(?) }");

			paramIndex = 0;

			// input parameters
			cs.setString(++paramIndex, "TransactionScore");
			cs.execute();

			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	private TransactionScore getTranScoreDetail(java.sql.Connection conn,
			int tranId) {
		TransactionScore ts = null;
		ResultSet rs1 = null;
		CallableStatement cs1 = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_tran_score_dtl_cv";

		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?,?,?) }");

		try {
			cs1 = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;
			cs1.setNull(++parm, Types.VARCHAR);
			cs1.setNull(++parm, Types.VARCHAR);
			cs1.setInt(++parm, tranId);
			cs1.setNull(++parm, Types.INTEGER);
			cs1.setNull(++parm, Types.INTEGER);

			// output parameters
			cs1.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs1.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs1.execute();

			long dbLogId = cs1.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs1 = (ResultSet) cs1.getObject(parm);
			List tranScoreCat = new ArrayList();
			int totalWeight = 0;

			while (rs1.next()) {
				if (ts == null) {
					ts = new TransactionScore();
					ts.setEmgTranId(rs1.getInt("EMG_TRAN_ID"));
					ts.setScoreCnfgId(rs1.getInt("SCORE_CNFG_ID"));
					ts.setScoreCnfgVerNbr(rs1.getInt("SCORE_CNFG_VER_NBR"));
					ts.setScoreCnfgStatCode(rs1
							.getString("SCORE_CNFG_STAT_CODE"));
					ts.setTranScoreNbr(rs1.getInt("TRAN_SCORE_NBR"));
					ts.setSysAutoRsltCode(rs1.getString("SYS_AUTO_RSLT_CODE"));
					ts.setScoreRvwCode(rs1.getString("SCORE_RVW_CODE"));
					ts.setTranScoreCmntText(rs1
							.getString("TRAN_SCORE_CMNT_TEXT"));
					ts.setCreateDate(df1.format(rs1.getTimestamp("CREATE_DATE")));
					ts.setCreateUserid(rs1.getString("CREATE_USERID"));
					ts.setLastUpdateDate(df1.format(rs1
							.getTimestamp("LAST_UPDATE_DATE")));
					ts.setLastUpdateUserid(rs1.getString("LAST_UPDATE_USERID"));
				}

				TransactionScoreCategory tsc = new TransactionScoreCategory();
				tsc.setEmgTranId(rs1.getInt("EMG_TRAN_ID"));
				tsc.setScoreCnfgId(rs1.getInt("SCORE_CNFG_ID"));
				tsc.setScoreCatCode(rs1.getString("SCORE_CAT_CODE"));
				tsc.setScoreCatName(rs1.getString("SCORE_CAT_NAME"));
				tsc.setScoreCatDesc(rs1.getString("SCORE_CAT_DESC"));
				tsc.setListTypeCode(rs1.getString("LIST_TYPE_CODE"));
				tsc.setDataTypeCode(rs1.getString("DATA_TYPE_CODE"));
				tsc.setInclFlag(rs1.getString("INCL_FLAG"));
				tsc.setScoreQueryRsltData(rs1
						.getString("SCORE_QUERY_RSLT_DATA"));
				tsc.setCatWgtNbr(rs1.getInt("CAT_WGT_NBR"));
				tsc.setRawScoreNbr(rs1.getInt("RAW_SCORE_NBR"));
				tsc.setWgtScoreNbr(rs1.getInt("WGT_SCORE_NBR"));
				tranScoreCat.add(tsc);
				if ("Y".equalsIgnoreCase(tsc.getInclFlag())) {
					totalWeight += tsc.getCatWgtNbr();
				}
			}

			if (ts != null) {
				ts.setTotalWeight(totalWeight);
				Iterator iter = tranScoreCat.iterator();
				while (iter.hasNext()) {
					TransactionScoreCategory tsc = (TransactionScoreCategory) iter
							.next();
					if ("Y".equalsIgnoreCase(tsc.getInclFlag())) {
						tsc.setWgtPct(nf.format(Math.round(tsc.getCatWgtNbr()
								* 10000F / totalWeight) / 100F)
								+ " %");
					} else {
						tsc.setWgtPct("n/a");
					}
				}
				ts.setTranScoreCatList(tranScoreCat);
			}

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs1);
			OracleAccess.close(cs1);
		}
		return ts;
	}

	public boolean isScoringConfigurationEditable(String userId,
			int scoreConfigId) throws DataSourceException {

		boolean editable = false;
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		String storedProcName = "pkg_em_status_and_type.prc_get_tran_score_flg";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;

			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, scoreConfigId);
			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int outIndex = paramIndex;
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);
			long returnInt = cs.getLong(outIndex);
			if (returnInt > 0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public boolean canTakeoverFromSysOwner(Transaction tran) {
		String prefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		String user = tran.getCsrPrcsUserid();
		user = user == null ? "" : user;
		if (user.length() <= prefix.length()) {
			return true;
		}

		if (!prefix.equalsIgnoreCase(user.substring(0, prefix.length()))) {
			return true;
		}

		return System.currentTimeMillis() - tran.getCsrPrcsDate().getTime()
				- (long) EMTAdmAppProperties.getWaitTimeForTakingOver() * 60L
				* 1000L > 0;
	}

	public boolean checkASPTran(Transaction tran, String userId) {
		String prefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		// String user = userId == null ? "" : userId;
		if (!tran
				.getStatus()
				.getStatusCode()
				.equalsIgnoreCase(
						TransactionStatus.AUTOMATIC_SCORING_PROCESS_CODE)) {
			return true;
		}
		return (userId.length() > prefix.length())
				&& (prefix
						.equalsIgnoreCase(userId.substring(0, prefix.length())));
	}

	public Transaction getTransactionForAutoProcess(String userId,
			String tranStatCode, String tranSubStatCode, String csrUserId,
			Date createDateTime, String partnerSiteId, String tranTypeCode)
			throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		Transaction transaction = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_get_working_queue_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?, ?,?,?, ?,?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.setString(++paramIndex, tranStatCode);
			cs.setString(++paramIndex, tranSubStatCode);
			cs.setString(++paramIndex, csrUserId);

			if (createDateTime != null) {
				cs.setTimestamp(++paramIndex,
						new Timestamp(createDateTime.getTime()));
			} else {
				cs.setNull(++paramIndex, OracleTypes.TIMESTAMP);
			}
			if (partnerSiteId != null) {
				cs.setString(++paramIndex, partnerSiteId);
			} else {
				cs.setNull(++paramIndex, OracleTypes.VARCHAR);
			}
			if (tranTypeCode != null) {
				cs.setString(++paramIndex, tranTypeCode);
			} else {
				cs.setNull(++paramIndex, OracleTypes.VARCHAR);
			}

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			// int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramCursorIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(paramCursorIndex);

			if (rs.next()) {
				transaction = new Transaction();
				transaction.setEmgTranId(rs.getInt("EMG_TRAN_ID"));
				transaction.setCsrPrcsUserid(rs.getString("csr_prcs_userid"));
				transaction.setCsrPrcsDate(rs.getDate("csr_prcs_date"));
			}

		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return transaction;

	}

	public Collection getDeadTransactions() {
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		Collection col = new ArrayList();
		;
		DeadTransaction deadTransaction = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_get_dead_transactions_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, "system");
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			// int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramCursorIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(paramCursorIndex);

			if (rs.next()) {
				deadTransaction = new DeadTransaction();
				deadTransaction.setTranId(rs.getInt("EMG_TRAN_ID"));
				deadTransaction.setCustId(rs.getInt("SND_CUST_ID"));
				deadTransaction.setEmailAddr(rs.getString("CUST_ELEC_ADDR_ID"));
				deadTransaction.setTranDate(rs.getTimestamp("SND_TRAN_DATE"));
				col.add(deadTransaction);
			}

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return col;
	}

	public int[] getLimboASPTransactions() {
		int[] tranList = new int[0];
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		List list = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_get_asp_limbo_trans_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, "system");
			cs.setString(++paramIndex, dbCallTypeCode);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			// int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramCursorIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(paramCursorIndex);

			list = new ArrayList();
			if (rs.next()) {
				Integer tranId = new Integer(rs.getInt("EMG_TRAN_ID"));
				list.add(tranId);
			}

			if (list.size() != 0) {
				tranList = new int[list.size()];
				Iterator iter = list.iterator();
				int i = 0;
				while (iter.hasNext()) {
					tranList[i++] = ((Integer) iter.next()).intValue();
				}
			}

		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return tranList;
	}

	private Date[] getLoginDates(java.sql.Connection conn, int custId,
			String userId) throws DataSourceException {
		Date[] dates = { null, null };
		ResultSet rs = null;
		CallableStatement cs = null;

		try {
			String storedProcName = "pkg_em_login_access.prc_get_first_last_login_cv";
			cs = conn
					.prepareCall("{ call " + storedProcName + "(?,?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			// int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramCursorIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(paramCursorIndex);

			if (rs.next()) {
				dates[0] = rs.getTimestamp("first_login");
				dates[1] = rs.getTimestamp("last_login");
			}

		} catch (Exception e) {
			throw new DataSourceException(e);
		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
			}
			try {
				cs.close();
			} catch (Exception e2) {
			}
		}

		return dates;
	}

	private Object[] getRecentReceivers(java.sql.Connection conn, int custId,
			String userId) throws DataSourceException {
		Object[] objs = { null, null, null };
		List tranList = new ArrayList();
		float total = 0F;
		List receiverList = new ArrayList();
		List rcvTranList = new ArrayList();
		String frstName = "";
		String lastName = "";

		ResultSet rs = null;
		CallableStatement cs = null;

		try {
			String storedProcName = "pkg_em_transactions.prc_get_recent_rcv_tran_csr_cv";
			cs = conn
					.prepareCall("{ call " + storedProcName + "(?,?,?, ?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, custId);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			// int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramCursorIndex = paramIndex;

			cs.execute();
			rs = (ResultSet) cs.getObject(paramCursorIndex);

			while (rs.next()) {
				String tmpFrst = rs.getString("rcv_cust_frst_name");
				tmpFrst = tmpFrst == null ? "" : tmpFrst;
				String tmpLast = rs.getString("rcv_cust_last_name");
				tmpLast = tmpLast == null ? "" : tmpLast;
				if (!StringHelper.isNullOrEmpty(frstName)
						&& (!tmpFrst.equals(frstName) || !tmpLast
								.equals(lastName))) {
					receiverList.add(new RecentTranReceiver(frstName, lastName,
							rcvTranList));
					frstName = tmpFrst;
					lastName = tmpLast;
					rcvTranList = new ArrayList();
				}
				frstName = tmpFrst;
				lastName = tmpLast;
				Transaction tran = getTransaction(rs.getInt("emg_tran_id"));
				tranList.add(tran);
				total += tran.getSndFaceAmt().floatValue();
				rcvTranList.add(tran);
			}

			if (!StringHelper.isNullOrEmpty(frstName)) {
				receiverList.add(new RecentTranReceiver(frstName, lastName,
						rcvTranList));
			}

			objs[0] = tranList;
			objs[1] = new Float(Math.round(total * 100F) / 100);
			objs[2] = receiverList;

		} catch (Exception e) {
			throw new DataSourceException(e);
		} finally {
			try {
				rs.close();
			} catch (Exception e1) {
			}
			try {
				cs.close();
			} catch (Exception e2) {
			}
		}

		return objs;
	}

	private Object[] getConsumerTranLists(java.sql.Connection conn, int custId,
			String userId) throws DataSourceException {
		final String den = TransactionStatus.DENIED_CODE;
		final String sen = TransactionStatus.SENT_CODE;
		final String cxl = TransactionStatus.CANCELED_CODE;
		final String acs = TransactionStatus.ACH_SENT_CODE;
		final String ccs = TransactionStatus.CC_SALE_CODE;

		Object[] objs = new Object[6];

		List totalList = new ArrayList();
		List denyList = new ArrayList();
		List sendList = new ArrayList();
		List cancelList = new ArrayList();
		List fundedList = new ArrayList();
		float total = 0F;

		TransactionSearchRequest tsr = new TransactionSearchRequest();
		tsr.setConsumerId(new Integer(custId));
		List tranList;
		try {
			tranList = getTransactionsWithCmnts(tsr, userId);
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
		Iterator iter = tranList.iterator();
		while (iter.hasNext()) {
			Transaction tran = (Transaction) iter.next();
			total += tran.getSndFaceAmt().floatValue();

			totalList.add(tran);

			if (den.equals(tran.getTranStatCode())) {
				denyList.add(tran);
			}

			if (sen.equals(tran.getTranStatCode())) {
				sendList.add(tran);
			}

			if (cxl.equals(tran.getTranStatCode())) {
				cancelList.add(tran);
			}

			if (acs.equals(tran.getTranSubStatCode())
					|| ccs.equals(tran.getTranSubStatCode())) {
				fundedList.add(tran);
			}
		}

		objs[0] = totalList;
		objs[1] = denyList;
		objs[2] = sendList;
		objs[3] = cancelList;
		objs[4] = fundedList;
		objs[5] = totalList.size() != 0 ? new Float(Math.round(total
				/ totalList.size() * 100F) / 100) : new Float(0F);

		return objs;
	}

	// private MicroDeposit setMDBean(int acctId) {
	// MicroDepositService mds = ServiceFactory.getInstance()
	// .getMicroDepositService();
	// MicroDeposit md = new MicroDeposit();
	// md.setCustAccountId(acctId);
	// md.setCustAccountVersionNbr(1);
	// Iterator iter;
	// try {
	// iter = mds.getMicroDepositValidations(md).iterator();
	// } catch (DataSourceException e) {
	// throw new EMGRuntimeException(e);
	// }
	// md = null;
	// while (iter.hasNext()) {
	// md = (MicroDeposit) iter.next();
	// }
	// return md;
	// }
	//
	// private ConsumerBankAccountView[] createBankAccountViews(List
	// bankAccounts) {
	// ConsumerBankAccountView[] views = new
	// ConsumerBankAccountView[bankAccounts == null ? 0
	// : bankAccounts.size()];
	// if (bankAccounts != null) {
	// int i = 0;
	// for (Iterator iter = bankAccounts.iterator(); iter.hasNext(); ++i) {
	// ConsumerBankAccount account = (ConsumerBankAccount) iter.next();
	// views[i] = new ConsumerBankAccountView(account);
	// views[i].setMicroDeposit(setMDBean(account.getId()));
	// }
	// }
	// return views;
	// }
	//
	// private ConsumerCreditCardAccountView[] createCreditCardViews(
	// List creditCardAccounts) {
	// ConsumerCreditCardAccountView[] views = new
	// ConsumerCreditCardAccountView[creditCardAccounts == null ? 0
	// : creditCardAccounts.size()];
	// if (creditCardAccounts != null) {
	// int i = 0;
	// for (Iterator iter = creditCardAccounts.iterator(); iter.hasNext(); ++i)
	// {
	// ConsumerCreditCardAccount account = (ConsumerCreditCardAccount) iter
	// .next();
	// views[i] = new ConsumerCreditCardAccountView(account);
	// }
	// }
	// return views;
	// }

	public ConsumerProfileDashboard getConsumerProfileDashboard(
			ConsumerProfile consumerProfile, String userId) {
		java.sql.Connection conn = null;
		try {
			conn = OracleAccess.getConnection();

			int custId = consumerProfile.getId();
			// get first & last login dates
			Date[] dates = getLoginDates(conn, consumerProfile.getId(), userId);

			ConsumerProfileDashboard cpd = ConsumerProfileDashboardFactory
					.create(consumerProfile.getPartnerSiteId());
			cpd.setFirstLoginDate(dates[0]);
			cpd.setLastLoginDate(dates[1]);

			// get last 30 days good transaction list, total amount, receivers
			Object[] objs = getRecentReceivers(conn, custId, userId);
			cpd.setRecentTranList((List) objs[0]);
			cpd.setRecentAmt(((Float) objs[1]).floatValue());
			cpd.setReceiverList((List) objs[2]);

			// get five lists of transactions for this consumer
			Object[] objs1 = getConsumerTranLists(conn, custId, userId);
			cpd.setTotalTranList((List) objs1[0]);
			cpd.setDenyTranList((List) objs1[1]);
			cpd.setSendTranList((List) objs1[2]);
			cpd.setCancelTranList((List) objs1[3]);
			cpd.setFundedTranList((List) objs1[4]);
			cpd.setTranAvgAmt(((Float) objs1[5]).floatValue());
			return cpd;
		} catch (DataSourceException e) {
			throw new EMGRuntimeException(e);
		} finally {
			OracleAccess.close(conn);
		}
	}

	public List getTranReasons(String userId, String tranStatCode,
			String tranSubStatCode) throws DataSourceException {
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List tranReasonList = new ArrayList();

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_status_and_type.prc_get_tran_reason_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?, ?,?, ?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);

			cs.setString(++paramIndex, tranStatCode);
			cs.setString(++paramIndex, tranSubStatCode);

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}
			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				String str1 = rs.getString("tran_stat_code") + "/"
						+ rs.getString("tran_sub_stat_code");
				String str2 = str1 + " - " + rs.getString("tran_reas_desc");
				LabelValueBean lvb = new LabelValueBean(str2, str1);
				tranReasonList.add(lvb);
			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return tranReasonList;
	}

	public List getTransactionsWithCmnts(
			TransactionSearchRequest searchCriteria, String callerLoginId)
			throws DataSourceException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;
		List transactions = new ArrayList();

		if (searchCriteria == null) {
			searchCriteria = new TransactionSearchRequest();
		}

		try {
			conn = OracleAccess.getConnection();

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDebugEnabled()) {
				try {
					AbstractOracleDAO.enableSQLDebugging(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"unable to enable sql debugging", ignore);
				}
			}

			String storedProcName = "pkg_em_transactions.prc_get_emg_trans_comments_cv";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?, ?,?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, callerLoginId);
			cs.setString(++paramIndex, AbstractOracleDAO.dbCallTypeCode);

			cs.setInt(++paramIndex, searchCriteria.getConsumerId().intValue());
			cs.setInt(++paramIndex, dynProps.getMaxConsumerTransShown());

			cs.registerOutParameter(++paramIndex, OracleTypes.INTEGER);
			int dbLogIdIndex = paramIndex;

			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int paramTransactionIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " started: "
										+ System.currentTimeMillis());
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName + " ended: "
										+ System.currentTimeMillis());
				EMGSharedLogger.getLogger(this.getClass().getName().toString())
						.diagnostic("dbLogId = " + dbLogId);
			}

			rs = (ResultSet) cs.getObject(paramTransactionIndex);

			int saveTranId = -1;
			int tranId = -999;
			List cmntsList = null;
			Transaction tran = null;
			long count = 0;
			while (rs.next()) {
				count++;
				tranId = rs.getInt("emg_tran_id");

				// new transaction but not the first one
				if (saveTranId != -1 && saveTranId != tranId) {
					if (cmntsList.size() != 0) {
						Collections.sort(cmntsList);
					}
					tran.setCmntList(cmntsList.size() == 0 ? null : cmntsList);
					transactions.add(tran);
				}

				// new transaction
				if (saveTranId != tranId) {
					saveTranId = tranId;
					cmntsList = new ArrayList();
					tran = createPartialTransactionBean(conn, rs);
				}

				// create comment if there is one
				if (!StringHelper.isNullOrEmpty(rs.getString("CMNT_TEXT"))) {
					TranComment comment = new TranComment();
					comment.setTranCmntReasCode(rs
							.getString("EMG_TRAN_CMNT_REAS_CODE"));
					comment.setCmntReasDesc(rs.getString("CMNT_REAS_DESC"));
					DateFormatter df = new DateFormatter("dd/MMM/yyyy hh:mm a",
							true);
					comment.setCreateUserId(rs.getString("CMNT_CREATE_USERID"));
					comment.setCreateDate(df.format(rs
							.getTimestamp("CMNT_CREATE_DATE")));
					comment.setDate(new Date(rs
							.getTimestamp("CMNT_CREATE_DATE").getTime()));
					comment.setCmntText(rs.getString("CMNT_TEXT"));
					cmntsList.add(comment);
				}
			}
			logEndTime(dbLogId, conn, count);
			if (saveTranId != -1) {
				tran.setCmntList(cmntsList.size() == 0 ? null : cmntsList);
				transactions.add(tran);
			}
		} catch (SQLException e) {
			throw new DataSourceException(e);
		} finally {
			if (conn != null) {
				try {
					AbstractOracleDAO.logSQLDebug(conn);
				} catch (SQLException ignore) {
					EMGSharedLogger.getLogger(
							this.getClass().getName().toString()).warn(
							"Exception during sql debug logging", ignore);
				}
			}
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return transactions;
	}

	private Transaction createPartialTransactionBean(java.sql.Connection conn,
			ResultSet rs) throws SQLException {

		int tranId = rs.getInt("EMG_TRAN_ID");
		String statusCode = rs.getString("TRAN_STAT_CODE");
		String subStatusCode = rs.getString("TRAN_SUB_STAT_CODE");
		TransactionStatus status = TransactionStatus.getInstance(statusCode,
				subStatusCode);

		Transaction transaction = new Transaction(status);
		transaction.setEmgTranId(tranId);
		transaction.setEmgTranTypeCode(rs.getString("EMG_TRAN_TYPE_CODE"));
		transaction.setEmgTranTypeDesc(rs.getString("EMG_TRAN_TYPE_DESC"));
		transaction.setCustId(rs.getInt("SND_CUST_ID"));
		transaction.setTranStatDesc(rs.getString("TRAN_STAT_DESC"));
		transaction.setTranSubStatDesc(rs.getString("TRAN_SUB_STAT_DESC"));
		transaction.setTranStatDate(rs.getTimestamp("TRAN_STAT_DATE"));
		transaction.setSndFaceAmt(rs.getBigDecimal("SND_FACE_AMT"));
		transaction.setRcvAgcyCode(rs.getString("RCV_AGCY_CODE"));
		transaction.setRcvISOCntryCode(rs.getString("RCV_ISO_CNTRY_CODE"));
		transaction.setRcvCustFrstName(rs.getString("RCV_CUST_FRST_NAME"));
		// transaction.setRcvCustMidName(rs.getString("RCV_CUST_MID_NAME"));
		transaction.setRcvCustLastName(rs.getString("RCV_CUST_LAST_NAME"));
		transaction.setCsrPrcsUserid(rs.getString("CSR_PRCS_USERID"));
		transaction.setCreateDate(rs.getTimestamp("CREATE_DATE"));
		transaction.setPartnerSiteId(rs.getString("BSNS_CODE"));
		TransactionScore ts = getTranScoreDetail(conn, tranId);
		if (ts != null) {
			transaction.setTransScores(ts.getTranScoreNbr());
			transaction.setSysAutoRsltCode(ts.getSysAutoRsltCode());
		}
		transaction.setTranScore(ts);

		return transaction;
	}

	public Collection getChargebackStatuses() throws DataSourceException,
			SQLException {
		Collection chargebackStatuses = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_emg_chgbk_status_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackStatus status = new ChargebackStatus();
				status.setChargebackStatusCode(rs.getString("CHGBK_STAT_CODE"));
				status.setChargebackStatusDesc(rs.getString("CHGBK_STAT_DESC"));
				chargebackStatuses.add(status);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackStatuses;
	}

	public Collection getGenderTypes() throws DataSourceException, SQLException {
		Collection genderTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_gender_type_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				GenderType genderType = new GenderType();
				genderType.setGenderCode(rs.getString("GNDR_CODE"));
				genderType.setGenderDesc(rs.getString("GNDR_DESC"));
				genderTypes.add(genderType);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return genderTypes;
	}

	public Collection getCustomerContactMethodTypes()
			throws DataSourceException, SQLException {
		Collection customerContactMethodTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_cust_cntct_mthd_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				CustomerContactMethodType customerContactMethodType = new CustomerContactMethodType();
				customerContactMethodType.setCustomerContactMethodTypeCode(rs
						.getString("CUST_CNTCT_MTHD_CODE"));
				customerContactMethodType.setCustomerContactMethodTypeDesc(rs
						.getString("CUST_CNTCT_MTHD_DESC"));
				customerContactMethodTypes.add(customerContactMethodType);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return customerContactMethodTypes;
	}

	public ChargebackTransactionHeader getChargebackTransactionHeader(
			String userId, int tranId) throws DataSourceException, SQLException {
		ChargebackTransactionHeader chargebackTransactionHeader = null;
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_emg_chgbk_tran_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				chargebackTransactionHeader = new ChargebackTransactionHeader();
				chargebackTransactionHeader.setTranId(rs.getInt("EMG_TRAN_ID"));
				chargebackTransactionHeader.setChargebackStatusCode(rs
						.getString("CHGBK_STAT_CODE"));

				chargebackTransactionHeader.setChargebackStatusDate(rs
						.getTimestamp("CHGBK_STAT_DATE"));

				chargebackTransactionHeader.setChargebackTrackingDate(rs
						.getTimestamp("CHGBK_TRK_DATE"));

				chargebackTransactionHeader.setGenderCode(rs
						.getString("GNDR_CODE"));
				chargebackTransactionHeader.setCustomerContactMethodCode(rs
						.getString("CUST_CNTCT_MTHD_CODE"));
				chargebackTransactionHeader.setEmailConsistentFlag(rs
						.getString("EMAIL_CNSTNT_FLAG"));
				chargebackTransactionHeader.setPhoneNumberVerifiedFlag(rs
						.getString("PH_NBR_VERFD_FLAG"));
				chargebackTransactionHeader.setCustomerVerbalContactFlag(rs
						.getString("CUST_VRBL_CNTCT_FLAG"));
				chargebackTransactionHeader.setUnSolicitPhoneCallFlag(rs
						.getString("UNSOLICIT_PH_CALL_FLAG"));
				chargebackTransactionHeader.setCustomerProfileValidationFlag(rs
						.getString("CUST_PRFL_VLDN_FLAG"));
				chargebackTransactionHeader.setSenderReceiverDistance(rs
						.getInt("SND_RCV_MILE_QTY"));
				chargebackTransactionHeader.setLinkedCustomerProfiles(rs
						.getInt("LINKED_CUST_PRFL_CNT"));
				chargebackTransactionHeader.setCreateDate(rs
						.getTimestamp("CREATE_DATE"));
				chargebackTransactionHeader.setUpdateDate(rs
						.getTimestamp("LAST_UPDATE_DATE"));
				chargebackTransactionHeader.setCreateUserId(rs
						.getString("CREATE_USERID"));
				chargebackTransactionHeader.setUpdateUserId(rs
						.getString("LAST_UPDATE_USERID"));

			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackTransactionHeader;
	}

	public Collection getChargebackComments(String userId, int tranId)
			throws DataSourceException, SQLException {
		Collection chargebackComments = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_chgbk_tran_comment_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackComment chargebackComment = new ChargebackComment();
				chargebackComment.setTranId(rs.getString("EMG_TRAN_ID"));
				chargebackComment.setChargebackCommentId(rs
						.getString("CHGBK_TRAN_CMNT_ID"));
				chargebackComment.setCommentText(rs
						.getString("CHGBK_CMNT_TEXT"));
				chargebackComment.setCreateDate(rs.getString("CREATE_DATE"));
				chargebackComment
						.setCreateUserId(rs.getString("CREATE_USERID"));
				chargebackComments.add(chargebackComment);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackComments;
	}

	public Collection getChargebackActions(String userId, int tranId)
			throws DataSourceException, SQLException {
		Collection chargebackActions = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_chgbk_tran_action_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackAction chargebackAction = new ChargebackAction();
				chargebackAction.setTranId(rs.getString("EMG_TRAN_ID"));
				chargebackAction.setChargebackActionId(rs
						.getString("CHGBK_TRAN_ACTN_ID"));
				chargebackAction.setChargebackStatusCode(rs
						.getString("CHGBK_STAT_CODE"));
				chargebackAction.setChargebackStatusDesc(rs
						.getString("CHGBK_STAT_DESC"));
				chargebackAction.setCreateDate(rs.getString("CREATE_DATE"));
				chargebackAction.setCreateUserId(rs.getString("CREATE_USERID"));
				chargebackActions.add(chargebackAction);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackActions;
	}

	public Collection getChargebackProviderCase(String userId, int tranId)
			throws DataSourceException, SQLException {
		Collection chargebackProviderCases = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_transactions.prc_get_chgbk_prvdr_case_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackProviderCase chargebackProviderCase = new ChargebackProviderCase();
				chargebackProviderCase.setChgbkCaseId(rs
						.getInt("chgbk_case_id"));
				chargebackProviderCase.setTranId(rs.getInt("emg_tran_id"));
				chargebackProviderCase.setChargebackDenialCode(rs
						.getString("chgbk_denial_code"));
				chargebackProviderCase.setChargebackDenialDesc(rs
						.getString("chgbk_denial_desc"));
				chargebackProviderCase.setChargebackProviderCaseId(rs
						.getString("chgbk_prvdr_case_id"));
				chargebackProviderCase.setChargebackOriginalStatementDate(rs
						.getDate("chgbk_orgn_stmt_date"));
				chargebackProviderCase.setCreateDate(rs.getDate("create_date"));
				chargebackProviderCase.setCreateUserId(rs
						.getString("create_userid"));
				chargebackProviderCase.setUpdateDate(rs
						.getDate("last_update_date"));
				chargebackProviderCase.setUpdateUserId(rs
						.getString("last_update_userid"));
				chargebackProviderCases.add(chargebackProviderCase);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackProviderCases;
	}

	public void insertChargebackTranComment(String userId, int tranId,
			String comment) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_insert_emg_chgbk_tran_cmnt";
			cs = conn
					.prepareCall("{ call " + storedProcName + "(?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);
			cs.setString(++paramIndex, comment);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	public void insertChargebackProviderCase(String userId, int tranId,
			String denialCode, String providerCaseId,
			Date originatorStatementDate) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_insert_chgbk_prvdr_case";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);
			cs.setString(++paramIndex, denialCode);
			cs.setString(++paramIndex, providerCaseId);
			cs.setDate(++paramIndex,
					new java.sql.Date(originatorStatementDate.getTime()));

			cs.registerOutParameter(++paramIndex, Types.BIGINT);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	public Collection getChargebackDenialTypes() throws DataSourceException,
			SQLException {
		Collection chargebackDenialTypes = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_chgbk_denial_type_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackDenialType chargebackDenialType = new ChargebackDenialType();
				chargebackDenialType.setChargebackDenialTypeCode(rs
						.getString("CHGBK_DENIAL_CODE"));
				chargebackDenialType.setChargebackDenialTypeDesc(rs
						.getString("CHGBK_DENIAL_DESC"));
				chargebackDenialTypes.add(chargebackDenialType);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
		return chargebackDenialTypes;
	}

	public void insertUpdateChargebackTransaction(String userId, int tranId,
			String chargebackStatusCode, Date chargebackTrackingDate,
			String genderType, String customerContactMethodFlag,
			String emailConsistentFlag, String phoneVerifiedFlag,
			String customerVerbalContactFlag, String unsolicitPhoneCallFlag,
			String customerProfileValidationFlag, String sendReceiverDistance,
			String linkedCustomerProfiles) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_iu_emg_chgbk_tran";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?) }");
			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, tranId);
			cs.setString(++paramIndex, chargebackStatusCode);

			if (chargebackTrackingDate == null)
				cs.setNull(++paramIndex, Types.DATE);
			else
				cs.setDate(++paramIndex, new java.sql.Date(
						chargebackTrackingDate.getTime()));

			cs.setString(++paramIndex, genderType);

			if (customerContactMethodFlag == null)
				cs.setNull(++paramIndex, Types.VARCHAR);
			else
				cs.setString(++paramIndex, customerContactMethodFlag);

			cs.setString(++paramIndex, emailConsistentFlag);
			cs.setString(++paramIndex, phoneVerifiedFlag);
			cs.setString(++paramIndex, customerVerbalContactFlag);
			cs.setString(++paramIndex, unsolicitPhoneCallFlag);
			cs.setString(++paramIndex, customerProfileValidationFlag);

			if (StringHelper.isNullOrEmpty(sendReceiverDistance))
				cs.setNull(++paramIndex, Types.INTEGER);
			else
				cs.setInt(++paramIndex, Integer.parseInt(sendReceiverDistance));

			if (StringHelper.isNullOrEmpty(linkedCustomerProfiles))
				cs.setNull(++paramIndex, Types.INTEGER);
			else
				cs.setInt(++paramIndex,
						Integer.parseInt(linkedCustomerProfiles));

			cs.registerOutParameter(++paramIndex, Types.BIGINT);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	public void updateFileControl(int iFileControlSeqNbr, int iRecCnt,
			int iSuccessCount, int iFailCount) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_batch_seg.prc_insrt_updt_file_control";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");

			cs.setInt(1, iFileControlSeqNbr);
			cs.setDate(2, null);
			cs.setInt(3, iRecCnt);
			cs.setString(4, null);
			cs.setLong(5, iSuccessCount);
			cs.setLong(6, iFailCount);
			cs.setNull(7, Types.NUMERIC);
			cs.setString(8, null);
			cs.setString(9, null);
			cs.setString(10, null);
			cs.setString(11, null);
			cs.setString(12, null);
			cs.setString(13, null);
			cs.setDate(14, null);
			cs.setString(15, dbCallTypeCode);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_batch_seg.prc_insrt_updt_file_control" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public int insertFileControl(String userId, String processName)
			throws DataSourceException {
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		int fileControlNbr = 0;

		try {
			conn = OracleAccess.getConnection();
			String storedProcName = "pkg_batch_seg.prc_create_file_control";
			cs = conn
					.prepareCall("{ call " + storedProcName + "(?,?,?, ?,?) }");

			int paramIndex = 0;

			// input parameters

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, processName);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int fileControlNbrIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_batch_seg.prc_create_file_control" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

			fileControlNbr = ((Integer) cs.getObject(fileControlNbrIndex))
					.intValue();

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return fileControlNbr;
	}

	// This returns only the latest record for that type of process. If there
	// are no row to return it returs
	// a null object
	public Collection getLatestFileControl(String processName)
			throws DataSourceException {
		ArrayList fileControlRecs = new ArrayList();
		// DateFormatter df = new DateFormatter("MM-dd-yyyy HH:mm:ss", true);

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_batch_seg.prc_get_file_control_cv";
			cs = conn.prepareCall("{ call " + storedProcName + "(?,?,?,?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, processName);

			cs.registerOutParameter(++paramIndex, Types.BIGINT);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int newIdIndex = paramIndex;

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			long dbLogId = cs.getLong(dbLogIdIndex);
			rs = (ResultSet) cs.getObject(newIdIndex);
			long count = 0;
			while (rs.next()) {
				count++;

				FileControl fileControl = new FileControl();
				fileControl
						.setFile_cntl_seq_nbr(rs.getInt("file_cntl_seq_nbr"));
				fileControl.setReceive_date(rs.getDate("Receive_date"));
				fileControl.setRec_amt(rs.getInt("Rec_amt"));
				fileControl.setRec_cnt(rs.getInt("Rec_cnt"));
				fileControl.setProc_cnt_1(rs.getInt("proc_cnt_1"));
				fileControl.setProc_cnt_2(rs.getInt("proc_cnt_2"));
				fileControl.setProc_cnt_3(rs.getInt("proc_cnt_3"));
				fileControl.setProc_cnt_4(rs.getInt("proc_cnt_4"));
				fileControl.setProc_amt_1(rs.getInt("proc_amt_1"));
				fileControl.setProc_amt_2(rs.getInt("proc_amt_2"));
				fileControl.setProc_amt_3(rs.getInt("proc_amt_3"));
				fileControl.setProc_amt_4(rs.getInt("proc_amt_4"));
				fileControl.setProcess_name(rs.getString("process_name"));
				fileControl.setProcess_date(rs.getDate("process_date"));
				fileControl.setInsrt_event(rs.getString("insrt_event"));
				fileControl.setInsrt_date(rs.getDate("insrt_date"));
				fileControl.setUpdt_event(rs.getString("updt_event"));
				fileControl.setUpdt_date(rs.getDate("updt_date"));

				fileControlRecs.add(fileControl);

			}
			logEndTime(dbLogId, conn, count);
		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return fileControlRecs;
	}

	public void deletePhoneAreaCode() throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			int paramIndex = 0;

			String storedProcName = "pkg_emg_type_stat_maint.prc_del_phone_area_code";
			cs = conn.prepareCall("{ call " + storedProcName + "(?) }");

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_emg_type_stat_maint.prc_del_phone_area_code" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public void insertPhoneAreaCode(String userId, String countryCode,
			String areaCode, String stateName) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_emg_type_stat_maint.prc_iu_phone_area_code";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?, ?) }");

			int paramIndex = 0;

			// input parameters

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, countryCode);
			cs.setString(++paramIndex, areaCode);
			cs.setString(++paramIndex, stateName);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_emg_type_stat_maint.prc_iu_phone_area_code" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

	}

	public void insertEmployeeRecord(String userId, String empLastName,
			String empFirstName, String empMidName, String ssnMask,
			int fileCtrlNumber, String empStatCode) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_emg_type_stat_maint.prc_iu_mgi_employee";
			cs = conn.prepareCall("{ call " + storedProcName
					+ "(?,?,?,?,?,?,?,?, ?) }");

			int paramIndex = 0;

			// input parameters

			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setString(++paramIndex, empLastName);
			cs.setString(++paramIndex, empFirstName);
			cs.setString(++paramIndex, empMidName);
			cs.setString(++paramIndex, ssnMask);
			cs.setInt(++paramIndex, fileCtrlNumber);
			cs.setString(++paramIndex, empStatCode);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_emg_type_stat_maint.prc_iu_mgi_employee" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public void inactivateEmployeeList(String userId)
			throws DataSourceException {
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();
			int paramIndex = 0;

			String storedProcName = "pkg_emg_type_stat_maint.prc_inactivate_employee_list";
			cs = conn.prepareCall("{ call " + storedProcName + "(?, ?) }");
			cs.setString(++paramIndex, userId);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								"pkg_emg_type_stat_maint.prc_del_phone_area_code" + " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public void insertAchReturn(String userId, int fileControlSeqNbr,
			AchReturn achReturn) throws DataSourceException {

		CallableStatement cs = null;
		java.sql.Connection conn = null;

		try {
			conn = OracleAccess.getConnection();

			String storedProcName = "pkg_em_transactions.prc_insert_emg_ach_return_tran";
			cs = conn
					.prepareCall("{ call "
							+ storedProcName
							+ "(?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?,?,?, ?) }");

			int paramIndex = 0;
			cs.setString(++paramIndex, userId);
			cs.setString(++paramIndex, dbCallTypeCode);
			cs.setInt(++paramIndex, fileControlSeqNbr);
			cs.setString(++paramIndex, "INI");

			if (achReturn.getTranId() != null)
				cs.setInt(++paramIndex, achReturn.getTranId().intValue());
			else
				cs.setNull(++paramIndex, Types.INTEGER);

			if (achReturn.getMicroTranId() != null)
				cs.setInt(++paramIndex, achReturn.getMicroTranId().intValue());
			else
				cs.setNull(++paramIndex, Types.INTEGER);

			cs.setDate(++paramIndex, new java.sql.Date(achReturn.getAsOfDate()
					.getTime()));

			cs.setString(++paramIndex, achReturn.getFileId());
			cs.setString(++paramIndex, achReturn.getCompanyId());
			cs.setString(++paramIndex, achReturn.getSettlementBank());
			cs.setString(++paramIndex, achReturn.getSettlementAccount());

			cs.setString(++paramIndex, achReturn.getReturnTypeCode());
			cs.setString(++paramIndex, achReturn.getReturnTypeDesc());

			cs.setDouble(++paramIndex, achReturn.getCreditAmount());
			cs.setDouble(++paramIndex, achReturn.getDebitAmount());

			cs.setString(++paramIndex, achReturn.getIndividualId()); // Individual
																		// Id
			cs.setString(++paramIndex, achReturn.getIndividualName());

			cs.setDate(++paramIndex, new java.sql.Date(achReturn
					.getEffectiveDate().getTime()));

			cs.setString(++paramIndex, achReturn.getDescriptiveDate());

			cs.setString(++paramIndex, achReturn.getReceivingRDFI());
			cs.setString(++paramIndex, achReturn.getAccountNo());
			cs.setString(++paramIndex, achReturn.getReturnTraceNo());
			cs.setString(++paramIndex, achReturn.getAccountType());
			cs.setString(++paramIndex, achReturn.getReturnReasonCode());
			cs.setString(++paramIndex, achReturn.getReturnReasonDesc());
			cs.setString(++paramIndex, achReturn.getOriginalTraceNo());
			cs.setString(++paramIndex, achReturn.getSearchResult());

			cs.registerOutParameter(++paramIndex, Types.BIGINT);

			if (EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.isDiagnosticEnabled()) {
				EMGSharedLogger
						.getLogger(this.getClass().getName().toString())
						.diagnostic(
								storedProcName
										+ " started: " + System.currentTimeMillis()); //$NON-NLS-1$
			}

			cs.execute();
			safeCommit(conn);

		} catch (SQLException e) {
			try {
				safeRollback(conn);
			} catch (SQLException e1) {
			}
			throw new DataSourceException(e);
		} finally {
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}
	}

	public Collection getChargebackTrackingQueue(String userId,
			ChargebackTrackingSearchRequest searchRequest)
			throws DataSourceException, SQLException, TooManyResultException {

		Collection chargebackTransactions = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		int maxDownloadTransactions = dynProps.getMaxDownloadTransactions();

		String storedProcName = "pkg_em_transactions.prc_get_emg_chgbk_trans_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?, ?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;

			cs.setString(++parm, userId);
			cs.setString(++parm, dbCallTypeCode);

			if (StringHelper.isNullOrEmpty(searchRequest.getBeginDateText())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, searchRequest.getBeginDateText());
			}

			if (StringHelper.isNullOrEmpty(searchRequest.getEndDateText())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, searchRequest.getEndDateText());
			}

			if (StringHelper.isNullOrEmpty(searchRequest.getStatus())) {
				cs.setNull(++parm, Types.VARCHAR);
			} else {
				cs.setString(++parm, searchRequest.getStatus());
			}

			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			rs = (ResultSet) cs.getObject(parm);
			long dbLogId = cs.getLong(dbLogIdIndex);

			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			int cnt = 0;
			long count = 0;
			while (rs.next()) {
				count++;
				ChargebackTransactionHeader chargebackTransactionHeader = new ChargebackTransactionHeader();

				chargebackTransactionHeader.setTranId(rs.getInt("EMG_TRAN_ID"));
				chargebackTransactionHeader.setChargebackStatusCode(rs
						.getString("CHGBK_STAT_CODE"));
				chargebackTransactionHeader.setChargebackStatusDesc(rs
						.getString("CHGBK_STAT_DESC"));

				chargebackTransactionHeader.setChargebackStatusDate(rs
						.getTimestamp("CHGBK_STAT_DATE"));

				chargebackTransactionHeader.setChargebackTrackingDate(rs
						.getTimestamp("CHGBK_TRK_DATE"));

				chargebackTransactionHeader.setGenderCode(rs
						.getString("GNDR_CODE"));
				chargebackTransactionHeader.setCustomerContactMethodCode(rs
						.getString("CUST_CNTCT_MTHD_CODE"));
				chargebackTransactionHeader.setEmailConsistentFlag(rs
						.getString("EMAIL_CNSTNT_FLAG"));
				chargebackTransactionHeader.setPhoneNumberVerifiedFlag(rs
						.getString("PH_NBR_VERFD_FLAG"));
				chargebackTransactionHeader.setCustomerVerbalContactFlag(rs
						.getString("CUST_VRBL_CNTCT_FLAG"));
				chargebackTransactionHeader.setUnSolicitPhoneCallFlag(rs
						.getString("UNSOLICIT_PH_CALL_FLAG"));
				chargebackTransactionHeader.setCustomerProfileValidationFlag(rs
						.getString("CUST_PRFL_VLDN_FLAG"));
				chargebackTransactionHeader.setSenderReceiverDistance(rs
						.getInt("SND_RCV_MILE_QTY"));
				chargebackTransactionHeader.setLinkedCustomerProfiles(rs
						.getInt("LINKED_CUST_PRFL_CNT"));
				chargebackTransactionHeader.setCreateDate(rs
						.getTimestamp("CREATE_DATE"));
				chargebackTransactionHeader.setUpdateDate(rs
						.getTimestamp("LAST_UPDATE_DATE"));
				chargebackTransactionHeader.setCreateUserId(rs
						.getString("CREATE_USERID"));
				chargebackTransactionHeader.setUpdateUserId(rs
						.getString("LAST_UPDATE_USERID"));

				chargebackTransactions.add(chargebackTransactionHeader);

				if (cnt++ > maxDownloadTransactions) {
					logEndTime(dbLogId, conn, count);
					throw new TooManyResultException(
							"Too many results and maximum download transaction is "
									+ maxDownloadTransactions);
				}
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return chargebackTransactions;
	}

	public MicroDeposit getMicroDepTrans(String userId, int microValidationId)
			throws DataSourceException, SQLException {

		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		MicroDeposit md = new MicroDeposit();

		String storedProcName = "pkg_em_cust_account_maint.prc_get_micro_deposit_trans_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?, ?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			// input parameters
			int parm = 0;

			cs.setString(++parm, userId);
			cs.setString(++parm, dbCallTypeCode);
			cs.setInt(++parm, microValidationId);

			cs.registerOutParameter(++parm, Types.INTEGER);
			int dbLogIdIndex = parm;
			cs.registerOutParameter(++parm, OracleTypes.CURSOR);

			cs.execute();
			rs = (ResultSet) cs.getObject(parm);
			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			int cnt = 1;
			md.setValidationId(microValidationId);

			while (rs.next()) {
				if (cnt == 1)
					md.setDepositTranId1(rs.getInt("MIC_DEP_TRAN_ID"));
				if (cnt == 2)
					md.setDepositTranId2(rs.getInt("MIC_DEP_TRAN_ID"));
				cnt++;
			}
			logEndTime(dbLogId, conn, cnt);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return md;
	}

	public Collection getAllTranSubStatus() throws DataSourceException,
			SQLException {
		Collection subStatuses = new ArrayList();
		ResultSet rs = null;
		CallableStatement cs = null;
		java.sql.Connection conn = null;

		String storedProcName = "pkg_em_status_and_type.prc_get_all_tran_sub_status_cv";
		StringBuffer sqlBuffer = new StringBuffer(64);
		sqlBuffer.append("{ call " + storedProcName + "(?,?,?,?,?) }");

		try {
			conn = OracleAccess.getConnection();
			cs = conn.prepareCall(sqlBuffer.toString());

			int paramIndex = 0;
			// input parameters
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);
			cs.setNull(++paramIndex, Types.VARCHAR);

			// output parameters
			cs.registerOutParameter(++paramIndex, Types.INTEGER);
			int dbLogIdIndex = paramIndex;
			cs.registerOutParameter(++paramIndex, OracleTypes.CURSOR);
			int rsIndex = paramIndex;

			cs.execute();

			long dbLogId = cs.getLong(dbLogIdIndex);
			EMGSharedLogger.getLogger(this.getClass().getName().toString())
					.diagnostic("dbLogId = " + dbLogId);

			rs = (ResultSet) cs.getObject(rsIndex);
			long count = 0;
			while (rs.next()) {
				count++;
				TranSubStatus subStatus = new TranSubStatus();
				subStatus
						.setTranSubStatCode(rs.getString("TRAN_SUB_STAT_CODE"));
				subStatus
						.setTranSubStatDesc(rs.getString("TRAN_SUB_STAT_DESC"));
				subStatuses.add(subStatus);
			}
			logEndTime(dbLogId, conn, count);
		} finally {
			OracleAccess.close(rs);
			OracleAccess.close(cs);
			OracleAccess.close(conn);
		}

		return subStatuses;
	}

	private void logEndTime(long logId, java.sql.Connection conn) {
		try {
			logEndTime(logId, conn, -1);
		} catch (Exception ignoreIt) {
			System.out.println("logEndTime exception:" + ignoreIt.getMessage());
			ignoreIt.printStackTrace();
		}

	}

	private void logEndTime(long logId, java.sql.Connection conn, long count) {

		try {
			// if a logId is passed, then log the end of the procedure call
			if (logId != 0) {
				CallableStatement cs = null;
				String storedProcName = "pkg_proc_log.prc_iu_nvl_process_log_ui";
				cs = conn.prepareCall("{ call " + storedProcName
						+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
				int paramIndex = 0;
				cs.setLong(++paramIndex, logId);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setString(++paramIndex, "SYSDATE");
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				if (count == -1)
					cs.setNull(++paramIndex, Types.FLOAT);
				else
					cs.setLong(++paramIndex, count);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.FLOAT);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.setNull(++paramIndex, Types.VARCHAR);
				cs.execute();
			}
		} catch (Exception ignoreIt) {
			System.out.println("logEndTime exception:" + ignoreIt.getMessage());
			ignoreIt.printStackTrace();
		}

	}

	public Transaction setMerchantId(Transaction tran) {
		//if (StringHelper.isNullOrEmpty(tran.getMccPartnerProfileId())) {
			// if MCC profile is not set, figure it out now.
			if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
				BillerLimit billerLimit = new BillerLimit();
				try {
					billerLimit = emgshared.services.ServiceFactory.getInstance().getBillerLimitService().getBillerLimit(tran.getRcvAgcyCode());
				} catch (Exception e) {
					throw new IllegalArgumentException("Biller MCC code could not be retrieved for transaction = " + tran.getEmgTranId());
				}
				// change here to corresponding global collect merchantid
				if(billerLimit.getPaymentProfileId().matches("\\d+")) {
					tran.setMccPartnerProfileId(billerLimit.getPaymentProfileId());
				}
				else {
					tran.setMccPartnerProfileId(EMTSharedContainerProperties.convertToGlobalCollectMerchantId(billerLimit.getPaymentProfileId()));
				}
			} else if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.MONEY_TRANSFER_SEND_CODE)) {
				tran.setMccPartnerProfileId(EMTSharedContainerProperties.getMerchantIdGC(tran.getPartnerSiteId()));
			}
		//}
		return tran;
	}

}
