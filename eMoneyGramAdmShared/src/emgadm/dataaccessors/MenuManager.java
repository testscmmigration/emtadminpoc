package emgadm.dataaccessors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import emgadm.model.ApplicationCommand;
import emgadm.model.Role;
import emgadm.model.UserProfile;
import emgshared.exceptions.DataSourceException;

public class MenuManager
{
	private static HashMap<String, List<ApplicationCommand>> menuMap = new HashMap<String, List<ApplicationCommand>>();

	public static HashMap<String, List<ApplicationCommand>> retrieveMenu()
			throws DataSourceException {
		if (menuMap.isEmpty()) {
			//menuMap.put("Reports", null);
			for (ApplicationCommand command : CommandManager.getCommands()) {
				if (command.isMenuItem()) {
					addToMenu(command);
				}
			}
		}
		return menuMap;
	}

	private static void addToMenu(ApplicationCommand menuItem) {
		List<ApplicationCommand> menu = menuMap.get(menuItem.getMenuGroupName());
		if (menu == null) {
			menu = new ArrayList<ApplicationCommand>();
		}
		menu.add(menuItem);
		menuMap.put(menuItem.getMenuGroupName(), menu);
	}

	public static String getMenu(Role role)
			throws DataSourceException {
		StringBuffer buffer = new StringBuffer();
		HashMap<String, List<ApplicationCommand>> map = retrieveMenu();

		Set<String> names = map.keySet();
		int ctr = 0;
		boolean isMenuAdded = false;
		// a kluge to force "Reports" menu to be first.  It is first if running in WSAD env
		// but last if running in Resin??  so force it now.
		//gk		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Reports','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$
		//gk		ctr++;

		for (String name : names) {
			List<ApplicationCommand> commands = map.get(name);

			int intCtr = 0;
			int currentMainCtr = 0;
			for (ApplicationCommand command : commands) {
				if (role.contains(command)) {
					if (!isMenuAdded) {
						isMenuAdded = true;
						if (!name.equalsIgnoreCase("REPORTS")) {  //$NON-NLS-1$
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
						} else {
							currentMainCtr = 0;
						}
					}
					buffer.append("oCMenu.makeMenu('" + currentMainCtr + "sub" + intCtr + "','top" + currentMainCtr + "','" + command.getDisplayName() + "','/" + command.getId() + ".do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					intCtr++;
				}
			}
			ctr++;
			isMenuAdded = false;
		}
//		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Logoff','/logoff.do','') \n"); //$NON-NLS-1$//$NON-NLS-2$
//		buffer.append("oCMenu.construct()"); //$NON-NLS-1$
		return buffer.toString();
	}

	public static String getMenu(UserProfile user) throws DataSourceException {
		StringBuffer buffer = new StringBuffer("");
		HashMap<String, List<ApplicationCommand>> map = retrieveMenu();

		Set<String> names = map.keySet();
		int ctr = 0;
		boolean isMenuAdded = false;
		// a kluge to force "Reports" menu to be first.  It is first if running in WSAD env
		// but last if running in Resin??  so force it now.
		//gk		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Reports','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$
		//gk		ctr++;
		String [] menuNames=new String[5];
		
		for(String name : names){
			if(name.equalsIgnoreCase("System Admin")){
				menuNames[0]=name;
			}else if(name.equalsIgnoreCase("Reporting")){
				menuNames[1]=name;
			}else if(name.equalsIgnoreCase("User Admin")){
				menuNames[2]=name;
			}else if(name.equalsIgnoreCase("Dashboard")){
				menuNames[3]=name;
			}else if(name.equalsIgnoreCase("Consumer")){
				menuNames[4]=name;
			}
		}
		
		for (String name : menuNames) {
			List<ApplicationCommand> commands = map.get(name);

			int intCtr = 0;
			int currentMainCtr = 0;
			for (ApplicationCommand command : commands) {
				if (user.hasPermission(command.getId())) {
					if (!isMenuAdded) {
						isMenuAdded = true;
						if (name.equalsIgnoreCase("Dashboard")) {
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','/showTransQueue.do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
						}
						else if (!name.equalsIgnoreCase("REPORTS")) {  //$NON-NLS-1$
							buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;" + name + "','','') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							currentMainCtr = ctr;
						}
						else {
							currentMainCtr = 0;
						}
					}
					
					if (!name.equalsIgnoreCase("Dashboard")) {
						buffer.append("oCMenu.makeMenu('" + currentMainCtr + "sub" + intCtr + "','top" + currentMainCtr + "','" + command.getDisplayName() + "','/" + command.getId() + ".do') \n"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
					}
					intCtr++;
				}
			}
			ctr++;
			isMenuAdded = false;
		}
//		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Logoff','/logoff.do','') \n"); //$NON-NLS-1$//$NON-NLS-2$
//		buffer.append("oCMenu.construct()"); //$NON-NLS-1$
		return buffer.toString();
	}

	public static String getEndMenu()
			throws DataSourceException {
		StringBuffer buffer = new StringBuffer();
		int ctr = 99;
		buffer.append("oCMenu.makeMenu('top" + ctr + "','','&nbsp;Logoff','/logoff.do','') \n"); //$NON-NLS-1$//$NON-NLS-2$
		buffer.append("oCMenu.construct()"); //$NON-NLS-1$
		return buffer.toString();
	}

}
