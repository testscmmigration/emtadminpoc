/*
 * Created on Mar 17, 2004
 *
 */
package emgadm.dataaccessors;

/**
 * @author A131
 *
 */
public class ManagerFactory
{
	private ManagerFactory()
	{
	}

	public static UserProfileManager createUserManager()
	{
		return UserProfileManagerImplRegular.getInstance();
	}

	public static BillerMainOfficeManager createBillerMainOfficeManager()
	{
		return BillerMainOfficeManagerImplRegular.instance();
	}

	public static TransactionManager createTransactionManager()
	{
		return TransactionManagerImplRegular.instance();
	}

	public static ACHReturnManager createACHReturnManager()
	{
		return ACHReturnManagerImplRegular.instance();
	}

}
