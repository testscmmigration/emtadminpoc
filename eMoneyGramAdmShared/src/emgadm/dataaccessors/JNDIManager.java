package emgadm.dataaccessors;

import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import emgadm.constants.EMoneyGramAdmLDAPKeys;
import emgadm.exceptions.UserAuthenticationException;
import emgadm.property.EMTAdmContainerProperties;
import emgshared.exceptions.DataSourceException;

public class JNDIManager {
	private static final String LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory"; //$NON-NLS-1$

	private static final String AUTHENTICATION_SIMPLE = "simple"; //$NON-NLS-1$

	private static String internalRoleContext = EMTAdmContainerProperties.getLDAP_INTERNAL_ROLE_APPLICATION_CONTEXT();

	public static InitialDirContext getInitialContext(String context) throws DataSourceException {
//		System.out.println("Logging in to LDAP server: " + EMTAdmContainerProperties.getLDAP_SERVER()
//				+ ":" + EMTAdmContainerProperties.getLDAP_PORT() + "/" + context);
		Properties props = new Properties();
		props.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CONTEXT_FACTORY);
		props.put(Context.PROVIDER_URL, EMTAdmContainerProperties.getLDAP_SERVER()
				+ ":" + EMTAdmContainerProperties.getLDAP_PORT() + "/" + context); //$NON-NLS-1$//$NON-NLS-2$
		props.put(Context.SECURITY_AUTHENTICATION, AUTHENTICATION_SIMPLE);
		props.put(Context.SECURITY_CREDENTIALS, EMTAdmContainerProperties.getLDAP_PASSWORD());
		props.put(Context.SECURITY_PRINCIPAL, EMTAdmContainerProperties.getLDAP_USER_ID());
		InitialDirContext initialContext;
		try {
			initialContext = new InitialDirContext(props);
		} catch (NamingException e) {
			throw new DataSourceException(e.getMessage());
		}
		return initialContext;
	}

	public static InitialDirContext getUserDirContext(String userID, String password) throws DataSourceException,
			UserAuthenticationException {
//		System.out.println("Logging in to LDAP server: " + EMTAdmContainerProperties.getLDAP_SERVER() +
//			 	  ":" + EMTAdmContainerProperties.getLDAP_PORT() + "/cn=" + userID + "," + 
//			 	  EMTAdmContainerProperties.getLDAP_USER_CONTEXT());
		Properties props = new Properties();
		props.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_CONTEXT_FACTORY);
		props.put(Context.PROVIDER_URL,
				  EMTAdmContainerProperties.getLDAP_SERVER() +
			 	  ":" + EMTAdmContainerProperties.getLDAP_PORT() + "/cn=" + userID + "," + 
			 	  EMTAdmContainerProperties.getLDAP_USER_CONTEXT()); //$NON-NLS-1$//$NON-NLS-2$//$NON-NLS-3$
		props.put(Context.SECURITY_AUTHENTICATION, AUTHENTICATION_SIMPLE);
		props.put(Context.SECURITY_PROTOCOL, "ssl");
		props.put(Context.SECURITY_CREDENTIALS, password);
		props.put(Context.SECURITY_PRINCIPAL, EMoneyGramAdmLDAPKeys.CN + "=" + userID + "," + EMTAdmContainerProperties.getLDAP_USER_CONTEXT()); //$NON-NLS-1$//$NON-NLS-2$
		InitialDirContext initialContext;
		try {
			initialContext = new InitialDirContext(props);
		} catch (AuthenticationException e) {
			throw new UserAuthenticationException(e.getMessage());
		} catch (NamingException e) {
			throw new DataSourceException(e.getMessage());
		}
		return initialContext;
	}

	public static InitialDirContext getInternalRoleContext() throws DataSourceException {
		try {
			return getInitialContext(internalRoleContext);
		} catch (DataSourceException e) {
			// EMoneyGramAdmLogger.getLogger().severe("Could not find Internal
			// Role Context.", e); //$NON-NLS-1$
			System.out.println("Severe: Could not find Internal Role Context.");
			throw (e);
		}
	}

	public static void releaseDirContext(DirContext dirContext) {
		if (dirContext != null) {
			try {
				dirContext.close();
			} catch (NamingException e) {
				System.out.println("Error: Exception thrown while closing DirContext - '" + e.getMessage() + "'");
			}
		}
	}
}
