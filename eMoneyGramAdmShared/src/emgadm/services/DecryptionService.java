/*
 * Created on Feb 7, 2005
 *
 */
package emgadm.services;

/**
 * @author A131
 *
 */
public interface DecryptionService
{
	public String decryptSSN(String encryptedSsn);
	public String decryptCreditCardNumber(String encryptedCreditCardNumber);
	public String decryptCreditCardBinNumber(String encryptedCreditCardBinNumber);
	public String decryptBankAccountNumber(String encryptedBankAccountNumber);
	public String decryptAdditionalId(String encryptedAdditionalId);
}
