package emgadm.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.AgentConnectAccessorDoddFrank;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.AgentConnectServiceDoddFrank;
import shared.mgo.services.MGOAgentProfileService;
import shared.mgo.services.MGOServiceFactory;

import com.moneygram.acclient.MoneyGramSendRequest;
import com.moneygram.acclient.MoneyGramSendResponse;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionRequest;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionResponse;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.processing.UpdateTransactionCommand;
import emgadm.sysmon.EventListConstant;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.BillerLimit;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.services.CommentService;
import emgshared.services.CreditCardService;
import emgshared.services.CreditCardServiceResponse;
import emgshared.util.Constants;
import eventmon.eventmonitor.EventMonitor;

public class TransactionServiceCyberSource extends TransactionServiceBase {

	private static final TransactionService INSTANCE = new TransactionServiceCyberSource();

	public static TransactionService getInstance() {
		return INSTANCE;
	}

	private TransactionServiceCyberSource() {;}

	public void sendPersonToPerson(final int tranId, final String userId, final boolean useBackupAccount)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException, InvalidRRNException, AgentConnectException {
		final TransactionManager tranManager = ManagerFactory.createTransactionManager();
		final Transaction tran = tranManager.getTransaction(tranId);
		UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
			public Object execute() {
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				tranManager.updateTransaction(tran, userId, null, null);
				if (!tran.isSendable()) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a sendable state.");
				}
				CommitTransactionRequest reqDoddFrank=null;
				CommitTransactionResponse respDoddFrank=null;
				AgentConnectAccessorDoddFrank agentConnectAccessorDoddFrank=null;
				Date before = new Date(System.currentTimeMillis());
				try {
					MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
					String agtid = String.valueOf(tran.getSndAgentId());
					MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());
					try {
						String rrn = tran.getRRN();
						if (rrn != null && rrn.startsWith("MG")) {
							AgentConnectServiceDoddFrank agentConnectServiceDoddFrank= MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
							if (!agentConnectServiceDoddFrank.isRRNumberFound(mgoAgentProfile, rrn)) {
								throw new InvalidRRNException("RRN is invalid", rrn);
							}
						}
					} catch (Exception e0) {
						throw new AgentConnectException(e0);
					}
					long beginTime = System.currentTimeMillis();
					try {
						agentConnectAccessorDoddFrank=new AgentConnectAccessorDoddFrank(mgoAgentProfile);
						reqDoddFrank=buildCommitTransactionRequest(tran);
						try {
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...START...\n\n");
							respDoddFrank=agentConnectAccessorDoddFrank.commitTransaction(reqDoddFrank);
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...END...\n\n");
						} catch (Exception e) {
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...ERROR...\n\n");
							acknowledgementFailEmail(tranId, respDoddFrank==null?"":respDoddFrank.getReferenceNumber(), e, userId);
							throw new AgentConnectException(e);
						}
					} catch (Exception e1) {
						EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("response failed.\n\n" , e1);
						throw new AgentConnectException(e1);
					} finally {
						EventMonitor.getInstance().recordEvent(EventListConstant.P2P_AC_SEND, beginTime, System.currentTimeMillis());
					}
					String referenceNumber = respDoddFrank.getReferenceNumber();
					tran.setLgcyRefNbr(referenceNumber);
					tran.setSndTranDate(new Date());
					tran.setTranAvailabilityDate(respDoddFrank.getExpectedDateOfDelivery());

					//S29a Add three minute phone number and three minute pin number
					if(tran.getDlvrOptnId()==DeliveryOption.WILL_CALL_ID||tran.getDlvrOptnId()==DeliveryOption.ONLY_AT_ID)
					{

					tran.setThreeMinuteFreePhoneNumber(respDoddFrank.getTollFreePhoneNumber());
					tran.setThreeMinuteFreePinNumber(respDoddFrank.getFreePhoneCallPIN());
					}
					// MGO- 12802 Changes starts,Adding the if condition.
					//Latest FX rate needs to be calculated on approving the transaction only for EPSEND
					if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE))
					{
					if (!"USA".equalsIgnoreCase(tran.getRcvISOCntryCode())) {
						BigDecimal exRate=null;
						exRate = getNewExchangeRateDoddFrank(
						        agentConnectAccessorDoddFrank,
						        tran.getRcvISOCntryCode(),
						        tran.getSndISOCntryCode(),
						        tran.getDlvrOptnId(),
								tran.getRcvISOCrncyId(),
								tran.getRcvPayoutISOCrncyId(),
								null,
								tran.getLoyaltyPgmMembershipId());
						// set to new foreign exchange rate
						if (exRate != null) {
							tran.setSndFxCnsmrRate(exRate);
                            if (!tran.getRcvISOCrncyId()
                                    .equals(tran.getRcvPayoutISOCrncyId())) {
                                tran.setRcvISOCrncyId(tran.getRcvPayoutISOCrncyId());
                            }
						}
					}
					}
					String refNbr = respDoddFrank.getPartnerConfirmationNumber();
					if (!StringHelper.isNullOrEmpty(refNbr)) {
						tran.setRcvAgentRefNbr(refNbr);
					}
					ConsumerAccountType accountType = useBackupAccount ? ConsumerAccountType.getInstance(tran.getSndBkupAcctTypeCode())
							: ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
					if (!useBackupAccount) {
						tran.setTranStatCode(TransactionStatus.SENT_CODE);
						tran.setSndTranDate(new Date());
						tranManager.updateTransaction(tran, userId, null, null);
					}
					try {
						sendPersonToPersonSuccessEmail(tran, accountType);
					} catch (Exception e) {
						// Email failed to get sent.
						// Not a serious error, log and move on.
						EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
					}
				} catch (Throwable t) {
					addAgentConnectErrorToTranComment(t, tranId, userId);
					String msg = "In Program: TransactionServiceImpl\n" + "In Method: sendPersonToPerson()\n" + "AC call started at: "
					+ before + "\n" + "AC call aborted at: " + new Date(System.currentTimeMillis()) + "\n"
					+ "AC Method called: moneyGramSend() with a transaction id of " + tran.getEmgTranId()
					+ " and the request object is as follows:\n\n" + reqDoddFrank;
					EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Send Person To Person failed.\n\n" + msg, t);
					sendAgentConnectFailMail(msg, t, userId);
					// tran.setTranStatCode(TransactionStatus.ERROR_CODE);
					// tranManager.setTransactionStatus(
					// tranId,
					// tran.getTranStatCode(),
					// tran.getTranSubStatCode(),
					// userId);
					return t;
				}
				return null;
			}
		};
//		Object obj = updateTransaction(tranId, userId, !isMgiTranId(tran)?command76:command1305);
		Object obj = updateTransaction(tranId, userId, command1305);
		
		if (obj != null) {
			throw new AgentConnectException((Throwable) obj);
		}
	}

	public ArrayList processMoneyGramTransaction(int tranId, String callerLoginId, String tranStat,
			String fundStat, String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException,
	AgentConnectException, InvalidRRNException, DataSourceException, SQLException {
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(tranId);
		ArrayList<String> errors = new ArrayList<String>();

		if (failCaliforniaCompliance(tranId, callerLoginId)) {
			errors.add("error.california.compliance.failed");
			return errors;
		}
		// Execute Credit Card Auth with APP/NOF status
		if (TransactionStatus.APPROVED_CODE.equals(tranStat) && TransactionStatus.NOT_FUNDED_CODE.equals(fundStat)) {
			CreditCardServiceResponse authResponse = executeCreditCardAuth(tranId, callerLoginId, contextRoot);
			if (authResponse == null) {
				return null;
			}
		}

		tm = ManagerFactory.createTransactionManager();
		tran = tm.getTransaction(tranId);

		// Do AgentConnect MoneyGram Send with status of APP/CCA
		if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())
				&& TransactionStatus.CC_AUTH_CODE.equals(tran.getTranSubStatCode())) {
			try {
				sendPersonToPerson(tranId, callerLoginId, false);
				tran = tm.getTransaction(tranId);
			} catch (AgentConnectException e) {
				// add admin message
				String[] params = { String.valueOf(tranId), tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
				sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.person.to.person.send.fail.notice", params));
				errors.add("error.agent.connect.failed");
				return errors;
			}
		}
		// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
		if (TransactionStatus.SENT_CODE.equals(tran.getTranStatCode()) && TransactionStatus.CC_AUTH_CODE.equals(tran.getTranSubStatCode())) {
			// find conf. code from tran action table with reason code of 28
			String ccAuthConfReasonCode = "28";
			String AuthConfCode = tm.getCCAuthConfId(tranId, ccAuthConfReasonCode);
			fundPersonToPerson(tranId, AuthConfCode, callerLoginId, false, contextRoot, tran.getTranSeqNumber());
		}
		return null; // all is well.
	}

	public void fundPersonToPerson(final int tranId, final String authRequestId, final String userId, final boolean useBackupAccount,
			final String contextRoot, final long effortId) throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		Validate.notNull(authRequestId, "authRequestId is a required parameter and must not be null.");
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				if (tran.isFundable() == false) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a fundable state");
				}
				final int tranId = tran.getEmgTranId();
				ConsumerAccountType accountType = useBackupAccount ? ConsumerAccountType.getInstance(tran.getSndBkupAcctTypeCode())
						: ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
				if (accountType.isBankAccount()) {
					tran.setTranSubStatCode(TransactionStatus.ACH_WAITING_CODE);
					tranManager.updateTransaction(tran, userId);
				} else if (accountType.isCardAccount()) {
					final double amount = tran.getSndTotAmt().doubleValue();
					long beginTime = System.currentTimeMillis();

					CreditCardServiceResponse captureResponse = null;
					CreditCardService ccservice = emgshared.services.ServiceFactory.getInstance().getCreditCardService();
					captureResponse = ccservice.executeCapture(authRequestId, amount,
							String.valueOf(tranId), tran.getMccPartnerProfileId());
					EventMonitor.getInstance().recordEvent(EventListConstant.CYBERSOURCE_CAPTURE, beginTime, System.currentTimeMillis());

					if (captureResponse != null && captureResponse.isAccepted()) {
						String requestId = captureResponse.getIdentifier();
						tran.setTranSubStatCode(TransactionStatus.CC_SALE_CODE);
						tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
					} else {
						CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
						commentService.insertCreditCardResponseComment(captureResponse, new Integer(tran.getSndCustAcctId()), new Integer(
								tranId), resourceFile, userId);
						if (captureResponse != null && !captureResponse.isReTriable()) {
							try {
								sendCreditCardFailEmail(tran);
							} catch (Exception e) {
								// Email failed to get sent.
								// Not a serious error, log and move on.
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:",
										e);
							}
							tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
							tranManager.updateTransaction(tran, userId, authRequestId, null);
							tran.setTranStatCode(TransactionStatus.ERROR_CODE);
							tranManager.updateTransaction(tran, userId, null, null);
						} else if (captureResponse != null && captureResponse.isReTriable()) {
							// add admin message
							String[] params = { String.valueOf(tran.getEmgTranId()), captureResponse.getReason(),
									getTransactionUrl(contextRoot) };
							sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.capture.fail.notice", params));
						}
					}
				}
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public CreditCardServiceResponse executeCreditCardAuth(final int tranId, final String userId, final String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (StringHelper.isNullOrEmpty(tran.getMccPartnerProfileId())) {
					// if MCC profile is not set, figure it out now.
					if (tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE)) {
						BillerLimit billerLimit = new BillerLimit();
						try {
							billerLimit = emgshared.services.ServiceFactory.getInstance().getBillerLimitService().getBillerLimit(
									tran.getRcvAgcyCode());
						} catch (Exception e) {
							throw new IllegalArgumentException("Biller MCC code could not be retrieved for transaction = " + tranId);
						}
						tran.setMccPartnerProfileId(billerLimit.getPaymentProfileId());
					} else {
						tran.setMccPartnerProfileId(EMTSharedContainerProperties.getMerchantIdEMG());
					}
					if (Constants.SITE_IDENTIFIER_WAP.equals(tran.getPartnerSiteId())) {
						tran.setMccPartnerProfileId(EMTAdmContainerProperties.getMCCPartnerIdOverride());
					}
				}
				CreditCardServiceResponse authResponse = null;
				final String TRANSACTION_DETAIL_URL = contextRoot + "/transactionDetail.do";
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				if (tran.isFundable() == false) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a fundable state");
				}
				final ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
				final int creditCardAccountId = (primaryAccountType.isCardAccount() ? tran.getSndCustAcctId() : tran.getSndCustBkupAcctId());
				double amount = tran.getSndTotAmt().doubleValue();
				if (primaryAccountType.isBankAccount()) {
					amount += eadp.getStandardNsfFee();
				}
				String requestId;
				boolean ccAuthAccepted;
				ServiceFactory adminServiceFactory = ServiceFactory.getInstance();
				long beginTime = System.currentTimeMillis();
				// adminServiceFactory.getCreditCardService()
				authResponse = adminServiceFactory.getSecuredCreditCardService().executeAuthorization(creditCardAccountId, amount,
						String.valueOf(tranId), userId, tran.getMccPartnerProfileId(), tran.getPartnerSiteId());
				EventMonitor.getInstance().recordEvent(EventListConstant.CYBERSOURCE_CREDIT_AUTH, beginTime, System.currentTimeMillis());
				if (authResponse != null) {
					// JMA here's where ccdecline, ccreject or ccaccept
					ccAuthAccepted = authResponse.isAccepted();
					requestId = authResponse.getIdentifier();
				} else {
					ccAuthAccepted = false;
					requestId = null;
				}
				CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
				if (ccAuthAccepted) {
					tran.setTranSubStatCode(TransactionStatus.CC_AUTH_CODE);
					tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
					ConsumerCreditCardAccount acctNum = null;
					try {
						acctNum = accountService.getCreditCardAccount(creditCardAccountId, userId);
					} catch (DataSourceException e) {
					}
					commentService.insertAVSCreditCardResponseComment(authResponse.getAVSCode(), null, new Integer(tranId), resourceFile,
							userId, acctNum.getAccountNumberMask());
				} else {
					commentService.insertCreditCardResponseComment(authResponse, new Integer(creditCardAccountId), new Integer(tranId),
							resourceFile, userId);
					if (authResponse != null && authResponse.isReTriable()) {
						// add admin message
						String[] params1 = { String.valueOf(tran.getEmgTranId()), authResponse.getReason(), TRANSACTION_DETAIL_URL };
						sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.auth.fail.notice", params1));
					} else {
						if (authResponse == null) {
							// add admin message
							String[] params1 = { String.valueOf(tran.getEmgTranId()),
									"Network Error: No response received from Cybersource", TRANSACTION_DETAIL_URL };
							sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.auth.fail.notice", params1));
						}
						try {
							sendCreditCardFailEmail(tran);
						} catch (Exception e) {
							// Email failed to get sent.
							// Not a serious error, log and move on.
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
						}
						tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
						tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
						tran.setTranStatCode(TransactionStatus.ERROR_CODE);
						tranManager.updateTransaction(tran, userId, null, null);
					}
				}
				return authResponse;
			}
		};
		return (CreditCardServiceResponse) updateTransaction(tranId, userId, command);
	}

	public void doCreditCardReversal(final int tranId, final String userId, final String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				final Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				String RequestId;
				try {
					RequestId = tranManager.getCCAuthConfId(tranId, ccFundedConfReasonCode);
					if (StringHelper.isNullOrEmpty(RequestId)) {
						RequestId = tranManager.getCCAuthConfId(tranId, ccFeeConfReasonCode);
					}
				} catch (Exception e) {
					throw new EMGRuntimeException(e);
				}
				Validate.notNull(RequestId, "authRequestId is a required parameter and must not be null.");
				final int tranId = tran.getEmgTranId();
				final double amount = tran.getSndTotAmt().doubleValue();
				CreditCardServiceResponse captureResponse = null;

				CreditCardService ccservice = emgshared.services.ServiceFactory.getInstance().getCreditCardService();
				long beginTime = System.currentTimeMillis();
				captureResponse = ccservice.executeCredit(RequestId, amount, String.valueOf(tranId), tran.getMccPartnerProfileId());
				EventMonitor.getInstance()
				.recordEvent(EventListConstant.CYBERSOURCE_CREDIT_REVERSAL, beginTime, System.currentTimeMillis());
				if (captureResponse != null && captureResponse.isAccepted()) {
					String requestId = captureResponse.getIdentifier();
					tran.setTranSubStatCode(TransactionStatus.CC_CANCEL_CODE);
					tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
				} else {
					CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
					commentService.insertCreditCardResponseComment(captureResponse, new Integer(tran.getSndCustAcctId()), new Integer(
							tranId), resourceFile, userId);
					if (captureResponse != null && !captureResponse.isReTriable()) {
						try {
							sendCreditCardFailEmail(tran);
						} catch (Exception e) {
							// Email failed to get sent.
							// Not a serious error, log and move on.
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
						}
						/*tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
						tranManager.updateTransaction(tran, userId, RequestId, null);
						tran.setTranStatCode(TransactionStatus.ERROR_CODE);
						tranManager.updateTransaction(tran, userId, null, null);*/
					} else if (captureResponse != null && captureResponse.isReTriable()) {
						// add admin message
						String[] params = { String.valueOf(tran.getEmgTranId()), captureResponse.getReason(),
								getTransactionUrl(contextRoot) };
						sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.reversal.fail.notice", params));
					}
				}
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	protected void fundExpressPayment(final int tranId, final String authRequestId, final String userId,
			final String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
				Validate.notNull(authRequestId, "uthRequestId is a required parameter and must not be null.");
				UpdateTransactionCommand command = new UpdateTransactionCommand() {
					public Object execute() {
						final Transaction tran = tranManager.getTransaction(tranId);
						if (tran == null) {
							throw new IllegalArgumentException("Unable to retrieve transaction with id = " + tranId);
						}
						if (tran.isFundable() == false) {
							throw new IllegalStateException("transaction id = " + tranId + " is not in a fundable state");
						}
						final int tranId = tran.getEmgTranId();
						ConsumerAccountType accountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
						if (accountType.isBankAccount()) {
							tran.setTranSubStatCode(TransactionStatus.ACH_WAITING_CODE);
							tranManager.updateTransaction(tran, userId);
						} else if (accountType.isCardAccount()) {
							final double amount = tran.getSndTotAmt().doubleValue();
							long beginTime = System.currentTimeMillis();
							CreditCardServiceResponse captureResponse = null;
							CreditCardService ccservice = emgshared.services.ServiceFactory.getInstance().getCreditCardService();
							captureResponse = ccservice
							.executeCapture(authRequestId, amount, String.valueOf(tranId), tran.getMccPartnerProfileId());
							EventMonitor.getInstance().recordEvent(EventListConstant.CYBERSOURCE_CAPTURE, beginTime, System.currentTimeMillis());
							if (captureResponse != null && captureResponse.isAccepted()) {
								String requestId = captureResponse.getIdentifier();
								tran.setTranSubStatCode(TransactionStatus.CC_SALE_CODE);
								tranManager.updateTransaction(tran, userId, StringUtils.trimToNull(requestId), null);
							} else {
								CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
								commentService.insertCreditCardResponseComment(captureResponse, new Integer(tran.getSndCustAcctId()), new Integer(
										tranId), resourceFile, userId);
								if (captureResponse != null && !captureResponse.isReTriable()) {
									try {
										sendCreditCardFailEmail(tran);
									} catch (Exception e) {
										// Email failed to get sent.
										// Not a serious error, log and move on.
										EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:",
												e);
									}
									tran.setTranSubStatCode(TransactionStatus.CC_FAILED_CODE);
									tranManager.updateTransaction(tran, userId, authRequestId, null);
									tran.setTranStatCode(TransactionStatus.ERROR_CODE);
									tranManager.updateTransaction(tran, userId, null, null);
								} else if (captureResponse != null && captureResponse.isReTriable()) {
									// add admin message
									String[] params = { String.valueOf(tran.getEmgTranId()), captureResponse.getReason(),
											getTransactionUrl(contextRoot) };
									sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.cc.capture.fail.notice", params));
								}
							}
						}
						return null;
					}
				};
				updateTransaction(tranId, userId, command);
			}

	public ArrayList processExpressPayTransaction(int tranId, String callerLoginId, String tranStat, String fundStat,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException {
				ArrayList<String> esl = new ArrayList<String>();
				TransactionManager tm = ManagerFactory.createTransactionManager();
				Transaction tran = ManagerFactory.createTransactionManager().getTransaction(tranId);
				ConsumerAccountType accountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
				if (accountType.isCardAccount()) {
					if (TransactionStatus.APPROVED_CODE.equals(tranStat) && TransactionStatus.NOT_FUNDED_CODE.equals(fundStat)) {
						executeCreditCardAuth(tranId, callerLoginId, contextRoot);
					}
				}
				tran = tm.getTransaction(tranId);
				if (tran.getTranStatCode().equals(TransactionStatus.APPROVED_CODE)
						&& tran.getTranSubStatCode().equals(TransactionStatus.CC_AUTH_CODE) && tran.isSendable()) {
					try {
						sendExpressPayment(tranId, callerLoginId, contextRoot);
						tran = tm.getTransaction(tranId);
					} catch (AgentConnectException e) {
						esl.add("error.agent.connect.failed");
						return esl;
					}
				}
				// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
				if (TransactionStatus.SENT_CODE.equals(tran.getTranStatCode()) && TransactionStatus.CC_AUTH_CODE.equals(tran.getTranSubStatCode())) {
					// find conf. code from tran action table with reason code of 28
					String ccAuthConfReasonCode = "28";
					String AuthConfCode;
					try {
						AuthConfCode = tm.getCCAuthConfId(tranId, ccAuthConfReasonCode);
						fundExpressPayment(tranId, AuthConfCode, callerLoginId, contextRoot);
					} catch (Exception e) {
						esl.add(new String("Failed CC Auth Retrieval"));
					}
				}
				return null;
			}
}
