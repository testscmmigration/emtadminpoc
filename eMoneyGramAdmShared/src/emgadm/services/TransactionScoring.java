package emgadm.services;

import java.sql.SQLException;
import java.util.Map;

import emgshared.exceptions.DataSourceException;
import emgshared.model.ScoreConfiguration;

public interface TransactionScoring {
	public Map getTransactionScore(String userId, int tranId) throws DataSourceException, SQLException;
	public Map getTransactionScore(String userId, int tranId, ScoreConfiguration scoreConfiguration, Map categoryResults)
	throws DataSourceException, SQLException;
}
