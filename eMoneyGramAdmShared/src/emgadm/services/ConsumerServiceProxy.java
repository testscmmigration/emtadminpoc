package emgadm.services;

import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest;


public interface ConsumerServiceProxy {
	
	public GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatusResponse(long customerId, String partnerSiteId) throws Exception;
	public GetConsumerIDImageResponse getConsumerIDImageResponse(long customerId, String partner) throws Exception;
	public void updateConsumerProfile(
			UpdateConsumerProfileRequest updateConsumerProfileRequest,
			String parentSiteId) throws Exception;
}
