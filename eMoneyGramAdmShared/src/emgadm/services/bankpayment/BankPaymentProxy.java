package emgadm.services.bankpayment;


public interface BankPaymentProxy 
{
//	public AuthAccountResponse authAcct(AuthAccountRequest authAccountRequest) throws RemoteException, ProxyException;
//    public PostToAccountResponse postToAcct(PostToAccountRequest parameters) throws java.rmi.RemoteException, ProxyException;
//    public AdjustmentResponse refundAcct(AdjustmentRequest parameters) throws java.rmi.RemoteException, ProxyException;
    // Local
    public StatusInquiryResponseMessage statusInquiry(StatusInquiryRequestMessage sIRM) throws Exception;
	public AdjustmentInquiryResponseMessage adjustmentInquiry(AdjustmentInquiryRequestMessage sIRM) throws Exception;
}
