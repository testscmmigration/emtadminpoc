package emgadm.services.bankpayment;

import java.io.Serializable;
import java.rmi.Remote;

public class StatusInquiryRequestMessage implements Remote, Serializable {

	private static final long serialVersionUID = 1L;
	private String clientTraceId;
	private String processorTraceId;

	public String getClientTraceId() {
		return clientTraceId;
	}
	public void setClientTraceId(String clientTraceId) {
		this.clientTraceId = clientTraceId;
	}
	public String getProcessorTraceId() {
		return processorTraceId;
	}
	public void setProcessorTraceId(String processorTraceId) {
		this.processorTraceId = processorTraceId;
	}
}
