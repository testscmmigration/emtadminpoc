package emgadm.services.bankpayment;

import java.io.Serializable;
import java.rmi.Remote;

public class AdjustmentInquiryRequestMessage implements Remote, Serializable {

	private static final long serialVersionUID = 1L;
	private String clientTraceId;
	private String processorTraceId;
	private String transactionType;
	private String totalAmmount;

	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTotalAmmount() {
		return totalAmmount;
	}
	public void setTotalAmmount(String totalAmmount) {
		this.totalAmmount = totalAmmount;
	}
	public String getClientTraceId() {
		return clientTraceId;
	}
	public void setClientTraceId(String clientTraceId) {
		this.clientTraceId = clientTraceId;
	}
	public String getProcessorTraceId() {
		return processorTraceId;
	}
	public void setProcessorTraceId(String processorTraceId) {
		this.processorTraceId = processorTraceId;
	}
}
