package emgadm.services;

import java.net.URL;


import com.moneygram.mgo.consumer_v2.common_v1.ClientHeader;
import com.moneygram.mgo.consumer_v2.common_v1.Header;
import com.moneygram.mgo.consumer_v2.common_v1.ProcessingInstruction;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDApprovalStatusResponse;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageRequest;
import com.moneygram.mgo.service.consumer_v2.GetConsumerIDImageResponse;
import com.moneygram.mgo.service.consumer_v2.MGOConsumerServicePortType_v2;
import com.moneygram.mgo.service.consumer_v2.MGOConsumerService_v2SoapBindingStub;
import com.moneygram.mgo.service.consumer_v2.UpdateConsumerProfileRequest;

import emgadm.property.EMTAdmContainerProperties;

public class ConsumerServiceProxyImpl implements ConsumerServiceProxy {

	private static final ConsumerServiceProxyImpl instance = new ConsumerServiceProxyImpl();
	
	private ConsumerServiceProxyImpl()
	{
	}

	public static final ConsumerServiceProxyImpl getInstance()
	{
		return instance;
	}
	
	public GetConsumerIDApprovalStatusResponse getConsumerIDApprovalStatusResponse(
		long customerId, String partnerSiteId) throws Exception {
		
		//String url = "http://q3wsintsvcs.qacorp.moneygram.com/MGOService/services/MGOConsumerService_v2?wsdl";
		String url = EMTAdmContainerProperties.getConsumerServiceUrl();
			
		URL consumerServiceURL = new URL(url);
		
		MGOConsumerServicePortType_v2 consumerService = 
			new MGOConsumerService_v2SoapBindingStub(consumerServiceURL, null);
		
		GetConsumerIDApprovalStatusRequest idApprovalStatusRequest;
		idApprovalStatusRequest = new GetConsumerIDApprovalStatusRequest();
		idApprovalStatusRequest.setCustId(customerId);
		
		//FIXME For now these are set to 0, do we need this?  Since the external id is 
		// available above via form.setIdNumber(id);
		idApprovalStatusRequest.setEncryptExtnlId("");
		idApprovalStatusRequest.setMaskExtnlId("");
		
		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction("getConsumerIDApprovalStatus_v2");
		
		pi.setReadOnlyFlag(false);
		pi.setPartnerSiteId(partnerSiteId);
		
		header.setProcessingInstruction(pi);
		ClientHeader ch = new ClientHeader();
		ch.setClientRequestID(String.valueOf(customerId) + 
				String.valueOf(new java.util.Date().getTime()));
		header.setClientHeader(ch);
		idApprovalStatusRequest.setHeader(header);
		
		GetConsumerIDApprovalStatusResponse idApprovalStatusResponse;
		idApprovalStatusResponse = consumerService.getConsumerIDApprovalStatus(idApprovalStatusRequest);
		
		return idApprovalStatusResponse;
		
	}

	public GetConsumerIDImageResponse getConsumerIDImageResponse(
			long customerId, String partnerSiteId) throws Exception {
		
		//String url = "http://q3wsintsvcs.qacorp.moneygram.com/MGOService/services/MGOConsumerService_v2?wsdl";
		String url = EMTAdmContainerProperties.getConsumerServiceUrl();
    	
		URL consumerServiceURL = new URL(url);

		MGOConsumerServicePortType_v2 consumerService;
		consumerService = new MGOConsumerService_v2SoapBindingStub(consumerServiceURL, null);
		
		// Obtain id image
		GetConsumerIDImageRequest idImageRequest = new GetConsumerIDImageRequest();
		
		//FIXME should consumerId type be changed to long
		idImageRequest.setCustomerExternalId(customerId);
		
		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction("getConsumerIDImage_v2");
		pi.setPartnerSiteId(partnerSiteId);
		pi.setReadOnlyFlag(false);
		
		header.setProcessingInstruction(pi);
		ClientHeader ch = new ClientHeader();
		ch.setClientRequestID(String.valueOf(customerId) + 
				"-" + String.valueOf(new java.util.Date().getTime()));
		header.setClientHeader(ch);
		idImageRequest.setHeader(header);
	
		GetConsumerIDImageResponse idImageResponse = 
			consumerService.getConsumerIDImage(idImageRequest);
		
		return idImageResponse;
	}
	public void updateConsumerProfile(
			UpdateConsumerProfileRequest updateConsumerProfileRequest,
			String partnerSiteId) throws Exception {
		// Added new method for MGO-9024
		Header header = new Header();
		
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction("updateConsumerProfile_v2");
		processingInstruction.setReadOnlyFlag(false);

		header.setProcessingInstruction(processingInstruction);

		updateConsumerProfileRequest.setHeader(header);

		new MGOConsumerService_v2SoapBindingStub(
				new URL(EMTAdmContainerProperties.getConsumerServiceUrl()),
				null).update(updateConsumerProfileRequest);
	}
}
