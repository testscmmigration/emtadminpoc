package emgadm.services.creditcardpayment;


import com.moneygram.service.CreditCardPaymentService_v1.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v1.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.StatusResponse;

import emgshared.model.Transaction;


public interface CreditCardPaymentProxy  {




	public AuthorizationResponse authorize(Transaction transaction, String callerLoginId) throws Exception;



	public StatusResponse status(String captureRequestId, long accountId,
			long effortId, int tranId, String merchantId, String currency) throws Exception;

	public StatusResponse status(String captureRequestId, long accountId,
	        long effortId, int tranId, String merchandId, String currency,
	        long orderId) throws Exception;

	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId) throws Exception;

	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId, int orderId) throws Exception;

	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long internalTranNumber) throws Exception;

	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long internalTranNumber, int orderId) throws Exception;
}
