package emgadm.services.creditcardpayment;


import org.apache.commons.lang.StringUtils;

import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.service.CreditCardPaymentService_v1.AuthorizationRequest;
import com.moneygram.service.CreditCardPaymentService_v1.AuthorizationResponse;
import com.moneygram.service.CreditCardPaymentService_v1.BusinessRules;
import com.moneygram.service.CreditCardPaymentService_v1.Card;
import com.moneygram.service.CreditCardPaymentService_v1.CardType;
import com.moneygram.service.CreditCardPaymentService_v1.CurrencyAmount;
import com.moneygram.service.CreditCardPaymentService_v1.GlobalCollectRequest;
import com.moneygram.service.CreditCardPaymentService_v1.PostTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v1.PostTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.RefundTransactionRequest;
import com.moneygram.service.CreditCardPaymentService_v1.RefundTransactionResponse;
import com.moneygram.service.CreditCardPaymentService_v1.StatusRequest;
import com.moneygram.service.CreditCardPaymentService_v1.StatusResponse;
import com.moneygram.service.CreditCardPaymentService_v1.client.CreditCardPaymentServiceClient;

import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ProxyException;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.model.ConsumerAddress;
import emgshared.model.ConsumerCreditCardAccount;
import emgshared.model.ConsumerProfile;
import emgshared.model.CreditCard;
import emgshared.model.Transaction;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.PCIService;
import emgshared.util.Constants;

public class CreditCardPaymentProxyImpl implements CreditCardPaymentProxy {

	public static final String GLOBAL_COLLECT_ACTION_AUTHORIZE = "authorize";
	public static final String GLOBAL_COLLECT_ACTION_STATUS = "orderStatus";
	public static final String GLOBAL_COLLECT_ACTION_REFUND = "refundTransaction";
	public static final String GLOBAL_COLLECT_ACTION_POST = "postTransaction";

	private EMTSharedDynProperties eadp = new EMTSharedDynProperties();
	private static final PCIService pciService = emgshared.services.ServiceFactory.getInstance().getPCIService();

	private static class SingletonContainer {
		public static final CreditCardPaymentProxy INSTANCE = new CreditCardPaymentProxyImpl();
	}

	private CreditCardPaymentProxyImpl() {
	}

	private static final int DEFAULT_TIMEOUT = 15000;
	private CreditCardPaymentServiceClient creditCardPaymentClient;

	public static CreditCardPaymentProxy getInstance() throws ProxyException {
		return SingletonContainer.INSTANCE;
	}

	private CreditCardPaymentServiceClient getCreditCardPaymentClient() throws ProxyException {
		if (creditCardPaymentClient != null) {
			return creditCardPaymentClient;
		} else {
			synchronized (CreditCardPaymentProxyImpl.class) {
				try {
					String url = EMTAdmContainerProperties.getCreditCardPaymentServiceUrl();
					String to = EMTAdmContainerProperties.getCreditCardPaymentServiceTimeout();
					int timeout = DEFAULT_TIMEOUT;
					if (StringUtils.isNumeric(to)) {
						timeout = Integer.parseInt(to);
					} else {
						EMGSharedLogger.getLogger(this.getClass().getName().toString()).warn(
								"Invalid timeout value for CreditCardPaymentService: " + to
										+ ". Using default value of " + timeout);
					}
					creditCardPaymentClient = new CreditCardPaymentServiceClient(url, timeout);
					return creditCardPaymentClient;
				} catch (Exception e) {
					throw new ProxyException("Failed to create CreditCardPaymentClient", e);
				}
			}
		}
	}


	private AuthorizationResponse authorize(AuthorizationRequest request) throws Exception {
		return getCreditCardPaymentClient().authorize(request);
	}

	public AuthorizationResponse authorize(Transaction transaction, String callerLoginId) throws Exception {

		int accountId = transaction.getSndCustAcctId();
		double amount = transaction.getSndTotAmt().doubleValue();
		String currency = transaction.getSndISOCrncyId();
		String merchantId = transaction.getMccPartnerProfileId();
		int tranId = transaction.getEmgTranId();

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory.getInstance();
		ConsumerProfileService profileService = sharedServiceFactory.getConsumerProfileService();
		ConsumerAccountService accountService = sharedServiceFactory.getConsumerAccountService();

		ConsumerCreditCardAccount account = accountService.getCreditCardAccount(accountId, callerLoginId);
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(account.getConsumerId(), callerLoginId, null);

		String decryptedAccountNumber =	pciService.retrieveCardNumber(account.getId(), true);
		CreditCard creditCard = new CreditCard(decryptedAccountNumber, null,
				account.getExpireMonth(), account.getExpireYear());
		ConsumerAddress billingAddress = account.getBillingAddress();

		AuthorizationRequest request = new AuthorizationRequest();

		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);
		request.setAmount(currencyAmount);

		BusinessRules businessRules = new BusinessRules();
		if (!eadp.isCcAVSTrans()) {
			businessRules.setDeclineAVSFlags("");
			businessRules.setIgnoreAVSResult(true);
		} else {
			businessRules.setDeclineAVSFlags(EMTSharedContainerProperties.getAvsDeclineFlags());
			businessRules.setIgnoreAVSResult(false);
		}
		request.setBusinessRules(businessRules);

		String cardTypeAccount = account.getCreditOrDebitCode();

		Card card = new Card();
		card.setAccountNumber(creditCard.getAccountNumber());
		// Set the card type field according the cardTypeAccount variable
		if (cardTypeAccount.equals(ConsumerCreditCardAccount.CREDIT_CODE))
			card.setCardType(CardType.credit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.DEBIT_CODE))
			card.setCardType(CardType.debit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE))
			card.setCardType(CardType.unknown);

		card.setExpirationMonth(creditCard.getExpireMonth());
		card.setExpirationYear(creditCard.getExpireYear());
		card.setCardBrandType(account.getAccountType().getCode());
		card.setFullName(consumerProfile.getFirstName() + " " + consumerProfile.getMiddleName()
				+ " " + consumerProfile.getLastName());
		request.setCard(card);

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		Long orderId=new Long(tranId);
		globalCollect.setOrderid(orderId);
		request.setGlobalCollect(globalCollect);

		request.setStreet1(billingAddress.getAddressLine1());
		request.setStreet2(billingAddress.getAddressLine2());
		request.setCity(billingAddress.getCity());
		request.setState(billingAddress.getState());
		request.setPostalCode(billingAddress.getPostalCode());

		request.setCountry(billingAddress.getIsoCountryCode());
		request.setCountryCode2Char(billingAddress.getIsoCountryCode().substring(0, 2)); //FIXME place correct country code

		request.setCustomerId(String.valueOf(consumerProfile.getId()));
		request.setFirstName(consumerProfile.getFirstName());
		request.setLastName(consumerProfile.getLastName());

		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();
		pi.setAction(GLOBAL_COLLECT_ACTION_AUTHORIZE);
		pi.setReturnErrorsAsException(true);
		header.setProcessingInstruction(pi);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		request.setPerformCVVAVS(false);
		AuthorizationResponse resp = authorize(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(request, resp, 255);
		} catch(Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("ERROR Formatting CCPS authorize resp data for txn comment:", e);
		}
		//formatting method requires request/response and request is build in this lower level method.  Get
		//the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		//return authorize(request);
	}



	private StatusResponse status(StatusRequest request) throws Exception {
		return getCreditCardPaymentClient().status(request);
	}

	public StatusResponse status(String captureRequestId, long accountId,
	        long effortId, int tranId, String merchantId, String currency) throws Exception {
	    // Last argument, order id, is set to -1 to identify Tx as non-maestro
	    return status(captureRequestId, accountId, effortId, tranId,
	            merchantId, currency, -1);
	}

	public StatusResponse status(String captureRequestId, long accountId,
			long effortId, int tranId, String merchantId, String currency,
			long orderId) throws Exception {
	    // Method argument orderId is '-1' when Tx is non-Maestro

		StatusRequest request = new StatusRequest();

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
	    globalCollect.setEffortId((orderId == -1 )? new Long(effortId): new Long(1));
	    globalCollect.setOrderid((orderId == -1 )? new Long(accountId): new Long(orderId));
	    if (orderId != -1) {
	        globalCollect.setAttemptId(new Long(1));
	    }
		request.setGlobalCollect(globalCollect);

		Header header = new Header();
		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(GLOBAL_COLLECT_ACTION_STATUS);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		StatusResponse resp = status(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(request, resp, 255);
		} catch(Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("ERROR Formatting CCPS Status resp data for txn comment:", e);
		}
		//formatting method requires request/response and request is build in this lower level method.  Get
		//the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		//return status(request);
	}


	private PostTransactionResponse postTransaction(PostTransactionRequest request) throws Exception {
		return getCreditCardPaymentClient().postTransaction(request);
	}

	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId) throws Exception {
	    // Last argument is set to -1 to identify Tx as non-maestro
		return postTransaction(accountId, amount,currency, callerLoginId, merchantId, effortId,tranId, -1 );
	}

	public PostTransactionResponse postTransaction(int accountId, double amount,
			String currency, String callerLoginId,
			String merchantId, long effortId, int tranId, int orderId) throws Exception {
	    // Method argument orderId is '-1' when Tx is non-Maestro

		emgshared.services.ServiceFactory sharedServiceFactory = emgshared.services.ServiceFactory.getInstance();
		ConsumerProfileService profileService = sharedServiceFactory.getConsumerProfileService();
		ConsumerAccountService accountService = sharedServiceFactory.getConsumerAccountService();

		ConsumerCreditCardAccount account = accountService.getCreditCardAccount(accountId, callerLoginId);
		ConsumerProfile consumerProfile = profileService.getConsumerProfile(account.getConsumerId(), callerLoginId, null);

		String decryptedAccountNumber =	pciService.retrieveCardNumber(account.getId(), true);
		CreditCard creditCard = new CreditCard(decryptedAccountNumber, null,
				account.getExpireMonth(), account.getExpireYear());
		ConsumerAddress billingAddress = account.getBillingAddress();

		PostTransactionRequest request = new PostTransactionRequest();

		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);

		request.setAmount(currencyAmount);

		// Card type account
		String cardTypeAccount = account.getCreditOrDebitCode();

		Card card = new Card();
		// Set the Card type enumerated vale
		card.setAccountNumber(creditCard.getAccountNumber());
		if (cardTypeAccount.equals(ConsumerCreditCardAccount.CREDIT_CODE))
			card.setCardType(CardType.credit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.DEBIT_CODE))
			card.setCardType(CardType.debit);
		else if (cardTypeAccount.equals(ConsumerCreditCardAccount.UNKNOWN_CODE))
			card.setCardType(CardType.unknown);

		// card.setCardType(account.getAccountType().getCode());
		card.setExpirationMonth(creditCard.getExpireMonth());
		card.setExpirationYear(creditCard.getExpireYear());
		card.setFullName(consumerProfile.getFirstName() + " " + consumerProfile.getMiddleName()
				+ " " + consumerProfile.getLastName());
		card.setCardBrandType(account.getAccountType().getCode());

		request.setCard(card);

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		// vy79
		// Apparently is the same logic but It should remain equal in case of future modifications 
		// for CC or Maestro that do not apply to the other product
		globalCollect.setEffortId((orderId == -1 )? new Long(effortId): new Long(1));
		globalCollect.setOrderid((orderId == -1 )? new Long(tranId): new Long(orderId));
		if (orderId != -1) {
		    globalCollect.setAttemptId(new Long(1));
		}
		request.setGlobalCollect(globalCollect);

		Header header = new Header();
		ProcessingInstruction pi = new ProcessingInstruction();

		pi.setAction(GLOBAL_COLLECT_ACTION_POST);
		pi.setReturnErrorsAsException(true);

		header.setProcessingInstruction(pi);
		request.setHeader(header);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		PostTransactionResponse resp = postTransaction(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(request, resp, 255);
		} catch(Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("ERROR Formatting CCPS PostTransaction resp data for txn comment:", e);
		}
		//formatting method requires request/response and request is build in this lower level method.  Get
		//the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		//return postTransaction(request);
	}


	private RefundTransactionResponse refundTransaction(RefundTransactionRequest request)
			throws Exception {
		return getCreditCardPaymentClient().refundTransaction(request);
	}
	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long tranId) throws Exception {
		// Last argument is set to -1 to identify Tx as non-maestro
		return refundTransaction(captureRequestId, amount, currency, accountId, merchantId, effortId, tranId, -1);
	}

	public RefundTransactionResponse refundTransaction(String captureRequestId, double amount,
			String currency, long accountId, String merchantId, long effortId, long tranId, int orderId) throws Exception {
		RefundTransactionRequest request = new RefundTransactionRequest();
		// Method argument orderId is '-1' when Tx is non-Maestro
		CurrencyAmount currencyAmount = new CurrencyAmount();
		currencyAmount.setAmount(new Float(amount));
		currencyAmount.setCurrency(currency);
		request.setAmount(currencyAmount);

		GlobalCollectRequest globalCollect = new GlobalCollectRequest();
		globalCollect.setEffortId((orderId == -1 )? new Long(effortId): new Long(1));
		globalCollect.setOrderid((orderId == -1 )? new Long(accountId): new Long(orderId));
		if (orderId != -1) {
		    globalCollect.setAttemptId(new Long(1));
		}
		request.setGlobalCollect(globalCollect);

		request.setMerchantID(merchantId);
		request.setMGIReferenceCode(String.valueOf(tranId));

		Header header = new Header();

		ClientHeader clientHeader = new ClientHeader();
		header.setClientHeader(clientHeader);
		ProcessingInstruction processingInstruction = new ProcessingInstruction();
		processingInstruction.setAction(GLOBAL_COLLECT_ACTION_REFUND);
		processingInstruction.setReturnErrorsAsException(true);
		header.setProcessingInstruction(processingInstruction);

		request.setHeader(header);

		RefundTransactionResponse resp = refundTransaction(request);
		String text = null;
		try {
			text = CreditCardPaymentServiceClient.getGCFormattedResponseText(request, resp, 255);
		} catch(Exception e) {
			text = "ERROR formatting resp data";
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("ERROR Formatting CCPS RefundTransaction resp data for txn comment:", e);
		}
		//formatting method requires request/response and request is build in this lower level method.  Get
		//the formatted text and store it in an unused response header field.
		if (resp.getHeader().getClientHeader() == null) {
			resp.getHeader().setClientHeader(new ClientHeader());
		}
		resp.getHeader().getClientHeader().setClientSessionID(text);

		return resp;
		//return refundTransaction(request);
	}

}
