package emgadm.services;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.commons.lang.StringUtils;

import com.moneygram.common.jms.JMSWriterCache;
import com.moneygram.common.jms.MessageWriter;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.util.StringHelper;
import emgshared.model.ConsumerEmail;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionType;

public class NotificationAccessImpl implements NotificationAccess {
	private static final Logger log = LogFactory.getInstance().getLogger(NotificationAccessImpl.class);
	public static final String NOTIFICATIONCONNECTIONFACTORY = "CONSUMER.NOTIFICATION.MSGING.REQ.QCF";
	public static final String NOTIFICATIONQUEUE = "CONSUMER.NOTIFICATION.MSGING.REQ";
	public static final String EMTQUEUE = "EMT.TO.MGOTXP.REQ";
	public static final String EMTQCONNECTIONFACTORY = "EMT.TO.MGOTXP.REQ.QCF";
	public static final String EMTQUEUEPREFIX = "jms/";
	private static final String SOURCE_SYSTEM = "MGO";
	
	/*
	 * TODO: These vars are hard-coded,but should be dynamic depending on the
	 * affiliate, the mechanism to make them dynamic is yet to be determined.
	 * Could be in the profiles.xml document, a database table, or possibly WAS
	 * resource environment variables.
	 */
	private static final String AFF_SENDER_EMAIL_ADDRESS = "no-reply.walmartmgo@moneygram.com";

	private static final String SENDER_EMAIL_ADDRESS = "no-reply.moneygramonline@moneygram.com";
	private static final String CONSUMER_SITE_URL = "https://www.moneygram.com/moneygramonline";
	private static final String CONSUMER_SITE_URL_UK = "https://www.moneygram.co.uk";
	private static final String HELP_LINE_NUMBER = "1-800-922-7146";
	private static final String HELP_LINE_NUMBER_DE = "0-800-606-6019";
	private static final String EMAILHEADER_LOGO_LINK = "http://www.moneygram.com/html/emailHeader.gif";
	private static final String AFFILIATE_NAME = "MoneyGram";
	private static final String AFFILIATE_SITE_NAME = "MoneyGramOnline";

	//SITES INDENTIFIERS
	public static final String SITEIDENTIFIER = "MGO";
	public static final String SITEIDENTIFIER_UK = "MGOUK";
    public static final String SITEIDENTIFIER_INLANE = "INLANE";
    public static final String SITEIDENTIFIER_WAP = "WAP"; // TODO is WAP correct?
    public static final String SITEIDENTIFIER_DE = "MGODE"; // TODO is WAP correct?

    //DELIVERY OPTIONS
    public static final String MGSEND = "MGSEND";
	public static final String DSSEND = "DSSEND";
	public static final String AFF_10_MINUTES ="10-Minute Transfer";
	public static final String AFF_4_HOURS ="4-Hour Transfer";

	/*
	 * TODO: needs to be dynamic at some point, admin site has no access to the
	 * user locale, it'll need to be stored when we go international (with the
	 * tran or with the profile??)
	 */
	// MESSAGE Country Locale
	private static final String COUNTRY_CODE = "USA";
	private static final String USER_LOCALE = "us-EN";
	// MESSAGE TYPES
	private static final String MSG_TYPE_TRNSND = "TRNSND";
	private static final String MSG_TYPE_TRNSNP = "TRNSNP";
	private static final String MSG_TYPE_PENDTRN = "PENTRN";
	private static final String MSG_TYPE_EXPAYTRN = "EXPTRN";
	private static final String MSG_TYPE_CCFAILTRN = "CCFTRN";
	private static final String MSG_TYPE_REFP2PTRN = "RPPTRN";
	private static final String MSG_TYPE_DENIEDTRN = "DENTRN";
	private static final String MSG_TYPE_ACHRETTRN = "ACHTRN";
	private static final String MSG_TYPE_NEEDINFOTRN = "INFTRN";
	private static final String AFF_MSG_TYPE_TRNSND = "ASNDTR";
	private static final String AFF_MSG_TYPE_PENDTRN = "APENTR";
	private static final String AFF_MSG_TYPE_CCFAILTRN = "ACCFTR";
	private static final String AFF_MSG_TYPE_REFP2PTRN = "ARPPTR";
	private static final String AFF_MSG_TYPE_DENIEDTRN = "ADENTR";
	private static final String AFF_MSG_TYPE_ACHRETTRN = "AACHTR";
	private static final String AFF_MSG_TYPE_NEEDINFOTRN = "AINFTR";
	 // ID RELATED MESSAGE TYPES
    private static final String ID_UPLOAD = "ID_UP";
    private static final String ID_PENDING_STATUS = "ID_PS";
    private static final String ID_DENIED_STATUS = "ID_DS";
    private static final String ID_APPROVED_WITHOUT_TX = "IDAPNT";
    private static final String ID_APPROVED_WITH_TX = "IDAPWT";

	// MESSAGE KEYS
	private static final String MSG_KEY_CONSUMER_SITE_URL = "consumerSiteURL";
	private static final String MSG_KEY_SENDER_NAME = "senderName";
	private static final String MSG_KEY_SENDER_FIRST_NAME = "firstName";
	private static final String MSG_KEY_RECEIVER_NAME = "receiverName";
	private static final String MSG_KEY_SEND_DATE = "sendDate";
	private static final String MSG_KEY_CONTACT_NUMBER = "contactNumber";
	private static final String MSG_KEY_THREE_MINUTE_PHONE_NBR = "threeMinutePhoneNbr";
	private static final String MSG_KEY_THREE_MINUTE_PIN_NBR = "threeMinutePinNbr";
	private static final String MSG_KEY_BILLER_NAME = "billerName";
	private static final String MSG_KEY_AFFILIATE_LOGO = "affiliateLogo";
	private static final String MSG_KEY_AFFILIATE_NAME = "affiliateName";
	private static final String MSG_KEY_AFFILIATE_SITE_NAME = "affiliateSiteName";
	private static final String MSG_KEY_SEND_AMOUNT = "sendAmount";
	private static final String MSG_KEY_FUNDING_TYPE = "fundingType";
	private static final String MSG_KEY_WAP_RECEIVE_PROVINCE = "province";
	private static final NumberFormat df = new DecimalFormat("#,##0.00");

	public void notifySentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage) {
		if(!(transaction.getDlvrOptnId()==DeliveryOption.CARD_DEPOSIT_ID || transaction.getDlvrOptnId()==DeliveryOption.BANK_DEPOSIT_ID)){
			if( (transaction.getThreeMinuteFreePhoneNumber()!= null &&!(transaction.getThreeMinuteFreePhoneNumber()).equals(""))&&
					(transaction.getThreeMinuteFreePinNumber()!= null &&!(transaction.getThreeMinuteFreePinNumber()).equals(""))){
				//if the consumer gets a free three minute phone call the message type is TRNSNP
				sendNotificationMessage(MSG_TYPE_TRNSNP, transaction, consumerEmail,preferedLanguage);
			}else{
				//if the consumer don't get a free three minute phone call the message type is TRNSND
				sendNotificationMessage(MSG_TYPE_TRNSND, transaction, consumerEmail,preferedLanguage);
			}
		}
	}

	public void notifyPendingTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_PENDTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyExpressPaymentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_EXPAYTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyCreditCardFailTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_CCFAILTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyRefundPersonToPersonTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_REFP2PTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyDeniedTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_DENIEDTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyACHReturnTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_ACHRETTRN, transaction, consumerEmail,preferedLanguage);
	}

	public void notify48hoursIdUpload(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_UPLOAD, transaction, consumerEmail,preferedLanguage);
	}

   	public void notifyIdPendingStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_PENDING_STATUS, transaction, consumerEmail,preferedLanguage);
	}

   	public void notifyIdDeniedStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_DENIED_STATUS, transaction, consumerEmail,preferedLanguage);
	}

   	public void notifyIdApprovedWithoutTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_APPROVED_WITHOUT_TX, transaction, consumerEmail,preferedLanguage);
	}


   	public void notifyApprovedWithTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(ID_APPROVED_WITH_TX, transaction, consumerEmail,preferedLanguage);
	}

	public void notifyNeedInfoTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		sendNotificationMessage(MSG_TYPE_NEEDINFOTRN, transaction, consumerEmail,preferedLanguage);
	}

	private void sendNotificationMessage(String messageType, Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage)
	{
		Session session=null;
		Connection conn=null;
		try {
			if (useReceiptQueue(messageType, transaction)) {
				InitialContext ctx=new InitialContext();
				ConnectionFactory connFactory=(javax.jms.ConnectionFactory)ctx.lookup(EMTQUEUEPREFIX+EMTQCONNECTIONFACTORY);
				Queue queue=(Queue) ctx.lookup(EMTQUEUEPREFIX+EMTQUEUE);
				conn=connFactory.createConnection();
				System.out.println("[sendNotificationMessage] created connection to EMTQUEUEPREFIX+EMTQUEUE");
				session=conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
				MessageProducer msgProducer=session.createProducer(queue);
				
				MapMessage mapMessage = session.createMapMessage();
                mapMessage.setLong("transactionId", transaction.getEmgTranId());
                mapMessage.setString("userLoginId", transaction.getSndCustLogonId());
                mapMessage.setString("transactionLanguageCode", preferedLanguage);
                mapMessage.setString("partnerSiteId", transaction.getPartnerSiteId());
                msgProducer.send(mapMessage);    
			} else {
				StringBuffer messageBuffer = new StringBuffer();
				messageBuffer.append("<MessageInfo>");
				messageBuffer.append(getMessageType(messageType, transaction,preferedLanguage));
				messageBuffer.append(getServices(consumerEmail.getConsumerEmail(), transaction));
				messageBuffer.append(getAdmEmailContentParms(transaction));
				messageBuffer.append("</MessageInfo>");
				String message = messageBuffer.toString();
				writeMessageToQueue(message);
			}
			
		} catch (Exception exception) {
			log.error("Error sending email: " + messageType, exception);
		} finally {
			try {
				session.close();
				conn.close();
			} catch (Exception e) {
				log.error("Error closing session and connection"+ EMTQUEUEPREFIX+EMTQCONNECTIONFACTORY + "MessageType: " + messageType, e);
			}
			
		}
	}
	
	private boolean useReceiptQueue(String messageType, Transaction transaction) {

	  return EMTAdmContainerProperties.isUseReceiptQueue() &&
	      (messageType.equals(MSG_TYPE_TRNSNP) ||
	       messageType.equals(MSG_TYPE_TRNSND));
    }

	private String getAdmEmailContentParms(Transaction transaction) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d hh:mm:ss a z yyyy");
		String tranDateFormatted = String.valueOf(dateFormat.format(transaction.getSndTranDate()));
		String senderName = buildFullName(transaction.getSndCustFrstName(), null, transaction.getSndCustLastName());
		String receiverName = buildFullName(transaction.getRcvCustFrstName(), transaction.getRcvCustMidName(), transaction
				.getRcvCustLastName());
		String siteUrl = CONSUMER_SITE_URL;
		if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_UK)) {
			siteUrl = CONSUMER_SITE_URL_UK;
		}
		String helpLineNumber = HELP_LINE_NUMBER;
		if(transaction.getPartnerSiteId().equals(SITEIDENTIFIER_DE)){
			helpLineNumber = HELP_LINE_NUMBER_DE;
		}
		StringBuffer messageBuffer = new StringBuffer();
		try {
			messageBuffer.append("<contentParameters>");
			messageBuffer.append(getContentParameter(MSG_KEY_SEND_DATE, tranDateFormatted));
			messageBuffer.append(getContentParameter(MSG_KEY_RECEIVER_NAME, receiverName));
			messageBuffer.append(getContentParameter(MSG_KEY_SENDER_NAME, senderName));
			messageBuffer.append(getContentParameter(MSG_KEY_SENDER_FIRST_NAME, transaction.getSndCustFrstName()));
			//messageBuffer.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, CONSUMER_SITE_URL));
			messageBuffer.append(getContentParameter(MSG_KEY_CONSUMER_SITE_URL, siteUrl));
			messageBuffer.append(getContentParameter(MSG_KEY_CONTACT_NUMBER, helpLineNumber));
			if (transaction.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.EXPRESS_PAYMENT_SEND_CODE))
				messageBuffer.append(getContentParameter(MSG_KEY_BILLER_NAME, transaction.getRcvAgentName()));
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_LOGO, EMAILHEADER_LOGO_LINK));
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_NAME, AFFILIATE_NAME));
			messageBuffer.append(getContentParameter(MSG_KEY_SEND_AMOUNT, df.format(transaction.getSndFaceAmt().doubleValue())));
			messageBuffer.append(getContentParameter(MSG_KEY_FUNDING_TYPE, getFundingType(transaction)));
			messageBuffer.append(getContentParameter(MSG_KEY_AFFILIATE_SITE_NAME, AFFILIATE_SITE_NAME));
			messageBuffer.append(getContentParameter(MSG_KEY_WAP_RECEIVE_PROVINCE, getReceiveProvince(transaction)));
			messageBuffer.append(getContentParameter(MSG_KEY_THREE_MINUTE_PHONE_NBR, transaction.getThreeMinuteFreePhoneNumber()));
			messageBuffer.append(getContentParameter(MSG_KEY_THREE_MINUTE_PIN_NBR, transaction.getThreeMinuteFreePinNumber()));


			messageBuffer.append("</contentParameters>");
		} catch (Exception e) {
			log.error("error in getAdmEmailContentParms:" + e.getLocalizedMessage());
			throw e;
		}
		return messageBuffer.toString();
	}

	private String getReceiveProvince(Transaction tran) {
		if (tran.getRcvISOCntryCode().equals("USA")) {
			return tran.getIntndDestStateProvinceCode();
		} else if (tran.getPartnerSiteId().equals("WAP")){
			return "Puerto Rico";
		} else {
			return tran.getRcvISOCntryCode();
		}
	}

	private String getFundingType(Transaction transaction)
	{
		String mgoProductType = transaction.getEmgTranTypeDesc();
		if (isAffiliateSite(transaction))
		{
			if(mgoProductType.equals(MGSEND))
			{
				return AFF_10_MINUTES;
			}
			else if( mgoProductType.equals(DSSEND))
			{
				return AFF_4_HOURS;
			}
		}
		return mgoProductType;
	}

	private String getContentParameter(String key, String value) {
		StringBuffer contentParameter = new StringBuffer();
		contentParameter.append("<contentParameter>");
		contentParameter.append("<name>");
		contentParameter.append(key);
		contentParameter.append("</name>");
		contentParameter.append("<value>");
		contentParameter.append(value);
		contentParameter.append("</value>");
		contentParameter.append("</contentParameter>");
		return contentParameter.toString();
	}

	private boolean isAffiliateSite(Transaction transaction)
    {
		String siteId = transaction.getPartnerSiteId();
		if (siteId != null && siteId.equals(SITEIDENTIFIER_WAP))
    	//if (siteId != null && !siteId.equals(SITEIDENTIFIER) && !siteId.equals(SITEIDENTIFIER_INLANE) && !siteId.equals(SITEIDENTIFIER_UK))
    	{
    		return true;
    	}
    	return false;
    }

	private String getMessageType(String messageTypeCode, Transaction transaction, String preferedLanguage)
    {
    	/* TODO: MGO database doesn't store the users locale.  Once MGO goes multi-lingual or international,
    	 * the locale will need to be stored with the consumer profile or potentially with each transaction.
    	 */
       // MGOLocale currentLocale = mgoConsumer.getMGOLocale();
    	if (isAffiliateSite(transaction))
    	{
    		String oldMsgType = messageTypeCode;
    		messageTypeCode = getAffiliateMessageType(messageTypeCode);
    		log.debug("Affiliate Program - Old MessageType: " + oldMsgType + ", New MessageType: " + messageTypeCode);
    	}
    	//locale should eventually be stored in DB, but for now either use en-US(MGO USA) or en-GB (MGO UK)
    	String locale = "en-US";
    	String country = "USA";
    	if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_UK)) {
    		locale = "en-GB";
    		country = "GBR";
    	}
    	if (transaction.getPartnerSiteId().equals(SITEIDENTIFIER_DE)) {
    		locale = preferedLanguage;
    		country = "DEU";
    	}
        StringBuffer messageType = new StringBuffer();
        messageType.append("<messageType>");
            messageType.append("<type>");
            messageType.append(messageTypeCode);
            messageType.append("</type>");
            messageType.append("<subtype>1</subtype>");
            messageType.append("<sourceSystem>" + SOURCE_SYSTEM + "</sourceSystem>");
            messageType.append("<countryCode>");
            //messageType.append(currentLocale.getISO3Country());
           // messageType.append("USA");
            messageType.append( country );
            messageType.append("</countryCode>");
            messageType.append("<languageCode>");
            //messageType.append(currentLocale.getISO5Language());
            //messageType.append("en-US");
            messageType.append(locale);
            messageType.append("</languageCode>");
        messageType.append("</messageType>");
        return messageType.toString();
    }

    private String getAffiliateMessageType(String msgType) {

    	if (msgType.equals(MSG_TYPE_TRNSND)) {
    		return AFF_MSG_TYPE_TRNSND;
    	}
    	if (msgType.equals(MSG_TYPE_TRNSNP)) {
    		return AFF_MSG_TYPE_TRNSND;
    	}
    	if (msgType.equals(MSG_TYPE_PENDTRN)) {
    		return AFF_MSG_TYPE_PENDTRN;
    	}
    	if (msgType.equals(MSG_TYPE_CCFAILTRN)) {
    		return AFF_MSG_TYPE_CCFAILTRN;
    	}
    	if (msgType.equals(MSG_TYPE_REFP2PTRN)) {
    		return AFF_MSG_TYPE_REFP2PTRN;
    	}
    	if (msgType.equals(MSG_TYPE_DENIEDTRN)) {
    		return AFF_MSG_TYPE_DENIEDTRN;
    	}
    	if (msgType.equals(MSG_TYPE_ACHRETTRN)) {
    		return AFF_MSG_TYPE_ACHRETTRN;
    	}
    	if (msgType.equals(MSG_TYPE_NEEDINFOTRN)) {
    		return AFF_MSG_TYPE_NEEDINFOTRN;
    	}
    	return msgType;
    }

	private String getServices(String receiverEmailAddress, Transaction transaction) {
		StringBuffer services = new StringBuffer();
		services.append("<services>");
		services.append("<service type=\"E-MAIL\">");
		services.append("<sender>");
		// services.append(MGOServiceUtil.getContextResourceVariable(
		// MGOServiceUtil.SENDEREMAILADDRESS));
		if (isAffiliateSite(transaction)) {
			services.append(AFF_SENDER_EMAIL_ADDRESS);
		}
		else {
			services.append(SENDER_EMAIL_ADDRESS);
		}
		services.append("</sender>");
		services.append("<receiver>");
		services.append(receiverEmailAddress);
		services.append("</receiver>");
		services.append("</service>");
		services.append("</services>");
		return services.toString();
	}

	private void writeMessageToQueue(MapMessage message) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeMessageToQueue: message=" + StringUtils.left(message.toString(), 1000) + "... length=" + message.toString().length());
		}
		try {
			MessageWriter writer=null;
			writer= JMSWriterCache.getInstance().getWriter(EMTQCONNECTIONFACTORY, EMTQUEUE, false);
			writer.writeObjectMessage((Serializable) message);
		} catch (Exception e) {
			log.error("Error in writing to the Message Queue: " + e.getLocalizedMessage());
			Exception msgException = new Exception("Error in writing to the Message Queue:" + e.getLocalizedMessage());
			throw msgException;
		}
	}

	private void writeMessageToQueue(String message) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("writeMessageToQueue: message=" + StringUtils.left(message, 1000) + "... length=" + message.length());
		}
		try {
			MessageWriter writer=null;
			writer= JMSWriterCache.getInstance().getWriter(NOTIFICATIONCONNECTIONFACTORY, NOTIFICATIONQUEUE, false);	
			
			writer.writeTextMessage(message);
		} catch (Exception e) {
			log.error("Error in writing to the Message Queue: " + e.getLocalizedMessage());
			Exception msgException = new Exception("Error in writing to the Message Queue:" + e.getLocalizedMessage());
			throw msgException;
		}
	}

	private String buildFullName(String firstName, String middleName, String lastName) {
		if (!StringHelper.isNullOrEmpty(middleName)) {
			return firstName + " " + middleName + " " + lastName;
		} else {
			return firstName + " " + lastName;
		}
	}
}
