package emgadm.services.iplookup.melissadata.client;

import java.util.StringTokenizer;

import org.apache.axis.AxisProperties;

public class Tester {
	 //TODO Removed unwanted entry
//	public static void main(String[] args) throws IPLocatorException {
//		// AxisProperties.setProperty("axis.socketSecureFactory","org.apache.axis.components.net.SunFakeTrustSocketFactory");
//		ResponseArray response = IPLocatorService.ipLocate("190.26.216.234",
//				"216.231.3.166", "12.143.150.173");
//		ResponseRecord[] resArray = response.getRecord();
//		// com.ibm.websphere.ssl.protocol.SSLSocketFactory a;
//
//		System.out.println("");
//		System.out.println("IPLocator Response");
//		System.out.println("----------------------------------------");
//
//		// If this method returns true, that means it printed an error,
//		// therefore, stop the process.
//		if (printGlobalResults(response.getResults())) {
//			return;
//		}
//
//		for (int i = 0; i < resArray.length; i++) {
//			// System.out.println("RECORD NUMBER: "
//			// + resArray[i].getRecordID());
//			// System.out.println("Result Codes: " + resArray[i].getResults());
//
//			// Print out results description
//			printResultDesc(resArray[i].getResults());
//
//			// Continue outputing results onto the console
//			// System.out.println("Latitude: "
//			// + resArray[i].getIP().getLatitude());
//			// System.out.println("Longitude: "
//			// + resArray[i].getIP().getLongitude());
//			// System.out.println("Zip: " + resArray[i].getIP().getZip());
//			// System.out
//			// .println("Region: " + resArray[i].getIP().getRegion());
//			// System.out.println("ISP Name: "
//			// + resArray[i].getIP().getISP().getName());
//			// System.out.println("Domain Name: "
//			// + resArray[i].getIP().getDomain().getName());
//			// System.out.println("City Name: "
//			// + resArray[i].getIP().getCity().getName());
//			// System.out.println("Country Name: "
//			// + resArray[i].getIP().getCountry().getName());
//			// System.out.println("");
//		}
//	}

	// ***********************************************
	// Method to tokenize global result code list
	// and output the equivalent result
	// description to console
	// ***********************************************
	private static boolean printGlobalResults(String resultCode) {
		StringTokenizer scanner;
		scanner = new StringTokenizer(resultCode, ",");

		System.out.println("Global Results: ");

		if (!resultCode.equals(" ")) {
			while (scanner.hasMoreTokens()) {
				String resultCodeElement = scanner.nextToken();

				// Verified Section
				if (resultCodeElement.equals("SE01")) {
					System.out.println("\t SE01: "
							+ "Web Servoce Internal Error");
				}
				if (resultCodeElement.equals("GE01")) {
					System.out.println("\t GE01: "
							+ "Empty XML Request Structure");
				}
				if (resultCodeElement.equals("GE02")) {
					System.out.println("\t GE02: "
							+ "Empty XML Request Record Structure");
				}
				if (resultCodeElement.equals("GE03")) {
					System.out.println("\t GE03: "
							+ "Inserted Records Exceed Maximum Amount");
				}
				if (resultCodeElement.equals("GE04")) {
					System.out.println("\t GE04: " + "Customer ID Empty");
				}
				if (resultCodeElement.equals("GE05")) {
					System.out.println("\t GE05: " + "Customer ID Not Valid");
					System.out
							.println("\t The Customer ID your entered is invalid. To retrieve a valid");
					System.out
							.println("\t Customer ID, please contact a Melissa Data Sales Representative");
					System.out
							.println("\t at 800-MELISSA ext. 3 (800-800-6245 ext. 3).");
				}
				if (resultCodeElement.equals("GE06")) {
					System.out.println("\t GE06: " + "Customer ID Disabled");
					System.out
							.println("\t The Customer ID your entered is invalid. To retrieve a valid");
					System.out
							.println("\t Customer ID, please contact a Melissa Data Sales Representative");
					System.out
							.println("\t at 800-MELISSA ext. 3 (800-800-6245 ext. 3).");
				}
				if (resultCodeElement.equals("GE07")) {
					System.out.println("\t GE07: " + "XML Request Invalid");
				}
				return true;
			}
		}
		return false;
	}

	// ***********************************************
	// Method to tokenize result code list
	// and output the equivalent result
	// description to console
	// ***********************************************
	private static void printResultDesc(String resultCode) {
		StringTokenizer scanner;
		scanner = new StringTokenizer(resultCode, ",");

		System.out.println("Result Description List: ");

		while (scanner.hasMoreTokens()) {
			String resultCodeElement = scanner.nextToken();

			// Verified Section
			if (resultCodeElement.equals("AS01")) {
				System.out.println("\t AS01: "
						+ "Address Matched to a postal patabase");
			}
			if (resultCodeElement.equals("AS02")) {
				System.out.println("\t AS02: "
						+ " Address matched to a non-postal databasee");
			}
			if (resultCodeElement.equals("AS10")) {
				System.out.println("\t AS10: " + "Address Matched to CMRA");
			}
			if (resultCodeElement.equals("AS11")) {
				System.out.println("\t AS11: " + "Address Vacant");
			}
			if (resultCodeElement.equals("AS12")) {
				System.out.println("\t AS12: " + "Address Matched to DPV");
			}
			if (resultCodeElement.equals("AS13")) {
				System.out.println("\t AS13: " + "Address updated by LACS");
			}

			// Errors Section
			if (resultCodeElement.equals("AE01")) {
				System.out.println("\t AE01: " + "Zip Code Error");
			}
			if (resultCodeElement.equals("AE02")) {
				System.out.println("\t AE02: " + "Unknown Street");
			}
			if (resultCodeElement.equals("AE03")) {
				System.out.println("\t AE03: " + "Component Error");
			}
			if (resultCodeElement.equals("AE04")) {
				System.out.println("\t AE04: " + "Non-Deliverable Address");
			}
			if (resultCodeElement.equals("AE05")) {
				System.out.println("\t AE05: "
						+ "Address matched to multiple records");
			}
			if (resultCodeElement.equals("AE06")) {
				System.out.println("\t AE06: "
						+ "Address Matched to Early Warning System");
			}
			if (resultCodeElement.equals("AE07")) {
				System.out.println("\t AE07: " + "Empty Address Input");
			}
			if (resultCodeElement.equals("AE08")) {
				System.out.println("\t AE08: " + "Suite Range Error");
			}
			if (resultCodeElement.equals("AE09")) {
				System.out.println("\t AE09: " + "Suite Range Missing");
			}
			if (resultCodeElement.equals("AE10")) {
				System.out.println("\t AE10: " + "Primary Rnage Error");
			}
			if (resultCodeElement.equals("AE11")) {
				System.out.println("\t AE11: " + "Primary Range Missing");
			}
			if (resultCodeElement.equals("AE12")) {
				System.out.println("\t AE12: "
						+ "Box Number Error from PO Box or RR");
			}
			if (resultCodeElement.equals("AE13")) {
				System.out.println("\t AE13: "
						+ "PO Box Number Missing from PO Box or RR");
			}
			if (resultCodeElement.equals("AE14")) {
				System.out
						.println("\t AE14: "
								+ "Input Address Matched to CMRA but secondary number not present");
			}
		}
	}

}