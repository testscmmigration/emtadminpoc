package emgadm.services.iplookup.melissadata.client;


import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import emgadm.property.EMTAdmContainerProperties;
import emgshared.model.IPDetails;

public class IPLocatorService {

	static RequestArray request;
	static ResponseArray response;
	static IService mdWebService;
	static List<RequestRecord> reqArray;
	
	private IPLocatorService() {
	}

	private static void initialize() {
		request = new RequestArray();
		response = new ResponseArray();
		mdWebService = null;
		reqArray =  new LinkedList<RequestRecord>();
		request.setCustomerID(EMTAdmContainerProperties.getIpLookUpCustomerID());
		request.setTransmissionReference("MoneyGram IP location");
		
	}

	private static ResponseArray doIpLocator(boolean isIpLookuponScoring) throws IPLocatorException {
		try {

			ServiceLocator locator = new ServiceLocator();
			int timeout = 0;
			request.setRecord((RequestRecord[]) reqArray.toArray(new RequestRecord[reqArray.size()]));
			if(isIpLookuponScoring){
				timeout = (int) EMTAdmContainerProperties.getIpLookUpTimeOutOnScoring();
			} else {
				timeout = (int) EMTAdmContainerProperties.getIpLookUpTimeOutOnLookUpIPAddress();
			}
			try {
				String url = EMTAdmContainerProperties.getIpLookUpServiceURL();
				mdWebService = locator.getBasicHttpBinding_IService((new URL(
						url)), timeout);

			} catch (ServiceException e) {
				throw new IPLocatorException("Service problems while doing IP location", e);
			} catch (MalformedURLException e) {
				throw new IPLocatorException("Wrong URL to invoke IP location service", e);
			}

			try {
				response = mdWebService.doIPLocation(request,timeout);
				
				
			} catch (RemoteException e) {
				throw new IPLocatorException("Problems on the server while executing IP location", e);
			}
			return response;

		} catch (Exception e) {
			throw new IPLocatorException(e);
		}

	}

	private static void insertRecord(RequestRecord record) {
		reqArray.add(record);
	}

	/**
	 * Obtains information about specified IP addesses
	 * @param ipAddresses the IP addresses to check
	 * @return a response array container with location information about each address received
	 * @throws IPLocatorException in case something fails during execution of the service
	 */
	public static ResponseArray ipLocate(boolean isIpLookuponScoring, String... ipAddresses) throws IPLocatorException {
		initialize();
		for (String ipAddress : ipAddresses) {
			RequestRecord requestRecord = new RequestRecord();
			requestRecord.setIPAddress(ipAddress);
			insertRecord(requestRecord);
		}
		return doIpLocator(isIpLookuponScoring);
	}

	public static String getFormattedStringForIpLookup(String ipAddress, boolean isIpLookuponScoring ) {
		if (ipAddress == null) {
			return "Error: No IP recorder for transaction";
		}
		StringBuilder message = new StringBuilder();
		try {
			ResponseArray response = IPLocatorService.ipLocate(isIpLookuponScoring, ipAddress);
			ResponseRecord record = response.getRecord(0);
			message.append("IP Address: " + ipAddress);
			message.append(", Country: " + record.getIP().getCountry().getName());
			message.append(", Region: " + record.getIP().getRegion());
			message.append(", City: " + record.getIP().getCity().getName());
			message.append(", Zip Code: " + record.getIP().getZip());
			message.append(", Provider: " + record.getIP().getISP().getName());
	    } catch(Exception e) {
	    	message.append("N/A");
	    	// TODO: Log
	    }
	    return message.toString();
	}
	public static IPDetails getIPDetailsFromLookupService(String ipAddress, boolean isIpLookuponScoring) {

		IPDetails ipDetails = new IPDetails();
		ResponseArray response = null;
		try {
			response = IPLocatorService.ipLocate(isIpLookuponScoring, ipAddress );
		} catch (IPLocatorException iPLocatorException) {
			ipDetails.setConsumerCity("System Error");
			ipDetails.setConsumerCountry("System Error");
			ipDetails.setConsumerDomain("System Error");
			ipDetails.setConsumerISP("System Error");
			ipDetails.setConsumerRegion("System Error");
			ipDetails.setConsumerZipCode("System Error");
			ipDetails.setCountryCode("System Error");
			return ipDetails;
		}

		if (response != null && response.getRecord(0) != null) {
			ResponseRecord responseRecord = response.getRecord(0);
			ipDetails.setConsumerISP(responseRecord.getIP().getISP().getName());
			ipDetails.setConsumerDomain(responseRecord.getIP().getDomain()
					.getName());
			ipDetails.setConsumerCountry(responseRecord.getIP().getCountry()
					.getName());
			ipDetails.setConsumerCity(responseRecord.getIP().getCity()
					.getName());
			ipDetails.setConsumerZipCode(responseRecord.getIP().getZip());
			if(responseRecord.getIP().getRegion()==null){
				ipDetails.setConsumerRegion("");
			} else {
				ipDetails.setConsumerRegion(responseRecord.getIP().getRegion());
			}
			ipDetails.setCountryCode(responseRecord.getIP().getCountry()
					.getAbbreviation());
		} else {
			ipDetails.setConsumerCity("System Error");
			ipDetails.setConsumerCountry("System Error");
			ipDetails.setConsumerDomain("System Error");
			ipDetails.setConsumerISP("System Error");
			ipDetails.setConsumerRegion("System Error");
			ipDetails.setConsumerZipCode("System Error");
			ipDetails.setCountryCode("System Error");
		}
		
		return ipDetails;
	}

}

