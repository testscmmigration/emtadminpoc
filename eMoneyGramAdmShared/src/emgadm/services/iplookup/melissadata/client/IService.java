/**
 * IService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public interface IService extends java.rmi.Remote {
    public emgadm.services.iplookup.melissadata.client.ResponseArray doIPLocation(emgadm.services.iplookup.melissadata.client.RequestArray request, int timeout) throws java.rmi.RemoteException;
}
