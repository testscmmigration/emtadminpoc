/**
 * Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public interface Service extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_IServiceAddress();

    public emgadm.services.iplookup.melissadata.client.IService getBasicHttpBinding_IService() throws javax.xml.rpc.ServiceException;

    public emgadm.services.iplookup.melissadata.client.IService getBasicHttpBinding_IService(java.net.URL portAddress,int timeout) throws javax.xml.rpc.ServiceException;
}
