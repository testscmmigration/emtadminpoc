/**
 * ResponseArrayRecord.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package emgadm.services.iplookup.melissadata.client;

public class ResponseRecord  implements java.io.Serializable {
    private java.lang.String recordID;

    private java.lang.String results;

    private emgadm.services.iplookup.melissadata.client.IP IP;

    public ResponseRecord() {
    }

    public ResponseRecord(
           java.lang.String recordID,
           java.lang.String results,
           emgadm.services.iplookup.melissadata.client.IP IP) {
           this.recordID = recordID;
           this.results = results;
           this.IP = IP;
    }


    /**
     * Gets the recordID value for this ResponseArrayRecord.
     * 
     * @return recordID
     */
    public java.lang.String getRecordID() {
        return recordID;
    }


    /**
     * Sets the recordID value for this ResponseArrayRecord.
     * 
     * @param recordID
     */
    public void setRecordID(java.lang.String recordID) {
        this.recordID = recordID;
    }


    /**
     * Gets the results value for this ResponseArrayRecord.
     * 
     * @return results
     */
    public java.lang.String getResults() {
        return results;
    }


    /**
     * Sets the results value for this ResponseArrayRecord.
     * 
     * @param results
     */
    public void setResults(java.lang.String results) {
        this.results = results;
    }


    /**
     * Gets the IP value for this ResponseArrayRecord.
     * 
     * @return IP
     */
    public emgadm.services.iplookup.melissadata.client.IP getIP() {
        return IP;
    }


    /**
     * Sets the IP value for this ResponseArrayRecord.
     * 
     * @param IP
     */
    public void setIP(emgadm.services.iplookup.melissadata.client.IP IP) {
        this.IP = IP;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResponseRecord)) return false;
        ResponseRecord other = (ResponseRecord) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordID==null && other.getRecordID()==null) || 
             (this.recordID!=null &&
              this.recordID.equals(other.getRecordID()))) &&
            ((this.results==null && other.getResults()==null) || 
             (this.results!=null &&
              this.results.equals(other.getResults()))) &&
            ((this.IP==null && other.getIP()==null) || 
             (this.IP!=null &&
              this.IP.equals(other.getIP())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordID() != null) {
            _hashCode += getRecordID().hashCode();
        }
        if (getResults() != null) {
            _hashCode += getResults().hashCode();
        }
        if (getIP() != null) {
            _hashCode += getIP().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResponseRecord.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">ResponseArray>Record"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "RecordID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("results");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "Results"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IP");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", "IP"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:mdWebServiceIPLocation", ">>ResponseArray>Record>IP"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
