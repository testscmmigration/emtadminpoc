/*
 * Created on Feb 11, 2005
 *
 */
package emgadm.services.processing;


/**
 * @author A131
 *
 */
public interface UpdateTransactionCommand
{
	public Object execute();
}
