/*
 * Created on May 3, 2010
 *
 */
package emgadm.services;
import java.util.Map;

import emgshared.model.ConsumerProfile;
public interface DVSService {

    /**
     * Validates the consumer using DVS.
     * @param 
     * @return consumer with error messages filled.
     * @throws ProxyException
     */
    public Map validateConsumer(ConsumerProfile consumer) throws Exception;
}
