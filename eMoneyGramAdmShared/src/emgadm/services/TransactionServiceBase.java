package emgadm.services;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.jms.DeliveryMode;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.util.MessageResources;

import shared.mgo.dto.MGOAgentProfile;
import shared.mgo.dto.MGOProductFieldInfo;
import shared.mgo.services.AbstractAgentConnectAccessor;
import shared.mgo.services.AbstractAgentConnectAccessorDoddFrank;
import shared.mgo.services.AgentConnectAccessor;
import shared.mgo.services.AgentConnectAccessorDoddFrank;
import shared.mgo.services.AgentConnectService;
import shared.mgo.services.AgentConnectServiceDoddFrank;
import shared.mgo.services.MGOAgentProfileService;
import shared.mgo.services.MGOServiceFactory;

import com.moneygram.acclient.BillPaymentSendRequest;
import com.moneygram.acclient.FeeLookupResponse;
import com.moneygram.acclient.LegalIdType;
import com.moneygram.acclient.MoneyGramSendRequest;
import com.moneygram.acclient.MoneyGramSendResponse;
import com.moneygram.acclient.PhotoIdType;
import com.moneygram.acclient.ProductType;
import com.moneygram.acclient.ProductVariant;
import com.moneygram.agentconnect1305.wsclient.BillerInfo;
import com.moneygram.agentconnect1305.wsclient.BpValidationRequest;
import com.moneygram.agentconnect1305.wsclient.BpValidationResponse;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionRequest;
import com.moneygram.agentconnect1305.wsclient.CommitTransactionResponse;
import com.moneygram.agentconnect1305.wsclient.Error;
import com.moneygram.agentconnect1305.wsclient.SendReversalType;
import com.moneygram.agentconnect1305.wsclient.TextTranslation;
import com.moneygram.common.util.StringUtility;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.dataaccessors.UserProfileManager;
import emgadm.exceptions.BankPaymentServiceException;
import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.CustAddress;
import emgadm.model.DeadTransaction;
import emgadm.model.GlAccount;
import emgadm.model.TranAction;
import emgadm.model.UserProfile;
import emgadm.model.UserQueryCriterion;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.bankpayment.AdjustmentInquiryRequestMessage;
import emgadm.services.bankpayment.AdjustmentInquiryResponseMessage;
import emgadm.services.bankpayment.BankPaymentProxy;
import emgadm.services.bankpayment.BankPaymentProxyImpl;
import emgadm.services.bankpayment.StatusInquiryRequestMessage;
import emgadm.services.bankpayment.StatusInquiryResponseMessage;
import emgadm.services.processing.UpdateTransactionCommand;
import emgadm.sysmon.EventListConstant;
import emgadm.util.StringHelper;
import emgshared.dataaccessors.EMGSharedLogger;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.AdminMessage;
import emgshared.model.BillerLimit;
import emgshared.model.ConsumerAccount;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.DeliveryOption;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedContainerProperties;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.CommentService;
import emgshared.services.ConsumerAccountService;
import emgshared.services.ConsumerProfileService;
import emgshared.services.MessageService;
import emgshared.services.PCIService;
import emgshared.util.Constants;
import emgshared.util.DateFormatter;
import emgshared.util.I18NHelper;
import emgshared.util.MailHelper;
import eventmon.eventmonitor.EventMonitor;


abstract class TransactionServiceBase implements TransactionService {

	protected static final TransactionManager tranManager = ManagerFactory.createTransactionManager();
	protected static final MailService mailService = ServiceFactory.getInstance().getMailService();
	protected static final ConsumerAccountService accountService = emgshared.services.ServiceFactory.getInstance()
	.getConsumerAccountService();
	protected static final ConsumerProfileService profileService = emgshared.services.ServiceFactory.getInstance()
	.getConsumerProfileService();
	protected static final PCIService pciService = emgshared.services.ServiceFactory.getInstance().getPCIService();
	protected static final DecryptionService decryptService = emgadm.services.ServiceFactory.getInstance().getDecryptionService();
	protected static final MessageService messageService = emgshared.services.ServiceFactory.getInstance().getMessageService();
	protected static final Set transactionsInProcess = new HashSet();
	protected static final String resourceFile = "emgadm.resources.ApplicationResources";
	// private static final String ccAuthConfReasonCode = "28";
	protected static final String ccFeeConfReasonCode = "131";
	protected static final String ccFundedConfReasonCode = "140";

	protected static final int PAYMENT_STATUS_PENDING   = 600;
	protected static final int PAYMENT_STATUS_READY     = 800;
	protected static final int PAYMENT_STATUS_PROCESSED = 900;

	protected static final String DECISION_ACCEPT = "accept";
	protected static final String DECISION_REJECT = "reject";

	protected EMTSharedDynProperties eadp = new EMTSharedDynProperties();


	public static TransactionService getInstance(String partnerSiteId) {
		if(EMTAdmContainerProperties.getUseGCForProcessing()
				|| Constants.MGOUK_PARTNER_SITE_ID.equals(partnerSiteId)
				|| Constants.MGODE_PARTNER_SITE_ID.equals(partnerSiteId)) {
			System.out.println("********** Using GlobalCollect");
			return TransactionServiceGlobalCollect.getInstance();
		} else {
			System.out.println("********** Using Cybersource");
			return TransactionServiceCyberSource.getInstance();
		}
	}

	/**
	 * This method will replace the getInstance method. This method will return the
	 * instance of cybersource or global collect transaction according the container
	 * property USE_GC_for_processig
	 *
	 * @param partnerSiteId
	 * @return
	 */
	public static TransactionService getInstanceGlobalCollectFlag() {

		boolean useGlobalCollecforProcessing = EMTAdmContainerProperties.getUseGCForProcessing();
		if(useGlobalCollecforProcessing) {
			return TransactionServiceGlobalCollect.getInstance();
		}
		else {
			return TransactionServiceCyberSource.getInstance();
		}
	}

	/**
	 * This method will replace the getInstance method. This method will return the
	 * instance of cybersource or global collect transaction according the container
	 * property USE_GC_for_processig
	 *
	 * @param partnerSiteId
	 * @return
	 */
	public static TransactionService getInstanceTmp(Integer pymtSrvProvCode, String partnerSiteId) {

		if(pymtSrvProvCode != null) {
			if(partnerSiteId.equals(EMoneyGramAdmApplicationConstants.MGOUK_PARTNER_SITE_ID)
					|| partnerSiteId.equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)
					|| pymtSrvProvCode == EMoneyGramAdmApplicationConstants.PYMT_PROVIDER_CODE_GLOBAL_COLLECT) {
					return TransactionServiceGlobalCollect.getInstance();
			} else {
					return TransactionServiceCyberSource.getInstance();
			}
		}
		else {
				return TransactionServiceCyberSource.getInstance();
		}
	}

	private static boolean isSameDay(Calendar a, Calendar b) {
		return (a.get(Calendar.YEAR) == b.get(Calendar.YEAR) &&
				a.get(Calendar.MONTH) == b.get(Calendar.MONTH) &&
				a.get(Calendar.DAY_OF_MONTH) == b.get(Calendar.DAY_OF_MONTH));
	}

	protected TransactionServiceBase() {
		super();
	}

	public void requestOwnership(final int tranId, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory.createTransactionManager();
				manager.setTransactionOwnership(tranId, userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, false);
	}

	public void takeOverOwnership(final int tranId, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory.createTransactionManager();
				manager.setTransactionOwnership(tranId, userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, false, false);
	}

	public void releaseOwnership(final int tranId, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory.createTransactionManager();
				manager.setTransactionOwnership(tranId, null);
				return null;
			}
		};
		updateTransaction(tranId, userId, command);
	}

	public void setTransactionStatus(final int tranId, final String tranStatus, final String fundStatus, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				TransactionManager manager = ManagerFactory.createTransactionManager();
				manager.setTransactionStatus(tranId, tranStatus, fundStatus, userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}


	public void recordLossAdjustment(final int tranId, final float amount, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		recordManualAdjustment(tranId, amount, TransactionStatus.MANUAL_RECORD_LOSS_REVENUE, userId);
	}

	public void recordFeeAdjustment(final int tranId, final float amount, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		recordManualAdjustment(tranId, amount, TransactionStatus.MANUAL_RECORD_FEE_REVENUE, userId);
	}

	public void recordConsumerRefundOrRemittanceAdjustment(final int tranId, final float amount, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		recordManualAdjustment(tranId, amount, TransactionStatus.MANUAL_RECORD_CONSUMER_REFUND_OR_REMITTANCE, userId);
	}

	public void recordMgActivityAdjustment(final int tranId, final float amount, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		recordManualAdjustment(tranId, amount, TransactionStatus.MANUAL_RECORD_MG_ACTIVITY, userId);
	}

	private void recordManualAdjustment(final int tranId, final float amount, final TransactionStatus transientStatus, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				if (tran.isManualAdjustable() == false) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a manually adjustable state");
				}
				TransactionManager manager = ManagerFactory.createTransactionManager();
				// Change the status so the stored procedure code records
				// ledger entries properly. For the moment, we call
				// update transaction as opposed to set transaction status
				// because set transaction status does not accept an amount.
				tran.setStatus(transientStatus);
				manager.updateTransaction(tran, userId, null, new Float(amount));
				// Change the status to generic manual because the
				// preceding status is considered a transient status.
				tran.setStatus(TransactionStatus.MANUAL_MANUAL);
				manager.setTransactionStatus(tran.getEmgTranId(), tran.getStatus(), userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	public void returnFee(final int tranId, final float amount, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	public void collectFee(final int tranId, final float amount, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	public void returnFunding(final int tranId, final float amount, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		moveToManualWithAdjustment(tranId, new Float(amount), userId);
	}

	private void moveToManualWithAdjustment(final int tranId, final Float amount, final String userId)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				TransactionManager manager = ManagerFactory.createTransactionManager();
				tran.setStatus(TransactionStatus.MANUAL_MANUAL);
				manager.updateTransaction(tran, userId, null, amount);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	/*
	 * currently the resubmission and the charging of fee is performed manually.
	 * this method is simply getting the accounting entries in sync with what
	 * has been done manually.
	 */
	public void resubmitAch(final int tranId, final Float fee, final String userId) throws TransactionAlreadyInProcessException,
	TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				if (fee != null) {
					tran.setTranSubStatCode(TransactionStatus.FEE_CODE);
					tranManager.updateTransaction(tran, userId, null, fee);
				}
				tran.setTranSubStatCode(TransactionStatus.ACH_SENT_CODE);
				tranManager.setTransactionStatus(tran.getEmgTranId(), tran.getStatus(), userId);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}


	public ArrayList approveDelayedSendTransaction(int tranId, String callerLoginId, String tranStat, String fundStat, String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException, AgentConnectException, InvalidRRNException,
	DataSourceException, SQLException {
		ArrayList<String> errors = new ArrayList<String>();

		// Get funding (CC Capture or ACH Waiting) with status of SEN/CCA
		if ( (TransactionStatus.FORM_FREE_SEND_CODE.equals(tranStat) ||
			   TransactionStatus.APPROVED_CODE.equals(tranStat) ||
			   TransactionStatus.PENDING_CODE.equals(tranStat) )
				&& TransactionStatus.NOT_FUNDED_CODE.equals(fundStat)) {
			try {
				fundDelayedSends(tranId, callerLoginId, contextRoot);
			} catch (BankPaymentServiceException e) {
				errors.add("error.bankpayment.posttoaccount");
				return errors;
			}
		}
		return null; // all is well.
	}


	protected void sendPersonToPersonSuccessEmail(Transaction tran, ConsumerAccountType accountType) throws Exception {
		ConsumerAccount account;
		if (accountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					na.notifySentTransaction(tran,consumerEmail,preferedlanguage);
				}
			}
		}
	}

	/**
	 *
	 * @param request
	 * @param trans
	 */

	public void sendEconomyServicePendingEmail(Transaction tran, String userId) throws Exception {
		ConsumerAccount account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					na.notifyPendingTransaction(tran, consumerEmail,preferedlanguage);
				}
			}
			TransactionManager manager = ManagerFactory.createTransactionManager();
			manager.setTransactionStatus(tran.getEmgTranId(), TransactionStatus.APPROVED_CODE, TransactionStatus.ACH_WAITING_CODE, userId);
		}
	}

	public void sendEconomyServiceAchReturnEmail(Transaction tran, String userId) throws Exception {
		ConsumerAccount account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					na.notifyACHReturnTransaction(tran, consumerEmail,preferedlanguage);
				}
			}
			TransactionManager manager = ManagerFactory.createTransactionManager();
			manager.setTransactionStatus(tran.getEmgTranId(), tran.getTranStatCode(), TransactionStatus.ACH_RETURNED_CODE, userId);
			if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())) {
				manager.setTransactionStatus(tran.getEmgTranId(), TransactionStatus.CANCELED_CODE, TransactionStatus.ACH_RETURNED_CODE,
						userId);
			}
		}
	}

	protected void sendCreditCardFailEmail(Transaction tran) throws Exception {
		ConsumerProfile profile = profileService.getConsumerProfile(new Integer(tran.getCustId()), tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyCreditCardFailTransaction(tran, consumerEmail,preferedlanguage);
			}
		}
	}

	/**
	 * Calls updateTransaction with verifyOwnership and verifyStrictOwnership
	 * set to true.
	 *
	 * @param transId
	 * @param userId
	 * @param command
	 * @return
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 *
	 *             Created on Mar 1, 2005
	 */
	protected Object updateTransaction(int transId, String userId, UpdateTransactionCommand command)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		return updateTransaction(transId, userId, command, true, true);
	}

	/**
	 *
	 * @param tranId
	 * @param userId
	 *            - id of user requesting update
	 * @param command
	 * @param verifyOwnership
	 *            - true implies transaction must not be owned by someone other
	 *            than the user requesting the update, i.e. owner can be empty.
	 * @param verifyStrictOwnership
	 *            - true implies transaction must be owned by user requesting
	 *            udpate. valid only when verifyOwnership is true.
	 * @return
	 * @throws TransactionAlreadyInProcessException
	 * @throws TransactionOwnershipException
	 *
	 *             Created on Mar 1, 2005
	 */
	protected Object updateTransaction(int tranId, String userId, UpdateTransactionCommand command, boolean verifyOwnership,
			boolean verifyStrictOwnership) throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		Integer transIdKey = new Integer(tranId);
		synchronized (transactionsInProcess) {
			if (transactionsInProcess.contains(transIdKey)) {
				String currentOwner;
				try {
					currentOwner = getCurrentOwner(tranId);
				} catch (Throwable t) {
					currentOwner = "unavailable";
					throw new TransactionAlreadyInProcessException("transaction is already in process", currentOwner, t);
				}
				throw new TransactionAlreadyInProcessException("transaction is already in process", currentOwner);
			}
			transactionsInProcess.add(transIdKey);
		}
		try {
			if (verifyOwnership) {
				String currentOwner = getCurrentOwner(tranId);
				boolean owned = StringUtils.isNotBlank(currentOwner);
				boolean ownerMatch = StringUtils.equalsIgnoreCase(userId, currentOwner);
				if (verifyStrictOwnership) {
					if (owned == false || ownerMatch == false) {
						throw new TransactionOwnershipException("Transaction is owned by " + currentOwner, currentOwner);
					}
				} else {
					if (owned == true && ownerMatch == false) {
						throw new TransactionOwnershipException("Transaction is owned by " + currentOwner, currentOwner);
					}
				}
			}
			return command.execute();
		} finally {
			synchronized (transactionsInProcess) {
				transactionsInProcess.remove(transIdKey);
			}
		}
	}


	private String getCurrentOwner(int tranId) {
		TransactionManager transactionManager = ManagerFactory.createTransactionManager();
		Transaction tran = transactionManager.getTransaction(tranId);
		return (tran == null ? "" : tran.getCsrPrcsUserid());
	}

	protected void acknowledgementFailEmail(int tranId, String refNumber, Throwable throwable, String userId) throws Exception {
		UserProfile up = getUserProfile(userId);
		String userName = userId;
		userName = up == null ? userName : up.getFirstName() + " " + up.getLastName() + " (" + userId + ")";
		Locale locale = Locale.getDefault();
		Object[] params = new Object[4];
		String label = "messages.mail.ac.acknowledge.failed.body";
		params[0] = tranId + "";
		params[1] = refNumber;
		params[2] = userName;
		String dump = emgshared.util.StringHelper.getExceptionDumpMsg(throwable);
		params[3] = dump;
		String body = I18NHelper.getFormattedMessage(label, params, resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage("messages.mail.ac.acknowledge.failed.subject", resourceFile, locale);
		// get error code and apend it to the subject line
		if (dump.indexOf("<ac:errorCode>") > 0) {
			int i = dump.indexOf("<ac:errorCode>") + 14;
			int j = dump.indexOf("</ac:errorCode>");
			subject = subject + " (Error Code: " + dump.substring(i, j).trim() + ")";
		}
		mailService.sendTransactionMail(EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected boolean sendAgentConnectFailMail(String message, Throwable throwable, String userId) {
		UserProfile up = getUserProfile(userId);
		String userName = "";
		if (up != null) {
			userName = up.getFirstName() + " " + up.getLastName() + " (" + userId + ")";
		}
		Locale locale = Locale.getDefault();
		Object[] params = new Object[2];
		String label = "messages.mail.ac.failed.body";
		params[0] = userName;
		String dump = emgshared.util.StringHelper.getExceptionDumpMsg(throwable);
		params[1] = message + dump;
		String body = I18NHelper.getFormattedMessage(label, params, resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage("messages.mail.ac.failed.subject", resourceFile, locale);
		// get error code and apend it to the subject line
		if (dump.indexOf("<ac:errorCode>") > 0) {
			int i = dump.indexOf("<ac:errorCode>") + 14;
			int j = dump.indexOf("</ac:errorCode>");
			subject = subject + " (Error Code: " + dump.substring(i, j).trim() + ")";
		}
		return MailHelper.sendMail(EMTSharedContainerProperties.getMailHost(), EMTSharedContainerProperties.getMailFrom(),
				EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected boolean addMGOGlobalAggErrorToTranComment(Exception e, int tranId, String userId) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String msg = sw.toString();
        try {
            pw.close();
            sw.close();
        } catch (Exception e0) {
        }
        UserProfile up = getUserProfile(userId);
        String userName = userId;
        userName = up == null ? userName : up.getFirstName() + " " + up.getLastName() + " (" + userId + ")";
        
        StringBuffer out = new StringBuffer();
        Error error=(Error) e.getCause();
        out.append(error.getErrorCode());
        out.append("-" + error.getSubErrorCode());
        out.append(" "+ error.getErrorString());
        msg = out.toString();
        if (msg.length() > 255) {
            msg = msg.substring(0, 254);
        }
        try {
            tranManager.setTransactionComment(tranId, "OTH", msg, "emgwww");
        } catch (Exception e1) {
            throw new EMGRuntimeException(e1);
        }
        return true;
	}

	protected boolean addAgentConnectErrorToTranComment(Throwable t, int tranId, String userId) {
		String className = "emgshared.exceptions.AgentConnectException";
		if (!t.getClass().getName().equals(className)) {
			return false;
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		String msg = sw.toString();
		try {
			pw.close();
			sw.close();
		} catch (Exception e) {
		}
		UserProfile up = getUserProfile(userId);
		String userName = userId;
		userName = up == null ? userName : up.getFirstName() + " " + up.getLastName() + " (" + userId + ")";
		StringBuffer out = new StringBuffer("User: " + userName + " **AgentConnect Error**  ");
		int i, j;
		// get error code
		if(t instanceof Error){
			Error error=(Error)t;
			out.append("Error Code: "+ error.getErrorCode());
			out.append("Error String: "+error.getErrorString());
			out.append("Sub-Error Code: "+error.getSubErrorCode());
			SimpleDateFormat sdf1 = new SimpleDateFormat();
		    sdf1.applyPattern("dd/MM/yyyy HH:mm:ss");
		    if(error.getTimeStamp()!=null){
			    String dateTime = sdf1.format(error.getTimeStamp().getTime());
				out.append("Timestamp: "+dateTime);
		    }
		    out.append("Detail String: " + error.getDetailString());
		} else {
			if (msg.indexOf("<ac:errorCode>") > 0) {
				i = msg.indexOf("<ac:errorCode>") + 14;
				j = msg.indexOf("</ac:errorCode>");
				out.append("Error Code: " + msg.substring(i, j).trim() + ", ");
			}
			// get error string
			if (msg.indexOf("<ac:errorString>") > 0) {
				i = msg.indexOf("<ac:errorString>") + 16;
				j = msg.indexOf("</ac:errorString>");
				out.append("Error String: " + msg.substring(i, j).trim() + ", ");
			}
			// get sub error code
			if (msg.indexOf("<ac:subErrorCode>") > 0) {
				i = msg.indexOf("<ac:subErrorCode>") + 17;
				j = msg.indexOf("</ac:subErrorCode>");
				out.append("Sub-Error Code: " + msg.substring(i, j).trim() + ", ");
			}
			// get timestamp
			if (msg.indexOf("<ac:timeStamp>") > 0) {
				i = msg.indexOf("<ac:timeStamp>") + 14;
				j = msg.indexOf("</ac:timeStamp>");
				out.append("Timestamp: " + msg.substring(i, j).trim() + ", ");
			}
			// get detail string
			if (msg.indexOf("<ac:detailString>") > 0) {
				i = msg.indexOf("<ac:detailString>") + 17;
				j = msg.indexOf("</ac:detailString>");
				out.append("Detail String: " + msg.substring(i, j).trim());
			}
		}
		msg = out.toString();
		if (msg.length() > 255) {
			msg = msg.substring(0, 254);
		}
		try {
			tranManager.setTransactionComment(tranId, "OTH", msg, "emgwww");
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
		return true;
	}

	public void recordCcChargeback(final int tranId, final float amount, final String userId, final String fundCode,
			final String subReasonCode) throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				// basic checking to see if Chargeback is allowed now.
				if (!((tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.WRITE_OFF_LOSS_CODE))
						|| (tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.RECOVERY_OF_LOSS_CODE)) || (tran
								.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)))) {
					throw new IllegalStateException("transaction id = " + tranId + " is not in a Record Chargeback State");
				}
				TransactionManager manager = ManagerFactory.createTransactionManager();
				tran.setTranSubStatCode(fundCode);
				manager.updateTransaction(tran, userId, null, new Float(amount), subReasonCode);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
	}

	// method that can be used for simple status transitions where
	// a sub-reason is also needed.
	public void confirmStatusChangeWithSubReason(final int tranId, final String userId, final String statusCode,
			final String subStatusCode, final String subReasonCode) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException {
		UpdateTransactionCommand command = new UpdateTransactionCommand() {
			public Object execute() {
				Transaction tran = tranManager.getTransaction(tranId);
				if (tran == null) {
					throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
				}
				TransactionManager manager = ManagerFactory.createTransactionManager();
				if (!StringHelper.isNullOrEmpty(statusCode))
					tran.setTranStatCode(statusCode);
				if (!StringHelper.isNullOrEmpty(subStatusCode))
					tran.setTranSubStatCode(subStatusCode);
				manager.updateTransaction(tran, userId, null, null, subReasonCode);
				return null;
			}
		};
		updateTransaction(tranId, userId, command, true, true);
		// TODO need to add code to update the sub-reason
	}

	protected MoneyGramSendRequest setMoneyGramSendRequest(Transaction tran, BigDecimal amount) throws Exception {
		MoneyGramSendRequest req = new MoneyGramSendRequest();
		req.setAmount(amount);
		AgentConnectService acs = MGOServiceFactory.getInstance().getAgentConnectService();
		MGOProductFieldInfo productFieldInfo = null;
		com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs.getDeliveryOptionInfo(tran.getDlvrOptnId());
		MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
		String agtid = String.valueOf(tran.getSndAgentId());
		String tc = tran.getEmgTranTypeCode();
		MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tc);
		if (tran.getRcvAgentId() > 0)
			productFieldInfo = acs.getFieldsByProduct(mgoAgentProfile, tran.getRcvISOCntryCode(), tran.getRcvISOCrncyId(), String
					.valueOf(tran.getRcvAgentId()), dlvrOptn.getDeliveryOption(), "0", amount);
		else
			productFieldInfo = acs.getFieldsByProduct(mgoAgentProfile, tran.getRcvISOCntryCode(), tran.getRcvISOCrncyId(), dlvrOptn
					.getDeliveryOption(), "0", amount);
		Map fields = productFieldInfo.getProductFieldInfo();
		int senderFirstNameMax = ((Long) fields.get("senderFirstName")).intValue();
		int senderLastNameMax = ((Long) fields.get("senderLastName")).intValue();
		int senderAddressMax = ((Long) fields.get("senderAddress")).intValue();
		int senderCityMax = ((Long) fields.get("senderCity")).intValue();
		int senderStateMax = ((Long) fields.get("senderState")).intValue();
		int senderZipCodeMax = ((Long) fields.get("senderZipCode")).intValue();
		int receiverFirstNameMax = ((Long) fields.get("receiverFirstName")).intValue();
		int receiverLastNameMax = ((Long) fields.get("receiverLastName")).intValue();
		int receiverLastName2Max = ((Long) fields.get("receiverLastName2")).intValue();
		int receiverCityMax = ((Long) fields.get("receiverCity")).intValue();
		int direction1Max = ((Long) fields.get("direction1")).intValue();
		int direction2Max = ((Long) fields.get("direction2")).intValue();
		int direction3Max = ((Long) fields.get("direction3")).intValue();
		int rcvAddress1Max = ((Long) fields.get("receiverAddress")).intValue();
		int rcvAddress2Max = ((Long) fields.get("receiverAddress2")).intValue();
		int rcvAddress3Max = ((Long) fields.get("receiverAddress3")).intValue();
		int messageField1Max = ((Long) fields.get("messageField1")).intValue();
		int messageField2Max = ((Long) fields.get("messageField2")).intValue();
		int testAnswerMax = ((Long) fields.get("testAnswer")).intValue();
		int testQuestionMax = ((Long) fields.get("testQuestion")).intValue();
		int senderPhotoIdNumberMax = ((Long) fields.get("senderPhotoIdNumber")).intValue();
		int senderOccupationMax = ((Long) fields.get("senderOccupation")).intValue();
		int senderLegalIdNumberMax = ((Long) fields.get("senderLegalIdNumber")).intValue();
		int receiverStateMax = ((Long) fields.get("receiverState")).intValue(); // added
		// for
		// TIGER
		req.setSenderFirstName(StringHelper.truncate(tran.getSndCustFrstName(), senderFirstNameMax));
		req.setSenderLastName(StringHelper.truncate(tran.getSndCustLastName(), senderLastNameMax));
		TransactionManager tm = ManagerFactory.createTransactionManager();
		CustAddress addr = tm.getCustAddress(tran.getSndCustAddrId());
		ConsumerProfile profile = profileService.getConsumerProfile(new Integer(tran.getSndCustId()), "sys", "");
		String addrText = addr.getAddrLine1Text();
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine2Text())) {
			addrText = addrText + " " + addr.getAddrLine2Text();
		}
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine3Text())) {
			addrText = addrText + " " + addr.getAddrLine3Text();
		}
		req.setSendCurrency(tran.getSndISOCrncyId());
		if (!StringHelper.isNullOrEmpty(profile.getPhoneNumber())) {
			req.setSenderHomePhone(StringHelper.truncate(profile.getPhoneNumber(), 16));
		}
		req.setSenderAddress(StringHelper.truncate(addrText, senderAddressMax));
		req.setSenderCity(StringHelper.truncate(addr.getAddrCityName(), senderCityMax));
		if(!StringHelper.isNullOrEmpty(addr.getAddrStateName()))
			req.setSenderState(StringHelper.truncate(addr.getAddrStateName(), senderStateMax));
		//else
		//	req.setSenderState(StringHelper.truncate("MN", senderStateMax));
		req.setSenderCountry(addr.getAddrCntryId());
		req.setSenderZipCode(StringHelper.truncate(addr.getAddrPostalCode(), senderZipCodeMax));
		req.setDeliveryOption(dlvrOptn.getDeliveryOption());
		req.setReceiverFirstName(StringHelper.truncate(tran.getRcvCustFrstName(), receiverFirstNameMax));
		String middleInitial = tran.getRcvCustMidName();
		if (middleInitial != null && middleInitial.length() == 1) {
			req.setReceiverMiddleInitial(middleInitial);
		}
		req.setReceiverLastName(StringHelper.truncate(tran.getRcvCustLastName(), receiverLastNameMax));
		req.setReceiverLastName2(StringHelper.truncate(tran.getRcvCustMatrnlName(), receiverLastName2Max));
		req.setReceiverCity(StringHelper.truncate(tran.getRcvCustAddrCityName(), receiverCityMax));
		req.setReceiverState(StringHelper.truncate(tran.getIntndDestStateProvinceCode(), receiverStateMax));
		req.setReceiveCountry(tran.getRcvISOCntryCode());
		req.setReceiveCurrency(tran.getRcvISOCrncyId());
		req.setDirection1(StringHelper.truncate(tran.getRcvCustDlvrInstr1Text(), direction1Max));
		req.setDirection2(StringHelper.truncate(tran.getRcvCustDlvrInstr2Text(), direction2Max));
		req.setDirection3(StringHelper.truncate(tran.getRcvCustDlvrInstr3Text(), direction3Max));
		req.setReceiverAddress(StringHelper.truncate(tran.getRcvCustAddrLine1Text(), rcvAddress1Max));
		req.setReceiverAddress2(StringHelper.truncate(tran.getRcvCustAddrLine2Text(), rcvAddress2Max));
		req.setReceiverAddress3(StringHelper.truncate(tran.getRcvCustAddrLine3Text(), rcvAddress3Max));
		req.setMessageField1(StringHelper.truncate(tran.getSndMsg1Text(), messageField1Max));
		req.setMessageField2(StringHelper.truncate(tran.getSndMsg2Text(), messageField2Max));
		req.setTestQuestion(StringHelper.truncate(tran.getTestQuestion(), testQuestionMax));
		req.setTestAnswer(StringHelper.truncate(tran.getTestQuestionAnswer(), testAnswerMax));
		req.setSenderOccupation(StringHelper.truncate(tran.getSndCustOccupationText(), senderOccupationMax));
		req.setSenderDOB(profile.getBirthdate());
		String legalId = tran.getSndCustLegalId();
		if(tran.getPartnerSiteId().equals(EMoneyGramAdmApplicationConstants.MGODE_PARTNER_SITE_ID)) {
			legalId = decryptService.decryptSSN(legalId);
		}
		req.setSenderLegalIdNumber(StringHelper.truncate(legalId, senderLegalIdNumberMax));
		if (tran.getSndCustLegalIdTypeCode() != null) {
			LegalIdType lit = LegalIdType.fromString(tran.getSndCustLegalIdTypeCode());
			req.setSenderLegalIdType(lit);
		}
		req.setSenderPhotoIdNumber(StringHelper.truncate(tran.getSndCustPhotoId(), senderPhotoIdNumberMax));
		req.setSenderPhotoIdState(tran.getSndCustPhotoIdStateCode());
		req.setSenderPhotoIdCountry(tran.getSndCustPhotoIdCntryCode());
		req.setConsumerId("0");
		if ((tran.getLoyaltyPgmMembershipId() == null) || (tran.getLoyaltyPgmMembershipId().equals(""))) {
			req.setFreqCustCardNumber(null);
			if ((tran.getCustomerAutoEnrollFlag() != null) && (tran.getCustomerAutoEnrollFlag().equalsIgnoreCase("Y")))
				req.setIssueFreqCustCard(Boolean.TRUE);
			else
				req.setIssueFreqCustCard(Boolean.FALSE);
		} else
			req.setFreqCustCardNumber(tran.getLoyaltyPgmMembershipId());
		if (tran.getSndCustPhotoIdTypeCode() != null) {
			PhotoIdType pid = PhotoIdType.fromString(tran.getSndCustPhotoIdTypeCode());
			req.setSenderPhotoIdType(pid);
		}
		if (tran.getRcvAgentId() > 0) {
			req.setReceiveAgentID(String.valueOf(tran.getRcvAgentId()));
		}
		if (tran.getRRN() != null) {
			req.setCustomerReceiveNumber(tran.getRRN());
		}
		return req;
	}

	protected CommitTransactionRequest buildCommitTransactionRequest(Transaction tran) throws Exception {
		CommitTransactionRequest req = new CommitTransactionRequest();
		req.setMgiTransactionSessionID(tran.getMgiTransactionSessionID());
		req.setProductType(com.moneygram.agentconnect1305.wsclient.ProductType.SEND);
		req.setStateRegulatorVersion(tran.getRegulationVersion());

		return req;
	}

	/*private String buildFullName(String firstName, String middleName, String lastName) {
		if (!StringHelper.isNullOrEmpty(middleName)) {
			return firstName + " " + middleName + ". " + lastName;
		} else {
			return firstName + " " + lastName;
		}
	}*/

	public List<GlAccount> getGLAccounts(String userId) throws Exception {
		TransactionManager transactionManager = ManagerFactory.createTransactionManager();
		List<GlAccount> glAccounts = transactionManager.getGLAccounts(userId);

		return glAccounts;
	}

	protected BigDecimal getNewExchangeRate(
	        AbstractAgentConnectAccessor ac,
	        String receiveCountry,
	        String sendCountry,
	        int dlvrOptnId,
	        String receiveCurrency,
	        String receivePayoutCurrency,
			String agencyId,
			String rewardsNumber) {
		FeeLookupResponse resp = null;
		try {
			AgentConnectService acs = MGOServiceFactory.getInstance().getAgentConnectService();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs.getDeliveryOptionInfo(dlvrOptnId);
			resp = ac.feeLookup(new BigDecimal(1), receiveCountry, ProductType.SEND, receiveCurrency, dlvrOptn.getDeliveryOption(), agencyId,
					rewardsNumber);
		} catch (Throwable t) {
			return null;
		}

        if (sendCountry.equals("USA") &&
                !receiveCurrency.equals(receivePayoutCurrency)) {
            return resp.getFeeInfo(0).getEstimatedExchangeRate();
        } else {
            return resp.getFeeInfo(0).getValidExchangeRate();
        }
	}

	protected BigDecimal getNewExchangeRateDoddFrank(
	        AbstractAgentConnectAccessorDoddFrank ac,
	        String receiveCountry,
	        String sendCountry,
	        int dlvrOptnId,
	        String receiveCurrency,
	        String receivePayoutCurrency,
	        String agencyId,
	        String rewardsNumber) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectServiceDoddFrank acs = MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs.getDeliveryOptionInfo(dlvrOptnId);
			resp = ac.feeLookup(new BigDecimal(1), receiveCountry, com.moneygram.agentconnect1305.wsclient.ProductType.SEND, receiveCurrency, dlvrOptn.getDeliveryOption(), agencyId,
					rewardsNumber);
		} catch (Throwable t) {
			return null;
		}

        /** 
         * WAP and MGO (US) partner sites will always retrieve the valid
         * exchange rate value.
         */
		if (sendCountry.equals("USA") &&
                !receiveCurrency.equals(receivePayoutCurrency)) {
		    return resp.getFeeInfo(0).getEstimatedExchangeRate();
		} else {
		    /**
		     *  Money is being sent from another country (site) such as 'MGO DE',
		     *  MGOUK, and so retrieve the estimated exchange rate.
		     */ 
		    return resp.getFeeInfo(0).getValidExchangeRate();
		}		
	}

	protected com.moneygram.agentconnect1305.wsclient.FeeLookupResponse feeLookupCall(AbstractAgentConnectAccessorDoddFrank ac, Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectServiceDoddFrank acs = MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs.getDeliveryOptionInfo(tran.getDlvrOptnId());
			resp = ac.feeLookup(
							tran.getSndFaceAmt(),
							tran.getRcvISOCntryCode(),
							com.moneygram.agentconnect1305.wsclient.ProductType.SEND,
							tran.getRcvISOCrncyId(),
							dlvrOptn.getDeliveryOption(), tran.getLoyaltyPgmMembershipId(),tran.getRcvAgentId() + "");
		} catch (Throwable t) {
			return null;
		}
		return resp;
	}

	protected com.moneygram.agentconnect1305.wsclient.FeeLookupResponse bpfeeLookupCall(AbstractAgentConnectAccessorDoddFrank ac, Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse resp = null;
		try {
			AgentConnectServiceDoddFrank acs = MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
			com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo dlvrOptn = acs.getDeliveryOptionInfo(tran.getDlvrOptnId());
			resp = ac.bpfeeLookup(
							tran.getSndFaceAmt(),
							tran.getRcvISOCntryCode(),
							com.moneygram.agentconnect1305.wsclient.ProductType.BP,
							tran.getRcvISOCrncyId(),
							dlvrOptn.getDeliveryOption(), tran.getLoyaltyPgmMembershipId(),tran.getRcvAgcyCode() + "");
		} catch (Throwable t) {
			return null;
		}
		return resp;
	}
	private UserProfile getUserProfile(String userId) {
		UserProfile up = null;
		UserProfileManager upm = ManagerFactory.createUserManager();
		try {
			up = upm.getUser(userId);
		} catch (Exception e) {
		}
		return up;
	}

	public Transaction getTransactionForAutoProcess(String userId, String tranStatCode,
			String tranSubStatCode, String csrUserId, Date createDateTime, String partnerSiteId,
			String tranTypeCode) throws DataSourceException {
		TransactionManager transactionManager = ManagerFactory.createTransactionManager();
		return transactionManager.getTransactionForAutoProcess(userId, tranStatCode,
				tranSubStatCode, csrUserId, createDateTime, partnerSiteId, tranTypeCode);
	}

	public Collection denyExpiredTrans() {
		TransactionManager manager = ManagerFactory.createTransactionManager();
		Collection deadTranList = manager.getDeadTransactions();
		Iterator iter = deadTranList.iterator();
		while (iter.hasNext()) {
			DeadTransaction dt = (DeadTransaction) iter.next();
			manager.setTransactionStatus(dt.getTranId(), "DEN", "NOF", "system");
		}
		return deadTranList;
	}

	public void updateLimboASPTrans() {
		TransactionManager manager = ManagerFactory.createTransactionManager();
		int[] tranList = (int[]) manager.getLimboASPTransactions();
		for (int i = 0; i < tranList.length; i++) {
			manager.setTransactionStatus(tranList[i], "FFS", "NOF", "system");
		}
	}

	protected void sendAdminMessage(String processUserId, String message) {
		AdminMessage amb = new AdminMessage();
		amb.setAdminMsgTypeCode("System Error");
		amb.setMsgPrtyCode("9");
		amb.setMsgBegDate(new Date(System.currentTimeMillis()));
		amb.setMsgThruDate(new Date(System.currentTimeMillis() + 24L * 60L * 60L * 1000L));
		amb.setMsgText(message);
		String userPrefix = EMTAdmAppProperties.getSystemUserIdPrefix();
		List userList = new ArrayList();
		// if it's a background processor, send message to all users
		// for the role specified; otherwise, just the processing user
		if (processUserId.startsWith(userPrefix)) {
			UserProfileManager upm = ManagerFactory.createUserManager();
			UserQueryCriterion criteria = new UserQueryCriterion();
			// currently, the role id is 'Manager'
			criteria.setRoleId(EMTAdmAppProperties.getRoleIdMsgSendTo());
			Iterator iter;
			try {
				iter = upm.findHollowUsers(criteria).iterator();
			} catch (DataSourceException e) {
				throw new EMGRuntimeException(e);
			}
			while (iter.hasNext()) {
				UserProfile user = (UserProfile) iter.next();
				userList.add(user.getUID());
			}
		} else {
			userList.add(processUserId);
		}
		String[] users = new String[userList.size()];
		int i = 0;
		Iterator iter = userList.iterator();
		while (iter.hasNext()) {
			users[i++] = (String) iter.next();
		}
		try {
			messageService.insertAdminMessage(processUserId, amb, users);
		} catch (DataSourceException e) {
			// just catch this and log it, just a message failure
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Error Inserting Admin Message:", e);
		}
	}

	protected String getMessageResource(String key, String[] params) {
		Locale locale = Locale.getDefault();
		MessageResources msgRes = MessageResources.getMessageResources("emgadm.resources.ApplicationResources");
		MessageFormat formatter = new MessageFormat(msgRes.getMessage(key));
		formatter.setLocale(locale);
		return formatter.format(params);
	}

	public boolean sendConsumerAdhocSearchTraceMail(ConsumerProfileSearchCriteria cpsc, String userId) {
		UserProfile up = getUserProfile(userId);
		String userName = "";
		if (up != null) {
			userName = up.getFirstName() + " " + up.getLastName() + " (" + userId + ")";
		}
		Locale locale = Locale.getDefault();
		Object[] params = new Object[2];
		String label = "messages.mail.consumer.search.body";
		params[0] = userName;
		StringBuffer criteria = new StringBuffer();
		if (cpsc.getCustLogonId() != null)
			criteria.append("Cust Logon Id : " + cpsc.getCustLogonId() + "\n");
		if (cpsc.getCustFrstName() != null)
			criteria.append("Cust First Name : " + cpsc.getCustFrstName() + "\n");
		if (cpsc.getCustLastName() != null)
			criteria.append("Cust Last Name : " + cpsc.getCustLastName() + "\n");
		if (cpsc.getCustPhoneNumber() != null)
			criteria.append("Cust Phone Number : " + cpsc.getCustPhoneNumber() + "\n");
		if (cpsc.getCustSSNMask() != null)
			criteria.append("Cust SSN Mask : " + cpsc.getCustSSNMask() + "\n");
		if (cpsc.getCustCCMask() != null)
			criteria.append("Cust CC Mask : " + cpsc.getCustCCMask() + "\n");
		if (cpsc.getCustBankMask() != null)
			criteria.append("Cust Bank Mask : " + cpsc.getCustBankMask() + "\n");
		if (cpsc.getCustAddrLine1Text() != null)
			criteria.append("Cust Addr Line 1 : " + cpsc.getCustAddrLine1Text() + "\n");
		if (cpsc.getCustFrstName() != null)
			criteria.append("Cust Addr City Name : " + cpsc.getCustAddrCityName() + "\n");
		if (cpsc.getCustAddrCityName() != null)
			criteria.append("Cust Addr State Name : " + cpsc.getCustAddrStateName() + "\n");
		if (cpsc.getCustAddrPostalCode() != null)
			criteria.append("Cust Addr Postal Code : " + cpsc.getCustAddrPostalCode() + "\n");
		if (cpsc.getCustCreateDate() != null)
			criteria.append("Cust Create Date : " + cpsc.getCustCreateDate() + "\n");
		if (cpsc.getCustCreateIpAddrId() != null)
			criteria.append("Cust Create IP Address : " + cpsc.getCustCreateIpAddrId() + "\n");
		if (cpsc.getCustPrmrCode() != null)
			criteria.append("Cust Premier Code : " + cpsc.getCustPrmrCode() + "\n");
		if (cpsc.getCustStatCode() != null)
			criteria.append("Cust Status Code : " + cpsc.getCustStatCode() + "\n");
		if (cpsc.getCustSubstatCode() != null)
			criteria.append("Cust Sub Status Code : " + cpsc.getCustSubstatCode() + "\n");
		params[1] = criteria.toString();
		String body = I18NHelper.getFormattedMessage(label, params, resourceFile, locale);
		String subject = I18NHelper.getFormattedMessage("messages.mail.consumer.search.subject", resourceFile, locale);
		return MailHelper.sendMail(EMTSharedContainerProperties.getMailHost(), EMTSharedContainerProperties.getMailFrom(),
				EMTSharedContainerProperties.getMailACTo(), subject, body);
	}

	protected String getTransactionUrl(String contextRoot) {
		return contextRoot + "/transactionDetail.do";
	}

	public void doCreditCardReversal(int tranId, String userId, String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException {
		// implement in subclass if needed

	}

	public void doCreditCardPartialRefund(int tranId, double refundAmount, String userId,
			String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, CreditCardRefundException {
		// UK only method

	}

	public boolean failCaliforniaCompliance(int tranId, String userId) {
		boolean notInCompliance = false;
		// get transaction bean
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(tranId);
		if (tran == null) {
			throw new EMGRuntimeException("Transaction " + tranId + " does not exists");
		}
		//FIXME: return if MGOUK as well
		// make sure transaction type is ES or MG
		String tranType = tran.getEmgTranTypeCode();
		if (!TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE.equals(tranType)
				&& !TransactionType.MONEY_TRANSFER_SEND_CODE.equals(tranType)) {
			return false;
		}
		// get sender's state
		CustAddress ca = null;
		String state = null;
		try {
			ca = tm.getCustAddress(tran.getSndCustAddrId());
		} catch (DataSourceException e2) {
			throw new EMGRuntimeException("Sender Address Id " + tran.getSndCustAddrId() + " does not exists");
		}
		if (ca != null) {
			state = ca.getAddrStateName();
		} else {
			throw new EMGRuntimeException("Sender Address Id " + tran.getSndCustAddrId() + " does not exists");
		}
		// check to see if it's a California consumer
		if (!"CA".equalsIgnoreCase(state)) {
			return false;
		}
		//check to see if this is a safe harbor - aka indicative rate - country, if not, no further action needed
		if(tran.getRcvPayoutISOCrncyId() != null && tran.getRcvISOCrncyId().equals(tran.getRcvPayoutISOCrncyId())){
			return false;
		}
		
		FeeLookupResponse resp = null;
		AgentConnectService acs =null;
		AgentConnectServiceDoddFrank agentConnectServiceDoddFrank=null;
		com.moneygram.agentconnect1305.wsclient.FeeLookupResponse respDoddFrank=null;


		try {
			MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
			String agtid = String.valueOf(tran.getSndAgentId());
			String tc = tran.getEmgTranTypeCode();
			MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tc);
			if(isMgiTranId(tran)){
				agentConnectServiceDoddFrank=MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
				com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo delOptInfo = agentConnectServiceDoddFrank.getDeliveryOptionInfo(tran.getDlvrOptnId());
				respDoddFrank = agentConnectServiceDoddFrank.mgFeeLookup(mgoAgentProfile, tran.getSndFaceAmt(), tran.getRcvISOCntryCode(), tran.getRcvISOCrncyId(), delOptInfo
						.getDeliveryOption(), tran.getLoyaltyPgmMembershipId());
			} else {
				acs = MGOServiceFactory.getInstance().getAgentConnectService();
				com.moneygram.agentconnect1305.wsclient.DeliveryOptionInfo delOptInfo = acs.getDeliveryOptionInfo(tran.getDlvrOptnId());
				resp = acs.mgFeeLookup(mgoAgentProfile, tran.getSndFaceAmt(), tran.getRcvISOCntryCode(), tran.getRcvISOCrncyId(), delOptInfo
						.getDeliveryOption(), tran.getLoyaltyPgmMembershipId());
			}
		} catch (Throwable t) {
			throw new EMGRuntimeException("Agent Connect feeLookup failed");
		}
		NumberFormat df = new DecimalFormat("\u00A4#,##0.00");
				
		// check to see if the exchange rate has decreased
		NumberFormat df1 = new DecimalFormat("#0.000000");
		BigDecimal newFxRate = !isMgiTranId(tran)?resp.getFeeInfo()[0].getEstimatedExchangeRate():respDoddFrank.getFeeInfo()[0].getEstimatedExchangeRate();
		double oldRate = tran.getSndFxCnsmrRate().doubleValue();
		double oldAmt = tran.getSndFaceAmt().doubleValue();
		double newRate = newFxRate.doubleValue();
		double newAmt = !isMgiTranId(tran)? resp.getFeeInfo()[0].getSendAmount().doubleValue():
			respDoddFrank.getFeeInfo()[0].getSendAmounts().getSendAmount().doubleValue();
		int oldRoundedAmt = (int) Math.round(oldRate * oldAmt * 100);
		int newRoundedAmt = (int) Math.round(newRate * newAmt * 100);
		if (oldRoundedAmt != newRoundedAmt && newRate < oldRate) {
			try {
				tm.setTransactionComment(tranId, "OTH", "The foreign exchange rate changed from " + df1.format(oldRate) + " to "
						+ df1.format(newRate) + ".  The process is terminated due to the compliance "
						+ "of the requirements of the California laws.", userId);
				//Logging added below to help determine how rates are getting rounded in prod
				String error = "Old unconverted rate = " + tran.getSndFxCnsmrRate() + ";new unconverted fxRate=" + newFxRate +
				";oldRoundedAmt = " + oldRoundedAmt + ";newRoundedAmt = " + newRoundedAmt +
				";oldRate = " + oldRate + ";newRate = " + newRate;
				EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(error);
			} catch (Exception e1) {
				throw new EMGRuntimeException("Add transaction comment for tran Id " + tranId + " Failed");
			}
			notInCompliance = true;
		}
		return notInCompliance;
	}

	protected void fundDelayedSends(final int tranId, final String userId, final String contextRoot)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException,
			BankPaymentServiceException {

				UpdateTransactionCommand command = new UpdateTransactionCommand() {
					public Object execute() {
						final Transaction tran = tranManager.getTransaction(tranId);
						if (tran == null) {
							throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
						}

						final int tranId = tran.getEmgTranId();

						try {
							long beginTime = System.currentTimeMillis();
							BankPaymentProxy proxy = BankPaymentProxyImpl.getInstance();
							StatusInquiryRequestMessage rqst = new StatusInquiryRequestMessage();
							rqst.setClientTraceId(tran.getTCInternalTransactionNumber());
							rqst.setProcessorTraceId(tran.getTCProviderTransactionNumber());
							StatusInquiryResponseMessage resp = proxy.statusInquiry(rqst);

							EventMonitor.getInstance().recordEvent(
									EventListConstant.BANK_PAYMENT_SERVICE_POST,
									beginTime, System.currentTimeMillis());

							boolean success = false;
							if (resp.getResponseCode().equalsIgnoreCase(BankPaymentProxyImpl.BANK_PAYMENT_RESP_POST_ACCOUNT_SUCCESS)) {
								//Change from FFS/NOF to APP/NOF
								if (tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.FORM_FREE_SEND_CODE) ||
										tran.getStatus().getStatusCode().equalsIgnoreCase(TransactionStatus.PENDING_CODE)) {
									tran.setTranStatCode(TransactionStatus.APPROVED_CODE);
									tranManager.updateTransaction(tran, userId, null, null);
								}
								//Change from APP/NOF to APP/BKS
								tran.setTranSubStatCode(TransactionStatus.BANK_SETTLED_CODE);
								tranManager.updateTransaction(tran, userId, resp.getApprovalCode(), null);
								success = true;
							}

							CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
							commentService.insertBankPaymentServiceResponseComment("STATUS", new Integer(tranId), success, userId, resp.getResponseCode(), resp.getApprovalCode(),
									resp.getDenialRecNbr(), resp.getAchStatus(), tran.getTCProviderTransactionNumber(), tran.getTCInternalTransactionNumber());

							if (!success) {
								return "BankPaymentService posting call unsuccessful";
							}

						} catch (Exception e) {
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to post to BankPaymentService:", e);
							CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
							commentService.insertBankPaymentServiceResponseComment("STATUS", new Integer(tranId), false, userId, null, null, null, null,
									tran.getTCProviderTransactionNumber(), tran.getTCInternalTransactionNumber());
							return "BankPaymentService Exception: " + e.getMessage();
						}

						return null;
					}
				};
				Object obj = updateTransaction(tranId, userId, command);
				if (obj != null) {
					throw new BankPaymentServiceException((String) obj);
				}
			}

	public ArrayList processDelayedSendTransaction(int tranId, String callerLoginId, String tranStat,
			String fundStat, String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException, InvalidRRNException,
			DataSourceException, SQLException {

		ArrayList<String> errors = new ArrayList<String>();
		if (failCaliforniaCompliance(tranId, callerLoginId)) {
			errors.add("error.california.compliance.failed");
			return errors;
		}
		// Do AgentConnect MoneyGram Send with status of APP/CCA
		TransactionManager tm = ManagerFactory.createTransactionManager();
		Transaction tran = tm.getTransaction(tranId);
		if (TransactionStatus.APPROVED_CODE.equals(tran.getTranStatCode())
				&& TransactionStatus.BANK_SETTLED_CODE.equals(tran.getTranSubStatCode())) {
			try {
				sendPersonToPerson(tranId, callerLoginId, false);
				tran = tm.getTransaction(tranId);
			} catch (AgentConnectException e) {
				// add admin message
				String[] params = { String.valueOf(tran.getEmgTranId()), tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
				sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.person.to.person.send.fail.notice", params));
				errors.add("error.agent.connect.failed");
				tran.setTranStatCode(TransactionStatus.ERROR_CODE);
				tran.setSndTranDate(new Date());
				tm.updateTransaction(tran, callerLoginId, null, null);
				return errors;
			}
		}
		return null; // all is well.
	}

	protected abstract void fundExpressPayment(int tranId, String authConfCode,
			String callerLoginId, String contextRoot)throws TransactionAlreadyInProcessException,
			TransactionOwnershipException;

	public ArrayList processEconomyServiceTransaction(int tranId, String callerLoginId, String tranStat,
			String fundStat, String contextRoot) throws TransactionAlreadyInProcessException,
			TransactionOwnershipException, AgentConnectException, InvalidRRNException,
			DataSourceException, SQLException {
				ArrayList elr = new ArrayList<String>();
				if (failCaliforniaCompliance(tranId, callerLoginId)) {
					elr.add("error.california.compliance.failed");
					return elr;
				}
				Transaction tran;
				tran = ManagerFactory.createTransactionManager().getTransaction(tranId);
				if (tran.isSendable()) {
					try {
						sendEconomyService(tranId, callerLoginId);
					} catch (AgentConnectException e) {
						// add admin message
						String[] params = { String.valueOf(tran.getEmgTranId()), tran.getLgcyRefNbr(), getTransactionUrl(contextRoot) };
						sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.economy.service.send.fail.notice", params));
						elr.add("error.agent.connect.failed");
						return elr;
					}
				}
				return null;
			}

	public void refundPersonToPerson(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException,
			AgentConnectException, EMGRuntimeException {
				UpdateTransactionCommand command = new UpdateTransactionCommand() {
					public Object execute() {
						final TransactionManager tranManager = ManagerFactory.createTransactionManager();
						final Transaction tran = tranManager.getTransaction(tranId);
						if (tran == null) {
							throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
						}
						if (tran.isRefundable() == false) {
							throw new IllegalStateException("transaction id = " + tranId + " is not in a refundable state.");
						}
						// int confirmationNumber = tran.getFormFreeConfNbr();
						try {
							Date before = new Date(System.currentTimeMillis());
							long beginTime = System.currentTimeMillis();
							MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
							String agtid = String.valueOf(tran.getSndAgentId());
							MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());
							AgentConnectAccessorDoddFrank agentConnectAccessorDoddFrank = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
							//Try 'C' (Cancel) reversal type first.  AC will return 605/3401 error/suberror if txn is in state 
				              //if 'R' (Refund) type is needed - if so, then do 'R' type reversal
				              try {
				                agentConnectAccessorDoddFrank.sendReversal(String.valueOf(tran.getLgcyRefNbr()),
				                    tran.getSndFaceAmt(), tran.getSndFeeAmt(),tran.getSndISOCrncyId(), SendReversalType.C);
				              } catch (Error err) {
				                if (err.getErrorCode().intValue() == 605 &&
				                  err.getSubErrorCode().intValue() == 3401) {
				                  agentConnectAccessorDoddFrank.sendReversal(String.valueOf(tran.getLgcyRefNbr()),
				                      tran.getSndFaceAmt(), tran.getSndFeeAmt(),tran.getSndISOCrncyId(), SendReversalType.R);                 
				                } else {
				                  throw err;
				                }               
				              } catch (Exception e1) {
								addAgentConnectErrorToTranComment(e1, tranId, userId);
								String msg = "In Program: TransactionServiceImpl\n" + "In Method: refundPersonToPerson()\n"
								+ "AC call started at: " + before + "\n" + "AC call aborted at: " + new Date(System.currentTimeMillis())
								+ "\n" + "AC Method called: sendReversal() with a transaction id of " + tran.getEmgTranId()
								+ " and the legacy reference number of " + tran.getLgcyRefNbr() + "\n\n";
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("send Economy Service failed.\n\n" + msg, e1);
								sendAgentConnectFailMail(msg, e1, userId);
								throw new AgentConnectException(e1);
							} finally {
								EventMonitor.getInstance().recordEvent(EventListConstant.AC_SEND_REVERSAL, beginTime, System.currentTimeMillis());
							}
							//FIXME S26 - set flag to indicate whether cancel cuz NSF for Bank(Telecheck) or customer initiated refund/cancellation
							//This is for email logic to know what type of email to send

							if (!tran.getEmgTranTypeCode().equalsIgnoreCase(TransactionType.DELAY_SEND_CODE)) {
								// set the transaction status to cancel
								tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
								tran.setTranStatDate(new Date());
								tranManager.updateTransaction(tran, userId);

								// set the fund status to ACH Cancel, ACH or CC Refund
								// Request
								if (tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.ACH_WAITING_CODE)) {
									tran.setTranSubStatCode(TransactionStatus.ACH_CANCEL_CODE);
								} else if (tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.ACH_SENT_CODE)) {
									tran.setTranSubStatCode(TransactionStatus.ACH_REFUND_REQUESTED_CODE);
								} else if (tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.CC_SALE_CODE)) {
									tran.setTranSubStatCode(TransactionStatus.CC_REFUND_REQUESTED_CODE);
								}
								tran.setTranStatDate(new Date());
								tranManager.updateTransaction(tran, userId);

							} else {
								//Handle DelayedSends status/substatus changes
								// SEN/BKS will go to SEN/BRR after AC cancel - it is odd, but will go to CXL/BRR
								// after Bank reversal
								if (tran.getTranStatCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)
										&& tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
									tran.setTranSubStatCode(TransactionStatus.BANK_REFUND_REQUEST_CODE);
								} else {
									tran.setTranStatCode(TransactionStatus.CANCELED_CODE);
								}
								tran.setTranStatDate(new Date());
								tranManager.updateTransaction(tran, userId);
							}

							ConsumerAccountType primaryAccountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
							try {
								refundPersonToPersonSuccessEmail(tran, primaryAccountType);
							} catch (Exception e) {
								// Email failed to get sent.
								// Not a serious error, log and move on.
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
							}
						} catch (Throwable t) {
							if (addAgentConnectErrorToTranComment(t, tranId, userId)) {
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error(
										"refundPersonToPerson AgentConnect reversal failed.", t);
								throw new EMGRuntimeException(t);
							} else {
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("refundPersonToPerson failed.", t);
								throw new EMGRuntimeException(t);
							}
						}
						return null;
					}
				};
				updateTransaction(tranId, userId, command);
			}

	public void recoveryOfLoss(final int tranId, final float amount, final float recoveredAmt, final String userId, final String fundCode,
			final String subReasonCode) throws TransactionAlreadyInProcessException, TransactionOwnershipException {
				UpdateTransactionCommand command = new UpdateTransactionCommand() {
					public Object execute() {
						Transaction tran = tranManager.getTransaction(tranId);
						if (tran == null) {
							throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
						}
						if (!tran.getTranStatCode().equalsIgnoreCase(TransactionStatus.SENT_CODE)
								|| !tran.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.WRITE_OFF_LOSS_CODE)) {
							throw new IllegalStateException("transaction id = " + tranId + " is not in a recovery of loss state");
						}
						TransactionManager manager = ManagerFactory.createTransactionManager();
						// Change the status so the stored procedure code records
						// ledger entries properly. For the moment, we call
						// update transaction as opposed to set transaction status
						// because set transaction status does not accept an amount.
						tran.setTranSubStatCode(fundCode);
						manager.updateTransaction(tran, userId, null, new Float(amount), subReasonCode);
						// int remain =
						// Math.round((totalAmt - amount - recoveredAmt) * 100F);
						// Change the status back to write off loss because the
						// preceding status is considered a transient status.
						// if (remain > 0)
						// {
						tran.setTranSubStatCode(TransactionStatus.WRITE_OFF_LOSS_CODE);
						manager.setTransactionStatus(tran.getEmgTranId(), tran.getStatus(), userId);
						// }
						return null;
					}
				};
				updateTransaction(tranId, userId, command, true, true);
				// TODO need to add code to update the sub-reason
			}

	public void doBankReversal(final int tranId, final String userId, final String contextRoot)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException {
				UpdateTransactionCommand command = new UpdateTransactionCommand() {
					public Object execute() {
						final Transaction tran = tranManager.getTransaction(tranId);
						if (tran == null) {
							throw new IllegalArgumentException(
									"unable to retrieve transaction with id = "
									+ tranId);
						}

						final int tranId = tran.getEmgTranId();

						if (!tran.getStatus().getSubStatusCode().equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)
								|| isWithinValidTimeFrame(tran, userId)) {
							//DO Adjustment/ Bank reversal
							final double amount = tran.getSndTotAmt().doubleValue();

							AdjustmentInquiryResponseMessage resp = null;
							try {
								BankPaymentProxy proxy = BankPaymentProxyImpl.getInstance();
								AdjustmentInquiryRequestMessage message = new AdjustmentInquiryRequestMessage();
								message.setClientTraceId(tran.getTCInternalTransactionNumber());
								message.setProcessorTraceId(tran.getTCProviderTransactionNumber());
								message.setTotalAmmount(tran.getSndTotAmt().toString());
								message.setTransactionType(BankPaymentProxyImpl.BANK_PAYMENT_ADJUSTMENT_TYPE);
								long beginTime = System.currentTimeMillis();
								resp = proxy.adjustmentInquiry(message);
								EventMonitor.getInstance().recordEvent(
										EventListConstant.BANK_PAYMENT_SERVICE_ADJUSTMENT,
										beginTime, System.currentTimeMillis());
								boolean success = false;
								if (resp.getResponseCode().equalsIgnoreCase(BankPaymentProxyImpl.BANK_PAYMENT_RESP_ADUSTMENT_SUCCESS)) {
									success = true;
								}

								CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
								commentService.insertBankPaymentServiceResponseComment("ADJUSTMENT", new Integer(tranId), success, userId, resp.getResponseCode(), null,
										resp.getDenialRecNbr(), resp.getAchStatus(),tran.getTCProviderTransactionNumber(), tran.getTCInternalTransactionNumber());

							} catch (Exception e) {
								CommentService commentService = emgshared.services.ServiceFactory.getInstance().getCommentService();
								commentService.insertBankPaymentServiceResponseComment("ADJUSTMENT", new Integer(tranId), false, userId, null, null, null, null,
										tran.getTCProviderTransactionNumber(), tran.getTCInternalTransactionNumber());
							}
							//Note: If errors with Bank reversal, it's ok.  EOC will handle after viewing Funding reports

						} else {
							try {
								tranManager.setTransactionComment(tranId, "OTH", "TELECHECK - Skipping adjustment it may be in ACH processing at Telecheck.", userId);
							} catch (Exception e) {
								EMGSharedLogger
								.getLogger(this.getClass().getName().toString())
								.warn("Failed to log comment about skipping adjustment.", e);
							}
						}
						return null;
					}
				};
				updateTransaction(tranId, userId, command);
			}

	private boolean isWithinValidTimeFrame(Transaction transaction, String userId) {

		List tas;
		try {
			tas = tranManager.getTranActions(transaction.getEmgTranId(), userId);
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to get tran actions:", e);
			return false;
		}
		Iterator iter = tas.iterator();
		TranAction ta = null;
		while (iter.hasNext()) {
			ta = (TranAction) iter.next();
			if (ta.getTranStatCode().equalsIgnoreCase(TransactionStatus.APPROVED_CODE) &&
					ta.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
				break;
			}
		}

		if (ta == null ||
				!ta.getTranStatCode().equalsIgnoreCase(TransactionStatus.APPROVED_CODE) ||
				!ta.getTranSubStatCode().equalsIgnoreCase(TransactionStatus.BANK_SETTLED_CODE)) {
			return false;
		}

		Calendar approvedDateTime = Calendar.getInstance();
		approvedDateTime.setTime(ta.getTranActionDate());

		//final TimeZone CST = TimeZone.getTimeZone("CST");
		Calendar lowerBoundLimit = Calendar.getInstance();
		Calendar upperBoundLimit = Calendar.getInstance();

		lowerBoundLimit = DateFormatter.setCalendarTimeFromHHMMString(eadp.getDsStartAchWindow(), lowerBoundLimit);
		upperBoundLimit = DateFormatter.setCalendarTimeFromHHMMString(eadp.getDsEndAchWindow(), upperBoundLimit);

		Calendar now = Calendar.getInstance();

		boolean sameDay = isSameDay(approvedDateTime, now);
		if (!sameDay) {
			return false;
		}

		//if it is now prior to start of processing window, can reverse today's approved items
		if (now.getTime().getTime() < lowerBoundLimit.getTime().getTime()) {
			return true;
		}

		//if it is between today's processing window, don't process anything
		if (now.getTime().getTime() >= lowerBoundLimit.getTime().getTime() &&
				now.getTime().getTime() <= upperBoundLimit.getTime().getTime()) {
			return false;
		}

		//if is after end of processing window and transaction approved after processing window can reverse
		if (now.getTime().getTime() >= upperBoundLimit.getTime().getTime() &&
				approvedDateTime.getTime().getTime() >= upperBoundLimit.getTime().getTime()) {
			return true;
		}

		return false;
	}

	public void refundPersonToPersonSuccessEmail(Transaction tran, ConsumerAccountType primaryAccountType) throws Exception {
		ConsumerAccount account;
		if (primaryAccountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					na.notifyRefundPersonToPersonTransaction(tran, consumerEmail,preferedlanguage);
				}
			}
		}
	}

	public void sendEconomyService(final int tranId, final String userId)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException,
			InvalidRRNException, AgentConnectException {
				final TransactionManager tranManager = ManagerFactory.createTransactionManager();
				final Transaction tran = tranManager.getTransaction(tranId);
				UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
					public Object execute() {
						if (tran == null) {
							throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
						}
						if (tran.isSendable() == false) {
							throw new IllegalStateException("transaction id = " + tranId + " is not in a sendable state.");
						}
						CommitTransactionRequest reqDoddFrank=null;
						CommitTransactionResponse respDoddFrank=null;

						Date before = new Date(System.currentTimeMillis());
						AgentConnectAccessorDoddFrank agentConnectAccessorDoddFrank=null;
						try {
							try {
								String rrn = tran.getRRN();
								if (rrn != null && rrn.startsWith("MG")) {
									AgentConnectServiceDoddFrank agentConnectServiceDoddFrank=null;
									MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
									String agtid = String.valueOf(tran.getSndAgentId());
									MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());
									agentConnectServiceDoddFrank=MGOServiceFactory.getInstance().getAgentConnectServiceDoddFrank();
									if (!agentConnectServiceDoddFrank.isRRNumberFound(mgoAgentProfile, rrn)) {
										throw new InvalidRRNException("RRN is invalid", rrn);
									}
								}
							} catch (Exception e0) {
								throw new AgentConnectException(e0);
							}
							MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
							String agtid = String.valueOf(tran.getSndAgentId());
							MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());
							agentConnectAccessorDoddFrank=new AgentConnectAccessorDoddFrank(mgoAgentProfile);
							long beginTime = System.currentTimeMillis();
							try {
								reqDoddFrank = buildCommitTransactionRequest(tran);
								if(reqDoddFrank.getToken()==null||reqDoddFrank.getToken().length()<1){
									reqDoddFrank.setToken("TEST");
								}
								try {
									EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...START...\n\n");
									respDoddFrank=agentConnectAccessorDoddFrank.commitTransaction(reqDoddFrank);
									EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...END...\n\n");
								} catch (Exception e) {
									EMGSharedLogger.getLogger(this.getClass().getName().toString()).debug("EMT Agent Connect v.13.05 ...ERROR...\n\n");
									acknowledgementFailEmail(tranId, respDoddFrank==null?"":respDoddFrank.getReferenceNumber(), e, userId);
									throw new AgentConnectException(e);
								}

							} catch (Exception e1) {
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("response failed.\n\n" , e1);
								throw new AgentConnectException(e1);
							} finally {
								EventMonitor.getInstance().recordEvent(EventListConstant.ES_AC_SEND, beginTime, System.currentTimeMillis());
							}
							String referenceNumber = respDoddFrank.getReferenceNumber();
							tran.setLgcyRefNbr(referenceNumber);
							tran.setSndTranDate(new Date());

							//S29a Add three minute phone number and three minute pin number
							if(tran.getDlvrOptnId()==DeliveryOption.WILL_CALL_ID ||tran.getDlvrOptnId()==DeliveryOption.ONLY_AT_ID)
							{
							tran.setThreeMinuteFreePhoneNumber(respDoddFrank.getTollFreePhoneNumber());
							tran.setThreeMinuteFreePinNumber(respDoddFrank.getFreePhoneCallPIN());
							}
							//MGO-12802 Changes starts, commenting the below if loop to avoid updating the new exchange rate while approving the transaction
							
							/*if (!"USA".equalsIgnoreCase(tran.getRcvISOCntryCode())) {
								BigDecimal exRate=null;
								exRate = getNewExchangeRateDoddFrank(
								        agentConnectAccessorDoddFrank,
								        tran.getRcvISOCntryCode(),
								        tran.getSndISOCntryCode(),
								        tran.getDlvrOptnId(),
										tran.getRcvISOCrncyId(),
										tran.getRcvPayoutISOCrncyId(),
										null,
										tran.getLoyaltyPgmMembershipId());
								// set to new foreign exchange rate
								if (exRate != null) {
									tran.setSndFxCnsmrRate(exRate);
									if (!tran.getRcvISOCrncyId()
									    .equals(tran.getRcvPayoutISOCrncyId())) {
									    
									} else {
									}
								}
							}
							*/
							//MGO-12802 Changes ends
							String refNbr = respDoddFrank.getPartnerConfirmationNumber();
							if (!StringHelper.isNullOrEmpty(refNbr)) {
								tran.setRcvAgentRefNbr(refNbr);
							}
							ConsumerAccountType accountType = ConsumerAccountType.getInstance(tran.getSndAcctTypeCode());
							tran.setTranStatCode(TransactionStatus.SENT_CODE);
							tran.setSndTranDate(new Date());
							tran.setTranAvailabilityDate(respDoddFrank.getExpectedDateOfDelivery());
							tranManager.updateTransaction(tran, userId, null, null);
							try {
								sendEconomyServiceSuccessEmail(tran, accountType);
							} catch (Exception e) {
								// Email failed to get sent.
								// Not a serious error, log and move on.
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
							}
						} catch (Throwable t) {
							addAgentConnectErrorToTranComment(t, tranId, userId);
							String msg = "In Program: TransactionServiceImpl\n" + "In Method: sendEconomyService()\n" + "AC call started at: "
							+ before + "\n" + "AC call aborted at: " + new Date(System.currentTimeMillis()) + "\n"
							+ "AC Method called: moneyGramSend() with a transaction id of " + tran.getEmgTranId()
							+ " and the request object is as follows:\n\n" + reqDoddFrank;
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("send Economy Service failed.\n\n" + msg, t);
							sendAgentConnectFailMail(msg, t, userId);
							return t;
						}
						return null;
					}
				};
//				Object obj = updateTransaction(tranId, userId, !isMgiTranId(tran)?command76:command1305);
				Object obj = updateTransaction(tranId, userId, command1305);
				if (obj != null) {
					throw new AgentConnectException((Throwable) obj);
				}
			}

	private void sendEconomyServiceSuccessEmail(Transaction tran, ConsumerAccountType accountType) throws Exception {
		ConsumerAccount account;
		if (accountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator<ConsumerEmail> iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = iter.next();
					na.notifySentTransaction(tran,consumerEmail,preferedlanguage);
				}
			}
		}
	}

	public void sendExpressPayment(final int tranId, final String userId, final String contextRoot)
			throws TransactionAlreadyInProcessException, TransactionOwnershipException,
			AgentConnectException {
				final TransactionManager tranManager = ManagerFactory.createTransactionManager();
				final Transaction tran = tranManager.getTransaction(tranId);
				
				
				
				
				UpdateTransactionCommand command1305 = new UpdateTransactionCommand() {
					public Object execute() {
						if (tran == null) {
							throw new IllegalArgumentException("unable to retrieve transaction with id = " + tranId);
						}
						if (tran.isSendable() == false) {
							throw new IllegalStateException("transaction id = " + tranId + " is not in a sendable state.");
						}
						com.moneygram.agentconnect1305.wsclient.FeeLookupResponse feeResponse=null;
						BpValidationRequest bpRequest=null;
						BpValidationResponse bpResponse=null;
						CommitTransactionRequest req = null;
						CommitTransactionResponse response = null;
						Date before = new Date(System.currentTimeMillis());
						try {
							MGOAgentProfileService maps = MGOServiceFactory.getInstance().getMGOAgentProfileService();
							String agtid = String.valueOf(tran.getSndAgentId());
							MGOAgentProfile mgoAgentProfile = maps.getMGOProfileByAgtId(agtid, tran.getEmgTranTypeCode());
							AgentConnectAccessorDoddFrank ac = new AgentConnectAccessorDoddFrank(mgoAgentProfile);
							long beginTime = System.currentTimeMillis();
							try {

								// The AC response will return an exception if there is
								// one
								// Send an email and then, throw an exception
								// add admin message
								try {
									feeResponse = bpfeeLookupCall(ac,tran);
								    tran.setMgiTransactionSessionID(feeResponse.getFeeInfo()[0].getMgiTransactionSessionID());
									bpRequest=buildBpValidationRequest(ac,tran, userId);
									bpResponse=ac.bpValidation(bpRequest);

									req = buildCommitTransactionRequest(tran);
									try{
									response = ac.commitTransaction(req);}
									catch(Exception r){
										EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("send Express Commit Transcation failed retrying.. Transcation Id" + tran.getEmgTranId());
										//Retry for RTS Real time, if error is 4200
											response = ac.commitTransaction(req);
									}
								} catch (Exception e) {
									String[] params = { String.valueOf(tran.getEmgTranId()), response.getReferenceNumber(),
											getTransactionUrl(contextRoot) };
									sendAdminMessage(tran.getCsrPrcsUserid(), getMessageResource("error.express.pay.send.fail.notice", params));
									// send email
									acknowledgementFailEmail(tranId, response==null?"":response.getReferenceNumber(), e, userId);
									throw new AgentConnectException(e);
								}
							} catch (Exception e1) {
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("response failed.\n\n" , e1);
								throw new AgentConnectException(e1);
							} finally {
								EventMonitor.getInstance().recordEvent(EventListConstant.EP_AC_SEND, beginTime, System.currentTimeMillis());
							}
							String referenceNumber = response.getReferenceNumber();
							tran.setReceiptTextInfoEng(getReceiptTextInfo(response.getReceiptTextInfo(), "ENG"));
							tran.setReceiptTextInfoSpa(getReceiptTextInfo(response.getReceiptTextInfo(), "SPA"));
							tran.setLgcyRefNbr(referenceNumber);
							tran.setSndTranDate(new Date());
							tran.setTranStatCode(TransactionStatus.SENT_CODE);
							tranManager.updateTransaction(tran, userId);
							tranManager.setTransactionStatus(tranId, tran.getTranStatCode(), tran.getTranSubStatCode(), userId);
							try {
								sendExpressPaymentSuccessMail(tran, ConsumerAccountType.getInstance(tran.getSndAcctTypeCode()));
							} catch (Exception e) {
								// Email failed to get sent. Not a serious error, log
								// and move on.
								EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to send Transaction Email:", e);
							}
						} catch (Throwable t) {
							addAgentConnectErrorToTranComment(t, tranId, userId);
							String msg = "In Program: TransactionServiceImpl\n" + "In Method: sendExpressPayment()\n" + "AC call started at: "
							+ before + "\n" + "AC call aborted at: " + new Date(System.currentTimeMillis()) + "\n"
							+ "AC Method called: expressPaySend() with the transaction id of " + tran.getEmgTranId() + "\n\n";
							EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("send Express Payment failed.\n\n" + msg, t);
							sendAgentConnectFailMail(msg, t, userId);
							return t;
						}
						return null;
					}
				};
//				Object obj = updateTransaction(tranId, userId, !isMgiTranId(tran)?command76:command1305);
				Object obj = updateTransaction(tranId, userId, command1305);
				if (obj != null) {
					throw new AgentConnectException((Throwable) obj);
				}
			}

	protected BpValidationRequest buildBpValidationRequest(AgentConnectAccessorDoddFrank ac,Transaction tran, String userId) throws Exception {
		BpValidationRequest req = new BpValidationRequest();
		req.setAmount(tran.getSndFaceAmt());
		req.setReceiveCode(tran.getRcvAgcyCode());
		DecryptionService decryptionService = emgadm.services.ServiceFactory.getInstance().getDecryptionService();
		String encryptedAccountNumber = tran.getRcvAgcyAcctEncryp();
		String decryptedAccountNumber = null;
		if (encryptedAccountNumber == null) { // it's always null when the
			// rcvAgcyAcctEncryp is a PAN
			decryptedAccountNumber = pciService.retrieveCardNumber(tran.getFormFreeConfNbr(), false);
		} else { // Not PAN, so use old decrypt method
			decryptedAccountNumber = decryptionService.decryptBankAccountNumber(encryptedAccountNumber);
		}
		req.setBillerAccountNumber(StringHelper.truncate(decryptedAccountNumber, 20));
		if(isBillerdoubleAccountNumber(ac,tran)){
			req.setValidateAccountNumber(StringHelper.truncate(decryptedAccountNumber, 20));
		}
		req.setDestinationCountry("USA");
		req.setSenderFirstName(StringHelper.truncate(tran.getSndCustFrstName(), 14));
		req.setSenderLastName(StringHelper.truncate(tran.getSndCustLastName(), 20));
		req.setProductVariant(com.moneygram.agentconnect1305.wsclient.ProductVariant.EP);
		req.setSendCurrency(tran.getSndISOCrncyId());
		req.setReceiveCurrency(tran.getRcvISOCrncyId());
		TransactionManager tm = ManagerFactory.createTransactionManager();
		CustAddress addr = tm.getCustAddress(tran.getSndCustAddrId());
		String addrText = addr.getAddrLine1Text();
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine2Text())) {
			addrText = addrText + " " + addr.getAddrLine2Text();
		}
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine3Text())) {
			addrText = addrText + " " + addr.getAddrLine3Text();
		}
		req.setSenderAddress(StringHelper.truncate(addrText, 30));
		req.setSenderCity(StringHelper.truncate(addr.getAddrCityName(), 20));
		req.setSenderState(StringHelper.truncate(addr.getAddrStateName(), 2));
		req.setSenderCountry(addr.getAddrCntryId());
		req.setAccountNumberRetryCount(new BigInteger("2")); //Nissan Biller - to always pass a �2� in the transaction attempt counter, regardless of the actually attempt number
		req.setSenderZipCode(StringHelper.truncate(addr.getAddrPostalCode(), 10));
		ConsumerProfile profile = profileService.getConsumerProfile(new Integer(tran.getSndCustId()), userId, "");
		if (!StringHelper.isNullOrEmpty(profile.getPhoneNumber())) {
			req.setSenderHomePhone(StringHelper.truncate(profile.getPhoneNumber(), 16));
		}
		req.setMessageField1(StringHelper.truncate(tran.getSndMsg1Text(), 40));
		req.setMessageField2(StringHelper.truncate(tran.getSndMsg2Text(), 33));
		req.setSenderOccupation(tran.getSndCustOccupationText());
		req.setSenderDOB(profile.getBirthdate());
		req.setSenderLegalIdNumber(tran.getSndCustLegalId());
		if (tran.getSndCustLegalIdTypeCode() != null) {
			com.moneygram.agentconnect1305.wsclient.LegalIdType lit = com.moneygram.agentconnect1305.wsclient.LegalIdType.fromString(tran.getSndCustLegalIdTypeCode());
			req.setSenderLegalIdType(lit);
		}
		req.setSenderPhotoIdNumber(tran.getSndCustPhotoId());
		req.setSenderPhotoIdState(tran.getSndCustPhotoIdStateCode());
		req.setSenderPhotoIdCountry(tran.getSndCustPhotoIdCntryCode());
		if (tran.getSndCustPhotoIdTypeCode() != null) {
			com.moneygram.agentconnect1305.wsclient.PhotoIdType pid = com.moneygram.agentconnect1305.wsclient.PhotoIdType.fromString(tran.getSndCustPhotoIdTypeCode());
			req.setSenderPhotoIdType(pid);
		}
		if (tran.getRcvAgentId() > 0) {
			req.setReceiveAgentID(String.valueOf(tran.getRcvAgentId()));
		}
		
		if(!StringUtility.isNullOrEmpty(tran.getRcvCustLastName())||!StringUtility.isNullOrEmpty(tran.getRcvCustFrstName())){
			req.setReceiverLastName(tran.getRcvCustLastName());
			req.setReceiverFirstName(tran.getRcvCustFrstName());
		}
		req.setFormFreeStaging(false);
		req.setMgiTransactionSessionID(tran.getMgiTransactionSessionID());
		if(tran.getRcvAgentId()>0)
			req.setReceiveAgentID(tran.getRcvAgentId()+"");
		else
			req.setReceiveAgentID(null);
		req.setPrimaryReceiptLanguage("ENG"); 
		req.setSecondaryReceiptLanguage("SPA");
		return req;
	}

	protected boolean isBillerdoubleAccountNumber(AgentConnectAccessorDoddFrank ac , Transaction tran) {
		com.moneygram.agentconnect1305.wsclient.BillerSearchResponse resp = null;
		boolean isdoubleBiller=false;
		try {
			resp = ac.billerSearch(tran.getRcvAgcyCode());
			if(resp!=null){
				BillerInfo billerDetails[]=resp.getBillerInfo();
				if(billerDetails==null || billerDetails.length>1) return false;
				for (int i = 0; i < billerDetails.length; i++) {
					isdoubleBiller=billerDetails[i].getDoubleAcctNumberEntryFlag().booleanValue();
				}
			}
		} catch (Exception e) {
			EMGSharedLogger.getLogger(this.getClass().getName().toString()).error("Failed to find the biller is double account or not", e);
			return isdoubleBiller;
		}
		return isdoubleBiller;
	}

	private BillPaymentSendRequest getBillPaySendRequest(Transaction tran, String userId) throws Exception {
		BillPaymentSendRequest req = new BillPaymentSendRequest();
		req.setAmount(tran.getSndFaceAmt());
		req.setReceiveCode(tran.getRcvAgcyCode());
		BillerLimit bl = emgshared.services.ServiceFactory.getInstance().getBillerLimitService().getBillerLimit(tran.getRcvAgcyCode());
		DecryptionService decryptionService = emgadm.services.ServiceFactory.getInstance().getDecryptionService();
		String encryptedAccountNumber = tran.getRcvAgcyAcctEncryp();
		String decryptedAccountNumber = null;
		if (encryptedAccountNumber == null) { // it's always null when the
			// rcvAgcyAcctEncryp is a PAN
			decryptedAccountNumber = pciService.retrieveCardNumber(tran.getFormFreeConfNbr(), false);
		} else { // Not PAN, so use old decrypt method
			decryptedAccountNumber = decryptionService.decryptBankAccountNumber(encryptedAccountNumber);
		}
		req.setBillerAccountNumber(StringHelper.truncate(decryptedAccountNumber, 20));
		req.setReceiveCountry("USA");
		req.setSenderFirstName(StringHelper.truncate(tran.getSndCustFrstName(), 14));
		req.setSenderLastName(StringHelper.truncate(tran.getSndCustLastName(), 20));
		req.setProductVariant(ProductVariant.EP);
		req.setSendCurrency(tran.getSndISOCrncyId());
		req.setReceiveCurrency(tran.getRcvISOCrncyId());
		TransactionManager tm = ManagerFactory.createTransactionManager();
		CustAddress addr = tm.getCustAddress(tran.getSndCustAddrId());
		String addrText = addr.getAddrLine1Text();
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine2Text())) {
			addrText = addrText + " " + addr.getAddrLine2Text();
		}
		if (!StringHelper.isNullOrEmpty(addr.getAddrLine3Text())) {
			addrText = addrText + " " + addr.getAddrLine3Text();
		}
		req.setSenderAddress(StringHelper.truncate(addrText, 30));
		req.setSenderCity(StringHelper.truncate(addr.getAddrCityName(), 20));
		req.setSenderState(StringHelper.truncate(addr.getAddrStateName(), 2));
		req.setSenderCountry(addr.getAddrCntryId());
		req.setTransactionAttemptNumber(new BigInteger("2")); //Nissan Biller - to always pass a �2� in the transaction attempt counter, regardless of the actually attempt number
		req.setSenderZipCode(StringHelper.truncate(addr.getAddrPostalCode(), 10));
		ConsumerProfile profile = profileService.getConsumerProfile(new Integer(tran.getSndCustId()), userId, "");
		if (!StringHelper.isNullOrEmpty(profile.getPhoneNumber())) {
			req.setSenderHomePhone(StringHelper.truncate(profile.getPhoneNumber(), 16));
		}
		req.setMessageField1(StringHelper.truncate(tran.getSndMsg1Text(), 40));
		req.setMessageField2(StringHelper.truncate(tran.getSndMsg2Text(), 33));
		req.setSenderOccupation(tran.getSndCustOccupationText());
		req.setSenderDOB(profile.getBirthdate());
		req.setSenderLegalIdNumber(tran.getSndCustLegalId());
		if (tran.getSndCustLegalIdTypeCode() != null) {
			LegalIdType lit = LegalIdType.fromString(tran.getSndCustLegalIdTypeCode());
			req.setSenderLegalIdType(lit);
		}
		req.setSenderPhotoIdNumber(tran.getSndCustPhotoId());
		req.setSenderPhotoIdState(tran.getSndCustPhotoIdStateCode());
		req.setSenderPhotoIdCountry(tran.getSndCustPhotoIdCntryCode());
		if (tran.getSndCustPhotoIdTypeCode() != null) {
			PhotoIdType pid = PhotoIdType.fromString(tran.getSndCustPhotoIdTypeCode());
			req.setSenderPhotoIdType(pid);
		}
        if (tran.getRcvAgentId() > 0) {
            req.setReceiveAgentID(String.valueOf(tran.getRcvAgentId()));
		} else {
		      req.setReceiveAgentID(bl.getId());
		}
		return req;
	}

	private void sendExpressPaymentSuccessMail(Transaction tran, ConsumerAccountType primaryAccountType) throws Exception {
		ConsumerAccount account;
		if (primaryAccountType.isCardAccount()) {
			account = accountService.getCreditCardAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		} else {
			account = accountService.getBankAccount(tran.getSndCustAcctId(), tran.getSndCustLogonId());
		}
		if (account != null) {
			ConsumerProfile profile = profileService.getConsumerProfile(new Integer(account.getConsumerId()), tran.getSndCustLogonId(), "");
			if (profile != null) {
				String preferedlanguage = profile.getPreferedLanguage();
				ConsumerEmail consumerEmail = null;
				// Loop sends out email to each address.
				NotificationAccess na = new NotificationAccessImpl();
				for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
					consumerEmail = (ConsumerEmail) iter.next();
					na.notifyExpressPaymentTransaction(tran, consumerEmail,preferedlanguage);
				}
			}
		}
	}

	protected boolean isMgiTranId(Transaction tran){
		if(tran.getMgiTransactionSessionID()!=null&&!tran.getMgiTransactionSessionID().equals("")){
			return true;
		}
		return false;
	}

	private String getReceiptTextInfo(TextTranslation[] rcptTextInfoa,
			String language) {
		if (rcptTextInfoa != null) {
			for (TextTranslation t : rcptTextInfoa) {
				if (language.equalsIgnoreCase(t.getLongLanguageCode())) {
					return t.getTextTranslation();
				}
			}
		}
		return null;

	}


}