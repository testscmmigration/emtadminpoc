/*
 * Created on Feb 11, 2005
 *
 */
package emgadm.services;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import emgadm.exceptions.CreditCardRefundException;
import emgadm.exceptions.InvalidRRNException;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.model.GlAccount;
import emgshared.exceptions.AgentConnectException;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;
import emgshared.model.ConsumerAccountType;
import emgshared.model.ConsumerProfileSearchCriteria;
import emgshared.model.Transaction;
import emgshared.services.CreditCardServiceResponse;

/**
 * @author A131
 */
public interface TransactionService
{

	// STATUS ALTERING METHODS
	public void requestOwnership(final int tranId, final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void takeOverOwnership(final int tranId, final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void releaseOwnership(final int tranId, final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	// HIGH LEVEL ACTIONS - Performs complex actions that result in status changes.
	/**
	 *
	 * @param tranId
	 * @param callerLoginId
	 * @param tranStat  - transaction status code
	 * @param fundStat  - transaction sub status code
	 * @param contextRoot TODO
	 * @returns ArrayList of error strings, or null if no errors.
	 */
	public List<String> processExpressPayTransaction(
			int tranId,
			String callerLoginId,
			String tranStat, // tranStatuscode
			String fundStat //tranSubStatusCode
			, String contextRoot
	)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException;

	/**
	 * @param tranId
	 * @param callerLoginId
	 * @param tranStat  - transaction status code
	 * @param fundStat  - transaction sub status code
	 * @param contextRoot TODO
	 * @returns ArrayList of error strings, or null if no errors.
	 */
	public List<String> processMoneyGramTransaction(
			int tranId,
			String callerLoginId,
			String tranStat, // tranStatuscode
			String fundStat //tranSubStatusCode
			, String contextRoot
	)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException,
	InvalidRRNException,
	DataSourceException,
	SQLException;

	/**
	 * @param tranId
	 * @param callerLoginId
	 * @param tranStat  - transaction status code
	 * @param fundStat  - transaction sub status code
	 * @param contextRoot TODO
	 * @returns ArrayList of error strings, or null if no errors.
	 */
	public List<String> approveDelayedSendTransaction(
			int tranId,
			String callerLoginId,
			String tranStat, // tranStatuscode
			String fundStat //tranSubStatusCode
			, String contextRoot
	)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException,
	InvalidRRNException,
	DataSourceException,
	SQLException;

	/**
	 * @param tranId
	 * @param callerLoginId
	 * @param tranStat  - transaction status code
	 * @param fundStat  - transaction sub status code
	 * @param contextRoot TODO
	 * @returns ArrayList of error strings, or null if no errors.
	 */
	public List<String> processDelayedSendTransaction(
			int tranId,
			String callerLoginId,
			String tranStat, // tranStatuscode
			String fundStat //tranSubStatusCode
			, String contextRoot
	)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException,
	InvalidRRNException,
	DataSourceException,
	SQLException;

	/**
	 * @param tranId
	 * @param callerLoginId
	 * @param tranStat  - transaction status code
	 * @param fundStat  - transaction sub status code
	 * @param contextRoot TODO
	 * @returns ArrayList of error strings, or null if no errors.
	 */
	public List<String> processEconomyServiceTransaction(
			int tranId,
			String callerLoginId,
			String tranStat, // tranStatuscode
			String fundStat //tranSubStatusCode
			, String contextRoot
	)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException,
	InvalidRRNException,
	DataSourceException,
	SQLException;

	/**
	 * @param tranId
	 * @param userId
	 * @param contextRoot TODO
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be funded.
	 * Created on Feb 15, 2005
	 */
	public CreditCardServiceResponse executeCreditCardAuth(
			final int tranId,
			final String userId, String contextRoot)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param authRequestId - the request id from the preceding authorization.  Must not be null.
	 * @param userId
	 * @param useBackupAccount
	 * @param contextRoot TODO
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId or when authRequestId is null.
	 * @throws IllegalStateException when transaction status implies the transaction should not be funded.
	 * Created on Apr 5, 2005
	 */

	public void fundPersonToPerson(
			final int tranId,
			final String authRequestId,
			final String userId,
			final boolean useBackupAccount,
			String contextRoot,
			final long effortId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param userId
	 * @param contextRoot TODO
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be sent.
	 * Created on Feb 15, 2005
	 */
	public void sendExpressPayment(final int tranId, final String userId, String contextRoot)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException;

	/**
	 * @param tranId
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be sent.
	 * Created on Feb 15, 2005
	 */

	public void sendPersonToPerson(
			final int tranId,
			final String userId,
			final boolean useBackupAccount)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	InvalidRRNException,
	AgentConnectException;

	public void sendEconomyService(final int tranId, final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	InvalidRRNException,
	AgentConnectException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be manually adjusted.
	 * Created on Mar 10, 2005
	 */
	public void recordLossAdjustment(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be manually adjusted.
	 * Created on Mar 10, 2005
	 */
	public void recordFeeAdjustment(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be manually adjusted.
	 * Created on Mar 10, 2005
	 */
	public void recordConsumerRefundOrRemittanceAdjustment(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * @throws IllegalStateException when transaction status implies the transaction should not be manually adjusted.
	 * Created on Mar 10, 2005
	 */
	public void recordMgActivityAdjustment(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * Created on Mar 10, 2005
	 */
	public void returnFee(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * Created on Mar 10, 2005
	 */
	public void collectFee(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param amount
	 * @param userId
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * Created on Mar 10, 2005
	 */
	public void returnFunding(
			final int tranId,
			final float amount,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param fee	- fee to charge credit card for ach returned
	 * @param userId	- login id of the caller
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 * Created on Mar 14, 2005
	 */
	public void resubmitAch(
			final int tranId,
			final Float fee,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	/**
	 * @param tranId
	 * @param tranStatus
	 * @param fundStatus
	 * @param userId	- login id of the caller
	 * @throws TransactionAlreadyInProcessException when transaction is already being processed by the system.
	 * @throws TransactionOwnershipException when the transaction is owned by a someone other than userId.
	 * @throws IllegalArgumentException when transaction cannot be retrieved based on tranId
	 *
	 * Created on Apr 14, 2005
	 */
	public void setTransactionStatus(
			final int tranId,
			final String tranStatus,
			final String fundStatus,
			final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void doBankReversal(final int tranId, final String userId,
			final String contextRoot)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void doCreditCardReversal(final int tranId, final String userId, String contextRoot)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void doCreditCardPartialRefund(final int tranId, final double refundAmount,
			final String userId, String contextRoot)
	throws TransactionAlreadyInProcessException, TransactionOwnershipException,
	CreditCardRefundException;

	public void refundPersonToPerson(final int tranId, final String userId)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException,
	AgentConnectException,
	EMGRuntimeException;

	public void refundPersonToPersonSuccessEmail(
			Transaction tran,
			ConsumerAccountType primaryAccountType) throws Exception;

	public void recoveryOfLoss(
			final int tranId,
			final float amount,
			final float recoveredAmt,
			final String userId,
			final String fundCode,
			final String subReasonCode)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void recordCcChargeback(
			final int tranId,
			final float amount,
			final String userId,
			final String fundCode,
			final String subReasonCode)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	public void confirmStatusChangeWithSubReason(
			final int tranId,
			final String userId,
			final String statusCode,
			final String subStatusCode,
			final String subReasonCode)
	throws TransactionAlreadyInProcessException,
	TransactionOwnershipException;

	void sendEconomyServicePendingEmail(Transaction tran, String userId)
	throws Exception;

	void sendEconomyServiceAchReturnEmail(Transaction tran, String userId)
	throws Exception;

	List<GlAccount> getGLAccounts(String userId) throws Exception;

	boolean failCaliforniaCompliance(int tranId, String userId);

	Transaction getTransactionForAutoProcess(String userId, String tranStatCode,
			String tranSubStatCode, String csrUserId,
			Date createDateTime, String partnerSiteId, String tranTypeCode)
	throws DataSourceException;

	Collection denyExpiredTrans();

	void updateLimboASPTrans();

	boolean sendConsumerAdhocSearchTraceMail(
			ConsumerProfileSearchCriteria cpsc, String userId);

}
