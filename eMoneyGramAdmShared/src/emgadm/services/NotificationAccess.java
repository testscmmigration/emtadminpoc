
package emgadm.services;

import emgshared.model.ConsumerEmail;
import emgshared.model.Transaction;
public interface NotificationAccess {

	public void notifySentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyPendingTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyExpressPaymentTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyCreditCardFailTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyRefundPersonToPersonTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyDeniedTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyACHReturnTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notifyNeedInfoTransaction(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
	public void notify48hoursIdUpload(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyIdPendingStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyIdDeniedStatus(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyIdApprovedWithoutTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);
   	public void notifyApprovedWithTX(Transaction transaction, ConsumerEmail consumerEmail, String preferedLanguage);

}
