package emgadm.sysmon;

public class EventStatView
{
	private String date;
	private String timePeriod;
	private String count;
	private String high;
	private String average;
	private String low;

	public String getDate()
	{
		return date;
	}

	public void setDate(String string)
	{
		date = string;
	}

	public String getTimePeriod()
	{
		return timePeriod;
	}

	public void setTimePeriod(String string)
	{
		timePeriod = string;
	}

	public String getCount()
	{
		return count;
	}

	public void setCount(String string)
	{
		count = string;
	}

	public String getHigh()
	{
		return high;
	}

	public void setHigh(String string)
	{
		high = string;
	}

	public String getAverage()
	{
		return average;
	}

	public void setAverage(String string)
	{
		average = string;
	}

	public String getLow()
	{
		return low;
	}

	public void setLow(String string)
	{
		low = string;
	}
}
