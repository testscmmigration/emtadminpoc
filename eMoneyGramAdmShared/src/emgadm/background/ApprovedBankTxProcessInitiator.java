package emgadm.background;

import java.util.Date;

import javax.servlet.ServletException;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgshared.exceptions.DataSourceException;

public class ApprovedBankTxProcessInitiator 
{
	private static final ApprovedBankTxProcessInitiator instance = new ApprovedBankTxProcessInitiator();
	public static final int RESPONSE_PROCESSED = 0;
	public static final int RESPONSE_CODE_NO_WORK = 200;
	public static final int RESPONSE_CODE_BUSY = 201;
	public static final int RESPONSE_BAD_PARM = 400;
	public static final int RESPONSE_INTERNAL_EXCEPTION = 500;
		
	private volatile Object lockObj = null;
	private ApprovedBankTxProcessWorker bob = null; // the Builder, of course. 

	private Logger logr = null;
	private ApprovedBankTxProcessInitiator()
	{
		// singleton
	}

	public static final ApprovedBankTxProcessInitiator getInstance()
	{
		return instance;
	}

	public ApprovedBankTxProcessorRequest executeRequest(ApprovedBankTxProcessorRequest bgpr)
			throws Exception {
		String act = bgpr.getRequestProcessingParameter();
		if ((act == null) || !act.equals("transaction")) {
			logr.error(this.getClass().getName()
					+ " Error in (ApprovedBankTx)BackGround Processor, invalid request parameter.");
			bgpr.setRequestResultCode(RESPONSE_BAD_PARM);
			return bgpr;
		}

		if (bob != null) {
			logr.debug("(ApprovedBankTx)Worker busy.");
			bgpr.setRequestResultCode(RESPONSE_CODE_BUSY);
			return bgpr;
		}

		if (lockObj == null) {
			init();
		}

		synchronized (lockObj) {
			try {
				if (bob != null) {
					logr.error("(ApprovedBankTx)sync block entered w/bob not null.");
					bgpr.setRequestResultCode(RESPONSE_CODE_BUSY);
					return bgpr;
				}

				logr.debug("(ApprovedBankTx)Back Ground Initiator looking for work.");

				// Execute!
				try {
					Date dateUntil = new Date(System.currentTimeMillis() - bgpr.getMinimumTimeToProcess());
					bob = ApprovedBankTxProcessWorker.getWorker(dateUntil); // getWorker
				} catch (DataSourceException dse) {
					logr.error("(ApprovedBankTx)Datasource Exception creating Worker. ", dse);
					bob = null;
					bgpr.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return bgpr;
				}

				if (bob == null) {
					logr.debug("(ApprovedBankTx)Background Initiator, no work to do.");
					bgpr.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return bgpr;
				}

				// Method!
				bob.processApprovedTransactions(bgpr.getContextRoot());
				logr.info("completed processApprovedBankTx(). updating stats.");
				BackgroundProcessControl.lastProcessed = bob.myTrans.getEmgTranId();
				BackgroundProcessControl.masterProcessedCount++;
				BackgroundProcessControl.setMasterProcessExceptionCount(BackgroundProcessControl
						.getMasterProcessExceptionCount() + 1);
				bgpr.setRequestResultCode(RESPONSE_PROCESSED);
				logr.info("(ApprovedBankTx)Processed Tran ID:" + bob.myTrans.getEmgTranId());
				return bgpr;
			} catch (Exception e) {
				logr.error("(ApprovedBankTx)Background Process Exception: ", e);
				bgpr.setRequestResultCode(RESPONSE_INTERNAL_EXCEPTION);
				return bgpr;
			} finally {
				bob = null;
			}
		}
	}

	public void init() throws ServletException
	{
		lockObj = new Object();
		logr = LogFactory.getInstance().getLogger(this.getClass());
	}
}