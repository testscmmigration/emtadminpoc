package emgadm.background;

import java.util.Date;
import java.util.List;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgshared.exceptions.DataSourceException;
import emgshared.model.IPDetails;
import emgshared.model.Transaction;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedDynProperties;

public class ApprovedBankTxProcessWorker {
	private static Logger logr = null;
	private static String CSRID = null;
	private static String CSRID_PREFX = null;
	private static final String USER_ID = "emgwww";
	private static final String PREMIER_AUTOMATED_PROCESS_CODE = "PRE";
	private static final String SCORING_AUTOMATED_PROCESS_CODE = "ASP";
	// private static final String MANUAL_PROCESS_CODE = "MAN";
	private static final String VELOCITY_FAILURE_PROCESS_CODE = "VEL";
	private static final String COUNTRY_EXCEPTION_PROCESS_CODE = "CEX";
	private static final String APP = TransactionStatus.APPROVED_CODE;
	private static final String NOF = TransactionStatus.NOT_FUNDED_CODE;
	private static final int SITE_COUNT = 3;
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();

	private TransactionService ts = ServiceFactory.getInstance().getTransactionService();
	protected Transaction myTrans = null;
	// temporary fix until stored procedures are changed


	static {
		logr = LogFactory.getInstance().getLogger("BackgroundProcessWorker");
		CSRID_PREFX = EMTAdmAppProperties.getSystemUserIdPrefix();
		try {
			logr.debug("(approved transaction processor)try to get localhost name");
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			String serverId = localMachine.getHostName().toUpperCase().replaceAll(".AD.MONEYGRAM.COM", "");
			CSRID = CSRID_PREFX + serverId;
		} catch (java.net.UnknownHostException uhe) {
			logr.error("static init exception while looking up server name", uhe);
			CSRID = CSRID_PREFX + " <unknown> ";
		}
		logr.debug("CSRID set to " + CSRID);
	}

	/**
	 * Factory Method.
	 *
	 * @return loaded worker with transaction, if no work to do, returns null.
	 * @throws DataSourceException
	 */
	protected static ApprovedBankTxProcessWorker getWorker(Date createDateTime) throws DataSourceException {
		logr.debug("isWorkToDo? find approved bank transactions not processed after delivery time (4 hours, initially)");
		ApprovedBankTxProcessWorker newBob = new ApprovedBankTxProcessWorker();
		// this bean is not fully populated.
		// only tranId and CSR fields have data.

		// Get next approved bank transaction (APP/BKS) created after specified date (initially, for longer than 4 hours)

		newBob.myTrans = newBob.ts.getTransactionForAutoProcess(USER_ID, TransactionStatus.APPROVED_CODE,
				TransactionStatus.BANK_SETTLED_CODE, CSRID, createDateTime,
				EMoneyGramAdmApplicationConstants.WAP_PARTNER_SITE_ID, TransactionType.DELAY_SEND_CODE);

		if (newBob.myTrans == null) {
			return null;
		}

		// Get a fully populated Transaction bean.
		newBob.myTrans = ManagerFactory.createTransactionManager().getTransaction(newBob.myTrans.getEmgTranId());
		IPDetails ipDetails = getIPDetailsFromLookupService(newBob.myTrans.getSndCustIPAddrId());
		newBob.myTrans.setIpDetails(ipDetails);
		logr.debug("Work to do (approved transaction processor) - myTrans.getEmgTranId(): " + newBob.myTrans.getEmgTranId());
		return newBob;
	}

	private ApprovedBankTxProcessWorker() {
	}

	public void processApprovedTransactions(final String contextRoot) {
		List<String> tranProcessError = null;
		try {
			tranProcessError = ts.processDelayedSendTransaction(myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
		} catch (Exception sqe) {
			logr.error("Exception in approved transaction process worker. myTrans.getEmgTranId():" + myTrans.getEmgTranId(), sqe);
		} finally {
			// RELEASE OWNERSHIP ( LOCK )
			try {
				logr.debug("call (approved transaction processor) releaseOwnership() - tranId:" + myTrans.getEmgTranId());
				ts.releaseOwnership(myTrans.getEmgTranId(), CSRID);
			} catch (TransactionAlreadyInProcessException taip) {
				logr.error("releaseOwnership (approved transaction processor) Exception - tranId:" + myTrans.getEmgTranId(), taip);
			} catch (TransactionOwnershipException toe) {
				logr.error("releaseOwnership (approved transaction processor) Exception - tranId:" + myTrans.getEmgTranId(), toe);
			}
		}
		if (tranProcessError != null) {
			String errsInLine = "";
			for (int i = 0; i < tranProcessError.size(); i++) {
				errsInLine += (String) tranProcessError.get(i) + "  ";
			}
			logr.error(myTrans.getEmgTranTypeCode() + " transaction #:" + myTrans.getEmgTranId() + " has returned error codes:"
					+ errsInLine);
		}
		logr.debug("(approved transaction processor) worker finished. tran:" + myTrans.getEmgTranId());
		return;
	}
	
	private static IPDetails getIPDetailsFromLookupService(String ipAddress) {

		logr.debug("Enter getIPDetailsFromLookupService.");

		// check PERFORM_IP_LOOKUP_FLAG is true .
		IPDetails ipDetails = new IPDetails();
		boolean isIpLookuponScoring = true;
		if (EMTAdmContainerProperties.isPerformIPLookUpFlag()) {

			ipDetails = IPLocatorService
					.getIPDetailsFromLookupService(ipAddress, isIpLookuponScoring);
		}

		logr.debug("Exit getIPDetailsFromLookupService.");

		return ipDetails;
	}
}