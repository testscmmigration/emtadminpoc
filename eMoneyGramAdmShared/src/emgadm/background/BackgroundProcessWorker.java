package emgadm.background;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;

import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.TransactionManager;
import emgadm.exceptions.TransactionAlreadyInProcessException;
import emgadm.exceptions.TransactionOwnershipException;
import emgadm.property.EMTAdmAppProperties;
import emgadm.property.EMTAdmContainerProperties;
import emgadm.services.NotificationAccess;
import emgadm.services.NotificationAccessImpl;
import emgadm.services.ServiceFactory;
import emgadm.services.TransactionScoring;
import emgadm.services.TransactionService;
import emgadm.services.iplookup.melissadata.client.IPLocatorException;
import emgadm.services.iplookup.melissadata.client.IPLocatorService;
import emgshared.dataaccessors.PropertyDAO;
import emgshared.exceptions.DataSourceException;
import emgshared.model.ConsumerEmail;
import emgshared.model.ConsumerProfile;
import emgshared.model.EMTTransactionType;
import emgshared.model.IPDetails;
import emgshared.model.PropertyBean;
import emgshared.model.PropertyKey;
import emgshared.model.ScoringSingleton;
import emgshared.model.Transaction;
import emgshared.model.TransactionScoreResult;
import emgshared.model.TransactionStatus;
import emgshared.model.TransactionType;
import emgshared.property.EMTSharedDynProperties;
import emgshared.services.ConsumerProfileService;
import emgshared.services.CountryExceptionService;


public class BackgroundProcessWorker {
	private static Logger logr = null;
	private static String CSRID = null;
	private static String CSRID_PREFX = null;
	private static final String USER_ID = "emgwww";
	private static final String PREMIER_AUTOMATED_PROCESS_CODE = "PRE";
	private static final String SCORING_AUTOMATED_PROCESS_CODE = "ASP";
	// private static final String MANUAL_PROCESS_CODE = "MAN";
	private static final String VELOCITY_FAILURE_PROCESS_CODE = "VEL";
	private static final String COUNTRY_EXCEPTION_PROCESS_CODE = "CEX";
	private static final String APP = TransactionStatus.APPROVED_CODE;
	private static final String NOF = TransactionStatus.NOT_FUNDED_CODE;
	private static EMTSharedDynProperties dynProps = new EMTSharedDynProperties();
	static {
		logr = LogFactory.getInstance().getLogger("BackgroundProcessWorker");
		CSRID_PREFX = EMTAdmAppProperties.getSystemUserIdPrefix();
		try {
			logr.debug("try to get localhost name");
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			String serverId = localMachine.getHostName().toUpperCase().replaceAll(".AD.MONEYGRAM.COM", "");
			CSRID = CSRID_PREFX + serverId;
		} catch (java.net.UnknownHostException uhe) {
			logr.error("static init exception while looking up server name", uhe);
			CSRID = CSRID_PREFX + " <unknown> ";
		}
		logr.debug("CSRID set to " + CSRID);
	}

	/**
	 * Factory Method.
	 *
	 * @return loaded worker with transaction, if no work to do, returns null.
	 * @throws DataSourceException
	 */
	protected static BackgroundProcessWorker getWorker() throws DataSourceException {
		logr.debug("isWorkToDo? call getTransactionForAutoProcess() ");
		BackgroundProcessWorker newBob = new BackgroundProcessWorker();
		// this bean is not fully populated.
		// only tranId and CSR fields have data.
		newBob.myTrans = newBob.ts.getTransactionForAutoProcess(USER_ID, TransactionStatus.AUTOMATIC_SCORING_PROCESS_CODE,
				TransactionStatus.NOT_FUNDED_CODE, CSRID, null, null, null);

		
		if (newBob.myTrans == null) {
			return null;
		}

		// get a fully populated Transaction bean.
		newBob.myTrans = ManagerFactory.createTransactionManager().getTransaction(newBob.myTrans.getEmgTranId());
		IPDetails ipDetails = getIPDetailsFromLookupService(newBob.myTrans.getSndCustIPAddrId());
		newBob.myTrans.setIpDetails(ipDetails);
		newBob.tm.updateTransaction(newBob.myTrans, CSRID);
		
		// getting correct localized transaction service instance
		newBob.ts = ServiceFactory.getInstance().getTransactionService(newBob.myTrans.getPartnerSiteId());
		logr.debug("Work to do - myTrans.getEmgTranId():" + newBob.myTrans.getEmgTranId());
		return newBob;
	}

	private BackgroundProcessWorker() {
	}

	private TransactionService ts = ServiceFactory.getInstance().getTransactionService();
	private TransactionScoring tscore = ServiceFactory.getInstance().getTransactionScoring();
	private TransactionManager tm = ManagerFactory.createTransactionManager();
	private ConsumerProfileService cps = emgshared.services.ServiceFactory.getInstance().getConsumerProfileService();
	private CountryExceptionService ces = emgshared.services.ServiceFactory.getInstance().getCountryExceptionService();
	private EMTSharedDynProperties dProp = new EMTSharedDynProperties();
	protected Transaction myTrans = null;

	public void scoreAndProcessTransaction(final String contextRoot) {
		try {
			// change status to FFS/NOF
			// if an Exception occurs the transaction will be accessible
			tm.setTransactionStatus(myTrans.getEmgTranId(), TransactionStatus.FORM_FREE_SEND_CODE,
			// use fixed fields because the transaction bean is
					// incomplete.
					TransactionStatus.NOT_FUNDED_CODE, CSRID);
			// If it's in the queue, we always score. It wouldn't be there
			// otherwise.
			// this also saves the score in the database.
			Map<String,Object> transactionScoreMap = tscore.getTransactionScore(CSRID, myTrans.getEmgTranId());
			// now in FFS/NOF and scored. Shall we do more? ( autoProcess, i.e.
			// autoApprove? )
			EMTTransactionType tt = new EMTTransactionType();
			tt.setEmgTranTypeCode(myTrans.getEmgTranTypeCode());
			tt.setPartnerSiteId(myTrans.getPartnerSiteId());
			tt.setEmgTranTypeCodeLabel(myTrans.getEmgTranTypeCode());
			ConsumerProfile cp = cps.getConsumerProfile(new Integer(myTrans.getCustId()), CSRID, USER_ID);
			// check to see if the customer is a premier customer
			boolean premierCustomer = cp.getCustPrmrCode().equalsIgnoreCase("P");
			dProp.checkForRefreshDynProperties(CSRID);
			// see if Premier Automation Prop is on
			boolean premierEPAutomation = dProp.isPrmrEPAllowed();
			boolean premierESAutomation = dProp.isPrmrESAllowed();
			boolean premierMGAutomation = dProp.isPrmrMTAllowed();
			boolean premierDSAutomation = dProp.isPrmrDSAllowed();
			// default to premierAutomated=false
			boolean premierAutomated = false;
			// get the trans type
			boolean isDSTran = myTrans.getEmgTranTypeCode().equals(TransactionType.DELAY_SEND_CODE);
			boolean isEPTran = myTrans.getEmgTranTypeCode().equals(TransactionType.EXPRESS_PAYMENT_SEND_CODE);
			boolean isESTran = myTrans.getEmgTranTypeCode().equals(TransactionType.MONEY_TRANSFER_ECONOMY_SEND_CODE);
			boolean isMGTran = myTrans.getEmgTranTypeCode().equals(TransactionType.MONEY_TRANSFER_SEND_CODE);
			// if P2P and Premier Prop on and Premier Cust, then automate.
			if (isMGTran && premierMGAutomation && premierCustomer)
				premierAutomated = true;
			// if EP and Premier Prop on and Premier Cust, then automate.
			if (isEPTran && premierEPAutomation && premierCustomer)
				premierAutomated = true;
			// if ES and Premier Prop on and Premier Cust, then automate.
			if (isESTran && premierESAutomation && premierCustomer)
				premierAutomated = true;
			// if DS and Premier Prop on and Premier Cust, then automate.
			if (isDSTran && premierDSAutomation && premierCustomer)
				premierAutomated = true;
			if (!premierAutomated && ScoringSingleton.getInstance().isAutoApprove(tt) == false)
				return; // no. get outta here. ( after doing finally{}... )
			String src = ((TransactionScoreResult)
					transactionScoreMap.get("Transaction Score Result")).getResultCode(); // scoring result code.
			// System.out.println("BG: Check if REJECTED...");
			/** REJECT ! -- fuggedaboutit * */
			if (!premierAutomated && src.equals("REJ")) {
				tm.setTransactionStatus(myTrans.getEmgTranId(), TransactionStatus.DENIED_CODE, TransactionStatus.NOT_FUNDED_CODE, CSRID);
				logr.debug("Decision: Reject Transaction:" + myTrans.getEmgTranId());
				try {
					// Send deny email.
					Transaction tns = tm.getTransaction(myTrans.getEmgTranId());
					sendDenyEmail(tns);
				} catch (Exception e) {
					logr.error("Send Email failed for tran#:" + myTrans.getEmgTranId());
				}
				return;
			}
			
			// MGO-12694: Added below code to put Fraud Billers in Manual Review
			if(isEPTran){
				String billerList = EMTAdmContainerProperties.getBillerReviewList();
				if(null != myTrans.getRcvAgcyCode() && StringUtils.isNotBlank(myTrans.getRcvAgcyCode()) 
						&& null != billerList && StringUtils.isNotEmpty(billerList)){
					boolean isBillerExist = billerSplit(billerList.trim(), myTrans.getRcvAgcyCode());
					if (isBillerExist) {
						// The Biller requires manual review of the
						// transaction regardless of scoring and premier status
						myTrans.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
						myTrans.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
						tm.updateTransaction(myTrans, CSRID);
						logr.debug("Decision: Biller Review required - Manual Review needed:" + myTrans.getEmgTranId());
						return;
					}
				}
			}
			//System.out.println("BG: REVIEW.... But first Country Exception...."
			// );
			// Check for Destination Country Override
			if (!isEPTran && ces.isCountryExceptionOverride(myTrans.getRcvISOCntryCode())) {
				// this destination country requires manual review of the
				// transaction regardless of scoring and premier status
				myTrans.setTranRvwPrcsCode(COUNTRY_EXCEPTION_PROCESS_CODE);
				myTrans.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
				myTrans.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
				tm.updateTransaction(myTrans, CSRID);
				logr.debug("Decision: Country Exception - Manual Review needed:" + myTrans.getEmgTranId());
				return;
			}
			// System.out.println(
			// "BG: Apparently not Country Exception, so continue the journey...."
			// );
			/** REVIEW -- I dunno, let a human figure it out. * */
			if (!premierAutomated && src.equals("REVW")) {
				logr.debug("Decision: Review Transaction " + myTrans.getEmgTranId());
				return;
			}
			// if it's ready to automate ...
			if (premierAutomated || src.equals("APRV")) {
				// check to see if it fails velocity test ...
				if (!isVelocityOKToAutomate(myTrans)) {
					myTrans.setTranRvwPrcsCode(VELOCITY_FAILURE_PROCESS_CODE);
					myTrans.setTranStatCode(TransactionStatus.FORM_FREE_SEND_CODE);
					myTrans.setTranSubStatCode(TransactionStatus.NOT_FUNDED_CODE);
					tm.updateTransaction(myTrans, CSRID);
					logr.debug("Decision: Velocity Failure - Manual Review needed:" + myTrans.getEmgTranId());
					return;
				}
			}

			// passed Velocity and is Premier or it scored well. Send it out.
			if (premierAutomated || src.equals("APRV")) {
				// AutoProcess the transaction.
				logr.debug("Processing Transaction:" + myTrans.getEmgTranId());
				ts.setTransactionStatus(myTrans.getEmgTranId(), APP, NOF, CSRID);
				// set the automation indicator on the trans
				// TransactionStatus tranStat = new TransactionStatus();
				myTrans.setTranStatCode(APP);
				myTrans.setTranSubStatCode(NOF);
				if (premierAutomated)
					myTrans.setTranRvwPrcsCode(PREMIER_AUTOMATED_PROCESS_CODE);
				else
					myTrans.setTranRvwPrcsCode(SCORING_AUTOMATED_PROCESS_CODE);
				tm.updateTransaction(myTrans, CSRID);
				List<String> tranProcessError = null;
				try {
					/** EXPRESS PAYMENT * */
					if (isEPTran) {
						tranProcessError = ts.processExpressPayTransaction(myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
					}
					/** ECONOMY SEND * */
					else if (isESTran) {
						/*
						 * Don't process!, just call Steve's handy dandy
						 * sendEconomyServicePendingEmail, which not only sends
						 * the consumer an email, but also changes the status to
						 * APP/ACH as a secret bonus!
						 */
						/*
						 * tranProcessError =
						 * ts.processEconomyServiceTransaction(
						 * myTrans.getEmgTranId(), CSRID, APP, NOF);
						 */
						ServiceFactory.getInstance().getTransactionService().sendEconomyServicePendingEmail(
								tm.getTransaction(myTrans.getEmgTranId()), USER_ID);
					}
					/** PERSON TO PERSON - MONEYGRAM SEND - SAME DAY * */
					else if (isMGTran) {
						tranProcessError = ts.processMoneyGramTransaction(myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
					}
					/** AFFILIATE PROGRAM - DELAYED SEND - FOUR HOURS * */
					else if (isDSTran) {
						tranProcessError = ts.approveDelayedSendTransaction(myTrans.getEmgTranId(), CSRID, APP, NOF, contextRoot);
					}
					/** UNKNOWN TRANS TYPE * */
					else if (!isDSTran && !isEPTran && !isESTran && !isMGTran) {
						logr.error("can't recognize transaction type. tran:" + myTrans.getEmgTranId() + " type code:"
								+ myTrans.getEmgTranTypeCode());
					}
				} catch (Exception e) {
					logr.error("transaction process exception, myTrans.getEmgTranId():" + myTrans.getEmgTranId(), e);
				}
				if (tranProcessError != null) {
					String errsInLine = "";
					for (int i = 0; i < tranProcessError.size(); i++)
						errsInLine += (String) tranProcessError.get(i) + "  ";
					logr.error(myTrans.getEmgTranTypeCode() + " transaction #:" + myTrans.getEmgTranId() + " has returned error codes:"
							+ errsInLine);
				}
			} else {
				logr.error("unknown transaction score result code: " + src + " for tran:" + myTrans.getEmgTranId() + "  cannot process.");
			}
		} catch (Exception sqe) {
			logr.error("Exception in worker. myTrans.getEmgTranId():" + myTrans.getEmgTranId(), sqe);
		} finally {
			// RELEASE OWNERSHIP ( LOCK )
			try {
				logr.debug("call releaseOwnership() - tranId:" + myTrans.getEmgTranId());
				ts.releaseOwnership(myTrans.getEmgTranId(), CSRID);
			} catch (TransactionAlreadyInProcessException taip) {
				logr.error("releaseOwnership Exception - tranId:" + myTrans.getEmgTranId(), taip);
			} catch (TransactionOwnershipException toe) {
				logr.error("releaseOwnership Exception - tranId:" + myTrans.getEmgTranId(), toe);
			}
		}
		logr.debug("worker finished. tran:" + myTrans.getEmgTranId());
		return;
	}

	// this method will apply the automation velocity filter logic.
	// even if a trans scores well and/or the profile is Premier, the velocity
	// check will prevent a compromised profile from automating more than
	// defined "X" trans in
	// a defined "Y" period of time. "X" and "Y" are re-read each time this
	// method executes
	// to get the lastest settings ... no caching of these settings.
	private boolean isVelocityOKToAutomate(Transaction tran) {
		PropertyKey pk = new PropertyKey();
		pk.setPropName(EMTSharedDynProperties.MAXIMUMNUMBERAUTOMATEDPERPERIOD);
		Map propMap = PropertyDAO.getPropertiesByKey(CSRID, pk, true);
		PropertyBean pb = (PropertyBean) propMap.get(pk.getPropName());
		Integer maxNumAutoPerPeriod = new Integer(pb.getPropKey().getPropVal());
		pk.setPropName(EMTSharedDynProperties.AUTOMATIONPERIODINHRS);
		propMap = PropertyDAO.getPropertiesByKey(CSRID, pk, true);
		pb = (PropertyBean) propMap.get(pk.getPropName());
		Integer automationPeriodHrs = new Integer(pb.getPropKey().getPropVal());
		long chkBackInMilliSecs = automationPeriodHrs.intValue() * 60 * 60 * 1000;
		Integer consID = new Integer(tran.getSndCustId());
		List transactions;
		int recentTransMeetingCriteriaCount = 0;
		try {
			transactions = tm.getTransactionsForVelocityCheck(consID, CSRID);
			// careful, this procedure returns sparsely populated transactions
			// for efficiency reasons
			for (Iterator iter = transactions.iterator(); iter.hasNext();) {
				Transaction transaction = (Transaction) iter.next();
				// don't look at the current one ...
				if (transaction.getEmgTranId() != tran.getEmgTranId()) {
					// limit to transactions in the created in the last "Y"
					// seconds.
					if ((System.currentTimeMillis() - transaction.getCreateDate().getTime()) < chkBackInMilliSecs) {
						recentTransMeetingCriteriaCount++;
					}
				}
			}
		} catch (Exception e1) {
			return false;
		}
		if (recentTransMeetingCriteriaCount >= maxNumAutoPerPeriod.intValue())
			return false;
		else
			return true;
	}

	// TODO Remove hardcoded resource file.
	private static final String resourceFile = "emgadm.resources.ApplicationResources";

	// This is cutNpaste under the gun from TransactionDetailAction.
	// refactor in a smart way, please.
	private void sendDenyEmail(Transaction tran) throws Exception {
		ConsumerProfile profile = emgshared.services.ServiceFactory.getInstance()
		.getConsumerProfileService().getConsumerProfile(new Integer(tran.getCustId()), tran.getSndCustLogonId(), "");
		if (profile != null) {
			String preferedlanguage = profile.getPreferedLanguage();
			ConsumerEmail consumerEmail = null;
			// Loop sends out email to each address.
			NotificationAccess na = new NotificationAccessImpl();
			for (Iterator iter = profile.getEmails().iterator(); iter.hasNext();) {
				consumerEmail = (ConsumerEmail) iter.next();
				na.notifyDeniedTransaction(tran, consumerEmail,preferedlanguage);
			}
		}
	}
	
	private static IPDetails getIPDetailsFromLookupService(String ipAddress) {

		logr.debug("Enter getIPDetailsFromLookupService.");

		// check PERFORM_IP_LOOKUP_FLAG is true .
		IPDetails ipDetails = new IPDetails();
		boolean isIpLookuponScoring = true;
		if (EMTAdmContainerProperties.isPerformIPLookUpFlag()) {

			ipDetails = IPLocatorService
					.getIPDetailsFromLookupService(ipAddress, isIpLookuponScoring);
		}

		logr.debug("Exit getIPDetailsFromLookupService.");

		return ipDetails;
	}
	
	private boolean billerSplit(String billerList, String rcvAgcyCode){
		boolean isExist = false;
		String[] billerSplit = billerList.split("\\,");  
		if(billerSplit.length > 0){
			for(String biller: billerSplit){
			 if(biller.equals(rcvAgcyCode)){
				 isExist = true;
			 }
			}
		} 
		return isExist;
	}
}
