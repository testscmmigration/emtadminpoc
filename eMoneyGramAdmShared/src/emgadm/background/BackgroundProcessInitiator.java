package emgadm.background;

import javax.servlet.ServletException;
import com.moneygram.common.log.LogFactory;
import com.moneygram.common.log.Logger;
import emgshared.exceptions.DataSourceException;

public class BackgroundProcessInitiator 
{
	private static final BackgroundProcessInitiator instance = new BackgroundProcessInitiator();
	public static final int RESPONSE_PROCESSED = 0;
	public static final int RESPONSE_CODE_NO_WORK = 200;
	public static final int RESPONSE_CODE_BUSY = 201;
	public static final int RESPONSE_BAD_PARM = 400;
	public static final int RESPONSE_INTERNAL_EXCEPTION = 500;
		
	private Object lockObj = null;
	private BackgroundProcessWorker bob = null; // the Builder, of course. 

	private Logger logr = null;
	private BackgroundProcessInitiator()
	{
		// singleton
	}

	public static final BackgroundProcessInitiator getInstance()
	{
		return instance;
	}

	public BackGroundProcessorRequest executeBackGroundRequest(BackGroundProcessorRequest request) throws Exception
	{
		String act = request.getRequestProcessingParameter();
		if (act == null || (!act.equals("transaction")))
		{
			logr.error(this.getClass().getName()+ " Error in BackGround Processor, invalid request parameter.");			
			request.setRequestResultCode(RESPONSE_BAD_PARM);
			return request;
		}

		if (bob != null)
		{
			logr.debug("Worker busy.");
			request.setRequestResultCode(RESPONSE_CODE_BUSY);
			return request;
		}

		if (lockObj==null)
			init();
		
		synchronized (lockObj)
		{
			try
			{
				if (bob != null)
				{
					logr.error("sync block entered w/bob not null.");
					request.setRequestResultCode(RESPONSE_CODE_BUSY);
					return request;
				}

				logr.debug("Back Ground Initiator looking for work.");
				try
				{
					bob = BackgroundProcessWorker.getWorker();
				} catch (DataSourceException dse)
				{
					logr.error("Datasource Exception creating Worker. ", dse);
					bob = null;
					request.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return request;
				}

				if (bob == null)
				{
					logr.debug("Background Initiator, no work to do.");
					request.setRequestResultCode(RESPONSE_CODE_NO_WORK);
					return request;
				}

				bob.scoreAndProcessTransaction(request.getContextRoot());
				logr.info("completed scoreAndProcessTransaction(). updating stats.");
				BackgroundProcessControl.lastProcessed =	bob.myTrans.getEmgTranId();
				BackgroundProcessControl.masterProcessedCount++;
				BackgroundProcessControl.setMasterProcessExceptionCount(BackgroundProcessControl.getMasterProcessExceptionCount()+1);
				request.setRequestResultCode(RESPONSE_PROCESSED);
				logr.info("Processed Tran ID:" + bob.myTrans.getEmgTranId());
				return request;
			} catch (Exception e)
			{
				logr.error("Background Process Exception: ", e);
				request.setRequestResultCode(RESPONSE_INTERNAL_EXCEPTION);
				return request;
			} finally
			{
				bob = null;
			}
		}
	}

	public void init() throws ServletException
	{
		lockObj = new Object();
		logr = LogFactory.getInstance().getLogger(this.getClass());
	}

	
}
