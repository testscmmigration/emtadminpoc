/*
 * Created on Jan 18, 2005
 *
 */
package emgadm.view;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import emgshared.model.AccountStatus;
import emgshared.model.ConsumerBankAccount;
import emgshared.model.MicroDeposit;
import emgshared.model.TaintIndicatorType;

/**
 * This view object is intended to be used by
 * the presentation layer.  It is similar to
 * a struts form object, and is necessary when
 * dealing with a collection of items.
 * 
 * @author A131
 *
 */
public class ConsumerBankAccountView
{
	private String accountId;
	private String accountNumberMask;
	private String routingNumber;
	private String financialInstitutionName;
	private String statusCode;
	private String subStatusCode;
	private String accountTypeCode;
	private String accountTypeDesc;
	private String accountTaintText;
	private String bankAbaTaintText;
	private List cmntList;
	private MicroDeposit microDeposit;

	public ConsumerBankAccountView()
	{
	}

	/**
	 * @param account
	 */
	public ConsumerBankAccountView(ConsumerBankAccount account)
	{
		if (account != null)
		{
			this.accountId = String.valueOf(account.getId());
			this.accountNumberMask = account.getAccountNumberMask();
			this.routingNumber = account.getABANumber();
			this.financialInstitutionName =
				account.getFinancialInstitutionName();
			this.statusCode = account.getStatusCode();
			this.subStatusCode = account.getSubStatusCode();
			this.accountTypeCode = account.getType().getCode();
			this.accountTypeDesc = account.getTypeDescription();

			if (account.isAccountNumberBlocked()) {
				this.accountTaintText = TaintIndicatorType.BLOCKED_TEXT;
			} else if (account.isAccountNumberOverridden()) {
				this.accountTaintText = TaintIndicatorType.OVERRIDE_TEXT;
			} else if (account.isAccountNumberNotBlocked()) {
				this.accountTaintText = TaintIndicatorType.NOT_BLOCKED_TEXT;
			} else {
				this.accountTaintText = "Unknown";
			}
			
			if (account.isBankAbaNumberBlocked()) {
				this.bankAbaTaintText = TaintIndicatorType.BLOCKED_TEXT;
			} else if (account.isBankAbaNumberOverridden()) {
				this.bankAbaTaintText = TaintIndicatorType.OVERRIDE_TEXT;
			} else if (account.isBankAbaNumberNotBlocked()) {
				this.bankAbaTaintText = TaintIndicatorType.NOT_BLOCKED_TEXT;
			} else {
				this.bankAbaTaintText = "Unknown";
			}
			
			this.cmntList = account.getCmntList();
		}
	}

	public String getAccountNumberMask()
	{
		return accountNumberMask;
	}

	public String getFinancialInstitutionName()
	{
		return financialInstitutionName;
	}

	public void setAccountNumberMask(String string)
	{
		accountNumberMask = string;
	}

	public void setFinancialInstitutionName(String string)
	{
		financialInstitutionName = string;
	}

	public String getAccountId()
	{
		return accountId;
	}

	public String getRoutingNumber()
	{
		return routingNumber;
	}

	public void setRoutingNumber(String string)
	{
		routingNumber = string;
	}

	public String getStatusCode()
	{
		return statusCode;
	}

	public String getSubStatusCode()
	{
		return subStatusCode;
	}

	public void setStatusCode(String string)
	{
		statusCode = string;
	}

	public void setSubStatusCode(String string)
	{
		subStatusCode = string;
	}

	public String getCombinedStatusCode()
	{
		return this.statusCode + AccountStatus.DELIMITER + this.subStatusCode;
	}

	public void setCombinedStatusCode(String combinedStatusCode)
	{
		String[] codes =
			StringUtils.split(combinedStatusCode, AccountStatus.DELIMITER);
		this.statusCode = (codes.length > 0 ? codes[0] : null);
		this.subStatusCode = (codes.length > 1 ? codes[1] : null);
	}

	public String getAccountTypeDesc()
	{
		return accountTypeDesc;
	}

	public void setAccountTypeDesc(String string)
	{
		accountTypeDesc = string;
	}

	public String getAccountTypeCode()
	{
		return accountTypeCode;
	}

	public void setAccountTypeCode(String string)
	{
		accountTypeCode = string;
	}
	/**
	 * @return
	 */
	public String getAccountTaintText() {
		return accountTaintText;
	}

	/**
	 * @param string
	 */
	public void setAccountTaintText(String string) {
		accountTaintText = string;
	}

	/**
	 * @return
	 */
	public String getBankAbaTaintText() {
		return bankAbaTaintText;
	}

	/**
	 * @param string
	 */
	public void setBankAbaTaintText(String string) {
		bankAbaTaintText = string;
	}

	/**
	 * @return
	 */
	public List getCmntList()
	{
		return cmntList;
	}

	/**
	 * @param list
	 */
	public void setCmntList(List list)
	{
		cmntList = list;
	}

	/**
	 * @return
	 */
	public MicroDeposit getMicroDeposit()
	{
		return microDeposit;
	}

	/**
	 * @param deposit
	 */
	public void setMicroDeposit(MicroDeposit deposit)
	{
		microDeposit = deposit;
	}

}
