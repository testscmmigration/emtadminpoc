/*
 * Created on Feb 25, 2004
 *
 */
package emgadm.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import emgshared.model.TransactionType;

/**
 * This class contains 'convenient' static methods
 * for working with dates.  There isn't any reason
 * to instantiate an object of this type, so the
 * construtor has been made private.
 * 
 * @author A131
 *
 */

public final class DateHelper
{
	public static final String DATE_TIME_PATTERN_DEFAULT = "dd/MMM/yyyy HH:mm:ss"; //$NON-NLS-1$
	public static final String DATE_TIME_AMPM_PATTERN_DEFAULT = "dd/MMM/yyyy hh:mm:ss a"; //$NON-NLS-1$
	public static final String DATE_TIME_REPORT_DISPLAY_PATTERN = "dd/MMM/yyyy hh:mm a z"; //$NON-NLS-1$
	public static final String TIME_24HOUR_PATTERN_DEFAULT = "HH:mm:ss"; //$NON-NLS-1$
	public static final String TIME_AMPM_PATTERN_DEFAULT = "h:mm:ss a"; //$NON-NLS-1$

	public static final String PACIFIC_STANDARD_TIMEZONE_ID = "America/Los_Angeles"; //$NON-NLS-1$	
	public static final String CENTRAL_STANDARD_TIMEZONE_ID = "America/Chicago"; //$NON-NLS-1$
	public static final String MOUNTAIN_STANDARD_TIMEZONE_ID = "America/Denver"; //$NON-NLS-1$
	public static final String EASTERN_STANDARD_TIMEZONE_ID = "America/New_York"; //$NON-NLS-1$
	
	public static final String DATE_REGEX_PATTERN = "(0[1-9]|[12][0-9]|3[01])[ /](Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[ /]([0-9]{4})";

	private DateHelper()
	{}

	public static DateFormat getDateTimeFormatter()
	{
		return (getDateTimeFormatter(null));
	}

	public static DateFormat getDateTimeFormatter(TimeZone timeZone)
	{
		return (getFormatter(DATE_TIME_PATTERN_DEFAULT, timeZone));
	}

	public static DateFormat getDateTimeDisplayFormatter(TimeZone timeZone)
	{
		return (getFormatter(DATE_TIME_REPORT_DISPLAY_PATTERN, timeZone));
	}

	public static DateFormat getTime24HourFormatter()
	{
		return (getTime24HourFormatter(null));
	}

	public static DateFormat getTime24HourFormatter(TimeZone timeZone)
	{
		return (getFormatter(TIME_24HOUR_PATTERN_DEFAULT, timeZone));
	}

	public static DateFormat getTimeAmPmFormatter()
	{
		return (getTimeAmPmFormatter(null));
	}

	public static DateFormat getTimeAmPmFormatter(TimeZone timeZone)
	{
		return (getFormatter(TIME_AMPM_PATTERN_DEFAULT, timeZone));
	}

	public static DateFormat getFormatter(String pattern, TimeZone timeZone)
	{
		DateFormat formatter = (StringHelper.isNullOrEmpty(pattern) ? new SimpleDateFormat() : new SimpleDateFormat(pattern));
		if (timeZone != null)
		{
			formatter.setTimeZone(timeZone);
		}
		return (formatter);
	}
	
	public static String getMonthName(int month)
	{
		switch(month)
		{
			case 1:
				return "Jan";
			case 2:
				return "Feb";
			case 3:
				return "Mar";
			case 4:
				return "Apr";
			case 5:
				return "May";
			case 6:
				return "Jun";
			case 7:
				return "Jul";
			case 8:
				return "Aug";
			case 9:
				return "Sep";
			case 10:
				return "Oct";
			case 11:
				return "Nov";
			case 12:
				return "Dec";
		}
		return "";
	}

	public static int calcAgeYears(Date calcDate)  {
	    	
    	try {
        	Calendar dob = Calendar.getInstance();
        	dob.setTime(calcDate);
        	Calendar today = Calendar.getInstance();
        	int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        	if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
        	age--;
        	return age;
			
		} catch (Exception e) {
			return 0;
		}
	}
	
	
    public static String calcAge(Date calcDate)  {
    	
    	try {
        	Calendar dob = Calendar.getInstance();
        	dob.setTime(calcDate);
        	Calendar today = Calendar.getInstance();
        	int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        	if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
        	age--;
        	String returnCalc = age + " Years";
        	return returnCalc;
			
		} catch (Exception e) {
			return "Error in Age Calculation";
		}
    }
    
    public static boolean isSameDay(Calendar a, Calendar b) {
		return (a.get(Calendar.YEAR) == b.get(Calendar.YEAR) &&
				a.get(Calendar.MONTH) == b.get(Calendar.MONTH) &&
				a.get(Calendar.DAY_OF_MONTH) == b.get(Calendar.DAY_OF_MONTH));
	}
}
