package emgadm.util;

import java.util.ArrayList;
import java.util.Iterator;
import javax.naming.directory.Attribute;
import javax.naming.NamingEnumeration;

/**
 * @author A119
 *
 * will provide functions to get LDAP attributes returned in ArrayList 
 * from the LDAP service client.
 */
public class LDAPAttributeUtil {
	
	private LDAPAttributeUtil() 
	{
	}

	public static String getAttributeValue(ArrayList attributes, String attributeName)
	{
		String value = "";
		try {
			for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
				Attribute attr = (Attribute) iterator.next();
				if (attr.getID().equalsIgnoreCase(attributeName))
				{
					value = attr.get().toString();
					break;
				}
			}			
		} catch (Exception e) {
			
		}
		return value;
		
	}

	public static ArrayList getMutliValueAttributeValues(ArrayList attributes,String attributeName)
	{
		ArrayList<String> returnList = new ArrayList();
		try {
			for (Iterator iterator = attributes.iterator(); iterator.hasNext();) {
				Attribute attr = (Attribute) iterator.next();
				if (!attr.getID().equalsIgnoreCase(attributeName))
				{
					NamingEnumeration ne = attr.getAll();
					while (ne.hasMoreElements()) {
						String value = ne.nextElement().toString();
						returnList.add(value);
					}
					break;
				}
			}			
		} catch (Exception e) {
			
		}
		return returnList;
	}
}
