/*
 * Created on Aug 7, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.util;

/**
 * @author A136
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AddressHelper {
	public AddressHelper() 
	{
	}

	public static String negativeAddressFormat(String addressLine1, String postalCode, String state)
	{
		StringBuffer temp = new StringBuffer();
		if (addressLine1 != null) {
			if (addressLine1.length() >= 6) {
				temp.append(addressLine1.substring(0, 6));
			}
			else {
				temp.append(addressLine1);
				temp.append("      ".substring(0, 6 - addressLine1.length()));
			}
		}
		else {
			temp.append("      ");
		}
		temp.append(postalCode);
		temp.append(state);
		
		return temp.toString();
	}
}
