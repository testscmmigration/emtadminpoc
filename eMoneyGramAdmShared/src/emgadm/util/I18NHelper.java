/*
 * Created on Mar 8, 2004
 *
 */
package emgadm.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author A131
 *
 */
public final class I18NHelper
{
	public static final String DEFAULT_MSG_RESOURCES = "expresspayment.resources.ApplicationResources"; //$NON-NLS-1$

	private I18NHelper()
	{}

	public static String getFormattedMessage(String messageKey)
	{
		return getFormattedMessage(messageKey, (Locale)null);
	}

	public static String getFormattedMessage(String messageKey, Locale locale)
	{
		if (locale == null)
		{
			locale = Locale.getDefault();
		}
		ResourceBundle msgBundle = ResourceBundle.getBundle(DEFAULT_MSG_RESOURCES, locale);
		return(msgBundle.getString(messageKey));
	}

	public static String getFormattedMessage(String messageKey, Object[] params)
	{
		return(getFormattedMessage(messageKey, params, (Locale)null));
	}

	public static String getFormattedMessage(String messageKey, Object[] params, Locale locale)
	{
		if (locale == null)
		{
			locale = Locale.getDefault();
		}
		ResourceBundle msgBundle = ResourceBundle.getBundle(DEFAULT_MSG_RESOURCES, locale);
		String msgTemplate = msgBundle.getString(messageKey);
		MessageFormat formatter = new MessageFormat(msgTemplate);
		formatter.setLocale(locale);
		return(formatter.format(params));
	}
}
