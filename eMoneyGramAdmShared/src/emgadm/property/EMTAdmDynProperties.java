package emgadm.property;

import javax.servlet.http.HttpServletRequest;

import emgshared.property.EMTSharedDynProperties;

public class EMTAdmDynProperties
{

    
    /**
     * Get Dynamic Properties Object - from session.
     * @param request
     * @return 
     * @deprecated as of 5.4.2 ( 9/27/05 ) the properties aren't cached;
     * this means.... don't change em! This method is deprecated and was left
     * just to provide the support for session cached props; please
     * use no args version instead.
     */
    public static EMTSharedDynProperties getDPO(HttpServletRequest request) {
        return new EMTSharedDynProperties();
    }

    
    /**
     * Get Dynamic Properties Object - from session.
     * @return dynamic properties object
     */
    public static EMTSharedDynProperties getDPO() {
        return new EMTSharedDynProperties();
    }
       
    
    
}
