package emgadm.property;

import emgshared.property.PropertyLoader;

public class EMTAdmContainerProperties
{
	private static boolean secureProtocolRequired = false;
	private static String securePort = null;
	private static String ldapServer = null;
	private static String ldapPort = null;
	private static String ldapMainUserTreeContext = null;
	private static String ldapUserContext = null;
	private static String ldapUserId = null;
	private static String ldapPassword = null;
	private static String ldapApplicationContext = null;
	private static String ldapInternalRoleApplicationContext = null;
	private static String privateKeyFile = null;
	private static String heartBeatPassword = null;
	private static String heartBeatPropPassword = null;

    private static int AC4APIVersion = 1;
    private static int AC4ClientVersion = 1;
    private static String AC4Address = null;
    private static String AC4NamespaceURI = null;
    private static String AC4LocalPart = null;
    private static String AC4WSDDServiceName = null;
    private static String DVSUrl = null;
    private static String DVSTimeout = null;
    private static String bankPaymentServiceUrl = null;
    private static String bankPaymentServiceTimeout = null;
    private static String MCCPartnerIdOverride = null;
    private static String loopbackServer = null;
    private static String loopbackHost = null;
	private static String creditCardPaymentServiceUrl;
    private static String creditCardPaymentServiceTimeout = null;
    private static boolean useGCForProcessing;
    private static String consumerServiceUrl = null;
    private static boolean useReceiptQueue;
    private static boolean performIPLookUpFlag;
    private static int ipLookUpTimeOutOnScoring;
    private static long ipLookUpTimeOutOnLookUpIPAddress;
    private static String ipLookUpServiceURL;
    private static String ipLookUpCustomerID;
    private static boolean startBackgroundTimer;
    private static boolean startApproveBankBackgroundTimer;


    private static String billerReviewList;
    private static int nbrBackgroundTimers;
    private static int nbrProcessApprovedBackgroundTimers;
 
 	private EMTAdmContainerProperties() {}
	 
    public static void initialize(PropertyLoader pv )
	{
        secureProtocolRequired = pv.getBoolean("SecureProtocolRequired");
        securePort = pv.getString("SecurePort");
        ldapServer = pv.getString("LDAPServer");
		ldapPort =	pv.getString("LDAPPort");
		ldapMainUserTreeContext = pv.getString("LDAPMainUserTreeContext");
		ldapUserContext = pv.getString("LDAPUserContext");
		ldapUserId = pv.getString("LDAPUserID");
		ldapPassword =	pv.getString("LDAPPwd");
		ldapApplicationContext = pv.getString("LDAPApplicationContext");
		ldapInternalRoleApplicationContext = pv.getString("LDAPInternalRoleApplicationContext");
		privateKeyFile = pv.getString("emgadmPrivateKeyFile");
		heartBeatPassword = pv.getString("HeartBeatPwd");
		heartBeatPropPassword = pv.getString("HeartBeatPropPwd");
        AC4APIVersion = pv.getInteger("AC4APIVersion");
        AC4ClientVersion = pv.getInteger("AC4ClientVersion");
        AC4Address = pv.getString("AC4Address");
        AC4NamespaceURI = pv.getString("AC4NamespaceURI");
        AC4LocalPart = pv.getString("AC4LocalPart");
        AC4WSDDServiceName = pv.getString("AC4WSDDServiceName");
		loopbackHost = pv.getString("LoopbackHost");
        loopbackServer = pv.getString("LoopbackServer");
        DVSUrl = pv.getString("DVS_URL");
        DVSTimeout = pv.getString("DVS_TIMEOUT");
        MCCPartnerIdOverride = pv.getString("mcc_partner_id_override");
        bankPaymentServiceUrl = pv.getString("BANKPAYMENT_SERVICE_URL");
        bankPaymentServiceTimeout = pv.getString("BANKPAYMENT_SERVICE_TIMEOUT");
        creditCardPaymentServiceUrl = pv.getString("CREDITCARDPAYMENT_SERVICE_URL");
        creditCardPaymentServiceTimeout = pv.getString("CREDITCARDPAYMENT_SERVICE_TIMEOUT");
        useGCForProcessing = "true".equalsIgnoreCase(pv.getString("USE_GC_For_Processing"));
        consumerServiceUrl = pv.getString("CONSUMER_SERVICE_URL");
        useReceiptQueue = "true".equalsIgnoreCase(pv.getString("USE_RECEIPT_QUEUE"));
		performIPLookUpFlag = "true".equalsIgnoreCase(pv
				.getString("PERFORM_IP_LOOKUP_FLAG"));
		ipLookUpTimeOutOnScoring = pv.getInteger("IP_LOOKUP_TIMEOUT_ON_SCORING");
		ipLookUpTimeOutOnLookUpIPAddress = pv.getInteger("IP_LOOKUP_TIMEOUT_ON_LookupIPAddress");
		ipLookUpServiceURL = pv.getString("IP_LOOKUP_SERVICE_URL");
		ipLookUpCustomerID = pv.getString("IP_LOOKUP_CUSTOMER_ID");
		billerReviewList = pv.getString("BILLER_REVIEW_LIST");
		startBackgroundTimer = "true".equalsIgnoreCase(pv
				.getString("startBackgroundTimer"));
		startApproveBankBackgroundTimer = "true".equalsIgnoreCase(pv
				.getString("startApproveBankBackgroundTimer"));
		nbrBackgroundTimers =pv.getInteger("nbrBackgroundTimers");
		nbrProcessApprovedBackgroundTimers = pv.getInteger("nbrProcessApprovedBackgroundTimers");
		
    }

	/*********************************************************************/
	/*    Security                                                       */
	/*********************************************************************/

	public static boolean isSecureProtocolRequired()
	{
		return secureProtocolRequired;
	}

	public static String getSecurePort()
	{
		return securePort;
	}

	/*********************************************************************/
	/*    LDAP Server                                                    */
	/*********************************************************************/

	public static String getDVSUrl()
	{
		return DVSUrl;
	}

	public static String getDVSTimeout()
	{
		return DVSTimeout;
	}

	public static String getLDAP_SERVER()
	{
		return ldapServer;
	}

	public static String getLDAP_PORT()
	{
		return ldapPort;
	}

	public static String getLDAP_MAIN_USER_TREE_CONTEXT()
	{
		return ldapMainUserTreeContext;
	}

	public static String getLDAP_USER_CONTEXT()
	{
		return ldapUserContext;
	}

	public static String getLDAP_USER_ID()
	{
		return ldapUserId;
	}

	public static String getLDAP_PASSWORD()
	{
		return ldapPassword;
	}

	public static String getLDAP_APPLICATION_CONTEXT()
	{
		return ldapApplicationContext;
	}

	public static String getLDAP_INTERNAL_ROLE_APPLICATION_CONTEXT()
	{
		return ldapInternalRoleApplicationContext;
	}

	/*********************************************************************/
	/*    Cryptography                                                   */
	/*********************************************************************/

	public static String getPrivateKeyFile()
	{
		return privateKeyFile;
	}

	/*********************************************************************/
	/*    Mail                                                           */
	/*********************************************************************/

	public static String getHeartBeatPassword()
	{
		return heartBeatPassword;
	}


	public static String getHeartBeatPropPassword()
	{
		return heartBeatPropPassword;
	}

	/*********************************************************************/
	/*    Agent Connect                                                  */
	/*********************************************************************/

	public static int getAC4APIVersion()
	{
		return AC4APIVersion;
	}

	public static int getAC4ClientVersion()
	{
		return AC4ClientVersion;
	}

	public static String getAC4Address()
	{
		return AC4Address;
	}

	public static String getAC4NamespaceURI()
	{
		return AC4NamespaceURI;
	}

	public static String getAC4LocalPart()
	{
		return AC4LocalPart;
	}

	public static String getAC4WSDDServiceName()
	{
		return AC4WSDDServiceName;
	}

	public static String getMCCPartnerIdOverride() {
		return MCCPartnerIdOverride;
	}

	public static String getBankPaymentServiceUrl() {
		return bankPaymentServiceUrl;
	}

	public static String getBankPaymentServiceTimeout() {
		return bankPaymentServiceTimeout;
	}

    /**
     * The Server Network Name for the background processing loopback http call.
     * Should be set to your Tomcat catalina connector ( localhost:8080 ) for dev workstations,
     * and to the Apache virtual server name ( usually the IP ) for Dev,Q/A, Prod.
     * The port can be appended if neccesary, but the Apache servers currently
     * are configured to pass port 80 through. The emg security filter does the
     * ssl ( port 443 ) redirect, not Apache.
     * used for the background processing loopback http request.
     * ( see emgadm.background.BackgroundProcessMaster.java )
     * @return
     */
    public static String getLoopbackServer()
    {
        return loopbackServer;
}
     /**
     * The Virtual Server Name for the background processing loopback http call.
     * For workstations ( which don't use virtual servers ) set to "NULL", which
     * will prevent the Host header from being modified. For Dev,Q/A and Prod should
     * be whatever virtual server name is assigned ( like emgadm.moneygram.com )
     * used for the background processing loopback http request.
     * ( see emgadm.background.BackgroundProcessMaster.java )
     * @param loopbackServer
     */

    public static String getLoopbackHost()
    {
        return loopbackHost;
    }

	public static String getCreditCardPaymentServiceUrl() {
		return creditCardPaymentServiceUrl;
	}

	public static String getCreditCardPaymentServiceTimeout() {
		return creditCardPaymentServiceTimeout;
	}

	public static boolean getUseGCForProcessing() {
		return useGCForProcessing;
	}

	public static String getConsumerServiceUrl()
	{
		return consumerServiceUrl;
	}

	public static boolean isUseReceiptQueue() {
		return useReceiptQueue;
	}

	public static void setUseReceiptQueue(boolean useReceiptQueue) {
		EMTAdmContainerProperties.useReceiptQueue = useReceiptQueue;
	}

	public static boolean isPerformIPLookUpFlag() {
		return performIPLookUpFlag;
	}

	public static void setPerformIPLookUpFlag(boolean performIPLookUpFlag) {
		EMTAdmContainerProperties.performIPLookUpFlag = performIPLookUpFlag;
	}

	public static int getIpLookUpTimeOutOnScoring() {
		return ipLookUpTimeOutOnScoring;
	}

	public static void setIpLookUpTimeOutOnScoring(int ipLookUpTimeOutOnScoring) {
		EMTAdmContainerProperties.ipLookUpTimeOutOnScoring = ipLookUpTimeOutOnScoring;
	}

	public static long getIpLookUpTimeOutOnLookUpIPAddress() {
		return ipLookUpTimeOutOnLookUpIPAddress;
	}

	public static void setIpLookUpTimeOutOnLookUpIPAddress(
			long ipLookUpTimeOutOnLookUpIPAddress) {
		EMTAdmContainerProperties.ipLookUpTimeOutOnLookUpIPAddress = ipLookUpTimeOutOnLookUpIPAddress;
	}

	public static String getIpLookUpServiceURL() {
		return ipLookUpServiceURL;
	}

	public static void setIpLookUpServiceURL(String ipLookUpServiceURL) {
		EMTAdmContainerProperties.ipLookUpServiceURL = ipLookUpServiceURL;
	}

	/**
	 * @return the billerReviewList
	 */
	public static String getBillerReviewList() {
		return billerReviewList;
	}

	/**
	 * @param billerReviewList the billerReviewList to set
	 */
	public static void setBillerReviewList(String billerReviewList) {
		EMTAdmContainerProperties.billerReviewList = billerReviewList;
	}
	
	public static String getIpLookUpCustomerID() {
		return ipLookUpCustomerID;
	}

	public static void setIpLookUpCustomerID(String ipLookUpCustomerID) {
		EMTAdmContainerProperties.ipLookUpCustomerID = ipLookUpCustomerID;
	}

	public static boolean isStartBackgroundTimer() {
		return startBackgroundTimer;
	}

	public static void setStartBackgroundTimer(boolean startBackgroundTimer) {
		EMTAdmContainerProperties.startBackgroundTimer = startBackgroundTimer;
	}

	public static boolean isStartApproveBankBackgroundTimer() {
		return startApproveBankBackgroundTimer;
	}

	public static void setStartApproveBankBackgroundTimer(
			boolean startApproveBankBackgroundTimer) {
		EMTAdmContainerProperties.startApproveBankBackgroundTimer = startApproveBankBackgroundTimer;
	}


	 public static int getNbrBackgroundTimers() {
		 return nbrBackgroundTimers;
	 }

	 public static int getNbrProcessApprovedBackgroundTimers(){
          return nbrProcessApprovedBackgroundTimers;
	 }
}