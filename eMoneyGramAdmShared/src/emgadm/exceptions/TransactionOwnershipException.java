/*
 * Created on Feb 11, 2005
 *
 */
package emgadm.exceptions;

import emgshared.exceptions.EMGBaseException;

/**
 * @author A131
 *
 */
public class TransactionOwnershipException extends EMGBaseException
{
	private final String currentOwner;
	public TransactionOwnershipException()
	{
		super();
		this.currentOwner = null;
	}

	public TransactionOwnershipException(String message)
	{
		super(message);
		this.currentOwner = null;
	}

	public TransactionOwnershipException(String message, String currentOwner)
	{
		super(message);
		this.currentOwner = currentOwner;
	}

	public TransactionOwnershipException(Throwable arg0)
	{
		super(arg0);
		this.currentOwner = null;
	}

	/**
	 * @return
	 * 
	 * Created on Apr 14, 2005
	 */
	public String getCurrentOwner()
	{
		return currentOwner;
	}
}
