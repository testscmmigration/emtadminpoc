package emgadm.exceptions;

/**
 * @author David Hadley
 *
 */
public class InactiveUserException extends MGIBaseException
{
	/**
	 * Constructor
	 */
	public InactiveUserException()
	{
		super();
	}

	/**
	 * Constructor
	 * @param arg0
	 */
	public InactiveUserException(String arg0)
	{
		super(arg0);
	}

}
