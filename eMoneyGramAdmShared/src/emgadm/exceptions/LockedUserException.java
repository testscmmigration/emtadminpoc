package emgadm.exceptions;

/**
 * @author David Hadley
 *
 */
public class LockedUserException extends MGIBaseException
{
	/**
	 * Constructor
	 */
	public LockedUserException()
	{
		super();
	}

	/**
	 * Constructor
	 * @param arg0
	 */
	public LockedUserException(String arg0)
	{
		super(arg0);
	}

}
