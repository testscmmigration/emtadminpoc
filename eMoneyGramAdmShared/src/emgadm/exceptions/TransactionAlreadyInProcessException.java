/*
 * Created on Feb 11, 2005
 *
 */
package emgadm.exceptions;

import emgshared.exceptions.EMGBaseException;

/**
 * @author A131
 *
 */
public class TransactionAlreadyInProcessException
		extends EMGBaseException {
	private static final long serialVersionUID = 2091679735370270576L;
	private final String currentOwner;

	public TransactionAlreadyInProcessException() {
		super();
		this.currentOwner = null;
	}

	public TransactionAlreadyInProcessException(String message) {
		super(message);
		this.currentOwner = null;
	}

	public TransactionAlreadyInProcessException(String message, String currentOwner) {
		super(message);
		this.currentOwner = currentOwner;
	}

	public TransactionAlreadyInProcessException(String message, String currentOwner, Throwable cause) {
		super(message, cause);
		this.currentOwner = currentOwner;
	}

	public TransactionAlreadyInProcessException(Throwable arg0) {
		super(arg0);
		this.currentOwner = null;
	}

	/**
	 * @return
	 * 
	 * Created on Apr 14, 2005
	 */
	public String getCurrentOwner() {
		return currentOwner;
	}
}
