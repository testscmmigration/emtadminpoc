/*
 * Created on May 25, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IPHistoryDetail {
	private String ipAddress;
	private String webServerName;
	private String loginDate;
	private String result;
	private String partnerSiteId;
	private boolean tainted;


	/**
	 * @return
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param string
	 */
	public void setIpAddress(String string) {
		ipAddress = string;
	}

	/**
	 * @return
	 */
	public String getWebServerName() {
		return webServerName;
	}

	/**
	 * @param string
	 */
	public void setWebServerName(String string) {
		webServerName = string;
	}

	/**
	 * @return
	 */
	public String getLoginDate() {
		return loginDate;
	}

	/**
	 * @param string
	 */
	public void setLoginDate(String string) {
		loginDate = string;
	}

	/**
	 * @return
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @param string
	 */
	public void setResult(String string) {
		result = string;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}
	
	public boolean isTainted() {
		return tainted;
	}

	public void setTainted(String tainted) {
		this.tainted = false;
		if (tainted.equals("Y")){
			this.tainted = true;
		}
		
	}
}
