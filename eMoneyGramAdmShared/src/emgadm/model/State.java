package emgadm.model;

import java.io.Serializable;

/**
 * @author David Hadley
 */
public class State implements Serializable, Comparable
{
	Country _country;
	String _name;
	String _abbreviatedName;

	/**
	 * Returns the abbreviatedName.
	 * @return String
	 */
	public String getAbbreviatedName()
	{
		return _abbreviatedName;
	}

	/**
	 * Returns the country.
	 * @return Country
	 */
	public Country getCountry()
	{
		return _country;
	}

	/**
	 * Returns the name.
	 * @return String
	 */
	public String getName()
	{
		return _name;
	}

	/**
	 * Sets the abbreviatedName.
	 * @param abbreviatedName The abbreviatedName to set
	 */
	public void setAbbreviatedName(String abbreviatedName)
	{
		_abbreviatedName = abbreviatedName;
	}

	/**
	 * Sets the country.
	 * @param country The country to set
	 */
	public void setCountry(Country country)
	{
		_country = country;
	}

	/**
	 * Sets the name.
	 * @param name The name to set
	 */
	public void setName(String name)
	{
		_name = name;
	}

	public boolean equals(Object object)
	{
		State state = (State) object;
		return getAbbreviatedName().equals(state.getAbbreviatedName());
	}

	public int compareTo(Object object)
	{
		State state = (State) object;
		return getName().compareTo(state.getName());
	}
}
