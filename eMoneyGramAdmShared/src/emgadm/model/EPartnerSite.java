/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.util.TimeZone;

/**
 * @author A119
 *  Holds info relating to MGO partner sites
 */
public enum EPartnerSite {
	
	USA("1", "MGO", "USA", "US", "MGO US", "USD", "CST"), 
	GBR("2", "MGOUK", "GBR", "UK", "MGO UK", "GBP", "GMT"), 
	WAP("3", "WAP", "WAP", "WAP", "WAP", "USD", "CST"), 
	GER("4", "MGODE", "DEU", "DEU", "MGO DE", "EURO", "Europe/Berlin");
	
	private PartnerSite partnetSide = new PartnerSite();
	
	private EPartnerSite(String partnerSiteId, String partnerSiteNarrDesc, String country, String countryAbbr,  String partnerSiteDesc, String partnerSiteCurrency, String timeZone) {
		setPartnerSiteId(partnerSiteId);
		setPartnerSiteNarrDesc(partnerSiteNarrDesc);
		setCountry(country);
		setCountryAbbr(countryAbbr);
		setPartnerSiteDesc(partnerSiteDesc);
		setPartnerSiteCurrency(partnerSiteCurrency);
		setTimeZone(TimeZone.getTimeZone(timeZone));
	}

	public static EPartnerSite getPartnerByCountry(String country) {
		for (EPartnerSite partner : EPartnerSite.values()) {
			if (partner.getCountry().equals(country)) {
				return partner;
			}
		}
		return USA;
	}
	
	public static EPartnerSite getPartnerByPartnerSiteId(String partnerSiteId) {
		for (EPartnerSite partner : EPartnerSite.values()) {
			if (partner.getPartnerSiteNarrDesc().equals(partnerSiteId)) {
				return partner;
			}
		}
		return USA;
	}
	
	
	public String getPartnerSiteCurrency() {
		return partnetSide.getPartnerSiteCurrency();
	}
	public void setPartnerSiteCurrency(String partnerSiteCurrency) {
		partnetSide.setPartnerSiteCurrency(partnerSiteCurrency);
	}
	public String getPartnetSideIdCode() {
		return partnetSide.getPartnetSideIdCode();
	}
	public void setPartnetSideIdCode(String partnetSideIdCode) {
		partnetSide.setPartnetSideIdCode(partnetSideIdCode);
	}
	public String getPartnerSiteId() {
		return partnetSide.getPartnerSiteId();
	}
	public void setPartnerSiteId(String partnerSiteId) {
		partnetSide.setPartnerSiteId(partnerSiteId);
	}
	public String getPartnerSiteDesc() {
		return partnetSide.getPartnerSiteDesc();
	}
	public void setPartnerSiteDesc(String partnerSiteDesc) {
		partnetSide.setPartnerSiteDesc(partnerSiteDesc);
	}
	public String getPartnerSiteNarrDesc() {
		return partnetSide.getPartnerSiteNarrDesc();
	}
	public void setPartnerSiteNarrDesc(String partnerSiteNarrDesc) {
		partnetSide.setPartnerSiteNarrDesc(partnerSiteNarrDesc);
	}
	public String getPartnerSiteDipslayOrder() {
		return partnetSide.getPartnerSiteDipslayOrder();
	}
	public void setPartnerSiteDipslayOrder(String partnerSiteDipslayOrder) {
		partnetSide.setPartnerSiteDipslayOrder(partnerSiteDipslayOrder);
	}
	public void setCountry(String country) {
		partnetSide.setCountry(country);
	}
	public String getCountry() {
		return partnetSide.getCountry();
	}
	public void setCountryAbbr(String countryAbbr) {
		partnetSide.setCountryAbbr(countryAbbr);
	}
	public String getCountryAbbr() {
		return partnetSide.getCountryAbbr();
	}
	public void setTimeZone(TimeZone timeZone) {
		partnetSide.setTimeZone(timeZone);
	}
	public TimeZone getTimeZone() {
		return partnetSide.getTimeZone();
	}

}
