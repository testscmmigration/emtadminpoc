/*
 * Created on Mar 21, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

public class TranActionView implements Comparable
{

	private static final DateFormatter dateFormatter =
		new DateFormatter("dd/MMM/yyyy hh:mm:ss a", true);
	private static final NumberFormat decimalFormatter_US =
		new DecimalFormat("\u00A4#,##0.00");
	private static final NumberFormat decimalFormatter_DE =
		new DecimalFormat("\u20Ac,##0.00");
		//new DecimalFormat("&#128,##0.00");
		

	private int tranActionId;
	private String tranId;
	private String tranReasCode;
	private String tranReasDesc;
	private String tranActnDate;
	private String tranCreateUserid;
	private String tranOldStatCode;
	private String tranOldSubStatCode;
	private String tranStatCode;
	private String tranSubStatCode;
	private String tranConfId;
	private String tranPostAmt;
	private String tranDrGlAcctId;
	private String tranCrGlAcctId;
	private String tranDrGlAcctDesc;
	private String tranCrGlAcctDesc;
	private String subReasonCode;
	private String tranReasTypeCode;
	private String tranSubReasDesc;
	private String partnerSiteId;

	
    /**
     * @return Returns the tranReasTypeCode.
     */
    public String getTranReasTypeCode() {
        return tranReasTypeCode;
    }
    /**
     * @param tranReasTypeCode The tranReasTypeCode to set.
     */
    public void setTranReasTypeCode(String trtc) {
        this.tranReasTypeCode = trtc;
    }
    /**
     * @return Returns the tranSubReasDesc.
     */
    public String getTranSubReasDesc() {
        return tranSubReasDesc;
    }
    /**
     * @param tranSubReasDesc The tranSubReasDesc to set.
     */
    public void setTranSubReasDesc(String tsrd) {
        this.tranSubReasDesc = tsrd;
    }
	public int getTranActionId()
	{
		return tranActionId;
	}

	public void setTranActionId(int i)
	{
		tranActionId = i;
	}

	public String getTranId()
	{
		return tranId;
	}

	public void setTranId(int i)
	{
		tranId = i + "";
	}

	public String getTranReasCode()
	{
		return tranReasCode == null ? "" : tranReasCode;
	}

	public void setTranReasCode(String string)
	{
		tranReasCode = string;
	}

	public String getTranReasDesc()
	{
		return tranReasDesc == null ? "" : tranReasDesc;
	}

	public void setTranReasDesc(String string)
	{
		tranReasDesc = string;
	}

	public String getTranCreateUserid()
	{
		return tranCreateUserid == null ? "" : tranCreateUserid;
	}

	public void setTranCreateUserid(String string)
	{
		tranCreateUserid = string;
	}

	public String getTranActnDate()
	{
		return tranActnDate;
	}

	public void setTranActnDate(Date d)
	{
		if (d != null)
		{
			tranActnDate = dateFormatter.format(d);
		} else
		{
			tranActnDate = "";
		}
	}

	public String getTranOldStatCode()
	{
		return tranOldStatCode == null ? "" : tranOldStatCode;
	}

	public void setTranOldStatCode(String string)
	{
		tranOldStatCode = string;
	}

	public String getTranOldSubStatCode()
	{
		return tranOldSubStatCode == null ? "" : tranOldSubStatCode;
	}

	public void setTranOldSubStatCode(String string)
	{
		tranOldSubStatCode = string;
	}

	public String getTranStatCode()
	{
		return tranStatCode == null ? "" : tranStatCode;
	}

	public void setTranStatCode(String string)
	{
		tranStatCode = string;
	}

	public String getTranSubStatCode()
	{
		return tranSubStatCode == null ? "" : tranSubStatCode;
	}

	public void setTranSubStatCode(String string)
	{
		tranSubStatCode = string;
	}

	public String getTranConfId()
	{
		return tranConfId == null ? "" : tranConfId;
	}

	public void setTranConfId(String string)
	{
		tranConfId = string;
	}

	public String getTranPostAmt()
	{
		return tranPostAmt;
	}

	public void setTranPostAmt(BigDecimal bd)
	{
		if (bd != null)
		{
			if (getPartnerSiteId() != null
					&& getPartnerSiteId().equalsIgnoreCase("MGODE")) {
				tranPostAmt = decimalFormatter_DE.format(bd.doubleValue());
			} else {
			tranPostAmt = decimalFormatter_US.format(bd.doubleValue());
			}
		} else
		{
			tranPostAmt = "";
		}
	}

	public String getTranDrGlAcctId()
	{
		return tranDrGlAcctId;
	}

	public void setTranDrGlAcctId(int i)
	{
		tranDrGlAcctId = i + "";
	}

	public String getTranCrGlAcctId()
	{
		return tranCrGlAcctId;
	}

	public void setTranCrGlAcctId(int i)
	{
		tranCrGlAcctId = i + "";
	}

	public String getTranDrGlAcctDesc()
	{
		return tranDrGlAcctDesc == null ? "" : tranDrGlAcctDesc;
	}

	public void setTranDrGlAcctDesc(String string)
	{
		tranDrGlAcctDesc = string;
	}

	public String getTranCrGlAcctDesc()
	{
		return tranCrGlAcctDesc == null ? "" : tranCrGlAcctDesc;
	}

	public void setTranCrGlAcctDesc(String string)
	{
		tranCrGlAcctDesc = string;
	}

	public int compareTo(Object o)
	{
		TranActionView tav = (TranActionView) o;
		if (this.tranActnDate.equals(tav.tranActnDate))
		{
			return this.tranActionId - tav.tranActionId;
		}

		int rc = 0;
		try
		{
			rc =
				dateFormatter.parse(this.tranActnDate).after(
					dateFormatter.parse(tav.tranActnDate))
					? 1
					: -1;
		} catch (ParseDateException e)
		{
			//  do nothing, just keep rolling
			e.printStackTrace();
		}

		return rc;
	}
    /**
     * @return Returns the subReasonCode.
     */
    public String getSubReasonCode() {
        return subReasonCode == null ? "" : subReasonCode;
    }

	/**
	 * @param subReasonCode
	 *            The subReasonCode to set.
	 */
	public void setSubReasonCode(String src) {
		this.subReasonCode = src;
	}

	public String getPartnerSiteId() {
		return partnerSiteId;
	}

	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}

}
