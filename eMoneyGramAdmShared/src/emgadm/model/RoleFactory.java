package emgadm.model;

import java.util.Collection;

/**
 * @author A132
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class RoleFactory {

	public static Role createRole(
			String id,
			String name,
			String description,
			Collection<ApplicationCommand> commands) {
		Role role = new RoleImpl();
		role.setId(id);
		role.setName(name);
		role.setDescription(description);
		//		roleImpl.setApplicationName(appName);
		role.setApplicationCommands(commands);
		//		roleImpl.setAccess(access);

		return role;
	}
}
