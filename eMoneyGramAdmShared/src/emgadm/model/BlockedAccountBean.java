package emgadm.model;

import java.util.List;

public class BlockedAccountBean {

	private String maskedText;
	private boolean block;
	private String encryptedText;
	private String clearText;
	private String aba;
	private List blockedAcctIds;

	/**
	 * @return the blockedAcctIds
	 */
	public List getBlockedAcctIds() {
		return blockedAcctIds;
	}

	/**
	 * @param blockedAcctIds the blockedAcctIds to set
	 */
	public void setBlockedAcctIds(List blockedAcctIds) {
		this.blockedAcctIds = blockedAcctIds;
	}

	
	private String blockedCCId;



	public String getBlockedCCId() {
		return blockedCCId;
	}

	public void setBlockedCCId(String blockedCCId) {
		this.blockedCCId = blockedCCId;
	}

	public String getMaskedText() {
		return maskedText;
	}

	public void setMaskedText(String maskedText) {
		this.maskedText = maskedText;
	}

	public boolean isBlock() {
		return block;
	}

	public void setBlock(boolean block) {
		this.block = block;
	}

	public String getEncryptedText() {
		return encryptedText;
	}

	public void setEncryptedText(String encryptedText) {
		this.encryptedText = encryptedText;
	}

	public String getClearText() {
		return clearText;
	}

	public void setClearText(String clearText) {
		this.clearText = clearText;
	}

	public String getAba() {
		return aba;
	}

	public void setAba(String aba) {
		this.aba = aba;
	}

}
