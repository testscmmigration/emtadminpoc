package emgadm.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import emgadm.view.ConsumerBankAccountView;
import emgadm.view.ConsumerCreditCardAccountView;

public abstract class ConsumerProfileDashboard
{
	private NumberFormat nf;
	private Date firstLoginDate = null;
	private Date lastLoginDate = null;

	private String parentSiteId;

	private float recentAmt = 0F;
	private List recentTranList = new ArrayList();
	private List receiverList = new ArrayList();

	private List totalTranList = new ArrayList();
	private List denyTranList = new ArrayList();
	private List sendTranList = new ArrayList();
	private List cancelTranList = new ArrayList();
	private List fundedTranList = new ArrayList();
	private float tranAvgAmt = 0F;

	protected ConsumerProfileDashboard(String numberFormat) {
		nf = new DecimalFormat(numberFormat);
	}

	private ConsumerBankAccountView[] bankAccounts;
	private ConsumerCreditCardAccountView[] creditCardAccounts;
	private ConsumerCreditCardAccountView[] debitCardAccounts;
	private ConsumerCreditCardAccountView[] maestroAccounts;

	public Date getFirstLogin()
	{
		return firstLoginDate;
	}

	public void setFirstLoginDate(Date date)
	{
		firstLoginDate = date;
	}

	public Date getLastLogin()
	{
		return lastLoginDate;
	}

	public void setLastLoginDate(Date date)
	{
		lastLoginDate = date;
	}

	public String getRecentAmt()
	{
		return nf.format(recentAmt);
	}

	public void setRecentAmt(float f)
	{
		recentAmt = f;
	}

	public List getRecentTranList()
	{
		return recentTranList == null ? new ArrayList() : recentTranList;
	}

	public void setRecentTranList(List list)
	{
		recentTranList = list;
	}

	public int getRecentTranCnt()
	{
		return recentTranList.size();
	}

	public List getReceiverList()
	{
		return receiverList == null ? new ArrayList() : receiverList;
	}

	public void setReceiverList(List list)
	{
		receiverList = list;
	}

	public List getTotalTranList()
	{
		return totalTranList == null ? new ArrayList() : totalTranList;
	}

	public void setTotalTranList(List list)
	{
		totalTranList = list;
	}

	public int getTotalTranCnt()
	{
		return totalTranList.size();
	}

	public List getDenyTranList()
	{
		return denyTranList == null ? new ArrayList() : denyTranList;
	}

	public void setDenyTranList(List list)
	{
		denyTranList = list;
	}

	public int getDenyTranCnt()
	{
		return denyTranList.size();
	}

	public List getSendTranList()
	{
		return sendTranList == null ? new ArrayList() : sendTranList;
	}

	public void setSendTranList(List list)
	{
		sendTranList = list;
	}

	public int getSendTranCnt()
	{
		return sendTranList.size();
	}

	public List getCancelTranList()
	{
		return cancelTranList == null ? new ArrayList() : cancelTranList;
	}

	public void setCancelTranList(List list)
	{
		cancelTranList = list;
	}

	public int getCancelTranCnt()
	{
		return cancelTranList.size();
	}

	public List getFundedTranList()
	{
		return fundedTranList == null ? new ArrayList() : fundedTranList;
	}

	public void setFundedTranList(List list)
	{
		fundedTranList = list;
	}

	public int getFundedTranCnt()
	{
		return fundedTranList.size();
	}

	public String getTranAvgAmt()
	{
		return nf.format(tranAvgAmt);
	}

	public void setTranAvgAmt(float f)
	{
		tranAvgAmt = f;
	}

	public ConsumerBankAccountView[] getBankAccounts()
	{
		return bankAccounts;
	}

	public void setBankAccounts(ConsumerBankAccountView[] list)
	{
		bankAccounts = list;
	}

	public int getBankAccountCnt()
	{
		return bankAccounts.length;
	}

	public ConsumerCreditCardAccountView[] getCreditCardAccounts()
	{
		return creditCardAccounts;
	}

	public void setCreditCardAccounts(ConsumerCreditCardAccountView[] list)
	{
		creditCardAccounts = list;
	}

	public int getCreditCardAccountCnt()
	{
		return creditCardAccounts.length;
	}

	public ConsumerCreditCardAccountView[] getDebitCardAccounts()
	{
		return debitCardAccounts;
	}

	public void setDebitCardAccounts(ConsumerCreditCardAccountView[] views)
	{
		debitCardAccounts = views;
	}

	public int getDebitCardAccountCnt()
	{
		return debitCardAccounts.length;
	}

	public String getParentSiteId() {
		return parentSiteId;
	}

	public void setParentSiteId(String parentSiteId) {
		this.parentSiteId = parentSiteId;
	}

	public ConsumerCreditCardAccountView[] getMaestroAccounts() {
		return maestroAccounts;
	}

	public void setMaestroAccounts(ConsumerCreditCardAccountView[] maestroAccounts) {
		this.maestroAccounts = maestroAccounts;
	}
}
