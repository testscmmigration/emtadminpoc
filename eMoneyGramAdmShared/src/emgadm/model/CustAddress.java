/*
 * Created on Feb 4, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CustAddress {
	private int addrId;
	private int custId;
	private String addrLine1Text;
	private String addrLine2Text;
	private String addrLine3Text;
	private String addrCityName;
	private String addrStateName;
	private String addrPostalCode;
	private String addrCntryId;
	private String createDate;
	private String addrStatCode;
	private String addrStatDesc;
	/**
	 * @return
	 */
	public int getAddrId() {
		return addrId;
	}

	/**
	 * @param i
	 */
	public void setAddrId(int i) {
		addrId = i;
	}

	/**
	 * @return
	 */
	public int getCustId() {
		return custId;
	}

	/**
	 * @param i
	 */
	public void setCustId(int i) {
		custId = i;
	}

	/**
	 * @return
	 */
	public String getAddrLine1Text() {
		return addrLine1Text == null ? "" : addrLine1Text;
	}

	/**
	 * @param string
	 */
	public void setAddrLine1Text(String string) {
		addrLine1Text = string;
	}

	/**
	 * @return
	 */
	public String getAddrLine2Text() {
		return addrLine2Text == null ? "" : addrLine2Text;
	}

	/**
	 * @param string
	 */
	public void setAddrLine2Text(String string) {
		addrLine2Text = string;
	}

	/**
	 * @return
	 */
	public String getAddrLine3Text() {
		return addrLine3Text == null ? "" : addrLine3Text;
	}

	/**
	 * @param string
	 */
	public void setAddrLine3Text(String string) {
		addrLine3Text = string;
	}

	/**
	 * @return
	 */
	public String getAddrCityName() {
		return addrCityName == null ? "" : addrCityName;
	}

	/**
	 * @param string
	 */
	public void setAddrCityName(String string) {
		addrCityName = string;
	}

	/**
	 * @return
	 */
	public String getAddrStateName() {
		return addrStateName == null ? "" : addrStateName;
	}

	/**
	 * @param string
	 */
	public void setAddrStateName(String string) {
		addrStateName = string;
	}

	/**
	 * @return
	 */
	public String getAddrPostalCode() {
		return addrPostalCode == null ? "" : addrPostalCode;
	}

	/**
	 * @param string
	 */
	public void setAddrPostalCode(String string) {
		addrPostalCode = string;
	}

	/**
	 * @return
	 */
	public String getAddrCntryId() {
		return addrCntryId == null ? "" : addrCntryId;
	}

	/**
	 * @param string
	 */
	public void setAddrCntryId(String string) {
		addrCntryId = string;
	}

	/**
	 * @return
	 */
	public String getCreateDate() {
		return createDate == null ? "" : createDate;
	}

	/**
	 * @param string
	 */
	public void setCreateDate(String string) {
		createDate = string;
	}

	/**
	 * @return
	 */
	public String getAddrStatCode() {
		return addrStatCode == null ? "" : addrStatCode;
	}

	/**
	 * @param string
	 */
	public void setAddrStatCode(String string) {
		addrStatCode = string;
	}

	/**
	 * @return
	 */
	public String getAddrStatDesc() {
		return addrStatDesc == null ? "" : addrStatDesc;
	}

	/**
	 * @param string
	 */
	public void setAddrStatDesc(String string) {
		addrStatDesc = string;
	}

}
