/*
 * Created on Aug 11, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AchReturnStatus {
	private String achReturnStatCode;
	private String achReturnStatDesc;
	
	/**
	 * @return Returns the achReturnStatCode.
	 */
	public String getAchReturnStatCode() {
		return achReturnStatCode;
	}
	/**
	 * @param achReturnStatCode The achReturnStatCode to set.
	 */
	public void setAchReturnStatCode(String achReturnStatCode) {
		this.achReturnStatCode = achReturnStatCode;
	}
	/**
	 * @return Returns the achReturnStatDesc.
	 */
	public String getAchReturnStatDesc() {
		return achReturnStatDesc;
	}
	/**
	 * @param achReturnStatDesc The achReturnStatDesc to set.
	 */
	public void setAchReturnStatDesc(String achReturnStatDesc) {
		this.achReturnStatDesc = achReturnStatDesc;
	}
}
