package emgadm.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;

public class ACHReturnQueueView implements Comparable
{
	public static String sortBy = "achTransId";
	public static int sortSeq = 1;

	private String achTransId;
	private String achTranStatus;
	private String mgiTransId;
	private String micrTransId;
	private String achRetType;
	private String achRetReasDesc;
	private String achAmt;
	private String achIndivId;
	private String achEffDate;
	private String achUploadId;
	private String isMicroDeposit;


	public int compareTo(Object o)
	{
		ACHReturnQueueView t = (ACHReturnQueueView) o;

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achTransId"))
		{
			return (achTransId.compareToIgnoreCase(t.achTransId)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achTranStatus"))
		{
			return (achTranStatus.compareToIgnoreCase(t.achTranStatus)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("mgiTransId"))
		{
			return (mgiTransId.compareToIgnoreCase(t.mgiTransId)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("micrTransId"))
		{
			return (micrTransId.compareToIgnoreCase(t.micrTransId)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achRetType"))
		{
			return (achRetType.compareToIgnoreCase(t.achRetType)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achRetType"))
		{
			return (achRetType.compareToIgnoreCase(t.achRetType)) * sortSeq;
		}
		
		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achRetReasDesc"))
		{
			return (achRetReasDesc.compareToIgnoreCase(t.achRetReasDesc)) * sortSeq;
		}

		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achAmt"))
		{
			double n1 = 0;
			double n2 = 0;

			NumberFormat df = new DecimalFormat("#,##0.00");
			try
			{
				n1 = df.parse(achAmt).doubleValue();
				n2 = df.parse(t.achAmt).doubleValue();
			} catch (ParseException e)
			{
				e.printStackTrace();
			}

			return (n1 >= n2 ? 1 : -1) * sortSeq;
		}
		
		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achIndivId"))
		{
			return (achIndivId.compareToIgnoreCase(t.achIndivId)) * sortSeq;
		}

		DateFormatter dfmt = new DateFormatter("dd/MMM/yyyy hh:mm a", true);
		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achEffDate"))
		{
			try
			{
				return (
					dfmt.parse(achEffDate).after(
						dfmt.parse(t.achEffDate))
						? 1 : -1) * sortSeq;
			} catch (ParseDateException e)
			{
				e.printStackTrace();
			}
		}
		
		if (ACHReturnQueueView.sortBy.equalsIgnoreCase("achUploadId"))
		{
			return (achUploadId.compareToIgnoreCase(t.achUploadId)) * sortSeq;
		}

		return 0;
	}


	public String getAchAmt() {
		return achAmt;
	}


	public void setAchAmt(String achAmt) {
		this.achAmt = achAmt;
	}


	public String getAchEffDate() {
		return achEffDate;
	}


	public void setAchEffDate(String achEffDate) {
		this.achEffDate = achEffDate;
	}


	public String getAchIndivId() {
		return achIndivId;
	}


	public void setAchIndivId(String achIndivId) {
		this.achIndivId = achIndivId;
	}


	public String getAchRetReasDesc() {
		return achRetReasDesc;
	}


	public void setAchRetReasDesc(String achRetReasDesc) {
		this.achRetReasDesc = achRetReasDesc;
	}


	public String getAchRetType() {
		return achRetType;
	}


	public void setAchRetType(String achRetType) {
		this.achRetType = achRetType;
	}


	public String getAchTransId() {
		return achTransId;
	}


	public void setAchTransId(String achTransId) {
		this.achTransId = achTransId;
	}


	public String getAchTranStatus() {
		return achTranStatus;
	}


	public void setAchTranStatus(String achTranStatus) {
		this.achTranStatus = achTranStatus;
	}


	public String getAchUploadId() {
		return achUploadId;
	}


	public void setAchUploadId(String achUploadId) {
		this.achUploadId = achUploadId;
	}


	public String getMgiTransId() {
		return mgiTransId;
	}


	public void setMgiTransId(String mgiTransId) {
		this.mgiTransId = mgiTransId;
	}


	public String getMicrTransId() {
		return micrTransId;
	}


	public void setMicrTransId(String micrTransId) {
		this.micrTransId = micrTransId;
	}


	public String getIsMicroDeposit() {
		return isMicroDeposit;
	}


	public void setIsMicroDeposit(String isMicroDeposit) {
		this.isMicroDeposit = isMicroDeposit;
	}


}
