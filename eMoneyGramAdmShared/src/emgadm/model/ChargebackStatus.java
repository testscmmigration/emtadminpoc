/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackStatus {
	private String chargebackStatusCode;
	private String chargebackStatusDesc;
	/**
	 * @return Returns the chargebackStatusCode.
	 */
	public String getChargebackStatusCode() {
		return chargebackStatusCode;
	}
	/**
	 * @param chargebackStatusCode The chargebackStatusCode to set.
	 */
	public void setChargebackStatusCode(String chargebackStatusCode) {
		this.chargebackStatusCode = chargebackStatusCode;
	}
	/**
	 * @return Returns the chargebackStatusDesc.
	 */
	public String getChargebackStatusDesc() {
		return chargebackStatusDesc;
	}
	/**
	 * @param chargebackStatusDesc The chargebackStatusDesc to set.
	 */
	public void setChargebackStatusDesc(String chargebackStatusDesc) {
		this.chargebackStatusDesc = chargebackStatusDesc;
	}
}
