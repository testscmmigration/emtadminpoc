/*
 * Created on Mar 22, 2004
 *
 */
package emgadm.model;

import java.util.Calendar;

import emgshared.util.I18NHelper;

/**
 * Class representing valid business week identifiers.
 * Follows typesafe enum pattern.
 *
 */
public abstract class BusinessWeekType
{
	protected static final String MON_FRI_TAG = "Monday - Friday"; //$NON-NLS-1$
	protected static final String MON_SAT_TAG = "Monday - Saturday"; //$NON-NLS-1$
	protected static final String MON_SUN_TAG = "Monday - Sunday"; //$NON-NLS-1$

	public static final BusinessWeekType M_F = new BusinessWeekTypeMonFri();
	public static final BusinessWeekType M_SAT = new BusinessWeekTypeMonSat();
	public static final BusinessWeekType M_SUN = new BusinessWeekTypeMonSun();

	private final String tag;

	protected BusinessWeekType(String tag)
	{
		this.tag = tag;
	}

	public static final BusinessWeekType getInstance(String businessWeekTypeTag)
		throws IllegalArgumentException
	{
		BusinessWeekType type;
		if (BusinessWeekType.MON_FRI_TAG.equalsIgnoreCase(businessWeekTypeTag))
		{
			type = BusinessWeekType.M_F;
		} else if (
			BusinessWeekType.MON_SAT_TAG.equalsIgnoreCase(businessWeekTypeTag))
		{
			type = BusinessWeekType.M_SAT;
		} else if (
			BusinessWeekType.MON_SUN_TAG.equalsIgnoreCase(businessWeekTypeTag))
		{
			type = BusinessWeekType.M_SUN;
		} else
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.invalid.param.value", new Object[] { businessWeekTypeTag, "businessWeekTypeTag" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new IllegalArgumentException(errorMessage);
		}
		return (type);
	}

	public String getTag()
	{
		return this.tag;
	}

	public String toString()
	{
		return this.tag;
	}

	/**
	 * @param cal -  If null, throws NullPointerException
	 * @return
	 * @throws NullPointerException
	 */
	public abstract boolean isBusinessDay(Calendar cal);

	public boolean equals(Object obj)
	{
		boolean equality = (this == obj);
		if (equality == false)
		{
			if (obj instanceof BusinessWeekType)
			{
				BusinessWeekType other = (BusinessWeekType) obj;
				equality = this.tag.equalsIgnoreCase(other.tag);
			}

		}
		return (equality);
	}

	public int hashCode()
	{
		return this.tag.hashCode();
	}

}