package emgadm.model;

import emgshared.util.Constants;

public class ConsumerProfileDashboardFactory {

	public static ConsumerProfileDashboard create(String parentSiteId) {
		if (Constants.MGO_PARTNER_SITE_ID.equals(parentSiteId)
				|| Constants.WAP_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new USConsumerProfileDashboard();
		} else if (Constants.MGOUK_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new UKConsumerProfileDashboard();
		} else if (Constants.MGODE_PARTNER_SITE_ID.equals(parentSiteId)) {
			return new DEConsumerProfileDashboard();
		} else {
			throw new IllegalArgumentException("Unsupported program: " + parentSiteId);
		}
	}

}
