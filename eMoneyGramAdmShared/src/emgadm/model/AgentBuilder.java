/*
 * Created on Mar 11, 2004
 *
 */
package emgadm.model;

/**
 * @author A131
 *
 */
public class AgentBuilder {
	private String receiveCode;
	private String agentId;
	private String agentName;
	private String agentCity;
	private String agentState;
	private String agentParentID;
	private String status;
	private int hierarchyLevel;

	public Agent buildAgent() {
		Agent agent = new Agent(this.agentId, this.agentParentID, this.agentName,this.agentCity,
				this.receiveCode, this.hierarchyLevel,this.status,this.agentState, null);
		return (agent);
	}
	/**
	 * @return
	 */
	public String getAgentCity() {
		return agentCity;
	}

	/**
	 * @return
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * @return
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @return
	 */
	public String getAgentParentID() {
		return agentParentID;
	}

	/**
	 * @return
	 */
	public int getHierarchyLevel() {
		return hierarchyLevel;
	}

	/**
	 * @return
	 */
	public String getReceiveCode() {
		return receiveCode;
	}

	/**
	 * @param string
	 */
	public void setAgentCity(String string) {
		agentCity = string;
	}

	/**
	 * @param string
	 */
	public void setAgentId(String string) {
		agentId = string;
	}

	/**
	 * @param string
	 */
	public void setAgentName(String string) {
		agentName = string;
	}

	/**
	 * @param string
	 */
	public void setAgentParentID(String string) {
		agentParentID = string;
	}

	/**
	 * @param date
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param i
	 */
	public void setHierarchyLevel(int i) {
		hierarchyLevel = i;
	}

	/**
	 * @param string
	 */
	public void setReceiveCode(String string) {
		receiveCode = string;
	}
	/**
	 * @return
	 */
	public String getAgentState() {
		return agentState;
	}

	/**
	 * @param string
	 */
	public void setAgentState(String string) {
		agentState = string;
	}

}
