/*
 * Created on May 27, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IpConsumer {
	private String logonId;
	private String statCode;
	private String subStatCode;
	private String frstName;
	private String lastName;
	private String phoneNbr;
	private String ssnMaskNbr;
	private String brthDate;
	private String beginDate;
	private String endDate;
	private String custId;
	private String custIdLink;

	private String custBlockedCode;

	
	public void setCustBlockedCode(String custBlockedCode) {
		this.custBlockedCode = custBlockedCode;
	}

	/**
	 * @return
	 */
	public String getLogonId() {
		return logonId;
	}

	/**
	 * @param string
	 */
	public void setLogonId(String string) {
		logonId = string;
	}

	/**
	 * @return
	 */
	public String getStatCode() {
		return statCode;
	}

	/**
	 * @param string
	 */
	public void setStatCode(String string) {
		statCode = string;
	}

	/**
	 * @return
	 */
	public String getSubStatCode() {
		return subStatCode;
	}

	/**
	 * @param string
	 */
	public void setSubStatCode(String string) {
		subStatCode = string;
	}

	/**
	 * @return
	 */
	public String getFrstName() {
		return frstName;
	}

	/**
	 * @param string
	 */
	public void setFrstName(String string) {
		frstName = string;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @return
	 */
	public String getPhoneNbr() {
		return phoneNbr;
	}

	/**
	 * @param string
	 */
	public void setPhoneNbr(String string) {
		phoneNbr = string;
	}

	/**
	 * @return
	 */
	public String getSsnMaskNbr() {
		return ssnMaskNbr;
	}

	/**
	 * @param string
	 */
	public void setSsnMaskNbr(String string) {
		ssnMaskNbr = string;
	}

	/**
	 * @return
	 */
	public String getBrthDate() {
		return brthDate;
	}

	/**
	 * @param string
	 */
	public void setBrthDate(String string) {
		brthDate = string;
	}

	/**
	 * @return
	 */
	public String getBeginDate() {
		return beginDate;
	}

	/**
	 * @param string
	 */
	public void setBeginDate(String string) {
		beginDate = string;
	}

	/**
	 * @return
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param string
	 */
	public void setEndDate(String string) {
		endDate = string;
	}
	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
		this.setCustIdLink(custId);
		
	}

	public String getCustBlockedCode() {
		return custBlockedCode;
	}

	public String getCustIdLink() {
		return custIdLink;
	}

	public void setCustIdLink(String custId) {
		StringBuffer sb = new StringBuffer();
		sb.append("showCustomerProfile.do?custId=");
		sb.append(custId);
		sb.append("&basicFlag=N");
		this.custIdLink = sb.toString();		
				
	}

}
