/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackAction {
	private String chargebackActionId;
	private String tranId;
	private String chargebackStatusCode;
	private String chargebackStatusDesc;
	private String createDate;
	private String createUserId;
	
	
	/**
	 * @return Returns the chargebackActionId.
	 */
	public String getChargebackActionId() {
		return chargebackActionId;
	}
	/**
	 * @param chargebackActionId The chargebackActionId to set.
	 */
	public void setChargebackActionId(String chargebackActionId) {
		this.chargebackActionId = chargebackActionId;
	}
	/**
	 * @return Returns the chargebackStatusCode.
	 */
	public String getChargebackStatusCode() {
		return chargebackStatusCode;
	}
	/**
	 * @param chargebackStatusCode The chargebackStatusCode to set.
	 */
	public void setChargebackStatusCode(String chargebackStatusCode) {
		this.chargebackStatusCode = chargebackStatusCode;
	}
	/**
	 * @return Returns the chargebackStatusDesc.
	 */
	public String getChargebackStatusDesc() {
		return chargebackStatusDesc;
	}
	/**
	 * @param chargebackStatusDesc The chargebackStatusDesc to set.
	 */
	public void setChargebackStatusDesc(String chargebackStatusDesc) {
		this.chargebackStatusDesc = chargebackStatusDesc;
	}
	/**
	 * @return Returns the createDate.
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate The createDate to set.
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return Returns the createUserId.
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId The createUserId to set.
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return Returns the tranId.
	 */
	public String getTranId() {
		return tranId;
	}
	/**
	 * @param tranId The tranId to set.
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
}
