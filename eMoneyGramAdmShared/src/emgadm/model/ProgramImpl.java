package emgadm.model;

import java.util.HashMap;
import java.util.Map;

class ProgramImpl implements Program {
	private String id;
	private String name;
	private String description;

	private static class InstanceHolder {
		private static Map<String, Program> instances = new HashMap<String, Program>();

		public static void add(ProgramImpl program) {
			instances.put(program.id, program);
		}

		public static Program get(String roleId) {
			return instances.get(roleId);
		}
		
		
	}

	ProgramImpl(String id, String name, String description) {
		setId(id);
		setName(name);
		setDescription(description);
		
		InstanceHolder.add(this);
	}

	/**
	 * @see emgadm.model.Program#getId()
	 */
	public String getId() {
		return id;
	}

	/**
	 * @see emgadm.model.Program#setId(java.lang.String)
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @see emgadm.model.Program#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see emgadm.model.Program#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see emgadm.model.Program#getDescription()
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @see emgadm.model.Program#setDescription(java.lang.String)
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProgramImpl other = (ProgramImpl) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public static Program getInstance(String roleId) {
		return InstanceHolder.get(roleId);
	}

	public String toString() {
		return this.getName();
	}
}
