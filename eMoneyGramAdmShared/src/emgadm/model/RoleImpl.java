package emgadm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import emgadm.dataaccessors.CommandManager;
import emgadm.dataaccessors.InvalidGuidException;
import emgadm.dataaccessors.ManagerFactory;
import emgadm.dataaccessors.MenuManager;
import emgadm.dataaccessors.RoleManager;
import emgadm.dataaccessors.UserProfileManager;
import emgshared.exceptions.DataSourceException;
import emgshared.exceptions.EMGRuntimeException;

public class RoleImpl implements Role, Serializable {
	private String name;
	private String id;
	private String description;
	private String applicationName;
	private String access;
	private boolean showSendingAgentInfo;
	private Collection<ApplicationCommand> applicationCommands = new ArrayList<ApplicationCommand>();
	private List<UserProfile> users;

	private static final String HTML_LINE_BREAK = "<br />";
	private static final String ROLE_ROLENAME_SUFFIX = " Role";
	private static final String APP_EMTADMIN_ROLENAME_PREFIX = "APP - EMTAdmin - ";
	
	public RoleImpl() {
	}

	/**
	 * @return
	 */
	public Collection<ApplicationCommand> getApplicationCommands() {
		return applicationCommands;
	}

	/**
	 * @return
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return
	 */
	public String getMenu()
	{
		try
		{
			return MenuManager.getMenu(this);
		} catch (DataSourceException e)
		{
		}
		return ""; //$NON-NLS-1$
	}

	/**
	 * @return
	 */
	public boolean isShowSendingAgentInfo()
	{
		return showSendingAgentInfo;
	}

	/**
	 * @param collection
	 */
	public void setApplicationCommands(Collection<ApplicationCommand> collection) {
		applicationCommands = collection;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string)
	{
		description = string;
	}

	/**
	 * @param string
	 */
	public void setId(String string)
	{
		id = string;
	}

	/**
	 * @param b
	 */
	public void setShowSendingAgentInfo(boolean b)
	{
		showSendingAgentInfo = b;
	}

//FIXME S26-Looks like some MGO UK changes were made to allow user to have multiple roles - right now, 
//below methods give ALL access to user - putting back old methods at least for now so we restrict buttons,menus,etc
//to only what the role is set up to do.
//	public boolean hasPermission(String command) {
//		return CommandManager.getCommandNames().contains(command);
//	}
//
//	public boolean hasPermission(String command, Role roles) {
//		return CommandManager.getCommandNames().contains(command);
//	}
	public boolean hasPermission(String command)
	{
		for (Iterator iter = getApplicationCommands().iterator();
			iter.hasNext();
			)
		{
			ApplicationCommandImpl appCommand =
				(ApplicationCommandImpl) iter.next();
			if (appCommand.getId().equalsIgnoreCase(command)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasPermission(String command, Role roles)
	{
		for (Iterator iter = getApplicationCommands().iterator();
			iter.hasNext();)
			{
			ApplicationCommandImpl appCommand =
				(ApplicationCommandImpl) iter.next();
			if (appCommand.getDisplayName().equalsIgnoreCase(command)) {
				return true;
			}
		}
	return false;
	}
	
	
	public void addCommand(ApplicationCommand command) {
		applicationCommands.add(command);
	}

	/**
	 * @return
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param string
	 */
	public void setName(String string)
	{
		name = string;
	}

	public boolean contains(ApplicationCommand command) {
		return applicationCommands.contains(command);
	}

	public boolean contains(String command) {
		for (Iterator<ApplicationCommand> iter = applicationCommands.iterator(); iter.hasNext();) {
			ApplicationCommand applicationCommand = iter.next();
			if (applicationCommand.getId().equalsIgnoreCase(command)) {
				return true;
			}
		}
		return false;

	}

	public String getDisplayName()
	{
		String displayName = getDescription();
		try {
			displayName = displayName + (isInternal() ? " - (Internal)" : " - (External)"); //$NON-NLS-1$//$NON-NLS-2$
		} catch (Exception e) {
		}
		return displayName;
	}

	private boolean isInternal()
		throws DataSourceException, NamingException, InvalidGuidException {
		for (Iterator<Role> iter = RoleManager.getInternalRoles().iterator();
			iter.hasNext(); ) {
			Role role = iter.next();
			if (role.getId().equals(getId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return
	 */
	public String getApplicationName()
	{
		return applicationName;
	}

	/**
	 * @param string
	 */
	public void setApplicationName(String appName)
	{
		applicationName = appName;
	}

	/**
		 * @return
		 */
	public String getAccess()
	{
		return access;
	}

	/**
	 * @param string
	 */
	public void setAccess(String appAccess)
	{
		access = appAccess;
	}

	/**
	 * @return
	 */
	public Collection<UserProfile> getUsers() {
		return getUsers(getId());
	}

	private Collection<UserProfile> getUsers(String roleId)
	{
		if (users != null) {
			return users;
		}
		
		UserProfileManager upm = ManagerFactory.createUserManager();

		String userID = "";
		String firstName = "";
		String lastName = "";
		String webStatus = "All";
		boolean isInternal = true;

		try {
			UserQueryCriterion criteria =
				new UserQueryCriterion(
					userID,
					firstName,
					lastName,
					webStatus,
					roleId,
					isInternal);
			users = upm.findHollowUsers(criteria);
		} catch (Exception e) {
			throw new EMGRuntimeException(e);
		}
		return users;
	}

	public int compareTo(Role role) {
		return this.getName().compareToIgnoreCase(role.getName());
	}

	public List<Program> getProgramsWithPermission(String command) {
		int separatorIdx = id.indexOf(PROGRAM_ROLENAME_SEPARATOR); 
		Program programInstance = ProgramFactory.getInstance((separatorIdx == -1)
				? ProgramFactory.DEFAULT_PROGRAM_ID
				: id.substring(0, separatorIdx));
		List<Program> programs = new ArrayList<Program>(1);
		programs.add(programInstance);
		return programs;
	}

	public String getShortNamePlusProgramPrefix() {
		StringBuilder tmpStr = new StringBuilder(getName().trim());
		supressPrefix(tmpStr);
		supressSuffix(tmpStr);
		
		return tmpStr.toString();
	}

	public String getShortName() {
		StringBuilder tmpStr = new StringBuilder(getName().trim());
		supressPrefix(tmpStr);
		supressProgramPrefix(tmpStr);
		supressSuffix(tmpStr);
		
		return tmpStr.toString();
	}

	private void htmlWrapLongName(StringBuilder line) {
		int half = line.length() / 2;
		if (tryToBreak(line, half)) {
			return;
		}
		int offset = 1;
		while (offset < half) {
			if (tryToBreak(line, half + offset)) {
				break;
			}
			if (tryToBreak(line, half - offset)) {
				break;
			}
			offset++;
		}
	}

	private boolean tryToBreak(StringBuilder line, int indexToTest) {
		if (isSpaceOrDash(line.charAt(indexToTest))) {
			htmlBreakLine(line, indexToTest);
			return true;
		}
		return false;
	}

	private void htmlBreakLine(StringBuilder line, int idx) {
		if (line.charAt(idx) == '-') {
			line.insert(idx, HTML_LINE_BREAK);
		} else {
			line.replace(idx, idx + 1, HTML_LINE_BREAK);
		}
	}

	private boolean isSpaceOrDash(char ch) {
		return (ch == '-') || Character.isWhitespace(ch);
	}

	private void supressPrefix(StringBuilder tmpStr) {
		if (tmpStr.indexOf(APP_EMTADMIN_ROLENAME_PREFIX) == 0) {
			tmpStr.delete(0, APP_EMTADMIN_ROLENAME_PREFIX.length());
		}
	}

	private void supressProgramPrefix(StringBuilder tmpStr) {
		int separatorPos = tmpStr.indexOf(Role.PROGRAM_ROLENAME_SEPARATOR);
		if (separatorPos != -1) {
			tmpStr.delete(0, separatorPos + 1);
		}
	}

	private void supressSuffix(StringBuilder tmpStr) {
		int suffixPos = tmpStr.indexOf(ROLE_ROLENAME_SUFFIX);
		if (suffixPos != -1) {
			tmpStr.delete(suffixPos, tmpStr.length());
		}
	}

	public String getHTMLWrappedShortName() {
		StringBuilder tmpStr = new StringBuilder(getShortName().trim());
		htmlWrapLongName(tmpStr);
		
		return tmpStr.toString();
	}
}
