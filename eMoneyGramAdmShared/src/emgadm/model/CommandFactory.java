package emgadm.model;

import java.util.HashMap;
import java.util.Map;


public class CommandFactory {


	private static class InstanceHolder {
		private static Map<String, ApplicationCommand> instances = new HashMap<String, ApplicationCommand>();

		public static ApplicationCommand get(String emtCommand) {
			return instances.get(emtCommand);
		}

		public static void put(String id, ApplicationCommandImpl applicationCommand) {
			instances.put(id, applicationCommand);
		}
	}

	public static ApplicationCommand createCommand(
		String displayName,
		String menuGroupName,
		String id,
		String menuLevel,
		String allowInternal,
		String isAuthRequired,
		String description,
		boolean isMenuItem) {

		ApplicationCommandImpl com = new ApplicationCommandImpl();
		com.setDisplayName(displayName);
		com.setMenuGroupName(menuGroupName);
		com.setId(id);
		com.setMenuLevel(menuLevel);
		com.setAllowInternal(allowInternal);
		com.setAuthorizationRequired(isAuthRequired);
		com.setDescription(description);
		com.setMenuItem(isMenuItem);
		InstanceHolder.put(id, com);
		return com;
	}

	public static ApplicationCommand getInstance(String emtCommand) {
		return InstanceHolder.get(emtCommand);
	}

	public static ApplicationCommand createCommand(String cmdId) {
		ApplicationCommandImpl com = new ApplicationCommandImpl();
		com.setId(cmdId);
		if (getInstance(cmdId) == null) {
			InstanceHolder.put(cmdId, com);
		}
		return com;
	}
}
