package emgadm.model;

import emgshared.exceptions.ParseDateException;
import emgshared.util.DateFormatter;


/**
 * Search criteria for Chargeback Tracking
 *
 */
public class ChargebackTrackingSearchRequest {
	
	DateFormatter df1 = new DateFormatter("MM/dd/yyyy", true);
	DateFormatter df2 = new DateFormatter("dd/MMM/yyyy", true);

	private String beginDateText;
	private String endDateText;
	private String status;
	
	/**
	 * @return Returns the beginDateText.
	 */
	public String getBeginDateText() {
		return beginDateText;
	}
	/**
	 * @param beginDateText The beginDateText to set.
	 */
	public void setBeginDateText(String beginDateText) {
		try
		{
			this.beginDateText = df1.format(df2.parse(beginDateText));
		}
		catch (ParseDateException e)
		{
			this.beginDateText = beginDateText;
		}
	}
	/**
	 * @return Returns the endDateText.
	 */
	public String getEndDateText() {
		return endDateText;
	}
	/**
	 * @param endDateText The endDateText to set.
	 */
	public void setEndDateText(String endDateText) {
		try
		{
			this.endDateText = df1.format(df2.parse(endDateText));
		}
		catch (ParseDateException e)
		{
			this.endDateText = endDateText;
		}
	}
	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
