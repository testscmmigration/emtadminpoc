/*
 * Created on Mar 22, 2004
 *
 */
package emgadm.model;

import java.util.Calendar;

import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
class BusinessWeekTypeMonFri extends BusinessWeekType
{
	BusinessWeekTypeMonFri()
	{
		super(MON_FRI_TAG);
	}

	public boolean isBusinessDay(Calendar cal)
	{
		if (cal == null)
		{
			String errorMessage = I18NHelper.getFormattedMessage("exception.required.param", new Object[] { "cal" }); //$NON-NLS-1$ //$NON-NLS-2$
			throw new NullPointerException(errorMessage);
		}

		int day = cal.get(Calendar.DAY_OF_WEEK);

		return (day != Calendar.SUNDAY && day != Calendar.SATURDAY);
	}
}
