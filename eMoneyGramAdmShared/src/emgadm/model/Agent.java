package emgadm.model;

public class Agent implements java.io.Serializable
{
	public static final int MAIN_OFFICE_LEVEL = 1;
	public static final int DIVISION_LEVEL = 2;
	public static final int LOCATION_LEVEL = 3;

	private static final String STATUS_ACTIVE = "1";

	private String receiveCode;
	private String agentId;
	private String agentName;
	private String agentCity;
	private String agentParentID;
	private String status;
	private String lagacyId;
	private String agentState;
	private int hierarchyLevel;
	private String paymentProfileId;

	public Agent(String agentId,	String agentParentID,String agentName,String agentCity,
		String receiveCode,	int hierarchyLevel,	String status,	String agentState,	String paymentProfileId)
	{
		final String emptyString = "";
		this.agentId = agentId;
		this.agentParentID = agentParentID;
		this.agentName = agentName == null ? emptyString : agentName;
		this.agentCity = agentCity == null ? emptyString : agentCity;
		this.receiveCode = receiveCode == null ? emptyString : receiveCode;
		this.hierarchyLevel = hierarchyLevel;
		this.status = status;
		this.agentState = agentState == null ? emptyString : agentState;
		this.paymentProfileId = paymentProfileId == null ? emptyString : paymentProfileId;
	}

	public String getReceiveCode()
	{
		return receiveCode;
	}

	public String getAgentId()
	{
		return agentId;
	}

	public String getAgentName()
	{
		return agentName;
	}

	public String getAgentCity()
	{
		return agentCity;
	}

	/**
	 * @return
	 */
	public String getAgentParentID()
	{
		return agentParentID;
	}

	/**
	 * @return
	 */
	public String getLagacyId()
	{
		return lagacyId == null ? "" : lagacyId;
	}

	/**
	 * @param string
	 */
	public void setLagacyId(String string)
	{
		lagacyId = string;
	}

	/**
	 * @return
	 */
	public String getAgentState()
	{
		return agentState == null ? "" : agentState;
	}

	/**
	 * @param string
	 */
	public void setAgentState(String string)
	{
		agentState = string;
	}

	/**
	 * @param string
	 */
	public void setAgentParentID(String parentID)
	{
		agentParentID = parentID;
	}

	public String getStatus()
	{
		return status == null ? "" : status;
	}

	public void setStatus(String string)
	{
		status = string;
	}
	public boolean isClosed()
	{
		return (STATUS_ACTIVE.equalsIgnoreCase(this.status) == false);
	}

	public String getDisplayName()
	{
		final String dash = " - "; //$NON-NLS-1$
		if ((getReceiveCode() == null) || (getReceiveCode().length() == 0))
		{
			return getAgentName() + dash + getAgentCity() + dash + getAgentId();
		} else
		{
			return getReceiveCode()
				+ dash
				+ getAgentName()
				+ dash
				+ getAgentCity()
				+ dash
				+ getAgentState()
				+ dash
				+ getAgentId();
		}
	}
	/**
	 * @return
	 */
	public int getHierarchyLevel()
	{
		return hierarchyLevel;
	}

	public boolean equals(Object obj)
	{
		boolean equality = (this == obj);
		if (equality == false)
		{
			if (obj instanceof Agent)
			{
				Agent other = (Agent) obj;
				equality = this.agentId.equalsIgnoreCase(other.agentId);
			}
		}
		return (equality);
	}

	public int hashCode()
	{
		return this.agentId.hashCode();
	}

	public String getPaymentProfileId() {
		return paymentProfileId;
	}

	public void setPaymentProfileId(String paymentProfileId) {
		this.paymentProfileId = paymentProfileId;
	}

}
