/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ChargebackComment {
	private String chargebackCommentId;
	private String tranId;
	private String commentText;
	private String createDate;
	private String createUserId;
	
	/**
	 * @return Returns the chargebackCommentId.
	 */
	public String getChargebackCommentId() {
		return chargebackCommentId;
	}
	/**
	 * @param chargebackCommentId The chargebackCommentId to set.
	 */
	public void setChargebackCommentId(String chargebackCommentId) {
		this.chargebackCommentId = chargebackCommentId;
	}
	/**
	 * @return Returns the commentText.
	 */
	public String getCommentText() {
		return commentText;
	}
	/**
	 * @param commentText The commentText to set.
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	/**
	 * @return Returns the createDate.
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate The createDate to set.
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return Returns the createUserId.
	 */
	public String getCreateUserId() {
		return createUserId;
	}
	/**
	 * @param createUserId The createUserId to set.
	 */
	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}
	/**
	 * @return Returns the tranId.
	 */
	public String getTranId() {
		return tranId;
	}
	/**
	 * @param tranId The tranId to set.
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
}
