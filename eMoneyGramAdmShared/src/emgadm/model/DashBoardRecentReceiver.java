package emgadm.model;

import java.util.List;

public class DashBoardRecentReceiver
{
	private String rcvFrstName;
	private String rcvLastName;
	private int cnt;
	private List rcvTranList;

	public String getRcvFrstName()
	{
		return rcvFrstName;
	}

	public void setRcvFrstName(String string)
	{
		rcvFrstName = string;
	}

	public String getRcvLastName()
	{
		return rcvLastName;
	}

	public void setRcvLastName(String string)
	{
		rcvLastName = string;
	}

	public int getCnt()
	{
		return cnt;
	}

	public void setCnt(int i)
	{
		cnt = i;
	}

	public List getRcvTranList()
	{
		return rcvTranList;
	}

	public void setRcvTranList(List list)
	{
		rcvTranList = list;
	}
}
