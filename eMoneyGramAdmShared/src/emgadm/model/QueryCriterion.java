/*
 * Created on May 12, 2004
 *
 */
package emgadm.model;

import java.util.Date;

import emgadm.exceptions.InvalidRangeException;
import emgadm.util.StringHelper;
import emgshared.util.I18NHelper;

/**
 * @author A131
 *
 */
public class QueryCriterion
{
	protected void validateWildcard(String s, char wildcard)
	{
		if (StringHelper.isNullOrEmpty(s) == false)
		{
			int wildcardIndex = s.indexOf(wildcard);
			if (wildcardIndex > -1)
			{
				if (s.length() < 3)
				{
					String message = I18NHelper.getFormattedMessage("errors.minlength.using.wildcard"); //$NON-NLS-1$
					throw new IllegalArgumentException(message);
				}
				if (wildcardIndex < s.length() - 1)
				{
					String message = I18NHelper.getFormattedMessage("errors.trailing.wildcard.only"); //$NON-NLS-1$
					throw new IllegalArgumentException(message);
				}
			}
		}
	}

	protected void validateRange(Date begin, Date end)
		throws InvalidRangeException
	{
		if (begin != null && end != null)
		{
			if (begin.after(end))
			{
				String message = I18NHelper.getFormattedMessage("errors.smaller.greater.than.larger", new Object[] { begin, end }); //$NON-NLS-1$
				throw new InvalidRangeException(message);
			}
		}
	}

	protected void validateRange(Double minimum, Double maximum)
		throws InvalidRangeException
	{
		if (minimum != null && maximum != null)
		{
			if (minimum.compareTo(maximum) > 0)
			{
				String message = I18NHelper.getFormattedMessage("errors.smaller.greater.than.larger", new Object[] { minimum, maximum }); //$NON-NLS-1$
				throw new InvalidRangeException(message);
			}
		}
	}
}
