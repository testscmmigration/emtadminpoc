/*
 * Created on Feb 4, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CustPhone {
	private int custId;
	private String custPhNbr;
	private String phTypeCode;
	private String phTypeDesc;
	
	/**
	 * @return
	 */
	public int getCustId() {
		return custId;
	}

	/**
	 * @param i
	 */
	public void setCustId(int i) {
		custId = i;
	}

	/**
	 * @return
	 */
	public String getCustPhNbr() {
		return custPhNbr == null ? "" : custPhNbr;
	}

	/**
	 * @param string
	 */
	public void setCustPhNbr(String string) {
		custPhNbr = string;
	}

	/**
	 * @return
	 */
	public String getPhTypeCode() {
		return phTypeCode == null ? "" : phTypeCode;
	}

	/**
	 * @param string
	 */
	public void setPhTypeCode(String string) {
		phTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getPhTypeDesc() {
		return phTypeDesc == null ? "" : phTypeDesc;
	}

	/**
	 * @param string
	 */
	public void setPhTypeDesc(String string) {
		phTypeDesc = string;
	}

}
