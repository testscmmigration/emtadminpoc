package emgadm.model;

import java.util.List;

import emgshared.model.ConsumerBankAccount;
import emgshared.model.ConsumerCreditCardAccount;

public class DashBoardView
{
	private String firstLoginDate;
	private String lastLoginDate;

	private String recentTotalAmount;
	private int recentTranCnt;
	private List recentTranList;

	private List recentRcvList;

	private int totalTranCnt;
	private String totalAvgAmt;
//	private int appCnt;
	private List appTranList;
//	private int sndCnt;
	private List sndTranList;
//	private int fundCnt;
	private List fundTranList;

//	private int bankAccountCnt;
	private ConsumerBankAccount[] bankAccounts;
//	private int creditCardAccountCnt;
	private ConsumerCreditCardAccount[] creditCardAccounts;

	public String getFirstLoginDate()
	{
		return firstLoginDate;
	}

	public void setFirstLoginDate(String string)
	{
		firstLoginDate = string;
	}

	public String getLastLoginDate()
	{
		return lastLoginDate;
	}

	public void setLastLoginDate(String string)
	{
		lastLoginDate = string;
	}

	public String getRecentTotalAmount()
	{
		return recentTotalAmount;
	}

	public void setRecentTotalAmount(String string)
	{
		recentTotalAmount = string;
	}

	public int getRecentTranCnt()
	{
		return recentTranCnt;
	}

	public void setRecentTranCnt(int i)
	{
		recentTranCnt = i;
	}

	public List getRecentTranList()
	{
		return recentTranList;
	}

	public void setRecentTranList(List list)
	{
		recentTranList = list;
	}

	public List getRecentRcvList()
	{
		return recentRcvList;
	}

	public void setRecentRcvList(List list)
	{
		recentRcvList = list;
	}

	public int getTotalTranCnt()
	{
		return totalTranCnt;
	}

	public void setTotalTranCnt(int i)
	{
		totalTranCnt = i;
	}

	public String getTotalAvgAmt()
	{
		return totalAvgAmt;
	}

	public void setTotalAvgAmt(String string)
	{
		totalAvgAmt = string;
	}

	public int getAppCnt()
	{
		return appTranList == null ? 0 : appTranList.size();
	}

	public List getAppTranList()
	{
		return appTranList;
	}

	public void setAppTranList(List list)
	{
		appTranList = list;
	}

	public int getSndCnt()
	{
		return sndTranList == null ? 0 : sndTranList.size();
	}

	public List getSndTranList()
	{
		return sndTranList;
	}

	public void setSndTranList(List list)
	{
		sndTranList = list;
	}

	public int getFundCnt()
	{
		return fundTranList == null ? 0 : fundTranList.size();
	}

	public List getFundTranList()
	{
		return fundTranList;
	}

	public void setFundTranList(List list)
	{
		fundTranList = list;
	}

	public int getBankAccountCnt()
	{
		return bankAccounts == null ? 0 : bankAccounts.length;
	}

	public ConsumerBankAccount[] getBankAccounts()
	{
		return bankAccounts;
	}

	public void setBankAccounts(ConsumerBankAccount[] accounts)
	{
		bankAccounts = accounts;
	}

	public int getCreditCardAccountCnt()
	{
		return 	creditCardAccounts == null ? 0 : creditCardAccounts.length;
	}

	public ConsumerCreditCardAccount[] getCreditCardAccounts()
	{
		return creditCardAccounts;
	}

	public void setCreditCardAccounts(ConsumerCreditCardAccount[] accounts)
	{
		creditCardAccounts = accounts;
	}
}
