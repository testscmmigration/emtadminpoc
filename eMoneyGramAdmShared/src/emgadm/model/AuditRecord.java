/*
 * Created on Mar 31, 2004
 */
package emgadm.model;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import emgadm.util.AuditLogUtils;
import emgadm.util.StringHelper;

/**
 * @author A132
 */
public final class AuditRecord {
	private static final Map events;
	private static final Map commands;

	private String logNumber;
	private String command;
	private String paramList;
	private String detail;
	private String uID;
	private String ipAddress;
	private String type;
	private String billerMainOfficeNumber;
	private String divisionNumber;
	private String locationNumber;
	private Date time = new Date();
	private boolean internal;

	static {
		Map eventMap = new HashMap();
		eventMap.put(AuditLogUtils.LOGIN_EXCESSIVE_FAILURE, "Login - Excessive Failures - Disabled ID"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.LOGIN_INACTIVE, "Login - Inactive User"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.PASSWORD_CHANGE, "Password Change"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.DAILY_TRAN_REPORT, "Daily Transaction Report"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.REALTIME_TRAN_REPORT, "Real-Time Transaction Report"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.ADHOC_TRAN_REPORT, "Adhoc Transaction Report"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.SUMMARY_REPORT, "Summary Transaction Report"); //$NON-NLS-1$
//		eventMap.put(AuditLogUtils.EXTERNAL_USER_ADD, "User Add"); //$NON-NLS-1$
//		eventMap.put(AuditLogUtils.EXTERNAL_USER_EDIT, "User Edit"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.INACTIVE_USERS_REPORT, "Inactive User Report"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.ACCT_ADMIN_SECURITY, "Account Admin - Security"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.ACCT_ADMIN_BUSINESS, "Account Admin - Business Day"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.TRANS_DETAIL_VIEW, "Transaction Detail View"); //$NON-NLS-1$
		eventMap.put(AuditLogUtils.SYSTEM_ERROR, "System Errors"); //$NON-NLS-1$
		events = Collections.unmodifiableMap(eventMap);
		eventMap = null;

		Map commandMap = new HashMap();
		commandMap.put(AuditLogUtils.LOGIN_SUCCESS, "eMoneyGramAdmLogin"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.LOGIN_INACTIVE, "eMoneyGramAdmLogin"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.LOGIN_EXCESSIVE_FAILURE, "eMoneyGramAdmLogin"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.PASSWORD_CHANGE, "changePassword"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.DAILY_TRAN_REPORT, "dailyTransactions"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.REALTIME_TRAN_REPORT, "realtimeTransactions"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.ADHOC_TRAN_REPORT, "adhocQuery"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.SUMMARY_REPORT, "summaryReport"); //$NON-NLS-1$
//		commandMap.put(AuditLogUtils.EXTERNAL_USER_ADD, "externalUserAdmin"); //$NON-NLS-1$
//		commandMap.put(AuditLogUtils.EXTERNAL_USER_EDIT, "externalUserAdmin"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.INACTIVE_USERS_REPORT, "inactiveUsers"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.ACCT_ADMIN_SECURITY, "administerSecurityStandards"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.ACCT_ADMIN_BUSINESS, "administerBusinessStandards"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.TRANS_DETAIL_VIEW, "showTransactionDetail"); //$NON-NLS-1$
		commandMap.put(AuditLogUtils.SYSTEM_ERROR, "systemError"); //$NON-NLS-1$
		commands = Collections.unmodifiableMap(commandMap);
		commandMap = null;
	}


	public AuditRecord(UserProfile user)
	{
		if (user != null)
		{
			uID = user.getUID();
			internal = user.isInternalUser();
		}
	}

	public String getDescription()
	{
		String description = null;
		String eventType = getType();
		if (StringHelper.isNullOrEmpty(eventType) == false)
		{
			description = (String) getEventMap().get(eventType.trim());
		}
		return description == null ? "" : description; //$NON-NLS-1$
	}

	public Map getEventMap()
	{
		return events;
	}

	public String getCommand(String eventType)
	{
		return (String) commands.get(eventType);
	}

	public String getBillerMainOfficeNumber()
	{
		return billerMainOfficeNumber;
	}

	public String getCommand()
	{
		return command;
	}

	public String getDetail()
	{
		return detail;
	}

	public String getDivisionNumber()
	{
		return divisionNumber;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public boolean isInternal()
	{
		return internal;
	}

	public String getLocationNumber()
	{
		return locationNumber;
	}

	public String getLogNumber()
	{
		return logNumber;
	}

	public String getParamList()
	{
		return paramList;
	}

	public Date getTime()
	{
		return time;
	}

	public String getType()
	{
		return type;
	}

	public String getUID()
	{
		return uID;
	}

	public void setBillerMainOfficeNumber(String string)
	{
		billerMainOfficeNumber = string;
	}

	public void setCommand(String string)
	{
		command = string;
	}

	public void setDetail(String string)
	{
		detail = string;
	}

	public void setDivisionNumber(String string)
	{
		divisionNumber = string;
	}

	public void setIpAddress(String string)
	{
		ipAddress = string;
	}

	public void setInternal(boolean b)
	{
		internal = b;
	}

	public void setLocationNumber(String string)
	{
		locationNumber = string;
	}

	public void setLogNumber(String string)
	{
		logNumber = string;
	}

	public void setParamList(String string)
	{
		paramList = string;
	}

	public void setTime(Date date)
	{
		time = date;
	}

	public void setType(String string)
	{
		type = string;
	}

	public void setUID(String string)
	{
		uID = string;
	}

	public String toString()
	{
		StringBuffer buffer = new StringBuffer();

		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Command: " + getCommand()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Detail: " + getDetail()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Event Type: " + getType()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("UserId: " + getUID()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("IsInternal: " + isInternal()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Biller Main Office: " + getBillerMainOfficeNumber()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Division: " + getDivisionNumber()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Location: " + getLocationNumber()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("IP Address: " + getIpAddress()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Param List: " + getParamList()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$
		buffer.append("Time: " + getTime()); //$NON-NLS-1$
		buffer.append("\n"); //$NON-NLS-1$

		return buffer.toString();
	}
}
