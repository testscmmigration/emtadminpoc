package emgadm.model;

public class CommandAndRoleMatrixBean
		implements Comparable<CommandAndRoleMatrixBean> {
	private String command;
	private String menuGroupName;
	private int menuLevel;
	private String displayName;
	private String[] rolePermission;
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String string) {
		command = string;
	}

	public String getMenuGroupName() {
		return menuGroupName;
	}

	public void setMenuGroupName(String string) {
		menuGroupName = string;
	}

	public int getMenuLevel() {
		return menuLevel;
	}

	public void setMenuLevel(int i) {
		menuLevel = i;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String string) {
		displayName = string;
	}

	public String[] getRolePermission() {
		return rolePermission;
	}
	
	public String getRolePermission(int i) {
		return rolePermission[i];
	}

	public void setRolePermission(String[] strings) {
		rolePermission = strings;
	}

	public int compareTo(CommandAndRoleMatrixBean cmd) {
		if (this.menuGroupName.equalsIgnoreCase(cmd.menuGroupName)) {
			if (this.menuLevel == cmd.menuLevel) {
				return this.command.compareTo(cmd.command);
			} else {
				return this.menuLevel - cmd.menuLevel;
			}
		}
		 
		return this.menuGroupName.compareTo(cmd.menuGroupName);
	}
}
