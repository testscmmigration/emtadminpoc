package emgadm.model;

import java.util.Collection;
import java.util.List;

public interface Role extends Comparable<Role> {
	public static final String PROGRAM_ROLENAME_SEPARATOR = ":";

	String getId();
	void setId(String s);
	String getName();
	void setName(String s);
	String getDescription();
	void setDescription(String s);
	String getDisplayName();
	String getShortName();
	String getApplicationName();
	String getAccess();
	String getMenu();
	Collection<ApplicationCommand> getApplicationCommands();
	void setApplicationCommands(Collection<ApplicationCommand> col);
	boolean isShowSendingAgentInfo();
	boolean hasPermission(String command);
	boolean hasPermission(String command, Role roles);
	boolean contains(ApplicationCommand command);
	boolean contains(String command);
	List<Program> getProgramsWithPermission(String command);
	String getHTMLWrappedShortName();
	String getShortNamePlusProgramPrefix();
}
