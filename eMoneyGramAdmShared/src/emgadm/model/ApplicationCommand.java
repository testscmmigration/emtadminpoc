/*
 * Created on Mar 1, 2004
 *
 */
package emgadm.model;

import java.util.Comparator;

/**
 * @author A131
 *
 */
public interface ApplicationCommand {
	String getDisplayName();
	int getMenuLevel();
	String getMenuGroupName();
	String getId();
	void setId(String s);
	String getDescription();
	boolean isMenuItem();
	boolean isAllowInternal();
	boolean isAuthorizationRequired();
	Comparator<ApplicationCommand> getCommandSorterByMenuAndDisplayName();
}