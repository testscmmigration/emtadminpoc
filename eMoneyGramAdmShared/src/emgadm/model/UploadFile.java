/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.upload.FormFile;

import emgadm.constants.EMoneyGramAdmApplicationConstants;
import emgadm.dataaccessors.TransactionManager;
import emgadm.sysadmin.AchReturnRow;
import emgadm.sysadmin.AreaCodeStateRow;
import emgadm.sysadmin.EmployeeListRow;
import emgadm.sysadmin.UploadFileRow;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class UploadFile {
    private FormFile theFile;
	private String fileType;
    private Collection fileContent;
    private int fileControlNumber;
    
	/**
	 * @return Returns the fileControlNumber.
	 */
	public int getFileControlNumber() {
		return fileControlNumber;
	}
	/**
	 * @param fileControlNumber The fileControlNumber to set.
	 */
	public void setFileControlNumber(int fileControlNumber) {
		this.fileControlNumber = fileControlNumber;
	}
	private int totalRows;
	private int totalGoodRows;
	private int totalFaultyRows;
	private int totalUploadRows;
	private int totalUploadGoodRows;
	private int totalUploadFaultyRows;
    
	public UploadFile(String fileType, FormFile theFile){
		this.fileType = fileType;
		this.theFile = theFile;
	}
	/**
	 * @return Returns the fileContent.
	 */
	public Collection getFileContent() {
		return fileContent;
	}
	/**
	 * @param fileContent The fileContent to set.
	 */
	public void setFileContent(Collection fileContent) {
		this.fileContent = fileContent;
	}
	/**
	 * @return Returns the totalUploadFaultyRows.
	 */
	public int getTotalUploadFaultyRows() {
		return totalUploadFaultyRows;
	}
	/**
	 * @param totalUploadFaultyRows The totalUploadFaultyRows to set.
	 */
	public void setTotalUploadFaultyRows(int totalUploadFaultyRows) {
		this.totalUploadFaultyRows = totalUploadFaultyRows;
	}
	/**
	 * @return Returns the totalUploadGoodRows.
	 */
	public int getTotalUploadGoodRows() {
		return totalUploadGoodRows;
	}
	/**
	 * @param totalUploadGoodRows The totalUploadGoodRows to set.
	 */
	public void setTotalUploadGoodRows(int totalUploadGoodRows) {
		this.totalUploadGoodRows = totalUploadGoodRows;
	}
	/**
	 * @return Returns the totalUploadRows.
	 */
	public int getTotalUploadRows() {
		return totalUploadRows;
	}
	/**
	 * @param totalUploadRows The totalUploadRows to set.
	 */
	public void setTotalUploadRows(int totalUploadRows) {
		this.totalUploadRows = totalUploadRows;
	}


	/**
	 * @return Returns the totalGoodRows.
	 */
	public int getTotalGoodRows() {
		return totalGoodRows;
	}
	/**
	 * @param totalGoodRows The totalGoodRows to set.
	 */
	public void setTotalGoodRows(int totalGoodRows) {
		this.totalGoodRows = totalGoodRows;
	}
    
	/**
	 * @return Returns the totalFaultyRows.
	 */
	public int getTotalFaultyRows() {
		return totalFaultyRows;
	}
	/**
	 * @param totalFaultyRows The totalFaultyRows to set.
	 */
	public void setTotalFaultyRows(int totalFaultyRows) {
		this.totalFaultyRows = totalFaultyRows;
	}
	/**
	 * @return Returns the totalRows.
	 */
	public int getTotalRows() {
		return totalRows;
	}
	/**
	 * @param totalRows The totalRows to set.
	 */
	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}
	/**
	 * @return Returns the theFile.
	 */
	public FormFile getTheFile() {
		return theFile;
	}
	/**
	 * @param theFile The theFile to set.
	 */
	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	public UploadFile(){
	}
	
	public UploadFile(String fileType){
		this.fileType = fileType;
	}

	/**
	 * @return Returns the fileType.
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType The fileType to set.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	
	public void parseFile() throws Exception{
        InputStreamReader isr = new InputStreamReader(this.getTheFile().getInputStream()); 
        BufferedReader br = new BufferedReader(isr);	
        List fileContent = new ArrayList();
        boolean addRow = true;
        int totalRows = 0;
        int totalFaultRows = 0;
        int totalGoodRows = 0;
    	UploadFileRow row = null;
    	
        String nextLine = null; 
        while ((nextLine = br.readLine()) != null) {
        	addRow = true;
        	
        	if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.EMPLOYEE_LIST_FILE_UPLOAD))
        		row = new EmployeeListRow();
        	else if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.AREACODE_STATE_FILE_UPLOAD))
        		row = new AreaCodeStateRow();
        	else if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.ACH_RETURN_FILE_UPLOAD))
        		row = new AchReturnRow();
        		

        	row.setRow(nextLine);
        	row.validate();
        	
        	if (row instanceof AchReturnRow){
        		AchReturnRow achReturnRow = (AchReturnRow)row;
        		if (achReturnRow.getAchReturn().getMicroTranId()!=null){
        			for (Iterator iter = fileContent.iterator(); iter.hasNext();){
        				AchReturnRow arRow = (AchReturnRow) iter.next();
        				if (achReturnRow.getAchReturn().getMicroTranId().equals(arRow.getAchReturn().getMicroTranId())){
        					addRow = false;
        					break;
        				}
        			}
        		}
        	}

        	if (addRow){
            	totalRows++;
            	if (!row.isValid())
            		totalFaultRows++;
            	else
            		totalGoodRows++;
        		fileContent.add(row);
        	}
        	
        }
        
        setTotalRows(totalRows);
        setTotalFaultyRows(totalFaultRows);
        setTotalGoodRows(totalGoodRows);
        setFileContent(fileContent);
	}	

	public void uploadFile(String userId, TransactionManager tm){
        int totalUploadRows = 0;
        int totalUploadFaultRows = 0;
        int totalUploadGoodRows = 0;
    	UploadFileRow row = null;
        
		for (Iterator it = getFileContent().iterator(); it.hasNext();)
		{
			totalUploadRows++;
			
        	if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.EMPLOYEE_LIST_FILE_UPLOAD))
        		row = (EmployeeListRow) it.next();
           	else if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.AREACODE_STATE_FILE_UPLOAD))
        		row = (AreaCodeStateRow) it.next();
           	else if (fileType.equalsIgnoreCase(EMoneyGramAdmApplicationConstants.ACH_RETURN_FILE_UPLOAD))
        		row = (AchReturnRow) it.next();

        	if (row.isValid())
				row.upload(userId, tm, this.fileControlNumber);

			if (!row.isValid())
				totalUploadFaultRows++;
        	else
        		totalUploadGoodRows++;
        }
        
        setTotalUploadRows(totalUploadRows);
        setTotalUploadFaultyRows(totalUploadFaultRows);
        setTotalUploadGoodRows(totalUploadGoodRows);
	}	
}
