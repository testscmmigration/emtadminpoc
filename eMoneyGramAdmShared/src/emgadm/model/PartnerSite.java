/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

import java.util.TimeZone;

/**
 * @author A119
 *  Holds info relating to MGO partner sites
 */
public class PartnerSite {
	
	
	private String partnetSideIdCode;
	private String partnerSiteId;
	private String partnerSiteDesc;
	private String partnerSiteNarrDesc;
	private String partnerSiteDipslayOrder;
	private String partnerSiteCurrency;
	private String country;
	private String countryAbbr;
	private TimeZone timeZone;
	
	public String getPartnerSiteCurrency() {
		return partnerSiteCurrency;
	}
	public void setPartnerSiteCurrency(String partnerSiteCurrency) {
		this.partnerSiteCurrency = partnerSiteCurrency;
	}
	public String getPartnetSideIdCode() {
		return partnetSideIdCode;
	}
	public void setPartnetSideIdCode(String partnetSideIdCode) {
		this.partnetSideIdCode = partnetSideIdCode;
	}
	public String getPartnerSiteId() {
		return partnerSiteId;
	}
	public void setPartnerSiteId(String partnerSiteId) {
		this.partnerSiteId = partnerSiteId;
	}
	public String getPartnerSiteDesc() {
		return partnerSiteDesc;
	}
	public void setPartnerSiteDesc(String partnerSiteDesc) {
		this.partnerSiteDesc = partnerSiteDesc;
	}
	public String getPartnerSiteNarrDesc() {
		return partnerSiteNarrDesc;
	}
	public void setPartnerSiteNarrDesc(String partnerSiteNarrDesc) {
		this.partnerSiteNarrDesc = partnerSiteNarrDesc;
	}
	public String getPartnerSiteDipslayOrder() {
		return partnerSiteDipslayOrder;
	}
	public void setPartnerSiteDipslayOrder(String partnerSiteDipslayOrder) {
		this.partnerSiteDipslayOrder = partnerSiteDipslayOrder;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	public void setCountryAbbr(String countryAbbr) {
		this.countryAbbr = countryAbbr;
	}
	public String getCountryAbbr() {
		return countryAbbr;
	}
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
	public TimeZone getTimeZone() {
		return timeZone;
	}

}
