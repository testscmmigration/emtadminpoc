package emgadm.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author David Hadley
 */
public class Country implements Serializable, Comparable
{
	private String _isoCountryCode;
	private String _countryName;
	private SortedSet _states = new TreeSet();

	public Country()
	{}

	public void addState(State state)
	{
		_states.add(state);
	}

	public void removeState(State state)
	{
		_states.remove(state);
	}

	public Collection getStates()
	{
		return _states;
	}

	public String getISOCountryCode()
	{
		return _isoCountryCode;
	}

	public void setISOCountryCode(String isoCountryCode)
	{
		_isoCountryCode = isoCountryCode;
	}

	public String getCountryName()
	{
		return _countryName;
	}

	public void setCountryName(String countryName)
	{
		_countryName = countryName;
	}

	public boolean equals(Object object)
	{
		Country country = (Country) object;
		return country.getISOCountryCode().equals(getISOCountryCode());
	}

	public int compareTo(Object object)
	{
		Country country = (Country) object;
		return country.getISOCountryCode().compareTo(getISOCountryCode());
	}
}
