/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T008
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TranSubStatus {
	private String tranSubStatCode;
	private String tranSubStatDesc;
	
	/**
	 * @return
	 */
	public String getTranSubStatCode() {
		return tranSubStatCode == null ? "" : tranSubStatCode;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatCode(String string) {
		tranSubStatCode = string;
	}

	/**
	 * @return
	 */
	public String getTranSubStatDesc() {
		return tranSubStatDesc == null ? "" : tranSubStatDesc;
	}

	/**
	 * @param string
	 */
	public void setTranSubStatDesc(String string) {
		tranSubStatDesc = string;
	}

}
