/*
 * Created on Aug 2, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.model;

/**
 * @author T004
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CustomerContactMethodType {
	private String customerContactMethodTypeCode;
	private String customerContactMethodTypeDesc;
	
	/**
	 * @return Returns the customerContactMethodTypeCode.
	 */
	public String getCustomerContactMethodTypeCode() {
		return customerContactMethodTypeCode;
	}
	/**
	 * @param customerContactMethodTypeCode The customerContactMethodTypeCode to set.
	 */
	public void setCustomerContactMethodTypeCode(
			String customerContactMethodTypeCode) {
		this.customerContactMethodTypeCode = customerContactMethodTypeCode;
	}
	/**
	 * @return Returns the customerContactMethodTypeDesc.
	 */
	public String getCustomerContactMethodTypeDesc() {
		return customerContactMethodTypeDesc;
	}
	/**
	 * @param customerContactMethodTypeDesc The customerContactMethodTypeDesc to set.
	 */
	public void setCustomerContactMethodTypeDesc(
			String customerContactMethodTypeDesc) {
		this.customerContactMethodTypeDesc = customerContactMethodTypeDesc;
	}
}
