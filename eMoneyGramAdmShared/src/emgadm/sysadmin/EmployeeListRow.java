/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import java.text.DecimalFormat;

import emgadm.dataaccessors.TransactionManager;
import emgshared.util.StringHelper;

/**
 * @author Jawahar Varadhan
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EmployeeListRow extends UploadFileRow{
	public String firstName;
	public String LastName;
	public String middleInitial;
	public String SSN;
	public String statusCode;
	
	/**
	 * @return Returns the statusCode.
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode The statusCode to set.
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public void validate(){
		String currentRow = this.row;
		
		try{
			if (currentRow==null || currentRow.trim().equals("")){
				setError("Empty or Null row or column");
				setValid(false);
				return;
			}else{
				currentRow = StringHelper.replaceAll(currentRow, "\"",  "");
				currentRow = StringHelper.replaceAll(currentRow, "\\.", "");
				if (StringHelper.split(currentRow, ",").length != 3){
					setError("Insufficient/Invalid Data");
					setValid(false);
					return;
				}else{
					ParseAndLoad(currentRow);
				}
			}
		}catch(Exception e){
				setError(e.toString());
				setValid(false);				
		}

	}
	
	/**
	 * This Method writes each row into the database. Only the validated rows will get Uploaded
	 */
	public void upload(String userId, TransactionManager tm, int fileControlNumber){
		String statusCode  = "ACT";

		try{
			if (this.isValid()){
				tm.insertEmployeeRecord(userId, this.LastName, this.firstName, this.middleInitial, this.SSN,
						                fileControlNumber, statusCode);
			}
		}catch(Exception e){
				setError(e.getMessage());
				setValid(false);				
		}		

	}	
	private void ParseAndLoad(String currentRow) throws Exception{
		DecimalFormat df = new DecimalFormat("0000");		
		String [] fields = StringHelper.split(currentRow, ",");

		setFirstName(fields[0].trim());
		setSSN(df.format(Integer.parseInt(fields[2])));
		
		String [] strArray = StringHelper.split(fields[1].trim()," ");

		if (strArray.length != 0){
			setLastName(strArray[0]);
			if (strArray.length > 1)
				setMiddleInitial(strArray[1].trim());
		}
			
		return;		
	}
	
	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return LastName;
	}
	/**
	 * @param lastName The lastName to set.
	 */
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	/**
	 * @return Returns the middleInitial.
	 */
	public String getMiddleInitial() {
		return middleInitial;
	}
	/**
	 * @param middleInitial The middleInitial to set.
	 */
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	/**
	 * @return Returns the sSN.
	 */
	public String getSSN() {
		return SSN;
	}
	/**
	 * @param ssn The sSN to set.
	 */
	public void setSSN(String ssn) {
		SSN = ssn;
	}
}
	

