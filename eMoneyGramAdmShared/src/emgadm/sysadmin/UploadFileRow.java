/*
 * Created on Feb 7, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package emgadm.sysadmin;

import emgadm.dataaccessors.TransactionManager;


/**
 * @author Jawahar Varadhan
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class UploadFileRow {

	protected String row;
	protected boolean valid = true;
	protected String error;
	
    public abstract void validate(); 
    public abstract void upload(String userId, TransactionManager tm, int fileControlNumber);
    
	/**
	 * @return Returns the error.
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error The error to set.
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return Returns the row.
	 */
	public String getRow() {
		return row;
	}
	/**
	 * @param row The row to set.
	 */
	public void setRow(String row) {
		this.row = row;
	}
	/**
	 * @return Returns the valid.
	 */
	public boolean isValid() {
		return valid;
	}
	/**
	 * @param valid The valid to set.
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}


