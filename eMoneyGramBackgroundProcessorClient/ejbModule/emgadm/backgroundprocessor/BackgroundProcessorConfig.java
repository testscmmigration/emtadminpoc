package emgadm.backgroundprocessor;


public class BackgroundProcessorConfig
		implements java.io.Serializable {

	private static final long serialVersionUID = 2345961528310181698L;

	private String loopbackServer;

	private String loopbackHost;

	private String contextName;


	public static final long DEFAULT_TIME_TO_PROCESS_NEXT = 2000;
	public static final long DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE = 30000;
	public static final long DEFAULT_TIME_TO_START = 1000;
	public static final long DEFAULT_TIME_TO_RETRY_WHEN_BUSY = 5000;
	public static final long DEFAULT_TIME_TO_RETRY_AFTER_ERROR = 30000;
	public static final int DEFAULT_MAX_ERRORS = 500;

	private long timeToProcessNext = DEFAULT_TIME_TO_PROCESS_NEXT;
	private long timeToPollBackAfterIdle = DEFAULT_TIME_TO_POLL_BACK_AFTER_IDLE;
	private long timeToStart = DEFAULT_TIME_TO_START;
	private long timeToRetryWhenBusy = DEFAULT_TIME_TO_RETRY_WHEN_BUSY;
	private long timeToRetryAfterError = DEFAULT_TIME_TO_RETRY_AFTER_ERROR;

	private int maxErrors = DEFAULT_MAX_ERRORS;

	public long getTimeToProcessNext() {
		return timeToProcessNext;
	}

	public void setTimeToProcessNext(long timeToProcessNext) {
		this.timeToProcessNext = timeToProcessNext;
	}

	public long getTimeToPollBackAfterIdle() {
		return timeToPollBackAfterIdle;
	}

	public void setTimeToPollBackAfterIdle(long timeToPollBackAfterIdle) {
		this.timeToPollBackAfterIdle = timeToPollBackAfterIdle;
	}

	public long getTimeToStart() {
		return timeToStart;
	}

	public void setTimeToStart(long timeToStart) {
		this.timeToStart = timeToStart;
	}

	public long getTimeToRetryWhenBusy() {
		return timeToRetryWhenBusy;
	}

	public void setTimeToRetryWhenBusy(long timeToRetryWhenBusy) {
		this.timeToRetryWhenBusy = timeToRetryWhenBusy;
	}

	public long getTimeToRetryAfterError() {
		return timeToRetryAfterError;
	}

	public void setTimeToRetryAfterError(long timeToRetryAfterError) {
		this.timeToRetryAfterError = timeToRetryAfterError;
	}


	public String getLoopbackServer() {
		return loopbackServer;
	}

	public void setLoopbackServer(String loopbackServer) {
		this.loopbackServer = loopbackServer;
	}

	public String getLoopbackHost() {
		return loopbackHost;
	}

	public void setLoopbackHost(String loopbackHost) {
		this.loopbackHost = loopbackHost;
	}

	public String getContextName() {
		return contextName;
	}

	public void setContextName(String contextName) {
		this.contextName = contextName;
	}

	public int getMaxErrors() {
		return maxErrors ;
	}

	public void setMaxErrors(int maxErrors) {
		this.maxErrors = maxErrors;
	}

}
