package emgadm.backgroundprocessor;

/**
 * Local interface for emgadm.backgroundprocessor.BackgroundProcessorBean bean.
 */
public interface BackgroundProcessorLocal extends javax.ejb.EJBLocalObject,
		emgadm.backgroundprocessor.BackgroundProcessor {

}
