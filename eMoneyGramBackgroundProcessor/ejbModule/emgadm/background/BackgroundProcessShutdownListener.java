package emgadm.background;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class BackgroundProcessShutdownListener
implements ServletContextListener
{
    
    public BackgroundProcessShutdownListener() {

    }
    
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("context destroy. shutting down background processing.");
        BackgroundProcessMaster.shutdownAutoProcessing();
        System.out.println("eMoneyTransfer Admin Application has shutdown");
    }
    public void contextInitialized(ServletContextEvent sce) {
        
    }
    
}
