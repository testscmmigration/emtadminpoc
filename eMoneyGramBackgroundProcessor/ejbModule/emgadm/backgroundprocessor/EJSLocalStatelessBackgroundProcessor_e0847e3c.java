package emgadm.backgroundprocessor;

import com.ibm.ejs.container.*;
import java.rmi.RemoteException;

/**
 * EJSLocalStatelessBackgroundProcessor_e0847e3c
 */
public class EJSLocalStatelessBackgroundProcessor_e0847e3c extends EJSLocalWrapper implements emgadm.backgroundprocessor.BackgroundProcessorLocal {
	/**
	 * EJSLocalStatelessBackgroundProcessor_e0847e3c
	 */
	public EJSLocalStatelessBackgroundProcessor_e0847e3c() {
		super();	}
	/**
	 * cancelAllTimers
	 */
	public void cancelAllTimers() {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if ( container.doesJaccNeedsEJBArguments(this) )
			{
				_jacc_parms = new Object[0];
			}
	emgadm.backgroundprocessor.BackgroundProcessorBean beanRef = (emgadm.backgroundprocessor.BackgroundProcessorBean)container.preInvoke(this, 0, _EJS_s, _jacc_parms);
			beanRef.cancelAllTimers();
		}
		catch (java.rmi.RemoteException ex) {
		 	_EJS_s.setUncheckedLocalException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedLocalException(ex);
		}

		finally {
			try {
				container.postInvoke(this, 0, _EJS_s);
			} catch ( RemoteException ex ) {
				_EJS_s.setUncheckedLocalException(ex);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * start
	 */
	public void start(emgadm.backgroundprocessor.BackgroundProcessorConfig config) {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if ( container.doesJaccNeedsEJBArguments(this) )
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = config;
			}
	emgadm.backgroundprocessor.BackgroundProcessorBean beanRef = (emgadm.backgroundprocessor.BackgroundProcessorBean)container.preInvoke(this, 1, _EJS_s, _jacc_parms);
			beanRef.start(config);
		}
		catch (java.rmi.RemoteException ex) {
		 	_EJS_s.setUncheckedLocalException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedLocalException(ex);
		}

		finally {
			try {
				container.postInvoke(this, 1, _EJS_s);
			} catch ( RemoteException ex ) {
				_EJS_s.setUncheckedLocalException(ex);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
	/**
	 * startProcessApprovedBankTx
	 */
	public void startProcessApprovedBankTx(emgadm.backgroundprocessor.ProcessApprovedBankTxProcessorConfig config) {
		EJSDeployedSupport _EJS_s = container.getEJSDeployedSupport(this);
		Object[] _jacc_parms = null;
		
		try {
			if ( container.doesJaccNeedsEJBArguments(this) )
			{
				_jacc_parms = new Object[1];
				_jacc_parms[0] = config;
			}
	emgadm.backgroundprocessor.BackgroundProcessorBean beanRef = (emgadm.backgroundprocessor.BackgroundProcessorBean)container.preInvoke(this, 2, _EJS_s, _jacc_parms);
			beanRef.startProcessApprovedBankTx(config);
		}
		catch (java.rmi.RemoteException ex) {
		 	_EJS_s.setUncheckedLocalException(ex);
		}
		catch (Throwable ex) {
			_EJS_s.setUncheckedLocalException(ex);
		}

		finally {
			try {
				container.postInvoke(this, 2, _EJS_s);
			} catch ( RemoteException ex ) {
				_EJS_s.setUncheckedLocalException(ex);
			} finally {
				container.putEJSDeployedSupport(_EJS_s);
			}
		}
		return ;
	}
}
