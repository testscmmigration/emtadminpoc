// @annotations-disabled tagSet="websphere" tagSet="ejb"
package emgadm.backgroundprocessor;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.SessionContext;
import javax.ejb.TimedObject;
import javax.ejb.Timer;

import com.moneygram.common.log.Logger;
import com.moneygram.common.log.log4j.Log4JFactory;

import emgadm.background.ApprovedBankTxProcessInitiator;
import emgadm.background.ApprovedBankTxProcessorRequest;
import emgadm.background.BackGroundProcessorRequest;
import emgadm.background.BackgroundProcessInitiator;
import emgadm.property.EMTAdmContainerProperties;

/**
 * Bean implementation class for Session Bean: BackgroundProcessor
 *
 * @ejb.bean
 *	name="BackgroundProcessor"
 *	type="Stateless"
 *	local-jndi-name="ejb/emgadm/backgroundprocessor/BackgroundProcessorLocalHome"
 *	view-type="local"
 *	transaction-type="Container"
 *
 * @ejb.home
 *	local-class="emgadm.backgroundprocessor.BackgroundProcessorLocalHome"
 *
 * @ejb.interface
 *	local-class="emgadm.backgroundprocessor.BackgroundProcessorLocal"
 *  local-extends="javax.ejb.EJBLocalObject, emgadm.backgroundprocessor.BackgroundProcessor"
 *
 */
public class BackgroundProcessorBean
		implements javax.ejb.SessionBean, BackgroundProcessor,
			TimedObject {
	private static final long serialVersionUID = -6200267820192202241L;

	protected static Logger mLogr = Log4JFactory.getInstance().getLogger(BackgroundProcessorBean.class);
	private SessionContext mySessionCtx;
	private static boolean bgScoringStarted = false;
	private static boolean bgApproveBankStarted = false;

	/**
	 * getSessionContext
	 */
	public SessionContext getSessionContext() {
		return mySessionCtx;
	}

	/**
	 * setSessionContext
	 */
	public void setSessionContext(SessionContext ctx) {
		mySessionCtx = ctx;
	}

	/**
	 * ejbCreate
	 */
	public void ejbCreate() throws CreateException {
		mLogr.debug("S26-BackgroundProcessor.ejbCreate()...");
	}

	/**
	 * ejbActivate
	 */
	public void ejbActivate() {
	}

	/**
	 * ejbPassivate
	 */
	public void ejbPassivate() {
	}

	/**
	 * ejbRemove
	 */
	public void ejbRemove() {
	}

	public void cancelAllTimers() {

		Collection<Timer> timers = mySessionCtx.getTimerService().getTimers();
		mLogr.debug("S26-cancelAllTimers(START);#timers = " + getNbrTimers());
		if (timers == null) {
			mLogr.debug("S26-cancelAllTimers - timers == null");
			return;
		}
		for (Iterator<Timer> iterator = timers.iterator(); iterator.hasNext();) {
			Timer timer = iterator.next();
			timer.cancel();
		}
		mLogr.debug("S26-cancelAllTimers(END);#timers = " + getNbrTimers());
	}

	public void start(BackgroundProcessorConfig config) {
		cancelTimers(config); // cancel any timers of this type hanging around
								// before restart
		bgScoringStarted = true;
		// Adding more than one timer in order to speed up the processing of
		// transactions.
		long timerStartValue = config.getTimeToStart();
		int nbrBackgroundTimers = backgroundTimers();
		for (int timerCount = 0; timerCount < nbrBackgroundTimers; timerCount++) {
			timerStartValue += (timerCount * 100);
			mySessionCtx.getTimerService().createTimer(timerStartValue, config);
		}

		mLogr.debug("S26-BGEJB.start(): #timers = " + getNbrTimers());
	}
		

	private int getNbrTimers() {
		int nbrTimers = mySessionCtx.getTimerService().getTimers()==null?0:mySessionCtx.getTimerService().getTimers().size();
		return nbrTimers;
	}

	public void startProcessApprovedBankTx(
			ProcessApprovedBankTxProcessorConfig config) {
		cancelTimers(config); // cancel any timers of this type hanging around
								// before restart
		bgApproveBankStarted = true;
		long timerStartValue = config.getTimeToStart();
		// Adding more than one timer in order to speed up the processing of
		// transactions.
		int nbrProcessApprovedBackgroundTimers = processApprovedBackgroundTimers();
		for (int timerCount = 0; timerCount < nbrProcessApprovedBackgroundTimers; timerCount++) {
			timerStartValue += (timerCount * 100);
			mySessionCtx.getTimerService().createTimer(timerStartValue, config);
		}
		mLogr.debug("S26-BGProcessEJB.start(): #timers = " + getNbrTimers());
	}

	public void ejbTimeout(Timer timer) {
		mLogr.debug("S26-ejbTimeout();type = "+ timer.getInfo().getClass().getName()+";#timers = " + getNbrTimers() +
				"; bgScoringStarted = " + bgScoringStarted + ";bgApprovedBankStarted=" + bgApproveBankStarted);

		//Timers get persisted and hang around when app is restarted, ejbTimeout occurs before loader
		//even has a chance to issue starts of the EJBs.  Get rid of all past timers
		if (!bgScoringStarted && !bgApproveBankStarted) {
			cancelAllTimers();
		}
		Serializable timerInfo = timer.getInfo();
		if (timerInfo instanceof BackgroundProcessorConfig) {
			if (!bgScoringStarted) {
				cancelTimers (timerInfo);
				return;
			}
			executeBackgroundProcessor(timer);
			return;
		}
		if (timerInfo instanceof ProcessApprovedBankTxProcessorConfig) {
			if (!bgApproveBankStarted) {
				cancelTimers (timerInfo);
				return;
			}
			executeProcessApprovedBankTx(timer);
			return;
		}
	}

	//Cancels all timers of the type found in timerInfo
	private void cancelTimers(Serializable timerInfo) {
		mLogr.debug("S26-cancelTimers; Type = " + timerInfo.getClass().getName());
		Collection<Timer> timers = mySessionCtx.getTimerService().getTimers();
		if (timers == null) {
			return;
		}
		for (Iterator<Timer> iterator = timers.iterator(); iterator.hasNext();) {
			Timer timer = iterator.next();
			Serializable ti = timer.getInfo();
			if (ti.getClass().getName().equals(timerInfo.getClass().getName())) {
				timer.cancel();
			}
		}
	}

	/***
	 * This method processes approved bank transactions after 4 hours of being placed into this state
	 *
	 * @param timer
	 */
	private void executeProcessApprovedBankTx(Timer timer) {
		ProcessApprovedBankTxProcessorConfig config = (ProcessApprovedBankTxProcessorConfig) timer.getInfo();

		int backGroundExceptions = 0;
		int maxErrors = config.getMaxErrors();
		try {
			ApprovedBankTxProcessorRequest bgpr = new ApprovedBankTxProcessorRequest();
			bgpr.setContextRoot(config.getContextName());
			bgpr.setRequestProcessingParameter("transaction");
			bgpr.setRequestResultCode(BackgroundProcessInitiator.RESPONSE_PROCESSED);
			bgpr.setMinimumTimeToProcess(config.getMinimumTimeToProcess());
			try {
				bgpr = ApprovedBankTxProcessInitiator.getInstance().executeRequest(bgpr);
			} catch (Exception hce) {
				hce.printStackTrace();
				mLogr.error("Background Processor (executeProcessApprovedBankTx) Exception ", hce);
				return;
			}
			int retCode = 999;
			try {
				retCode = bgpr.getRequestResultCode();
			} catch (Exception e) {
			}
			switch (retCode) {
				case BackgroundProcessInitiator.RESPONSE_PROCESSED: {
					mLogr.info("Background Processor Bean (executeProcessApprovedBankTx) finished the transaction sucessfully");
					reschedule(timer, config.getTimeToProcessAgain());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_CODE_NO_WORK: {
					mLogr.info("No work to do in Background Processor Bean (executeProcessApprovedBankTx)");
					reschedule(timer, config.getTimeToPollBackAfterIdle());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_CODE_BUSY: {
					mLogr.info("Background Processor Bean (executeProcessApprovedBankTx) is busy, waiting");
					reschedule(timer, config.getTimeToRetryWhenBusy());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_BAD_PARM: {
					mLogr.info("Invalid Parm to Background Processor Bean (executeProcessApprovedBankTx)");
					reschedule(timer, config.getTimeToRetryAfterError());
					break;
				}
				case 500: {
					backGroundExceptions++;
					mLogr.info("Exception in Background Processor Bean (executeProcessApprovedBankTx).");
					if (backGroundExceptions > maxErrors) {
						mLogr.info("Too many errors in Background Processor Bean (executeProcessApprovedBankTx); processor will be stopped");
						timer.cancel(); // too many errors, stop now
					} else {
						mLogr.info("Sleep and try again.");
						reschedule(timer, config.getTimeToRetryAfterError());
					}
					break;
				}
				default: {
					mLogr.error("Invalid Background Processor Bean (executeProcessApprovedBankTx) response:" + bgpr.getRequestResultCode());
					backGroundExceptions++;
					if (backGroundExceptions > maxErrors) {
						mLogr.info("Too many errors in Background Processor Bean (executeProcessApprovedBankTx); processor will be stopped");
						timer.cancel(); // too many errors, stop now
					} else {
						mLogr.info("Sleep and try again.");
						reschedule(timer, config.getTimeToRetryAfterError());
					}
					break;
				}
			}
		} catch (Exception e) {
			mLogr.error("Background Processor Bean (executeProcessApprovedBankTx) main loop severe exception, halting the processor.",e);
		}
	}

	private void executeBackgroundProcessor(Timer timer) {
		BackgroundProcessorConfig config = (BackgroundProcessorConfig) timer.getInfo();
		int backGroundExceptions = 0;
		int maxErrors = config.getMaxErrors();
		try {
			BackGroundProcessorRequest bgpr = new BackGroundProcessorRequest();
			bgpr.setContextRoot(config.getContextName());
			bgpr.setRequestProcessingParameter("transaction");
			bgpr.setRequestResultCode(BackgroundProcessInitiator.RESPONSE_PROCESSED);
			try {
				bgpr = BackgroundProcessInitiator.getInstance().executeBackGroundRequest(bgpr);
			} catch (Exception hce) {
				hce.printStackTrace();
				mLogr.error("Background Processor Exception ", hce);
				return;
			}
			int retCode = 999;
			try {
				retCode = bgpr.getRequestResultCode();
			} catch (Exception e) {
			}
			switch (retCode) {
				case BackgroundProcessInitiator.RESPONSE_PROCESSED: {
					mLogr.info("Background Processor Bean finished the transaction sucessfully");
					reschedule(timer, config.getTimeToProcessNext());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_CODE_NO_WORK: {
					mLogr.info("No work to do in Background Processor Bean");
					reschedule(timer, config.getTimeToPollBackAfterIdle());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_CODE_BUSY: {
					mLogr.info("Background Processor Bean is busy, waiting");
					reschedule(timer, config.getTimeToRetryWhenBusy());
					break;
				}
				case BackgroundProcessInitiator.RESPONSE_BAD_PARM: {
					mLogr.info("Invalid Parm to Background Processor Bean");
					reschedule(timer, config.getTimeToRetryAfterError());
					break;
				}
				case 500: {
					backGroundExceptions++;
					mLogr.info("Exception in Background Processor Bean.");
					if (backGroundExceptions > maxErrors) {
						mLogr.info("Too many errors in Background Processor Bean; processor will be stopped");
						timer.cancel(); // too many errors, stop now
					} else {
						mLogr.info("Sleep and try again.");
						reschedule(timer, config.getTimeToRetryAfterError());
					}
					break;
				}
				default: {
					mLogr.error("Invalid Background Processor Bean response:" + bgpr.getRequestResultCode());
					backGroundExceptions++;
					if (backGroundExceptions > maxErrors) {
						mLogr.info("Too many errors in Background Processor Bean; processor will be stopped");
						timer.cancel(); // too many errors, stop now
					} else {
						mLogr.info("Sleep and try again.");
						reschedule(timer, config.getTimeToRetryAfterError());
					}
					break;
				}
			}
		} catch (Exception e) {
			mLogr.error("Background Processor Bean main loop severe exception, halting the processor.",e);
		}
	}

	private void reschedule(Timer timer, long timeout) {
		mySessionCtx.getTimerService().createTimer(timeout, timer.getInfo());
	}
	private int backgroundTimers()
	{
		int nbrBackgroundTimers;
		try {
			nbrBackgroundTimers = EMTAdmContainerProperties
					.getNbrBackgroundTimers();
			if (nbrBackgroundTimers < 1) {
				nbrBackgroundTimers = 1;
			}
			return nbrBackgroundTimers;
		} catch (Exception ex) {
			mLogr.error("Please configure the value for nbrBackgroundTimers in container property");
			nbrBackgroundTimers = 1;
			return nbrBackgroundTimers;
		}
	}

	private int processApprovedBackgroundTimers() {
		int nbrProcessApprovedBackgroundTimers;
		try {
			nbrProcessApprovedBackgroundTimers = EMTAdmContainerProperties
					.getNbrProcessApprovedBackgroundTimers();
			if (nbrProcessApprovedBackgroundTimers < 1) {
				nbrProcessApprovedBackgroundTimers = 1;
			}
			return nbrProcessApprovedBackgroundTimers;
		} catch (Exception ex) {
			mLogr.error("Please configure the value for nbrProcessApprovedBackgroundTimers in container property");
			nbrProcessApprovedBackgroundTimers = 1;
			return nbrProcessApprovedBackgroundTimers;
		}

	}

}