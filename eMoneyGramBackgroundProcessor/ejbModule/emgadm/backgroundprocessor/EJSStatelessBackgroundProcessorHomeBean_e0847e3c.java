package emgadm.backgroundprocessor;

import com.ibm.ejs.container.*;

/**
 * EJSStatelessBackgroundProcessorHomeBean_e0847e3c
 */
public class EJSStatelessBackgroundProcessorHomeBean_e0847e3c extends EJSHome {
	static final long serialVersionUID = 61;
	/**
	 * EJSStatelessBackgroundProcessorHomeBean_e0847e3c
	 */
	public EJSStatelessBackgroundProcessorHomeBean_e0847e3c() throws java.rmi.RemoteException {
		super();	}
	/**
	 * create_Local
	 */
	public emgadm.backgroundprocessor.BackgroundProcessorLocal create_Local() throws javax.ejb.CreateException, java.rmi.RemoteException {
BeanO beanO = null;
emgadm.backgroundprocessor.BackgroundProcessorLocal result = null;
boolean createFailed = false;
boolean preCreateFlag = false;
try {
	result = (emgadm.backgroundprocessor.BackgroundProcessorLocal) super.createWrapper_Local(null);
}
catch (javax.ejb.CreateException ex) {
	createFailed = true;
	throw ex;
} catch (java.rmi.RemoteException ex) {
	createFailed = true;
	throw ex;
} catch (Throwable ex) {
	createFailed = true;
	throw new CreateFailureException(ex);
} finally {
	if (createFailed) {
		super.createFailure(beanO);
	}
}
return result;	}
}
